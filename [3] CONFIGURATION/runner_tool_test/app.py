import datetime
import time
from classes.rerun_comparison import RerunTypeComparison
from classes.rerun_webshots import RerunTypeWebshots
from classes.rerun_rankings import RerunTypeRankings
from classes.rerun_listings import RerunTypeListings
from pprint import pprint

if __name__ == "__main__":
	start_time = time.time()
	breaker = False

	while (not breaker):
		try:
			choice = input("SELECT CHOICE: \n[1] Do bulk rerun \n[2] Exit\n\r")
			if choice == '1':
				cc = input("Rerun type: ")
				if "rerun_webshots" in cc:
					try:
						temp = cc.split(" ")
						rtype = RerunTypeWebshots()

						num_items = rtype.execute_run(temp)
						pprint(time.time() - start_time)
						pprint('Total numbers of items generated: {num_items}')
					except:
						pprint("rerun type not found. . . ")
				
				elif "rerun_comparison" in cc or "rerun_cmp" in cc or "rerun_cmp.py" in cc:
					try:
						temp = cc.split(" ")
						rtype = RerunTypeComparison()
						num_items = rtype.execute_run(temp)
						pprint(round(time.time() - start_time), 2)
						pprint('Total numbers of items generated: {num_items}')
					except:
						pprint("rerun type not found. . . ")
				
				elif "rerun_rankings" in cc or "rerun_rnk" in cc or "rerun_rnk.py" in cc:
					try:
						temp = cc.split(" ")
						rtype = RerunTypeRankings()

						num_items = rtype.execute_run(temp)
						pprint('Total numbers of items generated: {num_items}')
					except Exception as e:
						print(e, "\n")
						print('Rerun type not found\n')

				elif "rerun_listings" in cc or "rerun_lst" in cc or "rerun_lst.py" in cc:
					try:
						temp = cc.split(' ')
						rtype = RerunTypeListings()

						num_items = rtype.execute_run(temp)
						pprint(f'Total numbers of items generated: {num_items}')
					except Exception as e:
						print(e, "\t", "Rerun type not found")

				elif "-h" or '-help' in cc:
					pprint("Rerun Webshots Format: rerun_webshots for type | -c for company | -s for source | -d for date ex. rerun_webshots -s etl -d 2021-01-17\n")
					pprint("Rerun Comparison Format: rerun_comparison for type | -s for status | -d for date ex. rerun_comparison -s crawl_failed -d 2021-01-17\n")
					pprint("Rerun Rankings Format: rerun_rankings for type | -s for status | -d for date ex. rerun_rankings -s crawl_failed -d 2021-12-04@22:00:00\n")
					print('\n')
			elif choice == '2':
				pprint("Goodbye")
				breaker = True
			else:
				print("\nInvalid input format! please select from the following:")
		except:
			pprint("Rerun type doesn't exist. You may use '-h' for help")