class DocumentWriter:
    def __init__(self):
        pass

    def get_input_data(self):
        # open file
        data = None
        with open('./docs/input_item_ids.txt') as f:
            data = [items.rstrip() for items in f]
        
        with open('./docs/rerun_cmd_generated.txt', 'r+') as f:
            f.truncate(0)
            f.close()
            
        return data

    def write_command(self, cmdtxt):
        try:
            with open('./docs/rerun_cmd_generated.txt', 'w') as f:
                f.write(cmdtxt)
                f.close()
            with open('./docs/input_item_ids.txt', 'r+') as f:
                f.truncate(0)
                f.close()
            return True
        except:
            return False
