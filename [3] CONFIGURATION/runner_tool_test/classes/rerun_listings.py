from pprint import pprint
from classes.doc_writer import DocumentWriter

class RerunTypeListings:
    def __init__(self):
        pass

    def execute_run(self, data):
        dwriter = DocumentWriter()
        items = dwriter.get_input_data()
        rstatus = data[2]
        rdate = data[-1]
        item_holder = list()

        if len(items):
            for pid in items:
                item = f'{rdate}{pid}'
                item_holder.append(item)

            merged_items = ','.join(item_holder)
            cmd = f'python rerun_lst.py -s {rstatus.upper()} -l {merged_items}'
            pprint(cmd)
            is_written = dwriter.write_command(cmd)

            if is_written:
                return len(items)
            else:
                return 0