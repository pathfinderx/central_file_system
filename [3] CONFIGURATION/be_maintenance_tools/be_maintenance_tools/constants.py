from enum import Enum

CLIENT_CODE_LIST = [
    '010', #InRiver Prod
    #'100', #Garmin
    '110', #Nintendo Benelux
    #'200', #Zound Industries
    '610', #Eva Solo
    #'A00', #Microsoft
    #'A10', #3M (2021)
    #'F00', #Samsung
    #'H10', #Zurn
    'K00', #Razer
    #'O00', #Samsung WG
    'P00', #Motorola
    'Q00', #Haglöfs
    #'U00', #Nintendo
    #'X00', #Nintendo Mexico
    #'Z00', #Sonos
]

CLIENT_PREMIUM_SCHEDULE = {
    '0': {'clients': ['200', '010', 'A10']},
    '1': {'clients': ['200', '100']},
    '2': {'clients': ['200', 'P00']},
    '3': {'clients': ['200', 'K00']},
    '4': {'clients': ['200', 'F00', 'O00']},
    '5': {'clients': ['200', '610', 'Q00', 'Z00']},
    '6': {'clients': ['200', '110', 'U00', 'X00', 'M00']}
}

class RMProductFields(Enum):
    PRICE = "price"
    CURRENCY = "currency"
    RATING_SCORE = "rating score"
    RATING_REVIEWS = "rating reviews"
    AVAILABILITY = "availability"
    TITLE = "title"

class SQLProductFields(Enum):
    PRICE = "price_value"
    CURRENCY = "price_currency"
    RATING_SCORE = "rating_score"
    RATING_REVIEWS = "rating_reviewers"
    AVAILABILITY = "in_stock"
    TITLE = "product_website"
