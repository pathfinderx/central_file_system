import pyodbc
import time

class MsDatabaseParams(object):
    """
    Class that encapsulates database parameters.
    """

    def __init__(self,
                 driver: str,
                 server: str,
                 database: str,
                 user: str,
                 password: str):
        self.driver = driver
        self.server = server
        self.database = database
        self.user = user
        self.password = password

class MSDatabaseConnection(object):
    RETRY_LIMIT = 5
    SLEEP_TIME = 1  # seconds

    def __init__(self, db_params: MsDatabaseParams, timeout=30):
        """
        Class constructor. Automatically attempts to connect to the database
        based on the parameters.

        :param db_params: The database parameters.
        :param timeout: Timeout.
        """
        self._driver = db_params.driver
        self._server = db_params.server
        self._database = db_params.database
        self._user = db_params.user
        self._password = db_params.password
        self._conn_timeout = timeout

        # Connect automatically
        self._connection = self._connect()
        print(f"[x] Connected to {self._database} database.")

    def _connect(self):
        """
        Connects to the database and returns the connection object.

        :return: The connection object.
        """
        connection = pyodbc.connect("Driver=%s;Server=%s;Database=%s;Uid=%s;\
                                    Pwd=%s;Connection Timeout=%s"
                                    % (self._driver,
                                       self._server,
                                       self._database,
                                       self._user,
                                       self._password,
                                       self._conn_timeout),
                                    timeout=self._conn_timeout,
                                    autocommit=False)
        connection.timeout = self._conn_timeout
        return connection

    def _disconnect(self):
        if self._connection:
            self._connection.close()
            print(f"[x] Disconnected to {self._database} database.")

    def _reconnect(self):
        if self._connection:
            self._disconnect()
        time.sleep(1)
        self._connection = self._connect()
        print(f"[x] Reconnected to {self._database} database.")

    def disconnect(self):
        self._disconnect()

    def execute_query(self, query, params=None,
                      with_results=False, commit=False):
        """
        Common method for executing SQL statements

        :param query: SQL statement
        :param params: parameters
        :param with_results: flag to return results from query
        :param commit: flag to commit a transaction

        :return: list of records or None
        """

        records = list()

        with self._connection.cursor() as cursor:
            retry_flag = True
            retry_count = 0
            while retry_flag and retry_count < self.__class__.RETRY_LIMIT:
                try:
                    if params:
                        cursor.execute(query, params)
                    else:
                        cursor.execute(query)
                    retry_flag = False
                except pyodbc.OperationalError:
                    print("[x] MSSQL communication link failed.\n")
                    self._disconnect()

                    print("Retry after 1 second")
                    retry_count = retry_count + 1
                    time.sleep(self.__class__.SLEEP_TIME)
                    self._disconnect()
                    self._reconnect()

            if with_results:
                columns = [column[0] for column in cursor.description]

                for row in cursor.fetchall():
                    records.append(dict(zip(columns, row)))

            if commit:
                cursor.commit()

        if with_results:
            return records
