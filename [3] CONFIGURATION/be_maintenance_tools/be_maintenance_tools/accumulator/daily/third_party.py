from accumulator.base import BaseAccumulator


class ThirdPartyDailyIssuesAccumulator(BaseAccumulator):
    FETCH_QUERY = """
    """

    def run(self):
        data = self.db_connection.execute_query(
            query=self.__class__.FETCH_QUERY,
            params=self.target_date,
            with_results=True)

        return data
