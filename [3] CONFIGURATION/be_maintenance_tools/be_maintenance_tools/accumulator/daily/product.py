from accumulator.base import BaseAccumulator


class ProductDailyIssuesAccumulator(BaseAccumulator):
    FETCH_QUERY = """
        declare @date date = ?;

        select
            'No' as premium,
            company,
            string_agg(issue_field, ',') within group (order by issue_field)
                as fields,
            retailer as website,
            data_id as item_id,
            string_agg(issue, ',') as issues
        from (
            select
                company,
                data_id,
                retailer,
                json.[key] as issue_field,
                case 
                    when json.value = 'NOT_FOUND' then '-1 ' + json.[key]
                    when json.value like '%extraction failed'
                        then '-3 ' + json.[key]
                    else '-2 ' + json.[key]
                end as issue
            from rm_internal_log ril
            cross apply openjson(payload, '$.fields') as json
            where process_name like '%transform%'
            and log_type = 'warning'
            and v_schedule_date = @date
            and data_id not like 'RANK%'
            and data_id not like 'LIST%'
            and data_id not like 'PS%'
            and (
                case
                    when company = 'A00' and (
                    case
                        when json.[key] = 'price' and
                            json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'availability' then 1
                        when json.[key] = 'title' then 1
                        else 0
                    end
                    ) = 1 then 1
                    when company = 'F00' and (
                    case 
                        when json.[key] = 'availability' then 1
                        when json.[key] = 'title' then 1
                        when json.[key] = 'delivery' and
                            json.[value] != 'NOT_FOUND' then 1
                        else 0
                    end
                    ) = 1 then 1
                    when company in ('100','O00') and (
                    case 
                        when json.[key] = 'availability' then 1
                        when json.[key] = 'rating score'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'rating reviews'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'title' then 1
                        when json.[key] = 'delivery'  and
                            json.[value] != 'NOT_FOUND' then 1
                        else 0
                    end
                    ) = 1 then 1
                    when company in ('010','110','610','A10','K00',
                        'P00','Q00','U00','X00','Z00') and (
                    case 
                        when json.[key] = 'price'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'availability' then 1
                        when json.[key] = 'rating score'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'rating reviews'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'title' then 1
                        else 0
                    end
                    ) = 1 then 1
                    when company = '200' 
                    and (
                    case 
                        when retailer in (
                            'amazon.co.uk/3P',
                            'amazon.de/3P',
                            'amazon.es/3P',
                            'amazon.fr/3P',
                            'amazon.it/3P',
                            'bol.com/3P',
                            'cdiscount.com/3P',
                            'comparer.be',
                            'darty.com/3P',
                            'fnac.com/3P',
                            'fr.toppreise.ch',
                            'heureka.cz',
                            'hintaopas.fi',
                            'idealo.at',
                            'idealo.co.uk',
                            'idealo.de',
                            'idealo.es',
                            'idealo.fr',
                            'pricerunner.dk',
                            'pricerunner.se',
                            'prisjakt.no',
                            'prisjakt.se',
                            'trovaprezzi.it',
                            'vergelijk.nl'
                        ) then 0
                        when json.[key] = 'price'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'availability' then 1
                        when json.[key] = 'rating score'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'rating reviews'
                            and json.[value] != 'NOT_FOUND' then 1
                        when json.[key] = 'title' then 1
                        else 0
                    end
                    ) = 1 then 1
                else 0
                end
            ) = 1
            group by
                company,
                data_id,
                retailer,
                json.[key],
                json.[value]
        ) t
        group by
            company,
            data_id,
            retailer
        order by retailer;
    """

    def run(self):
        data = self.db_connection.execute_query(
            query=self.__class__.FETCH_QUERY,
            params=self.target_date,
            with_results=True)

        return data
