from accumulator.base import BaseAccumulator
from clients.ms_db import MSDatabaseConnection


class ProductPremiumIssuesAccumulator(BaseAccumulator):
    FETCH_QUERY = """
        declare @date date = ?;

        select
            'Yes' as premium,
            company,
            string_agg(issue_field, ',') within group (order by issue_field)
                as fields,
            retailer as website,
            data_id as item_id,
            string_agg(issue, ',') as issues
        from (
            select
                company,
                data_id,
                retailer,
                case
                    when json.[key] = 'image_urls' then 'image_count'
                    else json.[key]
                end as issue_field,
                case
                    when json.[key] = 'image_urls' then 'No image_count'
                    when json.value = 'NOT_FOUND' then '-1 ' + json.[key]
                    when json.value like '%extraction failed'
                        then '-3 ' + json.[key]
                    else '-2 ' + json.[key]
                end as issue
            from rm_internal_log ril
            cross apply openjson(payload, '$.fields') as json
            where process_name like '%transform%'
            and log_type = 'warning'
            and v_schedule_date = @date
            and data_id not like 'RANK%'
            and data_id not like 'LIST%'
            and data_id not like 'PS%'
            and company in ({companies})
            and (
                case
                    when company = '200' 
                        and retailer in (
                            'amazon.co.uk/3P',
                            'amazon.de/3P',
                            'amazon.es/3P',
                            'amazon.fr/3P',
                            'amazon.it/3P',
                            'bol.com/3P',
                            'cdiscount.com/3P',
                            'comparer.be',
                            'darty.com/3P',
                            'fnac.com/3P',
                            'fr.toppreise.ch',
                            'heureka.cz',
                            'hintaopas.fi',
                            'idealo.at',
                            'idealo.co.uk',
                            'idealo.de',
                            'idealo.es',
                            'idealo.fr',
                            'pricerunner.dk',
                            'pricerunner.se',
                            'prisjakt.no',
                            'prisjakt.se',
                            'trovaprezzi.it',
                            'vergelijk.nl'
                        ) then 0
                    when json.[key] = 'description' then 1
                    when json.[key] = 'image_urls' then 1
                    else 0
                end
            ) = 1
            group by
                company,
                data_id,
                retailer,
                json.[key],
                json.[value]
        ) t
        group by
            company,
            data_id,
            retailer
        order by retailer;
    """

    def __init__(self, db_connection: MSDatabaseConnection,
                 target_date: str, clients: str):
        super().__init__(db_connection, target_date)
        self.clients = clients

    def run(self):
        data = self.db_connection.execute_query(
            query=self.__class__.FETCH_QUERY.format(companies=self.clients),
            params=self.target_date,
            with_results=True)

        return data
