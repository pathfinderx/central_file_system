from abc import ABCMeta, abstractmethod
from clients.ms_db import MSDatabaseConnection

class BaseAccumulator(metaclass=ABCMeta):
    def __init__(self, db_connection: MSDatabaseConnection, target_date: str):
        self.db_connection = db_connection
        self.target_date = target_date

    @abstractmethod
    def run(self):
        raise NotImplementedError
