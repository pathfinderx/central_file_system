import csv

from writer.base import baseIssuesWriter
from helper.rename_fields import rename_product_fields


class ProductIssuesWriter(baseIssuesWriter):
    FILENAME_PREFIX = 'product_tech_issues'
    FIELD_NAMES = ['date', 'premium', 'company', 'fields',
                   'website', 'item_id', 'issues']

    def sort_data(self):
        clients_websites_issues_dict = dict()
        sorted_data_list = list()
        separator = '&'

        for row in self.data_list:
            key = separator.join((row['premium'], row['company'], row['fields'],
                                  row['website'], row['issues'],))
            if key not in clients_websites_issues_dict.keys():
                clients_websites_issues_dict.update(
                    {key: row['item_id']})
            else:
                clients_websites_issues_dict[key] = '\n'.join((
                    clients_websites_issues_dict[key], row['item_id']))

        for key in clients_websites_issues_dict.keys():
            tech_issues_dict = dict(
                date=self.target_date,
                premium=key.split(separator)[0],
                company=key.split(separator)[1],
                fields=key.split(separator)[2],
                website=key.split(separator)[3],
                item_id=clients_websites_issues_dict[key],
                issues=key.split(separator)[4]
            )
            sorted_data_list.append(tech_issues_dict)

        return sorted_data_list

    def write(self):
        sorted_data_list = self.sort_data()
        print('[x] {} fetched technical issues'.format(len(sorted_data_list)))

        filename_ = '_'.join((self.__class__.FILENAME_PREFIX, self.target_date))
        path_filename = ''.join((self.__class__.PATH, filename_, '.csv'))

        with open(path_filename, self.__class__.WRITE_MODE,
                  encoding=self.__class__.FILE_ENCODING,
                  newline=self.__class__.FILE_NEW_LINE) as f:
            writer = csv.DictWriter(f, fieldnames=self.__class__.FIELD_NAMES)
            writer.writeheader()
            writer.writerows(sorted_data_list)
