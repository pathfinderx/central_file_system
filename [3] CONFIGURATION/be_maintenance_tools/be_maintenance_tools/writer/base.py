from abc import ABCMeta, abstractmethod
from tarfile import ENCODING

class baseIssuesWriter(metaclass=ABCMeta):
    PATH = 'output/'
    WRITE_MODE = 'a'
    FILE_ENCODING = 'UTF8'
    FILE_NEW_LINE = ''

    def __init__(self, data_list: list, target_date: str):
        self.data_list = data_list
        self.target_date = target_date

    @abstractmethod
    def sort_data(self):
        raise NotImplementedError

    @abstractmethod
    def write(self):
        raise NotImplementedError
