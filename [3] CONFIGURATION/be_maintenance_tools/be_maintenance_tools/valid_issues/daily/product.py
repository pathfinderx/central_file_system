DAILY_PROD_VALID_ISSUES = {
    'A00DEA40GC000000333630201903041013': ['-1 product_website'],
    'A00DEA40GC000000319400201903041007': ['-1 product_website'],
    'A00DEA40GC000000260540201903040940': ['-1 product_website'],
    'A00DEA40RB000000610590201905240704': ['-1 product_website'],
    'A00DEA40GC000000218440201903040911': ['-1 product_website'],
    'A00DEA40GC000000344340201903041017': ['-1 product_website'],
    'A00DEA40GC000000152960201903040503': ['-1 product_website'],
    'A00SG350KT020211012053255664519285': ['-1 in_stock'],
    'A00SG350KT020211012053255859942285': ['-1 in_stock'],
    'A00SG350KT020211012053256053463285': ['-1 in_stock'],
    'A00SG350RB000001191650202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191380202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191550202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191570202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191490202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191580202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191470202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001246630202011230345': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191410202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191480202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191440202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191460202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191560202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191430202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191520202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191450202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001246620202011230345': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191390202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001246640202011230345': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191420202008110404': ['-1 in_stock', '-1 product_website'],
    'A00SG350RB000001191660202008110404': ['-1 in_stock', '-1 product_website'],
    'P00RSZV01L000001593550202109200719': ['-1 product_website'],
    'P00RSZV01L000001623060202110180741': ['-1 product_website'],
    'P00RSZV01L000001454800202106220209': ['-1 product_website'],
    'P00RSZV01L000001454810202106220209': ['-1 product_website'],
    '200FIF0070000000551610201904050646': ['-1 in_stock'],
    '200FIF0070000000804410201907080521': ['-1 in_stock'],
    '200FIF0070000000552430201904050800': ['-1 in_stock'],
    'A00FRC40GC000000288480201903040955': ['-1 product_website'],
    'F00NLZG0JA000000858660201910110308': ['-1 product_website'],
    'F00NLZG0JA000000858830201910110315': ['-1 product_website'],
    'F00NLZG0JA000000858980201910110318': ['-1 product_website'],
    'F00NLZG0JA000000859430201910110332': ['-1 product_website'],
    'F00NLZG0JA000000859760201910110343': ['-1 product_website'],
    'F00NLZG0JA000000860080201910110358': ['-1 product_website'],
    'F00NLZG0JA000000859970201910110355': ['-1 product_website'],
    'F00NLZG0JA000000860540201910110412': ['-1 product_website'],
    'F00NLZG0JA000000859270201910110326': ['-1 product_website'],
    'A00AUE40RB000000877630201911080751': ['-1 product_website'],
    'A00AUE40RB000001411570202105120254': ['-1 product_website'],
    'A00AUE40RB000000877750201911080757': ['-1 product_website'],
    'A00AUE40RB000000886810201911201313': ['-1 product_website'],
    'P00HRGV01L000001460540202106280511': ['-1 product_website'],
    'A00AUE40RB000000877850201911080803': ['-1 product_website'],
    'A00AUE40RB000000955680202005250709': ['-1 product_website'],
    'A00AUE40RB000001411550202105120254': ['-1 product_website'],
    'A00AUE40RB000001550910202108170453': ['-1 product_website'],
    'A00AUE40RB000001550880202108170453': ['-1 product_website'],
    'A00AUE40RB000000877580201911080747': ['-1 product_website'],
    'A00AUE40RB000000886740201911201302': ['-1 product_website'],
    'A00AUE40RB000000877860201911080804': ['-1 product_website'],
    'A00AUE40RB000000972780202006232222': ['-1 product_website'],
    'A00AUE40RB000001246610202011230344': ['-1 product_website'],
    'A00AUE40RB000001411600202105120254': ['-1 product_website'],
    'A00AUE40RB000001411540202105120254': ['-1 product_website'],
    'A00AUE40RB000001411580202105120254': ['-1 product_website'],
    'A00AUE40RB000001411520202105120254': ['-1 product_website'],
    'A00AUE40RB000001411500202105120254': ['-1 product_website'],
    'A00AUE40RB000001411470202105120254': ['-1 product_website'],
    'A00AUE40RB000000878370201911080848': ['-1 product_website'],
    'A00AUE40RB000000890940201911262214': ['-1 product_website'],
    'A00AUE40RB000000878270201911080841': ['-1 product_website'],
    'A00AUE40RB000000877690201911080757': ['-1 product_website'],
    'A00AUE40RB000000876960201911080708': ['-1 product_website'],
    'A00AUE40RB000000955850202005250709': ['-1 product_website'],
    'A00AUE40RB000000878320201911080841': ['-1 product_website'],
    'A00AUE40RB000000972790202006232224': ['-1 product_website'],
    'A00AUE40RB000000955880202005250709': ['-1 product_website'],
    'A00AUE40RB000000955910202005250709': ['-1 product_website'],
    'A00AUE40RB000000886720201911201259': ['-1 product_website'],
    'A00AUE40RB000000877250201911080724': ['-1 product_website'],
    'A00AUE40RB000000877170201911080717': ['-1 product_website'],
    'A00AUE40RB000001344800202103100631': ['-1 product_website'],
    'F00NLZG0UD020220103055432334565003': ['-1 product_website'],
    'F00NLZG0ME020211207021624871773341': ['-1 product_website'],
    'F00NLZG0WJ000001582600202109130523': ['-1 product_website'],
    'F00NLZG040000001550480202108170144': ['-1 product_website'],
    'F00NLZG040000001550630202108170144': ['-1 product_website'],
    'F00NLZG040000001550050202108170144': ['-1 product_website'],
    'F00NLZG0JA000001423850202105250316': ['-1 product_website'],
    'F00NLZG0JA000001423490202105250316': ['-1 product_website'],
    'F00NLZG0JA000001416340202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416370202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416620202105170845': ['-1 product_website'],
    'F00NLZG0JA000001423670202105250316': ['-1 product_website'],
    'F00NLZG0JA000001423760202105250316': ['-1 product_website'],
    'F00NLZG0JA000001416650202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416430202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416450202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416480202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416510202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416540202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416570202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416290202105170845': ['-1 product_website'],
    'F00NLZG0WJ000001415820202105170845': ['-1 product_website'],
    'F00NLZG0WJ000001415840202105170845': ['-1 product_website'],
    'F00NLZG0JA000001415870202105170845': ['-1 product_website'],
    'F00NLZG040000001415080202105170845': ['-1 product_website'],
    'F00NLZG040000001415110202105170845': ['-1 product_website'],
    'F00NLZG040000001415140202105170845': ['-1 product_website'],
    'F00NLZG040000001415180202105170845': ['-1 product_website'],
    'F00NLZG040000001415240202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416010202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416030202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416090202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416060202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416950202105170845': ['-1 product_website'],
    'F00NLZG0JA000001416980202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417040202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417280202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417310202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417340202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417400202105170845': ['-1 product_website'],
    'F00NLZG0EL000001417640202105170845': ['-1 product_website'],
    'F00NLZG0JA000001417190202105170845': ['-1 product_website'],
    'F00NLZG040000001414900202105170845': ['-1 product_website'],
    'F00NLZG040000001414930202105170845': ['-1 product_website'],
    'F00NLZG040000001414960202105170845': ['-1 product_website'],
    'F00NLZG040000001414300202105170844': ['-1 product_website'],
    'F00NLZG040000001414330202105170844': ['-1 product_website'],
    'F00NLZG040000001414370202105170844': ['-1 product_website'],
    'P00PLXT01L000001341010202103050205': ['-1 product_website'],
    'P00PLXT01L000001341040202103050205': ['-1 product_website'],
    'P00PLXT01L000001428700202106010622': ['-1 product_website'],
    'P00PLXT01L000001454290202106210443': ['-1 product_website'],
    'P00ESL201L000001348550202103180106': ['-3 rating_score'],
    'P00ESL201L000001459330202106230519': ['-3 rating_score'],
    '100NOGT010000001439890202106090650': ['-1 availability'],
    '100NOGT010000001439890202106090650': ['-1 product_website'],
    '100NOGT010000001439890202106090650': ['-1 availability', '-1 product_website'],
    'Q00FRCX0IJ000001564590202108230711': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220328070519658931087': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220328070519788922087': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220328070519954552087': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220328070520201512087': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014631427197095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014631561495095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014631692769095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014638972337095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014639126680095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014706580180095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014707115830095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014707261590095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014707388691095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014707543245095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014707678384095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014708373622095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014708506375095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014708656947095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014708813882095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014708981702095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709118594095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709249689095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709378666095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709513926095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709668104095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709800865095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220405014709931729095': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043826764353101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043827095700101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043827299207101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043827430442101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043827610192101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'Q00FRCX07V020220411043827887652101': ['-3.00 price_value', '-3 in_stock', '-3 rating_score', '-3 rating_reviewers', '-3 product_website'],
    'F00BENX040000001658010202111152217': ['-1 product_website'],
    'F00BENX0EL000001658040202111152217': ['-1 product_website'],
    'F00BENX040000001635490202111040406': ['-1 product_website'],
    'F00BENX040000001635640202111040406': ['-1 product_website'],
    'F00BENX040000001636010202111040406': ['-1 product_website'],
    'F00BENX040000001636070202111040406': ['-1 product_website'],
    'F00BENX040000001636240202111040406': ['-1 product_website'],
    'F00BEQX0ME020211207020310528150341': ['-1 product_website'],
    'P00GBTT01L000001340160202103040626': ['-1 in_stock'],
    'P00RSZV01L000001454800202106220209': ['-1 product_website'],
    'P00RSZV01L000001454810202106220209': ['-1 product_website'],
    'P00RSZV01L000001593550202109200719': ['-1 product_website'],
    'P00RSZV01L000001623060202110180741': ['-1 product_website'],
}