from abc import ABCMeta, abstractmethod


class BaseIssuesChecker(metaclass=ABCMeta):
    def __init__(self, data_list: list):
        self.data_list = data_list

    @abstractmethod
    def _check_data(self):
        raise NotImplementedError

    @abstractmethod
    def run(self):
        raise NotImplementedError
