from checker.base import BaseIssuesChecker
from valid_issues.product import PRODUCT_VALID_ISSUES
from helper.rename_fields import rename_product_fields


class ProductIssueChecker(BaseIssuesChecker):
    def _check_data(self, item_id: str, fields_list: list, issues_list: list):
        add_to_list = False
        fields = None
        issues = None

        if item_id not in PRODUCT_VALID_ISSUES.keys():
            add_to_list = True
            fields = ','.join(str(field) for field in fields_list)
            issues = ','.join(str(issue) for issue in issues_list)
        else:
            fields = str()
            issues = str()
            for issue in issues_list:
                if issue not in PRODUCT_VALID_ISSUES[item_id]:
                    fields = ','.join((fields, issue.split(' ')[-1]))
                    issues = ','.join((issues, issue))
            fields = fields[1:]
            issues = issues[1:]
            if issues:
                add_to_list = True

        return add_to_list, fields, issues

    def run(self):
        tech_issues = list()
        add_to_list = False

        for row in self.data_list:
            row_fields = rename_product_fields(fields=row['fields'])
            row_issues = rename_product_fields(fields=row['issues'])
            fields_list = row_fields.split(',')
            issues_list = row_issues.split(',')
            add_to_list, fields, issues = self._check_data(
                item_id=row['item_id'],
                fields_list=fields_list,
                issues_list=issues_list)

            if add_to_list:

                tech_issues_dict = dict(
                    premium=row['premium'],
                    company=row['company'],
                    fields=fields,
                    website=row['website'],
                    item_id=row['item_id'],
                    issues=issues
                )
                tech_issues.append(tech_issues_dict)

        return tech_issues
