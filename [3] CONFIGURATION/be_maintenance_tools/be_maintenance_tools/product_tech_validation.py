import optparse
import os
import sys

from dotenv import load_dotenv
from clients.ms_db import MsDatabaseParams, MSDatabaseConnection
from accumulator.daily.product import ProductDailyIssuesAccumulator
from accumulator.premium.product import ProductPremiumIssuesAccumulator
from checker.product import ProductIssueChecker
from writer.product import ProductIssuesWriter
from helper.clients_prem_sched_getter import ClientsPremiumScheduleGetter

load_dotenv()

if __name__ == '__main__':
    # Check configurations
    env_keys = [
        'MSSQL_DRIVER',
        'MSSQL_SERVER',
        'MSSQL_USER',
        'MSSQL_PWD',
        'RM_DB'
    ]
    param_keys = [
        'mssql_driver',
        'mssql_server',
        'mssql_user',
        'mssql_pwd',
        'rm_db'
    ]

    for key in env_keys:
        if not os.getenv(key):
            print('Missing configuration: {}. Aborted.'.format(key))
            sys.exit()

    env_vars = dict(zip(param_keys, map(os.getenv, env_keys)))

    # Set target date
    parser = optparse.OptionParser()
    parser.add_option(
        "-d",
        "--date",
        action="store",
        help="target_date, yyyy-MM-dd format. Required."
    )
    options, args = parser.parse_args()
    
    if options.date:
        target_date = options.date
    else:
        print("Missing target date. Aborted.")
        sys.exit()

    print('\n[x] Fetch technical issues')
    db_params = MsDatabaseParams(env_vars['mssql_driver'],
                                 env_vars['mssql_server'],
                                 env_vars['rm_db'],
                                 env_vars['mssql_user'],
                                 env_vars['mssql_pwd']
    )

    try:
        product_issues_list = list()
        prod_daily_issues_list = list()
        prod_premium_issues_list = list()
        db_connection = MSDatabaseConnection(db_params=db_params)

        # Fetch daily tech issues
        prod_daily_issues_accumulator = ProductDailyIssuesAccumulator(
            db_connection=db_connection,
            target_date=target_date)
        prod_daily_issues_list = prod_daily_issues_accumulator.run()

        product_issues_list.extend(prod_daily_issues_list)

        # Fetch premium tech issues
        clients_getter = ClientsPremiumScheduleGetter(target_date=target_date)
        clients = clients_getter.run()

        prod_prem_issues_accumulator = ProductPremiumIssuesAccumulator(
            db_connection=db_connection,
            target_date=target_date,
            clients=clients
        )
        prod_premium_issues_list = prod_prem_issues_accumulator.run()

        product_issues_list.extend(prod_premium_issues_list)

        if product_issues_list:
            # Check if tech issues are valid or not
            checker = ProductIssueChecker(data_list=product_issues_list)
            checked_prod_issues_list = checker.run()

            # Write tech issues to file
            file_writer = ProductIssuesWriter(
                data_list=checked_prod_issues_list,
                target_date=target_date)
            file_writer.write()
            print('[x] Created technical issues file')
        else:
            print('[x] No technical issues')

        db_connection.disconnect()

    except Exception as e:
        print(f'[x] Exception encountered: {str(e)}')
