from constants import RMProductFields, SQLProductFields

def rename_product_fields(fields: str):
    #fields = fields.replace(RMProductFields.TITLE.value,
    #                        SQLProductFields.TITLE.value)
    #fields = fields.replace(RMProductFields.AVAILABILITY.value,
    #                        SQLProductFields.AVAILABILITY.value)
    #fields = fields.replace(RMProductFields.PRICE.value,
    #                        SQLProductFields.PRICE.value)
    #fields = fields.replace(RMProductFields.RATING_REVIEWS.value,
    #                        SQLProductFields.RATING_REVIEWS.value)
    #fields = fields.replace(RMProductFields.RATING_SCORE.value,
    #                        SQLProductFields.RATING_SCORE.value)
    for key in RMProductFields:
        fields = fields.replace(key.value, SQLProductFields[key.name].value)

    return fields
