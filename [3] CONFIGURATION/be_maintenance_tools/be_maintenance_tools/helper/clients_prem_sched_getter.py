import datetime
from constants import CLIENT_PREMIUM_SCHEDULE

class ClientsPremiumScheduleGetter(object):
    def __init__(self, target_date):
        self.target_date = target_date

    def _get_weekday(self):
        year, month, day = (int(d) for d in self.target_date.split('-'))
        return datetime.datetime(year, month, day).strftime('%w')

    def run(self):
        clients_list = CLIENT_PREMIUM_SCHEDULE[self._get_weekday()]['clients']
        clients = str(clients_list)[1:-1]

        return clients
