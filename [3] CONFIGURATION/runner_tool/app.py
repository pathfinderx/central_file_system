import datetime
import time
from classes.rerun_comparison import RerunTypeComparison
from classes.rerun_webshots import RerunTypeWebshots
from pprint import pprint

if __name__ == "__main__":
	start_time = time.time()
	try:
		cc = input("Rerun type: ")

		if "rerun_webshots" in cc:
			try:
				temp = cc.split(" ")
				rtype = RerunTypeWebshots()

				num_items = rtype.execute_run(temp)
				pprint(time.time() - start_time)
				pprint('Total of items generated: {}'.format(num_items))
			except:
				pprint("rerun type not found. . . ")
		
		if "rerun_comparison" in cc:
			try:
				temp = cc.split(" ")
				rtype = RerunTypeComparison()
				num_items = rtype.execute_run(temp)
				pprint(round(time.time() - start_time), 2)
				pprint('Total of items generated: {}'.format(num_items))
			except:
				pprint("rerun type not found. . . ")
		
		if "rerun_listings" in cc:
			print("later")

		if "-h" in cc:
			pprint("Rerun Webshots Format: rerun_webshots for type | -c for company | -s for source | -d for date ex. rerun_webshots -s etl -d 2021-01-17\n")
			pprint("Rerun Comparison Format: rerun_comparison for type | -s for status | -d for date ex. rerun_comparison -s crawl_failed -d 2021-01-17\n")
	except:
		pprint("Rerun type doesn't exist. You may use '-h' for help")
			
	