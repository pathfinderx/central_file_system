class TriggerTime:
    def __init__(self):
        pass
    
    def get_trigger_time(self, pid):
        country = pid[3:5]
        trigger_time = None

        # country trigger time global
        cntry_02 = ['BR']
        cntry_04 = ['US', 'CA']
        cntry_14 = ['AU']
        cntry_15 = ['JP']
        cntry_16 = ['CN', 'SG', 'KH']
        cntry_17 = ['VN']
        cntry_20 = ['AE', 'KW']
        cntry_21 = ['FI', 'RU', 'GR']
        cntry_22 = ['FR', 'DE', 'SE', 'ES', 'NL', 'NO', 'DK', 'IT', 'BE', 'CH', 'CZ', 'AT', 'PL']
        cntry_23 = ['GB']
        
        if country in cntry_02:
            trigger_time = '02:00:00'
        elif country in cntry_04:
            trigger_time = '04:00:00'
        elif country in cntry_14:
            trigger_time = '14:00:00'
        elif country in cntry_15:
            trigger_time = '15:00:00'
        elif country in cntry_16:
            trigger_time = '16:00:00'
        elif country in cntry_17:
            trigger_time = '17:00:00'
        elif country in cntry_22:
            trigger_time = '22:00:00'
        elif country in cntry_21:
            trigger_time = '21:00:00'
        elif country in cntry_23:
            trigger_time = '23:00:00'
        
        return trigger_time