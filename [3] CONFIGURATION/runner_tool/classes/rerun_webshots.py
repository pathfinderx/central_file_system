from classes.doc_writer import DocumentWriter
from classes.trigger_time import TriggerTime

class RerunTypeWebshots:
    def __init__(self):
        pass

    def execute_run(self, data):
        trigger_time = None
        ttime = TriggerTime()
        dwriter = DocumentWriter()
        items = dwriter.get_input_data()
        rcompany = data[2]
        rstatus = data[4]
        rdate = data[-1]
        item_holder = list()

        if len(items):
            for pid in items:
                trigger_time = ttime.get_trigger_time(pid)
                item = '{}@{}@{}'.format(rdate, trigger_time, pid)
                item_holder.append(item)

            merged_items = ','.join(item_holder)
            
            if rstatus == 'etl':
                cmd = 'python manual_cmp_webshots.py -c {} -i {}'.format(rcompany, merged_items)
                print(cmd)
                is_written = dwriter.write_command(cmd)
                
                return len(items) if is_written else 0
                
            else:
                cmd = 'python manual_cmp_webshots_override.py -c {} -i {}'.format(rcompany, merged_items)
                print(cmd)
                is_written = dwriter.write_command(cmd)

                return len(items) if is_written else 0
        else:
            return 0
               