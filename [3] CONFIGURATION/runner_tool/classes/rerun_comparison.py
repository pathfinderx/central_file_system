from pprint import pprint
from classes.doc_writer import DocumentWriter
from classes.trigger_time import TriggerTime

class RerunTypeComparison:
    def __init__(self):
        pass

    def execute_run(self, data):
        trigger_time = None
        ttime = TriggerTime()
        dwriter = DocumentWriter()
        items = dwriter.get_input_data()
        rstatus = data[2]
        rdate = data[-1]
        item_holder = list()

        if len(items):
            for pid in items:
                trigger_time = ttime.get_trigger_time(pid)
                item = '{}@{}@{}'.format(rdate, trigger_time, pid)
                item_holder.append(item)

            merged_items = ','.join(item_holder)
            cmd = 'python rerun_cmp.py -s {} -l {}'.format(rstatus.upper(), merged_items)
            pprint(cmd)
            is_written = dwriter.write_command(cmd)

            if is_written:
                return len(items)
            else:
                return 0