import json
from pprint import pprint
from transformers import Transformer
from post_transformers import PostTransformer
from builder import ETPayloadBuilder
from validators import RunningmanETDataValidator
def transform(retailer, country_code, company, data):
    transformer = Transformer(retailer, country_code=country_code, company_code=company)
    post_transformer = PostTransformer(retailer, country_code=country_code, company_code=company)
    payload_builder = ETPayloadBuilder(transformer, post_transformer)
    # raises an exception
    transformed_data = payload_builder.build(data)
    return transformed_data
def test_builder(website, country_code):
    transformer = Transformer(website, country_code=country_code)
    payload_builder = ETPayloadBuilder(transformer, PostTransformer)
    with open('sample.json', encoding="utf-8") as sample_json:
        scrape_data = json.load(sample_json)
    pprint(payload_builder.build(scrape_data))
if __name__ == '__main__':
    country_code = 'dk'
    retailer = 'www.fribikeshop.dk'
    company = '100'
    # test_transform(website, country_code)
    # test_builder(website, country_code)
    with open('et_test.json', encoding="utf-8") as f:
        data = json.load(f)
        transformed_data = transform(retailer, country_code, company, data)
        # print('Availability: ', str(transformed_data['availability']))
        pprint(transformed_data)
        rm_et_data_validator = RunningmanETDataValidator(transformed_data)
        warning_fields_mapping = rm_et_data_validator.get_warning_fields_mapping()
        print(warning_fields_mapping)