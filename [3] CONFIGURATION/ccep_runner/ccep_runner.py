from classes.couchclient import ListingCDBClient
from classes.doc_writer import DocumentWriter
from classes.doc_deleter import Documentdeleter
import datetime

def create_rerun_script(pid):
    db = ListingCDBClient()
    x = pid.split('\t')
    postal_code = x[0]
    url = x[1]
    dt = datetime.datetime.strptime(x[2], "%m/%d/%Y")
    dt = dt.strftime('%Y-%m-%d')
    count = x[3]

    selector = {
        "_id": {
            "$gt": dt+"@"
        },
        "parameters": {
            "url": url,
            "postal_code": postal_code
        }
    }

    documents = db.get_documents(selector=selector)
    copied_docs = [i for i in documents]
    txt_script = ''
    xlsx_script = ''
    o = 1
    for i in copied_docs:
        try:
            if o == 1:
                if i['status'] == 'DONE':
                    #print(str(url)+'-'+str(postal_code))
                    #print(str(i['parameters']['url'])+'-'+str(i['parameters']['postal_code']))
                    #print(str(i['products_count']) +'---'+ str(count))
                    if int(i['products_count']) !=  int(count):
                        luuid = i['parameters']['listing']['uuid']
                        ruuid = i['parameters']['website']['uuid']
                        txt_script = "python enqueue.py -d {} -c 700 -r {} -l {}\n".format(dt, ruuid, luuid)
                        xlsx_script = "{}\t{}\t{}\t{}\t{}\n".format(postal_code, url, x[2], int(count), int(i['products_count']))
            o = o + 1
        except KeyError as e:
            print('{} is {}'.format(i['job_id'], i['status']))
    
    with open('outputs/rerun_script.text', 'a') as f:
        f.write(txt_script)
    
    with open('outputs/checker.text', 'a') as f:
        f.write(xlsx_script)


if __name__ == '__main__':

    deleter = Documentdeleter()
    deleter.delete_data()
    dwriter = DocumentWriter()
    items = dwriter.get_input_data()
    print('KALMA LANG ATLIS HINDE MANUAL')
    print('STARTING NOW!')
    i = 1
    if len(items):
        for pid in items:
            print(i)
            create_rerun_script(pid)
            i+=1
    print('DONE FINISHED!')
    