from classes.couchclient import ListingCDBClient
import json

def create_rerun_script():
   db = ListingCDBClient()
   selector = {
      "_id": {
         "$gte": "2022-12-20@",
         "$lte": "2022-12-21@香"
      },
      "parameters.website.url": "www.alcampo.es"
   }
   documents = db.get_documents(selector=selector)
   
   open("rerun_command.txt", "w").close()

   copied_docs = [i for i in documents]

   print(len(copied_docs))

    #Comment 1 Loop for better result

   #POSTAL CODE with Different Urls 
   postal_code = {}
   for i in copied_docs:
       try:
           key = i['parameters']['postal_code']
           if key in postal_code:
               postal_code[key].append(i['parameters']['url'])
           else:
               postal_code[key] = [i['parameters']['url']]
       except KeyError as e:
           print('{} is {}'.format(i['job_id'], i['status']))
   with open('postal_with_url.json', 'w+') as f:
       json.dump(postal_code, f, indent=4)

   #PRODUCT_COUNT with Different job_ids
   product_count = {}
   for i in copied_docs:
      try:
         key = i['products_count']
         if key in product_count:
            product_count[key].append(i['job_id'])
         else:
            product_count[key] = [i['job_id']]
      except KeyError as e:
         print('{} is {}'.format(i['job_id'], i['status']))
         
   with open('data_count.json', 'w+') as f:
      json.dump(product_count, f, indent=4)

    #Comment 1 loop for better result
    #Rerun Command.txt output

    #For postal_with_url.json data
   for i in copied_docs:
      if 'parameters' in i.keys() and 'postal_code' in i['parameters']:
          d, c, r, l = i['job_id'].split('@')
          rerun_command= 'python enqueue.py -d {} -c {} -r {} -l {}\n'.format(d, c, r, l)


          with open('rerun_command.txt', 'a+') as f:
              f.write(rerun_command)

   print('Done!')

    #For data_count.json data
   for i in copied_docs:
       if 'postal_code' in i.keys():
           if i['products_count'] == 199:
               d, c, r, l = i['job_id'].split('@')
               rerun_commandForKey1 = 'python enqueue.py -d {} -c {} -r {} -l {}\n'.format(d, c, r, l)
               
               with open('rerun_command.txt', 'a+') as f:
                   f.write(rerun_commandKey1)

   print('Done!')


if __name__ == '__main__':
    create_rerun_script()

'408365',
'408651',
'408640',
'408354',
'408607',
'408310',
'408541',
'408728',
'408342',
'408343',
'408346',
'408925',
'408926',
'408771',
'408775',
'408299',
'408303',
'408302',
'408754',
'408738',
'408915',
'408919',
'407984',
'408144',
'408133',
'407968',
'408411',
'408409',
'408408',
'408246',
    