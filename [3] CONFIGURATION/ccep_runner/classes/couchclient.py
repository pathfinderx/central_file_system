from cloudant import CouchDB
from cloudant.query import Query
from cloudant.database import CouchDatabase

class CouchDBClient(object):
    def __init__(self, host, port, user, pwd, db_name):
        db_host = 'http://{host}:{port}'.format(host=host, port=port)
        self.server = CouchDB(user, pwd, url=db_host, connect=True)
        self.db = self.server[db_name]
        self.db_name = db_name
        
class DataCouchDBClient(CouchDBClient):
    def create_or_update_document(self, data):
        try:
            db = CouchDatabase(self.server, self.db_name, fetch_limit=10000, partitioned=False)
            db.bulk_docs(data)
        except Exception as e:
            print("Something wen't wrong. {}".format(e))
        return None
    def get_documents(self, selector):
        try:
            query = Query(self.db, selector=selector)
            return query(limit=9999)['docs']           
        except Exception as e:
            print("Something wen't wrong. {}".format(e))
        return None        

class ListingCDBClient(DataCouchDBClient):
    def __init__(self):
        self.user = 'admin'
        self.pwd = 'wT2v4wDe79e9dTP5'
        self.host = 'http://172.100.70.4:5984'
        self.db_name = 'd2ms-director-listing'
        self.server = CouchDB(self.user, self.pwd, url=self.host, connect=True)
        self.db = self.server[self.db_name]


    def create_or_update_document(self, data):

        try:
            db = CouchDatabase(self.server, self.db_name, fetch_limit=9999, partitioned=False)
            # db.bulk_docs(data)

        except Exception as e:
            print("Something wen't wrong. {}".format(e))

        return None

    def get_documents(self, **kwargs):
        try:
            query = Query(self.db, **kwargs)
            return query(limit=9999)['docs']
            

        except Exception as e:
            print("Something wen't wrong. {}".format(e))

        return None 

class RankingCDBClient(DataCouchDBClient):
    def __init__(self):
        self.user = 'admin'
        self.pwd = 'wT2v4wDe79e9dTP5'
        self.host = 'http://172.100.70.4:5984'
        self.db_name = 'd2ms-director-rankings'
        self.server = CouchDB(self.user, self.pwd, url=self.host, connect=True)
        self.db = self.server[self.db_name]


    def create_or_update_document(self, data):

        try:
            db = CouchDatabase(self.server, self.db_name, fetch_limit=9999, partitioned=False)
            # db.bulk_docs(data)

        except Exception as e:
            print("Something wen't wrong. {}".format(e))

        return None

    def get_documents(self, **kwargs):
        try:
            query = Query(self.db, **kwargs)
            return query(limit=9999)['docs']
            

        except Exception as e:
            print("Something wen't wrong. {}".format(e))

        return None 

