import os

class Documentdeleter:
    def __init__(self):
        pass

    def delete_data(self):
        
        if os.path.exists("outputs/rerun_script.text"):
            os.remove("outputs/rerun_script.text")

        if os.path.exists("outputs/checker.text"):
            os.remove("outputs/checker.text")

    def delete_rankingdata(self):

        if os.path.exists("outputs_ranking/rerun_script.text"):
            os.remove("outputs_ranking/rerun_script.text")

        if os.path.exists("outputs_ranking/checker.text"):
            os.remove("outputs_ranking/checker.text")
