import os

class DocumentWriter:
    def __init__(self):
        pass

    def get_input_data(self):
        
        # open file
        data = None
        with open('./docs/items.txt') as f:
            data = [items.rstrip() for items in f]
            
        return data

    def get_input_rankingdata(self):

        # open file
        data = None
        with open('./docs/items_rankings.txt') as f:
            data = [items.rstrip() for items in f]
            
        return data