from classes.couchclient import RankingCDBClient
from classes.doc_writer import DocumentWriter
from classes.doc_deleter import Documentdeleter
import datetime



def create_rerun_script(pid):
    db = RankingCDBClient()
    x = pid.split('\t')
    postal_code = x[0]
    website = x[1]
    keyword = x[2]
    dt = datetime.datetime.strptime(x[3], "%m/%d/%Y")
    dt = dt.strftime('%Y-%m-%d')
    
    selector = {
      "_id": {
         "$gt": dt+"@"
      },
      "parameters": {
        "website": {
            "text": website
         },
         "d2ms-v3": {
            "keyword": keyword,
            "params": {
               "postal_code": postal_code
            }
         }
      }
    }

    documents = db.get_documents(selector=selector)
    copied_docs = [i for i in documents]
    txt_script = ''
    xlsx_script = ''
    o = 1
    for i in copied_docs:
        try:
            if o == 1:
                if i['status'] == 'FAILED':
                    print('create')
                    
                o = o + 1
                    #print(str(url)+'-'+str(postal_code))
                    #print(str(i['parameters']['url'])+'-'+str(i['parameters']['postal_code']))
                    #print(str(i['products_count']) +'---'+ str(count))
        except KeyError as e:
            print('{} is {}'.format(i['job_id'], i['status']))
    
    with open('outputs_ranking/rerun_script.text', 'a') as f:
        f.write(txt_script)
    
    with open('outputs_ranking/checker.text', 'a') as f:
        f.write(xlsx_script)


if __name__ == '__main__':

    deleter = Documentdeleter()
    deleter.delete_rankingdata
    dwriter = DocumentWriter()
    items = dwriter.get_input_rankingdata()
    print('RANKINGS --- KALMA LANG ATLIS HINDE MANUAL')
    print('STARTING NOW!')
    i = 1
    if len(items):
        for pid in items:
            print(i)
            create_rerun_script(pid)
            i+=1
    print('DONE FINISHED!')
    