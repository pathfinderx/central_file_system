class TriggerTime:
    def get_trigger_time(self, data):
        country = data[3:5]

        if country in  ['BR']:
           return '02:00:00'
        elif country in ['US', 'CA']:
           return '04:00:00'
        elif country in ['MX']:
           return '05:00:00'
        elif country in ['AU']:
           return '14:00:00'
        elif country in ['JP']:
           return '15:00:00'
        elif country in ['CN', 'SG', 'KH']:
           return '16:00:00'
        elif country in  ['VN']:
           return '17:00:00'
        elif country in ['AE', 'KW']:
           return '20:00:00'
        elif country in ['FI', 'RU', 'GR', 'SA' 'RO']:
           return '21:00:00'
        elif country in  ['FR', 'DE', 'SE', 'ES', 'NL', 'NO', 'DK', 'IT', 'BE', 'CH', 'CZ', 'AT', 'PL', 'RS', 'LU', 'SK']: # RS (serbia) added:
           return '22:00:00'
        elif country in  ['GB', 'UK', 'PT']:
           return '23:00:00'
        else:
            return 'None'