import codecs, json, io

class DocumentWriter:
    def __init__(self):
        pass

    def get_item_ids(self, metrics='comparisons'):
        data = None

        if (metrics == 'comparisons'):
            with open('./docs/item_ids.txt') as item_ids:
                data = [x.rstrip() for x in item_ids]

        elif (metrics == 'rankings'):
            file = codecs.open("./docs/item_ids.txt", "r", "utf-8")
            data = [x.rstrip() for x in file]
                
        with open('./docs/api_payload.txt', 'r+') as item_ids:
            item_ids.truncate(0)
            item_ids.close()
        return data

    def write_command(self, data, status='single', metrics='comparisons'):
        try:
            if (status == 'single' and metrics == 'comparisons'):
                with io.open('./docs/api_payload.txt', 'w') as item_ids:
                    item_ids.write(data)
                    item_ids.close()
            elif (status == 'single' and metrics == 'rankings'):
                with io.open('./docs/api_payload.txt', 'w', encoding='utf8') as item_ids:
                    item_ids.write(json.dumps(data, ensure_ascii=False, indent=4))
                    item_ids.close()

            # with open('./docs/item_ids.txt', 'r+') as item_ids:
            #     item_ids.truncate(0)
            #     item_ids.close()

            elif (status == 'multiple'):
                with open('./docs/api_payload.txt', 'a') as item_ids:
                    item_ids.write(data + '\n\n')
                    item_ids.close()
                # with open('./docs/item_ids.txt', 'r+') as item_ids:
                #     item_ids.truncate(0)
                #     item_ids.close()

            return True
        except:
            return False
