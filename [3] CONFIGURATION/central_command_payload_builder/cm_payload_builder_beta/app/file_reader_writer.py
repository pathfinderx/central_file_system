import codecs, json, io

class FileReaderWriter:
    def __init__(self):
        pass

    def get_items(self, metrics='default'):
        data = None

        if (metrics == 'default'):
            with open('./file/item_identifier.txt') as items:
                data = [x.rstrip() for x in items]

        elif (metrics == 'rankings'):
            file = codecs.open("./file/item_identifier.txt", "r", "utf-8")
            data = [x.rstrip() for x in file]
                
        with open('./file/api_payload.txt', 'r+') as items:
            items.truncate(0)
            items.close()
        return data

    def write_command(self, data, status='single', metrics='default'):
        try:
            if (status == 'single' and metrics == 'default'):
                with io.open('./file/api_payload.txt', 'w') as items:
                    items.write(data)
                    items.close()
            elif (status == 'single' and metrics == 'rankings'):
                with io.open('./file/api_payload.txt', 'w', encoding='utf8') as items:
                    items.write(json.dumps(data, ensure_ascii=False, indent=4))
                    items.close()

            elif (status == 'multiple'):
                with open('./file/api_payload.txt', 'a') as items:
                    items.write(data + '\n\n')
                    items.close()

            with open('./file/item_identifier.txt', 'r+') as items:
                items.truncate(0)
                items.close()

        except Exception as e:
            assert(e)
