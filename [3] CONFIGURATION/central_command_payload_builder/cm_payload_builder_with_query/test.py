import datetime, time
from app.rerun_comparison import RerunTypeComparison
from app.rerun_ranking import RerunTypeRanking
from app.rerun_listing import RerunTypeListing
from app.rerun_webshots import RerunTypeWebshots
from pprint import pprint

if __name__ == "__main__":
	breaker = False

	while (not breaker):
		try:
			choice = input("\nSELECT CHOICE: \n[1] Build payload \n[2] Exit\n\r")
			if choice == '1':
				cc = input("\Type CHOICE: \n[1] rerun_cmp | cmp \n[2] rerun_rnk | rnk <date>@<time>@<db> \n[3] rerun_lst | lst <date>@<time>@<db> \n[4] rerun_webshots || ws -c <company> -s <source>\n\r")
			
				if (any(x in cc for x in ['rerun_cmp', 'cmp'])):
					try:
						RerunTypeComparison().execute()
					except Exception as e:
						print(e)
				elif (any(x in cc for x in ['rerun_rnk', 'rnk'])):
					try:
						RerunTypeRanking().execute(cc)
					except Exception as e:
						print(e)
				elif (any(x in cc for x in ['rerun_lst', 'lst'])):
					try:
						RerunTypeListing().execute(cc)
					except Exception as e:
						print(e)
				elif (any(x in cc for x in ['rerun_webshot', 'ws'])):
					try:
						RerunTypeWebshots().execute_run(cc)
					except Exception as e:
						print(e)
				else:
					print('Invalid rerun type: select from rerun_cmp, rerun_rnk, rerun_lst')

			elif choice == '2':
				pprint("Goodbye")
				breaker = True
			else:
				print("\nInvalid input format! please select from the following:")
		except:
			pprint("Rerun type doesn't exist. You may use '-h' for help")