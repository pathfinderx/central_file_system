from app.file_reader_writer import FileReaderWriter
from app.trigger_time import TriggerTime
from datetime import datetime, date
import json, re

class RerunTypeListing:
    def __init__(self):
        self.version = '1.0'
        self.user_uuid = '1Q2W3E4R5T6Y7U8I9O0P'

    def execute(self, command):
        items = FileReaderWriter().get_items()
        assert (re.search(r'([\d]{4}-[\d]{2}-[\d]{2})@([\d]{2}:|[\d]{2})+@([\d]{3}|[\w\d]+)', command)), "Invalid pre document id for rankings"

        company = command.split()[-1].split('@')[-1]
        trigger_time = command.split()[-1].split('@')[-2]

        _json, result, payload_generated_count = self.construct_payload(company, trigger_time, items), None, 0
        result = FileReaderWriter().write_command(_json, metrics='listings')
        payload_generated_count+=1
        print(f'Payload Generated: {payload_generated_count}')

    def construct_payload(self, company, trigger_time, items):
        payload = {
            "version": self.version,
            "request": {
                "task_name": f'Listings-Rerun-{datetime.now().strftime("%Y-%m-%d %H:%M")}',
                "user_uuid": self.user_uuid,
                "data": {
                    "company": company,
                    "run_mode": "rerun",
                    "schedule_time": trigger_time,
                    "schedule_date": date.today().strftime('%Y-%m-%d'),
                    "retailers": [
                    ],
                    "data_points": [x for x in items]
                }
            }
        }
        _json = json.dumps(payload, indent=4)
        return _json