from app.file_reader_writer import FileReaderWriter
from app.trigger_time import TriggerTime
from datetime import datetime, date

class RerunTypeWebshots:
    def __init__(self):
        pass

    def execute_run(self, data):
        trigger_time = None
        ttime = TriggerTime()
        dwriter = FileReaderWriter()
        
        items = dwriter.get_items() # no arguments = metrics='default'
        split_data = data.split()
        rcompany = split_data[2]
        rstatus = split_data[4]
        rdate = datetime.now().strftime("%Y-%m-%d") # date today generator so we will not be confused on writing dates
        item_holder = list()

        if len(items):
            for pid in items:
                trigger_time = ttime.get_trigger_time(pid)
                item = f'{rdate}@{trigger_time}@{pid}'
                item_holder.append(item)

            merged_items = ','.join(item_holder)
            
            if rstatus == 'etl':
                cmd = f'python manual_cmp_webshots.py -c {rcompany} -i {merged_items}'
                print(cmd)
                is_written = dwriter.write_command(cmd, metrics='webshots')
                
                return len(items) if is_written else 0
                
            else: # specify/set (rstatus in ['failed']) guard clause
                cmd = 'python manual_cmp_webshots_override.py -c {} -i {}'.format(rcompany, merged_items)
                print(cmd)
                is_written = dwriter.write_command(cmd, metrics='webshots')

                return len(items) if is_written else 0
        else:
            return 0
               