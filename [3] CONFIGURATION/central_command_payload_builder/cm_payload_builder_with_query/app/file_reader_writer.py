import codecs, json, io

class FileReaderWriter:
    def __init__(self):
        pass

    def get_items(self, metrics='default'):
        data = None

        if (metrics == 'default'):
            with open('./file/item_identifier.txt') as items:
                data = [x.rstrip() for x in items]

        elif (metrics == 'rankings'): # ranking is special: keywords for national language
            file = codecs.open("./file/item_identifier.txt", "r", "utf-8") # use codecs to correctly extract national language
            data = [x.rstrip() for x in file]
                
        with open('./file/api_payload.txt', 'r+') as items:
            items.truncate(0)
            items.close()

        return data

    def write_command(self, data, status='single', metrics='default'):
        try:
            if (status == 'single' and metrics == 'default'):
                with io.open('./file/api_payload.txt', 'w') as items:
                    items.write(data[0])
                    items.close()

                with io.open('./file/sql_query.txt', 'w') as items:
                    items.write(data[1])
                    items.close()

            elif (status == 'single' and metrics == 'rankings'):
                with io.open('./file/api_payload.txt', 'w', encoding='utf8') as items:
                    items.write(json.dumps(data, ensure_ascii=False, indent=4))
                    items.close()

                self.clear_sql_query_txt()

            elif (status == 'single' and metrics == 'listings'):
                with io.open('./file/api_payload.txt', 'w') as items:
                    items.write(data)
                    items.close()

                self.clear_sql_query_txt()

            elif (status == 'multiple'):
                with open('./file/api_payload.txt', 'a') as items:
                    items.write(data[0] + '\n\n')
                    items.close()

                with io.open('./file/sql_query.txt', 'a') as items:
                    items.write(data[1] + '\n\n')
                    items.close()

            elif (metrics in ['webshots']):
                with open('./file/api_payload.txt', 'w') as items:
                    items.write(data)
                    items.close()

                self.clear_sql_query_txt()

            self.clear_item_identifier_txt()

        except Exception as e:
            assert(e), e

    def clear_item_identifier_txt(self):
        with open('./file/item_identifier.txt', 'r+') as items:
                items.truncate(0)
                items.close()

    def clear_sql_query_txt(self):
        with open('./file/sql_query.txt', 'r+') as items:
            items.truncate(0)
            items.close()

