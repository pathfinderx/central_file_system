from this import d
from app.file_reader_writer import FileReaderWriter
from app.trigger_time import TriggerTime
from datetime import datetime, date
import json

class RerunTypeComparison:
    def __init__(self):
        self.version = '1.0'
        self.user_uuid = '1Q2W3E4R5T6Y7U8I9O0P'

    def execute(self):
        item_ids = FileReaderWriter().get_items()
        com_sched_id, group_container, payload = None, dict(), dict()

        assert (len(item_ids) > 0), "No item_ids extracted. Check the item_ids.txt; it might be empty"

        # item_id grouping using comparison schedule date identifier e.g. '200FR'
        for k,v in enumerate(item_ids):
            if not com_sched_id:  # first index
                com_sched_id = v[:5]
                group_container.update({com_sched_id:[v]})
            else:
                if (v[:5] == com_sched_id):
                    com_sched_id = v[:5]
                    group_container[com_sched_id].append(v)
                else:
                    if (v[:5] in group_container.keys()):  # if com_sched_id is existing in dict -> regroup
                        com_sched_id = v[:5]
                        group_container[com_sched_id].append(v)
                    else:  # create new group
                        com_sched_id = v[:5]
                        group_container.update({com_sched_id:[v]})

        if group_container:
            payload_generated_count = 0

            FileReaderWriter().clear_sql_query_txt() # clear sql_query.txt first

            for k,v in group_container.items():
                _json, result = self.construct_payload(k, v), None
                result = FileReaderWriter().write_command(_json, status='multiple') if len(group_container) > 1 else FileReaderWriter().write_command(_json, status='single')
                payload_generated_count+=1
            print(f'Payload Generated: {payload_generated_count}')
                
    def construct_payload(self, key, value):
        trigger_time = TriggerTime().get_trigger_time(key)
        date_today, date_time = datetime.now().strftime("%Y-%m-%d"), datetime.now().strftime("%Y-%m-%d %H:%M")

        # payload builder
        payload = {
            "version": self.version,
            "request": {
                "task_name": f'Comparisons-Rerun-{date_time}',
                "user_uuid": self.user_uuid,
                "data": {
                    "company": key[:3],
                    "run_mode": "rerun",
                    "schedule_time": trigger_time,
                    "schedule_date": date.today().strftime('%Y-%m-%d'),
                    "retailers": [
                    ],
                    "data_points": value
                }
            }
        }
        
        # sql query builder
        query_string = f"SELECT * FROM view_all_productdata where date='{date_today}' AND item_id in ({[x for x in value]}) ORDER BY source".replace('[', '').replace(']', '')

        _json = json.dumps(payload, indent=4)
        return _json, query_string