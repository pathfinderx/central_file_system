arrayNum = [23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48]

question:
	* assuming you have a number starting from 23, if you count it till 48 starting 23, it would be equal to 26
	* but 48 - 23 = 25? it should be equal to 26

	* prove that it should not be 25
		* 23 starting number + 25 the remaining numbers to add because 23 is included in the count
			* so 23 + 25 = 48
				* 25 = 48 - 23
				* 

			* [23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48] = 26 numbers in total
			* [23] + [25 (from 24 to 48)] = 48
			* 