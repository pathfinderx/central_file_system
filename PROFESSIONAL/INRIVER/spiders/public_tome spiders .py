@Scheduling
	# BLITZKRIEG
		# 0515 - 0630 - prayer, quicksort, mini coffee
		# 0630 - 0700 - warmup
		# 0700 - 0730 - floor, silhig outside
		# 0730 - 0800 - bfast, rest
		# 0800 - 0900 - technical training
		# 0900 - 0930 - power nap
		# 0930 - 1200 - WORK (2hr30min)

		# 1200 - 1230 - lunch
		# 1230 - 1400 - WORK (2hr30mins)
		# 1400 - 1430 - power nap
		# 1430 - 1800 - WORK (3hr30mins)
		# 1900 - 1930 - technical training

@Organizational
	# GIT
		# Sourcetree
			# jestony yap
			# request code access
			# clone repository after given acces to code

		# Puttygen
			# create ssh (secure shell?)
				# care after use
			# rsa - generate

		# contact person
			# Jestoni Yap - request OpenVPN and code access (access to source tree)
			# Ravilo Olivar - VM access (open vpn)

	# PYTHONt
		# Python beautifulsoup
			# scraping module for python.
		# virtual env
	# JS
		# Puppeteer
			# scarping module 
		# handled by sir Darwin

	# ORGANIZATIONAL
		# teams
			# frontend (client side system)
				# dan
			# backend (spiders) (et(extractance form) - convert values scrape to understandable value to convert to sql)
				# BE team
				# cms - mostly seniors
			# db - sql (then checked to validators) - mostly seniors
			# analytics
			# QA

		# knowledge tranfer (KT) for 1 month
			# web scaping
				# depending on new retailer
					# if new then create new
				# existing
					# maintain

@Comparison spiders procedures basic run
	# clone (please research more on this)
		# pull? (sugggested to always pull by sir jave)
			# updates the repository directory
			# suggested to always pull the latest request
		# to check checkout origin/develop (on left panel)
			# REMOTES origin>develop>+rightclick press checkout
				# then ok
	# .env (FYO, purchased and should be deleted every 5mins in channel)
	# test.py - should run here

	# two proxies
		# residential
			# reset router
		# unblocker
			# isog nga proxy
			# amazon, gusog mang block

	# run
		# breakpoint
			# 
			# if blacklisted
				# reset router

			# to test on breakpoint
				# go 

		# step into
			# f11, go to class check
			# f10 to factory or next line

@Comparison spider flow analysis
	@Personal Understanding
		# scripts
			# I can only explain the vague details with a little bit of technicalities
				# because our understanding is different if we are the ones who create it
			# but to our reference test.py
				# there are two vague execution that takes place here: 
					# we set or establish connection through LuminatiSessionRequest

		# 2 main executions 
			# LUMINATI SESSION REQUEST - requester for session
				# import dotenv - env configuration 
				# request > luminati.py
					# inherits from .base import SessionRequests
						# class LuminatiSessionRequests(SessionRequests): 

					# abstract base class (ABCMeta)
						# https://www.youtube.com/watch?v=UDmJGvM-OUw
						# abstract method - has declaration but no implementation
						# abstract class - class that has abtract method
						# purpose: allows us to create methods in a child class from the abstract class
							# or you declare or extend the capabilities of class but specified within the child class

				# dependency list
					# dotdenv - .env config reader or reads key-value pairs 
						# load_dotenv()  # take environment variables from .env.

				# LuminatiSessionRequests
					# establishes connection with the proxy api
					# underscore in python: to note that the method is for internal use
						# _check_parameter? read more on this
					# _init_
						# assert - detect problems early
						# isinstance(object, type)  - returns True if  specified object is specified type, otherwise False.
					# base.py > SessionRequests - adapter or session request
						# purpose : creating request session
					
					# http status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
						# informational responses: 100-199
						# successful responses: 200-299
						# redirection messages: 300-399
						# client error responses: 400-499
						# server error responses: 500-599
					# OSI (Open system interconnect) layers:
						# application
						# presentation
						# session - manages the connection between two communicating endpoints
						# transport
						# network
						# data link
						# physical

			# STRETEGY 
				# it basically mapping the strategy and extraction methods using soup are specified
				# strategy are the executables on what are properties and attributes that needs to be extracted

			# __init__ > factory.py
				# mappings: dl_map, ws_map
				# loads out the mapping first
				# purpose of mapping: identifies what website declared and if it is found in ws_map.json

				# ws_strategy = factory.get_website_strategy(website, country, company_code)(dl_strategy) in test.py
					# get_website_strategy(self, website, country_code, company_code=None):
						# references to WEBSITE_MODULE = 'strategies.website'
							#
			# OTHERS
			# what is proxy?
				# intermediary between the server and the client
					# acts as intermediary for every request of client to the server

			# what is sesion>
				# visiting time of visitor in the website
				# one of the key attributes required to connect to the web


		@Development Stage
			# samples
				# country = 'sa'
				# website = 'www.jarir.com'
				# url = 'https://www.jarir.com/razer-viper-mouse-550683.html'
				# company_code = '200'

				# country = 'uk'
				# website = 'www.carphonewarehouse.com'
				# url = 'https://www.carphonewarehouse.com/motorola/moto-g-5g-plus.html#!colour=blue&capacity=64GB&dealType=sf'
				# company_code = 'P00'

			# headers problem, what to include?
				# https://mkyong.com/computer-tips/how-to-view-http-headers-in-google-chrome/
				# https://www.youtube.com/watch?v=Oz902cJcCUg
				# ordinary headers will work

			# mapping
				# dl_map.json and ws_map.json mapping must be the same with the file name created in strategies

			# sample 

	@Guided Instructions
		# Dont touch the requester or LuminatiSessionRequest
		# test.py static website requests such as: written below, are just static but dynamic in Running Man (SQL application) 
			# country = 'uk'
			# website = 'www.carphonewarehouse.com'
			# url = 'https://www.carphonewarehouse.com/motorola/moto-g-5g-plus.html#!colour=blue&capacity=64GB&dealType=sf'
			# company_code = 'P00' 

		Download Strategies and Website Strategies
			# purpose of download strategies: download the source code first or PDP (Product Page Display)
			# procedure to making dl and ws strategies: 
				mapping first
					# strategies > dl_map.json : mapping the site url
						# key value pair for {url:variable}
					# website > ws_map.json : mapping the site url
						# key value pair for {url:variable}

				making download strategies: go to : stategies > downloads > country_foler (sg) > website_file (giant_sg.py)
					# set headers in "def download" function
					# if you opt to not creating download strategy, it can be handled by default
						# making download strategy is optional
							# strategies > download > default.py
				making website strategies: go to : stategies > website > country_folder (sg) > website_file (giant_sg.py)
					# NO Scraped data handler (error handling)
						# "NOT_FOUND" : declared in : strategies > website > constants.py
						# "NOT_FOUND" handler mapped on : strategies > website > templates.py
					# scraped result template:
						# mapped on : strategies > website > country_folder > static > result_base_template.py
					# you can only create and modify inside these:	
						'''
							def get_price(self, soup):
								try:
									# create and modify what to scrape here
								...
						'''

		Development Stage (Guided)
			# problems you might encounter: (sir Ryan)
				# at download strategy return respond as text
					# some sites should be converted to bytes before text
				# if sites you scrape contains different languages:
					# convert the site to english
				# scraped data should be {key:value} pairs but there are attributes containing multiple columns and scraping it might append all the 	values from different columns
					# to address this, assert this to the validators that data could only key:value pairs and that is our limitation
				# if you get video urls
					# unhandled exceptions
					# an error you dont see
			# problems you might encounter: (sir Seth)
				# during scraping
					# modal
					# 3p
						# convert to engish
						# div?
					# buybox
						# double slash?
						# only in buy box so far
			# get_rating
				# reviews
					# num. of reviewrs?
					# or score of product by percentage?
			# get_brand
				# if no brand found, the et will get the first value of the title if no attrib that handles brand
				# if duplicate brand from get_brand and first word of the get_title; et has mechanism for duplicates

@Luminati AND Unblocker
	# subscription names of BrightData (proxy api) or unblockers
	# BrightData is formerly luminati 
	# different proxy providers? ref: sir Ryan Banilad

	# explained by sir Ryan
		# Luminati
			# cheap and has no whitelisting of ip
		# Unblocker (BrightData)
			# expensive and unblocks almost all sites that blocks

@Api
	# go to networks > check preserve log and disable cache

	# click headers
		# test headers
			# accept: */*
			# accept-encoding: gzip, deflate, br
			# accept-language: en-US,en;q=0.9,fil;q=0.8
			# x-api-source: pc
			# x-requested-with: XMLHttpRequest
			# x-shopee-language: en

			# or something unusual

		# url
			# should the same for the api
			# general > request url
				# https://shopee.ph/api/v4/item/get?itemid=8081379685&shopid=260245345
					# get itemid and shopid
					# api url > https://shopee.ph/api/v4/item/get?itemid=8081379685&shopid=260245345

		# dl strat spiders new method

@Rankings
	# max rank 25
	# if search 10 does not reach 25
		# go to next page
	# extract only
		# title 
		# brand
		# url

	# 3 types system architecture of retail to extract
		# api
			# get
				# modified url
			# post
				# requires:
					# modified url with keyword
						# refrain from using urllib or url encoder
					# headers
						# if not the right headers, some html contents like url or href of the tag will change
							# it should be right header
							# instead test every header attributes with the right data
							# test the url
					# form-data or payload or endpoint
						# use an unparsed form-data or url data
		# html tags
			# use beautifulsoup
		# scripting
			# use find with regex compile
			# use substring
			# test with json.loads

	# should_next_page
		# loop dependent
		# count the max products per page
			# if pdp_products >= limit
			# proceed to next page
		# two types of next page
			# pagination
				# mostly url with page 2
				# with modified number of product or page request 
			# jquery triggered products
				# find the http request url
				# with modified number of product or page request

@Node @Javascript @puppeteer @ppt
	# setup
		# npm install
			# to install all modules in package.json
		# npm list -g
			# show dependencies installed in global
		# npm list
			# show dependencies installed in local repo

@Git
	# 4 branches
		# fix
		# feat
		# hot fix - fix masters for deployment / production

	# extract-transform-comparisons-spiders
		# before anything else:
			# fetch then pull
		# STAGING
			# GUI
				# select file or just click stage all
			# TERMINAL
				# : git status
					# to check files
				# : git add .
					# to put unstaged files to staging area '.' means all on staging area
		# COMMIT
			# write message on commit field below
				# Feat: Implement strategy for selexion.be
				# then press commit
		# PUSH
			# press push

	# comparisons-spiders
		# INITIALIZE
			# can do it once
			''' : git flow init -f
				: master? (just press enter)
				: develop
				: feature/
				: bugfix/
				: release/
				: hotfix/
				: support/
				skip tag prefix by pressing enter
				skip hooks by pressing enter
			''' 
		# WS_MAP
			# discard ws_map in source tree > unstaged
			# create new ws_map in comparison-spiders
		# CREATE BRANCH
			# : git flow feature start implement-strategy-selexion-be
			# : git flow bugfix start add-pagination-for-selexion-be
			# : git flow bugfix start update-pagination-for-selexion-be
		# STAGING
			# : git add .
				# this adds all 
		# COMMIT
			# : git commit -m "Feat: Implement strategy selexion.be"
			# : git commit -m "BugFix: Update strategy xxl.se"
		# FINISH BRANCH
			# or arrow up: change start to finish
			# :git flow feature finish implement-strategy-selexion-be --no-ff
				# '--no-ff' - fast forward means no direct push
				# if unsuccessful, 
					# double click develop then pull, then double click develop
					# then redo the command
		# VIM
			# : :wq
				# ':wq' - write quit means save the branch and commited file the quit VIM
				# note: vim for good developers and contains a lot of commands
		# PUSH
			# press push

		# SWITCH BRANCH
			# git checkout develop
			# git checkout master

@Production
	@Rebuild (Kubernetes / spiders )
		# using puttygen
			# commands are provided in group
		# to trace realtime logs
			# if unsure to local
			# production testing is the purpose
		# rerun?
			# turn on podtail first
		# limitation
			# should make an agreement who to rebuild first

		-----

		# config
			# putty
				# hostname: 172.100.60.5
					# or called .5 compliance vm
				# load ssh
					# got to connection > ssh > auth
						# browse for private key version 2
				# sir rav authorization
				# login as: detail
			
		# check rmq
			# prod1 and prod2
			# it should be zero to specific spiders

		# target directory to rebuild
			'cd /spiders/<target directory>'
			'git fetch && git pull' or in GUI

			# no need to rebuild if latest commit is yesterday and no new commit today for specific spider
				# comparison
				'. deploy-production.sh 202204090845-update-dl-strategy-for-krefel-be-nl'

				# rankings
				'. deploy-production.sh 202204021519-update-dl-strategy-for-argos-co-uk'

				'. deploy-production.sh 202204021534-product-url-for-sears-com-mx'

				# listings
				'. deploy-production.sh 202204011511-update-pagination-for-saturn-de'

				'. deploy-production.sh 202202191518-availability-for-elkjop-no'

					# . deploy-production.sh <yyyy-mm-hhmm>-<commit name>
						# commit name should be all lower case and replace spaces with dashes
					# this is the rebuild command
					# it should be always up to date

		# check logs
			'df -h'
				# watch on /dev/sdal
				# watch on use %
					# it shouldnt reach 99%, mostly reaches 1 month to 99%
					# if it reaches greater than 94%, it needs to be deleted, 
						* '<remove image command>'
							# only intermediates do this command

		# config (go to cluster if r)
			'az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2'
				# tunong and gusto tanawun
				# maoy tanawun nga metric
			# OTHER COMMANDS
				# az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
				# az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
				az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
				az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
				# az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction

				# az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
				# az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
				az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
				az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
				# az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction

				# az aks get-credentials --resource-group crawlers --name AKSListingsProduction
				# az aks get-credentials --resource-group crawlers --name AKSListingsETProduction

				az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
				az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
				# az aks get-credentials --resource-group crawlers --name AKSListingsLProduction


		# check pods
			# 'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
				# this shows the filtered pods

				# success indicators at 'status' column
					# terminating - process gina upload sa live
					# running
					# ContainerCreating - acceptable
				# failed indicators at 'status' column
					# if the statement is not (terminal, running, ContainerCreating)
					# report to sir Jes

				# rebuild done if 
					# age column is equal to or greater than 2m

			# * KUBECTL GET 
				# * pods
					# * to show filtered 

			# 	* if grouped
				# * it is named cluster or config

			# OTHER COMMANDS
				# check pods
					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt'
					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et'

					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings'
					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et'

					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings'
					'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et'

		# remove image
			# ask permission first to sir Rav or seniors
			# cd to spider (only to comparison, rankings and listings, no need et and special spiders (puppeteer))
				# Remove images - change grep "app" 
				'docker images | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
				'docker images | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
				'docker images | grep "listings" | awk '{print $1 ":" $2}' | xargs docker rmi'

	@Rebuild (kubernetes / webshot)
		teamviewer
		886486678
		deployment2019
		# go to source tree
			# go to webshot
				# git fetch and git pull	

				# go to source tree unix terminal
					# alias az="az.cmd"; . deploy-production-override.sh 202112191613-tango-lu-fr-update-delay	
				# go to rebuild compliance vm 172.100.20.5?
					# alias az="az.cmd"; . deploy-production.sh 202106231556-update-delivery-elgiganten--se

	@Guided Instructions (Rerun setup and SQL)
		# FIRST THING FIRST
			# check in tech validation - excel
				# 
			# check in sql
				# if no connection
					# connect to database sql server with (PROD - 1):
						# detail-ultima.database.windows.net
						# database/schema: F00
						# username : detailbe
						# 23ejT3UpwgF4K4U
					# connect to database sql server with (PROD - 2):
						# localhost: detail-sql.database.windows.net
						# database/schema: 010
						# username: detail
						# password: 530Av043b2G22vs
				# if has connection, copy
					# rename or F2 : PROD1 - 100
					# then edit connection or F4
						# change database schema name based on company code
					# open sql script or F3
						# compa
							# make query
							# SELECT * FROM view_all_productdata WHERE date = '2021-11-05' AND item_id = '100DKV3010000001324320202102090603'
							# run or ctrl + enter
						# rankigns
							# 
					# check for errors in sql colummn below
						# check for issue codes:
							# -1 Not Found
							# 1 instock
							# 0 out stock
							# -2 ET error
							# -3 Spider Error
							# auto-copy-over

			# couch
				# document_id
					# compa
						# date today + separator (@) + Trigger time +  separator (@)  +  item_id
						# 2021-11-05@22:00:00@Q00NOHW0IJ000001471510202107050703

					# rankings 
						# 
				# rankings document ID
				# 2 ways to check (70.4)
					# director-comparison
						# search in document ID text box using document_id
						# check for attribute: "runtime_statistics"
							# check for attribute: "status"
					# company-q00-productdata
						# search in document ID test box using document_id
						# check for the issues in tech val and then check for attributes in productdata

			# putty
				# set sessions
					# production1 
						# host name:
							# 172.100.60.6
						# saved Sessions
							# PRODUCTION - 1
						# then go to connection at left panel
							# SSH > Auth
							# then add your public key in browse
							# if no supported authentication method available (server sent: publickey)
								# contact sir Ravilo Olivar and address the public key issue
					# production2
						# host name: 172.100.20.5
						# saved sessions
							# PRODUCTION - 2
						# then go to connection at left panel
							# SSH > Auth
							# then add your public key in browse
							# if no supported authentication method available (server sent: publickey)
								# contact sir Ravilo Olivar and address the public key issue

				# rerun
					# commands at: D:\POST GRADUATION\PRESENT\[1]INRIVER\actual\spiders\PRODUCTION\RERUN SHIT.txt
					
					# open the saved session in putty (prod1)
						# 172.100.60.6
						# port22
						# user name: detail
						# cd /opt/job-manager-production
						# source env/bin/activate
					# open the saved session in putty (prod2)
						# 172.100.20.5
						# port22
						# user name: detail
						# cd /opt/job-manager
						# source /opt/venv/bin/activate

				# re on sql after re run
					# compa
						# python rerun_cmp.py -l 2021-06-25@23:00:00@200GBDF080000000556750201904220839 -s CRAWL_FINISHED
					# rankings
						# Currentdate@currenttime@item_id -s CRAWL_FAILED
						# python rerun_rnk.py -l 2021-11-05@04:00:00@Z00@e2ac17ec-8055-421c-8e43-bacae88e10e3@af0ae9c6-2100-494d-b896-f0822f6d7f09 -s CRAWL_FAILED
						# python rerun_rnk.py -l 2021-11-05@04:00:00@Z00@9224e01c-1747-4192-b1da-275c02b60406@rPZW6r-yEei-eQANOiOHiA -s CRAWL_FAILED
						# python rerun_rnk.py -l 2021-11-05@04:00:00@Z00@454564b8-0603-4816-8f05-aa69a0f9b789@rPZW6r-yEei-eQANOiOHiA -s CRAWL_FAILED

		# dbeaver
			# intall this
			# DBeaver 7.3.4
				
		# trigger time - time on when to run
			# 20:00:00 - AE, KW
			# 21:00:00 - FI, RU, GR, SA, RO
			# 22:00:00 - SE, DE FR, NL, NO, DK, IT, ES, BE, CH, CZ, AT, PL, RS
			# 23:00:00 - SE, UK, GB, PT
			# 02:00:00 - BR
			# 04:00:00 - US, CA
			# 05:00:00 - MX
			# 14:00:00 - AU
			# 15:00:00 - JP
			# 16:00:00 - CN, SG, KH
			# 17:00:00 - VN

		# for verification from sir Ryan
			# 20:00:00 - 04:00 AM AE
			# 21:00:00 - 05:00 AM FI RU SA GR KW RO BG
			# 22:00:00 - 06:00 AM SE DE FR NL NO DK IT ES BE CH CZ AT PL HR RS
			# 23:00:00 - 07:00 AM UK PT
			# 02:00:00 - 10:00 AM BR
			# 04:00:00 - 12:00 NN US CA
			# 05:00:00 - 01:00 PM MX
			# 14:00:00 - 10:00 PM AU
			# 15:00:00 - 11:00 PM JP
			# 16:00:00 - 12:00 AM CN SG, KH
			# 17:00:00 - VN

		# PRODUCTION 1 - COMPANY CODE
			# 100
			# 110
			# 410
			# 510
			# 610
			# 710
			# A00
			# E00
			# F00
			# J00
			# K00
			# M00
			# N00
			# O00
			# P00
			# Q00
			# R00
			# S00
			# T00
			# U00
			# V00
			# W00
			# X00
			# Z00

		# Production 2 
			# 010
			# 110
			# 200
			# 210
			# 310
			# 610
			# A00
			# A10
			# K00
			# P00
			# U00
			# X00
			# Z00

@SITES
	# outlook: https://outlook.office.com/mail/inbox
	# calamari: https://detailtechph.asia.calamari.io/clockin/main-new.do#/ui/clockin/clock/clock.html
	# https://app-new.calamari.io/clockin/clock
		# company: detailtechph
		# email and password: inriver credentials
	# unblocker: http://172.100.30.6:13475/home
		# knebril
		# adminpassword
	# whitelister: http://40.68.7.96:45606/main.html
		# kurt.nebril 
		# CCD21

	# backdoor-app: 
		# https://detail-backdoor-app.azurewebsites.net/#/

	# http://172.100.70.4:5984/_utils/#login
		# couch prod - 1
			# admin
			# wT2v4wDe79e9dTP5
	# http://172.100.70.9:5984/_utils/#
		# couch prod - 2
			# admin
			# YrAw8vJ9RwWTpSqZ
	# http://172.100.50.4:15672/#/queues
		# rabbit mq (rmq)
			# user: admin
			# pass: DetailUltima2018
	# http://13.69.73.79/admin/login/?next=/admin/
		# ultima
			# kurt.nebril@detailonline.tech
			# pw
				# testonly
				# randompassword

	# setup  sheet
		# https://detailmerchandisingonline.sharepoint.com/:x:/r/sites/DetailQA-Validation/_layouts/15/Doc.aspx?sourcedoc=%7B4694FA38-B25E-470E-94AD-361891CA291B%7D&file=Setup%20Validation%20Bugs%20List.xlsx&action=default&mobileredirect=true

		# https://inriver.sharepoint.com/:x:/s/EvaluateQA-Validation/EaJIyTullytKiBrG8jrKiLYB-1oRJciLABeZ2WMAiKgBTA

	# daily
		# https://detailmerchandisingonline.sharepoint.com/:x:/s/DetailQA-Validation/EadCFEkvsK1DsKGY098TW8UBSZi5rhi__xX1awoayur9TQ?e=3v96xy&isSPOFile=1

		# https://inriver.sharepoint.com/:x:/s/EvaluateQA-Validation/EbTJZnzFkYlEm4zbQtRvVdABDTRYVUkniPPDVPMaFybjfg?e=hxqbkv&wdOrigin=TEAMS-ELECTRON.p2p.bim&wdExp=TEAMS-CONTROL&wdhostclicktime=1647340162264

	# trello
		# https://trello.com/b/GJcNrRy8/backend-issues-new-architecture
		# kurt.nebril@detailonline.tech
		# object

	# inriver frankly survey
		# https://andfrankly.com/insight/be-frank

	# inriver intranet
		# https://inriver.sharepoint.com/sites/asONE365/SitePages/inRiver-2022-Kick-off.aspx?CT=1649689354183&OR=OWA-NT&CID=4deae802-1254-399f-77f5-37d394d7bf73

	# inriver trainings, gamificaiton
		# https://staff.inriver.academy/reports/userinfo/id:9048

	# dashboard with course list button
		# https://staff.inriver.academy/dashboard/index

	# # central command
	# 	guys. for central-command database:

	# 	server: detail-sql.database.windows.net
	# 	db_name: central-command
	# 	username: detailbe
	# 	password: 23ejT3UpwgF4K4U

	# unblocker sheet
		# https://inriver-my.sharepoint.com/:x:/r/personal/jonathan_shen_inriver_com/_layouts/15/Doc.aspx?sourcedoc=%7BB3DF770B-AC30-4BC8-B037-AB396126284A%7D&file=spider_retailer_dev_list.xlsx&wdOrigin=OFFICECOM-WEB.MAIN.REC&ct=1655192186819&action=default&mobileredirect=true&cid=aee49d70-aa3a-456b-9cbe-3dc5689cf270&clickparams=eyJBcHBOYW1lIjoiVGVhbXMtRGVza3RvcCIsIkFwcFZlcnNpb24iOiIyNy8yMjA2MDYxNDgwNSIsIkhhc0ZlZGVyYXRlZFVzZXIiOmZhbHNlfQ%3D%3D

	# techval
		# https://inriver.sharepoint.com/:x:/r/sites/EvaluateQA-Validation/_layouts/15/Doc.aspx?sourcedoc=%7B7C66C9B4-91C5-4489-9B8C-DB42D46F55D0%7D&file=Daily%20Run%20Technical%20and%20Validation%20Bugs%20List.xlsx&action=default&mobileredirect=true&cid=a5e61d95-61b2-49cc-b5bb-4098f8554afa

		# validation
			# https://inriver-my.sharepoint.com/:x:/r/personal/jonathan_shen_inriver_com/_layouts/15/doc2.aspx?sourcedoc=%7B587fce11-ec9f-46e1-bd2f-f156e79b5ee0%7D&action=edit&activeCell=%27prod_valid_issue%27!C835&wdinitialsession=129bec10-0e7a-46d2-96f8-749d03609256&wdrldsc=4&wdrldc=1&wdrldr=AccessTokenExpiredWarning%2CRefreshingExpiredAccessT&cid=ee664730-00a2-4e9d-b72f-dd7aac87bdb4

	# shared folders	
		# https://inriver-my.sharepoint.com/personal/jonathan_shen_inriver_com/_layouts/15/onedrive.aspx?ga=1&id=%2Fpersonal%2Fjonathan%5Fshen%5Finriver%5Fcom%2FDocuments%2Fda%5Foperations&OR=Teams%2DHL&CT=1675135295411&clickparams=eyJBcHBOYW1lIjoiVGVhbXMtRGVza3RvcCIsIkFwcFZlcnNpb24iOiIyNy8yMzAxMDEwMDkxMyIsIkhhc0ZlZGVyYXRlZFVzZXIiOmZhbHNlfQ%3D%3D

	RANKINGS AND LISTINGS DEVELOPMENT ASSIGNMENT
		# https://inriver-my.sharepoint.com/:x:/r/personal/jonathan_shen_inriver_com/_layouts/15/Doc.aspx?sourcedoc=%7BF5527BCF-F689-4648-8244-671300FD186E%7D&file=ms_rnk_lst_retailers.xlsx&action=default&mobileredirect=true&cid=b70f82a8-93f7-473c-b4a7-fac7058493e6&clickparams=eyJBcHBOYW1lIjoiVGVhbXMtRGVza3RvcCIsIkFwcFZlcnNpb24iOiIyNy8yMzAxMDUwNTYwMCIsIkhhc0ZlZGVyYXRlZFVzZXIiOmZhbHNlfQ%3D%3D

@ Calamari Activity status:
	daily standup - 00 Admin - Sys Admin
	setup (autopilot) - 00 Admin - Sys Admin
	team meeting - 2023 Evaluate Maintenance
	company meeting - 00 Admin - Sys Admin
	gathering tech validation - 2023 Evaluate Maintenance
	fixing tech validation, trello tickets, rerun and sql requests - 2023 <client_name> Maintenance
	MS retailer development - 2023 MS EEO/PGI Service Requests
	Other requests - projects to given by Jon
	IR Maintenance - 2023 Evaluate Maintenance

	

@Repositories
	# accessing inriver repos
		# https://dev.azure.com/inriver/Evaluate

		# instruction:
			# https://inriver-my.sharepoint.com/:w:/p/jonathan_shen/EV4mYVayWaNKosxEbduEr7kBJM9YjMiPtHzPQvV55rdWsQ?rtime=6VmiL5p52kg

	-------

	# git@52.157.149.161:comparison-spiders
	# git@git.v3.detailonline.com:extract-transform-comparison
	# git@git.v3.detailonline.com:puppeteer-comparison-spiders
	# git@ssh.dev.azure.com:v3/inriver/Evaluate/be-comparison-load

	# git@52.157.149.161:ranking-spiders
	# git@git.v3.detailonline.com:extract-transform-rankings
	# git@git.v3.detailonline.com:puppeteer-rankings-spiders
	# git@ssh.dev.azure.com:v3/inriver/Evaluate/be-rankings-load

	# git@52.157.149.161:listing-spiders
	# git@git.v3.detailonline.com:extract-transform-listings
	# git@git.v3.detailonline.com:puppeteer-listings-spiders

	# git@git.v3.detailonline.com:puppeteer-comparison-spiders
	# git@git.v3.detailonline.com:puppeteer-rankings-spiders
	# git@git.v3.detailonline.com:puppeteer-listings-spiders
	# git@git.v3.detailonline.com:webshot-nodejs

	# CCEP
		# https://github.com/Shin-Aska/nxtgen_spiders.git

	# PHYSICAL STORES
		# git@git.v3.detailonline.com:physical-stores-spiders
		# git@git.v3.detailonline.com:extract-transform-physical-stores

@keys