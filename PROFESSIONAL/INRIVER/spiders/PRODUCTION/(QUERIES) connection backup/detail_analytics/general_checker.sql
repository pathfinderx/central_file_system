-- ALL ACTIVATE websites
SELECT DISTINCT website
FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-18'

-- check count all postal
SELECT COUNT(*) 
FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-11'

-- check count without data job id
SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('amazon.es.dia') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('amazon.es.fresh') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('elcorteingles.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('hipercor.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('sanchez-romero.online') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-25' AND website in ('tienda.mercadona.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

-- amazon.es.dia 23
-- amazon.es.fresh 93
-- elcorteingles.es 219
-- hipercor.es 0
-- sanchez-romero.online 40
-- tienda.mercadona.es 3


-- CHECKING MISSING POSTAL CODE(JOB) TO production_spider_rawdata
SELECT * FROM [nxt_spiders].[job]
WHERE date = '2023-01-11'
AND company_code = '700'
AND type = 'listing'
--AND website in ('amazon.es.fresh','amazon.es.dia')
--AND website in ('sanchez-romero.online')
AND website in ('tienda.mercadona.es','sanchez-romero.online','hipercor.es','elcorteingles.es')
AND id NOT IN (
SELECT DISTINCT job_id FROM [nxt_spiders].[production_spider_rawdata])
order by website
