-- USE THIS FIND DUPLICATES FROM SPECIFIC POSTAL OF RETAILER
SELECT a.id, a.postal_code, CASE
    WHEN count(b.job_id) > 0 THEN count(b.job_id)
    ELSE 0
END as result, a.source_url
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
WHERE a.date = '2023-01-25'
AND a.website = 'hipercor.es'
AND a.postal_code = '19004'
GROUP BY a.id, a.postal_code, a.source_url, b.job_id