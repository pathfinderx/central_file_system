-- LISTINGS: DELETE ALL BY DATE + RETAILER --
declare @target_date DATE;
declare @website_id_tbl as table(id int)

---------------------------------------------
-- Set target date
set @target_date = '2021-07-21'

-- Set target website URLs with www prefix
print 'Number of selected website URLs:'
insert into @website_id_tbl(id)
select pg_id from rcms_websites
where website_url in (
'www.outnorth.se'
)
---------------------------------------------

print char(13) + 'Deleted ETL data:'
delete img_up from listings_data_element_image_lookup img_up
join listings_data_elements elem on img_up.element_id = elem.id
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
join rcms_listings rl on yoke.cms_listings_list_id = rl.pg_id
where 
	thread.recorded = @target_date and
	rl.website_id in (
		select id from @website_id_tbl
	)

delete elem from listings_data_elements elem
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
join rcms_listings rl on yoke.cms_listings_list_id = rl.pg_id
where 
	thread.recorded = @target_date and
	rl.website_id in (
		select id from @website_id_tbl
	)

print char(13) + 'Deleted copy over:'
delete [copy] from listings_data_override [copy]
join listings_data_yoke yoke on [copy].listings_data_yoke_id = yoke.id
join rcms_listings rl on yoke.cms_listings_list_id = rl.pg_id
where 
	[copy].recorded = @target_date and
	rl.website_id in (
		select id from @website_id_tbl
	)

print char(13) + 'Deleted dead link:'
delete ldl from listings_data_dead_links ldl 
join rcms_listings rl on ldl.cms_listings_list_id = rl.pg_id
where 
	ldl.record_date = @target_date and
	rl.website_id in (
		select id from @website_id_tbl
	)

print char(13) + 'Deleted backdoor data:'
delete bel from backdoor_etl_listings_data bel 
join rcms_listings rl on bel.cms_listings_list_id = rl.pg_id
where 
	bel.record_date = @target_date and
	rl.website_id in (
		select id from @website_id_tbl
	)

select count(1) from view_all_listings_data
where date = @target_date and 
	website_id in (
		select id from @website_id_tbl
	)