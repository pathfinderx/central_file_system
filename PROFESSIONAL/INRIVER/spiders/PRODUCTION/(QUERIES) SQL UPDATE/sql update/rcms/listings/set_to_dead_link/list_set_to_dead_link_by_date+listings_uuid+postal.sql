-- LISTINGS: SET TO DEAD LINK BY DATE + LISTING_UUID + POSTAL_CODE --
DECLARE @target_date DATE,
		@postal_code VARCHAR(10),
		@is_confirmed BIT = 1;
DECLARE @listings_uuid_id_tbl AS TABLE(listing_uuid_id INT);

-------------------------------------------------------------
-- Set postal code, default = '-1' (no postal code)
SET @postal_code = '-1'

-- Set target date
SET @target_date = '2022-06-21' 

-- Set target listing_uuid
PRINT CHAR(13) + 'Number of selected listing_uuid:'
INSERT INTO @listings_uuid_id_tbl(listing_uuid_id)
SELECT pg_id FROM rcms_listings
WHERE listing_uuid in (
'6ea9346f-77e6-43c5-a359-624367249f89'
)
-------------------------------------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_listings_data
WHERE record_date = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead links:'
DELETE FROM listings_data_dead_links
WHERE record_date = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM listings_data_override o
JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
WHERE o.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL data:'
DELETE i FROM listings_data_element_image_lookup i
JOIN listings_data_elements e ON i.element_id = e.id
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

DELETE e FROM listings_data_elements e
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Set to dead links:'
INSERT INTO listings_data_dead_links(record_date, cms_listings_list_id, postal_code,is_confirmed)
SELECT 
	@target_date, listing_uuid_id, @postal_code, @is_confirmed
FROM @listings_uuid_id_tbl

SELECT [date], listings_uuid, source, count(1) FROM view_all_listings_data
WHERE [date] = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)
GROUP BY [date], listings_uuid, source
ORDER BY listings_uuid, date, source
