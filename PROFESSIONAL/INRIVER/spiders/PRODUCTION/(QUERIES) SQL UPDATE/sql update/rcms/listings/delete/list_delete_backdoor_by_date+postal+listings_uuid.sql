-- LISTINGS: DELETE BACKDOOR BY DATE + POSTAL_CODE + LISTINGS_UUID --
declare @postal_code VARCHAR(255),
		@target_date DATE,
		@cms_listing_id INT;
declare @cms_listing_id_tbl as table(id int)

-----------------------------------------------
-- Set target date
set @target_date = '2021-07-21' 

-- Set target postal code, no postal = '-1'
set @postal_code = '-1'

-- Set target listings_uuid, comma separated
print 'Number of selected listings_uuid:'
insert into @cms_listing_id_tbl(id)
select pg_id from rcms_listings
where listing_uuid in (
'2d5c460b-cb5a-4ede-b4a6-239a1a56961d'
)

-----------------------------------------------

print char(13) + 'Deleted backdoor data:'
delete from backdoor_etl_listings_data
where 
	record_date = @target_date and
	postal_code = @postal_code and
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

select date, listings_uuid, source, count(1) from view_all_listings_data
where date = @target_date and 
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)
group by date, listings_uuid, source