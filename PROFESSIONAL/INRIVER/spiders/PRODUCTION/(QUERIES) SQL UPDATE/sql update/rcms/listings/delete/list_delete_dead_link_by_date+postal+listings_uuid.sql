-- DELETE DEAD LINK DATA BY DATE + POSTAL_CODE + LISTINGS_UUID --
declare @postal_code VARCHAR(255),
		@target_date DATE;
declare @cms_listing_id_tbl as table(id int)

-----------------------------------------------------------------
-- Set target date
set @target_date = '2022-07-27'

-- Set target postal code, no postal = '-1'
set @postal_code = '-1'

-- Set target listings_uuid, comma separated
insert into @cms_listing_id_tbl(id)
select pg_id from rcms_listings
where listing_uuid in ('7e77dbe9-5d4b-4d75-aebe-a6cb8278d94b',
'b9fdcad8-cf5e-4d36-babd-881623a7a746',
'3270c547-102f-4cd0-b7a7-96f75b444405',
'4a59a353-ae2e-4dc5-8cf9-b759a990b329')

-----------------------------------------------------------------

print ''
print 'Deleted dead link:'
delete from listings_data_dead_links
where 
	record_date = @target_date and
	postal_code = @postal_code and
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

select date, listings_uuid, source, count(1) from view_all_listings_data
where date = @target_date and 
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)
group by date, listings_uuid, source
	
