-- LISTINGS: DELETE COPY OVER BY DATE + POSTAL_CODE + LISTINGS_UUID --
declare @postal_code VARCHAR(255),
		@target_date DATE,
		@cms_listing_id INT;
declare @cms_listing_id_tbl as table(id int)

-----------------------------------------------
-- Set target date
set @target_date = '2022-08-07'

-- Set target postal code, no postal = '-1'
set @postal_code = '-1'

-- Set target listings_uuid, comma separated
print 'Number of selected listings_uuid:'
insert into @cms_listing_id_tbl(id)
select pg_id from rcms_listings
where listing_uuid in (
'07d4ac0c-443e-499d-80cc-db5de067b0bc'
)
-----------------------------------------------

print char(13) + 'Deleted copy over:'
delete [copy] from listings_data_override [copy]
join listings_data_yoke yoke on [copy].listings_data_yoke_id = yoke.id
where 
	[copy].recorded = @target_date and
	yoke.postal_code = @postal_code and
	yoke.cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

select date, listings_uuid, source, count(1) from view_all_listings_data
where date = @target_date and 
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)
group by date, listings_uuid, source