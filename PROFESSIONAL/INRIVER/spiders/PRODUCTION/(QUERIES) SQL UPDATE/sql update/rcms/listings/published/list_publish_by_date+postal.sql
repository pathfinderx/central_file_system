-- LISTINGS: PUBLISH/UNPUBLISH BY DATE + POSTAL --
declare @target_date date,
		@published bit,
		@postal_code varchar(20);
declare @source_tbl table(record_date date, list_id int, is_published bit, postal_code varchar(20));

--------------------------------------------------------------------
-- Set target date
set @target_date = '2021-07-21'

-- Set published value(1=published, 0=unpublished)
set @published = 1

-- Set postal code, default='-1'(no postal code)
set @postal_code = '-1'

-- Set target retailer with www. prefix
insert into @source_tbl(record_date, list_id, is_published, postal_code)
select @target_date, pg_id, @published, @postal_code
from rcms_listings
where is_active = 1
---------------------------------------------------------------------

print char(13) + 'Updated current published data:'
update t set t.published = s.is_published
from publish_listings_data t
join @source_tbl s on s.record_date = t.record_date
and s.postal_code = t.postal_code and s.list_id = t.cms_listings_list_id

print char(13) + 'Inserted new published data:'
insert into publish_listings_data(record_date, postal_code, cms_listings_list_id, published)
select s.record_date, s.postal_code, s.list_id, s.is_published from @source_tbl s
left join publish_listings_data t on s.record_date = t.record_date
and s.postal_code = t.postal_code and s.list_id = t.cms_listings_list_id
where t.id is null

select date, postal_code, is_published, count(1) from view_all_listings_data
where date = @target_date
and postal_code = @postal_code
and cms_listings_list_id in (
	select list_id from @source_tbl
)
group by date, postal_code, is_published
