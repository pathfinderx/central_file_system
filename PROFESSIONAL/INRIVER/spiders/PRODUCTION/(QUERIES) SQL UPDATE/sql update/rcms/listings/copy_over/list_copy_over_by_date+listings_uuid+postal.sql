-- LISTINGS: COPY OVER BY DATE + LISTING_UUID + POSTAL_CODE--
DECLARE @source_date DATE,
		@target_date DATE,
		@postal_code VARCHAR(10);
DECLARE @listings_uuid_id_tbl AS TABLE(listing_uuid_id INT);

-------------------------------------------------------------
-- Set postal code, default = '-1' (no postal code)
SET @postal_code = '-1'

-- Set source date
SET @source_date = '2021-07-20' 

-- Set target date
SET @target_date = '2021-07-21' 

-- Set target listing_uuid
PRINT CHAR(13) + 'Number of selected listing_uuid:'
INSERT INTO @listings_uuid_id_tbl(listing_uuid_id)
SELECT pg_id FROM rcms_listings
WHERE listing_uuid in (
'09017d24-4df0-42eb-a244-26c2b1756114'
)
-------------------------------------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_listings_data
WHERE record_date = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead links:'
DELETE FROM listings_data_dead_links
WHERE record_date = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM listings_data_override o
JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
WHERE o.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL data:'
DELETE i FROM listings_data_element_image_lookup i
JOIN listings_data_elements e ON i.element_id = e.id
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

DELETE e FROM listings_data_elements e
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Copied over from backdoor data:'
INSERT INTO backdoor_etl_listings_data(
	record_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
	description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
	promo_description_id, promo_price, promo_currency, availability, page_number, source)
SELECT 
	@target_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
	description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
	promo_description_id, promo_price, promo_currency, availability, page_number, 'be_copy_override'
FROM backdoor_etl_listings_data
WHERE record_date = @source_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Copied over from dead links:'
INSERT INTO listings_data_dead_links(record_date, postal_code, cms_listings_list_id, is_confirmed)
SELECT 
	@target_date, postal_code, cms_listings_list_id, is_confirmed FROM listings_data_dead_links
WHERE record_date = @source_date
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Copied over from copy over data:'
INSERT INTO listings_data_override(
	recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
	currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
	[availability], page_number, thread_id, element_id, copy_type)
SELECT 
	@target_date, o.listings_data_yoke_id, o.is_published, o.url_id, o.title_id, o.description_id, o.[rank], o.price,
	o.currency, o.unit_price, o.unit_currency, o.unit_metric, o.promo_description_id, o.promo_price, o.promo_currency,
	o.[availability], o.page_number, o.thread_id, o.element_id, 'manual_copy_over' 
FROM listings_data_override o
JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
WHERE o.recorded = @source_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

PRINT CHAR(13) + 'Copied over from ETL data:'
INSERT INTO listings_data_override(
	recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
	currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
	[availability], page_number, thread_id, element_id, copy_type)
SELECT 
	@target_date, t.listings_data_yoke_id, t.is_published, e.url_id, e.title_id, e.description_id, e.[rank], e.price,
	e.currency, e.unit_price, e.unit_currency, e.unit_metric, e.promo_description_id, e.promo_price, e.promo_currency,
	e.[availability], e.page_number, e.thread_id, e.id, 'manual_copy_over' 
FROM listings_data_elements e
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @source_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)

SELECT [date], listings_uuid, source, count(1) FROM view_all_listings_data
WHERE [date] in (@source_date, @target_date)
	AND postal_code = @postal_code
	AND cms_listings_list_id IN (
		SELECT listing_uuid_id FROM @listings_uuid_id_tbl
	)
GROUP BY [date], listings_uuid, source
ORDER BY listings_uuid, date, source
