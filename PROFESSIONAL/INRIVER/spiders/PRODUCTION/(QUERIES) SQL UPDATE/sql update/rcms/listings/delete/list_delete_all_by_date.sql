-- LISTINGS: DELETE ALL BY DATE --
declare @target_date date;

-- Set target date
set @target_date = '2020-06-28'

print 'Deleted ETL data:'
delete img_up from listings_data_element_image_lookup img_up
join listings_data_elements elem on img_up.element_id = elem.id
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
where 
	thread.recorded = @target_date

delete elem from listings_data_elements elem
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
where 
	thread.recorded = @target_date

print ''
print 'Deleted copy over:'
delete [copy] from listings_data_override [copy]
join listings_data_yoke yoke on [copy].listings_data_yoke_id = yoke.id
where 
	[copy].recorded = @target_date

print ''
print 'Deleted dead link:'
delete from listings_data_dead_links
where 
	record_date = @target_date

print ''
print 'Deleted backdoor data:'
delete from backdoor_etl_listings_data
where 
	record_date = @target_date

print ''
print 'Deleted published data:'
delete from publish_listings_data
where 
	record_date = @target_date

select count(1) from view_all_listings_data
where date = @target_date
