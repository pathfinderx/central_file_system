-- LISTINGS: DELETE BY DATE + LISTINGS_UUID + POSTAL_CODE + RANK--
DECLARE @target_date DATE,
		@postal_code VARCHAR(10),
		@rank INT,
		@listings_uuid_id INT;

-------------------------------------------------------------
-- Set postal code, default = '-1' (no postal code)
SET @postal_code = '-1'

-- Set target date
SET @target_date = '2021-07-21' 

-- Set target listing_uuid
SELECT TOP 1 @listings_uuid_id = pg_id from rcms_listings
where listing_uuid = '2d5c460b-cb5a-4ede-b4a6-239a1a56961d'

-- Set minimum included rank
SET @rank = 4

-------------------------------------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_listings_data
WHERE record_date = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id = @listings_uuid_id
	AND [rank] >= @rank

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM listings_data_override o
JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
WHERE o.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id = @listings_uuid_id
	AND [rank] >= @rank

PRINT CHAR(13) + 'Deleted ETL data:'
DELETE i FROM listings_data_element_image_lookup i
JOIN listings_data_elements e ON i.element_id = e.id
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id = @listings_uuid_id
	AND [rank] >= @rank

DELETE e FROM listings_data_elements e
JOIN listings_data_thread t ON e.thread_id = t.id
JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
WHERE t.recorded = @target_date
	AND y.postal_code = @postal_code
	AND y.cms_listings_list_id = @listings_uuid_id
	AND [rank] >= @rank

SELECT [date], listings_uuid, [rank] FROM view_all_listings_data
WHERE [date] = @target_date
	AND postal_code = @postal_code
	AND cms_listings_list_id = @listings_uuid_id
