-- LISTINGS: DELETE ALL BY DATE + POSTAL_CODE + LISTINGS_UUID --
declare @postal_code VARCHAR(255),
		@target_date DATE,
		@cms_listing_id INT;
declare @cms_listing_id_tbl as table(id int)

----------------------------------------------
-- Set target date
set @target_date = '2022-08-07' 

-- Set target postal code, no postal = '-1'
set @postal_code = '-1'

-- Set target listings_uuid, comma separated
print 'Number of selected listings_uuid:'
insert into @cms_listing_id_tbl(id)
select pg_id from rcms_listings
where listing_uuid in (
'07d4ac0c-443e-499d-80cc-db5de067b0bc'
)
---------------------------------------------

print char(13) + 'Deleted ETL data:'
delete img_up from listings_data_element_image_lookup img_up
join listings_data_elements elem on img_up.element_id = elem.id
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
where 
	thread.recorded = @target_date and
	yoke.postal_code = @postal_code and
	yoke.cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

delete elem from listings_data_elements elem
join listings_data_thread thread on elem.thread_id = thread.id
join listings_data_yoke yoke on thread.listings_data_yoke_id = yoke.id
where 
	thread.recorded = @target_date and
	yoke.postal_code = @postal_code and
	yoke.cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

print char(13) + 'Deleted copy over:'
delete [copy] from listings_data_override [copy]
join listings_data_yoke yoke on [copy].listings_data_yoke_id = yoke.id
where 
	[copy].recorded = @target_date and
	yoke.postal_code = @postal_code and
	yoke.cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

print char(13) + 'Deleted dead link:'
delete from listings_data_dead_links
where 
	record_date = @target_date and
	postal_code = @postal_code and
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

print char(13) + 'Deleted backdoor data:'
delete from backdoor_etl_listings_data
where 
	record_date = @target_date and
	postal_code = @postal_code and
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)

select date, listings_uuid, source, count(1) from view_all_listings_data
where date = @target_date and 
	cms_listings_list_id in (
		select id from @cms_listing_id_tbl
	)
group by date, listings_uuid, source