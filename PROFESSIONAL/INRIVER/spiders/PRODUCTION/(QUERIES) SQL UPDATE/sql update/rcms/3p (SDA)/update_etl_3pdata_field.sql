update p set p.<target column> = <updated value>
from etl_3pdata p
join etl_productdata e on p.etl_productdata_id = e.id
join cms_product_skus s on e.cms_product_sku_id = s.pg_id 
where e.record_date = <target date> and 
	item_id = <target item_id> and
	p.[rank] = <target position>

in_stock -> availability
position -> rank

example:
update p set p.availability = 1
from etl_3pdata p
join etl_productdata e on p.etl_productdata_id = e.id
join rcms_product_skus s on e.cms_product_sku_id = s.pg_id 
where e.record_date = '2022-11-26' and 
	s.item_id = '200NOE3070000001398460202105040415' and
	p.[rank] = 3

update e3o set e3o.is_new = 1
from etl_3pdata_override e3o
join rcms_product_skus s on e3o.cms_product_sku_id = s.pg_id 
where e3o.record_date = '2022-11-26'
and s.item_id = '200NOE3070000001398460202105040415'
and [rank] = 3

