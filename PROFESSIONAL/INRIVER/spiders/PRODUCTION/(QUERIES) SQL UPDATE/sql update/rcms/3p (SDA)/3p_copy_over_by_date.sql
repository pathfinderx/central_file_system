-- 3P COPY OVER BY DATE --
declare @target_date date,
		@source_date date;

------------------------------------
-- Set source date
set @source_date = '2021-12-16' 

-- Set target date
set @target_date = '2021-12-18' 

------------------------------------

print ''
print 'Deleted ETL data on: ' + CAST(@target_date as varchar(20))
delete e3 from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date

print ''
print 'Deleted copy over data on: ' + CAST(@target_date as varchar(20))
delete from etl_3pdata_override
where record_date = @target_date

print ''
print 'Copy over from ETL:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, ep.cms_product_sku_id, e3.etl_3p_title_lookup_id, e3.etl_3p_description_lookup_id, e3.etl_3p_retailer_name_lookup_id,
	e3.etl_3p_product_url_lookup_id, e3.etl_3p_logo_url_lookup_id, e3.etl_3p_condition_lookup_id, e3.etl_3p_delivery_lookup_id,
	e3.etl_3p_shipping_lookup_id, e3.price, e3.iso_currency, e3.availability, e3.rating_score, e3.rating_review, rank,
	etl_3p_retailer_url_lookup_id, is_direct_seller, e3.id, 'manual_copy_over', is_new 
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @source_date

print ''
print 'Copy over from override:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, cms_product_sku_id,	etl_3p_title_lookup_id,	etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, 'manual_copy_over', is_new
from etl_3pdata_override
where record_date = @source_date

select date, source, count(1) from view_3p_data
where date in (@source_date, @target_date)
group by date, source
order by date, source