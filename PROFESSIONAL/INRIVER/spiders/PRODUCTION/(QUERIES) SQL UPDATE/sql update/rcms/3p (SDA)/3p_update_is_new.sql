-- 3P UPDATE IS_NEW --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@position int,
		@is_new smallint;

------------------------------------
-- Set target date
set @target_date = '' 

-- Set target item_id
set @item_id = '' 

-- Set target position
set @position = 2

-- Set retailer_name
set @is_new = 0
------------------------------------

update p set p.is_new = 0
from etl_3pdata p
join etl_productdata e on p.etl_productdata_id = e.id
join rcms_product_skus s on e.cms_product_sku_id = s.pg_id 
where e.record_date = @target_date and 
	s.item_id = @item_id and
	p.[rank] = @position

update e3o set e3o.is_new = 0
from etl_3pdata_override e3o
join rcms_product_skus s on e3o.cms_product_sku_id = s.pg_id 
where e3o.record_date = @target_date
and s.item_id = @item_id
and [rank] = @position