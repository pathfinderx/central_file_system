-- 3P UPDATE RETAILER NAME --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@position int,
		@retailer_name nvarchar(255),
		@retailer_name_id int;
declare @inserted_retailer_name as table(retailer_name_id int);

------------------------------------
-- Set target date
set @target_date = '2022-08-07' 

-- Set target item_id
set @item_id = '200ES8D080000000790700201906140221' 

-- Set target position
set @position = 1

-- Set retailer_name
set @retailer_name = 'amazon.es' 
------------------------------------

select top 1 @sku_id = pg_id from rcms_product_skus
where item_id = @item_id

select top 1 @retailer_name_id = id from etl_3p_retailer_name_lookup
where hash_value = HASHBYTES('MD5', @retailer_name)

if @retailer_name_id is null
begin 	
	print char(13) + 'Inserted new retailer name:'
	insert into etl_3p_retailer_name_lookup(retailer)
	output inserted.id into @inserted_retailer_name
	values(@retailer_name)

	select @retailer_name_id = retailer_name_id from @inserted_retailer_name
end

print char(13) + 'Updated ETL data:'
update e3 set etl_3p_retailer_name_lookup_id = @retailer_name_id
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date
and ep.cms_product_sku_id = @sku_id
and e3.[rank] = @position

print char(13) + 'Updated copy over data:'
update etl_3pdata_override set etl_3p_retailer_name_lookup_id = @retailer_name_id
where record_date = @target_date
and cms_product_sku_id = @sku_id
and [rank] = @position

print char(13) + 'Result:'
select top 1 [date], item_id, position, retailer_name from view_3p_data
where [date] = @target_date
and item_id = @item_id
and position = @position