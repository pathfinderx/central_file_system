-- 3P COPY OVER BY DATE + RETAILER --
declare @target_date date,
		@source_date date;
declare @sku_id_tbl as table(id int, item_id VARCHAR(100));

------------------------------------
-- Set source date
set @source_date = '2022-01-03' 

-- Set target date
set @target_date = '2022-01-04' 

-- Set target retailers with www. prefix
print 'Number of selected item_ids:'
INSERT INTO @sku_id_tbl(id, item_id)
SELECT s.pg_id, s.item_id 
FROM rcms_product_skus s
JOIN rcms_websites w ON s.website_id = w.pg_id
WHERE s.is_active = 1 AND
w.website_url in ( 
'fr.toppreise.ch'
)
------------------------------------

print ''
print 'Deleted ETL data on: ' + CAST(@target_date as varchar(20))
delete e3 from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date
and ep.cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print ''
print 'Deleted copy over data on: ' + CAST(@target_date as varchar(20))
delete from etl_3pdata_override
where record_date = @target_date
and cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print ''
print 'Copy over from ETL:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, ep.cms_product_sku_id, e3.etl_3p_title_lookup_id, e3.etl_3p_description_lookup_id, e3.etl_3p_retailer_name_lookup_id,
	e3.etl_3p_product_url_lookup_id, e3.etl_3p_logo_url_lookup_id, e3.etl_3p_condition_lookup_id, e3.etl_3p_delivery_lookup_id,
	e3.etl_3p_shipping_lookup_id, e3.price, e3.iso_currency, e3.availability, e3.rating_score, e3.rating_review, rank,
	etl_3p_retailer_url_lookup_id, is_direct_seller, e3.id, 'manual_copy_over', is_new 
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @source_date and ep.cms_product_sku_id in (
		select id from @sku_id_tbl
	)

print ''
print 'Copy over from override:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, cms_product_sku_id,	etl_3p_title_lookup_id,	etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, 'manual_copy_over', is_new
from etl_3pdata_override
where record_date = @source_date and 
	cms_product_sku_id in (
		select id from @sku_id_tbl
	)

select website, date, source, count(1) from view_3p_data
where date in (@source_date, @target_date)
and item_id in (
	select item_id from @sku_id_tbl
)
group by website, [date], source
order by website, [date], source