-- 3P UPDATE CONDITION AND IS_NEW--
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@position int,
		@condition nvarchar(255),
		@condition_id int,
		@is_new int;
declare @inserted_condition as table(condition_id int);

------------------------------------
-- Set target date
set @target_date = '' 

-- Set target item_id
set @item_id = '' 

-- Set target position
set @position = 1

-- Set condition
set @condition = 'New' 

-- Set is_new
set @is_new = 1
------------------------------------

select top 1 @sku_id = pg_id from rcms_product_skus
where item_id = @item_id

select top 1 @condition_id = id from etl_3p_condition_lookup
where hash_value = HASHBYTES('MD5', @condition)

if @condition_id is null
begin 	
	print char(13) + 'Inserted new condition name:'
	insert into etl_3p_condition_lookup(condition)
	output inserted.id into @inserted_condition
	values(@condition)

	select @condition_id = condition_id from @inserted_condition
end

print char(13) + 'Updated ETL data:'
update e3 
set 
	etl_3p_condition_lookup_id = @condition_id,
	e3.is_new = @is_new
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date
and ep.cms_product_sku_id = @sku_id
and e3.[rank] = @position

print char(13) + 'Updated copy over data:'
update etl_3pdata_override 
set 
	etl_3p_condition_lookup_id = @condition_id,
	is_new = @is_new
where record_date = @target_date
and cms_product_sku_id = @sku_id
and [rank] = @position

print char(13) + 'Result:'
select top 1 [date], item_id, position, condition, is_new from view_3p_data
where [date] = @target_date
and item_id = @item_id
and position = @position