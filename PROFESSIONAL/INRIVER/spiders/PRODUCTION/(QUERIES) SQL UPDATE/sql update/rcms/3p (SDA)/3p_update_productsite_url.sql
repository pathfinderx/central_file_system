-- 3P UPDATE PRODUCTSITE URL --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@position int,
		@productsite_url nvarchar(max),
		@productsite_url_id int;
declare @inserted_productsite_url as table(productsite_url_id int);

------------------------------------
-- Set target date
set @target_date = '2022-02-17' 

-- Set target item_id
set @item_id = '200NL6D070000000551020201904050621' 

-- Set target position
set @position = 7

-- Set productsite_url
set @productsite_url = N'https://www.bol.com/nl/nl/p/marshall-mode-eq-in-ear-oordopjes-zwart/9200000035236903/?Referrer=ADVNLPPcef0e300cdbf9297001c603d3e001085347&utm_source=1085347&utm_medium=Affiliates&utm_campaign=CPS&utm_content=txl'
------------------------------------

select top 1 @sku_id = pg_id from rcms_product_skus
where item_id = @item_id

select top 1 @productsite_url_id = id from etl_3p_product_url_lookup
where hash_value = HASHBYTES('MD5', @productsite_url)

if @productsite_url_id is null
begin 	
	print char(13) + 'Inserted new productsite url:'
	insert into etl_3p_product_url_lookup(product_url)
	output inserted.id into @inserted_productsite_url
	values(@productsite_url)

	select @productsite_url_id = productsite_url_id from @inserted_productsite_url
end

print char(13) + 'Updated ETL data:'
update e3 set etl_3p_product_url_lookup_id = @productsite_url_id
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date
and ep.cms_product_sku_id = @sku_id
and e3.[rank] = @position

print char(13) + 'Updated copy over data:'
update etl_3pdata_override set etl_3p_product_url_lookup_id = @productsite_url_id
where record_date = @target_date
and cms_product_sku_id = @sku_id
and [rank] = @position

print char(13) + 'Result:'
select top 1 [date], item_id, position, productsite_url from view_3p_data
where [date] = @target_date
and item_id = @item_id
and position = @position