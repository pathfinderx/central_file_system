-- 3P COPY OVER BY DATE + ITEM_ID --
declare @target_date date,
		@source_date date;
declare @sku_id_tbl as table(id int, item_id VARCHAR(100));

------------------------------------
-- Set source date
set @source_date = ''  

-- Set target date
set @target_date = '' 

-- Set target item_ids
insert into @sku_id_tbl(id, item_id) 
select pg_id, item_id from rcms_product_skus
where item_id in (
'200FRMS070000001382730202104150735',
'200FRMS070000001398230202105040415',
'200FRMS0NC000001382760202104150735',
'200FRMS070000001382780202104150735',
'200FRMS070000001220880202010261317',
'200FRMS070000001220960202010261317',
'200FRMS070000001398360202105040415',
'200FRMS080000001382870202104150735',
'200FRMS080000001382910202104150735'
)
------------------------------------

print ''
print 'Deleted ETL data on: ' + CAST(@target_date as varchar(20))
delete e3 from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @target_date
and ep.cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print ''
print 'Deleted copy over data on: ' + CAST(@target_date as varchar(20))
delete from etl_3pdata_override
where record_date = @target_date
and cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print ''
print 'Copy over from ETL:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, ep.cms_product_sku_id, e3.etl_3p_title_lookup_id, e3.etl_3p_description_lookup_id, e3.etl_3p_retailer_name_lookup_id,
	e3.etl_3p_product_url_lookup_id, e3.etl_3p_logo_url_lookup_id, e3.etl_3p_condition_lookup_id, e3.etl_3p_delivery_lookup_id,
	e3.etl_3p_shipping_lookup_id, e3.price, e3.iso_currency, e3.availability, e3.rating_score, e3.rating_review, rank,
	etl_3p_retailer_url_lookup_id, is_direct_seller, e3.id, 'manual_copy_over', is_new 
from etl_3pdata e3
join etl_productdata ep on e3.etl_productdata_id = ep.id
where record_date = @source_date and ep.cms_product_sku_id in (
		select id from @sku_id_tbl
	)

print ''
print 'Copy over from override:'
insert into etl_3pdata_override(record_date, cms_product_sku_id, etl_3p_title_lookup_id, etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, copy_type, is_new)
select 
	@target_date, cms_product_sku_id,	etl_3p_title_lookup_id,	etl_3p_description_lookup_id,
	etl_3p_retailer_name_lookup_id,	etl_3p_product_url_lookup_id, etl_3p_logo_url_lookup_id, etl_3p_condition_lookup_id,
	etl_3p_delivery_lookup_id, etl_3p_shipping_lookup_id, price, iso_currency, availability, rating_score, rating_review, 
	rank, etl_3p_retailer_url_lookup_id, is_direct_seller, reference_id, 'manual_copy_over', is_new
from etl_3pdata_override
where record_date = @source_date and 
	cms_product_sku_id in (
		select id from @sku_id_tbl
	)
