-- 3P UPDATE PRICE --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@position int,
		@price money;

------------------------------------
-- Set target date
set @target_date = '2022-04-13' 

-- Set target item_id
set @item_id = '200FREF070000000556870201904220850' 

-- Set target position
set @position = 1

-- Set retailer_name
set @price = 0
------------------------------------

update p set p.price = @price
from etl_3pdata p
join etl_productdata e on p.etl_productdata_id = e.id
join rcms_product_skus s on e.cms_product_sku_id = s.pg_id 
where e.record_date = @target_date and 
	s.item_id = @item_id and
	p.[rank] = @position

update e3o set e3o.price = @price
from etl_3pdata_override e3o
join rcms_product_skus s on e3o.cms_product_sku_id = s.pg_id 
where e3o.record_date = @target_date
and s.item_id = @item_id
and [rank] = @position