-- NOTE:
--	1. From the given physical store document, get the necessary columns(location, comment, item_id, stock_status, visibility if existing)
--	2. Save the document as .csv file
--	3. Insert .csv file as flat file in the target database
--	4. Execute the queries below

declare @location_tbl as table([location] nvarchar(max));
declare @comment_tbl as table(comment nvarchar(max));
declare @temp_tbl as table([location] nvarchar(max), comment nvarchar(max), 
	item_id varchar(255), stock_status smallint, visibility bit)

-- Set target date
declare @target_date date = '2021-07-23'

-- Check selected columns if matched with inserted table
insert into @temp_tbl([location], comment, item_id, stock_status, visibility)
select 
	[Location], -- location
	external_reason, -- external_reason/comment
	item_id, -- item_id
	Stock_status, -- stock_status/availability
	Visibility -- visibility, 0 if not existing
from physical_store_100_20210723 -- Set inserted table name

insert into @location_tbl([location])
select [location] from @temp_tbl
group by [location]

insert into @comment_tbl(comment)
select comment from @temp_tbl
group by comment

merge etl_physicalstoresdata_name_lookup a using @location_tbl b
on a.hash_value = HASHBYTES('MD5', b.[location])
when not matched then
	insert ([name])
	values (b.[location]);

merge etl_physicalstoresdata_external_reason_lookup a using @comment_tbl b
on a.hash_value = HASHBYTES('MD5', b.comment)
when not matched then
	insert (external_reason)
	values (b.comment);

print char(13) + 'Number of inserted physical store data:'
insert into etl_physicalstoresdata(
	cms_product_skus_id,
	record_date,
	availability,
	visibility,
	etl_physicalstoresdata_name_lookup_id,
	etl_physicalstoresdata_external_reason_lookup_id
)
select 
	c.pg_id,
	@target_date,
	p.stock_status,
	p.visibility,
	e.id,
	b.id
from @temp_tbl p
join rcms_product_skus c on p.item_id = c.item_id
join etl_physicalstoresdata_name_lookup e on e.hash_value = HASHBYTES('MD5', p.[location])
join etl_physicalstoresdata_external_reason_lookup b on b.hash_value = HASHBYTES('MD5', p.[comment]) 