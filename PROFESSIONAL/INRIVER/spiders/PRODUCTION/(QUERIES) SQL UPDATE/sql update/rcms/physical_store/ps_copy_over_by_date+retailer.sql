-- PHYSICAL STORE: COPY OVER BY DATE + RETAILER --
DECLARE @source_date DATE, @target_date DATE;
DECLARE @retailer_id_tbl AS TABLE(retailer_id INT);
DECLARE @dummy_etl_data AS TABLE(dummy_data VARCHAR(255));

----------------------------------------------------
-- Set source date
SET @source_date = '2021-09-17'

-- Set target date
SET @target_date = '2021-09-16'

-- Set target retailers with www. prefix
INSERT INTO @retailer_id_tbl(retailer_id)
SELECT pg_id from rcms_websites
WHERE website_url IN (
'www.elgiganten.dk'
)
----------------------------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE p FROM backdoor_etl_physicalstoresdata p
JOIN cms_product_skus s ON p.cms_product_skus_id = s.pg_id
WHERE record_date = @target_date
AND s.website_id IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Inserted ETL dummy data:'
INSERT INTO @dummy_etl_data(dummy_data)
SELECT CONCAT(cms_product_skus_id,',',etl_physicalstoresdata_name_lookup_id) 
FROM etl_physicalstoresdata p 
JOIN cms_product_skus s ON p.cms_product_skus_id = s.pg_id
WHERE record_date = @target_date AND
	s.website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

INSERT INTO etl_physicalstoresdata(cms_product_skus_id, record_date, etl_physicalstoresdata_name_lookup_id)
SELECT cms_product_skus_id, @target_date, etl_physicalstoresdata_name_lookup_id 
FROM etl_physicalstoresdata p 
JOIN cms_product_skus s ON p.cms_product_skus_id = s.pg_id
WHERE record_date = @source_date AND
	s.website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	) AND
	CONCAT(cms_product_skus_id,',',etl_physicalstoresdata_name_lookup_id) NOT IN (
		SELECT dummy_data FROM @dummy_etl_data
	)

	
INSERT INTO backdoor_etl_physicalstoresdata(
	record_date, cms_product_skus_id, availability, etl_physicalstoresdata_name_lookup_id, 
	etl_physicalstoresdata_external_reason_lookup_id, etl_physicalstoresdata_internal_reason_lookup_id, visibility, source)
SELECT 
	@target_date, sku_id, stock_status, location_id, external_reason_id, internal_reason_id, visibility, 'be_copy_override'
FROM view_physicalstoresdata
WHERE [date] = @source_date AND
	website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

SELECT website, [date], source, count(1) FROM view_physicalstoresdata
WHERE DATE IN (@source_date, @target_date) AND
	website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)
GROUP BY website, [date], source
ORDER BY website, [date], source

