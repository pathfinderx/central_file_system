-- RANKINGS: DELETE ALL BY DATE + RETAILER + KEYWORD --
DECLARE @target_date DATE, @website_id INT;
DECLARE @keyword_id_tbl AS TABLE(keyword_id INT);

----------------------------------
-- Set target date
SET @target_date = '2021-07-21'

-- Set website URL with www. prefix
SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = 'www.mediamarkt.nl'

-- Set keyword, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords
WHERE [name] Collate SQL_Latin1_General_CP850_BIN2
IN (
N'samsung'
)
----------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date
	AND cms_website_id = @website_id
	AND cms_keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date
	AND y.cms_websites_pgid = @website_id
	AND y.cms_keywords_pgid IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE CAST(t.recorded AS DATE) = @target_date
	AND y.cms_websites_pgid = @website_id
	AND y.cms_keywords_pgid IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead links:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date
	AND cms_websites_id = @website_id
	AND cms_keywords_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

SELECT [date], website, keyword, [source], COUNT(1) FROM view_all_rankingsdata
WHERE [date] = @target_date
	AND website_id = @website_id
	AND keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)
GROUP BY [date], website, keyword, [source]

