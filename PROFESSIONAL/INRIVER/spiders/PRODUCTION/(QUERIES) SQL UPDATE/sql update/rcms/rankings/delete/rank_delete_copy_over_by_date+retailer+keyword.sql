-- RANKINGS: DELETE COPY OVER BY DATE + RETAILER + KEYWORD --
DECLARE @target_date DATE, @website_id INT;
DECLARE @keyword_id_tbl AS TABLE(keyword_id INT);

----------------------------------
-- Set target date
SET @target_date = '2022-03-02'

-- Set website URL with www. prefix
SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = 'www.pcdiga.com'

-- Set keyword, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords
WHERE [name] Collate SQL_Latin1_General_CP850_BIN2
IN (
N'Teclado mecanico'
)
----------------------------------

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date
	AND y.cms_websites_pgid = @website_id
	AND y.cms_keywords_pgid IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

SELECT [date], website, keyword, [source], COUNT(1) FROM view_all_rankingsdata
WHERE [date] = @target_date
	AND website_id = @website_id
	AND keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)
GROUP BY [date], website, keyword, [source]



