-- RANKINGS: DELETE ALL BY DATE + RETAILER + KEYWORD + RANK --
DECLARE @target_date DATE, @website_id INT, @keyword_id INT, @rank SMALLINT;

----------------------------------
-- Set target date
SET @target_date = '2021-07-21'

-- Set website URL with www. prefix
SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = 'www.mediamarkt.nl'

-- Set keyword, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
SELECT TOP 1 @keyword_id = pg_id FROM rcms_keywords
WHERE [name] = N'samsung' Collate SQL_Latin1_General_CP850_BIN2

-- Set rank (included in deletion)
SET @rank = 23

----------------------------------

IF @keyword_id IS NOT NULL
BEGIN
	PRINT CHAR(13) + 'Deleted backdoor data:'
	DELETE FROM backdoor_etl_rankingsdata
	WHERE record_date = @target_date
		AND cms_website_id = @website_id
		AND cms_keyword_id = @keyword_id
		AND product_rank >= @rank

	PRINT CHAR(13) + 'Deleted copy over data:'
	DELETE o FROM etl_rankingsdata_override o
	JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
	WHERE recorded = @target_date
		AND y.cms_websites_pgid = @website_id
		AND y.cms_keywords_pgid = @keyword_id
		AND o.product_rank >= @rank

	PRINT CHAR(13) + 'Deleted ETL:'
	DELETE e FROM etl_rankings_yoke_elements e
	JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
	JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
	WHERE CAST(t.recorded AS DATE) = @target_date
		AND y.cms_websites_pgid = @website_id
		AND y.cms_keywords_pgid = @keyword_id
		AND e.product_rank >= @rank

	SELECT [date], website, keyword, [rank] FROM view_all_rankingsdata
	WHERE [date] = @target_date
		AND website_id = @website_id
		AND keyword_id = @keyword_id
	order by [rank]
END
ELSE
	print 'No keyword selected. Try commenting out Collate SQL_Latin1_General_CP850_BIN2 or check given keyword/s.'
