-- RANKINGS: PUBLISH/UNPUBLISH BY DATE + RETAILER + KEYWORD --
DECLARE @target_date DATE, @published BIT, @website_id INT;
DECLARE @keyword_id_tbl as table(keyword_id int);

--------------------------------------------------------------------
-- Set target date(YYYY-MM-DD)
SET @target_date = '2021-07-21'

-- Set published value(1=published, 0=unpublished)
SET @published = 1

-- Set target retailer with www. prefix
SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = 'www.mediamarkt.be/nl/'

-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
PRINT CHAR(13) + 'Number of selected keywords:'
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords
WHERE [name] Collate SQL_Latin1_General_CP850_BIN2 
in (
N'Samsung S21'
)
---------------------------------------------------------------------

PRINT CHAR(13) + 'Deleted current published data:'
DELETE FROM publish_rankingsdata
WHERE record_date = @target_date
	AND cms_website_id = @website_id
	AND cms_keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Inserted new published data:'
INSERT INTO publish_rankingsdata(record_date, cms_website_id, cms_keyword_id, published)
SELECT @target_date, @website_id, keyword_id, @published
FROM @keyword_id_tbl

SELECT [date], website, keyword, published, count(1) from view_all_rankingsdata
WHERE [date] = @target_date AND 
	website_id = @website_id AND 
	keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)
GROUP BY [date], website, keyword, published
