-- RANKINGS: SET TO DEAD LINK BY DATE + RETAILER --
DECLARE @target_date DATE,
		@is_confirmed BIT = 1;
DECLARE @retailer_id_tbl AS TABLE(retailer_id int);
DECLARE @keyword_retailer_id_tbl AS TABLE(retailer_id int, keyword_id int);

-------------------------------------------------------------
-- Set target date
SET @target_date = '2021-07-21' 

-- Set target website URL with 'www.' prefix
PRINT CHAR(13) + 'Number of selected retailers:'
INSERT INTO @retailer_id_tbl(retailer_id)
SELECT pg_id FROM rcms_websites
WHERE website_url
in ( 
'www.bcc.nl'
)
------------------------------------------------------------

PRINT CHAR(13) + 'Number of fetched keyword+retailer pairs:'
INSERT INTO @keyword_retailer_id_tbl(retailer_id, keyword_id)
SELECT website_id, keyword_id FROM view_all_rankingsdata
WHERE [date] = @target_date AND
	website_id  IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)
GROUP BY website_id, keyword_id

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date AND
	cms_website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL data:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE CAST(t.recorded AS DATE) = @target_date AND
	y.cms_websites_pgid IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date AND
	y.cms_websites_pgid IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead link data:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date AND
	cms_websites_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Inserted dead link data:'
INSERT INTO rankingsdata_dead_links(record_date, cms_websites_id, cms_keywords_id, is_confirmed)
SELECT 
	@target_date, retailer_id, keyword_id, @is_confirmed
FROM @keyword_retailer_id_tbl

SELECT
	[date], website, keyword, source 
FROM view_all_rankingsdata
WHERE [date] = @target_date AND
	website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

