-- RANKINGS: PUBLISH/UNPUBLISH BY DATE + RETAILER --
DECLARE @target_date DATE, @published BIT;
DECLARE @source_tbl AS TABLE(website_id INT, keyword_id INT);

--------------------------------------------------------------------
-- Set target date(YYYY-MM-DD)
SET @target_date = '2021-07-21'

-- Set published value(1=published, 0=unpublished)
SET @published = 1

---------------------------------------------------------------------

PRINT CHAR(13) + 'Number of fetched keyword+retailer:'
INSERT INTO @source_tbl(website_id, keyword_id)
SELECT website_id, keyword_id FROM view_all_rankingsdata
WHERE [date] = @target_date
GROUP BY website_id, keyword_id

PRINT CHAR(13) + 'Deleted current published data:'
DELETE FROM publish_rankingsdata
WHERE record_date = @target_date

PRINT CHAR(13) + 'Inserted new published data:'
INSERT INTO publish_rankingsdata(record_date, cms_website_id, cms_keyword_id, published)
SELECT @target_date, website_id, keyword_id, @published
FROM @source_tbl

SELECT [date], website, keyword, published, count(1) from view_all_rankingsdata
WHERE [date] = @target_date
GROUP BY [date], website, keyword, published
