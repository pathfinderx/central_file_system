-- RANKINGS: COPY OVER BY DATE + RETAILER--
DECLARE @source_date DATE, @target_date DATE;
DECLARE @retailer_id_tbl AS TABLE(retailer_id INT);

----------------------------------
-- Set source date
SET @source_date = '2021-07-20' 

-- Set target date
SET @target_date = '2021-07-21'

-- Set target retailer URLs with 'www.' prefix
INSERT INTO @retailer_id_tbl(retailer_id)
SELECT pg_id FROM rcms_websites
WHERE website_url IN (
'www.coolblue.nl'
)
----------------------------------

PRINT CHAR(13) + 'Deleted rankings dead link:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date
AND cms_websites_id IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date
AND y.cms_websites_pgid IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings ETL data:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE cast(t.recorded as date) = @target_date
AND y.cms_websites_pgid IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date
AND cms_website_id IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Copied over data from dead link:'
INSERT INTO rankingsdata_dead_links(
	record_date, cms_keywords_id, cms_websites_id, is_confirmed
)
SELECT 
	@target_date, cms_keywords_id, cms_websites_id, is_confirmed
FROM rankingsdata_dead_links
WHERE record_date = @source_date
AND cms_websites_id IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Copied over data from override:'
INSERT INTO dbo.etl_rankingsdata_override(
	recorded, etl_rankings_yoke_id,	published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id,	element_id,	copy_type
)
SELECT
	@target_date, etl_rankings_yoke_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id, element_id, 'manual_copy_over'
FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @source_date
AND y.cms_websites_pgid IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Copied over data from ETL:'
INSERT INTO dbo.etl_rankingsdata_override(
	recorded, etl_rankings_yoke_id,	published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id, element_id, copy_type
)
SELECT
	@target_date, y.id, t.published, e.rankings_title_atheneum,
	e.rankings_url_atheneum, e.product_rank, t.id, e.id, 'manual_copy_over'
FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE cast(t.recorded as date) = @source_date
AND y.cms_websites_pgid IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Copied over data from backdoor:'
INSERT INTO dbo.backdoor_etl_rankingsdata(
	record_date, cms_keyword_id, cms_website_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, source
)
SELECT
	@target_date, cms_keyword_id, cms_website_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, 'be_copy_override'
FROM backdoor_etl_rankingsdata 
WHERE record_date = @source_date
AND cms_website_id IN (
	SELECT retailer_id FROM @retailer_id_tbl
)

PRINT CHAR(13) + 'Selected data:'
select website, date, source, count(1) from view_all_rankingsdata
where date in (@source_date, @target_date)
AND website_id in (
	SELECT retailer_id FROM @retailer_id_tbl
)	
group by date, website, source
order by website, date, source