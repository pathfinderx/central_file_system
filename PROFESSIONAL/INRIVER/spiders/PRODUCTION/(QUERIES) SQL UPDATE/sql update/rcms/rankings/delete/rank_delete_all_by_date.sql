-- RANKINGS: DELETE ALL BY DATE --
DECLARE @target_date DATE;

----------------------------------
-- Set target date
SET @target_date = '2021-07-21'

----------------------------------

PRINT CHAR(13) + 'Deleted published data:'
DELETE FROM publish_rankingsdata
WHERE record_date = @target_date

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE FROM etl_rankingsdata_override
WHERE recorded = @target_date

PRINT CHAR(13) + 'Deleted dead link:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date

PRINT CHAR(13) + 'Deleted ETL:'
DELETE rye FROM etl_rankings_yoke_elements rye
JOIN etl_rankings_yoke_thread ryt ON ryt.id = rye.etl_rankings_yoke_thread_id
WHERE CAST(ryt.recorded AS DATE) = @target_date

DELETE FROM etl_rankings_yoke_thread
WHERE cast(recorded as date) = @target_date

SELECT COUNT(1) FROM view_all_rankingsdata
WHERE [date] = @target_date