-- RANKINGS: DELETE ALL BY DATE + RETAILER --
DECLARE @target_date DATE;
DECLARE @retailer_id_tbl AS TABLE(retailer_id INT);

----------------------------------
-- Set target date
SET @target_date = '2021-07-21'

-- Set website URL with www. prefix
INSERT INTO @retailer_id_tbl(retailer_id)
SELECT pg_id FROM rcms_websites
WHERE website_url IN (
'www.mediamarkt.nl'
)
----------------------------------

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date
	AND cms_website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date
	AND y.cms_websites_pgid IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE CAST(t.recorded AS DATE) = @target_date
	AND y.cms_websites_pgid IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead links:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date
	AND cms_websites_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)

SELECT [date], website, [source], COUNT(1) FROM view_all_rankingsdata
WHERE [date] = @target_date
	AND website_id IN (
		SELECT retailer_id FROM @retailer_id_tbl
	)
GROUP BY [date], website, [source]

