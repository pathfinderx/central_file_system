-- RANKINGS: SET TO DEAD LINK BY DATE + RETAILER + KEYWORD --
DECLARE @target_date DATE,
		@website_url VARCHAR(255),
		@website_id INT,
		@is_confirmed BIT = 1;
DECLARE @keyword_id_tbl AS TABLE(keyword_id int);

-------------------------------------------------------------
-- Set target date
SET @target_date = '2022-08-13' 

-- Set target website URL with 'www.' prefix
SET @website_url = 'www.mediamarkt.nl'

-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
PRINT CHAR(13) + 'Number of selected keywords:'
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords
WHERE [name] Collate SQL_Latin1_General_CP850_BIN2 
in ( 
N'spelcomputer'
)
------------------------------------------------------------

SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = @website_url

PRINT CHAR(13) + 'Deleted backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date AND
	cms_website_id = @website_id AND
	cms_keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted ETL data:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE CAST(t.recorded AS DATE) = @target_date AND
	y.cms_websites_pgid = @website_id AND
	y.cms_keywords_pgid IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date AND
	y.cms_websites_pgid = @website_id AND
	y.cms_keywords_pgid IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Deleted dead link data:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date AND
	cms_websites_id = @website_id AND
	cms_keywords_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

PRINT CHAR(13) + 'Inserted dead link data:'
INSERT INTO rankingsdata_dead_links(record_date, cms_websites_id, cms_keywords_id, is_confirmed)
SELECT 
	@target_date, @website_id, keyword_id, @is_confirmed
FROM @keyword_id_tbl

SELECT
	[date], website, keyword, source 
FROM view_all_rankingsdata
WHERE [date] = @target_date AND
	website_id = @website_id AND
	keyword_id in (
		select keyword_id from @keyword_id_tbl
	)

