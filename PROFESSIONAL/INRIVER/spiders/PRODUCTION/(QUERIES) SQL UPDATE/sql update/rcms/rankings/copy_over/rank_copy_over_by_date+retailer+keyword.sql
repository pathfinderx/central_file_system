-- RANKINGS: COPY OVER BY DATE + RETAILER + KEYWORD --
DECLARE @source_date DATE, @target_date DATE, @retailer_id INT;
DECLARE @keyword_id_tbl AS TABLE(keyword_id INT);

----------------------------------------------------------------------------------------
-- Set source date
SET @source_date = '2021-07-20' 

-- Set target date
SET @target_date = '2021-07-21'

-- Set target retailer URLs with 'www.' prefix
SELECT TOP 1 @retailer_id = pg_id FROM rcms_websites
WHERE website_url = 'www.coolblue.nl'

-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords 
WHERE [name] COLLATE SQL_Latin1_General_CP850_BIN2 
IN (
N'Samsung Galaxy', 
N'samsung'
)
----------------------------------------------------------------------------------------

PRINT CHAR(13) + 'Deleted rankings dead link:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date
AND cms_websites_id = @retailer_id
AND cms_keywords_id IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings copy over data:'
DELETE o FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @target_date
AND y.cms_websites_pgid = @retailer_id
AND y.cms_keywords_pgid IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings ETL data:'
DELETE e FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE cast(t.recorded as date) = @target_date
AND y.cms_websites_pgid = @retailer_id
AND y.cms_keywords_pgid IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Deleted rankings backdoor data:'
DELETE FROM backdoor_etl_rankingsdata
WHERE record_date = @target_date
AND cms_website_id = @retailer_id
AND cms_keyword_id IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Copied over data from dead link:'
INSERT INTO rankingsdata_dead_links(
	record_date, cms_keywords_id, cms_websites_id, is_confirmed
)
SELECT 
	@target_date, cms_keywords_id, cms_websites_id, is_confirmed
FROM rankingsdata_dead_links
WHERE record_date = @source_date
AND cms_websites_id = @retailer_id
AND cms_keywords_id IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Copied over data from override:'
INSERT INTO dbo.etl_rankingsdata_override(
	recorded, etl_rankings_yoke_id,	published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id,	element_id,	copy_type
)
SELECT
	@target_date, etl_rankings_yoke_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id, element_id, 'manual_copy_over'
FROM etl_rankingsdata_override o
JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
WHERE recorded = @source_date
AND y.cms_websites_pgid = @retailer_id
AND y.cms_keywords_pgid IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Copied over data from ETL:'
INSERT INTO dbo.etl_rankingsdata_override(
	recorded, etl_rankings_yoke_id,	published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, thread_id, element_id, copy_type
)
SELECT
	@target_date, y.id, t.published, e.rankings_title_atheneum,
	e.rankings_url_atheneum, e.product_rank, t.id, e.id, 'manual_copy_over'
FROM etl_rankings_yoke_elements e
JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
WHERE cast(t.recorded as date) = @source_date
AND y.cms_websites_pgid = @retailer_id
AND y.cms_keywords_pgid IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Copied over data from backdoor:'
INSERT INTO dbo.backdoor_etl_rankingsdata(
	record_date, cms_keyword_id, cms_website_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, source
)
SELECT
	@target_date, cms_keyword_id, cms_website_id, published, rankings_title_atheneum,
	rankings_url_atheneum, product_rank, 'be_copy_override'
FROM backdoor_etl_rankingsdata 
WHERE record_date = @source_date
AND cms_website_id = @retailer_id
AND cms_keyword_id IN (
	SELECT keyword_id FROM @keyword_id_tbl
)

PRINT CHAR(13) + 'Selected data:'
select website, keyword, date, source, count(1) from view_all_rankingsdata
where date in (@source_date, @target_date)
AND website_id = @retailer_id
AND keyword_id IN (
	SELECT keyword_id FROM @keyword_id_tbl
)	
group by date, website, keyword, source
order by website, keyword, date, source