-- RANKINGS: DELETE DEAD LINK BY DATE + RETAILER + KEYWORD--
DECLARE @target_date DATE, @website_id INT;
DECLARE @keyword_id_tbl AS TABLE(keyword_id INT);

----------------------------------
-- Set target date
SET @target_date = '2022-01-04'

-- Set website URL with www. prefix
SELECT TOP 1 @website_id = pg_id FROM rcms_websites
WHERE website_url = 'www.elgiganten.dk'

-- Set keyword, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
INSERT INTO @keyword_id_tbl(keyword_id)
SELECT pg_id FROM rcms_keywords
WHERE [name] Collate SQL_Latin1_General_CP850_BIN2
IN (
N'motorola',
N'Motorola E',
N'motorola edge',
N'motorola G',
N'OLED display',
N'Snapdragon',
N'Motorola Edge 20',
N'Motorola E7',
N'Motorola G30'
)
----------------------------------

PRINT CHAR(13) + 'Deleted dead links:'
DELETE FROM rankingsdata_dead_links
WHERE record_date = @target_date
	AND cms_websites_id = @website_id
	AND cms_keywords_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)

SELECT [date], website, keyword, [source], COUNT(1) FROM view_all_rankingsdata
WHERE [date] = @target_date
	AND website_id = @website_id
	AND keyword_id IN (
		SELECT keyword_id FROM @keyword_id_tbl
	)
GROUP BY [date], website, keyword, [source]

