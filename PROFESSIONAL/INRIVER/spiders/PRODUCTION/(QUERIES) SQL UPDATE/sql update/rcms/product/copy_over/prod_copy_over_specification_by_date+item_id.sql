-- PRODUCT: COPY OVERRIDE SPECIFICATIONS BY DATE+ITEM_ID --
declare @source_date date,
		@target_date date,
		@source varchar(50);
declare @sku_tbl as table(id int, item_id varchar(255));
declare @source_tbl as table(record_date date, sku_id int, specifications_id int);

-- Set source date for copy over
set @source_date = '';

-- Set target date to copy over
set @target_date = '';

insert into @sku_tbl(id, item_id)
select pg_id, item_id from rcms_product_skus
where item_id in ( -- Set item_id for copy over
'100NOZ20ZQ020220124094332137825024'
)

insert into @source_tbl(record_date,sku_id,specifications_id)
select 	
	@target_date,
	coalesce(bd.id, be.id) as sku_id,
	coalesce(bd.productdata_specifications_atheneum, be.productdata_specifications_atheneum) as specifications_id
from (
	select st.id, ep.productdata_specifications_atheneum
	from etl_productdata ep
	join @sku_tbl st on ep.cms_product_sku_id = st.id
	where ep.record_date = @source_date
	union 
	select st.id, epo.productdata_specifications_atheneum
	from etl_productdata_override epo
	join @sku_tbl st on epo.cms_product_sku_id = st.id
	where epo.record_date = @source_date
)be 
full outer join (
	select 
		bep.cms_product_sku_id as id, 
		specs_id as productdata_specifications_atheneum
	from backdoor_etl_productdata bep
	join @sku_tbl st on bep.cms_product_sku_id = st.id
	where bep.record_date = @source_date
)bd on be.id = bd.id

print char(13) + 'Copy over on backdoor:'
update t set t.specs_id = s.specifications_id, t.source = 'be_copy_override', updatedate = GETDATE()
from backdoor_etl_productdata t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

print char(13) + 'Copy over on override:'
update t set t.productdata_specifications_atheneum = s.specifications_id
from etl_productdata_override t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

print char(13) + 'Copy over on ETL:'
update t set t.productdata_specifications_atheneum = s.specifications_id
from etl_productdata t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

select item_id, date, specification_id from view_all_productdata
where date in (@source_date, @target_date)
and fk_sku_pgid in (
	select id from @sku_tbl
)
order by item_id, date
