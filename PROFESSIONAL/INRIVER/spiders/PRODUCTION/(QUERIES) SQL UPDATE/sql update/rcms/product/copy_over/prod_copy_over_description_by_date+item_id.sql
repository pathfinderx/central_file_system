-- PRODUCT: COPY OVERRIDE DESCRIPTION BY DATE+ITEM_ID --
declare @source_date date,
		@target_date date,
		@source varchar(50);
declare @sku_tbl as table(id int);
declare @source_tbl as table(record_date date, sku_id int, description_id int);

-- Set source date for copy over
set @source_date = '';

-- Set target date to copy over
set @target_date = '';

insert into @sku_tbl(id)
select pg_id from rcms_product_skus
where item_id in ( -- Set item_id for copy over
'200FR21070000000030150201901180326',
'200FR21070000000033010201901180326',
'200FR21070000000034040201901180329',
'200FR21070000000034640201901180329',
'200FR21070000000038120201901180330',
'200FR21070000000040930201901180332',
'200FR21070000000702560201905280400',
'200FR21070000000702600201905280501',
'200FR21070000000929200202003300941',
'200FR21070000000956130202005260649',
'200FR21070000000994110202007310525',
'200FR21070000001245780202011200116',
'200FR21070000001380650202104140352',
'200FR21070000001384450202104200133',
'200FR21070000001384510202104200133',
'200FR21070000001384570202104200133',
'200FR21070000001384630202104200133',
'200FR21070000001474500202107060614',
'200FR21070000001474550202107060614',
'200FR21070000001474640202107060614',
'200FR21070000001474710202107060614',
'200FR2107R020220711022356114870192',
'200FR2107R020220711022356627728192',
'200FR2107R020220711022357144550192',
'200FR2107R020220711022357521267192',
'200FR2107R020220711022357994604192',
'200FR2108R020220711022358619931192',
'200FR2108R020220711022359018916192',
'200FR210B0000000105640201902071110',
'200FR210C0000000105560201902071110',
'200FR210C0000000105770201902071110',
'200FR210D0000000103160201902071109',
'200FR210D0000000103460201902071109',
'200FR210D0000000103980201902071109',
'200FR210D0000000104820201902071109',
'200FR210D0000000105150201902071110',
'200FR210D0000000105440201902071110',
'200FR210D0000000107100201902071112',
'200FR210F0000000107270201902071113',
'200FR210G0000000103930201902071109',
'200FR210J0000000106450201902071112',
'200FR210J0000000106660201902071112',
'200FR210N0000000106190201902071111',
'200FR210NC000001594050202109210326',
'200FR210NC000001594290202109210326',
'200FR210O0000000106830201902071112',
'200FR210P0000000103240201902071109',
'200FR210P0000000103720201902071109',
'200FR210P0000000104380201902071109',
'200FR210P0000000104440201902071109',
'200FR210PT020220711022358284102192',
'200FR210Q0000000104100201902071109'
)

insert into @source_tbl(record_date,sku_id,description_id)
select 	
	@target_date,
	coalesce(bd.id, be.id) as sku_id,
	coalesce(bd.productdata_description_atheneum, be.productdata_description_atheneum) as description_id
from (
	select st.id, ep.productdata_description_atheneum
	from etl_productdata ep
	join @sku_tbl st on ep.cms_product_sku_id = st.id
	where ep.record_date = @source_date
	union 
	select st.id, epo.productdata_description_atheneum
	from etl_productdata_override epo
	join @sku_tbl st on epo.cms_product_sku_id = st.id
	where epo.record_date = @source_date
)be 
full outer join (
	select 
		bep.cms_product_sku_id as id, 
		description_id as productdata_description_atheneum
	from backdoor_etl_productdata bep
	join @sku_tbl st on bep.cms_product_sku_id = st.id
	where bep.record_date = @source_date
)bd on be.id = bd.id

print char(13) + 'Copy over on backdoor:'
update t set t.description_id = s.description_id, t.source = 'be_copy_override', updatedate = GETDATE()
from backdoor_etl_productdata t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

print char(13) + 'Copy over on override:'
update t set t.productdata_description_atheneum = s.description_id
from etl_productdata_override t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

print char(13) + 'Copy over on ETL:'
update t set t.productdata_description_atheneum = s.description_id
from etl_productdata t
join @source_tbl s on s.record_date = t.record_date 
	and s.sku_id = t.cms_product_sku_id

select item_id, date, description from view_all_productdata
where date in (@source_date, @target_date)
and fk_sku_pgid in (
	select id from @sku_tbl
)
order by item_id, date
