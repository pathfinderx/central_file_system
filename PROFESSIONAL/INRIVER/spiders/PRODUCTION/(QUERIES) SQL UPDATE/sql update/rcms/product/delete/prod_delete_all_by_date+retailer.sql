-- PRODUCT: DELETE ALL BY DATE+RETAILER --

declare @date date;
declare @website_id_tbl as table(id int);

-- Set target date
set @date = '2022-08-10'

-- Set website URLs
print 'Number of selected website URLs:'
insert into @website_id_tbl(id)
select pg_id from rcms_websites
where website_url in (
'www.scan.co.uk'
)

print char(13) + 'Deleted ETL:'
delete pip  from etl_productdata_image_primer pip
join etl_productdata p on p.id = pip.etl_productdata_id
join rcms_product_skus skus on p.cms_product_sku_id = skus.pg_id
where p.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

delete pvp from etl_productdata_video_primer pvp
join etl_productdata p on p.id = pvp.etl_productdata_id
join rcms_product_skus skus on p.cms_product_sku_id = skus.pg_id
where p.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE p FROM post_webshotdata_performance p
JOIN post_webshotdata w ON p.post_webshotdata_id = w.id
JOIN etl_productdata e ON w.etl_productdata_id = e.id
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE w FROM post_webshotdata w
JOIN etl_productdata e ON w.etl_productdata_id = e.id
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

if DB_NAME(db_id()) in ('A00','200')
begin
	DELETE tp FROM etl_3pdata tp
	JOIN etl_productdata e ON tp.etl_productdata_id = e.id
	join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
	where e.record_date = @date and
		skus.website_id in (select id from @website_id_tbl)

	DELETE bb FROM etl_buyboxdata bb
	JOIN etl_productdata e ON bb.etl_productdata_id = e.id
	join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
	where e.record_date = @date and
		skus.website_id in (select id from @website_id_tbl)
end

delete e from etl_productdata e
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

print char(13) + 'Deleted copy over:'
DELETE w FROM post_webshotdata_override w
JOIN etl_productdata_override e ON w.etl_productdata_override_id = e.id
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE i FROM productdata_image_url_override i
JOIN etl_productdata_override e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE v FROM productdata_video_url_override v
JOIN etl_productdata_override e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE e FROM etl_productdata_override e
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

print char(13) + 'Deleted dead links:'
DELETE e FROM productdata_dead_links e
join rcms_product_skus skus on e.cms_product_skus_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

print char(13) + 'Deleted backdoor:'
DELETE e FROM backdoor_etl_productdata e
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

print char(13) + 'Deleted backdoor:'
DELETE w FROM backdoor_etl_productdata_webshotdata w
JOIN backdoor_etl_productdata e ON w.backdoor_etl_productdata_id = e.id
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE i FROM backdoor_productdata_image_url i
JOIN backdoor_etl_productdata e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE v FROM backdoor_productdata_video_url v
JOIN backdoor_etl_productdata e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

DELETE e FROM backdoor_etl_productdata e
join rcms_product_skus skus on e.cms_product_sku_id = skus.pg_id
where e.record_date = @date and
	skus.website_id in (select id from @website_id_tbl)

select [date], website, source from view_all_productdata
where [date] = @date and fk_website_pgid in (select id from @website_id_tbl)