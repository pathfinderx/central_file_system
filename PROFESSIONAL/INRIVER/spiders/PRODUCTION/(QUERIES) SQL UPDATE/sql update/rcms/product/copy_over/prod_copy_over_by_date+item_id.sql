-- PRODUCT: COPY OVER BY DATE + ITEM_ID --
DECLARE @source_date DATE, @target_date DATE;
DECLARE @sku_id_tbl AS TABLE(sku_id INT);

-------------------------------------------------
-- Set target date
SET @source_date = '' 

-- Set target date
SET @target_date = '' 

-- Set target item_id/s
print 'Number of selected item_ids:'
INSERT INTO @sku_id_tbl(sku_id)
SELECT pg_id FROM rcms_product_skus
WHERE item_id in ( 
'G10USW0008120220103042125662837003',
'G10USW0008120220103042125895753003',
'G10USW0008120220103042126086717003',
'G10USW0008120220103042126268040003',
'G10USW0008120220103042126453493003',
'G10USW0008120220113091006345679013',
'G10USW0008120220113091006582679013',
'G10USW0008120220113091006775677013',
'G10USW000A220220401082351331374091',
'G10USW000A220220401082351974382091'
)
-------------------------------------------------

print char(13) + 'Deleted ETL:'
delete pip from etl_productdata_image_primer pip
join etl_productdata e on e.id = pip.etl_productdata_id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

delete pvp from etl_productdata_video_primer pvp
join etl_productdata e on e.id = pvp.etl_productdata_id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE p FROM post_webshotdata_performance p
JOIN post_webshotdata w ON p.post_webshotdata_id = w.id
JOIN etl_productdata e ON w.etl_productdata_id = e.id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE w FROM post_webshotdata w
JOIN etl_productdata e ON w.etl_productdata_id = e.id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

if DB_NAME(db_id()) in ('A00','200')
begin
	DELETE tp FROM etl_3pdata tp
	JOIN etl_productdata e ON tp.etl_productdata_id = e.id
	where e.record_date = @target_date and
		e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

	DELETE bb FROM etl_buyboxdata bb
	JOIN etl_productdata e ON bb.etl_productdata_id = e.id
	where e.record_date = @target_date and
		e.cms_product_sku_id in (select sku_id from @sku_id_tbl)
end

delete from etl_productdata
where record_date = @target_date and cms_product_sku_id in (select sku_id from @sku_id_tbl)

print char(13) + 'Deleted copy over:'
DELETE w FROM post_webshotdata_override w
JOIN etl_productdata_override e ON w.etl_productdata_override_id = e.id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE i FROM productdata_image_url_override i
JOIN etl_productdata_override e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE v FROM productdata_video_url_override v
JOIN etl_productdata_override e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE FROM etl_productdata_override
where record_date = @target_date and
	cms_product_sku_id in (select sku_id from @sku_id_tbl)

print char(13) + 'Deleted backdoor:'
DELETE w FROM backdoor_etl_productdata_webshotdata w
JOIN backdoor_etl_productdata e ON w.backdoor_etl_productdata_id = e.id
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE i FROM backdoor_productdata_image_url i
JOIN backdoor_etl_productdata e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE v FROM backdoor_productdata_video_url v
JOIN backdoor_etl_productdata e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
where e.record_date = @target_date and
	e.cms_product_sku_id in (select sku_id from @sku_id_tbl)

DELETE FROM backdoor_etl_productdata
where record_date = @target_date and
	cms_product_sku_id in (select sku_id from @sku_id_tbl)

print char(13) + 'Deleted dead links:'
delete from productdata_dead_links
where record_date = @target_date and
	cms_product_skus_id in (select sku_id from @sku_id_tbl)

print ''
print 'Copied over from backdoor data.'
insert into backdoor_etl_productdata(
	cms_product_sku_id,	record_date, title_id, description_id, specs_id, delivery_id, price_override,
	visibility_override, [availability], rating_score, rating_review, published, is_hidden_price, [source], updatedate
)
select 
	cms_product_sku_id,	@target_date, title_id,	description_id,	specs_id, delivery_id, price_override,
	visibility_override, [availability], rating_score, rating_review, published, is_hidden_price, 'be_copy_override', GETDATE()
from backdoor_etl_productdata
where record_date = @source_date and
	cms_product_sku_id in (
		select sku_id from @sku_id_tbl
	)

print ''
print 'Copied over from ETL data.'
INSERT INTO dbo.etl_productdata_override(
	cms_product_sku_id, record_date, productdata_title_atheneum_id,	productdata_video_url_atheneum, 
	productdata_image_url_atheneum, productdata_specifications_atheneum, productdata_description_atheneum, price, 
	visibility, iso_currency, [availability], rating_score, rating_review, delivery, shipping, published, 
	productdata_brand_atheneum, productdata_delivery_atheneum, productdata_shipping_atheneum, is_hidden_price, reference_id, copy_type
)
SELECT
    cms_product_sku_id, @target_date, productdata_title_atheneum_id, productdata_video_url_atheneum,
    productdata_image_url_atheneum, productdata_specifications_atheneum, productdata_description_atheneum, price,
    visibility, iso_currency, [availability], rating_score, rating_review, delivery, shipping, published,
    productdata_brand_atheneum, productdata_delivery_atheneum, productdata_shipping_atheneum, is_hidden_price, id, 'manual_copy_over'
FROM etl_productdata
WHERE record_date = @source_date and cms_product_sku_id in (
		select sku_id from @sku_id_tbl
	)

INSERT productdata_image_url_override(record_date,cms_product_skus_id,atheneum_id)
SELECT @target_date, ep.cms_product_sku_id, atheneum_id 
FROM etl_productdata_image_primer epip
JOIN etl_productdata ep ON epip.etl_productdata_id = ep.id
WHERE ep.record_date = @source_date and
	ep.cms_product_sku_id in (
		select sku_id from @sku_id_tbl
	)

INSERT productdata_video_url_override(record_date,cms_product_skus_id,atheneum_id)
SELECT @target_date, ep.cms_product_sku_id, atheneum_id 
FROM etl_productdata_video_primer evip
JOIN etl_productdata ep ON evip.etl_productdata_id = ep.id
WHERE ep.record_date = @source_date and
	ep.cms_product_sku_id in (
		select sku_id from @sku_id_tbl
	)

print ''
print 'Copied over data from override.'
INSERT INTO dbo.etl_productdata_override(
	cms_product_sku_id, record_date, productdata_title_atheneum_id,	productdata_video_url_atheneum,
	productdata_image_url_atheneum,	productdata_specifications_atheneum, productdata_description_atheneum, price, 
	visibility, iso_currency, [availability], rating_score, rating_review, delivery, shipping, published, productdata_brand_atheneum, 
	productdata_delivery_atheneum, productdata_shipping_atheneum, is_hidden_price, reference_id, copy_type
)
SELECT
    cms_product_sku_id, @target_date, productdata_title_atheneum_id, productdata_video_url_atheneum,
    productdata_image_url_atheneum, productdata_specifications_atheneum, productdata_description_atheneum, price,
    visibility, iso_currency, [availability], rating_score, rating_review, delivery, shipping, published, productdata_brand_atheneum,
    productdata_delivery_atheneum, productdata_shipping_atheneum, is_hidden_price, reference_id, 'manual_copy_over'
FROM etl_productdata_override
WHERE record_date = @source_date and cms_product_sku_id in (
		select sku_id from @sku_id_tbl
	)
INSERT INTO productdata_image_url_override(record_date,cms_product_skus_id,atheneum_id)
SELECT @target_date, cms_product_skus_id, atheneum_id FROM productdata_image_url_override
WHERE record_date = @source_date and
	cms_product_skus_id in (
		select sku_id from @sku_id_tbl
	)

INSERT INTO productdata_video_url_override(record_date,cms_product_skus_id,atheneum_id)
SELECT @target_date, cms_product_skus_id, atheneum_id FROM productdata_video_url_override
WHERE record_date = @source_date and
	cms_product_skus_id in (
		select sku_id from @sku_id_tbl
	)

print ''
print 'Copied over from dead link.'
INSERT INTO productdata_dead_links(record_date,cms_product_skus_id,is_confirmed)
SELECT @target_date, cms_product_skus_id, is_confirmed FROM productdata_dead_links
WHERE record_date = @source_date and
	cms_product_skus_id in (
		select sku_id from @sku_id_tbl
	)

select item_id, date, source from view_all_productdata
where date in (@source_date, @target_date)
and fk_sku_pgid in (
	select sku_id from @sku_id_tbl
)
order by item_id, [date], source
