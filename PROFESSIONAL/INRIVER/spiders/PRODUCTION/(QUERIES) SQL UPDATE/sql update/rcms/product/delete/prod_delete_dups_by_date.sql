-- PRODUCT: DELETE DUPLICATE DATA BY DATE --

DECLARE @dups_tbl AS TABLE(
	sku_id INT,
	source VARCHAR(255),
	master_etl_id VARCHAR(255)
);
DECLARE @etl_id_tbl AS TABLE(
	etl_id VARCHAR(255)
);
DECLARE @target_date DATE = '2022-04-29',
		@counter INT = 1,
		@row_count INT = 0,
		@sku_id INT;

INSERT INTO @dups_tbl(sku_id, source, master_etl_id)
SELECT
    fk_sku_pgid, 
    STRING_AGG(source, ',') source, 
    STRING_AGG(master_etl_id, ',') master_etl_id
FROM view_all_productdata
WHERE date = @target_date
GROUP BY fk_sku_pgid
HAVING count(1) > 1

SELECT @row_count = count(1) FROM @dups_tbl;

WHILE @counter <= @row_count
BEGIN
	SELECT TOP 1 @sku_id = sku_id FROM @dups_tbl

	INSERT INTO @etl_id_tbl(etl_id)
	SELECT 
		DISTINCT(value)
	FROM
		string_split(
			(SELECT master_etl_id FROM @dups_tbl WHERE sku_id = @sku_id),
			','
		)
		
	IF ((SELECT COUNT(1) FROM @etl_id_tbl) > 1 AND 
		'-3' IN (SELECT etl_id FROM @etl_id_tbl))
	BEGIN
		PRINT 'Deleted dead link:'
		DELETE FROM productdata_dead_links
		WHERE record_date = @target_date
		AND cms_product_skus_id = @sku_id
	END
	ELSE IF (SELECT COUNT(1) FROM @etl_id_tbl) > 1
	BEGIN
		PRINT 'Deleted copy over:'
		DELETE pwo FROM post_webshotdata_override pwo
		JOIN etl_productdata_override epo ON pwo.etl_productdata_override_id = epo.id
		WHERE epo.record_date = @target_date
		AND epo.cms_product_sku_id = @sku_id

		DELETE FROM etl_productdata_override
		WHERE record_date = @target_date
		AND cms_product_sku_id = @sku_id
	END

	PRINT CHAR(13) + 'Cleared temporary table etl_id.'
	DELETE FROM @etl_id_tbl

	PRINT CHAR(13) + 'Cleared temporary table sku_id.'
	DELETE FROM @dups_tbl
	WHERE sku_id = @sku_id
	
	SET @counter = @counter + 1
END
