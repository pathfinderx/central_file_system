-- PRODUCT: DELETE ALL BY DATE + ITEM_ID --
declare @date date;
declare @skus_id_tbl as table(id int);

-- Set target date
set @date = '2029-10-26'

-- Set target item_ids
print 'Number of selected item_ids:'
insert into @skus_id_tbl(id)
select pg_id from rcms_product_skus
where item_id in (
''
)

print char(13) + 'Deleted ETL:'
delete pip  from etl_productdata_image_primer pip
join etl_productdata e on e.id = pip.etl_productdata_id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

delete pvp from etl_productdata_video_primer pvp
join etl_productdata e on e.id = pvp.etl_productdata_id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE p FROM post_webshotdata_performance p
JOIN post_webshotdata w ON p.post_webshotdata_id = w.id
JOIN etl_productdata e ON w.etl_productdata_id = e.id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE w FROM post_webshotdata w
JOIN etl_productdata e ON w.etl_productdata_id = e.id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

if DB_NAME(db_id()) = '200'
begin
	DELETE s FROM etl_sellersdata s
	JOIN etl_productdata e ON s.etl_productdata_id = e.id
	where e.record_date = @date and
		e.cms_product_sku_id in (select id from @skus_id_tbl)
end

if DB_NAME(db_id()) in ('A00','200')
begin
	DELETE tp FROM etl_3pdata tp
	JOIN etl_productdata e ON tp.etl_productdata_id = e.id
	where e.record_date = @date and
		e.cms_product_sku_id in (select id from @skus_id_tbl)

	DELETE bb FROM etl_buyboxdata bb
	JOIN etl_productdata e ON bb.etl_productdata_id = e.id
	where e.record_date = @date and
		e.cms_product_sku_id in (select id from @skus_id_tbl)
end

delete from etl_productdata
where record_date = @date and cms_product_sku_id in (select id from @skus_id_tbl)

print char(13) + 'Deleted copy over:'
DELETE w FROM post_webshotdata_override w
JOIN etl_productdata_override e ON w.etl_productdata_override_id = e.id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE i FROM productdata_image_url_override i
JOIN etl_productdata_override e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE v FROM productdata_video_url_override v
JOIN etl_productdata_override e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE FROM etl_productdata_override
where record_date = @date and
	cms_product_sku_id in (select id from @skus_id_tbl)

print char(13) + 'Deleted dead links:'
delete from productdata_dead_links
where record_date = @date and
	cms_product_skus_id in (select id from @skus_id_tbl)

print char(13) + 'Deleted backdoor:'
DELETE w FROM backdoor_etl_productdata_webshotdata w
JOIN backdoor_etl_productdata e ON w.backdoor_etl_productdata_id = e.id
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE i FROM backdoor_productdata_image_url i
JOIN backdoor_etl_productdata e ON i.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = i.record_date
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE v FROM backdoor_productdata_video_url v
JOIN backdoor_etl_productdata e ON v.cms_product_skus_id = e.cms_product_sku_id
	and e.record_date = v.record_date
where e.record_date = @date and
	e.cms_product_sku_id in (select id from @skus_id_tbl)

DELETE FROM backdoor_etl_productdata
where record_date = @date and
	cms_product_sku_id in (select id from @skus_id_tbl)

select [date],item_id,fk_sku_pgid as sku_id,source from view_all_productdata
where [date] = @date and
	fk_sku_pgid in (select id from @skus_id_tbl)