-- BUY BOX: COPY OVER BY DATE --
declare @target_date date,
		@source_date date;
------------------------------------------
-- Set source date
set @source_date = '2021-12-16' 

-- Set target date
set @target_date = '2021-12-18' 

------------------------------------------

print char(13) + 'Deleted ETL data on:'
delete eb from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @target_date

print char(13) + 'Deleted copy over data on:'
delete from etl_buyboxdata_override
where record_date = @target_date

print char(13) + 'Copied over from ETL:'
insert into etl_buyboxdata_override(record_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, copy_type)
select 
	@target_date, ep.cms_product_sku_id, eb.etl_buyboxdata_text_lookup_id, eb.etl_buyboxdata_retailer_lookup_id, eb.etl_buyboxdata_delivery_lookup_id,
	eb.etl_buyboxdata_shipping_lookup_id, eb.etl_buyboxdata_addon_lookup_id, eb.availability, eb.price, eb.iso_currency, eb.used_price, eb.new_price,
	eb.is_cartable, eb.is_buyable, eb.id, 'manual_copy_over'
from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @source_date

print char(13) + 'Copied over from override:'
insert into etl_buyboxdata_override(record_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, copy_type)
select 
	@target_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, 'manual_copy_over'
from etl_buyboxdata_override
where record_date = @source_date

select date, source, count(1) from view_buybox
where date in (@source_date, @target_date)
group by date, source
order by date, source