-- BUYBOX: UPDATE RETAILER --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@retailer nvarchar(255),
		@retailer_id int;
declare @inserted_retailer_name as table(retailer_name_id int);

------------------------------------
-- Set target date
set @target_date = '2022-05-30' 

-- Set target item_id
set @item_id = '200USW0070000000038080201901180330' 

-- Set retailer_name
set @retailer = 'Amazon.com' 
------------------------------------

select top 1 @sku_id = pg_id from rcms_product_skus
where item_id = @item_id

select top 1 @retailer_id = id from etl_buyboxdata_retailer_lookup
where hash_value = HASHBYTES('MD5', @retailer)

if @retailer_id is null
begin 	
	print char(13) + 'Inserted new retailer name:'
	insert into etl_buyboxdata_retailer_lookup(retailer)
	output inserted.id into @inserted_retailer_name
	values(@retailer)

	select @retailer_id = retailer_name_id from @inserted_retailer_name
end

print char(13) + 'Updated ETL data:'
update eb set eb.etl_buyboxdata_retailer_lookup_id = @retailer_id
from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @target_date
and ep.cms_product_sku_id = @sku_id

print char(13) + 'Updated copy over data:'
update etl_buyboxdata_override set etl_buyboxdata_retailer_lookup_id = @retailer_id
where record_date = @target_date
and cms_product_sku_id = @sku_id

print char(13) + 'Updated backdoor data:'
update backdoor_etl_buyboxdata set etl_buyboxdata_retailer_lookup_id = @retailer_id
where record_date = @target_date
and cms_product_sku_id = @sku_id

print char(13) + 'Result:'
select [date], item_id, retailer from view_buybox
where [date] = @target_date
and item_id = @item_id