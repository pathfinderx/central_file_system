-- BUY BOX: COPY OVER BY DATE + ITEM_ID --
declare @target_date date,
		@source_date date;
declare @sku_id_tbl as table(id int, item_id varchar(50))
declare @item_id_tbl as table(item_id varchar(100))

------------------------------------------
-- Set source date
set @source_date = '2021-10-07' 

-- Set target date
set @target_date = '2021-10-10' 

-- Set target item_ids
print char(13) + 'Selected item_ids:'
insert into @sku_id_tbl(id, item_id) 
select pg_id, item_id from rcms_product_skus
where item_id in (
'200CA54070000000901010202001200642',
'200CA540NC000000901290202001200725',
'200CA540B0000000902000202001210812',
'200CA540D0000000902700202001220346',
'200CA540D0000000902880202001220358',
'200CA54080000000901080202001200651',
'200CA540P0000000902380202001220311',
'200CA54070000000900870202001200613',
'200CA54080000001245860202011200116',
'200CA54080000000901260202001200723',
'200CA54080000000901220202001200720'
)

print char(13) + 'Deleted ETL data on:'
delete eb from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @target_date
and ep.cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print char(13) + 'Deleted copy over data on:'
delete from etl_buyboxdata_override
where record_date = @target_date
and cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print char(13) + 'Copied over from ETL:'
insert into etl_buyboxdata_override(record_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, copy_type)
select 
	@target_date, ep.cms_product_sku_id, eb.etl_buyboxdata_text_lookup_id, eb.etl_buyboxdata_retailer_lookup_id, eb.etl_buyboxdata_delivery_lookup_id,
	eb.etl_buyboxdata_shipping_lookup_id, eb.etl_buyboxdata_addon_lookup_id, eb.availability, eb.price, eb.iso_currency, eb.used_price, eb.new_price,
	eb.is_cartable, eb.is_buyable, eb.id, 'manual_copy_over'
from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @source_date and
	ep.cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

print char(13) + 'Copied over from override:'
insert into etl_buyboxdata_override(record_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, copy_type)
select 
	@target_date, cms_product_sku_id, etl_buyboxdata_text_lookup_id, etl_buyboxdata_retailer_lookup_id,
	etl_buyboxdata_delivery_lookup_id, etl_buyboxdata_shipping_lookup_id, etl_buyboxdata_addon_lookup_id, availability, price, iso_currency,
	used_price, new_price, is_cartable, is_buyable, reference_id, 'manual_copy_over'
from etl_buyboxdata_override
where record_date = @source_date and
	cms_product_sku_id in (
		select id from @sku_id_tbl 
	)

select date, item_id, source, count(1) from view_buybox
where date in (@source_date, @target_date)
and item_id in (
	select item_id from @sku_id_tbl
)
group by date, item_id, source
order by item_id, date, source