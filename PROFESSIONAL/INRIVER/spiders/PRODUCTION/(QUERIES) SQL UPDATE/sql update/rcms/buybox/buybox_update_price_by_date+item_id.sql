-- BUYBOX: UPDATE PRICE VALUE --
declare @target_date date,
		@item_id varchar(255),
		@sku_id int,
		@price_value money;

------------------------------------
-- Set target date
set @target_date = '' 

-- Set target item_id
set @item_id = '' 

-- Set retailer_name
set @price_value = 73.87
------------------------------------

select top 1 @sku_id = pg_id from rcms_product_skus
where item_id = @item_id


print char(13) + 'Updated ETL data:'
update eb set eb.price = @price_value
from etl_buyboxdata eb
join etl_productdata ep on eb.etl_productdata_id = ep.id
where ep.record_date = @target_date
and ep.cms_product_sku_id = @sku_id

print char(13) + 'Updated copy over data:'
update etl_buyboxdata_override set price = @price_value
where record_date = @target_date
and cms_product_sku_id = @sku_id

print char(13) + 'Updated backdoor data:'
update backdoor_etl_buyboxdata set price = @price_value
where record_date = @target_date
and cms_product_sku_id = @sku_id

print char(13) + 'Result:'
select [date], item_id, price_value from view_buybox
where [date] = @target_date
and item_id = @item_id