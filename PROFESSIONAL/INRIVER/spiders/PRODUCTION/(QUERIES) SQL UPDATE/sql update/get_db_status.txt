DECLARE @temp_sp_who2 as TABLE
    (
      SPID INT,
      Status VARCHAR(1000) NULL,
      Login SYSNAME NULL,
      HostName SYSNAME NULL,
      BlkBy SYSNAME NULL,
      DBName SYSNAME NULL,
      Command VARCHAR(1000) NULL,
      CPUTime INT NULL,
      DiskIO BIGINT NULL, -- int
      LastBatch VARCHAR(1000) NULL,
      ProgramName VARCHAR(1000) NULL,
      SPID2 INT,
      rEQUESTID INT NULL --comment out for SQL 2000 databases
    )
 
INSERT  INTO @temp_sp_who2
EXEC sp_who2;

SELECT  * FROM    @temp_sp_who2 a
WHERE a.DBName = DB_NAME(db_id())
ORDER BY a.[Status], a.BlkBy


====================
after query sa status
E KILL ANG MGA SLEEPING
command: KILL <SPID> (ineffective); 

KILL <blk.by>
example: KILL 54
