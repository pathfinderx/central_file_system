# rerun for Comparison
python rerun_cmp.py -s CRAWL_FINISHED -l
python rerun_cmp.py -s CRAWL_FAILED -l
-----------------------------------------------------------
# rerun for Rankings
python rerun_rnk.py -s CRAWL_FINISHED -l
python rerun_rnk.py -s CRAWL_FAILED -l
-----------------------------------------------------------
# rerun for Listings
python rerun_lst.py -s CRAWL_FINISHED -l
python rerun_lst.py -s CRAWL_FAILED -l
-----------------------------------------------------------
#NO WEBSHOTS
python manual_cmp_webshots.py -c 100 -i
#AUTO COPY OVER AND NO WEBSHOTS
python manual_cmp_webshots_override.py -c CRAWL_FAILED -i 
-----------------------------------------------------------
#RE ETL(NO DATA)
python manual_cmp_et.py -c 200 -i
python manual_rnk_et.py -i
python manual_lst_et.py -i
-----------------------------------------------------------

