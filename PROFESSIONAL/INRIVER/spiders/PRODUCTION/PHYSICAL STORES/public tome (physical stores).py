PHYSICAL STORES
	REPOSITORIES
		git@git.v3.detailonline.com:physical-stores-spiders
		git@git.v3.detailonline.com:extract-transform-physical-stores
		git@52.157.149.161:job-manager

	VALIDATORS
		GARMIN
		Mariejy, Keeshiamae

	ROYALCANIN
		Marielle, Turki
		QUERY
			select * from view_physicalstoresdata where date='2022-02-03' order by website
		RERUNS
			1. Login using ssh key: ssh detail@172.100.60.6 / rearchvm for oldcms, newcms for prod2*
			2. Navigate to the appropriate job manager: cd /opt/job-manager-ps/
			3. Activate virtual environment: source /opt/venv/bin/activate
			4. Perform reruns: python rerun_ps.py -p
			M00SEAR0WH000000962040202006220517:M00SEAR0WH000000962040202006220517 -d
			2021-12-06 -t 01:00:00


@SCHEDULES
	* [M00] ROYALCANIN update every monday in channel
	* [100] Garmin update every thursday in channel

10 04 22
	1. Login using ssh key: ssh detail@172.100.20.5 / rearchvm for oldcms, newcms for prod2*
	2. Navigate to the appropriate job manager: cd /opt/job-manager-ps/
	3. Activate virtual environment: source /opt/venv/bin/activate
	4. Perform reruns: python rerun_ps.py -p 100SE8H0ZQ020220718044723667559199 -d  2022-08-18 -t 01:00:00


10 06 22
	# all data from ps 
		select * from view_physicalstoresdata vp where date = '2022-10-06' and item_id in ('100SE30010000001248580202011250800')

	# check if theres missing physical stores data query (no result means no physical store data)
		select item_id, website from view_physicalstoresdata where date='2022-10-06'
			AND item_id IN ('100DK10010000001324440202102090603') GROUP BY item_id, website Order by website

	# check extras
		select * from rcms_product_skus where item_id in ('100NOMI0ZQ020220830051958656878242')

	# run physical stores
		python rerun_ps.py -p 100DK100ZQ020220124090754819827024:100DK100ZQ020220124090754656295024:100FIW2010000001249900202011252038:100NOZ2010000001249730202011251841:100DK100ZQ020220124090750451224024:100DK10010000001659390202111162228:100DK100ZQ020220124090753733277024:100DK10010000000818230201908020936:100DK10010000000818120201908020935:100DK10010000001397490202105040251:100DK10010000001222800202010280537:100SEHH0ZQ020221003083719665531276 -d 2022-10-13 -t 01:00:00

	# trigger physical stores (if no data or no first crawl for the day)
		python main.py -d 2022-10-06 -t 01:00:00 -p 100NOMI0ZQ020220830051958656878242 -j PHYSICALSTORES -c 100

	# check duplicates:
		use this formula from: https://www.youtube.com/watch?v=sJ1pSMNM6r0

@rebuild
	# vm entry
		use vm: 172.100.60.5
		cd spiders/physical-stores-spiders
		git fetch && git pull

	# exec rebuild script
		. deploy-production2.sh 202210291350-update-dlstrat-for-elgiganten-dk

	# cluster config
		az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2

	# kubtail
		kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=physstores