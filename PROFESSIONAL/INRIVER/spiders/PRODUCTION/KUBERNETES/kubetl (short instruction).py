# check pods
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings
kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et

User: detail
Putty Rebuild

---- REBUILD SCRIPT ----

cd spiders
cd comparison-spiders
cd extract-transform-comparison
cd puppeteer-comparison-spiders
cd ranking-spiders
cd extract-transform-rankings
cd listing-spiders
cd extract-transform-listings
git fetch && git pull

# for merging 
az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction
az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction
az aks get-credentials --resource-group crawlers --name AKSListingsProduction
az aks get-credentials --resource-group crawlers --name AKSListingsETProduction
az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
az aks get-credentials --resource-group crawlers --name AKSListingsLProduction

# 2 types to rebuild
. deploy-production.sh 202201061459-update-api-params-for-broze-be
. deploy-staging.sh 202109041216-update-DL-strat-for-bol-com

# alias az="az.cmd"; . deploy-production-new.sh 202201311645-update-goto-options-nevisport-com
	# to server


---- SCALE UP SCALE DOWN ----KODIGS:

cd ~/spiders/kubetail
az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction2
kubectl scale --replicas=0 deployment/comparison-dl
kubectl scale --replicas=1 deployment/comparison-dl



PODTAIL KODIGS:
cd ~/spiders/kubetail
az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction2
./kubetail comparison