how to show production.env

# change directory
	cd spider/puppeteer-rankings-spiders

# change cluster
	az aks get-credentials --resource-group crawlers --name AKSRankingsProduction

# show list of pods
	kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings

# copy pods from the vm
	e.g.: rankings-74675ccbbf-28244
	use 'ctrl + shift + c' to copy from vm

# enter pod
	kubectl exec -it rankings-74675ccbbf-28244 bash

	# and do commands
		ls -la

	# to get the product.env
		printenv

