@ SETUP
	# instruction
		# need ng access to putty - 52.232.2.76
		# and access sa - https://github.com/Shin-Aska/nxtgen_spider

	# generate access token
		https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token

	# github:
		# https://github.com/Shin-Aska/nxtgen_spider
		# pathfinderX77# obj

		# personal access token
			# https://github.com/settings/tokens
			# unlock all privileges (scopes) except for 2nd, 3rd and 4th if 1st scope was selected
			# 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

		# public key
			# ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCS8uCfDe6/8CbTXoNTLPrakniG2x7n4zoIGF7GdFsaGiRaYl7BDcSTfLMVvCRoSLB98rEK4dCLnwAAI3ZkHO0J1OEIzXBMj/YM06lRF+7UyHj5IewKrg2OvCXnlk6lczGuaz0R8HrXPLU7IuEj+YdDHEKESgjZiBPyrx2HSs0MCYtBgx4zbjCKognjbu5FatQsAUMPJ9Z+tdDeNDoRTMIx8tpA3iU2KDOKb68yQwki3vz0bcYEXF5WsYAGmkgb+vBVHyUA7lw4WgPfTXJIPbpwtxrJajvaIMCAIQ7VMKaWgYd1vqbPBspX5ZGp8SD+sZnPYNmeRteaG6zuPoAAbRvb
			
	# setup local repo:
		# npm install
			# install required dependencies from package.json
		# npm install pm2

	# setup dbeaver (sql)
		# host : detail-ultima.database.windows.net
		# database/Schema : detail-analytics
		# us : detailbe
		# pass : 23ejT3UpwgF4K4U

	# rmq
		# http://52.178.45.35:15672/#/queues/%2F/cokejobs
			# darwin
			# spidermen123

	# updated rmq
		# http://172.100.60.8:15672/#/queues/%2F/cokejobs
		# username: darwin
		# password: spidermen123
		# http://172.100.30.6:5984/_utils/#login
		# http://172.100.30.6:5984/_utils/#database/config/next_gen_spiders

	# couch
		# http://40.118.57.127:5984/_utils/#login

	# inriver repo
		# git@ssh.dev.azure.com:v3/inriver/Evaluate/be-ccep-listings-spiders-ppt

@COMMANDS
	# Creation of schedule (job table)

	# Old commands
		# Per type 
		# -	type=ranking companycode=700 date=2009-02-07 node scheduler.js 
		# -	type=listing companycode=700 date=2009-02-07 node scheduler.js

	# New commands
		# add schedules
			# All types 
			# -	companycode=700 date=2009-02-07 node scheduler.js

			# Specific retailer(listing)
			# -	type=listing companycode=700 r="tienda.mercadona.es" date=2009-02-07 node scheduler.js 

			# Specific retailer(ranking)
			# -	type=ranking companycode=700 r="tienda.mercadona.es" date=2009-02-07 node scheduler.js

			# Specific retailer, keyword, postal (ranking)
			# -	type=ranking companycode=700 r="tienda.mercadona.es" k=”Agua” p=28045 date=2009-02-07 node scheduler.js 
			# -	type=ranking companycode=700 r="mercadona.es" k=”Agua” p=50018 date=2009-02-07 node scheduler.js 
			# -	type=ranking companycode=700 r="sanchez-romero.online" k=”Agua” p=28043 date=2009-02-07 node scheduler.js


	# Run Created job (rmq coke jobs – production_spider_rawdata)
		# Old commands
			# -	type=listing companycode=700 date=2009-02-02 node distributor.js
			# -	type=ranking companycode=700 date=2009-02-02 node distributor.js

		# New commands
			# Run all types
				# -	companycode=700 date=2009-02-21 node distributor.js
				# Specific retailer (listing)
				# -	type=listing companycode=700 r=tienda.mercadona.es date=2009-02-23 node distributor.js 
				# Specific retailer (ranking)
				# -	type=ranking companycode=700 r=tienda.mercadona.es date=2009-02-23 node distributor.js 

				# Specific retailer,  postal code (listing)
				# -	type=ranking companycode=700 r=tienda.mercadona.es date=2009-02-23 p=28043 node distributor.js 

				# Specific retailer, keyword, postal code (ranking)
				# -	type=ranking companycode=700 r=tienda.mercadona.es date=2009-02-23 k=”Aqua” p=28043 node distributor.js 

# documentations
	# file:///C:/Users/PathfinderX/Downloads/NxtSpider_Documentation%20(1).pdf
	# C:\Users\PathfinderX\Downloads\CCEP COMMANDS

@Requirements
	# study pm2
	# create database for local test
	# winscp > file checker inside vm

@WORKFLOW
	# sql
		# scripts
		
	# noli navarro (02 24 22 session)
		# cd /opt/detail-job-manager/test_couch_listings
			# this file shows the mango db 

			# python enqueue.py -c 700 -d 2022-02-24 -r 920 

			# python enqueue.py -


		# date@company@hash

		# how to show all failed reruns

		# directory ccep_jobs-new
			# lists of uuids? 

	# john darie (02 24 22 session)
		# create run for this day (triiger or schedule)
			# type=listing companycode=700 date=2022-02-24 node schedule.js

			# set local db
			# set live db

			# all postal
				# ## COUCHDB
				# username: darwin
				# password: spidermen123
				# server: http://40.118.57.127:5984/_utils
				# http://40.118.57.127:5984/_utils/#database/config/next_gen_spiders

				# you can edit here
					# 


		@ workflow
			# first check dbeaver and run scheduler 
			# then couch > config > delete current day (0-sunday, 1-monday ...)
			# run scheduler in local
			# return 4 in config
			# run distributor in local

			# purge in rmq 