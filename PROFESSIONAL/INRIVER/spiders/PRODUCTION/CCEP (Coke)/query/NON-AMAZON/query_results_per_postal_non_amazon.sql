-- USE THIS FIND DUPLICATES FROM SPECIFIC POSTAL OF RETAILER
SELECT a.id, a.postal_code, CASE
    WHEN count(b.job_id) > 0 THEN count(b.job_id)
    ELSE 0
END as result, a.source_url
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
WHERE a.date = '2023-01-18'
AND a.postal_code in ('28220')
and a.website NOT IN ('amazon.es.fresh', 'amazon.es.dia')
GROUP BY a.id, a.postal_code, a.source_url, b.job_id