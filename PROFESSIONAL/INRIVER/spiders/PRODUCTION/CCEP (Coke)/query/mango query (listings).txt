MANGO QUERY (LISTING) - HOW TO CHECK AND RERUN

TO CHECK USING (Date, url and postal) LISTING
{
   "selector": {
      "_id": {
         "$gte": "2022-10-05@",
         "$lte": "2022-10-06@香"
      },
      "parameters": {
         "url": "https://www.carrefour.es/supermercado/bebidas/refrescos/colas/cat650010/c",
         "postal_code": "48970"
      }
   },
   "fields": [
      "_id",
      "status"
   ]
}

TO CHECK USING (Date, website, url and postal) LISTING
{
   "selector": {
      "_id": {
         "$gte": "2022-10-05@",
         "$lte": "2022-10-06@香"
      },
      "parameters": {
         "website" : "www.carrefour.es" ,
         "url": "https://www.carrefour.es/supermercado/bebidas/refrescos/colas/cat650010/c",
         "postal_code": "48970"
      }
   },
   "fields": [
      "_id",
      "status"
   ]
}

VM 60.5
1. cd /opt/detail-job-manager/
2. source vars.sh
3. source /opt/venv3/bin/activate

-l = listing": { "uuid" }
-r = "website": { "uuid" }

SINGLE RERUN

- python enqueue.py -d 2022-10-05 -c 700 -r G20 -l 89d324be-39e5-42ac-bc05-525a4a78d590

MULTIPLE  RERUN

- python enqueue.py -d 2022-10-05 -c 700 -r G20 -l 89d324be-39e5-42ac-bc05-525a4a78d590,89d324be-39e5-42ac-bc05-525a4a78d590


