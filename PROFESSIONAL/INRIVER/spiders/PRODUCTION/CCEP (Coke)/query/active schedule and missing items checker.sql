-- ALL ACTIVATE websites
SELECT DISTINCT website
FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04'

-- check count all postal
SELECT COUNT(*) 
FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04'

-- check count all postal
SELECT COUNT(*) 
FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04'

-- check count without data job id
SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('amazon.es.dia') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('amazon.es.fresh') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('elcorteingles.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('hipercor.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('sanchez-romero.online') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2023-01-04' AND website in ('tienda.mercadona.es') 
AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)

-- amazon.es.dia 23
-- amazon.es.fresh 93
-- elcorteingles.es 219
-- hipercor.es 0
-- sanchez-romero.online 40
-- tienda.mercadona.es 3

