SELECT a.id, a.postal_code, CASE
    WHEN count(b.job_id) > 0 THEN count(b.job_id)
    ELSE 0
END as result, a.source_url
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
WHERE a.website IN ('amazon.es.fresh') 
and a.date = '2023-01-11'
GROUP BY a.id, a.postal_code, a.source_url, b.job_id