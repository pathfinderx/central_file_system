-- CHECKING OF DATA WITH DIIFERENT SAME RESULT AS LAST RUN (AMAZON POSTAL)
SELECT a.id, a.postal_code,CASE 
    WHEN count(b.job_id) > 0 THEN count(b.job_id)
    ELSE 0
END as "new result", c.result as 'old result',
CASE
    WHEN count(b.job_id) != c.result THEN 'false'
    ELSE 'true'
END as compare, a.website ,
a.source_url
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
LEFT JOIN
(
SELECT a.id, a.postal_code, CASE
    WHEN count(b.job_id) > 0 THEN count(b.job_id)
    ELSE 0
END as result, a.source_url
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
-- OLD RESULT ADD DATE LAST POSTAL RUN
WHERE a.website IN ('amazon.es.fresh') and a.date = '2022-09-20'
GROUP BY a.id, a.postal_code, a.source_url, b.job_id
) as c ON c.postal_code = a.postal_code AND c.source_url = a.source_url
-- NEW RESULT ADD DATE NEW POSTAL RUN
WHERE a.website IN ('amazon.es.fresh') and a.date = '2022-10-27'
AND a.postal_code IN
(
'20170'
)
GROUP BY a.id, a.postal_code, a.source_url, b.job_id , c.result, a.website
HAVING count(b.job_id) != c.result
--ORDER BY  a.website ,a.postal_code,count(b.job_id)
ORDER BY  count(b.job_id),a.website ,a.postal_code
