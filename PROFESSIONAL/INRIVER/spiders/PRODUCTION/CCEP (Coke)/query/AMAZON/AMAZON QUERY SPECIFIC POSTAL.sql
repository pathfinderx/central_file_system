SELECT a.id, a.postal_code, count(b.job_id)
FROM [detail-analytics].nxt_spiders.job a
LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id  = b.job_id 
WHERE a.date = '2023-01-04'
AND a.postal_code IN (
'08028'
)
AND a.source_url = 'https://www.amazon.es/s?i=grocery&bbn=21445266031&rh=n%3A21445266031%2Cn%3A6198072031%2Cn%3A6347509031%2Cn%3A21902138031%2Cn%3A6347573031&s=featured-rank&dc&pd_rd_r=d95cc7a4-70b3-472e-9fe7-21b4766a7112&pd_rd_w=tuZ2v&pd_rd_wg=VEMI8&pf_rd_p=7ae28ec6-0ef3-4b2e-8881-c815a0f5261c&pf_rd_r=ER4M7TNTCCV2R70E34GT&qid=1644287714&rnid=21445266031&ref=sr_nr_n_5'
GROUP BY a.id, a.postal_code, b.job_id