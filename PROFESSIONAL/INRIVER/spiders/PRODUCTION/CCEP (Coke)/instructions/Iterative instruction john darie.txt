CCEP SPECIAL SPIDER - RERUN RERUN SHORTCUT
1st - CREATION OF JOBS (JOB TABLE)
All postal and retailer
companycode=700 date=2022-08-03 node scheduler.js



PER TYPE RANKING/LISTING
type=ranking companycode=700 date=2022-10-04 node scheduler.js
type=listing companycode=700 date=2022-10-04 node scheduler.js



PER TYPE RANKING/LISTING - PER RETAILER
type=ranking companycode=700 r=amazon.es.dia  date=2022-10-04 node scheduler.js
type=listing companycode=700 r=amazon.es.fresh date=2022-10-04 node scheduler.js



2nd - JOBS (JOB TABLE) - GOING TO RMQ
All postal and retailer -
companycode=700 date=2022-08-03 node distributor.js



PER TYPE RANKING/LISTING
type=ranking companycode=700 date=2022-08-03 node distributor.js
type=listing companycode=700  date=2022-08-03 node distributor.js



PER TYPE RANKING/LISTING - PER RETAILER
type=listing companycode=700 r=tienda.mercadona.es date=2022-08-03 node distributor.js



3rd - LOCAL RUN CREATIN OF WORKERS
creation of worker
pm2 start spider.js --name "Spider NXT Spider Runner #1"



4th
disable proxy if blocked
disableProxy=true pm2 restart 0 --update-env



5th(scale up and down)
scale up add +(number)
pm2 scale "Spider NXT Spider Runner #1" +6
scale down remove + sign
pm2 scale "Spider NXT Spider Runner #1" 4