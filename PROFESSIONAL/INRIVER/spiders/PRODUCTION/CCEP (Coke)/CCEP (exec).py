@ workflow (pm2 install, create worker, restart worker)
	# installation
		pm2 install@latest -g

	# check workers
		pm2 ls

	# create virtual worker
		pm2 start spider.js --name "Spider NXT Spider Runner #1"
		

	# activate worker
		disableProxy=true pm2 restart 0 --update-env
		disableProxy=true pm2 restart all --update-env		

	# for local command, needs to create numerous spider.js (file)
		disableProxy=true node spider.js

	# kill all workers
		pm2 kill
			# kills all active workers and
		pm2 stop all
			# stops workers

	# scale
		# scale up add +(number)
		pm2 scale "Spider NXT Spider Runner #1" +6
			# only affects "Spider NXT Spider Runner #1"

		# scale down remove + sign
		pm2 scale "Spider NXT Spider Runner #1" 1
			# only affects "Spider NXT Spider Runner #1"

	# FILE SYSTEM WALKTHROUGH
		# strategies > mercadona.es
			# 

		# spider.js
			# lookup couch > extract > connect to api

@ REMINDERS
	# schedule
		# every wednesday run

	# scheduler = scrapes data without postal code 
		# 11 urls * 5 postal codes 
	# distributor = execute to queue

@sources
		git@104.40.242.138:detail-listing-spiders
		git@104.40.242.138:d2ms-v3.5-rankings-ultima

	# queue
		http://172.100.60.8:15672/#/queues/%2F/cokejobs
		# username: darwin
		# password: spidermen123

	# couch
		http://172.100.70.4:5984/_utils/#database/d2ms-listing-data/_find

@ exec actual
	 # db check at 700
	 	# host: detail-ultima.database.windows.net
	 	# Database/schema: detail-analytics
	 	# username: detailbe
	 	# password: 23ejT3UpwgF4K4U	

	 # exec 
	 	# scheduler
	 		type=listing companycode=700 date=2022-10-19 node scheduler.js
		 	type=listing companycode=700 r="amazon.es.fresh" date=2023-01-04 node scheduler.js 

	 	# ditrubutor + type + specific retailer
			type=listing companycode=700 r='hipercor.es' date=2023-01-11 node distributor.js

		# ditrubutor + type + specific retailer + specific postal code 
			type=listing companycode=700 r='amazon.es.fresh' date=2023-01-04 p=08028 node distributor.js

	@CHECKERS OR QUERY

		 # check couch for coke ppt using mango query
		 	{
			   "selector": {
			      "_id": {
			         "$gt": "2022-10-05@"
			      },
			      "status": "FAILED"
			   },
			   "fields": [
			      "_id",
			      "status"
			   ]
			}

		# check available retailer to execute for shceduler
			SELECT DISTINCT website
			FROM [detail-analytics].nxt_spiders.job WHERE date = '2022-10-05'


		# checker for the count
			-- NO DATA JOB_ID
			SELECT COUNT(id) FROM [detail-analytics].nxt_spiders.job WHERE date = '2022-10-05' AND website = 'tienda.mercadona.es'
			AND id NOT IN (SELECT job_id FROM [detail-analytics].nxt_spiders.production_spider_rawdata)


		# check for old and new result:
			# SELECT a.id, a.postal_code,CASE 
			#     WHEN count(b.job_id) > 0 THEN count(b.job_id)

			#     ELSE 0
			# END as "new result", c.result as 'old result',
			# CASE
			#     WHEN count(b.job_id) != c.result THEN 'false'
			#     ELSE 'true'
			# END as compare, a.website ,
			# a.source_url
			# FROM [detail-analytics].nxt_spiders.job a
			# LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
			# LEFT JOIN
			# (
			# SELECT a.id, a.postal_code, CASE
			#     WHEN count(b.job_id) > 0 THEN count(b.job_id)
			#     ELSE 0
			# END as result, a.source_url
			# FROM [detail-analytics].nxt_spiders.job a
			# LEFT JOIN [detail-analytics].nxt_spiders.production_spider_rawdata b ON a.id = b.job_id
			# -- OLD RESULT ADD DATE LAST POSTAL RUN
			# WHERE a.website IN ('amazon.es.dia') and a.date = '2022-09-28'
			# GROUP BY a.id, a.postal_code, a.source_url, b.job_id
			# ) as c ON c.postal_code = a.postal_code AND c.source_url = a.source_url
			# -- NEW RESULT ADD DATE NEW POSTAL RUN
			# WHERE a.website IN ('amazon.es.dia') and a.date = '2022-10-05'
			# AND a.postal_code IN
			# (
			# '28016'
			# )
			# GROUP BY a.id, a.postal_code, a.source_url, b.job_id , c.result, a.website
			# HAVING count(b.job_id) != c.result
			# --ORDER BY  a.website ,a.postal_code,count(b.job_id)
			# ORDER BY  postal_code

		# after checking identify the job_id
			# then delete job_id
				-- DELETION OF RAWDATA PER ID
				DELETE
				FROM [detail-analytics].nxt_spiders.production_spider_rawdata
				WHERE job_id IN (
					'325452'
				)

	@ vm normal spider run
		# query
			# mango query on db
			# run here: http://172.100.70.4:5984/_utils/#database/d2ms-director-listing/_find

			# "$gte" -> should be test run date
			# "$lte" -> should be all postal run date
			{
			   "selector": {
			      "_id": {
			         "$gte": "2022-10-11@",
			         "$lte": "2022-10-12@香"
			      },
			      "parameters": {
			         "url": "https://www.alcampo.es/compra-online/bebidas/zumos-de-frutas/c/W1102?q=%3Arelevance%3AcategoryChild%3AW110209",
			         "postal_code": "08192"
			      }
			   },
			   "fields": [
			      "_id",
			      "status"
			   ]
			}

		# Run
			VM 60.5
			1. cd /opt/detail-job-manager/
			2. source vars.sh
			3. source /opt/venv3/bin/activate

			python enqueue.py -d 2022-10-12 -c 700 -r G20 -l 71089fe1-498b-448c-a9f0-417fc66141a2,43d9c74b-8186-428f-9ba9-ecaa59811130,b8476a12-0479-485a-9c48-43d97747ff22,43d9c74b-8186-428f-9ba9-ecaa59841700,839305b7-49e8-4f34-922b-08c664788abf,c5160ebb-634c-4498-8893-3ddaf6adb28d,43d9c74b-8186-428f-9ba9-ecaa59819004,f8b35490-2453-47a4-8562-09ca182ea8a9,43d9c74b-8186-428f-9ba9-ecaa59802006,e78b48f3-9b70-48ca-a31f-ccefd9db43b7,43d9c74b-8186-428f-9ba9-ecaa59816004,43d9c74b-8186-428f-9ba9-ecaa59844002

			python enqueue.py -d 2022-10-12 -c 700 -r G20 -l e78b48f3-9b70-48ca-a31f-ccefd9db43b7

			python main.py -r rankings -s FAILED -d [docids colon separated] 

	@ ccep listings debuging
		* set test.py
		* start at line 100 for elcorte_es_listings

@ Issues to resolve:
	* restricted access to normal spider ccep