Tech val issues
	# May 11, 2022 Technical Validation Updates

	# Number of issues last update: 17 (last updated on daily standup)
	# Number of issues today: 26 (daily sheet, tab, number of issues today)

	# GARMIN 100:
	# www.elkjop.no| -2 availability

	# ZOUND 200:
	# www.elgiganten.dk | -2 availability (no need to explain)
	# www.elgiganten.se | -2 availability

	# RAZER K00:
	# www.computeruniverse.net/de | No image_count
	# www.otto.de | -1 description


	PRODUCT DATA: view_prod_tech_validation_daily/view_prod_tech_validation_premium -> initial_prod_tech(till issue field only) -> Tech-PRODUCT (paste to ) -> (optional if there are valid item_ids) prod_valid_issue

	SDA: view_3p_tech_validation -> initial_sda_tech -> Tech- SDA Retailers -> (optional if there are valid item_ids) sda_valid_issues

	Haglöfs Q00


	| 200 | A00 | 100 | K00 | F00 | Q00 | M00 | O00 | P00 | U00 | 610 | A10 | 110 | 510(-1 day) | 010(-1) | 210(-1) | 310(-1) | X00(-1)| Z00(-1) | 410 |

	-- the rest of companies
	select * from view_prod_tech_validation_daily WHERE date = '2022-05-11' ORDER BY website

	select * from view_prod_tech_validation_premium WHERE date = '2022-05-11' ORDER BY website

	for rankings & listings
	-- 
	select * from view_rank_tech_validation
	where date = '2022-06-21'
	order by website

	select * from view_list_tech_validation
	where date = '2022-06-21'
	order by website

	--for 200
	select date,premium,client_code,field,website,item_id,issue from view_prod_tech_validation_daily WHERE date = '2022-05-04' and retailer_class = 'normal' ORDER BY website

	select date,premium,client_code,field,website,item_id,issue from view_prod_tech_validation_daily WHERE date = '2022-05-04' and retailer_class = 'SDA' ORDER BY website

	SELECT * from view_3p_tech_validation vptv where date = '2022-05-10' order by website 

OTHERS
	* purpose of tech val -> separate to daily to prevent getting overloading of request in daily
	* what is premium? deadline for this day -> fr publish


WORKFLOW
	* for company 200:
		use unique sql for normal, sda and 3p

	* others till U00
		use normal sql for daily and prem

	* VALIDATION IN SDA_TECH (for is_new issue):
		# valid if product doesn't contain these:
			# https://prnt.sc/3xxG8-mjrAoJ

REMINDERS
	# oblige techval assignment to rebuild before 11pm
	# 1 pm turnover time for techval
	# cannot make 'redirected' as techval valid issues because once there are changes in setup, it makes it not valid anymore

ACQUIRED EXPERIENCE
	# transfer valid issues to prod_valid_issue and sda_valid_issues
		# must not came from tech-product or tech-sda-retailers sheet because it will result in wrong lookup in initial_prod_tech and initial_sda_tech
			# it must must be from initial_prod_tech and initial_sda_tech because the issues are wholesome
			# tech-product or tech-sda-retailers have already distrubuted issues (https://prnt.sc/k6hjEHyARG4T)
			# valid issues must not have distributed issues in order to have correct look-up in initial_prod_tech (https://prnt.sc/57ibwFfwvFvN)
			