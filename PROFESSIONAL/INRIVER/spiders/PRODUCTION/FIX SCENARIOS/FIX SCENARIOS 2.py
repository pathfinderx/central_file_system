# pag daily ka kani ang dapat nimo tandaan.
	# w0w = acknowledge
	# Like = Done
	# sad = trello/blocked
	# heart = quick fix ( less than 30 minutes ok na sya)
	# angry = for rebuild/Fixing

# check for issue codes:
	# -1 Not Found
	# 1 instock
	# 0 out stock
	# -2 ET error
	# -3 Spider Error
	# auto-copy-over

# what to write in cause of issue? 
	# multiple reruns till get it
		# blocked
	# rerun once
		# proxy issue

# check other keyword search for country
	#SELECT date,source,website,keyword,ritem_id,product_url,country,count(1) from view_all_rankingsdata where website ='www.aptekagemini.pl' and date = '2021-11-12' GROUP BY date,source,website,keyword,ritem_id,product_url,country

# check other keyword search for country
	# SELECT * FROM view_all_rankingsdata WHERE date = '2021-11-13' AND website = 'www.netonnet.no' AND source != 'etl'

# for duplicates
	# SELECT date,website, keyword, product_website,product_url, ritem_id , source, COUNT(1) from view_all_rankingsdata var2 where var2.website = 'www.target.com' and var2.keyword in (N'') and var2.[date] = '2021-11-22' GROUP BY date,website, keyword, product_website,product_url, ritem_id , source

# runner bulk commands: 
	# cmd: cd C:\Users\kurt kevin nebril\Desktop\runner_tool
	# cmd: python app.py
	# paste the item_ids here or r_item_ids
		# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\input_item_ids.txt
	# cmd: 
		# how to generate webshots?
		# rerun_webshots -c A00 -s etl -d 2021-01-26

		# how to generate comparisons ?
		# rerun_comparison -c CRAWL_FINISHED -d 2021-01-26
	# rerun code generated here
		# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\rerun_cmd_generated.txt

	# identify the company code if prod1 or prod2
	# putty prod(n)
		# paste the generated file at putty
	# check rabbit mq or queues
	# rerun the sql in dbeaver if theres any changes

# Common Issues
	# auto copy over
		# always check local
		# check browser if empty value is valid
			# if yes, lightshot
		# rerun

		# crawl_failed
			# rerun

	# if needs further fixes
		# for trello

	# deadlink
		# check visible

	# 

02 01 22
	1 3037	2022-02-01	New CMS	P00	all	www.ozon.ru	
		P00RUXZ01L000001575880202109060525
		P00RU1W01L000001460490202106280511
		P00RU1W01L000001454910202106220209	

		missing data		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@P00RUXZ01L000001575880202109060525
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@P00RUXZ01L000001575880202109060525,2022-02-01@21:00:00@P00RU1W01L000001460490202106280511,2022-02-01@21:00:00@P00RU1W01L000001454910202106220209

			# rerun but questionnable

	1 3038	2022-02-01	New CMS	P00	all	www.a1.rs	
		P00RSVV0OV020220104012940836933004
		P00RSVV01L000001622720202110180740
	
		missing data		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00RSVV0OV020220104012940836933004,2022-02-01@22:00:00@P00RSVV01L000001622720202110180740

			# rerun

	1 3039	2022-02-01	New CMS	P00	image_count	www.citilink.ru	
		P00RUK801L000001458910202106230519	
			https://www.citilink.ru/product/smartfon-motorola-xt2083-3-64gb-4gb-zelenyi-3g-4g-2sim-6-517-and1-6-48-1402854/
		incorrect image count		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@P00RUK801L000001458910202106230519
			# rerun

	1 3042	2022-02-01	New CMS	610	price_value	magasin.dk	
		all item_ids	
		Mismatched Prices.
		Request Rerun 
		only.	 	
		Yes	Keeshia

		# solution
			# rerun

	1 3043	2022-02-01	New CMS	610	price_value	connox.de	
		all item_ids	
		Mismatched Prices. 
		Request Rerun only.
		Yes	Keeshia

		# solution
			# no issue

	0 (R D-1) 3044	2022-02-01	New CMS	P00	image_count	telia.fi	
		'P00FIM001L000001410550202105110756',
		'P00FIM001L000001582750202109130550',
		'P00FIM001L000001622080202110180740',
		'P00FIM001L000001621960202110180740',
		'P00FIM001L000001622020202110180740',
		'P00FIM00OV020220104012938633409004',
		'P00FIM00OV020220104012938985322004'

		Missing image count	
		https://prnt.sc/26mosfi	
		Yes	Jahar

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@P00FIM001L000001410550202105110756,2022-02-01@21:00:00@P00FIM001L000001582750202109130550,2022-02-01@21:00:00@P00FIM001L000001622080202110180740,2022-02-01@21:00:00@P00FIM001L000001621960202110180740,2022-02-01@21:00:00@P00FIM001L000001622020202110180740,2022-02-01@21:00:00@P00FIM00OV020220104012938633409004,2022-02-01@21:00:00@P00FIM00OV020220104012938985322004

			# multiple reruns failed
			# checking local: 
			# trello

	1 3050	2022-02-01	New CMS	100	all	utomhusliv.se	
		'100SETX010000001649300202111090812',
		'100SETX010000001649660202111090812',
		'100SETX010000001649680202111090812',
		'100SETX010000001649690202111090812',
		'100SETX010000001649840202111090812',
		'100SETX010000001650490202111090812',
		'100SETX010000001651110202111090812',
		'100SETX010000001651120202111090812'

		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@100SETX010000001649300202111090812,2022-02-01@22:00:00@100SETX010000001649660202111090812,2022-02-01@22:00:00@100SETX010000001649680202111090812,2022-02-01@22:00:00@100SETX010000001649690202111090812,2022-02-01@22:00:00@100SETX010000001649840202111090812,2022-02-01@22:00:00@100SETX010000001650490202111090812,2022-02-01@22:00:00@100SETX010000001651110202111090812,2022-02-01@22:00:00@100SETX010000001651120202111090812

			# rerun

	1 3051	2022-02-01	New CMS	100	in_stock	utomhusliv.se	
		100SETX010000001649590202111090812	

		Not_found in stock		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@100SETX010000001649590202111090812

			# rerun

	1 3054	2022-02-01	New CMS	Q00	all	outdoorxl.nl	
		Q00NLEX0IJ000001565450202108230711	
			https://www.outdoorxl.nl/head-vector-evo-110-2015f.html
		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@Q00NLEX0IJ000001565450202108230711

			# redirect: valid deadlink

	1 3056	2022-02-01	New CMS	P00	all	o2.sk	All item_ids	
		Auto copy over (site is up)		Yes	Glenn

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00SKQU01L000001395830202105030410,2022-02-01@22:00:00@P00SKQU01L000001395840202105030410,2022-02-01@22:00:00@P00SKQU01L000001395870202105030410,2022-02-01@22:00:00@P00SKQU01L000001395900202105030410,2022-02-01@22:00:00@P00SKQU01L000001395910202105030410,2022-02-01@22:00:00@P00SKQU01L000001395920202105030410,2022-02-01@22:00:00@P00SKQU01L000001459240202106230519

			# multiple rreruns failed 

			# valid deadlink

	2 3057	2022-02-01	New CMS	200	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-02-01' and webshot_url is NULL	No Webshots		
		Yes	Mojo

		# multiple rewebshots 
			# recommend cloudscraping 

	2 3061	2022-02-01	New CMS	200	all	amazon 3p retailer	
		200DE520NC000000846710201909090744

		'200ITGF070000000557590201904220945',
		'200ITGF070000000557370201904220937',
		'200ITGF080000000557530201904220944',
		'200ITGF080000000557080201904220902',
		'200ITGF070000000557970201904221001'

		'200ESFF070000000557640201904220949',
		'200ESFF070000000557470201904220941',
		'200ESFF070000000557860201904220956'

		Invalid deadlink		
		Yes	Louie

		# solution
			# DE
				# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200DE520NC000000846710201909090744

			# IT
				# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200ITGF070000000557590201904220945,2022-02-01@22:00:00@200ITGF070000000557370201904220937,2022-02-01@22:00:00@200ITGF080000000557530201904220944,2022-02-01@22:00:00@200ITGF080000000557080201904220902,2022-02-01@22:00:00@200ITGF070000000557970201904221001


			# ES
				# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200ESFF070000000557640201904220949,2022-02-01@22:00:00@200ESFF070000000557470201904220941,2022-02-01@22:00:00@200ESFF070000000557860201904220956

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200ITGF070000000557370201904220937,2022-02-01@22:00:00@200ITGF080000000557530201904220944

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200ITGF070000000557370201904220937

			# multiple reruns

	1 3062	2022-02-01	New CMS	U00	rating_reviewers	magazineluiza.com.br	
		'U00BRXC0OL000001302450202101200124', 4.3
			https://www.amazon.it/dp/B07712YX6L/ref=olp_aod_redir?th=1
		'U00BRXC0OL000001302410202101200124', 4
			https://www.amazon.it/dp/B07712YX6L/ref=olp_aod_redir#aod
		'U00BRXC0OL000001302380202101200124',
		'U00BRXC0OL000001302430202101200124',
		'U00BRXC0OL000001302440202101200124'

		incorrect ratings and review		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@02:00:00@U00BRXC0OL000001302450202101200124,2022-02-01@02:00:00@U00BRXC0OL000001302410202101200124,2022-02-01@02:00:00@U00BRXC0OL000001302380202101200124,2022-02-01@02:00:00@U00BRXC0OL000001302430202101200124,2022-02-01@02:00:00@U00BRXC0OL000001302440202101200124

			# multiple reruns

	2 3063	2022-02-01	New CMS	200	all	fnac.com/3P	
		'200FRDU070000001381340202104140811',
		'200FRDU080000001381460202104140811',
		'200FRDU0O0000001381520202104141007',
		'200FRDU080000001381640202104141007',
		'200FRDU070000001398120202105040259'

		Invalid deadlink		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200FRDU070000001381340202104140811,2022-02-01@22:00:00@200FRDU080000001381460202104140811,2022-02-01@22:00:00@200FRDU0O0000001381520202104141007,2022-02-01@22:00:00@200FRDU080000001381640202104141007,2022-02-01@22:00:00@200FRDU070000001398120202105040259

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200FRDU070000001381340202104140811,2022-02-01@22:00:00@200FRDU080000001381640202104141007

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200FRDU080000001381640202104141007

			# multilple reruns

			# No issues in spiders; 3p is blocked after several reruns, kindly MO this: 200FRDU080000001381640202104141007

	-----
	0 (R D-1) 1869	2022-02-01	New CMS	P00	all	www.citilink.ru 	
		Моторола G - 18
			746257c9-2321-4d05-96a4-f9ab37a547de@8THUOEXdEeeMawANOiZi-g
		Мото G10 - 18
			ee066408-eea2-445d-a747-48fa6164d00b@8THUOEXdEeeMawANOiZi-g
		Мото E7 - 3
			ee066408-eea2-445d-a747-48fa6164d00b@8THUOEXdEeeMawANOiZi-g

		Data Count Mismatch 7 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)

		https://prnt.sc/26mo756
		https://prnt.sc/26mo7wy
		https://prnt.sc/26mo8e7
		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@20:00:00@P00@746257c9-2321-4d05-96a4-f9ab37a547de@8THUOEXdEeeMawANOiZi-g,2022-02-01@20:00:00@P00@ee066408-eea2-445d-a747-48fa6164d00b@8THUOEXdEeeMawANOiZi-g,2022-02-01@20:00:00@P00@ee066408-eea2-445d-a747-48fa6164d00b@8THUOEXdEeeMawANOiZi-g

			# trello: https://trello.com/c/DJcircRP/3085-motorola-rankings-data-count-mismatch-in-wwwcitilinkru

	1 1870	2022-02-01	New CMS	P00	all	www.bol.com	
		smartphone	
			OIops0iTEei-cwANOiOHiA@UAjszpOqEei-eQANOiOHiA

		duplicate product_website & product_url	
		https://prnt.sc/26mobig
		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00@OIops0iTEei-cwANOiOHiA@UAjszpOqEei-eQANOiOHiA

			# rerun

	1 1871	2022-02-01	New CMS	P00	all	www.saturn.de	
		Motorola E
			78a4e801-c08b-49c1-a2e4-efde2b1977d5@UbbyaApIEem-gAANOiOHiA
		/ Moto E7	
			9c7af2b6-df07-4948-9159-a2e59c08f14f@UbbyaApIEem-gAANOiOHiA
		duplicate product_website

		https://prnt.sc/26moawj
		https://prnt.sc/26mo9xt	

		keen

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00@78a4e801-c08b-49c1-a2e4-efde2b1977d5@UbbyaApIEem-gAANOiOHiA,2022-02-01@22:00:00@P00@9c7af2b6-df07-4948-9159-a2e59c08f14f@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00@78a4e801-c08b-49c1-a2e4-efde2b1977d5@UbbyaApIEem-gAANOiOHiA

			# multiple reruns

	0 (R D-1) 1874	2022-02-01	New CMS	P00	all	www.mediaexpert.pl	
		smartfon	
			932692b8-8c63-419a-be45-42ab2a83325a@5u81hEXcEeeMawANOiZi-g
		Auto copyover for 2 days 	
		https://prnt.sc/26mols2		
		Mich

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@932692b8-8c63-419a-be45-42ab2a83325a@5u81hEXcEeeMawANOiZi-g
			# multiple reruns failed
			# check local

			# trello

	1 1876	2022-02-01	New CMS	P00	all	www.amazon.nl 
		Motorola
		1 Day ACO Data

		https://prnt.sc/26mopq8		
		Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@a398488a-c8a5-4582-b80d-fcf2848b36d8@6429dbf1-9dac-42f6-b67d-14d70d739206
			# rerun


	0 (R D-1) 1879	2022-02-01	New CMS	P00	all	www.cdiscount.com	
		'Android phone',
		'Camera phone',
		'Flip phone',
		'Moto E7',
		'Moto G10',
		'motorola G',
		'Smartphone'

		Autocopy Over for 1 day	

		https://prnt.sc/26moniy
		https://prnt.sc/26monyx
		https://prnt.sc/26moobx
		https://prnt.sc/26mookj
		https://prnt.sc/26moou2
		https://prnt.sc/26mos6d
		https://prnt.sc/26mosb9	

		Yes	Richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@bea0fe22-54c4-4871-be3d-d2d6368cde79@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@4c08a2f9-08dc-40b1-b5a7-6007e237ae0f@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@d9e77de8-de80-453e-964b-8297ee7ab456@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@9c7af2b6-df07-4948-9159-a2e59c08f14f@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@c76c9aa9-bf2c-45cb-b6f2-d994e2ff2e32@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@22d5c6fd-20d3-4e3e-a9ef-76ffb952ecec@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@OIops0iTEei-cwANOiOHiA@cXta9t_4Eee8oQANOijygQ

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@bea0fe22-54c4-4871-be3d-d2d6368cde79@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@d9e77de8-de80-453e-964b-8297ee7ab456@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@9c7af2b6-df07-4948-9159-a2e59c08f14f@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@22d5c6fd-20d3-4e3e-a9ef-76ffb952ecec@cXta9t_4Eee8oQANOijygQ,2022-02-01@22:00:00@P00@OIops0iTEei-cwANOiOHiA@cXta9t_4Eee8oQANOijygQ

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@9c7af2b6-df07-4948-9159-a2e59c08f14f@cXta9t_4Eee8oQANOijygQ

			# trello

	0 (R D-1) 1880	2022-02-01	New CMS	P00	all	www.cdiscount.com	
		motorola razr	
			110c563e-d2cc-4b40-84d7-31b7173c18ba@cXta9t_4Eee8oQANOijygQ

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26mopqj	
		Yes	Richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00@110c563e-d2cc-4b40-84d7-31b7173c18ba@cXta9t_4Eee8oQANOijygQ

			# multiple reruns failed
			# check local

			# trello

	1 1881	2022-02-01	New CMS	200	all	www.dns-shop.ru	наушники	
		Invalid Deadlink	
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
		https://prnt.sc/26moqn7		
		Kristian Angelo

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@21:00:00@200@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
			# multiple rerunss

	1 1884	2022-02-01	New CMS	P00	all	www.fnac.es	
		moto g
			d34c7c5b-fb95-41cd-bc08-2f8f89184ee7@FCuQnuOcEeeyugANOiZi-g
		Auto Copy Over for 1 Day	
		https://prnt.sc/26moxuq		
		keen

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@d34c7c5b-fb95-41cd-bc08-2f8f89184ee7@FCuQnuOcEeeyugANOiZi-g

			# rerun

	0 (R D-1) 1888	2022-02-01	New CMS	P00	all	www.mediamarkt.es	
		moto edge 20 lite 13
		moto e7i power 14
		moto e40 14

		Data Count Mismatch 8 (Fetched Data is Lesser than expected)
		Data Count Mismatch 9 (Fetched Data is Lesser than expected)
		Data Count Mismatch 8 (Fetched Data is Lesser than expected)

		https://prnt.sc/26mp503
		https://prnt.sc/26mp4gz
		https://prnt.sc/26mp3zy		

		keen

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@P00@9d4a72f1-471b-4d8a-9990-465cb7642306@m26IDkXlEeeMawANOiZi-g,2022-02-01@22:00:00@P00@f0742b3f-9b40-44bd-a744-1570139e5f7c@m26IDkXlEeeMawANOiZi-g,2022-02-01@22:00:00@P00@4f5c647a-f4db-410d-8fc5-9f357d32246f@m26IDkXlEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@9d4a72f1-471b-4d8a-9990-465cb7642306@m26IDkXlEeeMawANOiZi-g,2022-02-01@22:00:00@P00@4f5c647a-f4db-410d-8fc5-9f357d32246f@m26IDkXlEeeMawANOiZi-g

			# Trello

	1 1889	2022-02-01	New CMS	P00	all	www.amazon.it	
		moto g
		smartphone
			
		1 day Auto Copy Over	

		https://prnt.sc/26mp6i4
		https://prnt.sc/26mp6qh
		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@d34c7c5b-fb95-41cd-bc08-2f8f89184ee7@fFaOpnhPEei8ygANOijygQ,2022-02-01@22:00:00@P00@OIops0iTEei-cwANOiOHiA@fFaOpnhPEei8ygANOijygQ

			# rerun

	1 1890	2022-02-01	New CMS	P00	all	www.gigantti.fi	
		Kamera Phone	
		Data Count Mismatch 18 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26mp9fy		
		keen

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@P00@45222afd-aeff-43f5-bc96-320be14290be@jDJIFuO8EeaOEAANOrFolw
			# rerun

	1 1891	2022-02-01	New CMS	200	all	www.technopark.ru	
		беспроводная акустика bluetooth	

		Data Count Mismatch 1(Fetched Data is Greater than expected)	
		https://prnt.sc/26mpd24		
		Kristian Angelo

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@21:00:00@200@d3eb3d84-83b9-4bdc-8374-f1e7480daae1@d0bb62ad-8919-4942-a84e-09748f674d32

			# multiple reruns

	1 1894	2022-02-01	New CMS	200	all	www.elkjop.no	
		smarthøyttaler	
		Duplicate product_website & product_url
		https://prnt.sc/26mqk9j		
		Kristian Angelo

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200@86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw

			# rerun

	------
	1 644	2022-02-01	New CMS	200	all 	www.elkjop.no	
		Headphones Noise Cancelling	
			200@500@d5a22c87-f5fe-40c8-ac91-f811dccd1711
		Data Count Mismatch(Fetched Data is Lesser than expected	

		https://prnt.sc/26moiz3	
		Yes	Rhunick

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-01@22:00:00@200@500@d5a22c87-f5fe-40c8-ac91-f811dccd1711

			# rerun

	1 645	2022-02-01	New CMS	P00	all 	www.a1.rs	
		All phones offer with tariff	
			P00@VV0@13dc4a78-21a4-4d4c-99cb-68777e3ccf0f
		1 Day Auto Copy Over	
		https://prnt.sc/26mold5	Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@VV0@13dc4a78-21a4-4d4c-99cb-68777e3ccf0f
			# rerun

	0 (R D-1) 651	2022-02-01	New CMS	P00	all 	www.telekom.sk	
		All smartphones
			https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806

		5G Smartphones	
			https://eshop.telekom.sk/category/all-devices/list/product_listing?filter%5B%5D=filter.device_category%5B%5D=5g&currentPage=2&tariffId=RP1140&itemPerPage=16&bp=acquisition

		2 Days Auto Copy Over
		https://prnt.sc/26mqpgt	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d,2022-02-01@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

			# trello

	1 652	2022-02-01	New CMS	P00	all 	www.telenor.rs	
		All phones offer with tariff

		1 Day Auto Copy Over	
		https://prnt.sc/26mqr1i
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@PU0@ac224946-640f-4a31-8142-720c6a24b1bc
			# rerun

	0 (R D-1) 653	2022-02-01	New CMS	P00	all 	www.t-mobile.pl	
		All Devices	

		2 Days Auto Copy Over	
		https://prnt.sc/26mqtlw	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@XT0@87a2df05-de33-450d-a8e8-f3b4837b090b

			# trello

	0 (R D-1) 656	2022-02-01	New CMS	P00	all 	www.orange.pl	
		Telefony i urządzenia - z abonamentem	

		1 Day Auto Copy Over	
		https://prnt.sc/26mqgo3	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-01@22:00:00@P00@CS0@7a45a966-6700-4d08-a42f-809bb87bca79
			# rerun

			# trello


	Reference Trello
		582 amazon de
			page redirect, js rendered?

		2788 jarir.com
			unknown

		1853 currys.co.uk
			lacks api data for url

		1880 New CMS	P00	all	www.cdiscount.com
		1874 New CMS	P00	all	www.mediaexpert.pl
		1879 New CMS	P00	all	www.cdiscount.com
		1869 New CMS	P00	all	www.citilink.ru
		3044 New CMS	P00	image_count	telia.fi

		1888 New CMS	P00	all	www.mediamarkt.es
		651 New CMS	P00	all 	www.telekom.sk
		656 New CMS	P00	all 	www.orange.pl

		3057 New CMS	200	webshot_url

	Stats:
		Rerun: 34 * ((37) / 34) = 
			# count * ((sumDificultyLevel / count))
			# 


02 02 2022
	(F D-3) Trello
		/ 651 telecom.se 
			All smartphones
				https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806

			5G Smartphones	
				https://eshop.telekom.sk/category/all-devices/list/product_listing?filter%5B%5D=filter.device_category%5B%5D=5g&currentPage=2&tariffId=RP1140&itemPerPage=16&bp=acquisition
	Stats:
		Fix: 1 (3/1) = 3

02 03 2022
	(R D-1) 1853 currys.co.uk
		/ rerun
		* fix the rest

	* sort phone

	(D) Development
		* listings
			* https://www.ferguson.com/
				* https://www.ferguson.com/category/bathroom-plumbing/bathroom-faucets/bathroom-sink-faucets/_/N-zbq4i3Zzbq5ebZzbq7ix?ADA_Compliant_fr=some&Application_fr=some&Bathroom_Faucet_Type_fr=some&CEC_Compliant_fr=some&Center_Size_fr=some&Collection_fr=some&Color_Finish_Category_fr=some&Deck_Plate_Included_fr=some&Faucet_Mount_fr=some&Flow_Rate_fr=some&Handle_Type_fr=some&Nrpp=20&Number_of_Handles_fr=some&Number_of_Holes_fr=some&WaterSense_Labeled_fr=some&_=1643779825700&brand_fr=some&ds=list&sr=everywhere

				/ page1 , page 2
				* testing the rest of the 

	Stats:
		Rerun: 1
		Development: 1

02 04 2022
	SETUP
	0 (R D-1) amazon.com
		'H10USW00Q8220220125025700763994025',
		'H10USW00Q8220220125025647520502025',
		'H10USW00Q8220220131092056462130031',
		'H10USW00Q8220220131092056671189031',
		'H10USW00Q8220220125025640507379025',
		'H10USW00Q8220220125025700895983025',
		'H10USW00Q8220220125025701067139025',
		'H10USW00Q8220220125025700089385025',
		'H10USW00Q8220220125025700595818025',
		'H10USW00Q8220220125025700272018025',
		'H10USW00Q8220220131092058575726031',
		'H10USW00Q8220220125025640932124025',
		'H10USW00Q8220220125025642010815025',
		'H10USW00Q8220220125025641830429025',
		'H10USW00Q8220220131092056856610031',
		'H10USW00Q8220220131092057032725031',
		'H10USW00Q8220220131092058901621031',
		'H10USW00Q8220220125025702378616025',
		'H10USW00Q8220220125025641267329025',
		'H10USW00Q8220220125025642201157025',
		'H10USW00Q8220220125025702848780025',
		'H10USW00Q8220220125025711922341025',
		'H10USW00Q8220220125025711439677025',
		'H10USW00Q8220220125025711603334025',
		'H10USW00Q8220220125025711760195025',
		'H10USW00Q8220220125025712077590025',
		'H10USW00Q8220220125025710392845025',
		'H10USW00Q8220220125025710579020025',
		'H10USW00Q8220220125025703002212025',
		'H10USW00Q8220220125025704576253025',
		'H10USW00Q8220220125025656665298025',
		'H10USW00Q8220220125025657758205025',
		'H10USW00Q8220220125025648714038025',
		'H10USW00Q8220220125025647698280025',
		'H10USW00Q8220220131092057385157031',
		'H10USW00Q8220220131092057209392031',
		'H10USW00Q8220220125025648348139025',
		'H10USW00Q8220220125025709236506025',
		'H10USW00Q8220220125025704913216025',
		'H10USW00Q8220220125025707096675025'

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-03@04:00:00@H10USW00Q8220220125025647520502025

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-04@04:00:00@H10USW00Q8220220125025648348139025

			# multiple reruns failed
				# fixing in local

	(W D-1) PROD-748	2/3/2022	2/3/2022	blocked webshots in build.com	Inriver-Testclient5 (H10)

		H10US7X0Q8220220125025744039252025
		H10US7X0Q8220220125025738788689025
		H10US7X0Q8220220125025747090537025

		# solution
			# python manual_cmp_webshots.py -c H10 -i 2022-02-04@04:00:00@H10US7X0Q8220220125025744039252025,2022-02-04@04:00:00@		H10US7X0Q8220220125025738788689025,2022-02-04@04:00:00@H10US7X0Q8220220125025747090537025

			# re webshot

	(W D-1) PROD-749	2/3/2022	2/3/2022	no webshots in build.com	Inriver-Testclient5 (H10)	
		'H10US7X0Q8220220125025740093080025',
		'H10US7X0Q8220220125025740227005025',
		'H10US7X0Q8220220125025741136006025',
		'H10US7X0Q8220220125025741758079025',
		'H10US7X0Q8220220125025741917613025',
		'H10US7X0Q8220220125025743898670025',
		'H10US7X0Q8220220125025745149715025',
		'H10US7X0Q8220220125025746627691025'

		# solution
			# no issues, resolved with re webshot

	(R D-1) PROD-751	2/3/2022	2/3/2022	spider issue in delivery quote in homedepot.com	Inriver-Testclient5 (H10)	
		'H10USAX0Q8220220125025725024078025',
		'H10USAX0Q8220220125025720509108025',
		'H10USAX0Q8220220125025716966096025',
		'H10USAX0Q8220220125025714949554025',
		'H10USAX0Q8220220125025712880300025',
		'H10USAX0Q8220220125025721534023025',
		'H10USAX0Q8220220125025721010583025',
		'H10USAX0Q8220220125025734842709025',
		'H10USAX0Q8220220125025727190453025',
		'H10USAX0Q8220220125025729026390025',
		'H10USAX0Q8220220125025738486021025',
		'H10USAX0Q8220220125025738178114025',
		'H10USAX0Q8220220125025737525951025',
		'H10USAX0Q8220220125025731661626025',
		'H10USAX0Q8220220125025736079626025',
		'H10USAX0Q8220220125025734005685025',
		'H10USAX0Q8220220125025716028256025',
		'H10USAX0Q8220220125025716185287025',
		'H10USAX0Q8220220125025713831202025',
		'H10USAX0Q8220220125025725641556025',
		'H10USAX0Q8220220125025736566326025',
		'H10USAX0Q8220220131092059417067031',
		'H10USAX0Q8220220125025736695904025',
		'H10USAX0Q8220220125025730833052025',
		'H10USAX0Q8220220125025732332363025'

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-04@04:00:00@H10USAX0Q8220220125025716185287025,2022-02-04@04:00:00@H10USAX0Q8220220125025732332363025,2022-02-04@04:00:00@H10USAX0Q8220220125025716966096025,2022-02-04@04:00:00@H10USAX0Q8220220125025725641556025,2022-02-04@04:00:00@H10USAX0Q8220220125025729026390025,2022-02-04@04:00:00@H10USAX0Q8220220125025714949554025,2022-02-04@04:00:00@H10USAX0Q8220220125025734005685025,2022-02-04@04:00:00@H10USAX0Q8220220125025716028256025,2022-02-04@04:00:00@H10USAX0Q8220220125025721534023025,2022-02-04@04:00:00@H10USAX0Q8220220125025725024078025,2022-02-04@04:00:00@H10USAX0Q8220220125025720509108025,2022-02-04@04:00:00@H10USAX0Q8220220125025727190453025,2022-02-04@04:00:00@H10USAX0Q8220220125025736566326025,2022-02-04@04:00:00@H10USAX0Q8220220125025731661626025,2022-02-04@04:00:00@H10USAX0Q8220220131092059417067031,2022-02-04@04:00:00@H10USAX0Q8220220125025712880300025,2022-02-04@04:00:00@H10USAX0Q8220220125025713831202025,2022-02-04@04:00:00@H10USAX0Q8220220125025736079626025,2022-02-04@04:00:00@H10USAX0Q8220220125025736695904025,2022-02-04@04:00:00@H10USAX0Q8220220125025737525951025,2022-02-04@04:00:00@H10USAX0Q8220220125025738178114025,2022-02-04@04:00:00@H10USAX0Q8220220125025738486021025,2022-02-04@04:00:00@H10USAX0Q8220220125025734842709025,2022-02-04@04:00:00@H10USAX0Q8220220125025730833052025,2022-02-04@04:00:00@H10USAX0Q8220220125025721010583025

	(R D-1) KEY-336	1/27/2022	1/27/2022	Zurn	missing specification in www.amazon.com
		'H10USW00Q8220220125025645737157025',
		'H10USW00Q8220220125025658777011025',
		'H10USW00Q8220220125025643015156025',
		'H10USW00Q8220220125025643149907025',
		'H10USW00Q8220220125025642887031025',
		'H10USW00Q8220220125025652932386025',
		'H10USW00Q8220220125025642382954025',
		'H10USW00Q8220220125025709414569025',
		'H10USW00Q8220220125025641650920025',
		'H10USW00Q8220220125025646593020025',
		'H10USW00Q8220220125025644012959025',
		'H10USW00Q8220220125025710734209025',
		'H10USW00Q8220220125025710921082025',
		'H10USW00Q8220220125025709886690025',
		'H10USW00Q8220220125025711090027025',
		'H10USW00Q8220220125025645931153025',
		'H10USW00Q8220220125025640727580025',

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-04@04:00:00@H10USW00Q8220220125025645737157025,2022-02-04@04:00:00@H10USW00Q8220220125025658777011025,2022-02-04@04:00:00@H10USW00Q8220220125025643015156025,2022-02-04@04:00:00@H10USW00Q8220220125025643149907025,2022-02-04@04:00:00@H10USW00Q8220220125025642887031025,2022-02-04@04:00:00@H10USW00Q8220220125025652932386025,2022-02-04@04:00:00@H10USW00Q8220220125025642382954025,2022-02-04@04:00:00@H10USW00Q8220220125025709414569025,2022-02-04@04:00:00@H10USW00Q8220220125025641650920025,2022-02-04@04:00:00@H10USW00Q8220220125025646593020025,2022-02-04@04:00:00@H10USW00Q8220220125025644012959025,2022-02-04@04:00:00@H10USW00Q8220220125025710734209025,2022-02-04@04:00:00@H10USW00Q8220220125025710921082025,2022-02-04@04:00:00@H10USW00Q8220220125025709886690025,2022-02-04@04:00:00@H10USW00Q8220220125025711090027025,2022-02-04@04:00:00@H10USW00Q8220220125025645931153025,2022-02-04@04:00:00@H10USW00Q8220220125025640727580025

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-04@04:00:00@H10USW00Q8220220125025645931153025

	-----
	Stats:
		Rerun: 3 
		Webshots: 2

02 05 2022
	(F D-1) 3161	2022-02-05	New CMS	110	description	www.fnac.be/nl	All
		sample item_id: 110BE9B0OL000001478210202107120649	
		Missing description; -1 value	
		https://prnt.sc/26oypme
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110BE9B0OL000001478210202107120649
			# https://www.nl.fnac.be/a14745920/51-Worldwide-Games-NL-SWITCH-Spel-Nintendo-Switch#omnsearchpos=1

			# fix get description
			# rebuild

			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-05' AND website = 'www.fnac.be/nl' AND item_id in ('110BE9B0OL000001478210202107120649')

			# successful rerun

	(R D-1) 3162	2022-02-05	New CMS	110	description	www.fun.be	All
		sample item_id: 110BEKW0OL000001475270202107070457
		Special character issue and incomplete description	
		https://prnt.sc/26oyrdv
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110BEKW0OL000001475270202107070457

			# https://www.fun.be/switch-51-worldwide-games-hol.html
			# rebuild

			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-05' AND website = 'www.fun.be' AND item_id in ('110BEKW0OL000001475270202107070457')

			# successful rerun

	------
	(R D-1) 1965	2022-02-05	New CMS	200	all	www.dns-shop.ru	
		портативная акустика	
		invalid deadlink	
		https://prnt.sc/26oxw17		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@21:00:00@200@eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g
			# rerun

	(R D-1) 1966	2022-02-05	New CMS	200	all	www.re-store.ru	
		/ акустика wi-fi - 6
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная колонка wi-fi - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 6 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26oxp46
		https://prnt.sc/26oxq9k
		https://prnt.sc/26oxqix

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-05@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-05@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
			# 8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-05@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# rerun

	(R D-1) 1967	2022-02-05	New CMS	200	all	www.saturn.de	
		kopfhörer bluetooth	
		Duplicate product_website & product_url
		https://prnt.sc/26oxzbp		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@200@bbc80f13-6ef7-4837-925a-128b2119fcdc@UbbyaApIEem-gAANOiOHiA

			# rerun

	(R D-1) 1968	2022-02-05	New CMS	200	all	www.elkjop.no	
		multiroom høyttalere	
			# Bang & Olufsen BeoPlay A9 4.0 med Google Assistant (sort/valnøtt)
			# https://prnt.sc/26oynuu

		Duplicate product_website & product_url
		https://prnt.sc/26oy0yn		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@200@9467b5d3-4dd2-4afd-96a9-e64e7536aba2@EGO2juO8EeaOEAANOrFolw

			# multiple reruns, screenshot from site

	(R D-1) 1969	2022-02-05	New CMS	200	all	www.currys.co.uk	
		bluetooth speaker

		Duplicate product_website & product_url
		https://prnt.sc/26oyevp		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@23:00:00@200@18KcljsFEeeFswANOiOHiA@IO4KJA6jEeeOEgANOrFolw

			# rerun

	(R D-1) 1970	2022-02-05	New CMS	U00	all	www.americanas.com.br	
		'controle game sem fio',
		'mini game portatil',
		'aparelho de video game',
		/ 'aparelho de game',
		'pro controller',
		'console nintendo switch',
		'nintendo switch',
		'joy con',
		'controle switch',
		'controle joy con',
		'console'

		Auto Copy Over for 1 day	

		https://prnt.sc/26oyj3u
		https://prnt.sc/26oyjdu
		https://prnt.sc/26oyjgv
		https://prnt.sc/26oyjt8
		https://prnt.sc/26oyjz1
		https://prnt.sc/26oyk3y
		https://prnt.sc/26oykb7
		https://prnt.sc/26oyki5
		https://prnt.sc/26oykrl
		https://prnt.sc/26oyl25
		https://prnt.sc/26oyl6m		

		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@mFIWHkasEeeMawANOiZi-g

			# 

	0 (R D-1) 1971	2022-02-05	New CMS	U00	all	www.kabum.com.br	
		console switch	

		Auto Copy Over for 3 days	
		https://prnt.sc/26oyln7		
		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@12969bc0-97ba-4c05-88fd-c8464cfe5206
			# check local: missing api
			# trello

	(R D-1) 1972	2022-02-05	New CMS	U00	all	www.shoptime.com.br	
		/ aparelho de game
			493085f2-179f-4689-b55f-8a2cb7d036a4@1QomMkasEeeMawANOiZi-g
		/ mini video game
			94b59958-8189-4a66-a160-a1e724e3ad52@1QomMkasEeeMawANOiZi-g
		/ controle nintendo switch	
			1398bc6a-9453-421d-96c0-0204b23f38e7@1QomMkasEeeMawANOiZi-g

		Data Count Mismatch 1 (Fetched Data is Lesser than expected	

		https://prnt.sc/26oym84
		https://prnt.sc/26oymqh
		https://prnt.sc/26oymzv		

		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@1QomMkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@1QomMkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@1QomMkasEeeMawANOiZi-g

		# rerun

	(R D-1) 1973	2022-02-05	New CMS	U00	all	www.shoptime.com.br	
		nintendo switch	
		Auto Copy Over fr 1 day	
		https://prnt.sc/26oyn9l		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@1QomMkasEeeMawANOiZi-g
			# rerun

	(R D-1) 1974	2022-02-05	New CMS	110	all	www.mediamarkt.be/nl/	
		Nintendo Switch rouge/bleu	
		Auto Copy Over fr 1 Day	
		https://prnt.sc/26oycih
		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@2cd65bf0-faea-4d88-bd15-1ec07d394012@XEf8HrstEeidIgANOiZi-g
			# check local: redirect
				# https://prnt.sc/26p2ca7
				# valid deadlink

	0 (R D-1) 1975	2022-02-05	New CMS	110	all	www.dreamland.be/nl	
		Nintendo Switch grijs
			f4e20a67-a810-4e90-96fa-a1bf44470e55@85cbcb48-e115-4712-95cd-b52f782a7188
		Nintendo Switch Lite	
			36ca1602-96f4-4497-bfff-a93bd2deb067@85cbcb48-e115-4712-95cd-b52f782a7188

		Data Count Mismatch 16 (Fetched Data is Lesser than expected)
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26oyh8t
		https://prnt.sc/26oyiom	

		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@f4e20a67-a810-4e90-96fa-a1bf44470e55@85cbcb48-e115-4712-95cd-b52f782a7188,2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@85cbcb48-e115-4712-95cd-b52f782a7188

			# check local: no issues

			# local, site, rmq, good. maybe server
				# add cloudscraper (issues may raise on server)

			# same issue: treello

		# sql
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.dreamland.be/nl' and 
			# var2.keyword in (N'Nintendo Switch grijs') and 
			# var2.[date] = '2022-02-05' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, c 1ountry, source

	0 (R D-1) 1976	2022-02-05	New CMS	110	all	www.fnac.be/fr	
		console de jeu - 25
			aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g
		Nintendo Switch console - 25
			8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g
		/ Nintendo Switch Lite - 25
			36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26oyky0
		https://prnt.sc/26oylre
		https://prnt.sc/26oym89	

		yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g, 2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

			# check local: removed page breaker, goods

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g

			# multiple reruns failed

			# add cloudscraper

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g

			# same issue: trello

		# sql
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.fnac.be/fr' and 
			# var2.keyword in (N'Nintendo Switch console') and 
			# var2.[date] = '2022-02-05' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

	(R D-1) 1977	2022-02-05	New CMS	110	all	www.fnac.be/nl	
		Nintendo Switch rood/blauw - 6
			5084f045-4388-4158-b464-26cea5f48278@WWIF8OOfEeeyugANOiZi-g
			https://prnt.sc/26p1dnf
		Nintendo Switch software - 2
			4f38cc0c-4e8d-4198-a680-ad513d998c36@WWIF8OOfEeeyugANOiZi-g
			https://prnt.sc/26p1dtt

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	

		https://prnt.sc/26oyp1m
		https://prnt.sc/26oypxc	
		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@5084f045-4388-4158-b464-26cea5f48278@WWIF8OOfEeeyugANOiZi-g,2022-02-05@22:00:00@110@4f38cc0c-4e8d-4198-a680-ad513d998c36@WWIF8OOfEeeyugANOiZi-g

			# multiple reruns, investigate site, valid deadlink

	(R D-1) 1978	2022-02-05	New CMS	110	all	www.fun.be	
		'Nintendo Switch console',
		'Nintendo Switch grijs',
		'Nintendo Switch Lite',
		'Nintendo Switch rood/blauw',
		'Nintendo Switch software',
		'spelcomputer',
		'Switch software'

		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days
		Auto Copy Over fr 3 Days	

		https://prnt.sc/26oys26
		https://prnt.sc/26oyse8
		https://prnt.sc/26oysyf
		https://prnt.sc/26oyt9d
		https://prnt.sc/26oytn3
		https://prnt.sc/26oyu0p
		https://prnt.sc/26oyuaa	

		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@f4e20a67-a810-4e90-96fa-a1bf44470e55@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@5084f045-4388-4158-b464-26cea5f48278@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@4f38cc0c-4e8d-4198-a680-ad513d998c36@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@e772bc6b-f949-4561-a365-1de2e82b0bef@091e826a-4416-498d-a006-69f1435b3220,2022-02-05@22:00:00@110@2d06fcbc-4f8c-4dd0-b85d-35a3af94b7f5@091e826a-4416-498d-a006-69f1435b3220

	(R D-1) 1979	2022-02-05	New CMS	200	all	www.target.com	
		/ portable bluetooth speaker
			83f9d554-437c-487c-b214-6a015bf9f65b@0l0XDL-yEei-eQANOiOHiA
		/ portable bluetooth speakers
			5489b3c3-6386-4225-822b-cb7704c142fd@0l0XDL-yEei-eQANOiOHiA
		/ wifi speaker
			e447257e-9396-4735-9af7-68d864a12052@0l0XDL-yEei-eQANOiOHiA
		wireless bluetooth earbuds	
			58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA
			https://prnt.sc/26p1a53

		Duplicate product_website & product_url

		https://prnt.sc/26p02yn
		https://prnt.sc/26p03di
		https://prnt.sc/26p04er
		https://prnt.sc/26p04xa		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@04:00:00@200@83f9d554-437c-487c-b214-6a015bf9f65b@0l0XDL-yEei-eQANOiOHiA,2022-02-05@04:00:00@200@5489b3c3-6386-4225-822b-cb7704c142fd@0l0XDL-yEei-eQANOiOHiA,2022-02-05@04:00:00@200@e447257e-9396-4735-9af7-68d864a12052@0l0XDL-yEei-eQANOiOHiA,2022-02-05@04:00:00@200@58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@04:00:00@200@58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA

			# rerun, screenshot

	-----

	(R D-1) 0 676	2022-02-05	New CMS	110	all 	www.fun.be	
		Hardware
		Software	

		Autocopy Over for 3 days
		Autocopy Over for 3 days	

		https://prnt.sc/26ozi5y
		https://prnt.sc/26ozie3	
		Yes	Richelle

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@KW0@365fe49d-5be9-4d22-bd6f-f6f62e7de277,2022-02-05@22:00:00@110@KW0@c300d55f-4500-4da8-a9a3-895711ab750c

			# check local
			# Trello

	(F D-1) 677	2022-02-05	New CMS	110	all 	www.nedgame.nl	
		hardware
			110@411@9452d914-47d4-499c-bd00-09fb39168c22
		software	
			110@411@fc0849dc-6b31-4c6e-bb5c-48729a7566d1
		
		Autocopy Over for 2 days
		Autocopy Over for 2 days

		https://prnt.sc/26ozkbu
		https://prnt.sc/26ozkoz
		Yes Richelle

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@411@9452d914-47d4-499c-bd00-09fb39168c22, 2022-02-05@22:00:00@110@411@fc0849dc-6b31-4c6e-bb5c-48729a7566d1

			# check local, fix, push
			# for rebuild

			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-05' 
			# 	AND website = 'www.nedgame.nl' 
			# 	AND category in (N'Hardware','Software')
			# 	GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@411@9452d914-47d4-499c-bd00-09fb39168c22
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@411@fc0849dc-6b31-4c6e-bb5c-48729a7566d1

			# rerun

	-----
	Stats:
		Rerun - 16 (16)
		Fix - 2 * (1*2) = 4

02 06 22
	(R D-1) 3176	2022-02-06	New CMS	K00	price_value	mvideo.ru	
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP803F000001449440202106150452',
		'K00RUP803F000001527480202108020954',
		'K00RUP803F000001527440202108020954',
		'K00RUP803F000001449180202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000000980400202006250902',
		'K00RUP80CG000001245120202011170413',
		'K00RUP80CG000001310890202101250600',
		'K00RUP80CG000001310870202101250600',
		'K00RUP80CG000001524900202108020807',
		'K00RUP80CG000001524830202108020807',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000001311080202101250600',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449690202106150452',
		'K00RUP805K000001527570202108020954',
		'K00RUP805K000001527540202108020954',
		'K00RUP805K000001527500202108020954',
		'K00RUP80CG000001310990202101250600',
		'K00RUP80CG000000980380202006250902',
		'K00RUP80CG000001373010202104050819',
		'K00RUP80CG000001310810202101250600',
		'K00RUP80CG000000980440202006250902',
		'K00RUP80CG000000980430202006250902',
		'K00RUP80CG000001245170202011170413'

		Wrong stock
		Yes	Mayeeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-06@21:00:00@K00RUP80AD000001449550202106150452,2022-02-06@21:00:00@K00RUP80AD000001527640202108020954,2022-02-06@21:00:00@K00RUP803F000001449440202106150452,2022-02-06@21:00:00@K00RUP803F000001527480202108020954,2022-02-06@21:00:00@K00RUP803F000001527440202108020954,2022-02-06@21:00:00@K00RUP803F000001449180202106150452,2022-02-06@21:00:00@K00RUP80DO000001449630202106150452,2022-02-06@21:00:00@K00RUP80CG000000980370202006250902,2022-02-06@21:00:00@K00RUP80CG000000980400202006250902,2022-02-06@21:00:00@K00RUP80CG000001245120202011170413,2022-02-06@21:00:00@K00RUP80CG000001310890202101250600,2022-02-06@21:00:00@K00RUP80CG000001310870202101250600,2022-02-06@21:00:00@K00RUP80CG000001524900202108020807,2022-02-06@21:00:00@K00RUP80CG000001524830202108020807,2022-02-06@21:00:00@K00RUP80CG000001310970202101250600,2022-02-06@21:00:00@K00RUP80CG000001311080202101250600,2022-02-06@21:00:00@K00RUP80YO000001449660202106150452,2022-02-06@21:00:00@K00RUP80YO000001449690202106150452,2022-02-06@21:00:00@K00RUP805K000001527570202108020954,2022-02-06@21:00:00@K00RUP805K000001527540202108020954,2022-02-06@21:00:00@K00RUP805K000001527500202108020954,2022-02-06@21:00:00@K00RUP80CG000001310990202101250600,2022-02-06@21:00:00@K00RUP80CG000000980380202006250902,2022-02-06@21:00:00@K00RUP80CG000001373010202104050819,2022-02-06@21:00:00@K00RUP80CG000001310810202101250600,2022-02-06@21:00:00@K00RUP80CG000000980440202006250902,2022-02-06@21:00:00@K00RUP80CG000000980430202006250902,2022-02-06@21:00:00@K00RUP80CG000001245170202011170413

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-06@21:00:00@K00RUP80CG000001524830202108020807

			# multiple reruns successful

	(R D-1) 3177	2022-02-06	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP804K000001527200202108020954',
		'K00RUP804K000001528810202108020955',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001527250202108020954',
		'K00RUP804K000001528820202108020955',
		'K00RUP803F000001449360202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001449160202106150452',
		'K00RUP803F000001527310202108020954',
		'K00RUP803F000001449330202106150452',
		'K00RUP80CG000000980360202006250902',
		'K00RUP80CG000001592510202109200556',
		'K00RUP80CG000001310930202101250600'

		wrong ratings
		Yes	Mayeeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-06@21:00:00@K00RUP804K000001527200202108020954,2022-02-06@21:00:00@K00RUP804K000001528810202108020955,2022-02-06@21:00:00@K00RUP804K000001449090202106150452,2022-02-06@21:00:00@K00RUP804K000001527250202108020954,2022-02-06@21:00:00@K00RUP804K000001528820202108020955,2022-02-06@21:00:00@K00RUP803F000001449360202106150452,2022-02-06@21:00:00@K00RUP803F000001449420202106150452,2022-02-06@21:00:00@K00RUP803F000001449160202106150452,2022-02-06@21:00:00@K00RUP803F000001527310202108020954,2022-02-06@21:00:00@K00RUP803F000001449330202106150452,2022-02-06@21:00:00@K00RUP80CG000000980360202006250902,2022-02-06@21:00:00@K00RUP80CG000001592510202109200556,2022-02-06@21:00:00@K00RUP80CG000001310930202101250600

			# https://www.mvideo.ru/products/igrovaya-mysh-razer-deathadder-v2-rz01-03210100-r3m1-50133463

			# multiple reruns successflul

	(R D-1) 3179	2022-02-06	New CMS	F00	all	mediamarkt.be/nl/	All item_IDs	
		No Data		
		Yes	Glenn

		# solution
			# check previous date in sql
			# get atleast 1 item id
			# check couch: status: created

			# get uuid in couch: XEf8HrstEeidIgANOiZi

			# flags and argument (use python main.py --help)
				# -c company
				# -d date
				# -t time
				# -r uuid
				# -j metrics

			# python main.py -c F00 -d 2022-02-06 -t 22:00:00 -r XEf8HrstEeidIgANOiZi-g -j COMPARISON

			# re trigger successful

	(R D-1) 3180	2022-02-06	New CMS	100	all	hylte-lantman.com	
		100SEDH010000001224180202010280943
		100SEDH010000001321260202102050343	

		incorrect scraping of title, affecting other essential data such as Availabiliy
		https://prnt.sc/26pffx6	Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-06@22:00:00@100SEDH010000001224180202010280943,2022-02-06@22:00:00@100SEDH010000001321260202102050343

			# rerun successful

	(R D-1) 3191	2022-02-06	New CMS	K00	rating_score	alternate.de	
		K00DETF03F000001538370202108110153
		K00DETF0CG000001486500202107130336

		-3 in ratinsg amd reviews		
		Yes	Mayeeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-06@22:00:00@K00DETF0CG000001486500202107130336,2022-02-06@22:00:00@K00DETF03F000001538370202108110153

			# rerun successful

	-----
	(R D-1) 0 681	2022-02-06	New CMS	200	all 	www.currys.co.uk	
		'Headphones Bluetooth In Ear',
			https://www.currys.co.uk/gbuk/audio-and-headphones/headphones/headphones/291_3919_31664_xx_ba00010486-bv00308330%7Cbv00308562/xx-criteria.html
		'Headphones Bluetooth On Ear',
			https://www.currys.co.uk/gbuk/audio-and-headphones/headphones/headphones/291_3919_31664_xx_ba00010486-bv00308328%7Cbv00308329%7Cbv00308562/xx-criteria.html
		'Speakers Bluetooth',
			https://www.currys.co.uk/gbuk/audio-and-headphones/audio/hifi-systems-and-speakers/550_4278_31971_xx_ba00010892-bv00312686/xx-criteria.html
		'Speakers Multiroom',
			https://www.currys.co.uk/gbuk/multi-room-speakers/audio/hifi-systems-and-speakers/550_4278_31971_xx_ba00013487-bv00313067/xx-criteria.html
		'Speakers Portable',
			https://www.currys.co.uk/gbuk/portable-bluetooth-speakers/audio/hifi-systems-and-speakers/550_4278_31971_xx_ba00013487-bv00313062/xx-criteria.html
		'Speakers Voice',
			https://www.currys.co.uk/gbuk/audio-and-headphones/audio/hifi-systems-and-speakers/550_4278_31971_xx_ba00013488-bv00313073%7Cbv00313072%7Cbv00313074/xx-criteria.html	
		'Headphones Noise Cancelling'
			https://www.currys.co.uk/gbuk/noise-cancelling-headphones/headphones/headphones/291_3919_31664_xx_ba00010486-bv00308563/xx-criteria.html

		Auto copy over for 3 days	

		https://prnt.sc/26pef5z	Yes	MICHAEL

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-06@23:00:00@200@I00@8418fd8c-b98c-4892-9ae2-5a4065f702b2,2022-02-06@23:00:00@200@I00@74a2eec0-912a-4405-8783-08a2ee3a0848,2022-02-06@23:00:00@200@I00@284ad053-ee6e-4521-8c10-c5cd30558cff,2022-02-06@23:00:00@200@I00@9506fe79-bbf2-4cba-b8fe-a94bc558925d,2022-02-06@23:00:00@200@I00@c0260ca6-fd02-41f2-8b98-e0023ae723d7,2022-02-06@23:00:00@200@I00@d3c3171d-6ed9-4aa0-8e5d-2a4c9d8d6360,2022-02-06@23:00:00@200@I00@2215ddb3-ad7e-4d14-b170-6e07913924ba

			# valid deadlink
				# https://prnt.sc/26pguwg
				# https://prnt.sc/26pgvvp

			# data difficult to extract, requires more time
			# trello

			# unblocker blocked (5:37pm fix)
				# https://prnt.sc/26pkj7p

	(R D-1) Trello 1 
		1970	2022-02-05	New CMS	U00	all	www.americanas.com.br	
			'controle game sem fio',
			'mini game portatil',
			'aparelho de video game',
			'aparelho de game',
			'pro controller',
			'console nintendo switch',
			'nintendo switch',
			'joy con',
			'controle switch',
			'controle joy con',
			'console'

			Auto Copy Over for 1 day	

			https://prnt.sc/26oyj3u
			https://prnt.sc/26oyjdu
			https://prnt.sc/26oyjgv
			https://prnt.sc/26oyjt8
			https://prnt.sc/26oyjz1
			https://prnt.sc/26oyk3y
			https://prnt.sc/26oykb7
			https://prnt.sc/26oyki5
			https://prnt.sc/26oykrl
			https://prnt.sc/26oyl25
			https://prnt.sc/26oyl6m		

			kurt

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@mFIWHkasEeeMawANOiZi-g

				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@mFIWHkasEeeMawANOiZi-g,2022-02-05@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@mFIWHkasEeeMawANOiZi-g

			# solution
				# no issues in local
				# rerun but cant verify because the data is previous date and copy override

	(R D-1) 0 Trello 2 
		676	2022-02-05	New CMS	110	all www.fun.be	
			Hardware
				https://www.fun.be/multimedia/gaming/nintendo-switch/consoles.html
				# redirected: https://prnt.sc/26pixop
			Software	
				https://www.fun.be/multimedia/gaming/nintendo-switch/games.html
				# redirected: https://prnt.sc/26piy1f

			Autocopy Over for 3 days
			Autocopy Over for 3 days	

			https://prnt.sc/26ozi5y
			https://prnt.sc/26ozie3	
			Yes	Richelle

			# solution
				# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@KW0@365fe49d-5be9-4d22-bd6f-f6f62e7de277,2022-02-05@22:00:00@110@KW0@c300d55f-4500-4da8-a9a3-895711ab750c

				# check local
				# Trello

			# solution
				# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				# where var2.website = 'www.fun.be' and 
				# var2.category in (N'Hardware') and 
				# var2.[date] = '2022-02-05' 
				# GROUP BY date, website, category, product_website, category_url, litem_id, country, source

				# redirected: wait for setup's decision
					# screenshots per categories provided

	(R D-1) 0 Trello 3
		1975	2022-02-05	New CMS	110	all	www.dreamland.be/nl	
			nintendo switch grijs
				f4e20a67-a810-4e90-96fa-a1bf44470e55@85cbcb48-e115-4712-95cd-b52f782a7188
			Nintendo Switch Lite	
				36ca1602-96f4-4497-bfff-a93bd2deb067@85cbcb48-e115-4712-95cd-b52f782a7188

			Data Count Mismatch 16 (Fetched Data is Lesser than expected)
			Data Count Mismatch 3 (Fetched Data is Lesser than expected)	

			https://prnt.sc/26oyh8t
			https://prnt.sc/26oyiom	

			Yes	richelle

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@f4e20a67-a810-4e90-96fa-a1bf44470e55@85cbcb48-e115-4712-95cd-b52f782a7188,2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@85cbcb48-e115-4712-95cd-b52f782a7188

				# check local: no issues
				# local, site, rmq, good. maybe server
					# add cloudscraper (issues may raise on server)
				# same issue: trello

			# sql
				# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# where var2.website = 'www.dreamland.be/nl' and 
				# var2.keyword in (N'Nintendo Switch grijs') and 
				# var2.[date] = '2022-02-05' 
				# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

				# no issue in local: set main trigger? to test the data? 

	(R D-1) 0 1976	2022-02-05	New CMS	110	all	www.fnac.be/fr	
			console de jeu - 25
				aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g
			Nintendo Switch console - 25
				8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g
			/ Nintendo Switch Lite - 25
				36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

			Data Count Mismatch 5 (Fetched Data is Lesser than expected)
			Data Count Mismatch 5 (Fetched Data is Lesser than expected)
			Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

			https://prnt.sc/26oyky0
			https://prnt.sc/26oylre
			https://prnt.sc/26oym89	

			yes	richelle

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g

				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g, 2022-02-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

				# check local: removed page breaker, goods

				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g

				# multiple reruns failed

				# add cloudscraper

				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-05@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g,2022-02-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g

				# same issue: trello

			# sql
				# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# where var2.website = 'www.fnac.be/fr' and 
				# var2.keyword in (N'Nintendo Switch console') and 
				# var2.[date] = '2022-02-05' 
				# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

	-----
	Stats:
		Rerun: 10 (10)

02 09 22
	(F D-1) Special Fix
		* mediamarkt.be/nl

		https://www.mediamarkt.be/nl/product/_apple-iphone-13-pro-5g-512-gb-sierra-blue-mlvh3zd-a-1978776.html

		# solution
			# change selector

				# cluster_12	
					# https://www.mediamarkt.be/nl/product/_apple-iphone-13-pro-5g-512-gb-sierra-blue-mlvh3zd-a-1978776.html

				#  online > div.delvrmsg
					# https://www.mediamarkt.be/nl/product/_samsung-smartphone-galaxy-a32-5g-128-gb-awesome-white-sm-a326bzwveub-1937773.html

				# for rebuild
				# for rerun

				# goods all successful after rerun

	(R D-1) 0 Trello
		Issue Description: Data Count Mismatch  (Fetched Data is Lesser than expected)

		Date: February 5, 2022

		Retailer: 
		www.fnac.be/fr

		Keywords:
		console de jeu
		Nintendo Switch console
		Nintendo Switch Lite

		Screenshots:
		https://prnt.sc/26oyky0
		https://prnt.sc/26oylre
		https://prnt.sc/26oym89

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# where var2.website = 'www.fnac.be/fr' and 
				# var2.keyword in (N'console de jeu') and 
				# var2.[date] = '2022-02-05' 
				# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# already added clouscraper last 02-05-2021
			# moved trello to retest
			# cant rerun or verify sql because it was not triggered at current date

		-----

		Issue Description:
		Data Count Mismatch 16 (Fetched Data is Lesser than expected)
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)

		Date:
		February 5, 2022

		Retailer:
		www.dreamland.be/nl

		Keywords:
		Nintendo Switch grijs
		Nintendo Switch Lite

		Screenshots:
		https://prnt.sc/26oyh8t
		https://prnt.sc/26oyiom

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# where var2.website = 'www.dreamland.be/nl' and 
				# var2.keyword in (N'Nintendo Switch Lite') and 
				# var2.[date] = '2022-02-05' 
				# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source
			# already added clouscraper last 02-05-2021
			# moved trello to retest
			# cant rerun or verify sql because it was not triggered at current date

	-----
	(R D-1) 3269	2022-02-09	New CMS	H10	webshot_url amazon.com
		homedepot.com	

		select * from view_all_productdata where date='2022-02-09' and website in ('www.amazon.com', 'www.homedepot.com')
		and webshot_url is NULL 

		order by item_id	
		No Webshots		
		Yes	Keeshia

		# solution
			# rerun sent
			# rerun sent again
			# successful rerun

	(F D-1) 3270	2022-02-09	New CMS	200		elgiganten.dk	
		200DK100Q0000000032320201901180326	
		Incorrect PRice, stocks and DQ		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-09@22:00:00@200DK100Q0000000032320201901180326
			# wrong fetched details after rerun

			# check local:
				# https://www.elgiganten.dk/product/tv-lyd-billede/hojtalere-hi-fi/hojttalere/ultimate-ears-megaboom-tradlos-hojtaler-sort/UEMEGABOOMBK

				# use proxy, 
					# change price but weird
					# DQ, cant extract dq because it is out of stock: https://prnt.sc/26rrvyu

				# fix price extractor
				# push
				# rebuild
				# test rerun
	-----

	(R D-1) 2025	2022-02-09	New CMS	K00	all	www.box.co.uk	
		Gaming Keyboard	
		Invalid Deadlink	
		https://prnt.sc/26rln98		
		kurts

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-09' 
			# AND website = 'www.box.co.uk' 
			# AND keyword in (N'Gaming Keyboard')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-09@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# successful rerun

	(R D-2) 2036	2022-02-09	New CMS	K00	all	www.pcdiga.com	
		'Gamepads',
		'Gaming Controller',
		'Gaming Headset',
		'Headset',
		'Rato gaming',
		'Rato óptico',
		'Rato wireless',
		'Tapete gaming',
		'Tapetes',
		'Teclado',
		'Teclado gaming',
		'Teclado mecanico',
		'Teclado Mecânico'

		2 Days ACO data
		3 Days ACO data

		https://prnt.sc/26rmssx
		Yes	Rayyan

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-09' 
			# AND website = 'www.pcdiga.com' 
			# AND keyword in (N'Gamepads',
			# 		'Gaming Controller',
			# 		'Gaming Headset',
			# 		'Headset',
			# 		'Rato gaming',
			# 		'Rato óptico',
			# 		'Rato wireless',
			# 		'Tapete gaming',
			# 		'Tapetes',
			# 		'Teclado',
			# 		'Teclado gaming',
			# 		'Teclado mecanico',
			# 		'Teclado Mecânico'
			# )
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-09@23:00:00@K00@ae171b50-e71a-4c7e-9ad1-3d081ca9e584@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@4b5135ed-31a3-436d-8b08-d6bf94572c14@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@9ce256ab-4183-4ec9-b02e-35f70b3310c9@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@45397c61-2b3a-455e-823d-0d0ddce42788@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@61afd478-d63f-4d49-a87e-b238926a309c@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@27a20aff-2cb5-4665-a696-49cac626dd27@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@08144165-c782-4c40-8016-6e870cbd5d06@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@850a97a6-58af-4825-a547-ee14be05e68c@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@9354ac9a-fd2e-463d-86e6-0ea97cc0eff4@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@c3066c54-6897-4686-8859-7d471a5ebb48@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@edca00b4-c316-492b-9d0f-f06295f3a545@9da6a206-89f0-4464-b067-6931614ba09b,2022-02-09@23:00:00@K00@6184a352-a19e-47f4-8fcd-1ac7b7ecfbbd@9da6a206-89f0-4464-b067-6931614ba09b

			# 2022-02-09@23:00:00@K00@4b5135ed-31a3-436d-8b08-d6bf94572c14@9da6a206-89f0-4464-b067-6931614ba09b
			# 2022-02-09@23:00:00@K00@61afd478-d63f-4d49-a87e-b238926a309c@9da6a206-89f0-4464-b067-6931614ba09b
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-09@23:00:00@K00@4b5135ed-31a3-436d-8b08-d6bf94572c14@9da6a206-89f0-4464-b067-6931614ba09b, 2022-02-09@23:00:00@K00@61afd478-d63f-4d49-a87e-b238926a309c@9da6a206-89f0-4464-b067-6931614ba09b

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-09@23:00:00@K00@61afd478-d63f-4d49-a87e-b238926a309c@9da6a206-89f0-4464-b067-6931614ba09b

			# Rato óptico : check local, goods

			# successful rerun after 6 attempts
				# blocked

	(R D-2) 2037	2022-02-09	New CMS	K00	all	www.pcdiga.com	
		Wireless Headset
		Invalid Deadlink
		https://prnt.sc/26rmunp	
		Yes	Rayyan

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-09' 
			# AND website = 'www.pcdiga.com' 
			# AND keyword in (N'Wireless Headset')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-09@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@9da6a206-89f0-4464-b067-6931614ba09b
			# multiple reruns failed
			# successful rerun after 6 attempts

	-----
	(R D-1) 711	2022-02-09	New CMS	K00	all	www.alza.cz	
		Keyboards	
			K00@360@87617f4d-882c-49d6-8341-a68a3ab87869
		Data Count Mismatch 1 (Fetched Data is Lesser than expected	
		https://prnt.sc/26rlwru	
		Yes	Jairus

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
			# where var2.website = 'www.alza.cz' and 
			# var2.category in (N'Keyboards') and 
			# var2.[date] = '2022-02-09' 
			# GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-09@22:00:00@K00@360@87617f4d-882c-49d6-8341-a68a3ab87869

			# successful rerun

	Nintendo Benelux
	Samsung Mobile
	motorola maintenance

	Stats:
		Fix: 1
		Rerun: 7 (9)


02 10 22
	Trello
	(F D-2) Motorola
		Issue Details:
		Data Count Mismatch 18 (Fetched Data is Lesser than expected)

		cause : Need further Investigation

		Date: 01/18/2022

		Retailer: www.mediamarkt.es

		Keyword(s):

		motorola edge 20 pro

		Screenshot(s):
		https://prnt.sc/26eyyxt
		https://prnt.sc/26eywud

		# solution
			# fix headers
			# push
			# for rebuild
			# fda07abe-85c6-4162-8544-5256a1153f2b@m26IDkXlEeeMawANOiZi-g

	(R D-1) 0 MSPRICE
		Date: January 13, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/en-ca
		Issue: Incorrect Stock status

		SAMPLE ITEM ID:
		'A00CA640KT020211025061827303632298',
		'A00CA640KT020211025061827429306298',
		'A00CA640KT020211025061827181260298',
		'A00CA640KT020211025061827029252298',
		'A00CA640KT020211025061826781227298',
		'A00CA640KT020211025061821542564298',

		# solution
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard/90FJ6ZKW1RZB/J14K?activetab=pivot%3aoverviewtab
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard/90FJ6ZKW1RZB/G0QH?activetab=pivot%3aoverviewtab
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard-with-slim-pen-2/8VDB5VRKC5TQ/KHJ3?activetab=pivot%3aoverviewtab
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard-with-slim-pen-2/8VDB5VRKC5TQ/M6M3?activetab=pivot%3aoverviewtab
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard-with-slim-pen-2/8VDB5VRKC5TQ/6V0M?activetab=pivot%3aoverviewtab
			# https://www.microsoft.com/en-ca/d/surface-pro-signature-keyboard-with-slim-pen-2/8VDB5VRKC5TQ/H58Z?activetab=pivot%3aoverviewtab
		
			# check site: add cart as (stock indicator?)
				# check rankings or listings page and find availability indicator

				# https://www.microsoft.com/en-ca/d/samsung-galaxy-s21-5g-unlocked-essentials-bundle/8w0z48xkxl3r?cid=msft_web_collection&activetab=pivot%3aoverviewtab
				# https://www.microsoft.com/en-ca/d/surface-duo-unlocked/8p98gbqkdzl5?cid=msft_web_collection&activetab=pivot:overviewtab
				# OOS category
					# https://www.microsoft.com/en-ca/store/collections/windowsphones?icid=PhonesCat_R1_CP2_AllPhones_040921

	(R D-1) 3282	2022-02-10	New CMS	O00	all	www.krefel.be/nl	
		O00BEIS040000001374290202104060658
		O00BEIS040000001392700202104270517	
		invalid deadlink		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-10@22:00:00@O00BEIS040000001374290202104060658,2022-02-10@22:00:00@O00BEIS040000001392700202104270517

			# successful rerun

	(R D-1) 3283	2022-02-10	New CMS	O00	all	www.krefel.be/nl	
		O00BEIS040000001373930202104060658	
		missing data		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-10@22:00:00@O00BEIS040000001373930202104060658
			# successful rerun

	(R D-1) 3293	2022-02-10	New CMS	P00	all	ozon.ru	
		P00RU1W01L000001559220202108230324
		P00RU1W01L000001454890202106220209
		P00RU1W01L000001460560202106280511	

		invalid deadlinks		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-10@21:00:00@P00RU1W01L000001559220202108230324,2022-02-10@21:00:00@P00RU1W01L000001454890202106220209,2022-02-10@21:00:00@P00RU1W01L000001460560202106280511

			# successful rerun

	(R D-1) 3297	2022-02-10	New CMS	110	all	dreamland.be/fr	
		110BEQW0OL000001477850202107120648
		110BEQW0OL000001517170202107260528	
		invalid deadlinks		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAW_FINISHED -l 2022-02-10@22:00:00@110BEQW0OL000001477850202107120648,2022-02-10@22:00:00@110BEQW0OL000001517170202107260528

			# https://www.dreamland.be/e/fr/dl/nintendo-switch-new-super-mario-brosu-deluxe-fr-151280--2
			# https://www.dreamland.be/e/fr/dl/nintendo-switch-oled-blanc-125086

			# successful rerun

	(F D-1) 3304	2022-02-10	New CMS	X00	all	mercadolibre.com.mx	
		X00MXEV0OL000001439330202106080517	
		invalid deadlink		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-10@05:00:00@X00MXEV0OL000001439330202106080517

			# check couch: issue with post transform
			# check local: issues in et, post transform
			# rerun after rebuild

			# 

	-----
	(R D-1) 2047	2022-02-10	New CMS	O00	all	www.bol.com	
		Amerikaanse koelkast
		Droogkast aanbieding	

		auto copy over fofrr 1 day	

		https://prnt.sc/26shc99
		https://prnt.sc/26shbmy	

		keen

		# solution
			# de3f1595-821b-4d9f-ae3c-ff9b6b1354df@UAjszpOqEei-eQANOiOHiA
			# 9356dbf1-0c89-4b84-af3d-9deaa9d7a30d@UAjszpOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-10@22:00:00@O00@de3f1595-821b-4d9f-ae3c-ff9b6b1354df@UAjszpOqEei-eQANOiOHiA,2022-02-10@22:00:00@O00@9356dbf1-0c89-4b84-af3d-9deaa9d7a30d@UAjszpOqEei-eQANOiOHiA

			# rerun

	(R D-1) 2048	2022-02-10	New CMS	O00	all	www.mediamarkt.nl	
		Droogkast	
		Data Count Mismatch 1(Fetched Data is Greater than expected)	
		https://prnt.sc/26shg5d		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-10@22:00:00@O00@1ea0d70a-84c0-4393-a189-ad3c692e5fc8@-Sv-LJOqEei-eQANOiOHiA
			# successful rerun

	(R D-1) 2050	2022-02-10	New CMS	200	all	www.dns-shop.ru
		наушники накладные bluetooth
			4df6b44c-9ad7-40e1-bca8-3569af8ba83e@BzxplkXeEeeMawANOiZi-g

		Incorrect Product_website and Product_URL fetched

		https://prnt.sc/26shlzn
		Yes	Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-10@21:00:00@200@4df6b44c-9ad7-40e1-bca8-3569af8ba83e@BzxplkXeEeeMawANOiZi-g
			# rerun failed
			# successful rerun

	(R D-1)2051	2022-02-10	New CMS	200	all	www.dns-shop.ru
			портативная колонка wi-fi
				8e970e2e-b775-46bd-b172-00c845d5252c@BzxplkXeEeeMawANOiZi-g
			Invalid Deadlink
			https://prnt.sc/26shozv
			Yes	Rayyan

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-10@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@BzxplkXeEeeMawANOiZi-g
				# successful multiple reruns

	Stats:
		Rerun: (8) = 1
		Fix: (1(2))

02 11 22
	(R D-1) 0 Setup
		KEY-339	2/8/2022	2/8/2022	Zurn	Invalid deadlink in www.grainger.com
			H10US421Q8220220207035943959249038
			H10US421Q8220220207035945176448038
			H10US421Q8220220207035946135299038

			# solution
				# https://www.grainger.com/product/ZURN-WILKINS-Reduced-Pressure-Zone-Backflow-6AVY0?searchQuery=1-375&searchBar=true
				# https://www.grainger.com/product/ZURN-WILKINS-Reduced-Pressure-Zone-Backflow-45K858?searchQuery=34-375XL&searchBar=true
				# https://www.grainger.com/product/ZURN-WILKINS-Water-Pressure-Reducing-Valve-22N574?searchQuery=112-600XL&searchBar=true

				# 

		PROD-767	2/10/2022	2/10/2022	ETL Failure in Availability in ferguson.com	Inriver-Testclient5 (H10)	
			'H10US521Q8220220207040630885993038',
			'H10US521Q8220220207040629731357038',
			'H10US521Q8220220207040642445527038',
			'H10US521Q8220220207040649671943038',
			'H10US521Q8220220207040636599478038',
			'H10US521Q8220220207040636780752038',
			'H10US521Q8220220207040629010903038',
			'H10US521Q8220220207040633145534038',
			'H10US521Q8220220207040628805419038',
			'H10US521Q8220220207040644634229038',
			'H10US521Q8220220207040645269986038',
			'H10US521Q8220220207040646225702038',
			'H10US521Q8220220207040645922828038',
			'H10US521Q8220220207040645615651038',
			'H10US521Q8220220207040646504051038',
			'H10US521Q8220220207040647072494038',
			'H10US521Q8220220207040654183707038',
			'H10US521Q8220220207040650482292038'

			February 18, 2022
			PRODUCTS-3312

			https://prnt.sc/26slf3b
			https://prnt.sc/26slfbl
			2/10 Keeshia: Availability should be tagged as Out of Stock

			# solution
				# https://www.ferguson.com/product/zurn-wilkins-34-in-valve-repair-kit-wrk34375v/_/R-3908618?toPdpDirectly=true&searchKeyWord=rk34-375v&trackSignal=true

	-----
	(R D-1) 3315	2022-02-11	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000557200201904220929',
		'200ITGF070000000557480201904220941',
		'200ITGF080000000556960201904220853'
		invalid deadlink		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200ITGF070000000557200201904220929,2022-02-11@22:00:00@200ITGF070000000557480201904220941,2022-02-11@22:00:00@200ITGF080000000556960201904220853

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200ITGF070000000557480201904220941

			# rerun successful

	(F D-1) 3316	2022-02-11	New CMS	Z00	image_count	www.worldwidestereo.com	
		'Z00US4W0BR020211104071540346661308',
		'Z00US4W0BR020211104071541425076308',
		'Z00US4W0O0000001455010202106220604',
		'Z00US4W0O0000001455400202106220604',
		'Z00US4W0O0000001455560202106220724',
		'Z00US4W0O0000001455590202106220724',
		'Z00US4W0O0000001455620202106220724',
		'Z00US4W0O0000001455650202106220724',
		'Z00US4W0O0000001455680202106220724',
		'Z00US4W0O0000001455710202106220724',
		'Z00US4W0O0000001455740202106220724',
		'Z00US4W0O0000001455770202106220724',
		'Z00US4W0O0000001455800202106220724',
		'Z00US4W0O0000001455830202106220724',
		'Z00US4W0O0000001455860202106220724',
		'Z00US4W0O0000001455920202106220724',
		'Z00US4W0O0000001455950202106220724',
		'Z00US4W0O0000001455980202106220724',
		'Z00US4W0O0000001456010202106220724',
		'Z00US4W0O0000001485570202107130218',

		Missing image_count		
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@04:00:00@Z00US4W0BR020211104071540346661308,2022-02-11@04:00:00@Z00US4W0BR020211104071541425076308,2022-02-11@04:00:00@Z00US4W0O0000001455010202106220604,2022-02-11@04:00:00@Z00US4W0O0000001455400202106220604,2022-02-11@04:00:00@Z00US4W0O0000001455560202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455590202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455620202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455650202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455680202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455710202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455740202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455770202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455800202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455830202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455860202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455920202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455950202106220724,2022-02-11@04:00:00@Z00US4W0O0000001455980202106220724,2022-02-11@04:00:00@Z00US4W0O0000001456010202106220724,2022-02-11@04:00:00@Z00US4W0O0000001485570202107130218

			# fix spider
			# for rebuild
			# for rerun

			# successful rerun

	(R D-1) 0 3317	2022-02-11	New CMS	Z00	image_count	www.bhphotovideo.com	
		'Z00USC10BR020211104071540935357308',
		'Z00USC10O0000001362350202103310629',
		'Z00USC10O0000001362550202103310638'

		Missing image_count		
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-11' AND website = 'www.bhphotovideo.com' AND item_id IN ('Z00USC10BR020211104071540935357308',
			# 'Z00USC10O0000001362350202103310629',
			# 'Z00USC10O0000001362550202103310638') 

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@04:00:00@Z00USC10BR020211104071540935357308,2022-02-11@None@		Z00USC10O0000001362350202103310629,2022-02-11@None@		Z00USC10O0000001362550202103310638

			# check local: recommended for unblocker
			# trello

	(R D-1) 3318	2022-02-11	New CMS	K00	all	alza.cz	
		K00CZ3603F000001526940202108020954
		K00CZ3603F000001526980202108020954	
		spider issue -1 value		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@K00CZ3603F000001526940202108020954,2022-02-11@22:00:00@K00CZ3603F000001526980202108020954

			# rerun successful

	(R D-1) 3319	2022-02-11	New CMS	K00	all	power.dk	
		K00DKX20CG000001487180202107130336	
		spider issue -1 value		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@K00DKX20CG000001487180202107130336

			# rerun successful

	(F D-1) 3320	2022-02-11	New CMS	K00	all	saturn.de	
		'K00DE9104O000001434420202106020354',
		'K00DE9108O000001433500202106020354',
		'K00DE910GO000001434130202106020354',
		'K00DE910JO000001434170202106020354'
		spider issue -1 value
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@K00DE9104O000001434420202106020354,2022-02-11@22:00:00@K00DE9108O000001433500202106020354,2022-02-11@22:00:00@K00DE910GO000001434130202106020354,2022-02-11@22:00:00@K00DE910JO000001434170202106020354

			# fix ppt generic not found
			# for rebuild
			# for rerun
			# rerun successful

	-----
	(R D-1) 2058	2022-02-11	New CMS	Q00	all	www.carlson.fi	
		Naisten Toppahousut
		auto copy over fr 1 day 	
		https://prnt.sc/26td322		
		keen

		# solution
			# e53f1473-7e13-43a4-9e81-6e3ec02b0cb7@fb5b7667-48f2-44bd-b6ab-a6d5970e66d1
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-11' 
			# AND website = 'www.carlson.fi' 
			# AND keyword in (N'Naisten Toppahousut')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@21:00:00@Q00@e53f1473-7e13-43a4-9e81-6e3ec02b0cb7@fb5b7667-48f2-44bd-b6ab-a6d5970e66d1
	
			# successful rerun

	(R D-3) 2060	2022-02-11	New CMS	Q00	all	www.snowleader.com	
		'Imperméable hommes',
		'Pantalons de protection hommes',
		'Pantalons de randonnée femme',
		'Pantalons de ski hommes',
		'Vestes de randonnée femme'

		Auto copy over for 1 day
		Auto copy over for 2 day

		https://prnt.sc/26tdbke
		https://prnt.sc/26td8pi
		https://prnt.sc/26tdapi
		https://prnt.sc/26td9yl
		https://prnt.sc/26tddbh	
		Yes	michael

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-11' 
			# AND website = 'www.snowleader.com' 
			# AND keyword in (N'Imperméable hommes',
			# 		'Pantalons de protection hommes',
			# 		'Pantalons de randonnée femme',
			# 		'Pantalons de ski hommes',
			# 		'Vestes de randonnée femme')
			# GROUP BY keyword, ritem_id, country, source

			# rerun sent

			# failed:
				# 7bedf4b6-7a6a-4ed2-b71f-e72d5ebbd582@6652b08f-c068-4953-9eae-cccb8e4d1025
				# a08af035-571f-446c-9271-04c7b94ebb16@6652b08f-c068-4953-9eae-cccb8e4d1025

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@22:00:00@Q00@7bedf4b6-7a6a-4ed2-b71f-e72d5ebbd582@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-11@22:00:00@Q00@a08af035-571f-446c-9271-04c7b94ebb16@6652b08f-c068-4953-9eae-cccb8e4d1025

			# successful rerun

	(R D-2) 2061	2022-02-11	New CMS	Q00	all	www.snowleader.com	
		Vestes de ski femme	
		Data Count Mismatch 5(Fetched Data is Lesser than expected)
		https://prnt.sc/26td4i3
		Yes	michael

		# solution
			# c04656c6-3696-40dc-b1b4-de6ce1241274@6652b08f-c068-4953-9eae-cccb8e4d1025
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-11' 
			# AND website = 'www.snowleader.com' 
			# AND keyword in (N'Vestes de ski femme')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@22:00:00@Q00@c04656c6-3696-40dc-b1b4-de6ce1241274@6652b08f-c068-4953-9eae-cccb8e4d1025
			# rerun failed

			# rerun successful

	(R D-1) 2062	2022-02-11	New CMS	Q00	all	www.webtogs.com
		trousers women	
		Auto copy over fr 1 day	
		https://prnt.sc/26tdq07	Yes	michael

		# solution
			# 4c7064b4-1e74-476f-bf54-5df4bd0282a6@cd723c00-2e50-4b35-8d00-66cd8a21402e
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@23:00:00@Q00@4c7064b4-1e74-476f-bf54-5df4bd0282a6@cd723c00-2e50-4b35-8d00-66cd8a21402e
			# rerun successful

	(R D-1) 2063	2022-02-11	New CMS	Q00	all	www.wiggle.co.uk	
		Waterproof trousers women	
		Auto copy over fr 1 day	
		https://prnt.sc/26tdwbx	Yes	michael

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@23:00:00@Q00@7b151994-509e-48f0-9529-f0ce41a5550b@883f2489-860b-4515-9e30-9d70600eed5d
			# rerun successful

	(R D-3) 2066	2022-02-11	New CMS	200	all	www.saturn.de	
		/ anc kopfhörer
			# 16df96c8-a099-42b7-a0f2-6f17e28c8253@UbbyaApIEem-gAANOiOHiA
			# JLAB Studio ANC, On-ear Kopfhörer Bluetooth Schwarz
		/ google lautsprecher
			# dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA
			# GOOGLE Home Max Smart Speaker, Kreide
		/ kopfhörer anc
			# f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@UbbyaApIEem-gAANOiOHiA
			# JLAB Studio ANC, On-ear Kopfhörer Bluetooth Schwarz
		/ mini lautsprecher	
			# 4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA
			# IDANCE Beat Dude Mini Bluetooth Lautsprecher, purple)

		Duplicate product_website & product_url
		https://prnt.sc/26tebw8		
		Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.saturn.de' and 
			# var2.keyword in (N'anc kopfhörer') and 
			# var2.[date] = '2022-02-11' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@16df96c8-a099-42b7-a0f2-6f17e28c8253@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA

			# rerun sent, multiple reruns failed
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@16df96c8-a099-42b7-a0f2-6f17e28c8253@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@UbbyaApIEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@22:00:00@200@4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-11@22:00:00@200@16df96c8-a099-42b7-a0f2-6f17e28c8253@UbbyaApIEem-gAANOiOHiA,2022-02-11@22:00:00@200@f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@UbbyaApIEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA

			# successful reruns

	(R D-1) 2067	2022-02-11	New CMS	200	all	www.saturn.de	
		multiroom systeme	
		Data Count Mismatch 13 (Fetched Data is Greater than expected)
		https://prnt.sc/26tecyp		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@77f15bc2-32f6-461b-b706-6cab66ece8b6@UbbyaApIEem-gAANOiOHiA
			# rerun sent
			# rerun successful

	(R D-2) 2068	2022-02-11	New CMS	200	all	www.elkjop.no	
		/ hodetelefoner trådløse
			d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw
		/ trådløs høyttaler wifi
			77786a55-9ea9-4477-bdd4-8d085dfa4c5a@EGO2juO8EeaOEAANOrFolw
			Harman Kardon Omni 20+ trådløs høyttaler (hvit)
		/ trådløse ørepropper
			9d7c4e95-5997-4482-9949-e3fbac1e83d1@EGO2juO8EeaOEAANOrFolw

		Duplicate product_website & product_url
		https://prnt.sc/26teluo		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw,2022-02-11@22:00:00@200@77786a55-9ea9-4477-bdd4-8d085dfa4c5a@EGO2juO8EeaOEAANOrFolw,2022-02-11@22:00:00@200@9d7c4e95-5997-4482-9949-e3fbac1e83d1@EGO2juO8EeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@77786a55-9ea9-4477-bdd4-8d085dfa4c5a@EGO2juO8EeaOEAANOrFolw

			# rerun successful

	(R D-1) 2069	2022-02-11	New CMS	200	all	www.bol.com	
		spatwaterdichte speaker	
		
		Duplicate product_website & product_url	
		https://prnt.sc/26tetdh		
		Christopher

		# solution
			# rerun successful

	(R D-1) 2070	2022-02-11	New CMS	200	all	www.coolblue.nl	
		Bluetooth hoofdtelefoon	
			# https://prnt.sc/26thl9r
			# https://prnt.sc/26thkw5
		Duplicate product_website & product_url	
		https://prnt.sc/26tewj4		
		Christopher

		# solution
			# 069deba5-45a9-4199-8edb-302967c084c1@UAjszpOqEei-eQANOiOHiA
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@069deba5-45a9-4199-8edb-302967c084c1@UAjszpOqEei-eQANOiOHiA
			# successful reruns
	-----
	(R D-1) 716	2022-02-11	New CMS	200	all	www.re-store.ru
		Speakers Voice	
		Data Count Mismatch 2 (Fetched Data is Lesser than expected	
		https://prnt.sc/26td82y	
		Yes	Jairus

		# solution
			# SELECT * FROM view_all_listings_data var WHERE date = '2022-02-11' AND website = 'www.re-store.ru' AND category IN ('Speakers Voice')

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-11@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# successful rerun

	(R D-1) 717	2022-02-11	New CMS	200	all	www.saturn.de	
		Speakers Bluetooth	
		Data Count Mismatch 13 (Fetched Data is Lesser than expected	
		https://prnt.sc/26tdehu
		Yes	Jairus

		# solution
			# SELECT * FROM view_all_listings_data var WHERE date = '2022-02-11' AND website = 'www.saturn.de' AND category in ('Speakers Bluetooth')
			# 200@910@d31909aa-41a1-4033-b273-49332a7be5d8

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-11@22:00:00@200@910@d31909aa-41a1-4033-b273-49332a7be5d8

			# successful rerun

	Inriver
	200@Zound@11:00AM@11:50AM@Prod2
	Z00@Sonos@01:00PM@01:15AM@Prod2
	K00@Razer@11:00AM@11:25AM@Prod2

	-----
	Stats:
		Rerun: (17) (23)
		Fix: 2

02 12 22
	Daily
	(R D-1) 328	2022-02-12	New CMS	110	image_count	www.intertoys.nl	
		110NLDB0OL000001517200202107260528	
		Missing description; -3 value		
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@110NLDB0OL000001517200202107260528
			# successful rerun

	(R D-1) 3330	2022-02-12	New CMS	100	all	elgiganten.dk	
		'100DK100ZQ020220124093126379703024',
		'100DK10010000001248290202011250630',
		'100DK10010000001222810202010280537',
		'100DK10010000001397640202105040251'

		spider issue		
		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@100DK10010000001222810202010280537,2022-02-12@22:00:00@100DK10010000001248290202011250630,2022-02-12@22:00:00@100DK10010000001397640202105040251,2022-02-12@22:00:00@100DK100ZQ020220124093126379703024

			# 100DK10010000001248290202011250630
			# 100DK10010000001397640202105040251

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@100DK10010000001397640202105040251

			# fix with rerun

	-----
	(R D-1) 2081	2022-02-12	New CMS	200	all	www.dns-shop.ru	
		портативная акустика wi-fi	
		invalid deadlink	
		https://prnt.sc/26u3emd		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-12@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@BzxplkXeEeeMawANOiZi-g
			# successful rerun

	(R D-1) 2082	2022-02-12	New CMS	200	all	www.saturn.de	
		voice lautsprecher - 16
		multiroom systeme - 12

		Data Count Mismatch 9 (Fetched Data is Less than expected)
		Data Count Mismatch 13 (Fetched Data is Less than expected)	

		https://prnt.sc/26u2tr7
		https://prnt.sc/26u2vdq		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@77f15bc2-32f6-461b-b706-6cab66ece8b6@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-12@22:00:00@200@77f15bc2-32f6-461b-b706-6cab66ece8b6@UbbyaApIEem-gAANOiOHiA
			# multiple reruns successful

	(R D-1) 2083	2022-02-12	New CMS	200	all	www.elkjop.no	
		trådløse øreplugger	- 25

		Data Count Mismatch 13 (Fetched Data is Less than expected)	
		https://prnt.sc/26u2yy9		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw
			# multiple reruns successful

	(R D-1) 2084	2022-02-12	New CMS	200	all	www.re-store.ru	
		'наушники с проводом', - 19
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		'портативная акустика wi-fi', - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		'портативная колонка wi-fi' - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 10 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)	

		https://prnt.sc/26u3bsu
		https://prnt.sc/26u3cug
		https://prnt.sc/26u3df7		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-12@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-12@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-12@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful

	(R D-3) 2089	2022-02-12	New CMS	200	all	www.mediamarkt.de	
		/ bluetooth in ear kopfhörer
			d1c77a47-3811-49b8-9453-9b45f687e09b@M3YPUApIEem-gAANOiOHiA
		bluetooth kopfhörer anc
			f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA
		/ in ear bluetooth
			8ce24b98-3414-4ae6-909f-a4aac32f5bed@M3YPUApIEem-gAANOiOHiA
		/ in ear kopfhörer	
			a7a86579-0256-4cd7-a6d8-a27cdd475e9d@M3YPUApIEem-gAANOiOHiA

		invalid product_website & product_url	
		https://prnt.sc/26u3pp1
		https://prnt.sc/26u3ryx
		https://prnt.sc/26u3rrw
		https://prnt.sc/26u3sbh		

		Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, rank, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.mediamarkt.de' and 
			# var2.keyword in (N'bluetooth kopfhörer anc') and 
			# var2.[date] = '2022-02-12' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, rank, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@d1c77a47-3811-49b8-9453-9b45f687e09b@M3YPUApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@8ce24b98-3414-4ae6-909f-a4aac32f5bed@M3YPUApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@a7a86579-0256-4cd7-a6d8-a27cdd475e9d@M3YPUApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-12@22:00:00@200@f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@8ce24b98-3414-4ae6-909f-a4aac32f5bed@M3YPUApIEem-gAANOiOHiA
			
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA
	
			# this keyword failed: bluetooth kopfhörer anc
				# probably blocked

			# others: multiple reruns successful

	(R D-2) 2090	2022-02-12	New CMS	200	all	www.saturn.de 
		bluetooth kopfhörer anc
			f57a5238-de1f-48fe-9781-b8908d0f6342@UbbyaApIEem-gAANOiOHiA
		bluetooth lautsprecher
			a7f7ff19-c7ff-47ae-99f8-61864de8ed91@UbbyaApIEem-gAANOiOHiA
		in ear
			08cffbb8-e783-4d9b-829c-238083bcb957@UbbyaApIEem-gAANOiOHiA
		kabellose kopfhörer
			986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA
		lautsprecher bluetooth
			e5ca8a90-3052-4c6a-8d17-be2f285b3aa2@UbbyaApIEem-gAANOiOHiA
		mini lautsprecher
			4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA
		tragbare lautsprecher
			1920ce34-ce4e-4f00-a793-b23312c238f8@UbbyaApIEem-gAANOiOHiA

		invalid product_website & product_url	

		https://prnt.sc/26u3t43
		https://prnt.sc/26u3tgz
		https://prnt.sc/26u3tyz
		https://prnt.sc/26u3uds
		https://prnt.sc/26u3uwa
		https://prnt.sc/26u3va5
		https://prnt.sc/26u3vpu		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@f57a5238-de1f-48fe-9781-b8908d0f6342@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@a7f7ff19-c7ff-47ae-99f8-61864de8ed91@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@08cffbb8-e783-4d9b-829c-238083bcb957@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@e5ca8a90-3052-4c6a-8d17-be2f285b3aa2@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA,2022-02-12@22:00:00@200@1920ce34-ce4e-4f00-a793-b23312c238f8@UbbyaApIEem-gAANOiOHiA

			# suggested products error
			# for MO though has an issue

			# successful rerun

	(F D-3) 2095	2022-02-12	New CMS	U00	product_url	www.miranda.com.br	
		'Console',
		'Console Nintendo Switch',
		'Console Switch',
		'Controle Joy con',
		'Controle Nintendo Switch',
		'Controle sem fio Nintendo Switch',
		'Controle Switch',
		'Joy Con',
		'Nintendo Switch',
		'Pro Controller',
		'Mini Video Game',
		'Aparelho de Game',
		'Aparelho de Video Game',
		'Mini game portatil',
		'controle game sem fio'

		wrong product url fetched	
		https://prnt.sc/26u6npg
		kurt
		
		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-12@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@98ce312a-6d9f-4cfe-91f5-ee98234a99d6@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@b3998d4b-1788-4dde-be30-c3cdc274e6c4,2022-02-12@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@b3998d4b-1788-4dde-be30-c3cdc274e6c4

			# fix
			# rebuild
			# rerun
			# successful rerun

	-----
	(R D-1) 720	2022-02-12	New CMS	200	all	www.re-store.ru	
		Speakers Voice	

		Data Count Mismatch 2 (Fetched Data is Lesser than expected	
		https://prnt.sc/26u3mcj	
		Yes	kurt

		# solution
			# 200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-12@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd
			# successful rerun

	(R D-1) 721	2022-02-12	New CMS	200	all	www.saturn.de	
		Headphones Bluetooth In Ear
		Headphones Bluetooth On Ear
		Speakers Bluetooth	
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/26u3q2k	
		Yes	Rhunick

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@910@672dbccf-7445-4518-aa1d-0a1dd0e4aa2d,2022-02-12@22:00:00@200@910@c6f0dedb-bbbe-49e9-82c3-28d7eee6d8d8,2022-02-12@22:00:00@200@910@d31909aa-41a1-4033-b273-49332a7be5d8

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-12@22:00:00@200@910@d31909aa-41a1-4033-b273-49332a7be5d8

			# rerun successful

	U00@Nintendo Brazil@12:00NN@12:25AM@Prod2

	-----
	Stats:
		Rerun: 10 (13)
		Fix: 1

02 13 22
	(F D-2)KEY-339	2/8/2022	2/8/2022	Zurn	Invalid deadlink in www.grainger.com
		H10US421Q8220220207035943959249038
		H10US421Q8220220207035945176448038
		H10US421Q8220220207035946135299038

		# solution
			# https://www.grainger.com/product/ZURN-WILKINS-Reduced-Pressure-Zone-Backflow-6AVY0?searchQuery=1-375&searchBar=true
			# https://www.grainger.com/product/ZURN-WILKINS-Reduced-Pressure-Zone-Backflow-45K858?searchQuery=34-375XL&searchBar=true
			# https://www.grainger.com/product/ZURN-WILKINS-Water-Pressure-Reducing-Valve-22N574?searchQuery=112-600XL&searchBar=true

			'H10US421Q8220220207035954797480038',
			'H10US421Q8220220207035954962228038',
			'H10US421Q8220220207035955130505038',
			'H10US421Q8220220207040622451576038'

		# fixed deadlink
		# fixed specs extraction
		# fixing shipping data : failed
		# rebuild
		# rerun

	(R D-1) KEY-341	2/11/2022	2/11/2022	ZUrn	missing specs in www.grainger.com	
		'H10US421Q8220220207035954797480038',
		'H10US421Q8220220207035954962228038',
		'H10US421Q8220220207035955130505038',
		'H10US421Q8220220207040622451576038'

		18-Feb-22	
		PRODUCTS-3312
		https://prnt.sc/26toi9x

		# solution
			# select * from view_all_productdata where date='2022-02-13' and website='www.grainger.com' and item_id in ('H10US421Q8220220207035954797480038',
			# 'H10US421Q8220220207035954962228038',
			# 'H10US421Q8220220207035955130505038',
			# 'H10US421Q8220220207040622451576038')
			
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@04:00:00@H10US421Q8220220207035954797480038,2022-02-13@04:00:00@H10US421Q8220220207035954962228038,2022-02-13@04:00:00@H10US421Q8220220207035955130505038,2022-02-13@04:00:00@H10US421Q8220220207040622451576038

			# successful rerun

	(R D-1) KEY-342	2/12/2022	2/12/2022	Zurn	special character in titles in www.fastenal.com	
		'H10US321Q8220220207035914518561038',
		'H10US321Q8220220207035914686288038',
		'H10US321Q8220220207035924533242038',
		'H10US321Q8220220207035924691396038',
		'H10US321Q8220220207035925105861038',
		'H10US321Q8220220207035925928858038',
		'H10US321Q8220220207035926239589038',
		'H10US321Q8220220207035930556807038',
		'H10US321Q8220220207035930718988038',
		'H10US321Q8220220207035931035212038',
		'H10US321Q8220220207035931730606038',
		'H10US321Q8220220207035932427366038',
		'H10US321Q8220220207035933548246038',
		'H10US321Q8220220207035934592059038',
		'H10US321Q8220220207035934791535038',
		'H10US321Q8220220207035940330026038'
		18-Feb-22	
		PRODUCTS-3312	
		https://prnt.sc/26u99ne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@04:00:00@H10US321Q8220220207035914518561038,2022-02-13@04:00:00@H10US321Q8220220207035914686288038,2022-02-13@04:00:00@H10US321Q8220220207035924533242038,2022-02-13@04:00:00@H10US321Q8220220207035924691396038,2022-02-13@04:00:00@H10US321Q8220220207035925105861038,2022-02-13@04:00:00@H10US321Q8220220207035925928858038,2022-02-13@04:00:00@H10US321Q8220220207035926239589038,2022-02-13@04:00:00@H10US321Q8220220207035930556807038,2022-02-13@04:00:00@H10US321Q8220220207035930718988038,2022-02-13@04:00:00@H10US321Q8220220207035931035212038,2022-02-13@04:00:00@H10US321Q8220220207035931730606038,2022-02-13@04:00:00@H10US321Q8220220207035932427366038,2022-02-13@04:00:00@H10US321Q8220220207035933548246038,2022-02-13@04:00:00@H10US321Q8220220207035934592059038,2022-02-13@04:00:00@H10US321Q8220220207035934791535038,2022-02-13@04:00:00@H10US321Q8220220207035940330026038

	(R D-1) 3355	2022-02-13	New CMS	K00	price_value	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP807O000001449500202106150452',
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP803F000001449400202106150452',
		'K00RUP803F000001527420202108020954',
		'K00RUP803F000001528840202108020955',
		'K00RUP803F000001449380202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001524850202108020807',
		'K00RUP80CG000000980360202006250902',
		'K00RUP80CG000001310900202101250600',
		'K00RUP80CG000001310880202101250600',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001524900202108020807',
		'K00RUP80CG000001310960202101250600',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000001245170202011170413',
		'K00RUP805K000001527540202108020954',
		'K00RUP805K000001527570202108020954',
		'K00RUP80YO000001449690202106150452',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449670202106150452',
		'K00RUP807Q000001528850202108020955'
		wrong price fetched		
		Yes	MAyeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@K00RUP80CG000000980360202006250902,2022-02-13@21:00:00@K00RUP80CG000000980370202006250902,2022-02-13@21:00:00@K00RUP80CG000000980450202006250902,2022-02-13@21:00:00@K00RUP80CG000001245170202011170413,2022-02-13@21:00:00@K00RUP80CG000001310880202101250600,2022-02-13@21:00:00@K00RUP80CG000001310900202101250600,2022-02-13@21:00:00@K00RUP80CG000001310960202101250600,2022-02-13@21:00:00@K00RUP80CG000001311060202101250600,2022-02-13@21:00:00@K00RUP805O000001449050202106150452,2022-02-13@21:00:00@K00RUP803F000001449380202106150452,2022-02-13@21:00:00@K00RUP803F000001449400202106150452,2022-02-13@21:00:00@K00RUP807O000001449500202106150452,2022-02-13@21:00:00@K00RUP807O000001449510202106150452,2022-02-13@21:00:00@K00RUP80AD000001449550202106150452,2022-02-13@21:00:00@K00RUP80DO000001449630202106150452,2022-02-13@21:00:00@K00RUP80YO000001449660202106150452,2022-02-13@21:00:00@K00RUP80YO000001449670202106150452,2022-02-13@21:00:00@K00RUP80YO000001449690202106150452,2022-02-13@21:00:00@K00RUP80CG000001524850202108020807,2022-02-13@21:00:00@K00RUP80CG000001524900202108020807,2022-02-13@21:00:00@K00RUP803F000001527420202108020954,2022-02-13@21:00:00@K00RUP805K000001527540202108020954,2022-02-13@21:00:00@K00RUP805K000001527570202108020954,2022-02-13@21:00:00@K00RUP80AD000001527640202108020954,2022-02-13@21:00:00@K00RUP803F000001528840202108020955,2022-02-13@21:00:00@K00RUP807Q000001528850202108020955

		# rerun successful

	(R D-1) 3356	2022-02-13	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP804K000001528810202108020955',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP804K000001527750202108020954',
		'K00RUP804K000001449120202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001449160202106150452',
		'K00RUP803F000001527370202108020954',
		'K00RUP803F000001527390202108020954',
		'K00RUP803F000001449180202106150452',
		incorrect ratings fetched 		
		Yes	MAyeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@K00RUP804K000001528810202108020955,2022-02-13@21:00:00@K00RUP804K000001449090202106150452,2022-02-13@21:00:00@K00RUP804K000001528820202108020955,2022-02-13@21:00:00@K00RUP804K000001527750202108020954,2022-02-13@21:00:00@K00RUP804K000001449120202106150452,2022-02-13@21:00:00@K00RUP803F000001449420202106150452,2022-02-13@21:00:00@K00RUP803F000001449160202106150452,2022-02-13@21:00:00@K00RUP803F000001527370202108020954,2022-02-13@21:00:00@K00RUP803F000001527390202108020954,2022-02-13@21:00:00@K00RUP803F000001449180202106150452

			# multiple reruns successful

	(R D-1) 3360	2022-02-13	New CMS	200	all	re-store.ru	
		200RULQ0C0000000907260202001290728	
		invalid deadlink		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@200RULQ0C0000000907260202001290728

			# multiple reruns successful

	(R D-1) 3361	2022-02-13	New CMS	200	all	re-store.ru	
		200RULQ0C0000000907540202001290747	
		spider issue -1 values		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@200RULQ0C0000000907540202001290747
			# reruns successful

	(R D-1) 3362	2022-02-13	New CMS	200	all	fnac.com/3P	
		200FRDU070000001381350202104140811	
		invalid deadlink		
		Yes	Louie

		# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200FRDU070000001381350202104140811
		# successful rerun

	(R D-1) 3363	2022-02-13	New CMS	200	all			
		'200ITGF070000000801930201906210247',
		'200ITGF070000000557370201904220937',
		'200ITGF070000000557480201904220941',
		'200ITGF080000000557310201904220937',
		'200ITGF080000000557080201904220902',
		'200ITGF070000000557870201904220956',
		'200ITGF080000000556780201904220839'
		invalid deadlink		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200ITGF070000000801930201906210247,2022-02-13@22:00:00@200ITGF070000000557370201904220937,2022-02-13@22:00:00@200ITGF070000000557480201904220941,2022-02-13@22:00:00@200ITGF080000000557310201904220937,2022-02-13@22:00:00@200ITGF080000000557080201904220902,2022-02-13@22:00:00@200ITGF070000000557870201904220956,2022-02-13@22:00:00@200ITGF080000000556780201904220839

			# 200ITGF080000000557310201904220937
			# 200ITGF070000000557370201904220937

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200ITGF080000000557310201904220937,2022-02-13@22:00:00@200ITGF070000000557370201904220937

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200ITGF070000000557370201904220937

			# multiple reruns successful

	(R D-1) 3364	2022-02-13	New CMS	200	all	elgiganten.dk	
		'200DK100C0000000043630201901180334',
		'200DK10070000001245750202011200116'
		incorrect DQ and Price		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200DK100C0000000043630201901180334,2022-02-13@22:00:00@200DK10070000001245750202011200116

			# multiple reruns successful

	-----
	(R D-1) 2103	2022-02-13	New CMS	200	all	www.saturn.de	
		mobile lautsprecher 
		multiroom systeme
		voice lautsprecher

		Data Count Mismatch 4 (Fetched Data is Greater than expected)
		Data Count Mismatch 13 (Fetched Data is Greater than expected)
		Data Count Mismatch 9 (Fetched Data is Greater than expected)

		https://prnt.sc/26upxtt
		https://prnt.sc/26upy4n
		https://prnt.sc/26upyyx		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200@85240185-5b5e-4dfd-87d0-e1a16f9cbc5c@UbbyaApIEem-gAANOiOHiA,2022-02-13@22:00:00@200@77f15bc2-32f6-461b-b706-6cab66ece8b6@UbbyaApIEem-gAANOiOHiA,2022-02-13@22:00:00@200@c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-13@22:00:00@200@85240185-5b5e-4dfd-87d0-e1a16f9cbc5c@UbbyaApIEem-gAANOiOHiA

			# multiple reruns successful

	(R D-2) 2104	2022-02-13	New CMS	200	all	www.re-store.ru	
		/ наушники с проводом - 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ проводные наушники	- 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26uqmfl
		https://prnt.sc/26uqnct
		https://prnt.sc/26uqp9t		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-13@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-13@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-13@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-13@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# blocked

			# successful multiple reruns

	(R D-1) 2105	2022-02-13	New CMS	200	all	www.saturn.de	
		google lautsprecher	

		Duplicate product_website & product_url	
		https://prnt.sc/26ur2nt	

		Phil

		# solution
			# no issues

	(R D-1) 2106	2022-02-13	New CMS	200	all	www.elkjop.no	
			hodetelefoner bluetooth
			høyttaler bluetooth
			støydempende headset
			trådløse hodetelefoner trening	
			
			Duplicate product_website & product_url	
			https://prnt.sc/26uqurr
			https://prnt.sc/26uqw8d
			https://prnt.sc/26uqx6f
			https://prnt.sc/26uqxky
			https://prnt.sc/26uqy8g
			https://prnt.sc/26uqyv7
			https://prnt.sc/26ur06j
			https://prnt.sc/26ur0eq
			https://prnt.sc/26ur10r
			https://prnt.sc/26ur1dp		

			Phil

			# solution
				# no issues

	-----
	Stats:
		Rerun: 12 (13)
		Fix: 1 [2]

02 16 22
	(R D-2) 2161	2022-02-16	New CMS	K00	all	www.box.co.uk	
		'anc headphones',
		'headphones',
		'Ergonomic Chair',
		'chair',
		'Office Chair',
		'Gaming Chair',
		'Gaming Mousemat',
		'Mousemat',
		'2.1 Speakers',
		'Gaming Speakers',
		'Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Gaming Notebook',
		'Notebook',
		'Gaming Laptop',
		'Laptop',
		'mechanical keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Gaming Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse',
		'Gaming Mouse'

		Auto Copy Over fr 1 day

		https://prnt.sc/26wl0jt
		https://prnt.sc/26wl0wa
		https://prnt.sc/26wl16v
		https://prnt.sc/26wl1ld
		https://prnt.sc/26wl27z
		https://prnt.sc/26wl2hu
		https://prnt.sc/26wl2pp
		https://prnt.sc/26wl3wr
		https://prnt.sc/26wl5f5
		https://prnt.sc/26wl65u
		https://prnt.sc/26wl6nt
		https://prnt.sc/26wl6yx
		https://prnt.sc/26wl76h
		https://prnt.sc/26wl7cv
		https://prnt.sc/26wl828
		https://prnt.sc/26wl8eo
		https://prnt.sc/26wl8vi
		https://prnt.sc/26wl914
		https://prnt.sc/26wl9dk
		https://prnt.sc/26wl9mm
		https://prnt.sc/26wl9sz
		https://prnt.sc/26wl9yi
		https://prnt.sc/26wla7x
		https://prnt.sc/26wlb88
		https://prnt.sc/26wlbhg
		https://prnt.sc/26wlboy
		https://prnt.sc/26wlbyp
		https://prnt.sc/26wlc8p
		https://prnt.sc/26wlcij		

		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-16@23:00:00@K00@f54708f2-399f-43af-bfb8-20d00c1f294b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@ef6e28af-4210-4946-b21a-daeaa5c4ed38@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@918d322f-2c44-4c9c-aeed-5d7376531f44@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@d6871bcf-528c-4a32-a19d-c504066e1013@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@b73ac43d-b22d-4fa8-9dd4-58e266c9e7f1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@49e9ace7-ebd5-4861-8aaf-ea221ef92b83@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@c34de3e3-db49-4227-89a5-a811ffbfbb5e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@a5042d62-ba5e-4b40-ac02-fada0b36b92c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@88d127ad-cc16-4b9e-9746-7f2fda21d7ed@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@tTCFMzB2EeeFsgANOiOHiA@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@a88e5833-304b-411f-b354-3e02fbd9486a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@4ece2ecd-1087-42f9-a425-0435928a0b47@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@287f5d68-29fb-4230-8e11-b4ec207c0b9a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@e88ea211-dbf7-43af-afbd-c122b70031b6@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@bae74ad9-7e9d-4ed4-a99c-497ef7383e1e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@d66f0c45-669a-40f6-a642-79e628732743@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@d57f49ea-2101-46e4-9b69-85c55112ad1b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@05d84a59-bd95-4bf2-903a-ef0f90c004ff@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@b20c5fbe-7824-4b8e-b054-857117d42119@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@b82d2b40-abf7-49b9-9a73-680f0ffbe1e1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-16@23:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# successful rerun

	(R D-1) 2162	2022-02-16	New CMS	K00	all	fnac.com	
		Gaming Keypad	
		Data Count Mismatch 6(Fetched Data is greater than expected )	
		https://prnt.sc/26wl9h7		
		michael

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@22:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@LzP6XApHEem-gAANOiOHiA
			# multiple reruns failed
			# check local: url 2nd page issue, should not be extracted
			# Trello: go for temporary bypass?

	(R D-1) 2163	2022-02-16	New CMS	K00	all	fnac.com	
		Tapis de souris de jeu	
		Auto copy over fr 1 day 	
		https://prnt.sc/26wl416		
		michael

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-16@22:00:00@K00@e1a1753a-2763-4f9e-ba68-494baf22436b@LzP6XApHEem-gAANOiOHiA
			# successful rerun

	(R D-1) 2172	2022-02-16	New CMS	200	all	www.dns-shop.ru	
		беспроводные наушники	
		invalid deadlink	
		https://prnt.sc/26wlpxp		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-16@21:00:00@200@fcdf721a-231b-4ce6-a051-35769d9b1ebb@BzxplkXeEeeMawANOiZi-g
			# successful rerun

	(R D-2) 2173	2022-02-16	New CMS	200	all	www.re-store.ru	
		/'наушники с проводом', - 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		'портативная акустика wi-fi', - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/'портативная колонка wi-fi' - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)	

		https://prnt.sc/26wlhn9
		https://prnt.sc/26wli97
		https://prnt.sc/26wljch		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-16@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-16@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-16@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful


	Re Trigger main
		at 2022-02-16@22:00:00@
		be 2022-02-16@22:00:00@
		cz 2022-02-16@22:00:00@
		de 2022-02-16@22:00:00@
		dk 2022-02-16@22:00:00@
		fi 2022-02-16@21:00:00@
		fr 2022-02-16@22:00:00@
		gb 2022-02-16@23:00:00@
		it 2022-02-16@22:00:00@
		nl 2022-02-16@22:00:00@
		no 2022-02-16@22:00:00@
		ru 2022-02-16@21:00:00@
		se 2022-02-16@22:00:00@
		es 2022-02-16@22:00:00@
		us 2022-02-16@04:00:00@

		Re Trigger main 
		at 2022-02-16@22:00:00@
		be 2022-02-16@
		cz 2022-02-16@
		de 2022-02-16@
		dk 2022-02-16@
		fi 2022-02-16@21:00:00@
		fr 2022-02-16@
		gb 2022-02-16@23:00:00@
		it 2022-02-16@
		nl 2022-02-16@
		no 2022-02-16@
		ru 2022-02-16@21:00:00@
		se 2022-02-16@
		es 2022-02-16@
		us 2022-02-16@04:00:00@

		de
		fr
		nl
		no
		dk
		it
		es
		be
		cz
		at
		se

		(R D-1) elgiganten.dk

			python main.py -c 200 -d 2022-02-16 -t 22:00:00 -p 200DK10070000000702800201905280513:200DK10070000000702870201905280518:200DK10070000000891150201911281605:200DK10070000000929120202003300343:200DK10070000000984380202007030257:200DK10070000001245750202011200116:200DK10070000001397300202105040205:200DK10070000001559990202108230658:200DK10070000001560310202108230658:200DK10080000001245970202011200116:200DK10080000001246100202011200116:200DK100B0000000031520201901180326:200DK100B0000000043440201901180334:200DK100B0000000044070201901180334:200DK100C0000000029930201901180326:200DK100C0000000038800201901180331:200DK100C0000000039290201901180331:200DK100C0000000043630201901180334:200DK100D0000000001350201901180326:200DK100D0000000031960201901180326:200DK100D0000000032460201901180326:200DK100D0000000035070201901180329:200DK100D0000000038140201901180330:200DK100D0000000044120201901180334:200DK100G0000000031950201901180326:200DK100H0000000041600201901180334:200DK100J0000000043310201901180334:200DK100L0000000030810201901180326:200DK100N0000000039810201901180331:200DK100N0000000045170201901180334:200DK100NC000000846250201909081337:200DK100O0000000036230201901180329:200DK100O0000000043710201901180334:200DK100P0000000029820201901180326:200DK100P0000000031620201901180326:200DK100P0000000031760201901180326:200DK100P0000000033410201901180329:200DK100P0000000033550201901180329:200DK100P0000000040550201901180331:200DK100Q0000000032320201901180326:200DK100D0000000036640201901180329 -j COMPARISON

			# done


		(R D-1) www.cdiscount.com
		
			python main.py -c 200 -d 2022-02-16 -t 22:00:00 -p 200FR1V070000001560330202108230658:200FR1V0NC000001594000202109210326:200FR1V0NC000001594240202109210326:200FRB6070000001384490202104200133:200FRB6070000001384770202104200133:200FRB60J0000000835530201908220946:200FRB60N0000000835490201908220944:200FR1V070000001560010202108230658:200FRB6070000000835370201908220929:200FRB6070000000835390201908220931:200FRB6070000000835470201908220942:200FRB6070000000835480201908220944:200FRB6070000000835510201908220945:200FRB6070000000835540201908220946:200FRB6070000000835560201908220947:200FRB6070000001380680202104140352:200FRB6070000001384430202104200133:200FRB6070000001384550202104200133:200FRB6070000001384610202104200133:200FRB6070000001384690202104200133:200FRB6070000001384740202104200133:200FRB6070000001384750202104200133:200FRB6070000001384760202104200133:200FRB60B0000000835780201908221013:200FRB60B0000000835810201908221015:200FRB60B0000000835820201908221016:200FRB60C0000000835670201908221005:200FRB60C0000000835710201908221006:200FRB60C0000000835720201908221009:200FRB60C0000000835750201908221010:200FRB60D0000000835600201908220950:200FRB60D0000000835610201908220951:200FRB60D0000000835630201908220952:200FRB60F0000000835580201908220949:200FRB60G0000000835570201908220948:200FRB60H0000000835550201908220947:200FRB60M0000000835520201908220945:200FRB60N0000000835450201908220936:200FRB60N0000000835460201908220937:200FRB60N0000000835500201908220944:200FRB60O0000000835430201908220934:200FRB60O0000000835440201908220935:200FRB60P0000000835300201908220917:200FRB60P0000000835380201908220930:200FRB60P0000000835400201908220931:200FRB60P0000000835410201908220933:200FRB60Q0000000835270201908220911 -j COMPARISON

			# done 

		(R D-1) www.cdiscount.com/3P

			python main.py -c 200 -d 2022-02-16 -t 22:00:00 -p 200FRMS080000001382870202104150735:200FRMS070000001382950202104150735:200FRMS070000001220660202010261317:200FRMS070000001220690202010261317:200FRMS070000001220730202010261317:200FRMS070000001220800202010261317:200FRMS070000001220850202010261317:200FRMS070000001220880202010261317:200FRMS070000001220910202010261317:200FRMS070000001220960202010261317:200FRMS070000001221020202010261317:200FRMS070000001221080202010261317:200FRMS070000001221100202010261317:200FRMS070000001221120202010261317:200FRMS070000001221140202010261317:200FRMS070000001382730202104150735:200FRMS070000001382780202104150735:200FRMS070000001382810202104150735:200FRMS070000001382840202104150735:200FRMS070000001398230202105040415:200FRMS070000001398360202105040415:200FRMS080000001382910202104150735:200FRMS0NC000001382760202104150735 -j COMPARISON

			# 23 done

		(R D-1) mediamarkt.de

			python main.py -c 200 -d 2022-02-16 -t 22:00:00 -p 200DE81070000000033970201901180329:200DE81070000000034510201901180329:200DE81070000000038020201901180330:200DE81070000000040810201901180332:200DE81070000000104260201902071109:200DE81070000000106400201902071111:200DE81070000000529010201903140557:200DE81070000000702640201905280505:200DE81070000000929170202003300343:200DE81070000000984340202007030256:200DE81070000001245800202011200116:200DE81070000001397440202105040205:200DE81070000001474460202107060614:200DE81070000001559950202108230658:200DE81070000001560270202108230658:200DE810B0000000103660201902071109:200DE810C0000000105800201902071110:200DE810D0000000103170201902071109:200DE810D0000000105170201902071110:200DE810D0000000105450201902071110:200DE810D0000000107110201902071112:200DE810G0000000103870201902071109:200DE810N0000000107450201902071113:200DE810NC000000860930201910140424:200DE810NC000000861010201910140545:200DE810NC000001594120202109210326:200DE810NC000001594360202109210326:200DE810O0000000104960201902071110:200DE810P0000000106610201902071112:200DE810Q0000000104110201902071109 -j COMPARISON

			# 30 done

		(R D-1) saturn.de

			python main.py -c 200 -d 2022-02-16 -t 22:00:00 -p 200DE91070000000030080201901180326:200DE91070000000032940201901180326:200DE91070000000033980201901180329:200DE91070000000034520201901180329:200DE91070000000038030201901180330:200DE91070000000040820201901180332:200DE91070000000041980201901180334:200DE91070000000702650201905280505:200DE91070000000929180202003300343:200DE91070000000984350202007030256:200DE91070000001245830202011200116:200DE91070000001397450202105040205:200DE91070000001474470202107060614:200DE91070000001559960202108230658:200DE91070000001560280202108230658:200DE910B0000000103670201902071109:200DE910B0000000106350201902071111:200DE910C0000000105570201902071110:200DE910C0000000105790201902071110:200DE910D0000000103180201902071109:200DE910D0000000104830201902071109:200DE910D0000000105180201902071110:200DE910D0000000105460201902071110:200DE910D0000000107120201902071112:200DE910G0000000103880201902071109:200DE910N0000000107460201902071113:200DE910NC000000860940201910140424:200DE910NC000000861020201910140545:200DE910NC000001594140202109210326:200DE910NC000001594380202109210326:200DE910O0000000104970201902071110:200DE910P0000000106620201902071112 -j COMPARISON

			# 32 done

		(R D-1) currys.co.uk
			python main.py -c 200 -d 2022-02-16 -t 00:00:00 -p 200GBI0080000001426010202106010209:200GBI00B0000000103640201902071109:200GBI00C0000000103280201902071109:200GBI00C0000000105700201902071110:200GBI00J0000000106680201902071112:200GBI00NC000000959800202006190347:200GBI00O0000000104920201902071110:200GBI00O0000000106870201902071112:200GBI0070000000030060201901180326:200GBI0070000000032920201901180326:200GBI0070000000033940201901180329:200GBI0070000000034470201901180329:200GBI0070000000702880201905280520:200GBI0070000000929140202003300343:200GBI0070000000956120202005260631:200GBI0070000000984390202007030258:200GBI0070000001397330202105040205:200GBI0070000001425960202106010209:200GBI0070000001425990202106010209:200GBI0070000001426060202106010209:200GBI0070000001474420202107060614:200GBI0070000001474590202107060614:200GBI0070000001474660202107060614:200GBI0070000001474730202107060614:200GBI00NC000000959790202006190347:200GBI00NC000001575530202109060525:200GBI00NC000001575610202109060525 -j COMPARISON

			# 27 done

	-----
	Stats:
		Rerun: 11 (13)
			

02 17 22
	(R D-1) 3432	2022-02-17	New CMS	F00	all	post.lu/fr	
		F00BEWX0WJ000001656830202111120252	
		Not Found in Availability		
		Yes	Glenn

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@F00BEWX0WJ000001656830202111120252
			# no issues

	(R D-1) 3433	2022-02-17	New CMS	O00	image_count	www.krefel.be/nl	
		O00BEIS040000001375330202104120812	
		missing image count and specs		
		Yes	Charlemagne

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@O00BEIS040000001375330202104120812
			# successful rerun

	(R D-1) 3442	2022-02-17	New CMS	K00	all	power.dk	
		'K00DKX204K000001554290202108180340',
		'K00DKX203F000001554340202108180340',
		'K00DKX20CG000001487160202107130336',
		'K00DKX20CG000001487170202107130336',
		'K00DKX20CG000001487180202107130336',
		'K00DKX20CG000001487200202107130336',
		'K00DKX20CG000001487210202107130336',
		'K00DKX20CG000001487220202107130336',
		'K00DKX20CG000001487260202107130336',
		'K00DKX20CG000001487270202107130336',
		'K00DKX205K000001554370202108180340',
		'K00DKX205K000001554360202108180340'

		spider issue- -1 values		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@K00DKX204K000001554290202108180340,2022-02-17@22:00:00@K00DKX203F000001554340202108180340,2022-02-17@22:00:00@K00DKX20CG000001487160202107130336,2022-02-17@22:00:00@K00DKX20CG000001487170202107130336,2022-02-17@22:00:00@K00DKX20CG000001487180202107130336,2022-02-17@22:00:00@K00DKX20CG000001487200202107130336,2022-02-17@22:00:00@K00DKX20CG000001487210202107130336,2022-02-17@22:00:00@K00DKX20CG000001487220202107130336,2022-02-17@22:00:00@K00DKX20CG000001487260202107130336,2022-02-17@22:00:00@K00DKX20CG000001487270202107130336,2022-02-17@22:00:00@K00DKX205K000001554370202108180340,2022-02-17@22:00:00@K00DKX205K000001554360202108180340

			# successful rerun

	(R D-1) 3443	2022-02-17	New CMS	K00	all	alza.cz	
		K00CZ3603F000001526940202108020954	
		spider issue- -1 values		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@K00CZ3603F000001526940202108020954
			# rerun

	(R D-2) 3444	2022-02-17	New CMS	200	all	elgiganten.dk	
		'200DK100C0000000039290201901180331',
		'200DK10080000001245970202011200116'
		invalid deadlink		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-17' AND website = 'www.elgiganten.dk' AND item_id in 
			# ('200DK100C0000000039290201901180331','200DK10080000001245970202011200116')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200DK100C0000000039290201901180331,2022-02-17@22:00:00@200DK10080000001245970202011200116

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200DK100C0000000039290201901180331

			# multiple reruns successful

	(R D-1) 3445	2022-02-17	New CMS	200	price_value	elgiganten.dk	
		'200DK100B0000000044070201901180334',
		'200DK100C0000000038800201901180331',
		'200DK100C0000000043630201901180334',
		'200DK100D0000000031960201901180326',
		'200DK100D0000000038140201901180330',
		'200DK100D0000000044120201901180334',
		'200DK100G0000000031950201901180326',
		'200DK100J0000000043310201901180334',
		'200DK100H0000000041600201901180334',
		'200DK100L0000000030810201901180326',
		'200DK100N0000000039810201901180331',
		'200DK100O0000000043710201901180334',
		'200DK100P0000000033410201901180329',
		'200DK100P0000000033550201901180329',
		'200DK100P0000000031760201901180326',
		'200DK100P0000000031620201901180326',
		'200DK100P0000000040550201901180331'
		Incorrect Price and -3 values in Delivery	
		https://prnt.sc/26xfocd	
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200DK100B0000000044070201901180334,2022-02-17@22:00:00@200DK100C0000000038800201901180331,2022-02-17@22:00:00@200DK100C0000000043630201901180334,2022-02-17@22:00:00@200DK100D0000000031960201901180326,2022-02-17@22:00:00@200DK100D0000000038140201901180330,2022-02-17@22:00:00@200DK100D0000000044120201901180334,2022-02-17@22:00:00@200DK100G0000000031950201901180326,2022-02-17@22:00:00@200DK100J0000000043310201901180334,2022-02-17@22:00:00@200DK100H0000000041600201901180334,2022-02-17@22:00:00@200DK100L0000000030810201901180326,2022-02-17@22:00:00@200DK100N0000000039810201901180331,2022-02-17@22:00:00@200DK100O0000000043710201901180334,2022-02-17@22:00:00@200DK100P0000000033410201901180329,2022-02-17@22:00:00@200DK100P0000000033550201901180329,2022-02-17@22:00:00@200DK100P0000000031760201901180326,2022-02-17@22:00:00@200DK100P0000000031620201901180326,2022-02-17@22:00:00@200DK100P0000000040550201901180331

			# for rebuild

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200DK100P0000000031620201901180326,2022-02-17@22:00:00@200DK100P0000000031760201901180326,2022-02-17@22:00:00@200DK100P0000000033410201901180329,2022-02-17@22:00:00@200DK100P0000000033550201901180329,2022-02-17@22:00:00@200DK100N0000000039810201901180331,2022-02-17@22:00:00@200DK100P0000000040550201901180331,2022-02-17@22:00:00@200DK100O0000000043710201901180334

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200DK100P0000000033550201901180329

			# successful rerun

			# done react, done sheets

	(R D-1) 3446	2022-02-17	New CMS	200	all	pricerunner.dk	
		200DKF3070000000549680201904050401	
		invalid deadlink		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-17' AND website = 'www.pricerunner.dk' AND item_id in 
			# ('200DKF3070000000549680201904050401')	
	
			
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200DKF3070000000549680201904050401
			# check local, redirected
			# trello
			# done react, done sheets

	(R D-2) 3454	2022-02-17	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000801930201906210247',
		'200ITGF070000000557200201904220929',
		'200ITGF070000000557590201904220945',
		'200ITGF070000000557480201904220941',
		'200ITGF080000000557420201904220941',
		'200ITGF080000000557310201904220937',
		'200ITGF080000000556780201904220839',
		'200ITGF070000000557970201904221001'
		invalid deadlink		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200ITGF070000000801930201906210247,2022-02-17@22:00:00@200ITGF070000000557200201904220929,2022-02-17@22:00:00@200ITGF070000000557590201904220945,2022-02-17@22:00:00@200ITGF070000000557480201904220941,2022-02-17@22:00:00@200ITGF080000000557420201904220941,2022-02-17@22:00:00@200ITGF080000000557310201904220937,2022-02-17@22:00:00@200ITGF080000000556780201904220839,2022-02-17@22:00:00@200ITGF070000000557970201904221001

			# 200ITGF070000000557200201904220929
			# 200ITGF070000000557590201904220945

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200ITGF070000000557200201904220929,2022-02-17@22:00:00@200ITGF070000000557590201904220945
			
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@200ITGF070000000557200201904220929

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200ITGF070000000557200201904220929

			# successful rerun

			# done react, done sheets

	(R D-3) 3457	2022-02-17	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000000813290201908020245',
		'100DK10010000000813760201908020319',
		'100DK10010000000814140201908020341',
		'100DK100ZQ020220124093123122384024',
		'100DK10010000001248230202011250630',
		'100DK10010000001438730202106080416',
		'100DK10010000001248250202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001248290202011250630',
		'100DK10010000001248310202011250630',
		'100DK10010000001248330202011250630',
		'100DK10010000000818120201908020935',
		'100DK10010000000818230201908020936',
		'100DK10010000000816740201908020830',
		'100DK10010000000819000201908021008',
		'100DK100ZQ020220124093123601621024',
		'100DK10010000001327030202102090604',
		'100DK10010000001222790202010280537',
		'100DK10010000001222800202010280537',
		'100DK100ZQ020220124093123963399024',
		'100DK10010000001222720202010280537',
		'100DK10010000001222770202010280537',
		'100DK10010000001222780202010280537',
		'100DK10010000001222810202010280537',
		'100DK10010000001222820202010280537',
		'100DK10010000001222840202010280537',
		'100DK10010000001248360202011250630',
		'100DK10010000001248390202011250630',
		'100DK10010000001248400202011250630',
		'100DK10010000001324180202102080716',
		'100DK10010000001326370202102090603',
		'100DK10010000001326510202102090603',
		'100DK10010000001660410202111162228',
		'100DK10010000001248210202011250630',
		'100DK10010000001324310202102090603',
		'100DK10010000001324400202102090603',
		'100DK10010000001324440202102090603',
		'100DK10010000001397490202105040251',
		'100DK10010000001397640202105040251',
		'100DK10010000001397790202105040251',
		'100DK10010000001397940202105040251',
		'100DK10010000001400700202105040816',
		'100DK10010000001400740202105040816',
		'100DK10010000001400780202105040816',
		'100DK100ZQ020220124093124632102024',
		'100DK100ZQ020220124093124884947024',
		'100DK10010000001659210202111162228',
		'100DK10010000001659240202111162228',
		'100DK10010000001413660202105130512',
		'100DK10010000001659910202111162228',
		'100DK100ZQ020220124090748845248024',
		'100DK10010000001659390202111162228',
		'100DK10010000001659370202111162228',
		'100DK10010000001660000202111162228',
		'100DK10010000001659670202111162228',
		'100DK10010000001659250202111162228',
		'100DK100ZQ020220124093125637325024',
		'100DK100ZQ020220124090753449368024',
		'100DK100ZQ020220124090753733277024',
		'100DK100ZQ020220124090753862646024',
		'100DK100ZQ020220124090754819827024',
		'100DK100ZQ020220124090754985166024',
		'100DK100ZQ020220124093126211631024',
		'100DK100ZQ020220124093126379703024',
		'100DK100ZQ020220124093127658423024',
		'100DK100ZQ020220124090759827858024',
		'100DK100ZQ020220124093133398639024',
		'100DK100ZQ020220124093145806437024',
		'100DK100ZQ020220124093147433839024',
		'100DK100ZQ020220124093148226755024',
		'100DK100ZQ020220124093149319561024',
		'100DK100ZQ020220124093149555986024',
		'100DK100ZQ020220124093149795094024'

		-3 delivery		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@100DK10010000000813290201908020245,2022-02-17@22:00:00@100DK10010000000813760201908020319,2022-02-17@22:00:00@100DK10010000000814140201908020341,2022-02-17@22:00:00@100DK100ZQ020220124093123122384024,2022-02-17@22:00:00@100DK10010000001248230202011250630,2022-02-17@22:00:00@100DK10010000001438730202106080416,2022-02-17@22:00:00@100DK10010000001248250202011250630,2022-02-17@22:00:00@100DK10010000001248260202011250630,2022-02-17@22:00:00@100DK10010000001248290202011250630,2022-02-17@22:00:00@100DK10010000001248310202011250630,2022-02-17@22:00:00@100DK10010000001248330202011250630,2022-02-17@22:00:00@100DK10010000000818120201908020935,2022-02-17@22:00:00@100DK10010000000818230201908020936,2022-02-17@22:00:00@100DK10010000000816740201908020830,2022-02-17@22:00:00@100DK10010000000819000201908021008,2022-02-17@22:00:00@100DK100ZQ020220124093123601621024,2022-02-17@22:00:00@100DK10010000001327030202102090604,2022-02-17@22:00:00@100DK10010000001222790202010280537,2022-02-17@22:00:00@100DK10010000001222800202010280537,2022-02-17@22:00:00@100DK100ZQ020220124093123963399024,2022-02-17@22:00:00@100DK10010000001222720202010280537,2022-02-17@22:00:00@100DK10010000001222770202010280537,2022-02-17@22:00:00@100DK10010000001222780202010280537,2022-02-17@22:00:00@100DK10010000001222810202010280537,2022-02-17@22:00:00@100DK10010000001222820202010280537,2022-02-17@22:00:00@100DK10010000001222840202010280537,2022-02-17@22:00:00@100DK10010000001248360202011250630,2022-02-17@22:00:00@100DK10010000001248390202011250630,2022-02-17@22:00:00@100DK10010000001248400202011250630,2022-02-17@22:00:00@100DK10010000001324180202102080716,2022-02-17@22:00:00@100DK10010000001326370202102090603,2022-02-17@22:00:00@100DK10010000001326510202102090603,2022-02-17@22:00:00@100DK10010000001660410202111162228,2022-02-17@22:00:00@100DK10010000001248210202011250630,2022-02-17@22:00:00@100DK10010000001324310202102090603,2022-02-17@22:00:00@100DK10010000001324400202102090603,2022-02-17@22:00:00@100DK10010000001324440202102090603,2022-02-17@22:00:00@100DK10010000001397490202105040251,2022-02-17@22:00:00@100DK10010000001397640202105040251,2022-02-17@22:00:00@100DK10010000001397790202105040251,2022-02-17@22:00:00@100DK10010000001397940202105040251,2022-02-17@22:00:00@100DK10010000001400700202105040816,2022-02-17@22:00:00@100DK10010000001400740202105040816,2022-02-17@22:00:00@100DK10010000001400780202105040816,2022-02-17@22:00:00@100DK100ZQ020220124093124632102024,2022-02-17@22:00:00@100DK100ZQ020220124093124884947024,2022-02-17@22:00:00@100DK10010000001659210202111162228,2022-02-17@22:00:00@100DK10010000001659240202111162228,2022-02-17@22:00:00@100DK10010000001413660202105130512,2022-02-17@22:00:00@100DK10010000001659910202111162228,2022-02-17@22:00:00@100DK100ZQ020220124090748845248024,2022-02-17@22:00:00@100DK10010000001659390202111162228,2022-02-17@22:00:00@100DK10010000001659370202111162228,2022-02-17@22:00:00@100DK10010000001660000202111162228,2022-02-17@22:00:00@100DK10010000001659670202111162228,2022-02-17@22:00:00@100DK10010000001659250202111162228,2022-02-17@22:00:00@100DK100ZQ020220124093125637325024,2022-02-17@22:00:00@100DK100ZQ020220124090753449368024,2022-02-17@22:00:00@100DK100ZQ020220124090753733277024,2022-02-17@22:00:00@100DK100ZQ020220124090753862646024,2022-02-17@22:00:00@100DK100ZQ020220124090754819827024,2022-02-17@22:00:00@100DK100ZQ020220124090754985166024,2022-02-17@22:00:00@100DK100ZQ020220124093126211631024,2022-02-17@22:00:00@100DK100ZQ020220124093126379703024,2022-02-17@22:00:00@100DK100ZQ020220124093127658423024,2022-02-17@22:00:00@100DK100ZQ020220124090759827858024,2022-02-17@22:00:00@100DK100ZQ020220124093133398639024,2022-02-17@22:00:00@100DK100ZQ020220124093145806437024,2022-02-17@22:00:00@100DK100ZQ020220124093147433839024,2022-02-17@22:00:00@100DK100ZQ020220124093148226755024,2022-02-17@22:00:00@100DK100ZQ020220124093149319561024,2022-02-17@22:00:00@100DK100ZQ020220124093149555986024,2022-02-17@22:00:00@100DK100ZQ020220124093149795094024

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@100DK10010000000818120201908020935,2022-02-17@22:00:00@100DK10010000000818230201908020936,2022-02-17@22:00:00@100DK10010000000819000201908021008,2022-02-17@22:00:00@100DK10010000001222720202010280537,2022-02-17@22:00:00@100DK10010000001248210202011250630,2022-02-17@22:00:00@100DK10010000001248230202011250630,2022-02-17@22:00:00@100DK10010000001248260202011250630,2022-02-17@22:00:00@100DK10010000001248330202011250630,2022-02-17@22:00:00@100DK10010000001248390202011250630,2022-02-17@22:00:00@100DK10010000001248400202011250630,2022-02-17@22:00:00@100DK10010000001324180202102080716,2022-02-17@22:00:00@100DK10010000001324310202102090603,2022-02-17@22:00:00@100DK10010000001324400202102090603,2022-02-17@22:00:00@100DK10010000001324440202102090603,2022-02-17@22:00:00@100DK10010000001326370202102090603,2022-02-17@22:00:00@100DK10010000001326510202102090603,2022-02-17@22:00:00@100DK10010000001327030202102090604,2022-02-17@22:00:00@100DK10010000001400700202105040816,2022-02-17@22:00:00@100DK10010000001400740202105040816,2022-02-17@22:00:00@100DK10010000001400780202105040816,2022-02-17@22:00:00@100DK10010000001413660202105130512,2022-02-17@22:00:00@100DK10010000001659210202111162228,2022-02-17@22:00:00@100DK10010000001659240202111162228,2022-02-17@22:00:00@100DK10010000001659250202111162228,2022-02-17@22:00:00@100DK10010000001659370202111162228,2022-02-17@22:00:00@100DK10010000001659390202111162228,2022-02-17@22:00:00@100DK10010000001659670202111162228,2022-02-17@22:00:00@100DK10010000001659910202111162228,2022-02-17@22:00:00@100DK10010000001660000202111162228,2022-02-17@22:00:00@100DK10010000001660410202111162228,2022-02-17@22:00:00@100DK100ZQ020220124090748845248024,2022-02-17@22:00:00@100DK100ZQ020220124090753449368024,2022-02-17@22:00:00@100DK100ZQ020220124090754819827024,2022-02-17@22:00:00@100DK100ZQ020220124090754985166024,2022-02-17@22:00:00@100DK100ZQ020220124090759827858024,2022-02-17@22:00:00@100DK100ZQ020220124093123601621024,2022-02-17@22:00:00@100DK100ZQ020220124093123963399024,2022-02-17@22:00:00@100DK100ZQ020220124093124632102024,2022-02-17@22:00:00@100DK100ZQ020220124093124884947024,2022-02-17@22:00:00@100DK100ZQ020220124093125637325024,2022-02-17@22:00:00@100DK100ZQ020220124093126211631024,2022-02-17@22:00:00@100DK100ZQ020220124093126379703024,2022-02-17@22:00:00@100DK100ZQ020220124093127658423024,2022-02-17@22:00:00@100DK100ZQ020220124093133398639024,2022-02-17@22:00:00@100DK100ZQ020220124093145806437024,2022-02-17@22:00:00@100DK100ZQ020220124093147433839024,2022-02-17@22:00:00@100DK100ZQ020220124093148226755024,2022-02-17@22:00:00@100DK100ZQ020220124093149319561024,2022-02-17@22:00:00@100DK100ZQ020220124093149555986024,2022-02-17@22:00:00@100DK100ZQ020220124093149795094024

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@100DK10010000001324180202102080716,2022-02-17@22:00:00@100DK10010000001324310202102090603,2022-02-17@22:00:00@100DK10010000001324400202102090603,2022-02-17@22:00:00@100DK10010000001324440202102090603,2022-02-17@22:00:00@100DK10010000001326370202102090603,2022-02-17@22:00:00@100DK10010000001326510202102090603,2022-02-17@22:00:00@100DK10010000001327030202102090604,2022-02-17@22:00:00@100DK10010000001400700202105040816,2022-02-17@22:00:00@100DK10010000001400740202105040816,2022-02-17@22:00:00@100DK10010000001400780202105040816,2022-02-17@22:00:00@100DK10010000001413660202105130512,2022-02-17@22:00:00@100DK10010000001659210202111162228,2022-02-17@22:00:00@100DK10010000001659240202111162228,2022-02-17@22:00:00@100DK10010000001659250202111162228,2022-02-17@22:00:00@100DK10010000001659370202111162228,2022-02-17@22:00:00@100DK10010000001659390202111162228,2022-02-17@22:00:00@100DK10010000001659670202111162228,2022-02-17@22:00:00@100DK10010000001659910202111162228,2022-02-17@22:00:00@100DK10010000001660000202111162228,2022-02-17@22:00:00@100DK10010000001660410202111162228,2022-02-17@22:00:00@100DK100ZQ020220124090748845248024,2022-02-17@22:00:00@100DK100ZQ020220124090753449368024,2022-02-17@22:00:00@100DK100ZQ020220124090754819827024,2022-02-17@22:00:00@100DK100ZQ020220124090754985166024,2022-02-17@22:00:00@100DK100ZQ020220124090759827858024,2022-02-17@22:00:00@100DK100ZQ020220124093123601621024,2022-02-17@22:00:00@100DK100ZQ020220124093123963399024,2022-02-17@22:00:00@100DK100ZQ020220124093124632102024,2022-02-17@22:00:00@100DK100ZQ020220124093124884947024,2022-02-17@22:00:00@100DK100ZQ020220124093125637325024,2022-02-17@22:00:00@100DK100ZQ020220124093126211631024,2022-02-17@22:00:00@100DK100ZQ020220124093126379703024,2022-02-17@22:00:00@100DK100ZQ020220124093127658423024,2022-02-17@22:00:00@100DK100ZQ020220124093133398639024,2022-02-17@22:00:00@100DK100ZQ020220124093145806437024,2022-02-17@22:00:00@100DK100ZQ020220124093147433839024,2022-02-17@22:00:00@100DK100ZQ020220124093148226755024,2022-02-17@22:00:00@100DK100ZQ020220124093149319561024,2022-02-17@22:00:00@100DK100ZQ020220124093149555986024,2022-02-17@22:00:00@100DK100ZQ020220124093149795094024

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@100DK10010000001659240202111162228,2022-02-17@22:00:00@100DK10010000001660000202111162228,2022-02-17@22:00:00@100DK10010000001660410202111162228,2022-02-17@22:00:00@100DK100ZQ020220124090748845248024,2022-02-17@22:00:00@100DK100ZQ020220124090753449368024,2022-02-17@22:00:00@100DK100ZQ020220124090754819827024,2022-02-17@22:00:00@100DK100ZQ020220124093126379703024,2022-02-17@22:00:00@100DK100ZQ020220124093127658423024,2022-02-17@22:00:00@100DK100ZQ020220124093133398639024,2022-02-17@22:00:00@100DK100ZQ020220124093145806437024,2022-02-17@22:00:00@100DK100ZQ020220124093147433839024,2022-02-17@22:00:00@100DK100ZQ020220124093148226755024,2022-02-17@22:00:00@100DK100ZQ020220124093149319561024,2022-02-17@22:00:00@100DK100ZQ020220124093149555986024,2022-02-17@22:00:00@100DK100ZQ020220124093149795094024

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@100DK100ZQ020220124093147433839024,2022-02-17@22:00:00@100DK100ZQ020220124093148226755024,2022-02-17@22:00:00@100DK100ZQ020220124093149319561024,2022-02-17@22:00:00@100DK100ZQ020220124093149555986024,2022-02-17@22:00:00@100DK100ZQ020220124093149795094024

			# multiple reruns successful

			# done react, done sheets

	(R D-1) 3458	2022-02-17	New CMS	200	all	dns-shop.ru	all items	
		Auto copy over (site is up)		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@21:00:00@200RUL80G0000000908400202001290924,2022-02-17@21:00:00@200RUL8070000001560400202108230658,2022-02-17@21:00:00@200RUL80NC000001575500202109060525,2022-02-17@21:00:00@200RUL80NC000001575580202109060525

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-17@21:00:00@200RUL8070000000906280202001290545,2022-02-17@21:00:00@200RUL8070000000906340202001290547,2022-02-17@21:00:00@200RUL8070000000906390202001290550,2022-02-17@21:00:00@200RUL8070000000906440202001290553,2022-02-17@21:00:00@200RUL8070000000906530202001290605,2022-02-17@21:00:00@200RUL8070000000906610202001290611,2022-02-17@21:00:00@200RUL8070000000906650202001290612,2022-02-17@21:00:00@200RUL8070000000906710202001290622,2022-02-17@21:00:00@200RUL8070000000906750202001290622,2022-02-17@21:00:00@200RUL80P0000000906880202001290642,2022-02-17@21:00:00@200RUL80P0000000906970202001290657,2022-02-17@21:00:00@200RUL80P0000000907120202001290720,2022-02-17@21:00:00@200RUL80P0000000907220202001290728,2022-02-17@21:00:00@200RUL80C0000000907240202001290728,2022-02-17@21:00:00@200RUL80P0000000907360202001290739,2022-02-17@21:00:00@200RUL80C0000000907400202001290741,2022-02-17@21:00:00@200RUL80N0000000907480202001290745,2022-02-17@21:00:00@200RUL80C0000000907530202001290747,2022-02-17@21:00:00@200RUL80C0000000907550202001290749,2022-02-17@21:00:00@200RUL80N0000000907600202001290750,2022-02-17@21:00:00@200RUL80N0000000907770202001290809,2022-02-17@21:00:00@200RUL80N0000000907820202001290811,2022-02-17@21:00:00@200RUL80N0000000907870202001290818,2022-02-17@21:00:00@200RUL80M0000000907920202001290821,2022-02-17@21:00:00@200RUL80J0000000907990202001290826,2022-02-17@21:00:00@200RUL80J0000000908080202001290834,2022-02-17@21:00:00@200RUL80J0000000908180202001290844,2022-02-17@21:00:00@200RUL80F0000000908630202001290943,2022-02-17@21:00:00@200RUL8070000000933730202004240754,2022-02-17@21:00:00@200RUL8070000000959530202006160159,2022-02-17@21:00:00@200RUL8070000000959590202006160159,2022-02-17@21:00:00@200RUL8070000000959620202006160159,2022-02-17@21:00:00@200RUL8070000001353430202103250548,2022-02-17@21:00:00@200RUL8070000001397460202105040205,2022-02-17@21:00:00@200RUL8070000001560080202108230658,2022-02-17@21:00:00@200RUL8080000001568630202108300838,2022-02-17@21:00:00@200RUL8080000001568640202108300838,2022-02-17@21:00:00@200RUL8070000001568620202108300838

	(R D-1) 3459	2022-02-17	New CMS	200	price_value	re-store.ru	
		'200RULQ0B0000000906950202001290647',
		'200RULQ0B0000000906870202001290641',
		'200RULQ0B0000000907140202001290720',
		'200RULQ0B0000000907000202001290658',
		'200RULQ0B0000000907030202001290701',
		'200RULQ0B0000000907050202001290703',
		'200RULQ0C0000000907410202001290741',
		'200RULQ0D0000000908710202001290947',
		'200RULQ0J0000000908010202001290826',
		'200RULQ070000001568540202108300838'

		Spider issue in Price (-3 values) and Title not fetched	
		https://prnt.sc/26xj85a
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@21:00:00@200RULQ0B0000000906870202001290641,2022-02-17@21:00:00@200RULQ0B0000000906950202001290647,2022-02-17@21:00:00@200RULQ0B0000000907000202001290658,2022-02-17@21:00:00@200RULQ0B0000000907030202001290701,2022-02-17@21:00:00@200RULQ0B0000000907050202001290703,2022-02-17@21:00:00@200RULQ0B0000000907140202001290720,2022-02-17@21:00:00@200RULQ0C0000000907410202001290741,2022-02-17@21:00:00@200RULQ0J0000000908010202001290826,2022-02-17@21:00:00@200RULQ0D0000000908710202001290947,2022-02-17@21:00:00@200RULQ070000001568540202108300838

			# multiple reruns successful

			# done reacts, done sheets

	(R D-2) 3462	2022-02-17	New CMS	X00	price_value	liverpool.com.mx	
		'X00MXHC0OL000001421520202105190424',
		'X00MXHC0OL000001421590202105190424',
		'X00MXHC0OL000001421670202105190424',
		'X00MXHC0SV020211207055752813427341',
		'X00MXHC0PL000001461200202106300705',
		'X00MXHC0OL000001339090202103010159',
		'X00MXHC0OL000001421290202105190424',
		'X00MXHC0OL000001421440202105190424',
		'X00MXHC0QL000001461390202106300705',
		'X00MXHC0OL000001421220202105190424',
		'X00MXHC0OL000001327480202102090753',
		'X00MXHC0OL000001475140202107060855',
		'X00MXHC0OL000001338990202103010159',
		'X00MXHC0OL000001327380202102090753',
		'X00MXHC0OL000001327330202102090753',
		'X00MXHC0OL000001327520202102090753'

		incorrect price		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@05:00:00@X00MXHC0OL000001327330202102090753,2022-02-17@05:00:00@X00MXHC0OL000001327380202102090753,2022-02-17@05:00:00@X00MXHC0OL000001327480202102090753,2022-02-17@05:00:00@X00MXHC0OL000001327520202102090753,2022-02-17@05:00:00@X00MXHC0OL000001338990202103010159,2022-02-17@05:00:00@X00MXHC0OL000001339090202103010159,2022-02-17@05:00:00@X00MXHC0OL000001421220202105190424,2022-02-17@05:00:00@X00MXHC0OL000001421290202105190424,2022-02-17@05:00:00@X00MXHC0OL000001421440202105190424,2022-02-17@05:00:00@X00MXHC0OL000001421520202105190424,2022-02-17@05:00:00@X00MXHC0OL000001421590202105190424,2022-02-17@05:00:00@X00MXHC0OL000001421670202105190424,2022-02-17@05:00:00@X00MXHC0PL000001461200202106300705,2022-02-17@05:00:00@X00MXHC0QL000001461390202106300705,2022-02-17@05:00:00@X00MXHC0OL000001475140202107060855,2022-02-17@05:00:00@X00MXHC0SV020211207055752813427341

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@05:00:00@X00MXHC0QL000001461390202106300705

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-17@05:00:00@X00MXHC0QL000001461390202106300705

			# rerun successful except for this: X00MXHC0QL000001461390202106300705

			# 

	------
	(R D-1) 2192	2022-02-17	New CMS	O00	all	www.vandenborre.be/nl	
		/ 'Droogkast aanbieding',
		/ 'Energiezuinige koelkast',
		/ 'Grote koelkasten',
		/ 'Droger aanbieding'

		Auto Copy Over fr 1 Day
		Auto Copy Over fr 3 Days	

		https://prnt.sc/26xcyms
		https://prnt.sc/26xcx7s
		https://prnt.sc/26xcwp9
		https://prnt.sc/26xcvp2		
		keen

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@O00@9219be6b-63ba-42d5-a786-10c594553709@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d,2022-02-17@22:00:00@O00@9356dbf1-0c89-4b84-af3d-9deaa9d7a30d@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d,2022-02-17@22:00:00@O00@28bc7394-ef99-494a-a32a-3f3f6b900220@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d,2022-02-17@22:00:00@O00@fd1a72f7-0321-4505-ace8-047e418911bd@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@O00@9219be6b-63ba-42d5-a786-10c594553709@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d,2022-02-17@22:00:00@O00@9356dbf1-0c89-4b84-af3d-9deaa9d7a30d@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d

			# multiple rerun successful

	(R D-1) 2193	2022-02-17	New CMS	O00	all	www.mediamarkt.be/nl/	
		Beamer	- 22
		Data Count Mismatch 1  (Fetched Data is Lesser than expected)	
		https://prnt.sc/26xd64r		
		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-17@22:00:00@O00@04c0a6ff-c1c8-4392-933c-1d7d2c0f9178@XEf8HrstEeidIgANOiZi-g
			# rerun successful
	
	------
	(R D-1) 753	2022-02-17	New CMS	200	all	www.amazon.de	
		Headphones Noise Cancelling	
		invalid deadlink	
		https://prnt.sc/26xct0e	Yes	Jairus
		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200@Y00@ed3689e9-1520-49a9-bf58-2856c3252f8e
			# fixing: unblocker blocked sometimes
			# blocked: trello? 

	(F D-1)754	2022-02-17	New CMS	200	all	www.amazon.de	
		Speakers Portable	
		3 Days Auto Copy Over	
		https://prnt.sc/26xcsbw	Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-17@22:00:00@200@Y00@afe7674e-d459-4f31-902c-888187018662
			# fixing: fixed

			# successful rerun

	(R D-1) 757	2022-02-17	New CMS	200	all	www.re-store.ru	
		Headphones Wired In Ear - 8
			200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f
		Speakers Voice	- 3
			200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

		Mismatched Data	
		https://prnt.sc/26xd8hs	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-17@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-02-17@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-17@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# multiple reruns goods

	(R D-1) 758	2022-02-17	New CMS	200	all	www.saturn.de	
		Speakers Voice - 25
			https://www.saturn.de/de/category/_smart-speaker-675578.html
			200@910@7f0a6934-804d-4b97-b701-1ed407cf5011

		Mismatched Data	
		https://prnt.sc/26xd9xy	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED  -l 2022-02-17@22:00:00@200@910@7f0a6934-804d-4b97-b701-1ed407cf5011
			# blocked trello

	Stats:
		Rerun: 17 (23)
		Fix: 1 [1]

02 18 22
	(F D-1) Trello
		Cause: Spider Issue / Blocked

		Issue description: Data Count Mismatch 6(Fetched Data is greater than expected )

		keyword: Gaming Keypad

		Screenshot: https://prnt.sc/26wl9h7

		Note: manual count 19 Insite

		2162	2022-02-16	New CMS	K00	all	fnac.com	
			Gaming Keypad	
			Data Count Mismatch 6(Fetched Data is greater than expected )	
			https://prnt.sc/26wl9h7		
			michael

			# solution
				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-16@22:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@LzP6XApHEem-gAANOiOHiA
				# multiple reruns failed
				# check local: url 2nd page issue, should not be extracted
				# Trello: go for temporary bypass?

			# new solution
				# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-16' 
				# AND website = 'www.fnac.com' 
				# AND keyword in (N'Gaming Keypad')
				# GROUP BY keyword, ritem_id, country, SOURCE

				# https://trello.com/c/gaMuMmuL/3265-razerrankings-data-count-mismatch-fetched-data-is-greater-than-expected-in-fnaccom
				# no issues during test
		
	(F D-3) 3463	2022-02-18	New CMS	610	image_count	www.nordicnest.se	
		All	Missing image_count; 
		With trello: https://trello.com/c/In4oivdy/3279-eva-solo-compliance-missing-imagecount-in-wwwnordicnestse		
		Yes	Jurena

		# solution
			SELECT * FROM view_all_productdata vap WHERE date = '2022-02-18' AND WEBSITE = 'www.nordicnest.se' AND image_count = '0'
			
			# fix image extraction in local

			# done react, done mark rebuild on sheet
			# for rerun on 12:00

			# https://trello.com/c/In4oivdy/3279-eva-solo-compliance-missing-imagecount-in

			# re fix again
			# rebuild later
			# successful rerun > change trello for retest
			# done react and sheet

	0 (R D-1) 3480	2022-02-18	New CMS	P00	price_value	power.no	ALL items	
		incorrect price		
		Yes	Caesa

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-18' AND WEBSITE = 'www.power.no'

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@22:00:00@P00NOZ201L000001410660202105110756,2022-02-18@22:00:00@P00NOZ201L000001410720202105110756,2022-02-18@22:00:00@P00NOZ201L000001575840202109060525,2022-02-18@22:00:00@P00NOZ201L000001622320202110180740,2022-02-18@22:00:00@P00NOZ201L000001622360202110180740,2022-02-18@22:00:00@P00NOZ201L000001622390202110180740

			# multiple reruns failed

			# trello: requires more time to fix
	
	(W) 3481	2022-02-18	New CMS	H10	webshot_url		
		select * from view_all_productdata where date='2022-02-18' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com')
		and webshot_url is NULL 

		order by item_id	
		No WEbshots		
		Yes	Keeshia

		# solution
			# multiple reruns successful
			# done react and sheets

	(R D-1) 3482	2022-02-18	New CMS	H10	in_stock	lowes.com	
		'H10US8X0Q8220220202031804143536033',
		'H10US8X0Q8220220202031805361604033',
		'H10US8X0Q8220220202031805907282033',
		'H10US8X0Q8220220202031806068923033'

		Spider Issue in Availability		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-18' AND WEBSITE = 'www.lowes.com' AND item_id in ('H10US8X0Q8220220202031804143536033',
			# 'H10US8X0Q8220220202031805361604033',
			# 'H10US8X0Q8220220202031805907282033',
			# 'H10US8X0Q8220220202031806068923033')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@H10US8X0Q8220220202031804143536033,2022-02-18@04:00:00@H10US8X0Q8220220202031805361604033,2022-02-18@04:00:00@H10US8X0Q8220220202031805907282033,2022-02-18@04:00:00@H10US8X0Q8220220202031806068923033

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-18@04:00:00@H10US8X0Q8220220202031805907282033

			# multiple reruns successful

	(R D-2) 3483	2022-02-18	New CMS	Z00	image_count	www.bhphotovideo.com	
		'Z00USC10O0000001362310202103310629',
		'Z00USC10O0000001362390202103310632',
		'Z00USC10O0000001362590202103310645',
		'Z00USC10O0000001362710202103310652',
		'Z00USC10O0000001362790202103310654',
		'Z00USC10O0000001362870202103310656',
		'Z00USC10O0000001362910202103310657'
		Missing image_count	
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-11' AND website = 'www.bhphotovideo.com' AND item_id IN ('Z00USC10O0000001362310202103310629',
			# 'Z00USC10O0000001362390202103310632',
			# 'Z00USC10O0000001362590202103310645',
			# 'Z00USC10O0000001362710202103310652',
			# 'Z00USC10O0000001362790202103310654',
			# 'Z00USC10O0000001362870202103310656',
			# 'Z00USC10O0000001362910202103310657') 

			#	python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00USC10O0000001362310202103310629,2022-02-18@04:00:00@Z00USC10O0000001362390202103310632,2022-02-18@04:00:00@Z00USC10O0000001362590202103310645,2022-02-18@04:00:00@Z00USC10O0000001362710202103310652,2022-02-18@04:00:00@Z00USC10O0000001362790202103310654,2022-02-18@04:00:00@Z00USC10O0000001362870202103310656,2022-02-18@04:00:00@Z00USC10O0000001362910202103310657

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00USC10O0000001362790202103310654,2022-02-18@04:00:00@Z00USC10O0000001362870202103310656

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00USC10O0000001362590202103310645,2022-02-18@04:00:00@Z00USC10O0000001362710202103310652,2022-02-18@04:00:00@Z00USC10O0000001362790202103310654,2022-02-18@04:00:00@Z00USC10O0000001362870202103310656,2022-02-18@04:00:00@Z00USC10O0000001362910202103310657

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00USC10O0000001362790202103310654,2022-02-18@04:00:00@Z00USC10O0000001362870202103310656

			# multiple reruns successful
			# done react done sheets

	(R D-1) 3484	2022-02-18	New CMS	Z00	description	www.bhphotovideo.com	
		'Z00USC10O0000001362390202103310632',
		'Z00USC10O0000001362590202103310645',
		'Z00USC10O0000001362310202103310629',
		'Z00USC10O0000001362710202103310652',
		'Z00USC10O0000001362790202103310654',
		'Z00USC10O0000001362910202103310657'
		Missing description; 
		-3 value		
		Yes	Jurena

		# solution
			# multiple reruns successful (included in ticket)
			# done reacts and sheets

	(R D-1) 3485	2022-02-18	New CMS	200	all	fnac.com/3P	
		200FRDU0NC000001381000202104140811	
		invalid deadlink		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@22:00:00@200FRDU0NC000001381000202104140811
			# successful rerun
			# done react and sheets

	(R D-1) 3486	2022-02-18	New CMS	Z00	video_count	www.bhphotovideo.com	
		'Z00USC10O0000001362310202103310629',
		'Z00USC10O0000001362390202103310632'
		Missing video_count		
		Yes	Jurena

		# solution
			# successful (included in ticket 3483)
			# done react, done sheets

	(R D-1) 3487	2022-02-18	New CMS	P00	all	ozon.ru	
		'P00RUXZ01L000001575860202109060525',
		'P00RUXZ01L000001575870202109060525',
		'P00RUXZ01L000001575880202109060525',
		'P00RU1W01L000001559220202108230324',
		'P00RU1W01L000001454890202106220209',
		'P00RU1W01L000001559230202108230324'
		invalid deadlinks		
		Yes	Caesa

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-18' AND WEBSITE = 'www.ozon.ru' AND item_id in ('P00RUXZ01L000001575860202109060525',
			# 'P00RUXZ01L000001575870202109060525',
			# 'P00RUXZ01L000001575880202109060525',
			# 'P00RU1W01L000001559220202108230324',
			# 'P00RU1W01L000001454890202106220209',
			# 'P00RU1W01L000001559230202108230324')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@P00RU1W01L000001454890202106220209,2022-02-18@21:00:00@P00RU1W01L000001559220202108230324,2022-02-18@21:00:00@P00RU1W01L000001559230202108230324,2022-02-18@21:00:00@P00RUXZ01L000001575860202109060525,2022-02-18@21:00:00@P00RUXZ01L000001575870202109060525,2022-02-18@21:00:00@P00RUXZ01L000001575880202109060525

			# multiple reruns successful
			# done reacts and sheet

	(R D-1) 3501	2022-02-18	New CMS	X00	price_value	liverpool.com.mx	
		'X00MXHC0OL000001421220202105190424',
		'X00MXHC0OL000001327480202102090753',
		'X00MXHC0OL000001475140202107060855',
		'X00MXHC0OL000001327380202102090753',
		'X00MXHC0SV020211207055749402383341',
		'X00MXHC0OL000001327330202102090753',
		'X00MXHC0OL000001327520202102090753',
		'X00MXHC0OL000001339130202103010159',
		'X00MXHC0OL000001339090202103010159',
		'X00MXHC0SV020211207055750151998341',
		'X00MXHC0SV020211207055750930808341',
		'X00MXHC0Y0120211207055747461462341',
		'X00MXHC0QL000001461390202106300705',
		'X00MXHC0SV020211207055752413679341',
		'X00MXHC0SV020211207055751664810341',
		'X00MXHC0OL000001421520202105190424',
		'X00MXHC0OL000001421670202105190424',
		'X00MXHC0OL000001421830202105190424'

		incorrect price		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-18@05:00:00@X00MXHC0OL000001327330202102090753,2022-02-18@05:00:00@X00MXHC0OL000001327380202102090753,2022-02-18@05:00:00@X00MXHC0OL000001327480202102090753,2022-02-18@05:00:00@X00MXHC0OL000001327520202102090753,2022-02-18@05:00:00@X00MXHC0OL000001339090202103010159,2022-02-18@05:00:00@X00MXHC0OL000001339130202103010159,2022-02-18@05:00:00@X00MXHC0OL000001421220202105190424,2022-02-18@05:00:00@X00MXHC0OL000001421520202105190424,2022-02-18@05:00:00@X00MXHC0OL000001421670202105190424,2022-02-18@05:00:00@X00MXHC0OL000001421830202105190424,2022-02-18@05:00:00@X00MXHC0QL000001461390202106300705,2022-02-18@05:00:00@X00MXHC0OL000001475140202107060855,2022-02-18@05:00:00@X00MXHC0Y0120211207055747461462341,2022-02-18@05:00:00@X00MXHC0SV020211207055749402383341,2022-02-18@05:00:00@X00MXHC0SV020211207055750151998341,2022-02-18@05:00:00@X00MXHC0SV020211207055750930808341,2022-02-18@05:00:00@X00MXHC0SV020211207055751664810341,2022-02-18@05:00:00@X00MXHC0SV020211207055752413679341

	-----
	(R D-1) 2207	2022-02-18	New CMS	200	all	www.saturn.de	
			voice lautsprecher
			wlan lautsprecher

			Duplicate product_website & product_url	https://prnt.sc/LW84WhsJQoe4
			https://prnt.sc/cQ3j5KhCDxwO		

			Christopher

			# solution
				# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# 	where var2.website = 'www.saturn.de' and 
				# 	var2.keyword in (N'voice lautsprecher') and 
				# 	var2.[date] = '2022-02-18' 
				# 	GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

				# no issue
				# done react, done sheets

				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@22:00:00@200@c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA

				# successful rerun
				# 

	(R D-1) 2208	2022-02-18	New CMS	200	all	www.dns-shop.ru	
		наушники внутриканальные беспроводные bluetooth
			0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g
		портативная акустика	
			eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	

		https://prnt.sc/QGUosmkW-AVe
		https://prnt.sc/CgJZAQcoddGZ
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@200@0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g,2022-02-18@21:00:00@200@eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g

			# rerun successful
			# done react and sheet

	(R D-1) 2209	2022-02-18	New CMS	200	all	www.dns-shop.ru
		наушники с проводом	

		Invalid Deadlink
		https://prnt.sc/q5EDUVbZhX21		
		Christopher

		# solution
			#SELECT date,website, keyword, product_website,product_url, ritem_id, country, rank, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.dns-shop.ru' and 
			# var2.keyword in (N'наушники с проводом') and 
			# var2.[date] = '2022-02-18' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, rank, source	 

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-18@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done react and sheet

	(R D-1) 2210	2022-02-18	New CMS	200	all	www.dns-shop.ru	
		проводные наушники	

		Duplicate product_website & product_url
		https://prnt.sc/Xauo0sC67GWV		
		Christopher

		# solution
			# no issue
			# done react and sheet

			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				# where var2.website = 'www.dns-shop.ru' and 
				# var2.keyword in (N'проводные наушники') and 
				# var2.[date] = '2022-02-18' 
				# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@BzxplkXeEeeMawANOiZi-g

			# multiple reruns successful 
			# already reacted and commented on sheets

	(R D-1) 2211	2022-02-18	New CMS	200	all	www.re-store.ru
		/ НАУШНИКИ С ПРОВОДОМ - 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		ПОРТАТИВНАЯ КОЛОНКА WI-FI - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		ПРОВОДНЫЕ НАУШНИКИ - 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 6 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 6 (Fetched Data is Lesser than expected)	

		https://prnt.sc/qk8bWbCD19zo
		https://prnt.sc/Nu29JXvidddG
		https://prnt.sc/SLpOjls6uNq6
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-18@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-18@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-18@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# done react and sheets

	(R D-1) 2212	2022-02-18	New CMS	200	all	www.technopark.ru
		портативная колонка	
		Duplicate product_website & product_url	
		https://prnt.sc/d3do8yJ7c7tp
		Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# 	where var2.website = 'www.technopark.ru' and 
			# 	var2.keyword in (N'портативная колонка') and 
			# 	var2.[date] = '2022-02-18' 
			# 	GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# no issue
			# done react and sheet

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@200@6889e93b-60e8-4b27-a39e-690c8afbe53d@d0bb62ad-8919-4942-a84e-09748f674d32
			# successful rerun
			# already reacted and commented on sheets

	2213	

	(R D-1) 2214	2022-02-18	New CMS	Q00	all	www.snowleader.com
		'Pantalons de protection hommes',
		'Pantalon d''extérieur hommes',
		'veste coquille hommes',
		'Imperméable hommes',
		'Vestes de randonnée femme',
		'Vestes de ski femme'

		1 Day Auto Copy Over
		2 Days Auto Copy Over
		1 Day Auto Copy Over
		1 Day Auto Copy Over
		1 Day Auto Copy Over
		2 Days Auto Copy Over	

		https://prnt.sc/26ZmGRQYxVLQ
		https://prnt.sc/uwsHzYprorCe
		https://prnt.sc/brSM--zFfys1
		https://prnt.sc/-cge4xDWlPHM
		https://prnt.sc/_YV73ky27EHW
		https://prnt.sc/9LvgqBdUnQ1g		

		ailyn

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-18' 
			# AND website = 'www.snowleader.com' 
			# AND keyword in (N'Pantalons de protection hommes',
			# 		'Pantalon d''extérieur hommes',
			# 		'veste coquille hommes',
			# 		'Imperméable hommes',
			# 		'Vestes de randonnée femme',
			# 		'Vestes de ski femme')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@Q00@8811f9dc-edae-4dab-bacc-fcccfa7f668b@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@abf84d35-15bb-4de0-9a88-3aa5089e1811@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@63a1554d-6ab8-4bc0-a8db-608b062adaf3@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@33bbba35-81d7-4f3e-880a-f1af9c5607c3@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@8c56deea-d1e1-47d6-b504-0a1c1f0750bf@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@c04656c6-3696-40dc-b1b4-de6ce1241274@6652b08f-c068-4953-9eae-cccb8e4d1025

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@Q00@abf84d35-15bb-4de0-9a88-3aa5089e1811@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-02-18@22:00:00@Q00@63a1554d-6ab8-4bc0-a8db-608b062adaf3@6652b08f-c068-4953-9eae-cccb8e4d1025

			# multiple reruns successful

			# done react and sheet

	(R D-1) 2215	2022-02-18	New CMS	Q00	all	www.outnorth.se	
		Skaljackor herr	
		Auto copyover fr 1 day 	
		https://prnt.sc/aUmaB9VFcSi1		
		Mich

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-18' 
			# AND website = 'www.outnorth.se' 
			# AND keyword in (N'Skaljackor herr')
			# GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@Q00@5d01ca3c-b02e-4be5-a4bb-b5d7fce6eb5f@257c4048-6837-4e5c-8e31-b443baccb408

			# rerun successful

	(R D-1) 2216	2022-02-18	New CMS	Q00	all	www.xxl.se	
		Vandringsbyxor dam	

		Auto copyover fr 1 day 	
		https://prnt.sc/g9sUFeTZ6OCo		
		Mich

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-18' 
			# AND website = 'www.xxl.se' 
			# AND keyword in (N'Vandringsbyxor dam')
			# GROUP BY keyword, ritem_id, country, source

			#  python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@Q00@eeed0354-dc9e-410d-8e47-15cd187e4fd9@twTh4AWrEei-cgANOiOHiA
			# successful rerun
			# done react and sheet

	(R D-1) 2217	2022-02-18	New CMS	Q00	all	www.xxl.fi	
		Naisten Kevyttakki	

		Data Count Mismatch (Fetched Data is Greater than expected)
		https://prnt.sc/LQMOQI-HULan		
		Mich

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, rank, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.xxl.fi' and 
			# var2.keyword in (N'Naisten Kevyttakki') and 
			# var2.[date] = '2022-02-18' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, rank, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@21:00:00@Q00@cd0fb8fc-b7d9-424f-abd8-65108386ca45@1BC3FAWrEei-cgANOiOHiA

			# successful rerun

			# done react and sheet

	(R D-1) 2218	2022-02-18	New CMS	Z00	all	www.visions.ca/default.aspx	
		receiver	
		Duplicate product_website & product_url	https://prnt.sc/cGb_XUeoJRvp		
		renz

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.visions.ca/default.aspx' and 
			# var2.keyword in (N'receiver') and 
			# var2.[date] = '2022-02-18' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00@c2488cd9-a106-4aa2-9f0f-52e1154aed13@fe0c061d-175d-4f52-b17d-1cd561d54aa8

			# successful rerun
			# done react and sheet

	(R D-1) 2219	2022-02-18	New CMS	Z00	all	www.costco.ca	
		receiver	
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	
		https://prnt.sc/4OiBtDAsBfAB		
		renz

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-18@04:00:00@Z00@c2488cd9-a106-4aa2-9f0f-52e1154aed13@IMq4iEauEeeMawANOiZi-g
			# site check: downy invalid products? 
			# no need to fix > successful rerun
			# done react and sheets
	-----

	(R D-1) 761	2022-02-18	New CMS	610	all	www.kitchn.no	
		'Cuttlery',
			https://www.kitchn.no/nettbutikk/bestikk/
		'Gryter & Panner',
			https://www.kitchn.no/nettbutikk/glass-krus/drikkebeger/
		'Hvitvinsglass',
		'Rødvinsglass',
		'Kopper & Krus',
		'Dype Tallerkener',
		'Grill',
		'Drikkebeger',
		'Karafler'

		3 Days Auto Copy Over	

		https://prnt.sc/4Cwyhuyl2tRG	

		Yes	Joemike

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-18' 
			AND website = 'www.kitchn.no' 
			AND category in (N'Cuttlery',
					'Gryter & Panner',
					'Hvitvinsglass',
					'Rødvinsglass',
					'Kopper & Krus',
					'Dype Tallerkener',
					'Grill',
					'Drikkebeger',
					'Karafler')
			GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@610@PW0@b8cb5f32-618a-458f-a1ee-52b8692e0669,2022-02-18@22:00:00@610@PW0@b5354e5d-30df-4a92-8f88-e7f26305f18c,2022-02-18@22:00:00@610@PW0@e9008faf-f368-4fca-8ab8-d8504b46c46d,2022-02-18@22:00:00@610@PW0@02e9afae-b2df-4116-989b-89ed1be88f95,2022-02-18@22:00:00@610@PW0@f8379238-aa43-4aeb-a290-cabeff2d52ab,2022-02-18@22:00:00@610@PW0@24664dc6-2c41-4bc5-b5f7-be4ce032ae16,2022-02-18@22:00:00@610@PW0@08ec90a2-b397-4641-9d11-b52d78848521,2022-02-18@22:00:00@610@PW0@51c1ea49-2356-4e36-9083-3871bbc829d2,2022-02-18@22:00:00@610@PW0@be1d9ee7-568f-4d5f-a0d8-ace003451c21

			# done reacts, update rebuild on sheets
			# for rebuild

			# successful rerun
			# done react and sheets

	(R D-1) 762	2022-02-18	New CMS	610	all	www.kitchn.no	
		Termokanner	
			https://www.kitchn.no/nettbutikk/servering/termo-og-presskanner/termokanner/
		Invalid Deadlink	
		https://prnt.sc/YhDUm763FIJC	
		Yes	Joemike

		# solution
			# SELECT * FROM view_all_listings_data vald WHERE date = '2022-02-18' AND website = 'www.kitchn.no' AND category in (N'Termokanner')

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@610@PW0@886008b6-a29c-401a-a27a-eb9c750516fc
			# done reacts, update rebuild on sheets

			# for rebuild
			# successful rerun
			# done react and sheets

	(R D-1) 763	2022-02-18	New CMS	610	all	www.cervera.se	
		Ovrigt	
			

		-1 on Product Website	
		https://prnt.sc/9CJ1vVsswIkR	
		Yes	Joemike

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-18@22:00:00@610@ZW0@6c142e3c-e216-4687-90f9-00b08f6e55ef

			# successful rerun
			# done react, done sheets

	(R D-1) 768	2022-02-18	New CMS	Q00	all	www.bever.nl
		Trousers Men
		1 Day ACO Data
		https://prnt.sc/JXAJsgAOAzQ9
		Yes	Rayyan

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-18@22:00:00@Q00@8W0@d461c12f-ac49-4a76-a207-3729ec58036e
			# successful rerun
			# done react, done sheets

	K00@Razer@11:00AM@11:25AM@Prod2
	610@EvaSolo@--:-- --@--:-- --@Prod2

	Stats:
		Rerun: 24 (25)
		Webshots: 1
		Fix: 2 [1][2]

02 19 22
	(R D-1) 3502	2022-02-19	New CMS	K00	price_value	mvideo.ru	
		'K00RUP807O000001449500202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP804K000001528820202108020955',
		'K00RUP80CG000001209310202009280432',
		'K00RUP80CG000001524870202108020807',
		'K00RUP80CG000001245160202011170413',
		'K00RUP80CG000001310920202101250600',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001310980202101250600',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000000980420202006250902',
		'K00RUP80CG000000980420202006250902',
		'K00RUP80CG000001310850202101250600',
		'K00RUP80CG000001373010202104050819',
		'K00RUP80CG000001373010202104050819',
		'K00RUP80CG000001373010202104050819',
		'K00RUP80YO000001449690202106150452',
		'K00RUP80YO000001449660202106150452'
		Wrong price		
		Yes	Mayeeeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@K00RUP807O000001449500202106150452,2022-02-19@21:00:00@K00RUP80AD000001527640202108020954,2022-02-19@21:00:00@K00RUP804K000001528820202108020955,2022-02-19@21:00:00@K00RUP80CG000001209310202009280432,2022-02-19@21:00:00@K00RUP80CG000001524870202108020807,2022-02-19@21:00:00@K00RUP80CG000001245160202011170413,2022-02-19@21:00:00@K00RUP80CG000001310920202101250600,2022-02-19@21:00:00@K00RUP80CG000001311060202101250600,2022-02-19@21:00:00@K00RUP80CG000001310980202101250600,2022-02-19@21:00:00@K00RUP80CG000000980450202006250902,2022-02-19@21:00:00@K00RUP80CG000000980420202006250902,2022-02-19@21:00:00@K00RUP80CG000000980420202006250902,2022-02-19@21:00:00@K00RUP80CG000001310850202101250600,2022-02-19@21:00:00@K00RUP80CG000001373010202104050819,2022-02-19@21:00:00@K00RUP80CG000001373010202104050819,2022-02-19@21:00:00@K00RUP80CG000001373010202104050819,2022-02-19@21:00:00@K00RUP80YO000001449690202106150452,2022-02-19@21:00:00@K00RUP80YO000001449660202106150452

			# multiple reruns successful
			# done reacts and sheet

	(R D-1) 3503	2022-02-19	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP807O000001449500202106150452',
		'K00RUP804K000001528810202108020955',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001449330202106150452',
		'K00RUP803F000001527370202108020954',
		'K00RUP803F000001527370202108020954',
		'K00RUP803F000001528840202108020955',
		'K00RUP80CG000001592510202109200556',
		'K00RUP807U020220214043148827160045'
		Wrong ratings 		
		Yes	Mayeeeeee

		# solution
			SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-18' AND website = 'www.mvideo.ru' AND item_id in 
			('K00RUP807O000001449500202106150452',
					'K00RUP804K000001528810202108020955',
					'K00RUP804K000001449090202106150452',
					'K00RUP804K000001528820202108020955',
					'K00RUP803F000001449420202106150452',
					'K00RUP803F000001449330202106150452',
					'K00RUP803F000001527370202108020954',
					'K00RUP803F000001527370202108020954',
					'K00RUP803F000001528840202108020955',
					'K00RUP80CG000001592510202109200556',
					'K00RUP807U020220214043148827160045')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@K00RUP807O000001449500202106150452,2022-02-19@21:00:00@K00RUP804K000001528810202108020955,2022-02-19@21:00:00@K00RUP804K000001449090202106150452,2022-02-19@21:00:00@K00RUP804K000001528820202108020955,2022-02-19@21:00:00@K00RUP803F000001449420202106150452,2022-02-19@21:00:00@K00RUP803F000001449330202106150452,2022-02-19@21:00:00@K00RUP803F000001527370202108020954,2022-02-19@21:00:00@K00RUP803F000001527370202108020954,2022-02-19@21:00:00@K00RUP803F000001528840202108020955,2022-02-19@21:00:00@K00RUP80CG000001592510202109200556,2022-02-19@21:00:00@K00RUP807U020220214043148827160045

			# rerun successful
			# done react, done sheets

	(R D-1) 3504	2022-02-19	New CMS	K00	all	mvideo.ru	
		K00RUP80CG000001310920202101250600	
		Spider issue -1 value		
		Yes	Mayeeeeee

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-19' AND website = 'www.mvideo.ru' AND item_id in 
			# ('K00RUP80CG000001310920202101250600')	
			
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@K00RUP80CG000001310920202101250600

			# rerun successful
			# done react, done sheets

	(R D-1) 3505	2022-02-19	New CMS	K00	all	dns-shop.ru	
		K00RUL80CG000000979770202006250707	
		Invalid Deadlink		
		Yes	Mayeeeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-19@21:00:00@K00RUL80CG000000979770202006250707
			# rerun successful
			# done react done sheet

	-----

	(R D-1) 2223	2022-02-19	New CMS	M00	all	www.vetzoo.se	
		Cocker	
		Auto copy over fr 1 day	https://prnt.sc/5Zu896QMLqyX		
		MICHAEL

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@22:00:00@M00@8efe25d8-8b20-4741-9b37-98d048cb26c2@1cb791b7-ef8a-41c4-9caf-3f5591b75a65
			# rerun succesful
			# done react and sheet

	(R D-1) 2224	2022-02-19	New CMS	200	all	www.dns-shop.ru	
		наушники с шумоподавлением	
		invalid deadlink	
		https://prnt.sc/ui2-YsmpmcNE		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@21:00:00@200@e021030a-78d1-43f9-8a4e-5f6c3eb7b157@BzxplkXeEeeMawANOiZi-g

			# rerun successful
			# done react and sheet

	(R D-1) 2225	2022-02-19	New CMS	200	all	www.fnac.com	
		enceinte sans fil bluetooth	 
		Data Count Mismatch 5 ( Fetched Data is Lesser than expected)	
		https://prnt.sc/IwCjJH3sGtrX		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@22:00:00@200@65c894b9-75aa-4ab7-8315-fb0777bc8aa2@LzP6XApHEem-gAANOiOHiA
			# multiple reruns succesful
			# done react, done sheets

	(R D-1) 2226	2022-02-19	New CMS	200	all	www.saturn.de	
		mobile lautsprecher
		multiroom systeme	

		Data Count Mismatch 4 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 14 ( Fetched Data is Lesser than expected)	

		https://prnt.sc/90xXM15EXiks
		https://prnt.sc/znhj4TLH-jwt		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@85240185-5b5e-4dfd-87d0-e1a16f9cbc5c@UbbyaApIEem-gAANOiOHiA,2022-02-19@22:00:00@200@77f15bc2-32f6-461b-b706-6cab66ece8b6@UbbyaApIEem-gAANOiOHiA

			# multiple reruns succesful
			# done react, done sheet

	(R D-2) 2227	2022-02-19	New CMS	200	all	www.re-store.ru	
		акустика wi-fi
		портативная акустика wi-fi
		портативная колонка wi-fi	

		Data Count Mismatch 1 ( Fetched Data is Greater than expected)
		Data Count Mismatch 2 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 2 ( Fetched Data is Lesser than expected)	

		https://prnt.sc/bIfqDNJadWS1
		https://prnt.sc/IBqNclvipM_0
		https://prnt.sc/NsW0rT1P76h7		

		Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.re-store.ru' and 
			# var2.keyword in (N'портативная колонка wi-fi') and 
			# var2.[date] = '2022-02-19' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-19@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-19@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-19@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple rerun successful
			# done react, done sheets

	(R D-1) 2228	2022-02-19	New CMS	200	all	www.saturn.de	
		amazon lautsprecher
			a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA
		google lautsprecher	
			dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA

		Duplicate product_website & product_url	

		https://prnt.sc/rgDCP7VpPDCp
		https://prnt.sc/RWgnlI5KoNcJ
		https://prnt.sc/ttrQ7DzP_vGS

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA,2022-02-19@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA

			# multiple reruns successful
			# multiple reruns succesful
			# done react done sheets

	(R D-2) 2229	2022-02-19	New CMS	200	all	www.bol.com
		Bluetooth hoofdtelefoon
			07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA
		draagbaar speaker
			1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA
		headset
			9ce256ab-4183-4ec9-b02e-35f70b3310c9@UAjszpOqEei-eQANOiOHiA

		Duplicate product_website & product_url

		https://prnt.sc/yt1ByxaYlkvw
		https://prnt.sc/6FB4fCVFPuAT
		https://prnt.sc/AGTL42-TRxyW
		https://prnt.sc/jxpi3EJnOkTl

		Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.bol.com' and 
			# var2.keyword in (N'headset') and 
			# var2.[date] = '2022-02-19' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA,2022-02-19@22:00:00@200@1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA,2022-02-19@22:00:00@200@9ce256ab-4183-4ec9-b02e-35f70b3310c9@UAjszpOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA,2022-02-19@22:00:00@200@1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA

			# multiple reruns succesful
			# done react, done sheets

	(R D-1) 2230	2022-02-19	New CMS	110	all	www.fnac.be/fr	
		'console de jeu',
		'Nintendo Switch console',
		'Nintendo Switch Lite',
		'Nintendo Switch rouge/bleu'

		Data Count Mismatch 5 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Greater than expected)	

		https://prnt.sc/ZsV46GjU8vb3
		https://prnt.sc/ThEwlPcNSfJr
		https://prnt.sc/hzxxrBdQtXKw
		https://prnt.sc/Eps4AIU-0mFl	

		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@110@aea0946f-13dc-4852-8277-0bde28a3f151@Hhe47uOhEeeyugANOiZi-g,2022-02-19@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g,2022-02-19@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g,2022-02-19@22:00:00@110@2cd65bf0-faea-4d88-bd15-1ec07d394012@Hhe47uOhEeeyugANOiZi-g

			# multiple reruns failed
			# multiple reruns succesful, no need fix but fix is still good
			# for rebuild

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@22:00:00@110@2cd65bf0-faea-4d88-bd15-1ec07d394012@Hhe47uOhEeeyugANOiZi-g

	(F D-1) 2231	2022-02-19	New CMS	110	all	www.fnac.be/nl	
		'Nintendo Switch console',
		'Nintendo Switch Lite',
		'spelcomputer'

		Data Count Mismatch 5 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Greater than expected)	

		https://prnt.sc/wgdfGMzIz-Ak
		https://prnt.sc/XVwCyCE2dWrv
		https://prnt.sc/ptEpKRcq8lls	

		Yes	richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@Hhe47uOhEeeyugANOiZi-g,2022-02-19@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@Hhe47uOhEeeyugANOiZi-g

			# done fix, for rebuild

	(R D-1) 2232	2022-02-19	New CMS	U00	all	www.amazon.com.br	
		controle switch	

		Auto Copy Over fr 1 day	
		https://prnt.sc/EVfT1bH2issc	
		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@maMXRrZqEeidIgANOiZi-g
			# successful rerun
			# done react, done sheets

	(R D-1) 2233	2022-02-19	New CMS	U00	all	www.americanas.com.br	
		'controle game sem fio',
		'aparelho de video game',
		'mini video game',
		'controle sem fio nintendo switch',
		'console switch',
		'console',
		'console nintendo switch',
		'nintendo switch',
		'controle switch',
		'controle nintendo switch',
		'console switch'

		Auto Copy Over fr 1 day
		Auto Copy Over fr 2 days	

		https://prnt.sc/M4TLsHuaiiXi
		https://prnt.sc/Noej_5gppObe
		https://prnt.sc/VQciKWkMmDxL
		https://prnt.sc/hjrAqIIBPBCB
		https://prnt.sc/oqyiNHIfvlUU
		https://prnt.sc/EdiFMieNAXRV

		https://prnt.sc/ltswIoVV3h72
		https://prnt.sc/J_b17QGxO3BJ
		https://prnt.sc/ZLl6nXgTMKQw
		https://prnt.sc/3RsAJEnxrWu-
		https://prnt.sc/QLgFLr0jMbri	

		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-19@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@98ce312a-6d9f-4cfe-91f5-ee98234a99d6@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@mFIWHkasEeeMawANOiZi-g,2022-02-19@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@mFIWHkasEeeMawANOiZi-g

			# site might had undergo maintenance
				# https://prnt.sc/rW9Yze65Fexp

	(R D-1) 2234	2022-02-19	New CMS	U00	all	www.shoptime.com.br	
		console nintendo switch	

		Data Count Mismatch  1 ( Fetched Data is Lesser than expected)
	 	https://prnt.sc/LXHLZpTomD1y	
	 	Yes	kurt

	 	# solution
	 		# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-19@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@1QomMkasEeeMawANOiZi-g
	 		# successful rerun
	 		# done react, dont sheets0.

	110@Nintendo Benelux@12:00NN@12:25AM@Prod2

	Stats:
		Rerun: 15 (17)
		Fix:  1 (1)

	-----

02 20 22
	(R D-1) Trello 1
		# https://trello.com/c/ZusiRy2v/3311-nintendo-mx-compliance-incorrect-imagecount-in-wwwelektracommx
		Spder is not fetching the complete image found on pdp

		date: 2022-02-19
		retailer: www.elektra.com.mx
		sample item_id: X00MXWC0OL000001454090202106210433

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-19@05:00:00@X00MXWC0OL000001454090202106210433

			# update spiders: no issues but added with backup selector and handles
			# valid 4 images and 1 video: https://prnt.sc/RJtlVYY6Qt-F
			# for rebuild but moved to retest because it is expected without changes

			# Issue: No issue; the last thumbnail in carousel is a video not an image
			# Action: Investigate and provide screenshot (https://prnt.sc/RJtlVYY6Qt-F)

	(R D-1) Trello 2
		# https://trello.com/c/J0i8FTk3/3314-nintendo-brazil-rankings-auto-copy-over-in-wwwamericanascombr
		Retailer:
		www.americanas.com.br

		Keywords:
		controle game sem fio
		aparelho de video game
		mini video game
		controle sem fio nintendo switch
		console switch
		console

		console nintendo switch
		nintendo switch
		controle switch
		controle nintendo switch
		console switch

		Issue: Auto Copy Over for 1 day
		Auto Copy Over for 2 days

		Screenshot:
		https://prnt.sc/M4TLsHuaiiXi
		https://prnt.sc/Noej_5gppObe
		https://prnt.sc/VQciKWkMmDxL
		https://prnt.sc/hjrAqIIBPBCB
		https://prnt.sc/oqyiNHIfvlUU
		https://prnt.sc/EdiFMieNAXRV

		https://prnt.sc/ltswIoVV3h72
		https://prnt.sc/J_b17QGxO3BJ
		https://prnt.sc/ZLl6nXgTMKQw
		https://prnt.sc/3RsAJEnxrWu-
		https://prnt.sc/QLgFLr0jMbri

		# solution
			# Issue: Server Maintenance as observed from (02-19-22 - 02-20-22)
			# Action: Investigate and provide screenshot (https://prnt.sc/Z9gzux6aw2ss)

	(R D-1) 3538	2022-02-20	New CMS	200	title	re-store.ru	
		200RULQ070000000933740202004240754	
		-1 in title		
		Yes	Louie

		 # solution
		 	# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200RULQ070000000933740202004240754
		 	# successful rerun
		 	# done react, done sheet

	(R D-1) 3539	2022-02-20	New CMS	200	all	re-store.ru	
		200RULQ070000001353440202103250548	
		invalid deadlink		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200RULQ070000001353440202103250548
			# successful rerun
			# done react, done sheets

	(R D-1) 3540	2022-02-20	New CMS	200	all	fnac.com/3P
		'200FRDU070000001381310202104140811',
		'200FRDU070000001398100202105040257',
		'200FRDU080000001381480202104140811',
		'200FRDU080000001381690202104141007'

		invalid deadlink		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200FRDU070000001381310202104140811,2022-02-20@22:00:00@200FRDU070000001398100202105040257,2022-02-20@22:00:00@200FRDU080000001381480202104140811,2022-02-20@22:00:00@200FRDU080000001381690202104141007

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200FRDU080000001381480202104140811
			# successful multiple reruns
			# done react, done sheets

	(F D-2) 3541	2022-02-20	New CMS	200	in_stock	elgiganten.dk	
		200DK100D0000000036640201901180329
		200DK10070000001560310202108230658
		wrong stocks		
		Yes	Louie

		# solution
			SELECT * FROM view_all_productdata vap  WHERE date = '2022-02-20' AND website = 'www.elgiganten.dk' AND item_id in 
			('200DK100D0000000036640201901180329','200DK10070000001560310202108230658')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200DK100D0000000036640201901180329,2022-02-20@22:00:00@200DK10070000001560310202108230658

			# wrong stock

			# check local: https://www.elgiganten.dk/product/tv-lyd-billede/horetelefoner-tilbehor/horetelefoner/bose-soundlink-around-ear-hovedtelefoner-ii-sort/SLAE2BK

			# check local: 
				# https://www.elgiganten.dk/product/tv-lyd-billede/horetelefoner-tilbehor/horetelefoner/marshall-motif-anc-true-wireless-in-ear-horetelefoner-sort/357756

			# fixing > for rebuild > fix again, goods, > rebuild again > rerun
			# successful rerun
			# done react, done sheets

	(R D-1) 3542	2022-02-20	New CMS	200	all	elgiganten.dk	
		200DK10070000000104750201902071109	
		spider issues 
		-1 values		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200DK10070000000104750201902071109
			# successful rerun
			# done react, done sheet

	-----
	(R D-1) 2242	2022-02-20	New CMS	200	all	www.dns-shop.ru	
		беспроводные наушники	

		invalid deadlink	
		https://prnt.sc/sKK4oaGMTVNs	
		Yes	Phil

		# solution
			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.dns-shop.ru' and 
			var2.keyword in (N'беспроводные наушники') and 
			var2.[date] = '2022-02-20' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-20@21:00:00@200@fcdf721a-231b-4ce6-a051-35769d9b1ebb@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	(R D-1) 2243	2022-02-20	New CMS	200	all	www.darty.com	
		casque audio reducteur de bruit	- 13
		Data Count Mismatch 1 ( Fetched Data is Lesser than expected)	
		https://prnt.sc/T-LCJvC5ZGYS	
		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-20' 
			AND website = 'www.darty.com' 
			AND keyword in (N'casque audio reducteur de bruit')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@ds73aApHEem-gAANOiOHiA
			# successful rerun
			# done react, done sheets

	(R D-1) 2244	2022-02-20	New CMS	200	all	www.fnac.com	
		ecouteur bluetooth - 25
		enceinte sans fil bluetooth - 25

		Data Count Mismatch 5 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 5 ( Fetched Data is Lesser than expected)	

		https://prnt.sc/R6wp7E9v3TFe
		https://prnt.sc/VjPgAmP2x5C6	

		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-20' 
			AND website = 'www.fnac.com' 
			AND keyword in (N'ecouteur bluetooth','enceinte sans fil bluetooth')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-20@22:00:00@200@ce335027-5c9e-4ebd-98d0-369a259bac0c@LzP6XApHEem-gAANOiOHiA,2022-02-20@22:00:00@200@65c894b9-75aa-4ab7-8315-fb0777bc8aa2@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-20@22:00:00@200@ce335027-5c9e-4ebd-98d0-369a259bac0c@LzP6XApHEem-gAANOiOHiA

			# successful rerun
			# done reacts, done sheets

	(R D-1) 2245	2022-02-20	New CMS	200	all	www.saturn.de	
		voice lautsprecher	- 16

		Data Count Mismatch 9 ( Fetched Data is Greater than expected)	
		https://prnt.sc/PHHiabVbejod	
		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-20' 
			AND website = 'www.saturn.de' 
			AND keyword in (N'voice lautsprecher')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@200@c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA
			# rerun successful
			# done react, done sheets

	(R D-2) 2246	2022-02-20	New CMS	200	all	www.re-store.ru	
		/ акустика wi-fi - 7
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники с проводом - 19
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная колонка wi-fi - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 9 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 2 ( Fetched Data is Lesser than expected)
		Data Count Mismatch 2 ( Fetched Data is Lesser than expected)	

		https://prnt.sc/39wph0QmGAJH
		https://prnt.sc/PoWfHNGhb9V7
		https://prnt.sc/dU7_DTjn4Q2p
		https://prnt.sc/y2ybpddSMlsG	

		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-20' 
			AND website = 'www.re-store.ru' 
			AND keyword in (N'портативная колонка wi-fi')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# done react, done sheets

	-----

	(F D-2) 773	2022-02-20	New CMS	100	all	www.bilka.dk	
		Aktivitetsure - 24
			https://www.bilka.dk/elektronik/telefon-og-gps/sportsure-pulsure/aktivitetsure/pl/aktivitetstrackere/
		GPS til bil - 25
			https://www.bilka.dk/elektronik/telefon-og-gps/gps/gps-til-bil/pl/gps-bil/
		Løbeur - 10
			https://www.bilka.dk/elektronik/telefon-og-gps/sportsure-pulsure/loebeure/pl/loebeure/
			

		miss match data	
		https://prnt.sc/OXCSegn0RRZw
		https://prnt.sc/fny0BCBR28QF
		https://prnt.sc/FrTEs_DsVvSI	
		Yes	michael

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-20' 
			AND website = 'www.bilka.dk' 
			AND category in (N'Aktivitetsure',
						'GPS til bil', 'Løbeur')
			GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-20@22:00:00@100@730@af07f7d0-bad8-49e3-acb6-c732243e0663,2022-02-20@22:00:00@100@730@c90d732b-0dfd-490d-b1db-eb18bc415410,2022-02-20@22:00:00@100@730@4d2a1ef7-51ff-486d-af6f-52e603aa5e85
			# multiple reruns successful
			# angry react and update sheet
			# check local: not ppt, 
				# fixed new api request
				# rebuild > rerun
			# successful rerun
			# done react, done sheet

	(R D-1) 774	2022-02-20	New CMS	100	all	www.sykkelkomponenter.no	
		sykkelruller	
		1 Day Auto Copy Over	
		https://prnt.sc/rKmeX10ELfue	
		Yes	Kristian

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-20' 
			AND website = 'www.sykkelkomponenter.no' 
			AND category in (N'sykkelruller')
			GROUP BY category, litem_id, country, category_url, source	 

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-20@22:00:00@100@JT0@2825dae3-a551-4f67-94d3-8894fbeb4f7c
			# rerun successful
			# done react, done sheets

	(R D-1) 775	2022-02-20	New CMS	200	all	www.iport.ru	
		headphones bluetooth in ear	- 13

		Data Count Mismatch 1(Fetched Data is Lesser than expected)	
		https://prnt.sc/zBayjmMTCt5-	
		Yes	kurt

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-20' 
			AND website = 'www.iport.ru' 
			AND category in (N'headphones bluetooth in ear')
			GROUP BY category, litem_id, country, category_url, source	

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-20@21:00:00@200@LU0@7c659a7a-3e62-4db0-9481-a91a10ae2a50
			# rerun successful
			# done react, done sheets

	----- ANALYTICS -----
	Focus Grit Speed Sequencing Score (SGSS)
		Total Attemps divide by percentage: 

		Commits:
			2 lisings (90%)
			5 rankings (90%)
			5 compa (90%)
			3541 (90%)
			773 (90%)
			3541 (85%)
			== 89%

	200@Zound@11:00AM@11:50AM@Prod2
	100@Garmin@12:00NN@12:35AM@Prod21

	Stats:
		Rerun: 13 (14)
		Fix: 1[2] 1[2]

02 23 22
	(R D-1) 3614	2022-02-23	New CMS	F00	all	post.lu/fr	
		'F00BEWX040000001656140202111120252',
		'F00BEWX040000001656240202111120252',
		'F00BEWX0JA000001656290202111120252',
		'F00BEWX0JA000001656310202111120252',
		'F00BEWX0JA000001656380202111120252',
		'F00BEWX0JA000001656490202111120252'

		spider issue (-1 values)		
		Yes	Glenn

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-23@None@00BEWX040000001656140202111120252,2022-02-23@22:00:00@F00BEWX040000001656240202111120252,2022-02-23@22:00:00@F00BEWX0JA000001656290202111120252,2022-02-23@22:00:00@F00BEWX0JA000001656310202111120252,2022-02-23@22:00:00@F00BEWX0JA000001656380202111120252,2022-02-23@22:00:00@F00BEWX0JA000001656490202111120252

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@F00BEWX040000001656140202111120252

			# successful multiple reruns
			# done react, done sheets

	(R D-2) 3615	2022-02-23	New CMS	K00	description	power.no	
		'K00NOZ20CG000001473570202107060432',
		'K00NOZ20CG000001473510202107060432',
		'K00NOZ204K000001554810202108180340',
		'K00NOZ205K000001555210202108180340'

		missing description		
		Yes	Switzell

		# solution
			SELECT * FROM view_all_productdata vap WHERE date = '2022-01-20' AND website = 'www.power.no' AND item_id in 
			('K00NOZ20CG000001473570202107060432',
			'K00NOZ20CG000001473510202107060432',
			'K00NOZ204K000001554810202108180340',
			'K00NOZ205K000001555210202108180340')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00NOZ20CG000001473570202107060432,2022-02-23@22:00:00@K00NOZ20CG000001473510202107060432,2022-02-23@22:00:00@K00NOZ204K000001554810202108180340,2022-02-23@22:00:00@K00NOZ205K000001555210202108180340

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00NOZ204K000001554810202108180340

			# multiple reruns failed 
			# check local: 
				#  https://www.power.no/gaming/pc/gamingmus/hyperx-pulsefire-core-gamingmus/p-1109242/?q=HyperX+Pulsefire+
			# multiple reruns successful
			# done react, done sheets

	-----
	(R D-1) 2296	2022-02-22	New CMS	K00	all	mvideo.ru	
		ГАРНИТУРА ДЛЯ ПК	
		Auto copyover fr 1 day 	
		https://prnt.sc/kFZpjfUfzWGo		
		Mich

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@aAI9AEXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheet

	(R D-1) 2298	2022-02-22	New CMS	K00	all	fnac.com	
		'SOURIS',
		'PORTABLE JEU',
		'Chaise ergonomique',
		'Casque ANC'	

		Auto copyover fr 1 day 	

		https://prnt.sc/9B0oe1pXQXJO
		https://prnt.sc/QXtdV4v6Egud
		https://prnt.sc/ubIJyqEX4i-B
		https://prnt.sc/a0iK17TDV1iO		

		michael

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-23' 
			AND website = 'www.fnac.com' 
			AND keyword in (N'SOURIS',
					'PORTABLE JEU',
					'Chaise ergonomique')
			GROUP BY keyword, ritem_id, country, SOURCE

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@22:00:00@K00@7b1eb6e4-03c3-49b1-8067-6ef1fe83358d@LzP6XApHEem-gAANOiOHiA,2022-02-23@22:00:00@K00@bd7e0886-f373-4a1a-9e6f-f2905b7ee1f8@LzP6XApHEem-gAANOiOHiA,2022-02-23@22:00:00@K00@b77bee4d-e09f-4496-b9d0-9e2af881e93f@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@22:00:00@K00@9a6c5621-7e43-4fb0-8ee1-a27cd98ffa03@LzP6XApHEem-gAANOiOHiA

			# rerun successful
			# done react, done sheets

	(R D-2) 2299	2022-02-22	New CMS	K00	all	www.box.co.uk	
		'Ps4 Controller',
		'Xbox Controller',
		'Xbox One X Controller',
		'PS Controller',
		'Ergonomic Chair',
		'chair',
		'Gaming Chair',
		'Gaming Mousemat',
		'2.1 Speakers',
		'Gaming Speakers',
		'Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Gaming Notebook',
		'Notebook',
		'Gaming Laptop',
		'Laptop',
		'mechanical keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Gaming Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse',
		'Gaming Mouse'

		Auto Copy Over fr 1 day		

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@23:00:00@K00@f54708f2-399f-43af-bfb8-20d00c1f294b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@918d322f-2c44-4c9c-aeed-5d7376531f44@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@d6871bcf-528c-4a32-a19d-c504066e1013@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@b73ac43d-b22d-4fa8-9dd4-58e266c9e7f1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@49e9ace7-ebd5-4861-8aaf-ea221ef92b83@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@c34de3e3-db49-4227-89a5-a811ffbfbb5e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@a5042d62-ba5e-4b40-ac02-fada0b36b92c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@88d127ad-cc16-4b9e-9746-7f2fda21d7ed@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@a88e5833-304b-411f-b354-3e02fbd9486a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@4ece2ecd-1087-42f9-a425-0435928a0b47@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@287f5d68-29fb-4230-8e11-b4ec207c0b9a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@bae74ad9-7e9d-4ed4-a99c-497ef7383e1e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@d57f49ea-2101-46e4-9b69-85c55112ad1b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@05d84a59-bd95-4bf2-903a-ef0f90c004ff@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@b20c5fbe-7824-4b8e-b054-857117d42119@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@114ea769-3c3c-4377-82b0-3718ef921eea@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@d951b742-2201-4753-b46a-871f3615bb69@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@b82d2b40-abf7-49b9-9a73-680f0ffbe1e1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@975dac39-523a-49ad-a614-1be58f2f8d8c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-02-23@23:00:00@K00@cbcce8a1-c80c-4b89-867e-b1b61693dc4e@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# done react, done sheets

	(R D-1) 2300	2022-02-23	New CMS	K00	all	www.emag.ro
		Mouse pad gaming
		Auto Copy Over fr 1 day	
		https://prnt.sc/GzLW39tgG4NE		
		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@21:00:00@K00@a2be15b9-f24a-4132-b4ea-09d964b496c9@qCsk7EXdEeeMawANOiZi-g
			# successful rerun
			# done raect, done sheets

	(R D-1) 2301	2022-02-23	New CMS	K00	all	virginmegastore.ae	
		Keypad	
		Auto Copy Over fr 1 day	
		https://prnt.sc/leSLy-XSoLfR		
		Kristian

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@19:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@7548df03-04bb-436a-b531-ebe3d5901cd9
			# done react, done sheets

	(R D-3) 2302	2022-02-23	New CMS	K00	all	www.otto.de
		MECHANISCHE TASTATUR
		Gaming Stuhl
			1f1ef139-e692-4bd3-a1a1-d26ea4095405@xhsDgt_tEee8oQANOijygQ

			BASETBL Gaming-Stuhl »Gaming Stuhl, Bürostuhl, Schreibtischstuhl mit fußstützen, Verstellbare Armlehne, PC Gamer Racing Stuhl, Ergonomischer Gaming Sessel, bis 150 kg belastbar« (Packung)

			https://prnt.sc/amFkO-ylIRT7

		Ergonomischer Stuhl
			7a1bf54f-a3cc-4a73-8350-60c5f98d8743@xhsDgt_tEee8oQANOijygQ

			SONGMICS Gaming-Stuhl »OBG73«, Bürostuhl, Schreibtischstuhl, ergonomisch mit Fußstützen, 67 x 66 x 116-126 cm (L x B x H)

			BASETBL Gaming-Stuhl »Gaming Stuhl, Bürostuhl, Schreibtischstuhl mit fußstützen, Verstellbare Armlehne, PC Gamer Racing Stuhl, Ergonomischer Gaming Sessel, bis 150 kg belastbar« (Packung)

			SONGMICS Gaming-Stuhl »OBG73«, Bürostuhl, Schreibtischstuhl, ergonomisch mit Fußstützen, 67 x 66 x 116-126 cm (L x B x H)

			https://prnt.sc/HaULOf-ko0eB

		duplicate product_website	
		https://prnt.sc/y27oCojEBmB9
		https://prnt.sc/xwRH7682FhAn
		https://prnt.sc/5ZXT1ZxF696f

		keen

		# solution
			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.otto.de' and 
			var2.keyword in (N'MECHANISCHE TASTATUR') and 
			var2.[date] = '2022-02-23' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00@6025fd52-2db2-42e3-9f89-e8ba5db200e6@xhsDgt_tEee8oQANOijygQ
			# successful rerun
			# done react, done sheets

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00@1f1ef139-e692-4bd3-a1a1-d26ea4095405@xhsDgt_tEee8oQANOijygQ,2022-02-23@22:00:00@K00@7a1bf54f-a3cc-4a73-8350-60c5f98d8743@xhsDgt_tEee8oQANOijygQ

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00@7a1bf54f-a3cc-4a73-8350-60c5f98d8743@xhsDgt_tEee8oQANOijygQ

			# multiple reruns failed
			# check site: valid duplicate

			# done react, done sheets

	(R D-1) 2303	2022-02-23	New CMS	K00	all	www.otto.de
		PC Keypad	- 15

		Data Count Mismatch 1 (Fetched Data is Greater than expected)		
		https://prnt.sc/WO1Z1Dn-s7wI		
		keen

		# solution
			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.otto.de' and 
			var2.keyword in (N'PC Keypad') and 
			var2.[date] = '2022-02-23' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@22:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@xhsDgt_tEee8oQANOijygQ
			# successful rerun
			# done react, done sheets

	(R D-1) 2304	2022-02-23	New CMS	K00	all	ebuyer.com	
		GAMING KEYBOARD	
		-1 product_website & -1 product_url	
		https://prnt.sc/Yrj3jgue0RwQ		
		Kristian

		# solution
			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.ebuyer.com' and 
			var2.keyword in (N'GAMING KEYBOARD') and 
			var2.[date] = '2022-02-23' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@qcWnsMefEei-eQANOiOHiA
			# successful rerun
			# done react, done sheets

	(R D-1) 2307	2022-02-23	New CMS	200	all	www.technopark.ru	
		беспроводная акустика bluetooth	

		Data Count Mismatch 1 (Fetched Data is Less than expected)	
		https://prnt.sc/1udJ8VtBcFRl	
		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@d3eb3d84-83b9-4bdc-8374-f1e7480daae1@d0bb62ad-8919-4942-a84e-09748f674d32

			# rerun successful
			# done react, done sheets

	(R D-2) 2308	2022-02-23	New CMS	200	all	www.re-store.ru	
		/ 'акустика wi-fi', 7
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'наушники с проводом', 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'портативная колонка wi-fi', 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'проводные наушники' 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4 (Fetched Data is Less than expected)
		Data Count Mismatch 7 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 9 (Fetched Data is Less than expected)	

		https://prnt.sc/8oTJn7I_92NO
		https://prnt.sc/xvIm5_d9aKTp
		https://prnt.sc/0kp3kN63e71E
		https://prnt.sc/NLM7WGFMhuvz	
		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-23' 
			AND website = 'www.re-store.ru' 
			AND keyword in (N'проводные наушники')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-23@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns
			# done react, done sheets

	(R D-1) 2309	2022-02-23	New CMS	200	all	www.dns-shop.ru	
		наушники с шумоподавлением - 25
			e021030a-78d1-43f9-8a4e-5f6c3eb7b157@BzxplkXeEeeMawANOiZi-g
		портативная акустика - 25
			eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g	

		Data Count Mismatch 7 (Fetched Data is Less than expected)
		Data Count Mismatch 7 (Fetched Data is Less than expected)	

		https://prnt.sc/9hYTNzc9Z9N7
		https://prnt.sc/rSPTPN-7AOZ3	

		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.dns-shop.ru' and 
			var2.keyword in (N'наушники с шумоподавлением') and 
			var2.[date] = '2022-02-23' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@e021030a-78d1-43f9-8a4e-5f6c3eb7b157@BzxplkXeEeeMawANOiZi-g,2022-02-23@21:00:00@200@eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g

			# multiple reruns succesful
			# done react, done sheets

	(R D-1) 2310	2022-02-23	New CMS	200	all	www.dns-shop.ru	
		наушники вкладыши проводные	
		invalid deadlink	
		https://prnt.sc/Cpv0EGzBCJ_2	
		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-23@21:00:00@200@41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done react, done sheets

	(R D-1) 2311	2022-02-23	New CMS	200	all	www.dns-shop.ru	
		акустика wi-fi	
			b49d9544-7399-4fca-b188-152c384feddc@BzxplkXeEeeMawANOiZi-g
		наушники	
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g

		Duplicate product_website & product_url
		https://prnt.sc/OZFCpVTavYPY	

		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@BzxplkXeEeeMawANOiZi-g,2022-02-23@21:00:00@200@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g

			# multiple reruns succesful
			# done react, done sheets

	0 (R D-1) 2312	2022-02-23	New CMS	200	all	www.currys.co.uk	
		bluetooth speaker	
			JBL Flip 6 Portable Bluetooth Speaker Black
			JBL Flip 6 Portable Bluetooth Speaker Black

		Duplicate product_website & product_url	
		https://prnt.sc/xe0nQOsPcmTK	
		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@23:00:00@200@18KcljsFEeeFswANOiOHiA@IO4KJA6jEeeOEgANOrFolw
			# Trello

	-----
	0 (R D-1)799	2022-02-21	New CMS	410	all	www.cotswoldoutdoor.com
		'Haglöfs Dambyxor 6',
		'Haglöfs Dambyxor M Regular Black',
		'Haglöfs Dambyxor S Regular Black',
		'Haglöfs Dambyxor XXS Regular Grey',
		'Haglöfs Herrjackor S Blue',
		'Haglöfs Herrjackor S Grey',
		'Haglöfs Herrjackor XL Grey',
		'Haglöfs Herrjackor XXL Blue'

		has -3 even if its valid etl	

		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-
		https://prnt.sc/c7WL-jVBXG5-	

		Yes	Joemike

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-23' 
			AND website = 'www.cotswoldoutdoor.com' 
			AND category in (N'Haglöfs Dambyxor 6',
					'Haglöfs Dambyxor M Regular Black',
					'Haglöfs Dambyxor S Regular Black',
					'Haglöfs Dambyxor XXS Regular Grey',
					'Haglöfs Herrjackor S Blue',
					'Haglöfs Herrjackor S Grey',
					'Haglöfs Herrjackor XL Grey',
					'Haglöfs Herrjackor XXL Blue')
			GROUP BY category, litem_id, country, category_url, source

			SELECT * FROM view_all_listings_data WHERE date = '2022-02-23' AND website = 'www.cotswoldoutdoor.com' AND category in ('Haglöfs Dambyxor 6')

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-21@02:00:00@410@3V0@6eb64d74-34ef-4711-9278-474bc026f03a,2022-02-21@02:00:00@410@3V0@4c9bee54-397c-4f47-b4b5-2b64f331b047,2022-02-21@02:00:00@410@3V0@887cd9cb-560b-4947-944e-67294e42d9d2,2022-02-21@02:00:00@410@3V0@a2d233f9-72bb-4bec-b300-e2411450d369,2022-02-21@02:00:00@410@3V0@94d196ce-a6d9-445f-afac-fe493caf6d90,2022-02-21@02:00:00@410@3V0@8b67f54d-1f11-4af7-aab8-066d12829dc2,2022-02-21@02:00:00@410@3V0@8f938176-4f1c-456c-ae58-4531ad337046,2022-02-21@02:00:00@410@3V0@1dfbf44e-0e54-45c1-95a7-fa9a7f749749

			# trello 

	-----

	----- ANALYTICS -----
	Focus Grit Speed Sequencing Score (SGSS)
		Total Attemps divide by percentage: 

		Commits:
		3614 (90%)
		2296, 3614 (90%)
		2296, 2298, 2299, 2300, 2301, 2304 (90%)
		2302, 2303 (70%)
		inriver survey (70%)
		2308 - 2312 (80%)

	https://trello.com/c/wEy550zk/3347-haglofs-listings-has-3-even-if-its-valid-etl-in-wwwcotswoldoutdoorcom

	stats:
		rerun: 16 (22) = 22/16 = 1.375

02 24 22
	/ sir Jon brownout tom
	/ trello issue haglofs report
		/ rebuild
	* assessment
	* bir
	/ clone cceep

	(R D-2) Trello 1
		799	2022-02-21	New CMS	410	all	www.cotswoldoutdoor.com
			'Haglöfs Dambyxor 6',
				410@3V0@6eb64d74-34ef-4711-9278-474bc026f03a
			'Haglöfs Dambyxor M Regular Black',
			'Haglöfs Dambyxor S Regular Black',
			'Haglöfs Dambyxor XXS Regular Grey',
			'Haglöfs Herrjackor S Blue',
			'Haglöfs Herrjackor S Grey',
			'Haglöfs Herrjackor XL Grey',
			'Haglöfs Herrjackor XXL Blue'

			has -3 even if its valid etl	

			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-
			https://prnt.sc/c7WL-jVBXG5-	

			Yes	Joemike

			# solution
				SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-21' 
				AND website = 'www.cotswoldoutdoor.com' 
				AND category in (N'Haglöfs Dambyxor 6',
							410@3V0@6eb64d74-34ef-4711-9278-474bc026f03a
						'Haglöfs Dambyxor M Regular Black',
						'Haglöfs Dambyxor S Regular Black',
							410@3V0@887cd9cb-560b-4947-944e-67294e42d9d2
						'Haglöfs Dambyxor XXS Regular Grey',
							410@3V0@a2d233f9-72bb-4bec-b300-e2411450d369

						'Haglöfs Herrjackor S Blue',
							410@3V0@94d196ce-a6d9-445f-afac-fe493caf6d90
						'Haglöfs Herrjackor S Grey',
							410@3V0@8b67f54d-1f11-4af7-aab8-066d12829dc2
						'Haglöfs Herrjackor XL Grey',

						'Haglöfs Herrjackor XXL Blue')
							410@3V0@1dfbf44e-0e54-45c1-95a7-fa9a7f749749

				GROUP BY category, litem_id, country, category_url, source

				SELECT * FROM view_all_listings_data WHERE date = '2022-02-21' AND website = 'www.cotswoldoutdoor.com' AND category in ('Haglöfs Dambyxor 6')

			# solution
				# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-21@02:00:00@410@3V0@6eb64d74-34ef-4711-9278-474bc026f03a
				# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-21@02:00:00@410@3V0@887cd9cb-560b-4947-944e-67294e42d9d2
				# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-21@02:00:00@410@3V0@a2d233f9-72bb-4bec-b300-e2411450d369

				# python rerun_lst.py -s CRAWL_FINISHED -l 2022-02-21@02:00:00@410@3V0@94d196ce-a6d9-445f-afac-fe493caf6d90,2022-02-21@02:00:00@410@3V0@8b67f54d-1f11-4af7-aab8-066d12829dc2,2022-02-21@02:00:00@410@3V0@1dfbf44e-0e54-45c1-95a7-fa9a7f749749

				# done all, successful rerun

	0 (R D-1) Trello 2
		2312	2022-02-23	New CMS	200	all	www.currys.co.uk	
			bluetooth speaker	
				JBL Flip 6 Portable Bluetooth Speaker Black
				JBL Flip 6 Portable Bluetooth Speaker Black

			Duplicate product_website & product_url	
			https://prnt.sc/xe0nQOsPcmTK	
			Yes	Phil

			# solution
				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-23@23:00:00@200@18KcljsFEeeFswANOiOHiA@IO4KJA6jEeeOEgANOrFolw
				# Trello

			# solution
				# 

	-----
	(R D-1) 3647	2022-02-24	New CMS	P00	all	krefel.be/fr	
		P00BESU01L000001420550202105180758
		P00BESU01L000001420540202105180758	

		invalid deadlinks		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@P00BESU01L000001420550202105180758,2022-02-24@22:00:00@P00BESU01L000001420540202105180758
			# rerun succesful
			# done react, done sheet

	(R D-1) 3648	2022-02-24	New CMS	P00	all	ozon.ru	
		'P00RUXZ01L000001575860202109060525',
		'P00RUXZ01L000001575870202109060525',
		'P00RUXZ01L000001575880202109060525',
		'P00RU1W01L000001460490202106280511',
		'P00RU1W01L000001477080202107120510',
		'P00RU1W01L000001454910202106220209'
		invalid deadlinks		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@21:00:00@P00RUXZ01L000001575860202109060525,2022-02-24@21:00:00@P00RUXZ01L000001575870202109060525,2022-02-24@21:00:00@P00RUXZ01L000001575880202109060525,2022-02-24@21:00:00@P00RU1W01L000001460490202106280511,2022-02-24@21:00:00@P00RU1W01L000001477080202107120510,2022-02-24@21:00:00@P00RU1W01L000001454910202106220209

			# rerun successful
			# done react, done sheets

	(R D-1) 3649	2022-01-24	New CMS	200	all	fnac.com/3P	
		'200FRDU070000001380970202104140811',
		'200FRDU0NC000001381000202104140811',
		'200FRDU0NC000001381010202104140811',
		'200FRDU070000001381310202104140811',
		'200FRDU080000001381440202104140811',
		'200FRDU070000001381570202104141007',
		'200FRDU0Q0000001381620202104141007',
		'200FRDU080000001381640202104141007',
		'200FRDU080000001381650202104141007',
		'200FRDU080000001381660202104141007',
		'200FRDU080000001381680202104141007',
		'200FRDU080000001381700202104141007'
		invalid deadlink		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@200FRDU070000001380970202104140811,2022-02-24@22:00:00@200FRDU0NC000001381000202104140811,2022-02-24@22:00:00@200FRDU0NC000001381010202104140811,2022-02-24@22:00:00@200FRDU070000001381310202104140811,2022-02-24@22:00:00@200FRDU080000001381440202104140811,2022-02-24@22:00:00@200FRDU070000001381570202104141007,2022-02-24@22:00:00@200FRDU0Q0000001381620202104141007,2022-02-24@22:00:00@200FRDU080000001381640202104141007,2022-02-24@22:00:00@200FRDU080000001381650202104141007,2022-02-24@22:00:00@200FRDU080000001381660202104141007,2022-02-24@22:00:00@200FRDU080000001381680202104141007,2022-02-24@22:00:00@200FRDU080000001381700202104141007

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@200FRDU070000001381310202104140811,2022-02-24@22:00:00@200FRDU080000001381640202104141007,2022-02-24@22:00:00@200FRDU080000001381650202104141007

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 3653	2022-02-24	New CMS	H10	in_stock	homedepot.com	
		H10USAX0Q8220220125025716668212025

		Spider	Issue in Availability		
		Yes	Keeshia

		# solution	
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@04:00:00@H10USAX0Q8220220125025716668212025
			# rerun successful
			# done react, done sheets

	(R D-1) 3654	2022-02-24	New CMS	200	all	amazon.it/3P	
		200ITGF080000000556960201904220853	
		invalid deadlink	
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@200ITGF080000000556960201904220853
			# successful rerun
			# done react, done sheets

	0 (R D-1) 3655	2022-02-24	New CMS	O00	specifications	www.wehkamp.nl	
		'O00NLH503R020211122151317790311326',
		'O00NLH503R020211122151318115404326',
		'O00NLH503R020211207074659969130341',
		'O00NLH503R020211207074700125720341',
		'O00NLH5040000001335800202102220759',
		'O00NLH5040000001335900202102220759',
		'O00NLH5040000001335980202102220759',
		'O00NLH5040000001336310202102220759',
		'O00NLH5040000001336410202102220759',
		'O00NLH5040000001336460202102220759',
		'O00NLH5040000001336550202102220759',
		'O00NLH5040000001336600202102220759',
		'O00NLH5040000001336740202102220759',
		'O00NLH5040000001336850202102220759',
		'O00NLH5040000001336870202102220759',
		'O00NLH5040000001336990202102220759',
		'O00NLH5040000001337110202102220759',
		'O00NLH5040000001337470202102220759',
		'O00NLH5040000001337540202102220759',
		'O00NLH5040000001337580202102220759',
		'O00NLH5040000001337790202102220759',
		'O00NLH5040000001337800202102220759',
		'O00NLH5040000001337820202102220759',
		'O00NLH5040000001337830202102220759',
		'O00NLH5040000001337850202102220759',
		'O00NLH5040000001337930202102220759',
		'O00NLH5040000001337950202102220759',
		'O00NLH5040000001337970202102220759',
		'O00NLH5040000001343960202103100453',
		'O00NLH5040000001354020202103290459',
		'O00NLH5040000001375830202104120812',
		'O00NLH5040000001377450202104120812',
		'O00NLH5040000001377480202104120812',
		'O00NLH5040000001377500202104120812',
		'O00NLH5040000001387700202104210357',
		'O00NLH5040000001387970202104210357',
		'O00NLH5040000001396850202105030819',
		'O00NLH5040000001396900202105030819',
		'O00NLH5040000001397030202105030819',
		'O00NLH5040000001441850202106140324',
		'O00NLH5040000001441870202106140324',
		'O00NLH5040000001474050202107060504',
		'O00NLH5040000001581820202109130240',
		'O00NLH5040000001591440202109200420'
		Missing specs; -3 value		
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@O00NLH503R020211122151317790311326,2022-02-24@22:00:00@O00NLH503R020211122151318115404326,2022-02-24@22:00:00@O00NLH503R020211207074659969130341,2022-02-24@22:00:00@O00NLH503R020211207074700125720341,2022-02-24@22:00:00@O00NLH5040000001335800202102220759,2022-02-24@22:00:00@O00NLH5040000001335900202102220759,2022-02-24@22:00:00@O00NLH5040000001335980202102220759,2022-02-24@22:00:00@O00NLH5040000001336310202102220759,2022-02-24@22:00:00@O00NLH5040000001336410202102220759,2022-02-24@22:00:00@O00NLH5040000001336460202102220759,2022-02-24@22:00:00@O00NLH5040000001336550202102220759,2022-02-24@22:00:00@O00NLH5040000001336600202102220759,2022-02-24@22:00:00@O00NLH5040000001336740202102220759,2022-02-24@22:00:00@O00NLH5040000001336850202102220759,2022-02-24@22:00:00@O00NLH5040000001336870202102220759,2022-02-24@22:00:00@O00NLH5040000001336990202102220759,2022-02-24@22:00:00@O00NLH5040000001337110202102220759,2022-02-24@22:00:00@O00NLH5040000001337470202102220759,2022-02-24@22:00:00@O00NLH5040000001337540202102220759,2022-02-24@22:00:00@O00NLH5040000001337580202102220759,2022-02-24@22:00:00@O00NLH5040000001337790202102220759,2022-02-24@22:00:00@O00NLH5040000001337800202102220759,2022-02-24@22:00:00@O00NLH5040000001337820202102220759,2022-02-24@22:00:00@O00NLH5040000001337830202102220759,2022-02-24@22:00:00@O00NLH5040000001337850202102220759,2022-02-24@22:00:00@O00NLH5040000001337930202102220759,2022-02-24@22:00:00@O00NLH5040000001337950202102220759,2022-02-24@22:00:00@O00NLH5040000001337970202102220759,2022-02-24@22:00:00@O00NLH5040000001343960202103100453,2022-02-24@22:00:00@O00NLH5040000001354020202103290459,2022-02-24@22:00:00@O00NLH5040000001375830202104120812,2022-02-24@22:00:00@O00NLH5040000001377450202104120812,2022-02-24@22:00:00@O00NLH5040000001377480202104120812,2022-02-24@22:00:00@O00NLH5040000001377500202104120812,2022-02-24@22:00:00@O00NLH5040000001387700202104210357,2022-02-24@22:00:00@O00NLH5040000001387970202104210357,2022-02-24@22:00:00@O00NLH5040000001396850202105030819,2022-02-24@22:00:00@O00NLH5040000001396900202105030819,2022-02-24@22:00:00@O00NLH5040000001397030202105030819,2022-02-24@22:00:00@O00NLH5040000001441850202106140324,2022-02-24@22:00:00@O00NLH5040000001441870202106140324,2022-02-24@22:00:00@O00NLH5040000001474050202107060504,2022-02-24@22:00:00@O00NLH5040000001581820202109130240,2022-02-24@22:00:00@O00NLH5040000001591440202109200420

			# trello: requires more time to fix

	-----

	(R D-1) 2329	2022-02-24	New CMS	O00	all	www.coolblue.nl	
		4K televisie

		1 Day ACO Data

		https://prnt.sc/Jb7p2GJqu35j		
		ailyn

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-24@22:00:00@O00@8a3fc5ac-6799-4181-a9e8-9adda584446c@dyBnppOqEei-eQANOiOHiA
			# successful rerun
			# done react, done sheet

	(R D-1) 2330	2022-02-24	New CMS	O00	all	www.mediamarkt.nl	
		Droogkast - 24
		8K televisie - 12	

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	

		https://prnt.sc/8ECoFOxDxyFc
		https://prnt.sc/h6A04owuuemC		

		ailyn

		# solution
			# 1e36d6f8-dc5a-4ab0-bbc6-92bba1de7aa7@-Sv-LJOqEei-eQANOiOHiA
			# 1ea0d70a-84c0-4393-a189-ad3c692e5fc8@-Sv-LJOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-24@22:00:00@O00@1e36d6f8-dc5a-4ab0-bbc6-92bba1de7aa7@-Sv-LJOqEei-eQANOiOHiA,2022-02-24@22:00:00@O00@1ea0d70a-84c0-4393-a189-ad3c692e5fc8@-Sv-LJOqEei-eQANOiOHiA

			# successful rerun
			# done react, done sheet

	(R D-1) 2339	2022-02-24	New CMS	200	all	www.technopark.ru
		портативная колонка с bluetooth
		Duplicate Product_website and product_url
		https://prnt.sc/8O1gvdX14Fyi
		Yes	Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-24@21:00:00@200@464302c9-2bae-4430-9574-e3f1484e9b10@d0bb62ad-8919-4942-a84e-09748f674d32
			# successful rerun
			# done react done sheets

	(R D-1) 2340	2022-02-24	New CMS	200	all	www.dns-shop.ru
		наушники с проводом
		Duplicate Product_website and product_url
		https://prnt.sc/bVMTdF52LHQb	
		Yes	Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-24@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheets

	-----

	----- ANALYTICS -----
	Focus Grit Speed Sequencing Score (SGSS)
		Total Attemps divide by percentage: 

		Commits:
		
	200@Zound@11:00AM@11:50AM@Prod2
	O00@samsungce@11:00AM@11:50AM@Prod2

	Stats:
		reruns: 12 (13)

02 26 22
	(R D-1) 3695	2022-02-26	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP803F000001449400202106150452',
		'K00RUP803F000001449160202106150452',
		'K00RUP803F000001449330202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001209310202009280432',
		'K00RUP80CG000001310900202101250600',
		'K00RUP80CG000001310920202101250600',
		'K00RUP80CG000001310880202101250600',
		'K00RUP80CG000001310860202101250600',
		'K00RUP80CG000001310870202101250600',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001373070202104050819',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000001310850202101250600',
		'K00RUP80CG000000980380202006250902',
		'K00RUP805K000001527570202108020954',
		'K00RUP80YO000001449690202106150452',
		'K00RUP80YO000001449660202106150452',
		incorrect ratings fetched 		
		Yes	Mayeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@K00RUP805O000001449050202106150452,2022-02-26@21:00:00@K00RUP807O000001449510202106150452,2022-02-26@21:00:00@K00RUP80AD000001449550202106150452,2022-02-26@21:00:00@K00RUP803F000001449400202106150452,2022-02-26@21:00:00@K00RUP803F000001449160202106150452,2022-02-26@21:00:00@K00RUP803F000001449330202106150452,2022-02-26@21:00:00@K00RUP80DO000001449630202106150452,2022-02-26@21:00:00@K00RUP80CG000000980370202006250902,2022-02-26@21:00:00@K00RUP80CG000001209310202009280432,2022-02-26@21:00:00@K00RUP80CG000001310900202101250600,2022-02-26@21:00:00@K00RUP80CG000001310920202101250600,2022-02-26@21:00:00@K00RUP80CG000001310880202101250600,2022-02-26@21:00:00@K00RUP80CG000001310860202101250600,2022-02-26@21:00:00@K00RUP80CG000001310870202101250600,2022-02-26@21:00:00@K00RUP80CG000001311060202101250600,2022-02-26@21:00:00@K00RUP80CG000001373070202104050819,2022-02-26@21:00:00@K00RUP80CG000000980450202006250902,2022-02-26@21:00:00@K00RUP80CG000001310850202101250600,2022-02-26@21:00:00@K00RUP80CG000000980380202006250902,2022-02-26@21:00:00@K00RUP805K000001527570202108020954,2022-02-26@21:00:00@K00RUP80YO000001449690202106150452,2022-02-26@21:00:00@K00RUP80YO000001449660202106150452

			# successful rerun
			# done react, done sheets

	(R D-1) 3696	2022-02-26	New CMS	K00	rating_reviewers	mvideo.ru	
		'K00RUP804K000001527710202108020954',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP804K000001449120202106150452',
		'K00RUP803F000001449360202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001527310202108020954',
		'K00RUP803F000001527370202108020954',
		'K00RUP803F000001528840202108020955',
		'K00RUP80CG000000980360202006250902',
		'K00RUP80CG000001311070202101250600',
		'K00RUP80CG000001592510202109200556',
		'K00RUP807U020220214043148827160045'

		incorrect reviews fetched		
		Yes	Mayeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@K00RUP804K000001527710202108020954,2022-02-26@21:00:00@K00RUP804K000001449090202106150452,2022-02-26@21:00:00@K00RUP804K000001528820202108020955,2022-02-26@21:00:00@K00RUP804K000001449120202106150452,2022-02-26@21:00:00@K00RUP803F000001449360202106150452,2022-02-26@21:00:00@K00RUP803F000001449420202106150452,2022-02-26@21:00:00@K00RUP803F000001527310202108020954,2022-02-26@21:00:00@K00RUP803F000001527370202108020954,2022-02-26@21:00:00@K00RUP803F000001528840202108020955,2022-02-26@21:00:00@K00RUP80CG000000980360202006250902,2022-02-26@21:00:00@K00RUP80CG000001311070202101250600,2022-02-26@21:00:00@K00RUP80CG000001592510202109200556,2022-02-26@21:00:00@K00RUP807U020220214043148827160045

			# rerun successful
			# done react, done sheets

	(R D-1) 3707	2022-02-26	New CMS	100	all	power.se	
		100SEY2010000000821080201908050425	
		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@100SEY2010000000821080201908050425
			# successful rerun

	(R D-1) 3708	2022-02-26	New CMS	F00	in_stock	proximus.be/nl/ 	
		'F00BEJX0WJ000001635190202111040405',
		'F00BEJX040000001635200202111040406',
		'F00BEIX05R020220214072904614567045',
		'F00BEIX05R020220214072829937253045',
		'F00BEIX05R020220214072837795568045',
		'F00BEJX040000001637360202111040406',
		'F00BEJX0EL000001641080202111040407',
		'F00BEJX0JA000001637480202111040406',
		'F00BEJX0JA000001637910202111040406',
		'F00BEJX0JA000001638280202111040406',
		'F00BEJX0JA000001638330202111040406',
		'F00BEJX0JA000001639250202111040406',
		'F00BEJX0JA000001638520202111040406',
		'F00BEJX0JA000001638650202111040406',
		'F00BEJX0JA000001638790202111040406',
		'F00BEJX0JA000001638850202111040406',
		'F00BEJX0JA000001639030202111040406',
		'F00BEJX0JA000001639070202111040406'

		spider issue (-3 values)		
		Yes	Crestine

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-02-26' AND item_id in ('F00BEJX0WJ000001635190202111040405',
			# 'F00BEJX040000001635200202111040406',
			# 'F00BEIX05R020220214072904614567045',
			# 'F00BEIX05R020220214072829937253045',
			# 'F00BEIX05R020220214072837795568045',
			# 'F00BEJX040000001637360202111040406',
			# 'F00BEJX0EL000001641080202111040407',
			# 'F00BEJX0JA000001637480202111040406',
			# 'F00BEJX0JA000001637910202111040406',
			# 'F00BEJX0JA000001638280202111040406',
			# 'F00BEJX0JA000001638330202111040406',
			# 'F00BEJX0JA000001639250202111040406',
			# 'F00BEJX0JA000001638520202111040406',
			# 'F00BEJX0JA000001638650202111040406',
			# 'F00BEJX0JA000001638790202111040406',
			# 'F00BEJX0JA000001638850202111040406',
			# 'F00BEJX0JA000001639030202111040406',
			# 'F00BEJX0JA000001639070202111040406')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@F00BEJX0WJ000001635190202111040405,2022-02-26@22:00:00@F00BEJX040000001635200202111040406,2022-02-26@22:00:00@F00BEIX05R020220214072904614567045,2022-02-26@22:00:00@F00BEIX05R020220214072829937253045,2022-02-26@22:00:00@F00BEIX05R020220214072837795568045,2022-02-26@22:00:00@F00BEJX040000001637360202111040406,2022-02-26@22:00:00@F00BEJX0EL000001641080202111040407,2022-02-26@22:00:00@F00BEJX0JA000001637480202111040406,2022-02-26@22:00:00@F00BEJX0JA000001637910202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638280202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638330202111040406,2022-02-26@22:00:00@F00BEJX0JA000001639250202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638520202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638650202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638790202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638850202111040406,2022-02-26@22:00:00@F00BEJX0JA000001639030202111040406,2022-02-26@22:00:00@F00BEJX0JA000001639070202111040406

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@F00BEJX040000001635200202111040406,2022-02-26@22:00:00@F00BEJX0JA000001638330202111040406

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@F00BEJX040000001635200202111040406

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 3709	2022-02-26	New CMS	H10	all	homedepot.com	
		H10USAX0Q8220220125025714658060025
		H10USAX0Q8220220125025723039038025

		-3 in Availabilty		
		Yes	Mayeeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-02-26@04:00:00@H10USAX0Q8220220125025714658060025,2022-02-26@04:00:00@H10USAX0Q8220220125025723039038025
			# successful rerun
			# done react, done sheet

	------
	(R D-1) 2364	2022-02-26	New CMS	200	all	www.dns-shop.ru	
		портативная колонка	
		3 Days Auto Copy Over 	
		https://prnt.sc/HzlRMBfURKb5	
		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-26' 
			AND website = 'www.dns-shop.ru' 
			AND keyword in (N'портативная колонка')
			GROUP BY keyword, ritem_id, country, source
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@21:00:00@200@6889e93b-60e8-4b27-a39e-690c8afbe53d@BzxplkXeEeeMawANOiZi-g

			# successful multiple reruns
			# done react, done sheets

	(R D-1) 2365	2022-02-26	New CMS	200	all	www.dns-shop.ru	
		проводные внутриканальные наушники	
		invalid deadlink	
		https://prnt.sc/yEoVun_DtW-a	
		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-26' 
			AND website = 'www.dns-shop.ru' 
			AND keyword in (N'проводные внутриканальные наушники')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@21:00:00@200@6c37e71c-f038-438f-84a2-2ba31fcd3323@BzxplkXeEeeMawANOiZi-g
			# successful multiple reruns  rerun
			# done react, done sheets

	(R D-1) 2366	2022-02-26	New CMS	200	all	www.fnac.com	
		ecouteur
			3c1bf492-7bd9-44a0-a161-4d23ecad1ee3@LzP6XApHEem-gAANOiOHiA
		enceinte bluetooth portable	
			51405791-5c01-4b1a-9f24-1e5f24dd5bbf@LzP6XApHEem-gAANOiOHiA

		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 5 (Fetched Data is Less than expected)	

		https://prnt.sc/_p3ICyQ3WdXd
		https://prnt.sc/-aJn9fxjeavU	

		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@200@3c1bf492-7bd9-44a0-a161-4d23ecad1ee3@LzP6XApHEem-gAANOiOHiA,2022-02-26@22:00:00@200@51405791-5c01-4b1a-9f24-1e5f24dd5bbf@LzP6XApHEem-gAANOiOHiA
			# successful rerun
			# done react, done sheets

	(R D-1) 2367	2022-02-26	New CMS	200	all	www.saturn.de	
		anc kopfhörer
		mini lautsprecher	

		Data Count Mismatch 19 (Fetched Data is Less than expected)
		Data Count Mismatch 7 (Fetched Data is Less than expected)	

		https://prnt.sc/2EBznODRl6Nx
		https://prnt.sc/oF0EFMvnvUf_	

		Yes	Phil

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-26' 
			AND website = 'www.saturn.de' 
			AND keyword in (N'anc kopfhörer', 'mini lautsprecher')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@200@16df96c8-a099-42b7-a0f2-6f17e28c8253@UbbyaApIEem-gAANOiOHiA,2022-02-26@22:00:00@200@4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA
			# successful rerun
			# done react, done sheets

	(R D-1) 2368	2022-02-26	New CMS	200	all	www.re-store.ru	
		/ акустика wi-fi - 7 
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники с проводом - 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3 
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ проводные наушники	- 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab


		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Less than expected)
		Data Count Mismatch 9 (Fetched Data is Less than expected)	

		https://prnt.sc/sRXi23SW3-X2
		https://prnt.sc/f4ME1N_YkaLf
		https://prnt.sc/ej5SJxJmcimE
		https://prnt.sc/f7T5itmlKejs	

		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-26@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-26@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-26@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-02-26@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2369	2022-02-26	New CMS	200	all	www.bol.com	
		Bluetooth hoofdtelefoon	
		Duplicate product_website & product_url	
		https://prnt.sc/hHoim6nNbyxt	
		Yes	
		Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.bol.com' and 
			var2.keyword in (N'Bluetooth hoofdtelefoon') and 
			var2.[date] = '2022-02-26' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2370	2022-02-26	New CMS	200	all	www.dns-shop.ru	
		акустика wi-fi
			b49d9544-7399-4fca-b188-152c384feddc@BzxplkXeEeeMawANOiZi-g
		наушники с проводом	
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g

		Duplicate product_website & product_url	
		https://prnt.sc/LVNxANWiCvVU	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			# where var2.website = 'www.dns-shop.ru' and 
			# var2.keyword in (N'акустика wi-fi') and 
			# var2.[date] = '2022-02-26' 
			# GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@BzxplkXeEeeMawANOiZi-g,2022-02-26@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@BzxplkXeEeeMawANOiZi-g,2022-02-26@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g

			# multiple reruns successful 
			# done react, done sheets

	(R D-1) 2376	2022-02-26	New CMS	U00	all	www.shoptime.com.br
		mini video game	
		Auto Copy Over for 3 day	
		https://prnt.sc/hjGYUw9a29J3	
		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@1QomMkasEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	(R D-2) 2377	2022-02-26	New CMS	U00	all	www.shoptime.com.br	
		'mini game portatil', - 25
		'aparelho de video game', - 
		'pro controller',
		'nintendo switch',
		'joy con',
		'controle switch',
		'controle sem fio nintendo switch',
		'controle nintendo switch',
		'controle joy con',
		'console switch',
		'console nintendo switch',
		'console'

		Data Count Mismatch 10 (Fetched Data is Less than expected)
		Data Count Mismatch 18 (Fetched Data is Less than expected)
		Data Count Mismatch 22 (Fetched Data is Less than expected)
		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 10 (Fetched Data is Less than expected)
		Data Count Mismatch 22 (Fetched Data is Less than expected)
		Data Count Mismatch 22 (Fetched Data is Less than expected)
		Data Count Mismatch 22 (Fetched Data is Less than expected)
		Data Count Mismatch 24 (Fetched Data is Less than expected)
		Data Count Mismatch 22 (Fetched Data is Less than expected)
		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 18 (Fetched Data is Less than expected)	

		https://prnt.sc/zbgSiJE37w3G
		https://prnt.sc/VuKxd6MzTa6P
		https://prnt.sc/sdojX9qn442e
		https://prnt.sc/-QXxQNO3iHcg
		https://prnt.sc/gsiC7ps8oBCl
		https://prnt.sc/Pzbm2iEi_zUu
		https://prnt.sc/5hMo7LEXDQWj
		https://prnt.sc/1Z-8sJGw3U85
		https://prnt.sc/bMZbRc2jidte
		https://prnt.sc/70DEXHNviJMW
		https://prnt.sc/i1bO4q29YYEy
		https://prnt.sc/UV7WBrplnhci	

		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-02-26@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@98ce312a-6d9f-4cfe-91f5-ee98234a99d6@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@1QomMkasEeeMawANOiZi-g,2022-02-26@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@1QomMkasEeeMawANOiZi-g

			# rerun successful
			# done react, done sheets

	0 (R D-1) 2378	2022-02-26	New CMS	U00	all	www.kabum.com.br	
		console switch	

		Auto Copy Over for 3 day	
		https://prnt.sc/BAwckntJrNnB	

		Yes	kurt

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-02-26' 
			AND website = 'www.kabum.com.br' 
			AND keyword in (N'console switch')
			GROUP BY keyword, ritem_id, country, SOURCE 

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@12969bc0-97ba-4c05-88fd-c8464cfe5206
			# multiple rerun failed 
			# check local: 
			# trello

	(R D-1) 2379	2022-02-26	New CMS	U00	all	www.extra.com.br	
		controle joy con	
		Auto Copy Over for 1 day	
		https://prnt.sc/x_UFMZiPU0Ew	
		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@VqLIREasEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	(R D-1) 2380	2022-02-26	New CMS	U00	all	www.mercadolivre.com.br	
		console	
		Auto Copy Over for 1 day

		https://prnt.sc/2oGEl8UzLj-x	
		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-02-26@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@qPUYcLZqEeidIgANOiZi-g
			# successful rerun
			# done react, done sheets

	0 (R D-1) 2387	2022-02-26	New CMS	U00	all	www.mercadolivre.com.br	
		aparelho de game
		aparelho de video game
		console
		console nintendo switch
		console switch
		controle game sem fio
		controle joy con
		controle nintendo switch
		controle sem fio nintendo switch
		controle switch
		joy con
		mini game portatil
		mini video game
		nintendo switch
		pro controller	

		wrong url fetched	
		https://prnt.sc/97LbAwHD8R2w	
		Yes	kurt	

		# solution
			# trello

	U00@Nintendo Brazil@12:00NN@12:25AM@Prod2
	110@Nintendo Benelux@12:00NN@12:25AM@Prod2

	------
	(F D-2) 814	2022-02-25	New CMS	110	all	www.nl.fnac.be	
		hardware	
		Auto Copy Over for 3 Days	
		https://prnt.sc/Xn8GhZ5ZxRLF	
		Yes	richelle

		# solution
			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-02-25' 
			AND website = 'www.fnac.be/nl' 
			AND category in (N'hardware')
			GROUP BY category, litem_id, country, category_url, SOURCE	

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-26@22:00:00@110@9B0@d438865e-7705-45d2-ba93-16df3e3cb3e4
			# fixing: done add cloudcraper, dynamic cookie, description
			
			# for rebuild
			# successful rerun
			# done react, done sheets
			# to improve: additional 5 data from api

	(F D-1) 815	2022-02-25	New CMS	110	all	www.fr.fnac.be	
		hardware	
		Auto Copy Over for 3 Days	
		https://prnt.sc/8JRwJP-wOfxk	
		Yes	richelle

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-02-26@22:00:00@110@AB0@32c5b7d1-abc6-4185-a4c8-7b38155a0a26
			# fixing: done add cloudcraper, dynamic cookie, description
			
			# for rebuild
			# fix pdp checker
			# fix redirect
			# rebuild
			# successful rerun

			# to improve: additional 5 data from api

	Stats:
		Rerun: 18 (19)
		Fix: 1[1] 1[2]

02 27 22
	(F D-1) Trello 1
		issue details: Duplicate product_website & product_url
		retailer: www.currys.co.uk
		keyword:
		bluetooth speaker
		screenshot: https://prnt.sc/xe0nQOsPcmTK

		# solution
			# no no issues but added with backup requester
			# moved for rebuild and for retest
			# resolved

	(F D-3) Trello 2
		Retailer:
		www.kabum.com.br

		Keyword:
		console switch

		Screenshot:
		https://prnt.sc/BAwckntJrNnB

		issue:
		Auto Copy Over for 3 day

		# solution
			# fixed dl strategy
			# fixed ws strategy
			# fixed url
			# for rebuild
			# cant confirm, copy override but rerun already sent

	(F D-3) Trello 3
		Retailer:
		www.mercadolivre.com.br

		Keywords:
		'aparelho de game',
		'aparelho de video game',
		'console',
		'console nintendo switch',
		'console switch',
		'controle game sem fio',
		'controle joy con',
		'controle nintendo switch',
		'controle sem fio nintendo switch',
		'controle switch',
		'joy con',
		'mini game portatil',
		'mini video game',
		'nintendo switch',
		'pro controller',

		Issue:
		Wrong Product_url

		Screenshot:
		https://prnt.sc/97LbAwHD8R2w

		# solution
			# extract mlb
			# extract title
			# test on few
			# for rebuild
			# 

	(R D-1) 3731	2022-02-27	New CMS	110	all	playerone.be	
		110BE911RV020211123023451947267327
		invalid deadlink		
		Yes	Glenn

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-02-27@22:00:00@110BE911RV020211123023451947267327

	110@Nintendo Benelux@12:00NN@12:25AM@Prod2
	Nintendo Brazil
	Inriver

	Stats:
		Rerun: 1
		Fix: 1[1] 1[3] 1[3]

03 02 22
	(R D-2) 2436	2022-03-02	New CMS	K00	all	fnac.com	
		SOURIS PC
		PORTABLE JEU
		PC Keypad

		Auto Copy Over for 1 Day
		Auto Copy Over for 2 Days	https://prnt.sc/mKXX9g44vY3o
		https://prnt.sc/kBnOuzzAH5X8
		https://prnt.sc/zKXD7bCiV8VC	
		Yes	michael

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@22:00:00@K00@bd7e0886-f373-4a1a-9e6f-f2905b7ee1f8@LzP6XApHEem-gAANOiOHiA,2022-03-02@22:00:00@K00@0f34c756-1c03-42e4-bfe7-0c604fb9fe27@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@22:00:00@K00@bd7e0886-f373-4a1a-9e6f-f2905b7ee1f8@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@22:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2437	2022-03-02	New CMS	K00	all	mvideo.ru	
		Контроллер PlayStation	
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	
		https://prnt.sc/rjQywzuquBVW	
		Yes	michelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@21:00:00@@K00@f4b38090-8593-426e-a62f-630e83b51d8c@aAI9AEXeEeeMawANOiZi-g
			# successful rerun
			# doen react, done sheets

	(R D-1) 2438	2022-03-02	New CMS	K00	all	virginmegastore.ae	
		Ergonomic Chair
		1 Day Auto Copy Over	
		https://prnt.sc/bU_AVpNankPw	
		Yes	Kristian

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@19:00:00@K00@d6871bcf-528c-4a32-a19d-c504066e1013@7548df03-04bb-436a-b531-ebe3d5901cd9
			# rerun successful
			# done react, done sheet

	(R D-2) 2439	2022-03-02	New CMS	K00	all	www.box.co.uk	
		'Streaming',
		'anc headphones',
		'Xbox Controller',
		'Xbox One X Controller',
		'PS Controller',
		'chair',
		'Gaming Chair',
		'Gaming Mousemat',
		'Mousemat',
		'2.1 Speakers',
		'Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Gaming Notebook',
		'Notebook',
		'Gaming Laptop',
		'Laptop',
		'mechanical keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Gaming Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse',
		'Gaming Mouse'

		Auto Copy Over for 1 day	

		https://prnt.sc/GVKKzNeL5PJN
		https://prnt.sc/vHXlt_sOdoys
		https://prnt.sc/F9QutQN2cLiz
		https://prnt.sc/PeOLN6IMP2wy
		https://prnt.sc/UoSNxO7ftp50
		https://prnt.sc/Hszt5B7vzfS6
		https://prnt.sc/5_GpJ_Jdj2vj
		https://prnt.sc/1qL-puOpioaP
		https://prnt.sc/FmG8MfIvuyC7
		https://prnt.sc/UrXZh14YL199
		https://prnt.sc/u8oJA3lwnuha
		https://prnt.sc/FF6OsJ2TophZ
		https://prnt.sc/O_JYSCKahbjR
		https://prnt.sc/zqj5hVmvF7VF
		https://prnt.sc/dESC-xVdPfQw
		https://prnt.sc/CZGv4uWBm3_7
		https://prnt.sc/yC0dOj0Iip0b
		https://prnt.sc/z8MKWguw_-4u
		https://prnt.sc/TOEIro06Pdes
		https://prnt.sc/h4r4ixYL2oKY
		https://prnt.sc/E192HgKr1oUE
		https://prnt.sc/K_SXI5hJQjHu
		https://prnt.sc/hdwBX84B4fkD
		https://prnt.sc/eULODgxb5vhy
		https://prnt.sc/nyTPYr7qFIF8
		https://prnt.sc/xHGXMazo1zDB
		https://prnt.sc/1qfPLWMclfD_
		https://prnt.sc/sgQ198vxiIiI
		https://prnt.sc/Z4RueO03h-3o	

		Yes	kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@23:00:00@K00@f54708f2-399f-43af-bfb8-20d00c1f294b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@ef6e28af-4210-4946-b21a-daeaa5c4ed38@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@918d322f-2c44-4c9c-aeed-5d7376531f44@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@b73ac43d-b22d-4fa8-9dd4-58e266c9e7f1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@49e9ace7-ebd5-4861-8aaf-ea221ef92b83@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@c34de3e3-db49-4227-89a5-a811ffbfbb5e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@a5042d62-ba5e-4b40-ac02-fada0b36b92c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@a88e5833-304b-411f-b354-3e02fbd9486a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@4ece2ecd-1087-42f9-a425-0435928a0b47@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@287f5d68-29fb-4230-8e11-b4ec207c0b9a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@e88ea211-dbf7-43af-afbd-c122b70031b6@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@bae74ad9-7e9d-4ed4-a99c-497ef7383e1e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@d57f49ea-2101-46e4-9b69-85c55112ad1b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@05d84a59-bd95-4bf2-903a-ef0f90c004ff@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@b20c5fbe-7824-4b8e-b054-857117d42119@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@114ea769-3c3c-4377-82b0-3718ef921eea@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@b82d2b40-abf7-49b9-9a73-680f0ffbe1e1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@21959252-c63d-402e-babc-d9467671756c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@975dac39-523a-49ad-a614-1be58f2f8d8c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-02@23:00:00@K00@cbcce8a1-c80c-4b89-867e-b1b61693dc4e@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306
			# rerun successful
			# done react, done sheets

	(R D-1) 2440	2022-03-02	New CMS	K00	all	otto.de	
		Xbox Controller
		auto copy over for 1 day		
		https://prnt.sc/EuEIfSPqKv3C	
		Yes	KEEN

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@22:00:00@K00@975dac39-523a-49ad-a614-1be58f2f8d8c@xhsDgt_tEee8oQANOijygQ
			# rerun successful
			# done react, done sheets

	(R D-1) 2447	2022-03-02	New CMS	K00	all	komplett.no
		Ps4-kontroller	
		Data Count Mismatch 1  (Fetched Data is Lesser than expected)	
		https://prnt.sc/ywRDhlHxz3Ak	
		Yes	keen

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@22:00:00@K00@06273a7f-725a-4460-9ab1-90c43510c21a@sWDIkgihEeeOEgANOrFolw
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@22:00:00@K00@06273a7f-725a-4460-9ab1-90c43510c21a@sWDIkgihEeeOEgANOrFolw
			# multiple reruns successful
			# done react, done sheets

	(R D-3) 2453	2022-03-02	New CMS	K00	all	dns-shop.ru	
		БЕСПРОВОДНАЯ ГАРНИТУРА
			'd4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g',
		ГАРНИТУРА ДЛЯ ПК
			'4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g',
		ИГРОВАЯ ГАРНИТУРА
			'b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g',
		КЛАВИАТУРА
			'5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g',
		МЕХАНИЧЕСКАЯ КЛАВИАТУРА
			'2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g',
		Клавиатура
			'5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g',
		Механическая клавиатура
			'2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g',
		Динамики
			'0810e6f3-da20-4952-929a-68c5da92ecc6@BzxplkXeEeeMawANOiZi-g',
		Игровой стул
			'a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g',
		Контроллер Xbox One X
			'4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g',
		Контроллер PS4
			'784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g',
		Коврик для мыши	
			9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g
		Игровые колонки
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g	
		Коврик для мыши
			9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g

		Auto Copy Over for 3 Days
		Auto Copy Over for 1 Day
		Auto Copy Over for 1 Day
		Auto Copy Over for 1 Day
		Auto Copy Over for 1 Day
		Auto Copy Over for 1 Day
		Auto Copy Over for 1 Day
		Auto Copy Over for 2 Days
		Auto Copy Over for 1 Day
		Auto Copy Over for 3 Days
		Auto Copy Over for 3 Days
		Auto Copy Over for 2 Days	

		https://prnt.sc/ky7DTLdXYm0c
		https://prnt.sc/RDI8CxTUp_z1
		https://prnt.sc/mWIj2OJzI8wg
		https://prnt.sc/UYdkZQzMQwjn
		https://prnt.sc/Zi257mMUzsma
		https://prnt.sc/UYdkZQzMQwjn
		https://prnt.sc/Zi257mMUzsma
		https://prnt.sc/QkCKgaIaAzZf
		https://prnt.sc/G689UvP3C-Jr
		https://prnt.sc/ivkzLVKfXVM7
		https://prnt.sc/0zkO0KOzm1s7
		https://prnt.sc/cupihtXXbOF8	

		Yes	Richelle

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.dns-shop.ru' 
				AND ritem_id in ('d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g',
				'4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g',
				'b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g',
				'5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g',
				'2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g',
				'5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g',
				'2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g',
				'0810e6f3-da20-4952-929a-68c5da92ecc6@BzxplkXeEeeMawANOiZi-g',
				'a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g',
				'4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g',
				'784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@21:00:00@K00@d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@0810e6f3-da20-4952-929a-68c5da92ecc6@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g,2022-03-02@21:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done rect, done sheets

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-02@20:00:00@K00@9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g

	(R D-2) 2454	2022-03-02	New CMS	K00	all	dns-shop.ru
		МЫШЬ - 11 (25)
			29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g
			https://prnt.sc/5nQ4VCxlp4GJ
		Игровые колонки - 11 (25)
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
			https://prnt.sc/XHasvT_zOQtx
		Контроллер Xbox - 16
			bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g
			https://prnt.sc/v83eqcpBzpBe

		Data Count Mismatch 7 (Fetched Data is Greater than expected)
		Data Count Mismatch 7 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	

		https://prnt.sc/-9E_lDbVJse_
		https://prnt.sc/jGhVYe_QdtH7
		https://prnt.sc/Xngb6PXVz5GT

		Yes	Richelle

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.dns-shop.ru' 
				AND keyword in (N'Контроллер Xbox')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@20:00:00@K00@29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g,2022-03-02@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-02@20:00:00@K00@bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@20:00:00@K00@bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g

			# multiple reruns failed
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2455	2022-03-02	New CMS	K00	all	dns-shop.ru	
		Контроллер PlayStation

		Invalid Deadlink	
		https://prnt.sc/8HlnEueSX8jv	
		Yes	Richelle

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.dns-shop.ru' 
				AND keyword in (N'Контроллер PlayStation')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@BzxplkXeEeeMawANOiZi-g
			# multiple reruns successful
			# done react, done sheets

	(R D-2) 2456	2022-03-02	New CMS	K00	all	citilink.ru	
		/ ГАРНИТУРА ДЛЯ ПК - 24
			4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g
		/ Контроллер PS4 - 3
			784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	

		https://prnt.sc/4JqSHAc32J3k
		https://prnt.sc/x-oTPSGQGhgP
		Yes	Richelle

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.citilink.ru' 
				AND keyword in (N'Контроллер PS4')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g,2022-03-02@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g

			# multiple reruns successful: assisted by sir Ryan Banilad
			# done react, done sheets

	(R D-2) 2457	2022-03-02	New CMS	K00	all	citilink.ru	
		PS Контроллер - 8
			f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g
		/ Контроллер PlayStation - 9
			f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g

		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 3 (Fetched Data is Greater than expected)

		https://prnt.sc/hZubM_UlFgwk
		https://prnt.sc/U1Ap99PacE5u	

		Yes	Richelle

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.citilink.ru' 
				AND keyword in (N'Контроллер PlayStation')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-02@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-02@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g

			# trello: 16 rerun attempts
				# Ryan suggestion: refer to this ticket: https://trello.com/c/P0RWk3uE/2622-razerrankings-new-cmsdata-count-mismatch-fetched-data-is-lesser-than-expected-in-citilinkru

			# 

	(R D-1) 2458	2022-03-02	New CMS	K00	all	citilink.ru
		Игровой коврик для мыши

		Auto Copy Over for 3 Days

		https://prnt.sc/24jmfLKeUbVp
		Yes	Richelle

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-02' 
				AND website = 'www.citilink.ru' 
				AND keyword in (N'Игровой коврик для мыши')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-02@20:00:00@K00@0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@8THUOEXdEeeMawANOiZi-g
			# done react, done sheets

	Stats:
		Rerun: 12 (19) = 1.58

03 03 22
	0 (R D-1) Trello 1
		Please remove html/css tags in the description

		date:2022-03-02
		retailer: www.power.dk
		items:
		K00DKX207U020211115034625647709319
		K00DKX20CG000001487450202107130336
		K00DKX207U020220222101457559740053

		# solution
			# https://www.power.dk/gaming-og-underholdning/gaming-moebler/gaming-stole/razer-iskur-gamingstol/p-1141432/
			# https://www.power.dk/gaming-og-underholdning/gaming-moebler/gaming-stole/razer-enki-x-gaming-stol/p-1206461/
			# https://www.power.dk/computere-og-tablets/tastatur-og-mus/tastatur/razer-pro-type-ultra-traadloest-tastatur/p-1235007/

	-----
	0 (R D-1) 3825	2022-03-03	New CMS	200	in_stock	hintaopas.fi	
		200FIF0070000000549910201904050430
			# https://hintaopas.fi/product.php?q=Acton%202%20BT&p=4924848
		200FIF0080000000551220201904050635	
			# https://hintaopas.fi/product.php?q=Sumpan%20Black&p=3343540
		Wrong Stocks Fetch		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-03' AND website = 'www.hintaopas.fi'	AND item_id in ('200FIF0070000000549910201904050430', '200FIF0080000000551220201904050635')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-03@21:00:00@200FIF0070000000549910201904050430,2022-03-03@21:00:00@200FIF0080000000551220201904050635

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-03@21:00:00@200FIF0070000000549910201904050430
			# for rerun after rebuild
			# assisted by sir Paul Ryan
				# issue still exist
				# for Trello: for observation

	(R D-1) 3823	2022-03-03	New CMS	O00	specifications	www.wehkamp.nl	
		'O00NLH5040000001337470202102220759',
		'O00NLH5040000001337910202102220759',
		'O00NLH5040000001337970202102220759'
		Missing specifications		
		Yes	Jurena

		# solution
			# fixed by sir ryan
			# for rerun
			# create script after rankings reruns
			# for rerun after rebuild

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-03@22:00:00@O00NLH5040000001337470202102220759,2022-03-03@22:00:00@O00NLH5040000001337910202102220759,2022-03-03@22:00:00@O00NLH5040000001337970202102220759

			# done by someone
			# done react, done sheets

	-----

	(R D-1) 2464	2022-03-03	New CMS	O00	all	www.coolblue.nl	
		NEO QLED	
		1 Day Auto Copy Over	
		https://prnt.sc/WfSfcEZPqgTJ	
		Yes	ailyn

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-03' 
				AND website = 'www.coolblue.nl' 
				AND keyword in (N'NEO QLED')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@O00@70ef8b5c-7f58-4c4d-ac71-81cd13ddbfd4@dyBnppOqEei-eQANOiOHiA
			# rerun successful
			# done react, done sheets

	(R D-1) 2465	2022-03-03	New CMS	200	all	www.dns-shop.ru
		беспроводная акустика
		Duplicate product website and product url
		https://prnt.sc/d9Ncc-xm7-wR
		Yes	Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-03@21:00:00@200@56587abe-ec6a-4464-b129-8218839eebce@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	(R D-1) 2467	2022-03-03	New CMS	200	all	www.dns-shop.ru
		наушники накладные беспроводные с bluetooth
		Invalid Deadlink
		https://prnt.sc/Zp9IAdTz6wZu
		Yes	Rayyan

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@21:00:00@200@e4842b4e-bd1e-4aaa-8054-33faad7516b6@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	(F D-1) 2473	2022-03-03	New CMS	O00	all	www.exellent.be/nl	
		was-droogcombinatie	
		Auto copy over for 3 days	
		https://prnt.sc/SWb-44wz_3VD	
		Yes	keen

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@O00@b78aa926-43ea-4c10-bdc0-943201617b2c@e8506ab2-4e86-4961-b273-853ce5ee882b
			# multiple reruns failed
			# check local: fix page checker
			# for rebuild
			# successful rerun
			# done react, done sheets

	(R D-2) 2474	2022-03-03	New CMS	200	all	www.fnac.com
		casque audio
		casque à reduction de bruit active	

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)

		https://prnt.sc/10RjqVVVC_RR
		https://prnt.sc/CJx25UGTt78B

		Yes	Christopher

		# solution
			#SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-03' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio',
						'casque à reduction de bruit active')
				GROUP BY keyword, ritem_id, country, source
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-03@22:00:00@200@b2db2e37-0274-4bc0-b760-ae162877018e@LzP6XApHEem-gAANOiOHiA,2022-03-03@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@200@b2db2e37-0274-4bc0-b760-ae162877018e@LzP6XApHEem-gAANOiOHiA,2022-03-03@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@200@b2db2e37-0274-4bc0-b760-ae162877018e@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2475	2022-03-03	New CMS	200	all	www.fnac.com	
		casque à réduction de bruit	

		Invalid Deadlink	
		https://prnt.sc/S47NAbK_VE7-	
		Yes	Christopher

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-03' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque à réduction de bruit')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@200@8a1b70f9-4eb2-47ea-ae5b-782ef2545e4f@LzP6XApHEem-gAANOiOHiA
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2476	2022-03-03	New CMS	200	all	www.fnac.com	
		ecouteur intra auriculaire	
		day 3 auto copy over
		https://prnt.sc/2MdpAF1tHp1D	
		Yes	Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@200@6356baa6-4979-44ca-bb9d-e88fc7ef4735@LzP6XApHEem-gAANOiOHiA
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2477	2022-03-03	New CMS	200	all	www.saturn.de	
		mini lautsprecher	

		Duplicate product website and product url	
		https://prnt.sc/it18iEK5MX9Z	
		Yes	Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-03@22:00:00@200@4db15ec8-b7a3-4ba6-b40c-e72e9f486bea@UbbyaApIEem-gAANOiOHiA
			# successful rerun
			# done react, done sheets

	-----
	(R D-1) 841	2022-03-03	New CMS	O00	all	www.bol.com	
		Wash/Dry Combo	

		Duplicate Product Website/Product Url	
		https://prnt.sc/JH8PoUApdzPq	
		Yes	Joemike

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.bol.com' and 
				var2.category in (N'Wash/Dry Combo') and 
				var2.[date] = '2022-03-03' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source	

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-03@22:00:00@O00@N10@9b7211ba-e899-407c-b87a-2d5e6123f08b
			# successful rerun
			# done react, done sheet

	(R D-1) 842	2022-03-03	New CMS	O00	all	www.vandenborre.be/nl	
		Vacuum	
		1 Day Auto Copy Over	
		https://prnt.sc/BPpvR_kZf-Yt	
		Yes	Joemike

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-03' 
				AND website = 'www.vandenborre.be/nl' 
				AND category in (N'Vacuum')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-03@22:00:00@O00@MT0@8d2a8089-f9fd-47ae-aeaf-1d4d921d8c36
			# successful rerun
			# done react, done sheets

	(R D-1) 845	2022-03-03	New CMS	200	all	www.dns-shop.ru
		Headphones Wired In Ear	
		Invalid Deadlink	
		https://prnt.sc/aara_Kt_XEvw	
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-03@21:00:00@200@L80@6a25b104-c13d-4732-9931-0843e14b977e
			# successful rerun
			# done react, done sheets

	O00@samsungce@11:00AM@11:50AM@Prod2
	K00@Razer@11:00AM@11:25AM@Prod2

	Stats:
		Rerun: 13 (14) = 1.08
		Fix: 1[1]

03 04 22
	(R D-1) 3841	2022-03-04	New CMS	Q00	image_count	www.cotswoldoutdoor.com
		Q00GB3V0IJ000001612620202110040508	
		Missing image_count		
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.cotswoldoutdoor.com'	AND item_id in ('Q00GB3V0IJ000001612620202110040508')
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@Q00GB3V0IJ000001612620202110040508
			# successful rerun
			# done react, done sheets

	(R D-1) 3842	2022-03-04	New CMS	Q00	image_count	www.bever.nl	
		Q00NL8W0IJ000001629930202110250542	
		Missing image_count		
		Yes	Jurena

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@Q00NL8W0IJ000001629930202110250542
			# successful rerun
			# done react, done shset

	(R D-2) 3843	2022-03-04	New CMS	610	image_count	www.nordicnest.se	
		'610SEXW00P000001492780202107200326',
		'610SEXW00P000001492870202107200326',
		'610SEXW00P000001492900202107200327',
		'610SEXW00P000001492930202107200327',
		'610SEXW00P000001492990202107200327',
		'610SEXW00P000001493170202107200327',
		'610SEXW00P000001493440202107200327',
		'610SEXW00P000001493470202107200327',
		'610SEXW00P000001493500202107200327',
		'610SEXW00P000001493530202107200327',
		'610SEXW00P000001493620202107200327',
		'610SEXW00P000001494910202107200327',
		'610SEXW00P000001495240202107200327',
		'610SEXW00P000001496110202107200327',
		'610SEXW00P000001496140202107200327',
		'610SEXW00P000001496170202107200327',
		'610SEXW00P000001496260202107200327',
		'610SEXW00P000001496290202107200327',
		'610SEXW00P000001496320202107200327',
		'610SEXW00P000001497160202107200327',
		'610SEXW00P000001610260202109300304',
		'610SEXW00P000001610280202109300304',
		'610SEXW00P000001610290202109300304',
		'610SEXW00P000001610300202109300304',
		'610SEXW00P000001610310202109300304',
		'610SEXW00P000001610320202109300304',
		'610SEXW00P000001610330202109300304',
		'610SEXW00P000001610390202109300304',
		'610SEXW00P000001610410202109300304',
		'610SEXW00P000001630730202110280537',
		'610SEXW00P000001631280202110280537',
		'610SEXW00P000001631320202110280537',
		'610SEXW00P000001631350202110280537',
		'610SEXW01P000001493680202107200327',
		'610SEXW01P000001493740202107200327',
		'610SEXW01P000001493980202107200327',
		'610SEXW01P000001494010202107200327',
		'610SEXW01P000001494040202107200327',
		'610SEXW01P000001494130202107200327',
		'610SEXW01P000001494220202107200327',
		'610SEXW01P000001494280202107200327',
		'610SEXW01P000001494370202107200327',
		'610SEXW01P000001494550202107200327',
		'610SEXW01P000001495330202107200327',
		'610SEXW01P000001495450202107200327',
		'610SEXW01P000001495480202107200327',
		'610SEXW01P000001495750202107200327',
		'610SEXW01P000001495780202107200327',
		'610SEXW01P000001495870202107200327',
		'610SEXW01P000001496020202107200327',
		'610SEXW01P000001496650202107200327',
		'610SEXW01P000001496740202107200327',
		'610SEXW01P000001497040202107200327',
		'610SEXW01P000001497130202107200327',
		'610SEXW01P000001497460202107200328',
		'610SEXW01P000001497730202107200328',
		'610SEXW01P000001497850202107200328',
		'610SEXW01P000001497970202107200328',
		'610SEXW01P000001498180202107200328',
		'610SEXW01P000001498330202107200328',
		'610SEXW01P000001498360202107200328',
		'610SEXW01P000001498600202107200328',
		'610SEXW01P000001498690202107200328',
		'610SEXW01P000001498750202107200328',
		'610SEXW01P000001498840202107200328',
		'610SEXW01P000001498990202107200328',
		'610SEXW01P000001499110202107200328',
		'610SEXW01P000001499170202107200328',
		'610SEXW01P000001499260202107200328',
		'610SEXW01P000001499320202107200328',
		'610SEXW01P000001499500202107200328',
		'610SEXW01P000001499620202107200328',
		'610SEXW01P000001610450202109300304',
		'610SEXW01P000001610460202109300304'
		Missing image_count		
		Yes	Jurena

		 # solution
		 	# rerun script not copies because it was too long
		 	# multiple reruns successful
		 	# done react, done sheets

	(R D-1) 3846	2022-03-04	New CMS	200		elgiganten.dk	
		200DK100N0000000039810201901180331
		200DK100Q0000000032320201901180326

		Wrong price, stocks and DQ		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK100N0000000039810201901180331', '200DK100Q0000000032320201901180326')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200DK100Q0000000032320201901180326,2022-03-04@22:00:00@200DK100N0000000039810201901180331

			# successful rerun
			# done react, done sheets

	(W) 3847	2022-03-04	New CMS	200	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-04' and webshot_url is NULL	No Webshots		
		Yes	Mojo

		# solution
			# rerun sent
			# resent 
			# resent 
				99 
				97 
				94
				76
				70
				51
				47
				45 
				44
				41
				36

			python manual_cmp_webshots.py -c 200 -i 2022-03-04@22:00:00@200SE300D0000000029740201901180326,2022-03-04@22:00:00@200SE300P0000000029830201901180326,2022-03-04@22:00:00@200SE300C0000000029940201901180326,2022-03-04@22:00:00@200SE30070000000030020201901180326,2022-03-04@04:00:00@200USC0070000000030120201901180326,2022-03-04@22:00:00@200SE300B0000000031530201901180326,2022-03-04@22:00:00@200SE300G0000000031890201901180326,2022-03-04@22:00:00@200SE300D0000000031970201901180326,2022-03-04@22:00:00@200SE300Q0000000032330201901180326,2022-03-04@22:00:00@200SE300D0000000032470201901180326,2022-03-04@22:00:00@200SE30070000000032880201901180326,2022-03-04@04:00:00@200USC0070000000032980201901180326,2022-03-04@22:00:00@200SE300P0000000033420201901180329,2022-03-04@22:00:00@200SE300P0000000033560201901180329,2022-03-04@22:00:00@200SE30070000000033900201901180329,2022-03-04@04:00:00@200USC0070000000034020201901180329,2022-03-04@22:00:00@200SE30070000000034430201901180329,2022-03-04@04:00:00@200USC0070000000034560201901180329,2022-03-04@22:00:00@200SE300D0000000035080201901180329,2022-03-04@22:00:00@200SE300D0000000036650201901180329,2022-03-04@22:00:00@200SE300D0000000038150201901180330,2022-03-04@22:00:00@200SE300C0000000038810201901180331,2022-03-04@22:00:00@200SE300C0000000039300201901180331,2022-03-04@22:00:00@200SE300P0000000040560201901180331,2022-03-04@22:00:00@200SE300B0000000041750201901180334,2022-03-04@22:00:00@200SE300C0000000043640201901180334,2022-03-04@22:00:00@200SE300B0000000044080201901180334,2022-03-04@22:00:00@200SE300D0000000044130201901180334,2022-03-04@04:00:00@200USC00D0000000103120201902071109,2022-03-04@04:00:00@200USC00C0000000103330201902071109,2022-03-04@04:00:00@200USC00D0000000103480201902071109,2022-03-04@04:00:00@200USC00B0000000103610201902071109,2022-03-04@04:00:00@200USC00P0000000103800201902071109,2022-03-04@04:00:00@200USC00Q0000000104070201902071109,2022-03-04@04:00:00@200USC00D0000000104180201902071109,2022-03-04@04:00:00@200USC00P0000000104350201902071109,2022-03-04@04:00:00@200USC00P0000000104410201902071109,2022-03-04@04:00:00@200USC00C0000000104490201902071109,2022-03-04@04:00:00@200USC00D0000000104790201902071109,2022-03-04@04:00:00@200USC00O0000000105000201902071110,2022-03-04@04:00:00@200USC00D0000000105110201902071110,2022-03-04@04:00:00@200USC00P0000000105270201902071110,2022-03-04@04:00:00@200USC00D0000000105400201902071110,2022-03-04@22:00:00@200FR210B0000000105640201902071110,2022-03-04@04:00:00@200USC00C0000000105730201902071110,2022-03-04@04:00:00@200USC00N0000000105890201902071111,2022-03-04@04:00:00@200USC00P0000000105990201902071111,2022-03-04@04:00:00@200USC00N0000000106230201902071111,2022-03-04@04:00:00@200USC00B0000000106310201902071111,2022-03-04@04:00:00@200USC00P0000000106580201902071112,2022-03-04@04:00:00@200USC00J0000000106730201902071112,2022-03-04@04:00:00@200USC00C0000000106790201902071112,2022-03-04@04:00:00@200USC00O0000000106900201902071112,2022-03-04@04:00:00@200USC00D0000000107060201902071112,2022-03-04@04:00:00@200USC00F0000000107250201902071113,2022-03-04@04:00:00@200USC00B0000000107310201902071113,2022-03-04@04:00:00@200USC0070000000566690201905170407,2022-03-04@04:00:00@200USC0070000000590250201905211054,2022-03-04@22:00:00@200SE30070000000702740201905280513,2022-03-04@22:00:00@200SE30070000000702810201905280517,2022-03-04@04:00:00@200USC0070000000702920201905280525,2022-03-04@22:00:00@200SE300NC000000846240201909081337,2022-03-04@04:00:00@200USC00NC000000861040201910140545,2022-03-04@04:00:00@200USC0070000000929070202003300343,2022-03-04@22:00:00@200SE30070000000929090202003300343,2022-03-04@22:00:00@200SE30070000000984360202007030257,2022-03-04@04:00:00@200USC0070000001216380202010090538,2022-03-04@04:00:00@200USC0070000001245700202011200116,2022-03-04@22:00:00@200SE30070000001245760202011200116,2022-03-04@22:00:00@200SE30080000001245980202011200116,2022-03-04@22:00:00@200SE30080000001246110202011200116,2022-03-04@04:00:00@200USC0070000001311110202101260209,2022-03-04@22:00:00@200SE30070000001397270202105040205,2022-03-04@04:00:00@200USC0070000001474390202107060614,2022-03-04@04:00:00@200USC0070000001474520202107060614,2022-03-04@04:00:00@200USC0070000001536870202108100127,2022-03-04@04:00:00@200USC0070000001559890202108230652,2022-03-04@04:00:00@200USC0070000001559900202108230658,2022-03-04@22:00:00@200SE30070000001560110202108230658,2022-03-04@22:00:00@200SE30070000001560430202108230658

		# set to manual by validator (mojo)

	(W) EMEA REGION WEBSHOTS
		# created script and query
		# re webshot sent
			66
			58
			54
			42
			22
			13
			12
			
		# re webshot sent
			11
			3

		# done 
			# Angel set the 3 remaining to manual
			# done react

	(R D-1) 3848	2022-03-04	New CMS	610	webshot_url	nordicnest.se	all item_ids	
		Webshot Issue (modal issue)	
		https://prnt.sc/aOR_m-iBw-Ro	
		Yes	Mojo

		# solution
			# done re webshot. assisted by sir jave
			# done react, done sheets

	(R D-1) 3853	2022-03-04	New CMS	K00	all	alza.cz	
		'K00CZ3604K000001526880202108020954',
		'K00CZ3603F000001526950202108020954',
		'K00CZ360CG000001485750202107130335',
		'K00CZ360CG000001489420202107130337',
		'K00CZ360FO000001527050202108020954',
		'K00CZ3605K000001527060202108020954'
		spider issue -1 values		
		Yes	maRyEEEEL

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.alza.cz'	AND item_id in ('K00CZ3604K000001526880202108020954',
				'K00CZ3603F000001526950202108020954',
				'K00CZ360CG000001485750202107130335',
				'K00CZ360CG000001489420202107130337',
				'K00CZ360FO000001527050202108020954',
				'K00CZ3605K000001527060202108020954')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@K00CZ3604K000001526880202108020954,2022-03-04@22:00:00@K00CZ3603F000001526950202108020954,2022-03-04@22:00:00@K00CZ360CG000001485750202107130335,2022-03-04@22:00:00@K00CZ360CG000001489420202107130337,2022-03-04@22:00:00@K00CZ360FO000001527050202108020954,2022-03-04@22:00:00@K00CZ3605K000001527060202108020954

			# successful rerun 
			# done react, done sheets

	(R D-1) 3854	2022-03-04	New CMS	K00	all	power.dk	
		'K00DKX20CG000001487140202107130336',
		'K00DKX20CG000001487260202107130336',
		'K00DKX20CG000001487270202107130336'
		spider issue -1 values		
		Yes	maRyEEEEL

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.power.dk'	AND item_id in ('K00DKX20CG000001487140202107130336',
			# 'K00DKX20CG000001487260202107130336',
			# 'K00DKX20CG000001487270202107130336')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@K00DKX20CG000001487140202107130336,2022-03-04@22:00:00@K00DKX20CG000001487260202107130336,2022-03-04@22:00:00@K00DKX20CG000001487270202107130336

			# successful rerun
			# doen react, done sheets

	0 (R D-1) 3857	2022-03-04	New CMS	Q00	image_count	naturligvis.com	
		'Q00DKSU0IJ000001407680202105101010',
		'Q00DKSU0IJ000001551810202108170817',
		'Q00DKSU0IJ000001551890202108170817',
		'Q00DKSU0IJ000001551910202108170817',
		'Q00DKSU0IJ000001551960202108170817',
		'Q00DKSU0IJ000001552080202108170817',
		'Q00DKSU0IJ000001552150202108170817',
		'Q00DKWY0IJ000001618340202110110911'

		missing image count		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-03-04' AND WEBSITE = 'www.naturligvis.com' AND item_id in ('Q00DKSU0IJ000001407680202105101010',
			'Q00DKSU0IJ000001551810202108170817',
			'Q00DKSU0IJ000001551890202108170817',
			'Q00DKSU0IJ000001551910202108170817',
			'Q00DKSU0IJ000001551960202108170817',
			'Q00DKSU0IJ000001552080202108170817',
			'Q00DKSU0IJ000001552150202108170817',
			'Q00DKWY0IJ000001618340202110110911')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@Q00DKSU0IJ000001407680202105101010,2022-03-04@22:00:00@Q00DKSU0IJ000001551810202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001551890202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001551910202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001551960202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001552080202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001552150202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001618340202110110911

			# Trello: 
			# done react, done sheets

	0 (R D-1)3858	2022-03-04	New CMS	Q00	description	naturligvis.com	
		all item_id	
		missing description		
		Yes	Switzell

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@Q00DKSU0IJ000001552000202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001583020202109130622,2022-03-04@22:00:00@Q00DKSU0IJ000001552080202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001652140202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001652370202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001611170202110040508,2022-03-04@22:00:00@Q00DKSU0IJ000001530710202108030602,2022-03-04@22:00:00@Q00DKSU0IJ000001567930202108300607,2022-03-04@22:00:00@Q00DKWY0IJ000001595100202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001583190202109130622,2022-03-04@22:00:00@Q00DKWY0IJ000001651860202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001594840202109220247,2022-03-04@22:00:00@Q00DKWY0IJ000001606750202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001674480202112060317,2022-03-04@22:00:00@Q00DKWY0IJ000001578290202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001408540202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001652070202111091221,2022-03-04@22:00:00@Q00DKSU0IJ000001551880202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001407880202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001577970202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001407390202105101010,2022-03-04@22:00:00@Q00DKSU0IJ000001407730202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001611350202110040508,2022-03-04@22:00:00@Q00DKWY0IJ000001606680202109290108,2022-03-04@22:00:00@Q00DKSU0IJ000001407680202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001611100202110040508,2022-03-04@22:00:00@Q00DKSU0IJ000001552150202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001652240202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001652660202111091221,2022-03-04@22:00:00@Q00DKSU0IJ000001407380202105101010,2022-03-04@22:00:00@Q00DKSU0IJ000001551890202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001606710202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001652480202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001594980202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001583050202109130622,2022-03-04@22:00:00@Q00DKWY0IJ000001607050202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001671730202111290731,2022-03-04@22:00:00@Q00DKWY0IJ000001584230202109130622,2022-03-04@22:00:00@Q00DKWY0IJ000001577760202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001530690202108030602,2022-03-04@22:00:00@Q00DKSU0IJ000001551810202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001596070202109220248,2022-03-04@22:00:00@Q00DKSU0IJ000001552830202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001595670202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001618340202110110911,2022-03-04@22:00:00@Q00DKWY0IJ000001674390202112060317,2022-03-04@22:00:00@Q00DKWY0IJ000001595730202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001595810202109220248,2022-03-04@22:00:00@Q00DKSU0IJ000001552100202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001408020202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001652200202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001578010202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001551960202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001675540202112140618,2022-03-04@22:00:00@Q00DKWY0IJ000001651900202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001630040202110250542,2022-03-04@22:00:00@Q00DKWY0IJ000001652010202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001577960202109080312,2022-03-04@22:00:00@Q00DKWY0IJ000001584190202109130622,2022-03-04@22:00:00@Q00DKWY0IJ000001611290202110040508,2022-03-04@22:00:00@Q00DKWY0IJ000001618300202110110911,2022-03-04@22:00:00@Q00DKWY0IJ000001652650202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001652400202111091221,2022-03-04@22:00:00@Q00DKSU0IJ000001552750202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001408170202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001651970202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001676080202112140650,2022-03-04@22:00:00@Q00DKSU0IJ000001552780202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001594900202109220247,2022-03-04@22:00:00@Q00DKWY0IJ000001652040202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001578350202109080312,2022-03-04@22:00:00@Q00DKWY0IJ000001611310202110040508,2022-03-04@22:00:00@Q00DKSU0IJ000001407070202105101010,2022-03-04@22:00:00@Q00DKSU0IJ000001552690202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001607170202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001652160202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001671700202111290731,2022-03-04@22:00:00@Q00DKWY0IJ000001595790202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001618440202110110911,2022-03-04@22:00:00@Q00DKWY0IJ000001652100202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001595160202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001652060202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001578340202109080312,2022-03-04@22:00:00@Q00DKWY0IJ000001606880202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001676060202112140650,2022-03-04@22:00:00@Q00DKWY0IJ000001595630202109220248,2022-03-04@22:00:00@Q00DKSU0IJ000001567890202108300607,2022-03-04@22:00:00@Q00DKSU0IJ000001407410202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001652170202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001606950202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001595130202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001578360202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001552070202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001595750202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001651930202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001652510202111091221,2022-03-04@22:00:00@Q00DKWY0IJ000001584150202109130622,2022-03-04@22:00:00@Q00DKWY0IJ000001595190202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001578310202109080312,2022-03-04@22:00:00@Q00DKSU0IJ000001407970202105101010,2022-03-04@22:00:00@Q00DKWY0IJ000001652000202111091221,2022-03-04@22:00:00@Q00DKSU0IJ000001552660202108170817,2022-03-04@22:00:00@Q00DKSU0IJ000001407430202105101010,2022-03-04@22:00:00@Q00DKSU0IJ000001551910202108170817,2022-03-04@22:00:00@Q00DKWY0IJ000001606980202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001595050202109220248,2022-03-04@22:00:00@Q00DKWY0IJ000001606910202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001606860202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001606970202109290108,2022-03-04@22:00:00@Q00DKWY0IJ000001578300202109080312

			# Trello
			# done react, done sheets

	0(R D-1) 3859	2022-03-04	New CMS	200	all	amazon.it/3P	
		200ITGF070000000557480201904220941
		200ITGF080000000556780201904220839	
		invalid deadlink		
		Yes	Mojo

		200ITGF070000000557480201904220941

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-03-04' AND WEBSITE = 'www.amazon.it/3P' AND item_id in ('200ITGF070000000557480201904220941', '200ITGF080000000556780201904220839')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200ITGF070000000557480201904220941,2022-03-04@22:00:00@200ITGF080000000556780201904220839
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200ITGF070000000557480201904220941
			# Multiple reruns failed
			# for trello: for ACO

	(R D-1) 3860	2022-03-04	New CMS	H10	in_stock	homedepot.com	
		H10USAX0Q8220220131092100271766031
		Availability.
		Should be Instock		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata vap  WHERE date = '2022-03-04' AND website = 'www.homedepot.com' AND item_id in 
			('H10USAX0Q8220220131092100271766031')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@04:00:00@H10USAX0Q8220220131092100271766031
			# successful rerun
			# doen react, done sheets

	(R D-1) 3861	2022-03-04	New CMS	H10	webshot_url		
		select * from view_all_productdata where date='2022-03-04' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.ferguson.com', 'www.grainger.com')
		and webshot_url is NULL 
		order by item_id
		No Webshots		Yes	Keeshia

		# solution
			# done: assisted by sir jave through creation of query and python rerun script
			# done react, done sheets

	(R D-1) 3862	2022-03-04	New CMS	200	all	idealo.es	
		200ES8D080000000790700201906140221	
		invalid deadlink		
		Yes	Moj

		# solution
			# SELECT * FROM view_all_productdata WHERE date = '2022-03-04' AND item_id IN ('200ES8D080000000790700201906140221')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200ES8D080000000790700201906140221
			# successful rerun
			# done react, done sheets

	-----
	(R D-1) 2478	2022-03-04	New CMS	610	all	www.bagarenochkocken.se	
		vattenkokare	
		1 Day Auto Copy Over	
		https://prnt.sc/t5JVdW5WNDvY	
		Yes	Joemike

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@610@2d20b344-262b-4527-81b1-bc11c6d76df1@fa8feddc-0176-46a8-a009-a493cd47be61
			# successful rerun
			# done react, done sheets

	(R D-1) 2479	2022-03-04	New CMS	Q00	all	www.johnlewis.com	
		Ski jackets women	- 6
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/tgHtITNN7390	
		Yes	ailyn

		# solution
			# SELECT * FROM view_all_rankingsdata var WHERE date = '2022-03-04' AND website = 'www.johnlewis.com' AND keyword in (N'Ski jackets women')

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@Q00@dad82486-e06a-48d9-b0a8-e16125d2b834@GWJcVrlSEeem0wANOiOHiA
			# successful rerun
			# done react, done sheets

	(R D-1) 2480	2022-03-04	New CMS	Q00	all	www.snowleader.com	 
		'Imperméable hommes',
		'Pantalon d"extérieur hommes',
		'Vestes Gore-Tex hommes' 

        1  Day Auto Copy Over	

        https://prnt.sc/llp7zm3PFFWx
		https://prnt.sc/UNk8DyfLdYH8
		https://prnt.sc/ud8h7hz5AwT	Yes	Rod

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.snowleader.com' 
				AND keyword in (N'Imperméable hommes',
						'Pantalon d''extérieur hommes',
						'Vestes Gore-Tex hommes' )
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@8811f9dc-edae-4dab-bacc-fcccfa7f668b@6652b08f-c068-4953-9eae-cccb8e4d1025,2022-03-04@22:00:00@Q00@e173f2f1-d0d5-4d92-be8c-e814faefc2e4@6652b08f-c068-4953-9eae-cccb8e4d1025

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@abf84d35-15bb-4de0-9a88-3aa5089e1811@6652b08f-c068-4953-9eae-cccb8e4d1025

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2481	2022-03-04	New CMS	Q00	all	www.neye.dk	
		skijakke dame	
		Auto copy over for 1 day	
		https://prnt.sc/4JG0wjAWdEvB	
		Yes	renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.neye.dk' 
				AND keyword in (N'skijakke dame')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@5fb8d8da-a688-4aaa-a7c0-79800ed15824@c2682348-af37-483a-86f4-2f55378406ff
			# successful rerun
			# done react, done sheets

	(R D-1) 2483	2022-03-04	New CMS	200	all	www.fnac.com	
		'casque audio',
		'enceinte connectée',
		'casque à reduction de bruit active',

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)

		https://prnt.sc/I6m0qNeEW3-e
		https://prnt.sc/Y4SigOedcg00
		https://prnt.sc/Zmv28vhl8F-x	

		Yes	Christopher

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio',
						'enceinte connectée',
						'casque à reduction de bruit active')
				GROUP BY keyword, ritem_id, country, source


			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200@b2db2e37-0274-4bc0-b760-ae162877018e@LzP6XApHEem-gAANOiOHiA,2022-03-04@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-04@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-04@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# multiple reruns succesful
			# done react, done sheets

	(R D-1) 2484	2022-03-04	New CMS	200	all	www.bol.com	
		Bluetooth hoofdtelefoon	
		Duplicate product website and product url	
		https://prnt.sc/Fpkhka5CSivu	
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'Bluetooth hoofdtelefoon') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA\
			# multiple reruns successful
			# # done react, done sheets

	(R D-1) 2485	2022-03-04	New CMS	200	all	www.mediamarkt.be/fr	
		smart speaker	- 25
		Data Count Mismatch 4 (Fetched Data is Lesser than expected)	
		https://prnt.sc/7l5Ae9vzl0ME	
		Yes	Christopher

		# solution	
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.mediamarkt.be/fr' 
				AND keyword in (N'smart speaker')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@JpQcJEXgEeeMawANOiZi-g
			# rerun successful
			# doen react, done sheets

	(R D-1) 2486	2022-03-04	New CMS	200	all	www.dns-shop.ru	
		наушники
		Invalid Deadlink	
		https://prnt.sc/7cku_vtY1xxQ
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@21:00:00@200@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2487	2022-03-04	New CMS	200	all	www.dns-shop.ru	
		наушники с шумоподавлением
			e021030a-78d1-43f9-8a4e-5f6c3eb7b157@BzxplkXeEeeMawANOiZi-g
		проводные наушники
			bfed8a03-9385-4010-9dec-9360fe56bc5b@BzxplkXeEeeMawANOiZi-g
		гарнитура
			bee38489-f39a-4402-9a45-4b7a61706873@BzxplkXeEeeMawANOiZi-g

		Duplicate product website and product url	

		https://prnt.sc/nKiCRA4l7deO
		htps://prnt.sc/A11pmG1Rqmk4

		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники с шумоподавлением') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@e021030a-78d1-43f9-8a4e-5f6c3eb7b157@BzxplkXeEeeMawANOiZi-g,2022-03-04@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@BzxplkXeEeeMawANOiZi-g,2022-03-04@21:00:00@200@bee38489-f39a-4402-9a45-4b7a61706873@BzxplkXeEeeMawANOiZi-g

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2488	2022-03-04	New CMS	200	all	www.re-store.ru	
		акустика wi-fi - 8
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		НАУШНИКИ С ПРОВОДОМ - 19
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	

		https://prnt.sc/FlnXSLfeI3i_
		https://prnt.sc/9b6sZQASjKQD	

		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.re-store.ru' and 
				var2.keyword in (N'НАУШНИКИ С ПРОВОДОМ') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-04@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# done react, done sheets

	0 (R D-1) 2489	2022-03-04	New CMS	200	all	www.re-store.ru	
		НАУШНИКИ С ШУМОПОДАВЛЕНИЕМ	
		Invalid Deadlink	
		https://prnt.sc/vVk-MemCF2jE	
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.re-store.ru' and 
				var2.keyword in (N'НАУШНИКИ С ШУМОПОДАВЛЕНИЕМ') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@21:00:00@200@e021030a-78d1-43f9-8a4e-5f6c3eb7b157@a4059226-f529-46c6-8a35-c120219fbbab
			# Trello blocked
			# done react, done sheets

	(R D-1) 2490	2022-03-04	New CMS	200	all	www.technopark.ru
		наушники активная система шумоподавления - 25
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		https://prnt.sc/aQazdNdaj7FR
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.technopark.ru' and 
				var2.keyword in (N'наушники активная система шумоподавления') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@ca380523-00cd-433e-90f5-1bf0d2c982a6@d0bb62ad-8919-4942-a84e-09748f674d32

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2491	2022-03-04	New CMS	Q00	all	www.scandinavianoutdoor.fi	
		Naisten Talvihousut	
		Auto copy over for 1 day	
		https://prnt.sc/G7b_A2RnxScD	
		Yes	renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.scandinavianoutdoor.fi' 
				AND keyword in (N'Naisten Talvihousut')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@21:00:00@Q00@cfdb2608-071e-4367-9d31-e77e44f149b3@ef6560a2-abb4-428a-af56-39a7a544c1dd
			# successful rerun
			# done react, done sheets

	(R D-1) 2492	2022-03-04	New CMS	Q00	all	www.sportsmagasinet.no
		fritidsbukse kvinner	
		Auto copy over for 1 day	
		https://prnt.sc/IjBkOPXjxs2V	
		yes	flor

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.sportsmagasinet.no' 
				AND keyword in (N'fritidsbukse kvinner')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@19076b9c-6087-4569-a70a-2bf55f17dfac@15ae38d9-e3d8-476d-8870-76a5a46f8fc3
			# successful rerun
			# done react, done sheets

	(R D-1) 2493	2022-03-04	New CMS	Q00	all	www.bever.nl	
		Gore-tex schoenen	
		1 Day Auto Copy Over 	
		https://prnt.sc/dn98CLyGsC88	
		Yes	ailyn

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.bever.nl' 
				AND keyword in (N'Gore-tex schoenen')
				GROUP BY keyword, ritem_id, country, source
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@7e3af622-6255-454c-a931-92c6ba7d5989@e3f4e065-5786-4860-981f-fe6ba7a542b4
			# done react, done sheets

	(R D-1) 2494	2022-03-04	New CMS	Q00	all	www.bergzeit.de	
		'Freizeithosen männer',
			79bfe464-bef1-43dc-a31b-9224318a1d84@bd8b190f-904c-4b92-aeb9-b1657b260972


			Keyword: Freizeithosen männer
			Product Name: 
				Maloja Damen PianaM. Hose
				Maloja Damen LantschettaM. Hose
				CMP Herren Stretch Structure Hose
				Alprausch Damen Chillerhose Hose				

			https://prnt.sc/K4yo8PA6QPvL
			https://prnt.sc/WgmIc5262i4i
			https://prnt.sc/ldNBhg7QkyEF
			https://prnt.sc/erXBiW37RIRS

		'Hüttenschuhe herren'
			04973702-2f30-471e-b528-81c6b1112691@bd8b190f-904c-4b92-aeb9-b1657b260972

			Keyword: Hüttenschuhe herren
			Product Name:
			Rab Cirrus Hausschuhe
			Rab Cirrus Slipper Hausschuhe
			Rab Down Slipper Hausschuhe
			The North Face Herren Thermoball Traction Mule Schuhe
			
			https://prnt.sc/FawLAofLSjrR
			https://prnt.sc/pt0_Uj3QN3nx
			https://prnt.sc/TglmvzEVVAVe
			https://prnt.sc/qn-RqpFVYXHf

		duplicate product_website

		https://prnt.sc/Hdrvq4CaUxhI
		https://prnt.sc/8gAJpUOr0KW4
		Yes	keen

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bergzeit.de' and 
				var2.keyword in (N'Hüttenschuhe herren') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@Q00@79bfe464-bef1-43dc-a31b-9224318a1d84@bd8b190f-904c-4b92-aeb9-b1657b260972,2022-03-04@22:00:00@Q00@04973702-2f30-471e-b528-81c6b1112691@bd8b190f-904c-4b92-aeb9-b1657b260972

			# done react, done sheets

	(R D-1) 2495	2022-03-04	New CMS	Q00	all	www.campz.de	
		Laufhosen männer
		1 Day Auto Copy Over 	
		https://prnt.sc/GjhGWFt3uarg
		Yes	keen

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.campz.de' 
				AND keyword in (N'Laufhosen männer')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@22:00:00@Q00@93aaf5b1-db9a-4bce-abbb-63b38f369974@716d5c42-2ae4-4bb0-8c51-325933c5eef8
			# successful rerun
			# done react, done sheets\

	(R D-1) 2496	2022-03-04	New CMS	Q00	all	www.wiggle.co.uk	
		Hiking jackets men	
		1 Day Auto Copy Over	
		https://prnt.sc/aZEfuURJpwrB	
		Yes	rod

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.wiggle.co.uk' and 
				var2.keyword in (N'Hiking jackets men') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@23:00:00@Q00@947edc84-8244-4a09-8705-4c30b0fe4f6c@883f2489-860b-4515-9e30-9d70600eed5d
			# successful rerun
			# done react, done sheets

	(R D-1) 2497	2022-03-04	New CMS	Q00	all	www.wiggle.co.uk	
		Waterproof jackets men	- 14
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/SoQY2ibvPGbD	
		Yes	rod

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.wiggle.co.uk' and 
				var2.keyword in (N'Waterproof jackets men') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@Q00@ba33dc0f-9a1b-4623-b4f5-6e0e3c38339e@883f2489-860b-4515-9e30-9d70600eed5d

			# rerun successful
			# done react, done sheets

	(R D-1) 2498	2022-03-04	New CMS	200	all	www.target.com
		bluetooth speakers
			081206d9-6161-49ab-9c0c-c00acaaebde2@0l0XDL-yEei-eQANOiOHiA
			Bose SoundLink Micro Bluetooth Speaker - Black (783342-0100)

		portable speaker
			42e6294a-567d-40b0-b773-06d6e745ab46@0l0XDL-yEei-eQANOiOHiA
			Bose SoundLink Flex Portable Bluetooth Speaker - Blue
			https://prnt.sc/L17tpz4DXyqk

		Duplicate product website and product url	

		https://prnt.sc/2OIr4MAW35iy
		https://prnt.sc/_d1N6zW2-xDc
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.target.com' and 
				var2.keyword in (N'portable speaker') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-04@04:00:00@200@081206d9-6161-49ab-9c0c-c00acaaebde2@0l0XDL-yEei-eQANOiOHiA,2022-03-04@04:00:00@200@42e6294a-567d-40b0-b773-06d6e745ab46@0l0XDL-yEei-eQANOiOHiA

			# done react, done sheets

	0 (R D-1) 2499	2022-03-04	New CMS	Z00 all	www.abt.com	
		'alexa',
		'amp',
		'Bluetooth Speaker',
		'home theater receiver',
		'portable bluetooth speaker',
		'portable speaker',
		'receiver',
		'smart speaker',
		'Sound bar',
		'Soundbar',
		'soundbars',
		'speakers bluetooth wireless',
		'stereo receiver',
		'surround sound',
		'wireless speaker'
		1 Day Auto Copy Over	
		https://prnt.sc/W8sdkhBpcPib	
		Yes	ailyn

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.abt.com' 
				AND keyword in (N'alexa',
						'amp',
						'Bluetooth Speaker',
						'home theater receiver',
						'portable bluetooth speaker',
						'portable speaker',
						'receiver',
						'smart speaker',
						'Sound bar',
						'Soundbar',
						'soundbars',
						'speakers bluetooth wireless',
						'stereo receiver',
						'surround sound',
						'wireless speaker')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@04:00:00@Z00@9224e01c-1747-4192-b1da-275c02b60406@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@6dc581b7-a9b8-4e7f-a198-270a8593c35e@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@18KcljsFEeeFswANOiOHiA@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@53a5b9fb-bee7-476e-9331-00db85e49214@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@83f9d554-437c-487c-b214-6a015bf9f65b@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@42e6294a-567d-40b0-b773-06d6e745ab46@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@c2488cd9-a106-4aa2-9f0f-52e1154aed13@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@d28ab242-6497-4333-9a57-723386971be7@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@e2ac17ec-8055-421c-8e43-bacae88e10e3@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@5e496670-d21e-4f1f-ac97-d261102f2e73@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@afd67553-ac55-4fa0-9380-2714a3f394cc@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@e7f39f20-b3fd-40dc-8762-067b02387194@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@f9d6d588-a738-413f-a8af-1deec79a60db@e47776ec-dabf-4777-af22-5651577ae4af,2022-03-04@04:00:00@Z00@f0e304c3-9f65-4c95-afb0-a531b79c0488@e47776ec-dabf-4777-af22-5651577ae4af

			# multiple reruns failed
			# trello:https://trello.com/c/U0FvW0io/3403-sonos-rankings-auto-copy-over-in-wwwabtcom
			# done react, done sheets

	(R D-1) 2500	2022-03-04	New CMS	Z00 	all	www.costco.ca	
		Speaker	
		1 Day Auto Copy Over	
		https://prnt.sc/32afdSkTrjfJ	
		Yes	ailyn

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-04' 
				AND website = 'www.costco.ca' 
				AND keyword in (N'Speaker')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-04@04:00:00@Z00@c716c87e-dc84-4e31-b1e0-255cf61c0d5b@IMq4iEauEeeMawANOiZi-g
			# successful rerun
			# done react, done sheets

	-----
	(R D-1) 847	2022-03-04	New CMS	200	all	www.amazon.co.uk	
		'Headphones Bluetooth In Ear',
		'Headphones Bluetooth On Ear'
		fetching data even if its deadlink	
		https://prnt.sc/RiVGZ5t9jSM6	
		Yes	Jairus

		# solution
		 	# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.amazon.co.uk' and 
				var2.category in (N'Headphones Bluetooth In Ear',
						'Headphones Bluetooth On Ear') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

		 	# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@200@G00@bd91be1f-2495-49d5-8220-906f55242bd6,2022-03-04@23:00:00@200@G00@a3b64247-6b9d-4f54-818d-b88889214619
		 	# set to manual? 
		 			# done react, done sheets

	(R D-1) 848	2022-03-04	New CMS	200	all	www.fnac.com	
		Headphones Wired In Ear	- 25
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/v-Aa4bD_-Msd	
		Yes	Jairus

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.fnac.com' and 
				var2.category in (N'Headphones Wired In Ear') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200@210@ce58e593-1df6-4099-8c2d-ecb5f0806f11

			# successful rerun
			# done react, done sheets

	(R D-1) 849	2022-03-04	New CMS	200	all	www.mediamarkt.de	
		Headphones Noise Cancelling	- 25 
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/6Upajn8U2iJl	
		Yes	Jairus

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.mediamarkt.de' and 
				var2.category in (N'Headphones Noise Cancelling	') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@22:00:00@200@810@a9ffcaa0-ad45-4dd4-8149-8aa4370b7976

			# successful rerun
			# done react, done sheets

	(R D-2) 850	2022-03-04	New CMS	200	all	www.re-store.ru
		/ Headphones Bluetooth On Ear  - 25
			200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90
		/ Headphones Noise Cancelling - 25
			200@LQ0@9a67dfe8-7a48-4a2e-a92e-76526cff38c0
		/ Headphones Wired In Ear - 8
			200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f
		/ Speakers Voice - 3
			200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

		Data Count Mismatch (Fetched Data is Lesser than expected)
		https://prnt.sc/j4GrR1OyiDlB	
		Yes	Jairus

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.re-store.ru' and 
				var2.category in (N'Speakers Voice') and 
				var2.[date] = '2022-03-04' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-04@21:00:00@200@LQ0@9a67dfe8-7a48-4a2e-a92e-76526cff38c0,2022-03-04@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-03-04@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-04@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-04@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# multiple reruns successful
			# done react, done sheets

	-----
	Sir Jave
		python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@Q00GBX10IJ000001428160202106010433,2022-03-04@23:00:00@Q00GBX10IJ000001428180202106010433,2022-03-04@23:00:00@Q00GBX10IJ000001428250202106010433,2022-03-04@23:00:00@Q00GBX10IJ000001531020202108030602,2022-03-04@23:00:00@Q00GBX10IJ000001531040202108030602,2022-03-04@23:00:00@Q00GBX10IJ000001531060202108030602,2022-03-04@23:00:00@Q00GBX10IJ000001595960202109220248,2022-03-04@23:00:00@Q00GBX10IJ000001630160202110250542

		python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-04@23:00:00@Q00GBX10IJ000001595960202109220248

	Stats:
		Reruns: 39 (41) = 41/39
		Webshots: 2

03 05 22
	0 (R D-1) Trello 1
		Date: 2022-03-01
		Issue: incomplete specification in orange.lu/fr
		Retailer: www.orange.lu/fr
		Item IDs:

		all item_ids

		F00BEKX0EL000001643580202111060938
		F00BEKX040000001644020202111070520
		F00BEJX0KB020220214082622090652045

		https://www.orange.lu/fr/telephones/xiaomi-11t/
		https://www.orange.lu/fr/telephones/samsung-galaxy-z-fold3-5g/
		https://www.orange.lu/fr/telephones/apple-iphone-13-pro/

		F00@Samsung Mobile@10:00AM@10:30AM@Prod2

	-----
	(R D-1) 3874	2022-03-05	New CMS	110	title	www.gamemania.be/nl	
		110BELW0RV020211109092309804511313	

		Incorrect brand in title		
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata where date='2022-01-23' AND website = 'www.gamemania.be/nl'	AND item_id in ('110BELW0RV020211109092309804511313')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@110BELW0RV020211109092309804511313
			# multiple reruns failed
			# check local: manual override? changes wont reflect
			# done react, done sheet

	(R D-1) 3875	2022-03-05	New CMS	P00	all	krefel.be/fr	
		'P00BESU01L000001420530202105180758',
		'P00BESU01L000001420480202105180758',
		'P00BESU01L000001420470202105180758'

		Spider Issue		
		Yes	Keeshia

		# solution
			SELECT * FROM view_all_productdata vap WHERE date = '2022-03-05' AND WEBSITE = 'www.krefel.be/fr' AND item_id in ('P00BESU01L000001420530202105180758',
			'P00BESU01L000001420480202105180758',
			'P00BESU01L000001420470202105180758')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@P00BESU01L000001420530202105180758,2022-03-05@22:00:00@P00BESU01L000001420480202105180758,2022-03-05@22:00:00@P00BESU01L000001420470202105180758

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@P00BESU01L000001420530202105180758

			# multiple reruns successful
			# done react, done sheet

	(W) EMEA Region webshots
		# python manual_cmp_webshots.py -c A00 -i 2022-03-05@22:00:00@A00DE8Q0KT020220302122957662752061,2022-03-05@22:00:00@A00DE8Q0KT020220302122957315808061,2022-03-05@22:00:00@A00DE8Q0KT020211115034837583060319,2022-03-05@22:00:00@A00DE810KT020211115034838247849319,2022-03-05@22:00:00@A00DEY00KT020220302122953556496061,2022-03-05@22:00:00@A00DE910KT020220302122955812015061,2022-03-05@22:00:00@A00DE810KT020220302122956136085061,2022-03-05@22:00:00@A00DE8Q0KT020220302122951455311061,2022-03-05@22:00:00@A00DE8Q0KT020220302122952503858061,2022-03-05@22:00:00@A00DE810KT020220302122952584668061,2022-03-05@22:00:00@A00DEY00KT020211012064142009791285,2022-03-05@22:00:00@A00DE8Q0KT020211012064141581340285,2022-03-05@22:00:00@A00DE8Q0KT020211012064139128225285,2022-03-05@22:00:00@A00DE810KT020211012064139181032285,2022-03-05@22:00:00@A00DE810KT020211012064144527127285,2022-03-05@22:00:00@A00DE8Q0KT020211012064144977531285,2022-03-05@22:00:00@A00DE810KT020211012064145055931285,2022-03-05@22:00:00@A00DE810KT020211012064145585830285,2022-03-05@22:00:00@A00DE810KT020211012064146083288285,2022-03-05@22:00:00@A00DE8Q0KT020211012064147851921285,2022-03-05@22:00:00@A00DE8Q0KT020211012064148341910285,2022-03-05@22:00:00@A00DE810KT020211012064148687786285,2022-03-05@22:00:00@A00DE810KT020211012064149242284285,2022-03-05@22:00:00@A00DE910KT020211012064137526850285,2022-03-05@22:00:00@A00DE8Q0KT020211012064143447359285,2022-03-05@22:00:00@A00DEY00KT020211012064142502589285,2022-03-05@22:00:00@A00DE8Q0KT020211012064137321634285,2022-03-05@22:00:00@A00DEY00RB000000872610201911060954,2022-03-05@22:00:00@A00DE810RB000000872490201911060945,2022-03-05@22:00:00@A00DE810RB000000872300201911060926,2022-03-05@22:00:00@A00DEZ00RB000000872000201911060833,2022-03-05@22:00:00@A00DE910RB000000873550201911061147,2022-03-05@22:00:00@A00DE810RB000000873450201911061131,2022-03-05@22:00:00@A00DE810RB000000875010201911070627,2022-03-05@22:00:00@A00DEY00RB000000875350201911070657,2022-03-05@22:00:00@A00DEY00RB000000875530201911070707,2022-03-05@22:00:00@A00DEY00RB000000949870202005220114,2022-03-05@22:00:00@A00DE910RB000000950070202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000950150202005220114,2022-03-05@22:00:00@A00DE810RB000000950240202005220114,2022-03-05@22:00:00@A00DEY00RB000000950360202005220114,2022-03-05@22:00:00@A00DE810RB000000950390202005220114,2022-03-05@22:00:00@A00DE910RB000000950420202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000950550202005220114,2022-03-05@22:00:00@A00DE810RB000000950580202005220114,2022-03-05@22:00:00@A00DE810RB000000950650202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000950760202005220114,2022-03-05@22:00:00@A00DE810RB000000949740202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000949780202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000949630202005220114,2022-03-05@22:00:00@A00DE8Q0RB000000899610202001170930,2022-03-05@22:00:00@A00DE8Q0RB000000902290202001211634,2022-03-05@22:00:00@A00DE8Q0RB000000896940202001160406,2022-03-05@22:00:00@A00DE8Q0RB000000898150202001170408,2022-03-05@22:00:00@A00DE8Q0RB000000898680202001170741,2022-03-05@22:00:00@A00DE8Q0RB000000899040202001170811,2022-03-05@22:00:00@A00DE8Q0RB000000898810202001170757,2022-03-05@22:00:00@A00DE8Q0RB000000898820202001170757,2022-03-05@22:00:00@A00DE8Q0RB000000898900202001170804,2022-03-05@22:00:00@A00DE8Q0RB000000898910202001170804,2022-03-05@22:00:00@A00DE8Q0RB000000899150202001170823,2022-03-05@22:00:00@A00DE8Q0RB000000899170202001170830,2022-03-05@22:00:00@A00DE8Q0RB000000899180202001170831,2022-03-05@22:00:00@A00DE810RB000001236880202011060511,2022-03-05@22:00:00@A00DE810RB000001236910202011060511,2022-03-05@22:00:00@A00DE810RB000001236920202011060511,2022-03-05@22:00:00@A00DE8Q0RB000001361120202103310255,2022-03-05@22:00:00@A00DE8Q0RB000001361160202103310255,2022-03-05@22:00:00@A00DE8Q0RB000001361170202103310255,2022-03-05@22:00:00@A00DE810RB000001343050202103100452,2022-03-05@22:00:00@A00DE810RB000001343070202103100452,2022-03-05@22:00:00@A00DE8Q0RB000001404070202105050652,2022-03-05@22:00:00@A00DE810RB000001404080202105050652,2022-03-05@22:00:00@A00DE8Q0RB000001404090202105050652,2022-03-05@22:00:00@A00DE8Q0RB000001404150202105050652,2022-03-05@22:00:00@A00DE8Q0RB000001404190202105050652,2022-03-05@22:00:00@A00DEY00RB000001234800202011060442,2022-03-05@22:00:00@A00DEY00RB000001234810202011060442,2022-03-05@22:00:00@A00DE8Q0RB000001192980202008141317,2022-03-05@22:00:00@A00DE940GC000000333070201903041013,2022-03-05@22:00:00@A00DEY00RB000000801520201906200909,2022-03-05@22:00:00@A00DE810GC000000218400201903040911,2022-03-05@22:00:00@A00DE910GC000000218930201903040911,2022-03-05@22:00:00@A00DE810GC000000260530201903040940,2022-03-05@22:00:00@A00DE940GC000000260510201903040940,2022-03-05@22:00:00@A00DE910GC000000288080201903040955,2022-03-05@22:00:00@A00DE810GC000000279270201903040950,2022-03-05@22:00:00@A00DE810GC000000296640201903040958,2022-03-05@22:00:00@A00DE940GC000000144700201903040455,2022-03-05@22:00:00@A00DE810GC000000151800201903040502,2022-03-05@22:00:00@A00DE910RB000000612320201905240710,2022-03-05@22:00:00@A00DE910GC000000254690201903040937,2022-03-05@22:00:00@A00DE810GC000000342830201903041017,2022-03-05@22:00:00@A00DE810GC000000229300201903040918,2022-03-05@22:00:00@A00DE910GC000000224740201903040917,2022-03-05@22:00:00@A00DE940GC000000162840201903040512,2022-03-05@22:00:00@A00DE940GC000000163720201903040522,2022-03-05@22:00:00@A00DE910GC000000186830201903040712,2022-03-05@22:00:00@A00DE810GC000000186840201903040712,2022-03-05@22:00:00@A00DE810GC000000152970201903040503,2022-03-05@22:00:00@A00DE940GC000000132420201903040449,2022-03-05@22:00:00@A00FR9Q0KT020211012060850703087285,2022-03-05@22:00:00@A00FR710KT020211115034836935407319,2022-03-05@22:00:00@A00FR410KT020211115034837092154319,2022-03-05@22:00:00@A00FR710GC000000320050201903041007,2022-03-05@22:00:00@A00FR410GC000000172820201903040526,2022-03-05@22:00:00@A00FR410GC000000265040201903040942,2022-03-05@22:00:00@A00FR710GC000000288280201903040955,2022-03-05@22:00:00@A00FR410GC000000288310201903040955,2022-03-05@22:00:00@A00FR410GC000000350670201903041019,2022-03-05@22:00:00@A00FR710RB000001235150202011060456,2022-03-05@22:00:00@A00FR9Q0RB000001204320202009032214,2022-03-05@22:00:00@A00FR9Q0RB000001361260202103310255,2022-03-05@22:00:00@A00FR9Q0RB000001404530202105050652,2022-03-05@22:00:00@A00FR410RB000001411090202105120254,2022-03-05@22:00:00@A00FR410RB000001342820202103100446,2022-03-05@22:00:00@A00FR410RB000001235630202011060456,2022-03-05@22:00:00@A00FR9Q0RB000000897270202001160617,2022-03-05@22:00:00@A00FR9Q0RB000000928530202003101352,2022-03-05@22:00:00@A00FR9Q0RB000000948720202005210955,2022-03-05@22:00:00@A00FR410RB000000948840202005210959,2022-03-05@22:00:00@A00FR410RB000000890020201911251858,2022-03-05@22:00:00@A00FR410RB000000876070201911070759,2022-03-05@22:00:00@A00FR410RB000000876200201911070818,2022-03-05@22:00:00@A00FR410RB000000872950201911061022,2022-03-05@23:00:00@A00GB840KT020211025061807954467298,2022-03-05@23:00:00@A00GB7Q0KT020211025061811885721298,2022-03-05@23:00:00@A00GB7Q0RB000001404860202105050652,2022-03-05@23:00:00@A00GBX10RB000000878640201911102053,2022-03-05@22:00:00@A00ITL40KT020211025061817490563298,2022-03-05@22:00:00@A00ITL40RB000000993850202007290904,2022-03-05@22:00:00@A00ITNR0RB000000993510202007230615,2022-03-05@22:00:00@A00NLWQ0KT020211025061818590174298,2022-03-05@22:00:00@A00NLWQ0KT020211115034839695502319,2022-03-05@22:00:00@A00NLWQ0KT020211115034840059087319,2022-03-05@22:00:00@A00NLWQ0RB000001235240202011060456,2022-03-05@22:00:00@A00NLQ10RB000001205470202009082124

		count: 
			137
			76
			70
			78
			72
			61
			79
			64
			52
			34

		# python manual_cmp_webshots.py -c A00 -i 2022-03-05@22:00:00@A00FR410RB000000876200201911070818,2022-03-05@22:00:00@A00DE8Q0RB000000898810202001170757,2022-03-05@22:00:00@A00DE8Q0RB000000899180202001170831,2022-03-05@22:00:00@A00DE810KT020211012064139181032285,2022-03-05@22:00:00@A00DE8Q0RB000000899170202001170830,2022-03-05@22:00:00@A00DE8Q0KT020211012064139128225285,2022-03-05@22:00:00@A00DE810RB000000950240202005220114,2022-03-05@22:00:00@A00DE810KT020211012064146083288285,2022-03-05@22:00:00@A00DE810RB000000950650202005220114,2022-03-05@22:00:00@A00DE810KT020211012064148687786285,2022-03-05@22:00:00@A00DE810RB000000872490201911060945,2022-03-05@22:00:00@A00DE810RB000001236910202011060511,2022-03-05@22:00:00@A00DE8Q0RB000000899040202001170811,2022-03-05@22:00:00@A00DE810RB000001343070202103100452,2022-03-05@22:00:00@A00DE810RB000001236880202011060511,2022-03-05@22:00:00@A00DE8Q0RB000000950760202005220114,2022-03-05@22:00:00@A00DE8Q0KT020220302122951455311061,2022-03-05@22:00:00@A00NLWQ0RB000001235240202011060456,2022-03-05@22:00:00@A00DE940GC000000132420201903040449,2022-03-05@22:00:00@A00DE8Q0RB000000898820202001170757,2022-03-05@22:00:00@A00DE810KT020220302122952584668061,2022-03-05@22:00:00@A00DE810GC000000229300201903040918,2022-03-05@22:00:00@A00DE8Q0KT020220302122957315808061,2022-03-05@22:00:00@A00DE8Q0RB000000898150202001170408,2022-03-05@22:00:00@A00DE8Q0KT020211115034837583060319,2022-03-05@22:00:00@A00DE810GC000000342830201903041017,2022-03-05@22:00:00@A00DE810KT020211115034838247849319,2022-03-05@22:00:00@A00DE940GC000000162840201903040512,2022-03-05@22:00:00@A00DE810GC000000296640201903040958,2022-03-05@22:00:00@A00NLWQ0KT020211115034839695502319,2022-03-05@22:00:00@A00DE810GC000000218400201903040911,2022-03-05@22:00:00@A00DE810RB000000873450201911061131,2022-03-05@22:00:00@A00DE810KT020211012064144527127285

		count: 
			5

		# done udpate validator, done react

	(R D-1) 3879	2022-03-05	New CMS	K00	all	alza.cz	
		'K00CZ3607U020211115112916888564319',
		'K00CZ360CG000001486100202107130335',
		'K00CZ360CG000001486010202107130335'

		spider issue -1 values		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.alza.cz'	AND item_id in (	'K00CZ3607U020211115112916888564319',
			# 'K00CZ360CG000001486100202107130335',
			# 'K00CZ360CG000001486010202107130335')

			# no issue
				# done react, done sheets

	(R D-1) 3880	2022-03-05	New CMS	K00	all	power.dk	
		'K00DKX203F000001554340202108180340',
		'K00DKX20CG000001487760202107130336'

		spider issue -1 values		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-04' AND website = 'www.power.dk'	AND item_id in ('K00DKX203F000001554340202108180340',
			# 'K00DKX20CG000001487760202107130336')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@K00DKX203F000001554340202108180340,2022-03-05@22:00:00@K00DKX20CG000001487760202107130336

			# successful rerun, valid -1 value
				https://prnt.sc/zlhMVgUdFlix

			# done react, done sheets

	(R D-1) 3887	2022-03-05	New CMS	F00	in_stock	krefel.be/nl	
		'F00BEIS040000001658090202111152217',
		'F00BEIS05R020220103055429666279003',
		'F00BEIS040000001658100202111152217',
		'F00BEIS040000001623770202110250510',
		'F00BEIS05R020220214072839787131045',
		'F00BEIS040000001626060202110250511',
		'F00BEIS0EL000001625320202110250511',
		'F00BEIS0EL000001626480202110250511',
		'F00BEIS0EL000001625500202110250511',
		'F00BEIS0JA000001624230202110250510',
		'F00BEIS0JA000001626340202110250511',
		'F00BEIS0JA000001624570202110250510',
		'F00BEIS0JA000001624510202110250510',
		'F00BEIS0JA000001624470202110250510',
		'F00BEIS0JA000001624940202110250510',
		'F00BEIS0JA000001626220202110250511'

		Spider issue in Availability	
		Yes	Crestine

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-03-04' AND website = 'www.krefel.be/nl' AND item_id in ('F00BEIS040000001658090202111152217',
			# 'F00BEIS05R020220103055429666279003',
			# 'F00BEIS040000001658100202111152217',
			# 'F00BEIS040000001623770202110250510',
			# 'F00BEIS05R020220214072839787131045',
			# 'F00BEIS040000001626060202110250511',
			# 'F00BEIS0EL000001625320202110250511',
			# 'F00BEIS0EL000001626480202110250511',
			# 'F00BEIS0EL000001625500202110250511',
			# 'F00BEIS0JA000001624230202110250510',
			# 'F00BEIS0JA000001626340202110250511',
			# 'F00BEIS0JA000001624570202110250510',
			# 'F00BEIS0JA000001624510202110250510',
			# 'F00BEIS0JA000001624470202110250510',
			# 'F00BEIS0JA000001624940202110250510',
			# 'F00BEIS0JA000001626220202110250511')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@F00BEIS040000001658090202111152217,2022-03-05@22:00:00@F00BEIS05R020220103055429666279003,2022-03-05@22:00:00@F00BEIS040000001658100202111152217,2022-03-05@22:00:00@F00BEIS040000001623770202110250510,2022-03-05@22:00:00@F00BEIS05R020220214072839787131045,2022-03-05@22:00:00@F00BEIS040000001626060202110250511,2022-03-05@22:00:00@F00BEIS0EL000001625320202110250511,2022-03-05@22:00:00@F00BEIS0EL000001626480202110250511,2022-03-05@22:00:00@F00BEIS0EL000001625500202110250511,2022-03-05@22:00:00@F00BEIS0JA000001624230202110250510,2022-03-05@22:00:00@F00BEIS0JA000001626340202110250511,2022-03-05@22:00:00@F00BEIS0JA000001624570202110250510,2022-03-05@22:00:00@F00BEIS0JA000001624510202110250510,2022-03-05@22:00:00@F00BEIS0JA000001624470202110250510,2022-03-05@22:00:00@F00BEIS0JA000001624940202110250510,2022-03-05@22:00:00@F00BEIS0JA000001626220202110250511

			# rerun successful
			# done react, done sheets

	(R D-2) 3888	2022-03-05	New CMS	200	all	fnac.com/3P	
		'200FRDU0NC000001381010202104140811',
		'200FRDU070000001381360202104140811',
		'200FRDU080000001381660202104141007',
		'200FRDU080000001381690202104141007',
		'200FRDU070000001398120202105040259'
		failed to scrape 3p / Invalid deadlinks		
		Yes	Jeremy

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-05' AND website = 'www.fnac.com/3P'	AND item_id in ('200FRDU0NC000001381010202104140811',
			# '200FRDU070000001381360202104140811',
			# '200FRDU080000001381660202104141007',
			# '200FRDU080000001381690202104141007',
			# '200FRDU070000001398120202105040259')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@200FRDU0NC000001381010202104140811,2022-03-05@22:00:00@200FRDU070000001381360202104140811,2022-03-05@22:00:00@200FRDU080000001381660202104141007,2022-03-05@22:00:00@200FRDU080000001381690202104141007,2022-03-05@22:00:00@200FRDU070000001398120202105040259

			# python rerun_cmp.py -s CRAWL_FINISHED -l2022-03-05@22:00:00@200FRDU080000001381690202104141007,2022-03-05@22:00:00@200FRDU070000001398120202105040259

			# python rerun_cmp.py -s CRAWL_FINISHED -l2022-03-05@22:00:00@200FRDU080000001381690202104141007

			# multiple reruns successful
			# done react, done sheets

	-----

	(R D-1) 2504	2022-03-05	New CMS	110	all	www.bol.com	
		nintendo switch console	
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/XSa5iggFxB93	
		YEs	flor

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'nintendo switch console') and 
				var2.[date] = '2022-03-05' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@110@8ef2270b-79dd-4640-899e-4dcb6462050f@UAjszpOqEei-eQANOiOHiA
			# rerun successful
			# done react, done sheets

	(R D-1) 2505	2022-03-05	New CMS	110	all	Duplicate product website and product url	www.bol.com	
		spelcomputer	
		https://prnt.sc/Kckk4ljcHlCP	
		Yes	flor

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
			where var2.website = 'www.bol.com' and 
			var2.keyword in (N'spelcomputer') and 
			var2.[date] = '2022-03-05' 
			GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@110@e772bc6b-f949-4561-a365-1de2e82b0bef@UAjszpOqEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheets

	(F D-2) 2507	2022-03-05	New CMS	U00	product_url	www.mercadolivre.com.br	
		'aparelho de game',
		'aparelho de video game',
		'console',
		'console nintendo switch',
		'console switch',
		'controle game sem fio',
		'controle joy con',
		'controle nintendo switch',
		'controle sem fio nintendo switch',
		'controle switch',
		'joy con',
		'mini game portatil',
		'mini video game',
		'nintendo switch',
		'pro controller'

		Wrong Product_Url	
		https://prnt.sc/tHBB3F5jrSSF	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
				AND website = 'www.mercadolivre.com.br' 
				AND keyword in (N'aparelho de game',
						'aparelho de video game',
						'console',
						'console nintendo switch',
						'console switch',
						'controle game sem fio',
						'controle joy con',
						'controle nintendo switch',
						'controle sem fio nintendo switch',
						'controle switch',
						'joy con',
						'mini game portatil',
						'mini video game',
						'nintendo switch',
						'pro controller')
				GROUP BY keyword, ritem_id, country, SOURCE 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@b152a464-cf02-4050-8338-93e13eb48a5b@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@98ce312a-6d9f-4cfe-91f5-ee98234a99d6@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@76028b0c-acb7-4262-81c8-5e4ffacf297f@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@qPUYcLZqEeidIgANOiZi-g,2022-03-05@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@qPUYcLZqEeidIgANOiZi-g

			# multiple reruns failed
			# check local: 
			# done fix
			# done rebuild
			# successful rerun
			# done react, done sheets

	(R D-1) 2508	2022-03-05	New CMS	U00	all	www.magazineluiza.com.br	
		pro controller	
		Data Count Mismatch 15 (Fetched Data is Greater than expected)	
		https://prnt.sc/PLudqsKE6rBY	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
			AND website = 'www.magazineluiza.com.br' 
			AND keyword in (N'pro controller')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@G3u21LZqEeidIgANOiZi-g

			# successful rerun
			# done rect, done sheets

	(R D-1) 2509	2022-03-05	New CMS	110	all	www.dreamland.be/fr
		Nintendo Switch Lite	
		Data Count Mismatch 1(Fetched Data is Lesser than expected)	
		https://prnt.sc/xnnI480Qe0Oq	
		yes	flor

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
				AND website = 'www.dreamland.be/fr' 
				AND keyword in (N'Nintendo Switch Lite')
				GROUP BY keyword, ritem_id, country, source
			

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@110@36ca1602-96f4-4497-bfff-a93bd2deb067@391fc560-aefc-4c88-8c9a-7c98201355ce

			# rerun successful
			# done react, done sheets

	(F D-2) 2511	2022-03-05	New CMS	X00	product_url	www.mercadolibre.com.mx
		'Accesorios para Nintendo Switch',
		'Consola',
		'Consola Nintendo Lite',
		'Consola Nintendo Switch',
		'Consola Nintendo Switch Lite',
		'Consola Switch',
		'Consola Switch Lite',
		'Joy Con',
		'Nintendo Joy Con',
		'Nintendo Pro Controller',
		'Nintendo Switch',
		'Nintendo Switch Lite',
		'Nintendo Switch Pro Controller',
		'Switch Nintendo',
		'Switch Nintendo Lite',
		'Switch OLED',
		'Nintendo switch OLED'

		wrong URL fetched	
		https://prnt.sc/c65MuqumR6sx	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
				AND website = 'www.mercadolibre.com.mx' 
				AND keyword in (N'Accesorios para Nintendo Switch',
						'Consola',
						'Consola Nintendo Lite',
						'Consola Nintendo Switch',
						'Consola Nintendo Switch Lite',
						'Consola Switch',
						'Consola Switch Lite',
						'Joy Con',
						'Nintendo Joy Con',
						'Nintendo Pro Controller',
						'Nintendo Switch',
						'Nintendo Switch Lite',
						'Nintendo Switch Pro Controller',
						'Switch Nintendo',
						'Switch Nintendo Lite',
						'Switch OLED',
						'Nintendo switch OLED')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-05@05:00:00@X00@8304e3b7-b53a-44d3-9082-dfc4c6170fbc@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@79cd174a-b94b-405e-97a5-e36568e39106@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@49d3844d-b750-462b-8951-06dfb60fcb21@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@3d9c0a46-e754-4fda-87ae-8675bf610e1d@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@4bc89052-7d33-4a6b-bda4-23c1ae037d9d@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@6a0f6d9b-7fd8-47f0-b384-dfafbf60ff5c@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@f47e7d8d-9235-4033-87fb-00b7466e702f@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@86c5be64-e098-4678-beda-fd4d8895e764@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@c3b588ab-2bbc-4c43-831d-1f84455db1d9@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@795f8f41-ea92-42dc-83ae-34f175697537@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@36ca1602-96f4-4497-bfff-a93bd2deb067@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@687f71ee-e5e7-427b-8ccd-6013c0d07916@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@13b8d32f-378a-4430-934d-966f5f30aee2@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@22a3e243-e46a-4fee-bef4-14cf3fe7cdaa@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@674037ed-5a15-4c2f-a51b-8e4cd65d1c09@742a6974-cd40-41a4-bad0-d8362961ad65,2022-03-05@05:00:00@X00@a2920dd5-d8f3-4050-94a7-a3cf19c28c9a@742a6974-cd40-41a4-bad0-d8362961ad65

		# fix local: same issue with 2507
		# done fix
		# done rebuild
		# done rerun
		# successful rerun
		# done react, done sheets

	(R D-1) 2512	2022-03-05	New CMS	X00	all	www.sears.com.mx	
		Consola Nintendo Switch	
		1 Day Auto Copy Over	
		https://prnt.sc/MWGDbLI1CA5Z	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
				AND website = 'www.sears.com.mx' 
				AND keyword in (N'Consola Nintendo Switch')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-05@05:00:00@X00@3d9c0a46-e754-4fda-87ae-8675bf610e1d@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2513	2022-03-05	New CMS	X00	all	www.sears.com.mx	
		Switch OLED	
		Invalid Deadlink	
		https://prnt.sc/5flJdOC5tAk3	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-05' 
				AND website = 'www.sears.com.mx' 
				AND keyword in (N'Switch OLED')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-05@05:00:00@X00@a2920dd5-d8f3-4050-94a7-a3cf19c28c9a@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# multiple reruns successful
			# done react, done sheets

	-----
	(R D-2) 852	2022-03-05	New CMS	200	all	www.re-store.ru	
		/ 'headphones bluetooth on ear', - 25
		/ 'headphones wired in ear', - 8
		'speakers voice' - 3

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)
		Data Count Mismatch (Fetched Data is Lesser than expected)
		Data Count Mismatch (Fetched Data is Lesser than expected)	

		https://prnt.sc/c3KacH2wfH8Z
		https://prnt.sc/dl5TbZH8UYPg
		https://prnt.sc/EjJzlOoHWdwi	

		Yes	kurt

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-05' 
			AND website = 'www.re-store.ru' 
			AND category in (N'headphones bluetooth on ear',
						'headphones wired in ear',
						'speakers voice')
			GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-05@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-05@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-03-05@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-05@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# multiple reruns successful
			# done react, done sheets

	0 (R D-1) 853	2022-03-05	New CMS	110	all	www.nl.fnac.be	
		Hardware	
			https://www.nl.fnac.be/n470487/Switch/Switch-consoles#bl=ACSwitchARBO
		Auto Copy Over for 3 Days	
		https://prnt.sc/6XHTs5Hto7Uu	
		Yes	richelle

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-05' 
			AND website = 'www.fnac.be/nl' 
			AND category in (N'Hardware')
			GROUP BY category, litem_id, country, category_url, source	

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-05@22:00:00@110@9B0@d438865e-7705-45d2-ba93-16df3e3cb3e4
			# multiple reruns failed
				# check local: no issue: setting random timeout

			# for rebuild
			# reacted and update sheets
			# For Trello: still blocked
			# done react, done sheets

	(R D-1) 854	2022-03-05	New CMS	200	all	www.mediamarkt.de	
		Speakers Voice
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/Re9exUD-ALtQ	
		Yes	Rhunick

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-05' 
				AND website = 'www.mediamarkt.de' 
				AND category in (N'Speakers Voice')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-05@22:00:00@200@810@88735ab3-25d4-44b4-8260-1bbd6576a511
			# rerun successful
			# done react, done sheets

	110@Nintendo Benelux@12:00NN@12:25AM@Prod2
	U00@Nintendo Brazil@12:00NN@12:25AM@Prod2
	X00@Nintendo Mexico@12:00NN@12:25AM@Prod2

	Stats:
		Rerun: 16 (19) = 19/16 = 1.19
		Webshots: 1
		Fix: 2[2]

03 06 22
	0 (R D-1)Trello 1
		Issue: Auto Copy Over for 3 Days
		Date: March 5, 2022
		Retailer: www.fnac.be/nl
		Category: hardware
		Listing UUID:
		d438865e-7705-45d2-ba93-16df3e3cb3e4

		Screenshot:
		https://prnt.sc/6XHTs5Hto7Uu

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-05' 
				AND website = 'www.fnac.be/nl' 
				AND category in (N'hardware')
				GROUP BY category, litem_id, country, category_url, SOURCE	

	-----

	(R D-1) 3889	2022-03-06	New CMS	K00	all	mvideo.ru	
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449540202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP804K000001527170202108020954',
		'K00RUP803F000001449440202106150452',
		'K00RUP803F000001449400202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001245150202011170413',
		'K00RUP80CG000001245120202011170413',
		'K00RUP80CG000001310920202101250600',
		'K00RUP807U020220214043148528186045',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000001245170202011170413',
		'K00RUP80CG000001310980202101250600',
		'K00RUP807U020220214043148675826045',
		'K00RUP80IO000001449640202106150452',
		'K00RUP80YO000001449670202106150452',
		'K00RUP805K000001527540202108020954',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449690202106150452'
		wrong price and availability		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-06' AND website = 'www.mvideo.ru'	AND item_id in ('K00RUP807O000001449510202106150452',
			'K00RUP80AD000001449540202106150452',
			'K00RUP80AD000001527640202108020954',
			'K00RUP804K000001527170202108020954',
			'K00RUP803F000001449440202106150452',
			'K00RUP803F000001449400202106150452',
			'K00RUP80CG000000980370202006250902',
			'K00RUP80CG000001245150202011170413',
			'K00RUP80CG000001245120202011170413',
			'K00RUP80CG000001310920202101250600',
			'K00RUP807U020220214043148528186045',
			'K00RUP80CG000000980450202006250902',
			'K00RUP80CG000001245170202011170413',
			'K00RUP80CG000001310980202101250600',
			'K00RUP807U020220214043148675826045',
			'K00RUP80IO000001449640202106150452',
			'K00RUP80YO000001449670202106150452',
			'K00RUP805K000001527540202108020954',
			'K00RUP80YO000001449660202106150452',
			'K00RUP80YO000001449690202106150452')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@21:00:00@K00RUP807O000001449510202106150452,2022-03-06@21:00:00@K00RUP80AD000001449540202106150452,2022-03-06@21:00:00@K00RUP80AD000001527640202108020954,2022-03-06@21:00:00@K00RUP804K000001527170202108020954,2022-03-06@21:00:00@K00RUP803F000001449440202106150452,2022-03-06@21:00:00@K00RUP803F000001449400202106150452,2022-03-06@21:00:00@K00RUP80CG000000980370202006250902,2022-03-06@21:00:00@K00RUP80CG000001245150202011170413,2022-03-06@21:00:00@K00RUP80CG000001245120202011170413,2022-03-06@21:00:00@K00RUP80CG000001310920202101250600,2022-03-06@21:00:00@K00RUP807U020220214043148528186045,2022-03-06@21:00:00@K00RUP80CG000000980450202006250902,2022-03-06@21:00:00@K00RUP80CG000001245170202011170413,2022-03-06@21:00:00@K00RUP80CG000001310980202101250600,2022-03-06@21:00:00@K00RUP807U020220214043148675826045,2022-03-06@21:00:00@K00RUP80IO000001449640202106150452,2022-03-06@21:00:00@K00RUP80YO000001449670202106150452,2022-03-06@21:00:00@K00RUP805K000001527540202108020954,2022-03-06@21:00:00@K00RUP80YO000001449660202106150452,2022-03-06@21:00:00@K00RUP80YO000001449690202106150452

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@21:00:00@K00RUP80CG000001245170202011170413

			# multiple reruns successful
			# done react, done sheets
	
	(R D-1) 3890	2022-03-06	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP804K000001528820202108020955',
		'K00RUP804K000001527200202108020954',
		'K00RUP803F000001528840202108020955',
		'K00RUP804K000001449120202106150452',
		'K00RUP803F000001449180202106150452',
		'K00RUP803F000001527440202108020954',
		'K00RUP803F000001449360202106150452',
		'K00RUP803F000001527370202108020954',
		'K00RUP803F000001527310202108020954',
		'K00RUP80CG000000980360202006250902',
		'K00RUP80CG000001311070202101250600',
		'K00RUP80CG000001524980202108020807'
		wrong ratings
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-06' AND website = 'www.mvideo.ru'	AND item_id in ('K00RUP804K000001528820202108020955',
			'K00RUP804K000001527200202108020954',
			'K00RUP803F000001528840202108020955',
			'K00RUP804K000001449120202106150452',
			'K00RUP803F000001449180202106150452',
			'K00RUP803F000001527440202108020954',
			'K00RUP803F000001449360202106150452',
			'K00RUP803F000001527370202108020954',
			'K00RUP803F000001527310202108020954',
			'K00RUP80CG000000980360202006250902',
			'K00RUP80CG000001311070202101250600',
			'K00RUP80CG000001524980202108020807')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@21:00:00@K00RUP804K000001528820202108020955,2022-03-06@21:00:00@K00RUP804K000001527200202108020954,2022-03-06@21:00:00@K00RUP803F000001528840202108020955,2022-03-06@21:00:00@K00RUP804K000001449120202106150452,2022-03-06@21:00:00@K00RUP803F000001449180202106150452,2022-03-06@21:00:00@K00RUP803F000001527440202108020954,2022-03-06@21:00:00@K00RUP803F000001449360202106150452,2022-03-06@21:00:00@K00RUP803F000001527370202108020954,2022-03-06@21:00:00@K00RUP803F000001527310202108020954,2022-03-06@21:00:00@K00RUP80CG000000980360202006250902,2022-03-06@21:00:00@K00RUP80CG000001311070202101250600,2022-03-06@21:00:00@K00RUP80CG000001524980202108020807

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 3895	2022-03-06	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000557590201904220945',
		'200ITGF080000000557310201904220937'
		failed to scrape 3p / Invalid deadlinks		
		Yes	jEREMY

		# solution
			# SELECT * FROM view_all_productdata where date='2022-01-23' AND website = 'www.amazon.it/3P'	AND item_id in ('200ITGF070000000557590201904220945',
			'200ITGF080000000557310201904220937')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@200ITGF070000000557590201904220945,2022-03-06@22:00:00@200ITGF080000000557310201904220937
			# successful rerun
			# done react, done sheets

	(R D-1) 3897	2022-03-06	New CMS	P00	all	alza.cz	
		P00CZ3601L000001429810202106010821	
		Spider Issue		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-03-06' AND WEBSITE = 'www.alza.cz' AND item_id in ('P00CZ3601L000001429810202106010821')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@P00CZ3601L000001429810202106010821
			# successful rerun
			# done react, done sheets

	(R D-1) 3898	2022-03-06	New CMS	F00	all	krefel.be/nl	
		'F00BEIS0EL000001626730202110250511',
		'F00BEIS0WJ000001626540202110250511',
		'F00BEIS0WJ000001626460202110250511',
		'F00BEIS0WJ000001641600202111040958',
		'F00BEIS040000001625900202110250511',
		'F00BEIS040000001626140202110250511',
		'F00BEIS040000001625620202110250511',
		'F00BEIS05R020220214072840901955045',
		'F00BEIS040000001626060202110250511',
		'F00BEIS0EL000001623940202110250510',
		'F00BEIS0EL000001626660202110250511',
		'F00BEIS0EL000001625410202110250511',
		'F00BEIS0WJ000001641700202111040958',
		'F00BEIS0JA000001624170202110250510',
		'F00BEIS0JA000001624430202110250510',
		'F00BEIS0JA000001625000202110250510',
		'F00BEIS0JA000001624130202110250510',
		'F00BEIS0JA000001626260202110250511'
		spider issue -1 values		
		Yes	Glenn

		# solution
			# SELECT * FROM view_all_productdata where date='2022-01-23' AND website = 'www.krefel.be/nl'	AND item_id in ('F00BEIS0EL000001626730202110250511',
			'F00BEIS0WJ000001626540202110250511',
			'F00BEIS0WJ000001626460202110250511',
			'F00BEIS0WJ000001641600202111040958',
			'F00BEIS040000001625900202110250511',
			'F00BEIS040000001626140202110250511',
			'F00BEIS040000001625620202110250511',
			'F00BEIS05R020220214072840901955045',
			'F00BEIS040000001626060202110250511',
			'F00BEIS0EL000001623940202110250510',
			'F00BEIS0EL000001626660202110250511',
			'F00BEIS0EL000001625410202110250511',
			'F00BEIS0WJ000001641700202111040958',
			'F00BEIS0JA000001624170202110250510',
			'F00BEIS0JA000001624430202110250510',
			'F00BEIS0JA000001625000202110250510',
			'F00BEIS0JA000001624130202110250510',
			'F00BEIS0JA000001626260202110250511')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@F00BEIS0EL000001626730202110250511,2022-03-06@22:00:00@F00BEIS0WJ000001626540202110250511,2022-03-06@22:00:00@F00BEIS0WJ000001626460202110250511,2022-03-06@22:00:00@F00BEIS0WJ000001641600202111040958,2022-03-06@22:00:00@F00BEIS040000001625900202110250511,2022-03-06@22:00:00@F00BEIS040000001626140202110250511,2022-03-06@22:00:00@F00BEIS040000001625620202110250511,2022-03-06@22:00:00@F00BEIS05R020220214072840901955045,2022-03-06@22:00:00@F00BEIS040000001626060202110250511,2022-03-06@22:00:00@F00BEIS0EL000001623940202110250510,2022-03-06@22:00:00@F00BEIS0EL000001626660202110250511,2022-03-06@22:00:00@F00BEIS0EL000001625410202110250511,2022-03-06@22:00:00@F00BEIS0WJ000001641700202111040958,2022-03-06@22:00:00@F00BEIS0JA000001624170202110250510,2022-03-06@22:00:00@F00BEIS0JA000001624430202110250510,2022-03-06@22:00:00@F00BEIS0JA000001625000202110250510,2022-03-06@22:00:00@F00BEIS0JA000001624130202110250510,2022-03-06@22:00:00@F00BEIS0JA000001626260202110250511

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@F00BEIS0EL000001626730202110250511

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@F00BEIS040000001626140202110250511
			# successful rerun
			# done react, done sheets

	(F D-1) 3899	2022-03-06	New CMS	H10	in_stock	homedepot.com	
		H10USAX0Q8220220125025736863729025	
		Spider issue in Availability		
		Yes	Keeshia

		# solution
			# select * from view_all_productdata where date='2022-03-06' and website='homedepot.com' AND item_id in ('H10USAX0Q8220220125025736863729025')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-06@04:00:00@H10USAX0Q8220220125025736863729025

			# fixed et
			# for rebuild
			# successful rerun
			# done react, done sheets

	-----
	(R D-1) 2514	2022-03-06	New CMS	200	all	www.bol.com	
		Bluetooth hoofdtelefoon
		hoofdtelefoon	
		Duplicate product website and product url	

		https://prnt.sc/eDncCH36MaTf
		https://prnt.sc/7jqPcDjvtH3n
		https://prnt.sc/chPZPBT24qxB
		https://prnt.sc/sPNdgwWaQVJY	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'hoofdtelefoon') and 
				var2.[date] = '2022-03-06' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source


			# 07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA
			# 94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA,2022-03-06@22:00:00@200@94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA

			# successful rerun
			# done react, done sheets

	(R D-1) 2517	2022-03-06	New CMS	200	all	www.darty.com	
		casque bluetooth reducteur de bruit	- 14
		Data Count Mismatch 10 (Fetched Data is Less than expected)	
		https://prnt.sc/5CkFPGg_GhsI	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.darty.com' and 
				var2.keyword in (N'casque bluetooth reducteur de bruit') and 
				var2.[date] = '2022-03-06' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source
		
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-06@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@ds73aApHEem-gAANOiOHiA
			# successful rerun
			# done react, done sheets

	-----
	(R D-1) 858	2022-03-06	New CMS	100	all	www.dustinhome.se	
		Wearables	
		1 Day Auto Copy Over Data	
		https://prnt.sc/YwoWOoLePvn-	
		Yes	Rhunick

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-06@22:00:00@100@L10@f227664b-0f94-480c-882c-31e0e2412087
			# successful rerun
			# done react, done sheets


	(R D-1) 860	2022-03-06	New CMS	100	all	www.seasea.se	
		Ekolod/Fishfinder	
		1 Day Auto Copy Over Data	
		https://prnt.sc/qBJdV4aG0EGE	
		Yes	Rhunick

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-06' 
				AND website = 'www.seasea.se' 
				AND category in (N'Ekolod/Fishfinder')
				GROUP BY category, litem_id, country, category_url, source	
							

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-06@22:00:00@100@CH0@d5a869df-f736-49b0-9ab0-557b02052a32
			# successful rerun
			# done react, done sheets

	(R D-1) 861	2022-03-06	New CMS	100	all	www.mediamarkt.se	
		Wearables	
		1 Day Auto Copy Over Data	
		https://prnt.sc/I3RuuBmk8_ht	
		Yes	Rhunick

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-06' 
				AND website = 'www.mediamarkt.se' 
				AND category in (N'Wearables')
				GROUP BY category, litem_id, country, category_url, source		

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-06@22:00:00@100@A00@63b286c4-7c51-4647-80c0-b2fee0235067
			# successful rerun
			# done react, done sheets

	Stats:
		Rerun: 11
		Fix: 1[1]

03 09 22
	(R D-1) 3939	2022-03-09	New CMS	K00	description	www.mediamarkt.de	
		K00DE8107O000001433180202106020354	
		incorrect description	
		https://prnt.sc/oXmSi5LnrBOJ	
		Yes	Charlemagne

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-09' AND website = 'www.mediamarkt.de'	AND item_id in ('K00DE8107O000001433180202106020354')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00DE8107O000001433180202106020354
			# rerun successful
			# done react, done sheets

	-----
	(R D-1) 2563	2022-03-09	New CMS	K00	all	www.box.co.uk	
		Gaming Headset
		Gaming Mouse	

		Invalid Deadlink	
		https://prnt.sc/P6j2mjGd6wO7
		https://prnt.sc/84wX54LlJhzb	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-09' 
				AND website = 'www.box.co.uk' 
				AND keyword in (N'Gaming Headset','Gaming Mouse')
				GROUP BY keyword, ritem_id, country, SOURCE

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-09@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# rerun successful
			# done react, done sheets

	(R D-2) 2564	2022-03-09	New CMS	K00	all	www.box.co.uk	
		'Streaming Microphone',
		'best headphones',
		'anc headphones',
		'Xbox One X Controller',
		'Ergonomic Chair',
		'Gaming Chair',
		'Gaming Mousemat',
		'Mousemat',
		'2.1 Speakers',
		'Gaming Speakers',
		'Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Gaming Notebook',
		'Notebook',
		'Gaming Laptop',
		'Laptop',
		'mechanical keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse'

		Auto Copy Over for 1 day	

		https://prnt.sc/QrWsy6aBYuBy
		https://prnt.sc/q1-Mnm048BKB
		https://prnt.sc/9EEhMJJa_YiU
		https://prnt.sc/1Bg1cYK0xTfX
		https://prnt.sc/X_imE6joU0y6
		https://prnt.sc/ZKRL0IU-lC0x
		https://prnt.sc/0-QZYtehyOQ6
		https://prnt.sc/3E-x8cZ7OWWn
		https://prnt.sc/AeZhKP7gZAmd
		https://prnt.sc/sCpX_57DlX5s
		https://prnt.sc/WDYIRREBXzpo
		https://prnt.sc/RlhbmkQxhmO1
		https://prnt.sc/Jpp-qzvqiy6k
		https://prnt.sc/-yDG73cJdiQW
		https://prnt.sc/hfP_aWcsomOl
		https://prnt.sc/WHWR4J0-BsSo
		https://prnt.sc/muWg-VsulJ4d
		https://prnt.sc/Lht2p6oAphlw
		https://prnt.sc/m8zrN4EtERTY
		https://prnt.sc/YJY9End23EfW
		https://prnt.sc/kgTjqrhPYSoT
		https://prnt.sc/r7EHSoZahFzX
		https://prnt.sc/kVzEpHSaLvPY
		https://prnt.sc/DpbvyMekFWW-
		https://prnt.sc/RfWHrj-WYebh
		https://prnt.sc/3LaEwk-qlhCc
		https://prnt.sc/518z8XwFCKoq	

		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-09' 
				AND website = 'www.box.co.uk' 
				AND keyword in (N'Streaming Microphone',
						'best headphones',
						'anc headphones',
						'Xbox One X Controller',
						'Ergonomic Chair',
						'Gaming Chair',
						'Gaming Mousemat',
						'Mousemat',
						'2.1 Speakers',
						'Gaming Speakers',
						'Speakers',
						'Keypad',
						'PC Keypad',
						'Gaming Keypad',
						'Gaming Notebook',
						'Notebook',
						'Gaming Laptop',
						'Laptop',
						'mechanical keyboard',
						'Keyboard',
						'PC Keyboard',
						'Gaming Keyboard',
						'Wireless Headset',
						'PC Headset',
						'Wireless Mouse',
						'Mouse',
						'PC Mouse')
				GROUP BY keyword, ritem_id, country, SOURCE

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-09@23:00:00@K00@f54708f2-399f-43af-bfb8-20d00c1f294b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@ef6e28af-4210-4946-b21a-daeaa5c4ed38@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@d29b8e26-ffcc-4bda-a5ec-5681de160607@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@d6871bcf-528c-4a32-a19d-c504066e1013@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@b73ac43d-b22d-4fa8-9dd4-58e266c9e7f1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@49e9ace7-ebd5-4861-8aaf-ea221ef92b83@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@c34de3e3-db49-4227-89a5-a811ffbfbb5e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@a5042d62-ba5e-4b40-ac02-fada0b36b92c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@88d127ad-cc16-4b9e-9746-7f2fda21d7ed@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@a88e5833-304b-411f-b354-3e02fbd9486a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@4ece2ecd-1087-42f9-a425-0435928a0b47@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@287f5d68-29fb-4230-8e11-b4ec207c0b9a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@e88ea211-dbf7-43af-afbd-c122b70031b6@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@bae74ad9-7e9d-4ed4-a99c-497ef7383e1e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@d57f49ea-2101-46e4-9b69-85c55112ad1b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@05d84a59-bd95-4bf2-903a-ef0f90c004ff@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@b20c5fbe-7824-4b8e-b054-857117d42119@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@b82d2b40-abf7-49b9-9a73-680f0ffbe1e1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@e89d5de5-2856-4a30-ad6f-bc60e531eb6e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-09@23:00:00@K00@cbcce8a1-c80c-4b89-867e-b1b61693dc4e@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# rerun successful
			# done react, done sheets

	(R D-1) 2565	2022-03-09	New CMS	K00	all	www.alza.cz
		Gamepady ps 4	
		Data Count Mismatch 1(Fetched Data is Greater than expected)	
		https://prnt.sc/vXIE038lbVZw	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-09' 
				AND website = 'www.alza.cz' 
				AND keyword in (N'Gamepady ps 4')
				GROUP BY keyword, ritem_id, country, SOURCE

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00@65e0f19c-7cbd-4b83-ba8b-9d57d698117b@1cAhckXZEeeMawANOiZi-g
			# rerun successful
			# done react, done sheets

	(R D-1) 2566	2022-03-09	New CMS	K00	all	ebuyer.com	
		'Gaming Speakers',
		'headphones'
		1 Day Auto Copy Over	
		https://prnt.sc/Yie-XLnCKTGJ
		https://prnt.sc/ZcCcoR7X4xaL	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-09' 
				AND website = 'www.ebuyer.com' 
				AND keyword in (N'Gaming Speakers',
						'headphones')
				GROUP BY keyword, ritem_id, country, SOURCE
						

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-09@23:00:00@K00@88d127ad-cc16-4b9e-9746-7f2fda21d7ed@qcWnsMefEei-eQANOiOHiA,2022-03-09@23:00:00@K00@tTCFMzB2EeeFsgANOiOHiA@qcWnsMefEei-eQANOiOHiA
			# rerun successful
			# done react, done sheets

	(R D-3) 2567	2022-03-09	New CMS	K00	all	otto.de	
		'Gaming Stuhl',
		'TASTATUR',
			Logitech »MX Keys Advanced« Tastatur
			https://prnt.sc/aIq8jYFHvmES

			Logitech »Combo Touch iPad Pro 11 Zoll (1., 2., 3. Gen - 2018, 2020, 2021) Keyboard Case - Abnehmbare Tastatur-Case - Click-Anywhere Trackpad, Smart Connector - Grau, QWERTZ« iPad-Tastatur

			https://prnt.sc/QcHkC54x2J0J
		'GAMING-NOTEBOOK'
			MSI GF63 Thin 11UD-448 Gaming-Notebook (39,6 cm/15,6 Zoll, Intel Core i5 11400H, GeForce RTX 3050 Ti, 512 GB SSD)
			MSI Katana GF76 11UE-058 Gaming-Notebook (43,9 cm/17,3 Zoll, Intel Core i7 11800H, GeForce RTX™ 3060, 1000 GB SSD, Kostenloses Upgrade auf Windows 11, sobald verfügbar)

			MSI GF63 Thin 11UD-448 Gaming-Notebook (39,6 cm/15,6 Zoll, Intel Core i5 11400H, GeForce RTX 3050 Ti, 512 GB SSD)
			MSI Katana GF76 11UE-058 Gaming-Notebook (43,9 cm/17,3 Zoll, Intel Core i7 11800H, GeForce RTX™ 3060, 1000 GB SSD, Kostenloses Upgrade auf Windows 11, sobald verfügbar)

			MSI Katana GF76 11UG-412 Gaming-Notebook (43,9 cm/17,3 Zoll, Intel Core i7 11800H, GeForce RTX 3070, 1000 GB SSD)
				https://prnt.sc/OSI3kv-cDAQo
			MSI Katana GF76 11UE-058 Gaming-Notebook (43,9 cm/17,3 Zoll, Intel Core i7 11800H, GeForce RTX™ 3060, 1000 GB SSD, Kostenloses Upgrade auf Windows 11, sobald verfügbar)
				https://prnt.sc/m88zVUr3QUfB
			MSI GF63 Thin 11UD-448 Gaming-Notebook (39,6 cm/15,6 Zoll, Intel Core i5 11400H, GeForce RTX 3050 Ti, 512 GB SSD)
				https://prnt.sc/VXpf3-0_uTyF

		duplicate product_website
		https://prnt.sc/RY0EmiUewSVt
		https://prnt.sc/Ak9WhiFr3RtZ
		https://prnt.sc/OLsVdeYr2_qC

		Yes	keen

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.otto.de' and 
				var2.keyword in (N'TASTATUR') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# fb134120-09bc-4aa4-a14b-bb8abc9ac0f4@xhsDgt_tEee8oQANOijygQ
			# 2a986dce-b9ba-4b83-ad21-116f3dd7b7ef@xhsDgt_tEee8oQANOijygQ

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00@fb134120-09bc-4aa4-a14b-bb8abc9ac0f4@xhsDgt_tEee8oQANOijygQ,2022-03-09@22:00:00@K00@2a986dce-b9ba-4b83-ad21-116f3dd7b7ef@xhsDgt_tEee8oQANOijygQ

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00@2a986dce-b9ba-4b83-ad21-116f3dd7b7ef@xhsDgt_tEee8oQANOijygQ
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00@fb134120-09bc-4aa4-a14b-bb8abc9ac0f4@xhsDgt_tEee8oQANOijygQ
			# site behaviuor: valid after several reruns
			# done react, done sheets

	(R D-1) 2568	2022-03-09	New CMS	K00	all	www.emag.ro	
		'Wireless controller',
		'Gaming Mouse',
		'Controller Gaming',
		'Controller'

		Auto Copy Over for 1 day	

		https://prnt.sc/ZgeizEo69dek
		https://prnt.sc/630gbagxQo7i
		https://prnt.sc/V1JiWy0mB4wW
		https://prnt.sc/bkUfIN-CG-cO	

		Yes	kurt

		# solution
			# no issues during observation
			# done react, done sheets

	(R D-1) 2575	2022-03-09	New CMS	K00	all	komplett.no	
		Spillmousemat	
		Data Count Mismatch 12  (Fetched Data is Lesser than expected)
		https://prnt.sc/TwIL5BnObngs	
		Yes	keen

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-09' 
			AND website = 'www.komplett.no' 
			AND keyword in (N'Spillmousemat')
			GROUP BY keyword, ritem_id, country, SOURCE	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@22:00:00@K00@b22815f0-5b47-4dc1-8c43-d03c3bb59491@sWDIkgihEeeOEgANOrFolw
			# successful rerun
			# done react, done sheets

	0 (R D-3) 2576	2022-03-09	New CMS	K00	all	citilink.ru
		ГАРНИТУРА ДЛЯ ПК - 25
			4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g
		/ PS Контроллер - 8
			f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g
		/ Контроллер PS4 - 4
			784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g
		/ Контроллер PlayStation - 12
			f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g
		/ лучшие наушники	- 16
			f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@8THUOEXdEeeMawANOiZi-g

		Data Count Mismatch 9 (Fetched Data is Lesser than expected)
		Data Count Mismatch 6 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)
		Data Count Mismatch 3 (Fetched Data is Greater than expected)	

		https://prnt.sc/qlm4YEEUkFKm
		https://prnt.sc/b7GXTft6TeIj
		https://prnt.sc/T2jAt4FnJtz2
		https://prnt.sc/-wm6i6zhC821
		https://prnt.sc/47wnbnCWE2N0	
		Yes	renz

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.citilink.ru' and 
				var2.keyword in (N'PS Контроллер') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source		

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g,2022-03-09@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@8THUOEXdEeeMawANOiZi-g
				
			# for trello
				# has issues here: https://trello.com/c/P0RWk3uE/2622-razerrankings-new-cmsdata-count-mismatch-in-citilinkru

			# done react, done sheets

	(R D-1) 2577	2022-03-09	New CMS	200	all www.dns-shop.ru	
		колонка блютуз
		наушники с проводом	
		Duplicate product website and product url	
		https://prnt.sc/XAl9_1_2oXAD	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'колонка блютуз') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@21:00:00@200@047cab82-7af7-4f55-943c-8f044566b246@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheets

	(R D-1) 2578	2022-03-09	New CMS	200	all	www.re-store.ru	
		наушники c шумоподавлением	 
		invalid product website
		https://prnt.sc/PWHgHFjpjIhu	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.re-store.ru' and 
				var2.keyword in (N'наушники c шумоподавлением') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@21:00:00@200@9db59147-b69d-4b68-9154-63b1ccd0628f@a4059226-f529-46c6-8a35-c120219fbbab

			# rerun successful
			# done react, done sheets

	(R D-2) 2579	2022-03-09	New CMS	200	all	www.re-store.ru	
		/ наушники с проводом - 19
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная колонка wi-fi - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		/ проводные наушники	- 16
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4 (Fetched Data is lesser than expected)
		Data Count Mismatch 2 (Fetched Data is lesser than expected)
		Data Count Mismatch 5(Fetched Data is lesser than expected)	

		https://prnt.sc/ZSJUD4pToXD2
		https://prnt.sc/VESsBfMd6NVY
		https://prnt.sc/JzKSjKSiGK2n
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.re-store.ru' and 
				var2.keyword in (N'проводные наушники') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-09@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-09@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-09@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-09@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns

	-----
	(R D-1) 877	2022-03-07	New CMS	410	all	www.addnature.com	
		Haglöfs Dambyxor L Blue	
		Invalid Deadlink	
		https://prnt.sc/LlFv6Z1yjc9q	
		Yes	Yev

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-07' AND website = 'www.addnature.com' AND category in ('Haglöfs Dambyxor L Blue')

			# Select * from rm_internal_log
				Where v_schedule_date = '2022-03-07'
				And company = '410'
				And retailer = 'addnature.com'
				And data_id = 'LIST@Haglöfs Dambyxor L Blue'
				ORDER By timestamp

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-07@02:00:00@410@IU0@491d28e0-7f8d-4259-a12d-5ad0e4e45659
			# done react, done sheets

	(R D-1) 878	2022-03-07	New CMS	410	all	www.outnorth.se	
		Haglöfs Herrjackor XXL Yellow	
		Invalid Deadlink	
		https://prnt.sc/oUUTVUjKBHQ2	
		Yes	Yev

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-07' AND website = 'www.outnorth.se' AND category in ('Haglöfs Herrjackor XXL Yellow')

			# Select * from rm_internal_log
				Where v_schedule_date = '2022-03-07'
				And company = '410'
				And retailer = 'outnorth.se'
				And data_id = 'LIST@Haglöfs Herrjackor XXL Yellow'
				ORDER By timestamp

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-07@02:00:00@410@BH0@45d7ff97-eea0-4324-b353-2a7537a0e06f
			# successful rerun
			# done react, done sheets
			# to remove duplicate: through sir jon

	(R D-1) 879	2022-03-09	New CMS	K00	all	www.komplett.no	
		Gaming Tastatur	
		1 Day Auto Copy Over Data	
		https://prnt.sc/IzG-l4aGhyNe	
		Yes	Jairus

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.komplett.no' and 
				var2.category in (N'Gaming Tastatur') and 
				var2.[date] = '2022-03-09' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-09@22:00:00@K00@120@e586fa18-4afc-45bd-a7d6-e5a0cf21084f
			# successful rerun
			# done react, done sheets

	Stats: 
		Rerun: 15 (21) 


03 10 22
	(R D-1) 3963	2022-03-10	New CMS	F00	webshot_url	orange.be/nl/	
		'F00BELX040000001635540202111040406',
		'F00BEOX05R020220117024910829893017',
		'F00BELX040000001637030202111040406',
		'F00BELX040000001637250202111040406',
		'F00BEOX0ME020211207020310709480341',
		'F00BELX0EL000001658460202111152217',
		'F00BELX0EL000001658470202111152217',
		'F00BELX0JA000001637540202111040406',
		'F00BELX0JA000001637610202111040406',
		'F00BELX0JA000001637680202111040406',
		'F00BELX0JA000001638190202111040406',
		'F00BELX0JA000001639390202111040406',
		'F00BELX0JA000001639960202111040407',
		'F00BELX0JA000001640170202111040407',
		'F00BELX0JA000001640230202111040407',
		'F00BELX0JA000001640390202111040407',
		'F00BELX0JA000001640920202111040407',
		'F00BELX0JA000001640970202111040407',
		'F00BELX0JA000001639080202111040406'
		autocopy over (site is up)		
		Yes	Glenn

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-10' AND website = 'www.orange.be/nl/'	AND item_id in ('F00BELX040000001635540202111040406',
			'F00BEOX05R020220117024910829893017',
			'F00BELX040000001637030202111040406',
			'F00BELX040000001637250202111040406',
			'F00BEOX0ME020211207020310709480341',
			'F00BELX0EL000001658460202111152217',
			'F00BELX0EL000001658470202111152217',
			'F00BELX0JA000001637540202111040406',
			'F00BELX0JA000001637610202111040406',
			'F00BELX0JA000001637680202111040406',
			'F00BELX0JA000001638190202111040406',
			'F00BELX0JA000001639390202111040406',
			'F00BELX0JA000001639960202111040407',
			'F00BELX0JA000001640170202111040407',
			'F00BELX0JA000001640230202111040407',
			'F00BELX0JA000001640390202111040407',
			'F00BELX0JA000001640920202111040407',
			'F00BELX0JA000001640970202111040407',
			'F00BELX0JA000001639080202111040406')

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@F00BELX040000001635540202111040406,2022-03-10@22:00:00@F00BEOX05R020220117024910829893017,2022-03-10@22:00:00@F00BELX040000001637030202111040406,2022-03-10@22:00:00@F00BELX040000001637250202111040406,2022-03-10@22:00:00@F00BEOX0ME020211207020310709480341,2022-03-10@22:00:00@F00BELX0EL000001658460202111152217,2022-03-10@22:00:00@F00BELX0EL000001658470202111152217,2022-03-10@22:00:00@F00BELX0JA000001637540202111040406,2022-03-10@22:00:00@F00BELX0JA000001637610202111040406,2022-03-10@22:00:00@F00BELX0JA000001637680202111040406,2022-03-10@22:00:00@F00BELX0JA000001638190202111040406,2022-03-10@22:00:00@F00BELX0JA000001639390202111040406,2022-03-10@22:00:00@F00BELX0JA000001639960202111040407,2022-03-10@22:00:00@F00BELX0JA000001640170202111040407,2022-03-10@22:00:00@F00BELX0JA000001640230202111040407,2022-03-10@22:00:00@F00BELX0JA000001640390202111040407,2022-03-10@22:00:00@F00BELX0JA000001640920202111040407,2022-03-10@22:00:00@F00BELX0JA000001640970202111040407,2022-03-10@22:00:00@F00BELX0JA000001639080202111040406

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@F00BELX040000001637250202111040406,2022-03-10@22:00:00@F00BELX0JA000001637610202111040406,2022-03-10@22:00:00@F00BELX0JA000001640970202111040407

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 3964	2022-03-10	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001324310202102090603',
		'100DK10010000001659210202111162228',
		'100DK100ZQ020220124090756117378024',
		'100DK100ZQ020220124090754197223024',
		'100DK100ZQ020220124090753862646024',
		'100DK100ZQ020220124093144653882024',
		'100DK10010000001324400202102090603',
		'100DK10010000001248210202011250630',
		'100DK10010000001659390202111162228',
		'100DK10010000000814140201908020341',
		'100DK10010000001659300202111162228',
		'100DK100ZQ020220124093147829564024',
		'100DK100ZQ020220124093147433839024',
		'100DK100ZQ020220124093147040174024',
		'100DK100ZQ020220124093145806437024',
		'100DK10010000001248290202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001659670202111162228',
		'100DK100ZQ020220124093123122384024',
		'100DK10010000001248360202011250630',
		'100DK100ZQ020220214062205360488045',
		'100DK10010000001660410202111162228',
		'100DK100ZQ020220214062204402797045',
		'100DK100ZQ020220124093149319561024',
		'100DK10010000001248390202011250630',
		'100DK100ZQ020220124093124632102024',
		'100DK10010000001324630202102090603',
		'100DK10010000001400780202105040816',
		'100DK10010000001222780202010280537',
		'100DK100ZQ020220124093148982275024',
		'100DK10010000001397490202105040251',
		'100DK10010000001397940202105040251',
		'100DK100ZQ020220124093123601621024',
		'100DK10010000001248400202011250630',
		'100DK10010000001326370202102090603',
		'100DK10010000001222840202010280537',
		'100DK10010000000818120201908020935',
		'100DK10010000001327030202102090604',
		'100DK100ZQ020220207085518362413038',
		'100DK100ZQ020220124093133398639024',
		'100DK10010000000821000201908050425',
		'100DK10010000001222820202010280537'
			
		wrong delivery		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-10' AND website = 'www.elgiganten.dk'	AND item_id in ('100DK10010000001324310202102090603',
			'100DK10010000001659210202111162228',
			'100DK100ZQ020220124090756117378024',
			'100DK100ZQ020220124090754197223024',
			'100DK100ZQ020220124090753862646024',
			'100DK100ZQ020220124093144653882024',
			'100DK10010000001324400202102090603',
			'100DK10010000001248210202011250630',
			'100DK10010000001659390202111162228',
			'100DK10010000000814140201908020341',
			'100DK10010000001659300202111162228',
			'100DK100ZQ020220124093147829564024',
			'100DK100ZQ020220124093147433839024',
			'100DK100ZQ020220124093147040174024',
			'100DK100ZQ020220124093145806437024',
			'100DK10010000001248290202011250630',
			'100DK10010000001248260202011250630',
			'100DK10010000001659670202111162228',
			'100DK100ZQ020220124093123122384024',
			'100DK10010000001248360202011250630',
			'100DK100ZQ020220214062205360488045',
			'100DK10010000001660410202111162228',
			'100DK100ZQ020220214062204402797045',
			'100DK100ZQ020220124093149319561024',
			'100DK10010000001248390202011250630',
			'100DK100ZQ020220124093124632102024',
			'100DK10010000001324630202102090603',
			'100DK10010000001400780202105040816',
			'100DK10010000001222780202010280537',
			'100DK100ZQ020220124093148982275024',
			'100DK10010000001397490202105040251',
			'100DK10010000001397940202105040251',
			'100DK100ZQ020220124093123601621024',
			'100DK10010000001248400202011250630',
			'100DK10010000001326370202102090603',
			'100DK10010000001222840202010280537',
			'100DK10010000000818120201908020935',
			'100DK10010000001327030202102090604',
			'100DK100ZQ020220207085518362413038',
			'100DK100ZQ020220124093133398639024',
			'100DK10010000000821000201908050425',
			'100DK10010000001222820202010280537')

			# https://prnt.sc/I1L-cBO4DHL8

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@100DK10010000001324310202102090603,2022-03-10@22:00:00@100DK10010000001659210202111162228,2022-03-10@22:00:00@100DK100ZQ020220124090756117378024,2022-03-10@22:00:00@100DK100ZQ020220124090754197223024,2022-03-10@22:00:00@100DK100ZQ020220124090753862646024,2022-03-10@22:00:00@100DK100ZQ020220124093144653882024,2022-03-10@22:00:00@100DK10010000001324400202102090603,2022-03-10@22:00:00@100DK10010000001248210202011250630,2022-03-10@22:00:00@100DK10010000001659390202111162228,2022-03-10@22:00:00@100DK10010000000814140201908020341,2022-03-10@22:00:00@100DK10010000001659300202111162228,2022-03-10@22:00:00@100DK100ZQ020220124093147829564024,2022-03-10@22:00:00@100DK100ZQ020220124093147433839024,2022-03-10@22:00:00@100DK100ZQ020220124093147040174024,2022-03-10@22:00:00@100DK100ZQ020220124093145806437024,2022-03-10@22:00:00@100DK10010000001248290202011250630,2022-03-10@22:00:00@100DK10010000001248260202011250630,2022-03-10@22:00:00@100DK10010000001659670202111162228,2022-03-10@22:00:00@100DK100ZQ020220124093123122384024,2022-03-10@22:00:00@100DK10010000001248360202011250630,2022-03-10@22:00:00@100DK100ZQ020220214062205360488045,2022-03-10@22:00:00@100DK10010000001660410202111162228,2022-03-10@22:00:00@100DK100ZQ020220214062204402797045,2022-03-10@22:00:00@100DK100ZQ020220124093149319561024,2022-03-10@22:00:00@100DK10010000001248390202011250630,2022-03-10@22:00:00@100DK100ZQ020220124093124632102024,2022-03-10@22:00:00@100DK10010000001324630202102090603,2022-03-10@22:00:00@100DK10010000001400780202105040816,2022-03-10@22:00:00@100DK10010000001222780202010280537,2022-03-10@22:00:00@100DK100ZQ020220124093148982275024,2022-03-10@22:00:00@100DK10010000001397490202105040251,2022-03-10@22:00:00@100DK10010000001397940202105040251,2022-03-10@22:00:00@100DK100ZQ020220124093123601621024,2022-03-10@22:00:00@100DK10010000001248400202011250630,2022-03-10@22:00:00@100DK10010000001326370202102090603,2022-03-10@22:00:00@100DK10010000001222840202010280537,2022-03-10@22:00:00@100DK10010000000818120201908020935,2022-03-10@22:00:00@100DK10010000001327030202102090604,2022-03-10@22:00:00@100DK100ZQ020220207085518362413038,2022-03-10@22:00:00@100DK100ZQ020220124093133398639024,2022-03-10@22:00:00@100DK10010000000821000201908050425,2022-03-10@22:00:00@100DK10010000001222820202010280537

			100DK100ZQ020220124093133398639024
			100DK10010000001248290202011250630
			100DK100ZQ020220124093147040174024

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@100DK100ZQ020220124093133398639024,2022-03-10@22:00:00@100DK10010000001248290202011250630,2022-03-10@22:00:00@100DK100ZQ020220124093147040174024

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@100DK100ZQ020220124093133398639024

			# rerun successful
			# done react, done sheets

	(R D-2) 3965	2022-03-10	New CMS	P00	all	alza.cz	
		P00CZ3601L000001429920202106010821	
		spider issue (invalid/incorrect data scraped)		
		Yes	Caesa

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@P00CZ3601L000001429920202106010821
			# rerun failed
			# check local: 
			# goods ndaw. hahaha
			# 

	-----
	(R D-1) 2595	2022-03-10	New CMS	F00	all	Coolblue.nl
		'Galaxy S21',
		'Samsung S'
		Auto copyover for 1 days	

		https://prnt.sc/8VMa22YRsT3H
		https://prnt.sc/uNr0UvapweqO
		yes	Mich

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-10' 
				AND website = 'www.Coolblue.nl' 
				AND keyword in (N'Galaxy S21',
						'Samsung S')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@F00@fc7d7040-4168-4d02-9d2e-2f0cc8f5630e@dyBnppOqEei-eQANOiOHiA,2022-03-10@22:00:00@F00@76ed170e-e499-48b4-8038-2859144dee3b@dyBnppOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@F00@76ed170e-e499-48b4-8038-2859144dee3b@dyBnppOqEei-eQANOiOHiA
			# successful rerun
			# done react, done sheet

	(R D-1) 2597	2022-03-10	New CMS	200	all	www.dns-shop.ru
		наушники с проводом
		Invalid Deadlink
		https://prnt.sc/VMeW6Kg1Ux2H
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники с проводом') and 
				var2.[date] = '2022-03-10' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-1) 2598	2022-03-10	New CMS	200	all	www.dns-shop.ru
		гарнитура
		Duplicate Product Website and product Url
		https://prnt.sc/UkfVVRjfyRiG
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'гарнитура') and 
				var2.[date] = '2022-03-10' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-10@21:00:00@200@bee38489-f39a-4402-9a45-4b7a61706873@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheet

	(R D-2) 2603	2022-03-10	New CMS	200	all	www.mediamarkt.be/fr
		'Bluetooth headset',
		'enceinte connectée'
		3 days ACO Data
		https://prnt.sc/79Npi3yXpgNU	
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.mediamarkt.be/fr' and 
				var2.keyword in (N'Bluetooth headset',
						'enceinte connectée') and 
				var2.[date] = '2022-03-10' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@200@c5c38081-d328-406b-8bad-1829de2b616d@JpQcJEXgEeeMawANOiZi-g,2022-03-10@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@JpQcJEXgEeeMawANOiZi-g

			# multiple reruns failed
			# check local: 
			# goods ndaw. hahhaa
			# 

					
	(R D-1) 2604	2022-03-10	New CMS	200	all	www.darty.com
		'casque audio reducteur de bruit', - 11
		'Casque bluetooth reducteur de bruit' - 13

		Data Count Mismatch 9 (Fetched Data is Lesser than expected)
		Data Count Mismatch 9 (Fetched Data is Lesser than expected)

		https://prnt.sc/lOofTDtY7C6r 
		https://prnt.sc/d8icXBDYPTM1	
		Yes	Christopher

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-10' 
				AND website = 'www.darty.com' 
				AND keyword in (N'casque audio reducteur de bruit',
						'Casque bluetooth reducteur de bruit')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@ds73aApHEem-gAANOiOHiA,2022-03-10@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@ds73aApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2605	2022-03-10	New CMS	200	all	www.fnac.com	
		'casque audio sans fil',
		'casque bluetooth',
		'enceinte sans fil bluetooth',
		'mini enceinte bluetooth'

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)

		https://prnt.sc/qdIFuHhcBq8t
		https://prnt.sc/TyVxdyxG0zUo	
		Yes	Christopher

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-10' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio sans fil',
						'casque bluetooth',
						'enceinte sans fil bluetooth',
						'mini enceinte bluetooth')
				GROUP BY keyword, ritem_id, country, source 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@200@1c673651-2a20-453f-ab31-340d94dc9d29@LzP6XApHEem-gAANOiOHiA,2022-03-10@22:00:00@200@676500d2-0e92-42bd-81e7-ddad4608f9db@LzP6XApHEem-gAANOiOHiA,2022-03-10@22:00:00@200@65c894b9-75aa-4ab7-8315-fb0777bc8aa2@LzP6XApHEem-gAANOiOHiA,2022-03-10@22:00:00@200@4c50f1e9-6a50-42ba-96ab-ff8cf9d8ce9a@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@200@1c673651-2a20-453f-ab31-340d94dc9d29@LzP6XApHEem-gAANOiOHiA,2022-03-10@22:00:00@200@65c894b9-75aa-4ab7-8315-fb0777bc8aa2@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@200@65c894b9-75aa-4ab7-8315-fb0777bc8aa2@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful 
			# done react, done sheets

	(R D-1) 2606	2022-03-10	New CMS	200	all	www.saturn.de	
		kopfhörer anc	
		Duplicate product website and product url
		https://prnt.sc/23aOJIDGyNcF
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.saturn.de' and 
				var2.keyword in (N'kopfhörer anc') and 
				var2.[date] = '2022-03-10' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@200@f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@UbbyaApIEem-gAANOiOHiA
			# rerun succesful
			# done react, done sheet

	(R D-1) 2607	2022-03-10	New CMS	200	all	www.bol.com	
		hoofdtelefoon	
		Duplicate product website and product url
		https://prnt.sc/4Q3EJwgHkYY7
		Yes	Christopher

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'hoofdtelefoon') and 
				var2.[date] = '2022-03-10' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-10@22:00:00@200@94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheet

	-----
	(R D-1) 885	2022-03-10	New CMS	O00	all	www.expert.nl	
		TV 2020	
		1 day auto copy over	
		https://prnt.sc/1mnIlDuyq-ZM	
		Yes	Yev

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-10' 
				AND website = 'www.expert.nl' 
				AND category in (N'TV 2020')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-10@22:00:00@O00@HS0@6e787901-b16f-4289-8e10-d65c69663670
			# successful rerun
			# done react, done sheet

	(R D-1) 886	2022-03-10	New CMS	200	all	www.amazon.co.uk	
		Headphones Bluetooth In Ear
		Headphones Bluetooth On Ear	

		Fetching data even if its deadlink	

		https://prnt.sc/KhDyWu8PjHAc
		https://prnt.sc/ZbNS1yWNGGp8	
		Yes	Jairus

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-10' 
				AND website = 'www.amazon.co.uk' 
				AND category in (N'Headphones Bluetooth In Ear', 'Headphones Bluetooth On Ear')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-10@23:00:00@200@G00@bd91be1f-2495-49d5-8220-906f55242bd6,2022-03-10@23:00:00@200@G00@a3b64247-6b9d-4f54-818d-b88889214619

			# check local (should be valid deadlink):
				https://www.amazon.co.uk/s/ref=lp_4085751_nr_p_n_style_browse-bin_0?fst=as%3Aoff&rh=n%3A560798%2Cn%3A%21560800%2Cn%3A1345741031%2Cn%3A560882%2Cn%3A4085731%2Cn%3A4085751%2Cp_n_style_browse-bin%3A1601093031&bbn=4085751&ie=UTF8&qid=1547558922&rnid=1600975031

				https://www.amazon.co.uk/s/s/ref=sr_nr_p_n_style_browse-bin_2?fst=as%3Aoff&rh=n%3A560798%2Cn%3A%21560800%2Cn%3A1345741031%2Cn%3A560882%2Cn%3A4085731%2Cn%3A4085751%2Cp_n_style_browse-bin%3A1601094031%7C1601095031&bbn=4085751&ie=UTF8&qid=1547558993&rnid=1600975031

				# obfuscated, cant create indicator for deadlink, but acceptable
				# done push: to alter
				# done react, done sheet

		200@Zound@11:00AM@11:50AM@Prod2

	Stats:
		Rerun: 13 (17)
		
03 11 22
	(W) WEBSHOTS EMEA REGION (A00)
		# solution
			# python manual_cmp_webshots_override.py -c A00 -i 2022-03-11@22:00:00@A00DE810KT020220302122958443277061,2022-03-11@22:00:00@A00DEY00KT020220302122957602829061,2022-03-11@22:00:00@A00DEY00KT020220302122956023099061,2022-03-11@22:00:00@A00DE8Q0KT020220302122956082046061,2022-03-11@22:00:00@A00DE8Q0KT020220302122956617645061,2022-03-11@22:00:00@A00DE8Q0KT020220302122951455311061,2022-03-11@22:00:00@A00DE810KT020211012064148951796285,2022-03-11@22:00:00@A00DE910KT020211012064143756677285,2022-03-11@22:00:00@A00DE8Q0KT020211012064143943780285,2022-03-11@22:00:00@A00DE810KT020211012064143501972285,2022-03-11@22:00:00@A00DE8Q0KT020211012064145522899285,2022-03-11@22:00:00@A00DEY00KT020211012064145929817285,2022-03-11@22:00:00@A00DE8Q0KT020211012064146007754285,2022-03-11@22:00:00@A00DE8Q0KT020211012064142843474285,2022-03-11@22:00:00@A00DE810KT020211012064142924207285,2022-03-11@22:00:00@A00DE8Q0KT020211012064142579387285,2022-03-11@22:00:00@A00DE8Q0KT020211012064141581340285,2022-03-11@22:00:00@A00DE810KT020211012064142132029285,2022-03-11@22:00:00@A00DE8Q0KT020211012064138294905285,2022-03-11@22:00:00@A00DE8Q0KT020211012064146362869285,2022-03-11@22:00:00@A00DE810KT020211012064147432594285,2022-03-11@22:00:00@A00DE8Q0KT020211012064140231866285,2022-03-11@22:00:00@A00DE810KT020211012064149779601285,2022-03-11@22:00:00@A00DE810RB000001343080202103100452,2022-03-11@22:00:00@A00DE8Q0RB000001344670202103100607,2022-03-11@22:00:00@A00DE8Q0RB000001404070202105050652,2022-03-11@22:00:00@A00DE810RB000001404100202105050652,2022-03-11@22:00:00@A00DE8Q0RB000001404110202105050652,2022-03-11@22:00:00@A00DE8Q0RB000001404170202105050652,2022-03-11@22:00:00@A00DE8Q0RB000001361070202103310255,2022-03-11@22:00:00@A00DE8Q0RB000001361090202103310255,2022-03-11@22:00:00@A00DE8Q0RB000001361120202103310255,2022-03-11@22:00:00@A00DE8Q0RB000001361150202103310255,2022-03-11@22:00:00@A00DE8Q0RB000001404230202105050652,2022-03-11@22:00:00@A00DE8Q0RB000001404190202105050652,2022-03-11@22:00:00@A00DE810RB000001236830202011060511,2022-03-11@22:00:00@A00DE810RB000001236850202011060511,2022-03-11@22:00:00@A00DE910RB000000992250202007210557,2022-03-11@22:00:00@A00DE910JA000001202910202008280227,2022-03-11@22:00:00@A00DE810RB000000950310202005220114,2022-03-11@22:00:00@A00DE8Q0RB000000950430202005220114,2022-03-11@22:00:00@A00DE8Q0RB000000950550202005220114,2022-03-11@22:00:00@A00DE8Q0RB000000950620202005220114,2022-03-11@22:00:00@A00DE810RB000000949610202005220114,2022-03-11@22:00:00@A00DE8Q0RB000000899610202001170930,2022-03-11@22:00:00@A00DE8Q0RB000000902290202001211634,2022-03-11@22:00:00@A00DE8Q0RB000000896640202001160334,2022-03-11@22:00:00@A00DE8Q0RB000000896960202001160409,2022-03-11@22:00:00@A00DE8Q0RB000000898230202001170423,2022-03-11@22:00:00@A00DE8Q0RB000000898120202001170400,2022-03-11@22:00:00@A00DE8Q0RB000000898350202001170611,2022-03-11@22:00:00@A00DE8Q0RB000000898550202001170728,2022-03-11@22:00:00@A00DE8Q0RB000000898910202001170804,2022-03-11@22:00:00@A00DE8Q0RB000000898880202001170802,2022-03-11@22:00:00@A00DE8Q0RB000000898800202001170756,2022-03-11@22:00:00@A00DE8Q0RB000000898770202001170754,2022-03-11@22:00:00@A00DE8Q0RB000000899040202001170811,2022-03-11@22:00:00@A00DE8Q0RB000000899400202001170903,2022-03-11@22:00:00@A00DE910RB000000873340201911061118,2022-03-11@22:00:00@A00DE910RB000000873410201911061125,2022-03-11@22:00:00@A00DE810RB000000872840201911061015,2022-03-11@22:00:00@A00DE810RB000000871990201911060833,2022-03-11@22:00:00@A00DE810RB000000872180201911060909,2022-03-11@22:00:00@A00DEY00RB000000873180201911061053,2022-03-11@22:00:00@A00DEY00RB000000875350201911070657,2022-03-11@22:00:00@A00DEY00RB000000875610201911070713,2022-03-11@22:00:00@A00DE810GC000000218920201903040911,2022-03-11@22:00:00@A00DE810GC000000296640201903040958,2022-03-11@22:00:00@A00DE810GC000000134210201903040450,2022-03-11@22:00:00@A00DE810GC000000159920201903040511,2022-03-11@22:00:00@A00DE810GC000000171080201903040525,2022-03-11@22:00:00@A00DE910GC000000175940201903040527,2022-03-11@22:00:00@A00DE810GC000000175960201903040527,2022-03-11@22:00:00@A00DE810GC000000229300201903040918,2022-03-11@22:00:00@A00ES0B0RB000000992200202007210542,2022-03-11@22:00:00@A00FR410GC000000274400201903040948,2022-03-11@22:00:00@A00FR410GC000000330210201903041012,2022-03-11@22:00:00@A00FR410GC000000265710201903040942,2022-03-11@22:00:00@A00FR410RB000000872950201911061022,2022-03-11@22:00:00@A00FR410RB000001235610202011060456,2022-03-11@22:00:00@A00FR410RB000001235590202011060456,2022-03-11@22:00:00@A00FR410RB000001411190202105120254,2022-03-11@22:00:00@A00FR410RB000001411170202105120254,2022-03-11@22:00:00@A00FR410RB000001411090202105120254,2022-03-11@22:00:00@A00FR410RB000001235070202011060456,2022-03-11@22:00:00@A00FR410KT020211012060848264050285,2022-03-11@22:00:00@A00FR410KT020211115034837092154319,2022-03-11@22:00:00@A00FR9Q0KT020211115034835896026319,2022-03-11@23:00:00@A00GB7Q0RB000000896580202001160328,2022-03-11@23:00:00@A00GBI00RB000001235550202011060456,2022-03-11@23:00:00@A00GBI00RB000001235300202011060456,2022-03-11@23:00:00@A00GBI00RB000001235390202011060456,2022-03-11@23:00:00@A00GBI00RB000001404990202105050652,2022-03-11@23:00:00@A00GBG00GC000000190640201903040714,2022-03-11@23:00:00@A00GBI00KT020211025061807096749298,2022-03-11@23:00:00@A00GBG00KT020211025061813446288298,2022-03-11@23:00:00@A00GBI00KT020211025061814878457298,2022-03-11@22:00:00@A00ITNR0RB000000993350202007230615,2022-03-11@22:00:00@A00ITNR0RB000001404740202105050652,2022-03-11@22:00:00@A00NLWQ0RB000001235260202011060456,2022-03-11@22:00:00@A00NLWQ0RB000000993940202007291033

			count:
				63
				61
				53
				18
				0
			# done update Jovy Curinay

	(R D-3) 3981	2022-03-11	New CMS	610	image_count		
		'610SEXW00P000001492810202107200326',
		'610SEXW00P000001492960202107200327',
		'610SEXW00P000001493050202107200327',
		'610SEXW00P000001493440202107200327',
		'610SEXW00P000001493500202107200327',
		'610SEXW00P000001493620202107200327',
		'610SEXW00P000001494910202107200327',
		'610SEXW00P000001495000202107200327',
		'610SEXW00P000001495030202107200327',
		'610SEXW00P000001495600202107200327',
		'610SEXW00P000001495630202107200327',
		'610SEXW00P000001496110202107200327',
		'610SEXW00P000001496170202107200327',
		'610SEXW00P000001496350202107200327',
		'610SEXW00P000001496680202107200327',
		'610SEXW00P000001497940202107200328',
		'610SEXW00P000001610280202109300304',
		'610SEXW00P000001610330202109300304',
		'610SEXW00P000001610360202109300304',
		'610SEXW00P000001610390202109300304',
		'610SEXW00P000001630670202110280537',
		'610SEXW00P000001631410202110280537',
		'610SEXW01P000001493920202107200327',
		'610SEXW01P000001493980202107200327',
		'610SEXW01P000001494130202107200327',
		'610SEXW01P000001494190202107200327',
		'610SEXW01P000001494430202107200327',
		'610SEXW01P000001494460202107200327',
		'610SEXW01P000001494520202107200327',
		'610SEXW01P000001494610202107200327',
		'610SEXW01P000001494850202107200327',
		'610SEXW01P000001495150202107200327',
		'610SEXW01P000001495180202107200327',
		'610SEXW01P000001495450202107200327',
		'610SEXW01P000001495480202107200327',
		'610SEXW01P000001495690202107200327',
		'610SEXW01P000001495720202107200327',
		'610SEXW01P000001495780202107200327',
		'610SEXW01P000001495870202107200327',
		'610SEXW01P000001496050202107200327',
		'610SEXW01P000001496080202107200327',
		'610SEXW01P000001496530202107200327',
		'610SEXW01P000001496560202107200327',
		'610SEXW01P000001496590202107200327',
		'610SEXW01P000001496800202107200327',
		'610SEXW01P000001496830202107200327',
		'610SEXW01P000001497010202107200327',
		'610SEXW01P000001497100202107200327',
		'610SEXW01P000001497850202107200328',
		'610SEXW01P000001498210202107200328',
		'610SEXW01P000001498240202107200328',
		'610SEXW01P000001498300202107200328',
		'610SEXW01P000001498330202107200328',
		'610SEXW01P000001498360202107200328',
		'610SEXW01P000001498390202107200328',
		'610SEXW01P000001498720202107200328',
		'610SEXW01P000001498870202107200328',
		'610SEXW01P000001499080202107200328',
		'610SEXW01P000001499140202107200328',
		'610SEXW01P000001499200202107200328',
		'610SEXW01P000001499320202107200328',
		'610SEXW01P000001499380202107200328',
		'610SEXW01P000001499440202107200328',
		'610SEXW01P000001499590202107200328',
		'610SEXW01P000001499620202107200328',
		'610SEXW01P000001610420202109300304',
		'610SEXW01P000001610450202109300304'

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW00P000001492810202107200326,2022-03-11@22:00:00@610SEXW00P000001492960202107200327,2022-03-11@22:00:00@610SEXW00P000001493050202107200327,2022-03-11@22:00:00@610SEXW00P000001493440202107200327,2022-03-11@22:00:00@610SEXW00P000001493500202107200327,2022-03-11@22:00:00@610SEXW00P000001493620202107200327,2022-03-11@22:00:00@610SEXW00P000001494910202107200327,2022-03-11@22:00:00@610SEXW00P000001495000202107200327,2022-03-11@22:00:00@610SEXW00P000001495030202107200327,2022-03-11@22:00:00@610SEXW00P000001495600202107200327,2022-03-11@22:00:00@610SEXW00P000001495630202107200327,2022-03-11@22:00:00@610SEXW00P000001496110202107200327,2022-03-11@22:00:00@610SEXW00P000001496170202107200327,2022-03-11@22:00:00@610SEXW00P000001496350202107200327,2022-03-11@22:00:00@610SEXW00P000001496680202107200327,2022-03-11@22:00:00@610SEXW00P000001497940202107200328,2022-03-11@22:00:00@610SEXW00P000001610280202109300304,2022-03-11@22:00:00@610SEXW00P000001610330202109300304,2022-03-11@22:00:00@610SEXW00P000001610360202109300304,2022-03-11@22:00:00@610SEXW00P000001610390202109300304,2022-03-11@22:00:00@610SEXW00P000001630670202110280537,2022-03-11@22:00:00@610SEXW00P000001631410202110280537,2022-03-11@22:00:00@610SEXW01P000001493920202107200327,2022-03-11@22:00:00@610SEXW01P000001493980202107200327,2022-03-11@22:00:00@610SEXW01P000001494130202107200327,2022-03-11@22:00:00@610SEXW01P000001494190202107200327,2022-03-11@22:00:00@610SEXW01P000001494430202107200327,2022-03-11@22:00:00@610SEXW01P000001494460202107200327,2022-03-11@22:00:00@610SEXW01P000001494520202107200327,2022-03-11@22:00:00@610SEXW01P000001494610202107200327,2022-03-11@22:00:00@610SEXW01P000001494850202107200327,2022-03-11@22:00:00@610SEXW01P000001495150202107200327,2022-03-11@22:00:00@610SEXW01P000001495180202107200327,2022-03-11@22:00:00@610SEXW01P000001495450202107200327,2022-03-11@22:00:00@610SEXW01P000001495480202107200327,2022-03-11@22:00:00@610SEXW01P000001495690202107200327,2022-03-11@22:00:00@610SEXW01P000001495720202107200327,2022-03-11@22:00:00@610SEXW01P000001495780202107200327,2022-03-11@22:00:00@610SEXW01P000001495870202107200327,2022-03-11@22:00:00@610SEXW01P000001496050202107200327,2022-03-11@22:00:00@610SEXW01P000001496080202107200327,2022-03-11@22:00:00@610SEXW01P000001496530202107200327,2022-03-11@22:00:00@610SEXW01P000001496560202107200327,2022-03-11@22:00:00@610SEXW01P000001496590202107200327,2022-03-11@22:00:00@610SEXW01P000001496800202107200327,2022-03-11@22:00:00@610SEXW01P000001496830202107200327,2022-03-11@22:00:00@610SEXW01P000001497010202107200327,2022-03-11@22:00:00@610SEXW01P000001497100202107200327,2022-03-11@22:00:00@610SEXW01P000001497850202107200328,2022-03-11@22:00:00@610SEXW01P000001498210202107200328,2022-03-11@22:00:00@610SEXW01P000001498240202107200328,2022-03-11@22:00:00@610SEXW01P000001498300202107200328,2022-03-11@22:00:00@610SEXW01P000001498330202107200328,2022-03-11@22:00:00@610SEXW01P000001498360202107200328,2022-03-11@22:00:00@610SEXW01P000001498390202107200328,2022-03-11@22:00:00@610SEXW01P000001498720202107200328,2022-03-11@22:00:00@610SEXW01P000001498870202107200328,2022-03-11@22:00:00@610SEXW01P000001499080202107200328,2022-03-11@22:00:00@610SEXW01P000001499140202107200328,2022-03-11@22:00:00@610SEXW01P000001499200202107200328,2022-03-11@22:00:00@610SEXW01P000001499320202107200328,2022-03-11@22:00:00@610SEXW01P000001499380202107200328,2022-03-11@22:00:00@610SEXW01P000001499440202107200328,2022-03-11@22:00:00@610SEXW01P000001499590202107200328,2022-03-11@22:00:00@610SEXW01P000001499620202107200328,2022-03-11@22:00:00@610SEXW01P000001610420202109300304,2022-03-11@22:00:00@610SEXW01P000001610450202109300304

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001499320202107200328,2022-03-11@22:00:00@610SEXW01P000001499620202107200328,2022-03-11@22:00:00@610SEXW01P000001494460202107200327,2022-03-11@22:00:00@610SEXW01P000001494130202107200327,2022-03-11@22:00:00@610SEXW01P000001499590202107200328,2022-03-11@22:00:00@610SEXW01P000001495720202107200327,2022-03-11@22:00:00@610SEXW01P000001499140202107200328,2022-03-11@22:00:00@610SEXW00P000001610330202109300304,2022-03-11@22:00:00@610SEXW01P000001497100202107200327,2022-03-11@22:00:00@610SEXW00P000001492810202107200326,2022-03-11@22:00:00@610SEXW01P000001495450202107200327,2022-03-11@22:00:00@610SEXW01P000001494430202107200327,2022-03-11@22:00:00@610SEXW01P000001498720202107200328,2022-03-11@22:00:00@610SEXW01P000001498390202107200328,2022-03-11@22:00:00@610SEXW00P000001493620202107200327,2022-03-11@22:00:00@610SEXW01P000001495870202107200327,2022-03-11@22:00:00@610SEXW00P000001610390202109300304,2022-03-11@22:00:00@610SEXW00P000001495630202107200327,2022-03-11@22:00:00@610SEXW01P000001496050202107200327,2022-03-11@22:00:00@610SEXW01P000001495480202107200327,2022-03-11@22:00:00@610SEXW01P000001496530202107200327,2022-03-11@22:00:00@610SEXW01P000001494850202107200327,2022-03-11@22:00:00@610SEXW00P000001495600202107200327,2022-03-11@22:00:00@610SEXW01P000001496080202107200327,2022-03-11@22:00:00@610SEXW00P000001497940202107200328,2022-03-11@22:00:00@610SEXW01P000001494190202107200327,2022-03-11@22:00:00@610SEXW00P000001496350202107200327,2022-03-11@22:00:00@610SEXW01P000001496560202107200327,2022-03-11@22:00:00@610SEXW01P000001494610202107200327

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001499590202107200328,2022-03-11@22:00:00@610SEXW01P000001495720202107200327,2022-03-11@22:00:00@610SEXW01P000001494430202107200327,2022-03-11@22:00:00@610SEXW01P000001498720202107200328,2022-03-11@22:00:00@610SEXW00P000001493620202107200327,2022-03-11@22:00:00@610SEXW01P000001495870202107200327,2022-03-11@22:00:00@610SEXW00P000001495630202107200327,2022-03-11@22:00:00@610SEXW01P000001496050202107200327,2022-03-11@22:00:00@610SEXW01P000001495480202107200327,2022-03-11@22:00:00@610SEXW01P000001496530202107200327,2022-03-11@22:00:00@610SEXW01P000001494850202107200327,2022-03-11@22:00:00@610SEXW01P000001496080202107200327,2022-03-11@22:00:00@610SEXW00P000001496350202107200327,2022-03-11@22:00:00@610SEXW01P000001496560202107200327

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001499590202107200328,2022-03-11@22:00:00@610SEXW01P000001495720202107200327,2022-03-11@22:00:00@610SEXW01P000001494430202107200327,2022-03-11@22:00:00@610SEXW01P000001498720202107200328,2022-03-11@22:00:00@610SEXW00P000001493620202107200327,2022-03-11@22:00:00@610SEXW01P000001495870202107200327,2022-03-11@22:00:00@610SEXW00P000001495630202107200327,2022-03-11@22:00:00@610SEXW01P000001496530202107200327,2022-03-11@22:00:00@610SEXW00P000001496350202107200327

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001495720202107200327,2022-03-11@22:00:00@610SEXW01P000001498720202107200328,2022-03-11@22:00:00@610SEXW01P000001495870202107200327

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001495720202107200327
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@610SEXW01P000001498720202107200328

			# multiple reruns successful
			# done react, done sheets

	(R D-2) 3982	2022-03-11	New CMS	100	delivery	elgiganten.dk	
		'100DK100ZQ020220124093126046040024',
		'100DK100ZQ020220124090755315930024',
		'100DK100ZQ020220124090754985166024',
		'100DK100ZQ020220124090754656295024',
		'100DK100ZQ020220124090754819827024',
		'100DK100ZQ020220124090754027240024',
		'100DK100ZQ020220124090753733277024',
		'100DK100ZQ020220124093144653882024',
		'100DK10010000001324400202102090603',
		'100DK10010000001324440202102090603',
		'100DK10010000001659410202111162228',
		'100DK10010000000813290201908020245',
		'100DK10010000001248210202011250630',
		'100DK10010000001659390202111162228',
		'100DK100ZQ020220124093144395897024',
		'100DK100ZQ020220124093144115061024',
		'100DK10010000001659370202111162228',
		'100DK100ZQ020220124093147433839024',
		'100DK100ZQ020220124093147040174024',
		'100DK10010000001248290202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001248230202011250630',
		'100DK100ZQ020220124093123122384024',
		'100DK10010000001413660202105130512',
		'100DK10010000001248310202011250630',
		'100DK10010000001660000202111162228',
		'100DK10010000001248360202011250630',
		'100DK100ZQ020220214062205360488045',
		'100DK100ZQ020220124090759827858024',
		'100DK10010000001660410202111162228',
		'100DK100ZQ020220214062210619118045',
		'100DK100ZQ020220214062204402797045',
		'100DK100ZQ020220124093149555986024',
		'100DK100ZQ020220124093149319561024',
		'100DK10010000001248390202011250630',
		'100DK10010000001400780202105040816',
		'100DK10010000001222780202010280537',
		'100DK10010000001222770202010280537',
		'100DK10010000001222720202010280537',
		'100DK100ZQ020220124093148982275024',
		'100DK10010000001400700202105040816',
		'100DK10010000001397790202105040251',
		'100DK10010000001397940202105040251',
		'100DK100ZQ020220124093123601621024',
		'100DK10010000001326370202102090603',
		'100DK10010000001326510202102090603',
		'100DK10010000001222790202010280537',
		'100DK100ZQ020220124093131854524024',
		'100DK10010000001222840202010280537',
		'100DK10010000000818230201908020936',
		'100DK10010000000818120201908020935',
		'100DK100ZQ020220124093133398639024',
		'100DK100ZQ020220124093132423284024',
		'100DK10010000000821000201908050425',
		'100DK10010000000819000201908021008',
		'100DK10010000001222820202010280537'
		wrong delivery		Yes	Turki

		# solution
			# 

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@100DK100ZQ020220124093126046040024,2022-03-11@22:00:00@100DK100ZQ020220124090755315930024,2022-03-11@22:00:00@100DK100ZQ020220124090754985166024,2022-03-11@22:00:00@100DK100ZQ020220124090754656295024,2022-03-11@22:00:00@100DK100ZQ020220124090754819827024,2022-03-11@22:00:00@100DK100ZQ020220124090754027240024,2022-03-11@22:00:00@100DK100ZQ020220124090753733277024,2022-03-11@22:00:00@100DK100ZQ020220124093144653882024,2022-03-11@22:00:00@100DK10010000001324400202102090603,2022-03-11@22:00:00@100DK10010000001324440202102090603,2022-03-11@22:00:00@100DK10010000001659410202111162228,2022-03-11@22:00:00@100DK10010000000813290201908020245,2022-03-11@22:00:00@100DK10010000001248210202011250630,2022-03-11@22:00:00@100DK10010000001659390202111162228,2022-03-11@22:00:00@100DK100ZQ020220124093144395897024,2022-03-11@22:00:00@100DK100ZQ020220124093144115061024,2022-03-11@22:00:00@100DK10010000001659370202111162228,2022-03-11@22:00:00@100DK100ZQ020220124093147433839024,2022-03-11@22:00:00@100DK100ZQ020220124093147040174024,2022-03-11@22:00:00@100DK10010000001248290202011250630,2022-03-11@22:00:00@100DK10010000001248260202011250630,2022-03-11@22:00:00@100DK10010000001248230202011250630,2022-03-11@22:00:00@100DK100ZQ020220124093123122384024,2022-03-11@22:00:00@100DK10010000001413660202105130512,2022-03-11@22:00:00@100DK10010000001248310202011250630,2022-03-11@22:00:00@100DK10010000001660000202111162228,2022-03-11@22:00:00@100DK10010000001248360202011250630,2022-03-11@22:00:00@100DK100ZQ020220214062205360488045,2022-03-11@22:00:00@100DK100ZQ020220124090759827858024,2022-03-11@22:00:00@100DK10010000001660410202111162228,2022-03-11@22:00:00@100DK100ZQ020220214062210619118045,2022-03-11@22:00:00@100DK100ZQ020220214062204402797045,2022-03-11@22:00:00@100DK100ZQ020220124093149555986024,2022-03-11@22:00:00@100DK100ZQ020220124093149319561024,2022-03-11@22:00:00@100DK10010000001248390202011250630,2022-03-11@22:00:00@100DK10010000001400780202105040816,2022-03-11@22:00:00@100DK10010000001222780202010280537,2022-03-11@22:00:00@100DK10010000001222770202010280537,2022-03-11@22:00:00@100DK10010000001222720202010280537,2022-03-11@22:00:00@100DK100ZQ020220124093148982275024,2022-03-11@22:00:00@100DK10010000001400700202105040816,2022-03-11@22:00:00@100DK10010000001397790202105040251,2022-03-11@22:00:00@100DK10010000001397940202105040251,2022-03-11@22:00:00@100DK100ZQ020220124093123601621024,2022-03-11@22:00:00@100DK10010000001326370202102090603,2022-03-11@22:00:00@100DK10010000001326510202102090603,2022-03-11@22:00:00@100DK10010000001222790202010280537,2022-03-11@22:00:00@100DK100ZQ020220124093131854524024,2022-03-11@22:00:00@100DK10010000001222840202010280537,2022-03-11@22:00:00@100DK10010000000818230201908020936,2022-03-11@22:00:00@100DK10010000000818120201908020935,2022-03-11@22:00:00@100DK100ZQ020220124093133398639024,2022-03-11@22:00:00@100DK100ZQ020220124093132423284024,2022-03-11@22:00:00@100DK10010000000821000201908050425,2022-03-11@22:00:00@100DK10010000000819000201908021008,2022-03-11@22:00:00@100DK10010000001222820202010280537

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@100DK10010000001248290202011250630,2022-03-11@22:00:00@100DK10010000001397790202105040251,2022-03-11@22:00:00@100DK10010000001397940202105040251

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@100DK10010000001248290202011250630

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 3983	2022-03-11	New CMS	100	delivery	elgiganten.dk	
		100DK10010000001659670202111162228	
		spider issue -1 values		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-11' AND website = 'www.elgiganten.dk' AND item_id in ('100DK10010000001659670202111162228')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@100DK10010000001659670202111162228
			# rerun successful
			# done react, done sheet

	(R D-1) 3984	2022-03-11	New CMS	Q00	image_count	carlson.fi	
		Q00FIBW0IJ000001578390202109080312	
		incorrect image count		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata vap WHERE date = '2022-03-11' AND WEBSITE = 'www.carlson.fi' AND item_id in ('Q00FIBW0IJ000001578390202109080312')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@21:00:00@Q00FIBW0IJ000001578390202109080312

			# successful rerun
			# done react, done sheet

	(R D-1) 3985	2022-03-11	New CMS	200	all	heureka.cz	
		'200CZ9D080000000804600201907080640',
		'200CZ9D080000000550480201904050601',
		'200CZ9D070000000804260201907080347',
		'200CZ9D070000000550290201904050558'
		invalid deadlink		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-11' AND website = 'www.heureka.cz'	AND item_id in ('200CZ9D080000000804600201907080640',
			'200CZ9D080000000550480201904050601',
			'200CZ9D070000000804260201907080347',
			'200CZ9D070000000550290201904050558')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@200CZ9D080000000804600201907080640,2022-03-11@22:00:00@200CZ9D080000000550480201904050601,2022-03-11@22:00:00@200CZ9D070000000804260201907080347,2022-03-11@22:00:00@200CZ9D070000000550290201904050558

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@200CZ9D080000000550480201904050601

			# multiple reruns successful
			# done react, done sheet

	(W) 3988	2022-03-11	New CMS	A10	webshot_url		
		SELECT * FROM
			view_all_productdata where
			date ='2022-03-11' and
			webshot url is NULL	No Webshot	
		Yes	Have

		# python manual_cmp_webshots.py -c A10 -i 2022-03-11@22:00:00@A10PLIX000120210916032738136740259,2022-03-11@22:00:00@A10PLIX000120210916032738721261259,2022-03-11@22:00:00@A10PLIX000120210916032738946168259,2022-03-11@22:00:00@A10PLIX000120210916032739205405259,2022-03-11@22:00:00@A10PLIX000120210916032739818251259,2022-03-11@22:00:00@A10PLIX000120210916032741319110259,2022-03-11@22:00:00@A10PLIX000120210916032741503988259,2022-03-11@22:00:00@A10PLIX000120210916032741711135259,2022-03-11@22:00:00@A10PLIX000120210916032742158133259,2022-03-11@22:00:00@A10PLIX000120210916032743012517259,2022-03-11@22:00:00@A10PLIX000120210916032743605902259,2022-03-11@22:00:00@A10PLIX020120210916032744898341259,2022-03-11@22:00:00@A10PLHX020120210916032745273908259,2022-03-11@22:00:00@A10PLIX020120210916032745590912259,2022-03-11@22:00:00@A10PLIX010120210916032746682749259,2022-03-11@22:00:00@A10PLIX010120210916032748985513259,2022-03-11@22:00:00@A10PLIX010120210916032749398261259,2022-03-11@22:00:00@A10PLHX010120210916032749720727259,2022-03-11@22:00:00@A10PLIX010120210916032750865721259,2022-03-11@22:00:00@A10PLIX010120210916032752069827259,2022-03-11@22:00:00@A10PLIX010120210916032752222025259,2022-03-11@22:00:00@A10PLIX010120210916032752387636259,2022-03-11@22:00:00@A10PLIX010120210916032754549126259,2022-03-11@22:00:00@A10PLIX010120210916032755545532259,2022-03-11@22:00:00@A10PLIX010120210916032756145066259,2022-03-11@22:00:00@A10PLHX010120210916032757011060259,2022-03-11@22:00:00@A10PLIX030120210916032757601283259,2022-03-11@22:00:00@A10PLIX040120210916032758032021259,2022-03-11@22:00:00@A10PLIX050120210916032758367805259,2022-03-11@22:00:00@A10PLIX050120210916032758623319259,2022-03-11@22:00:00@A10PLIX060120210916032759108701259,2022-03-11@22:00:00@A10PLIX060120210916032759210213259,2022-03-11@22:00:00@A10PLIX060120210916032759886582259,2022-03-11@22:00:00@A10PLIX060120210916032800633343259,2022-03-11@22:00:00@A10PLIX060120210916032802157236259,2022-03-11@22:00:00@A10PLJX060120210916032803682416259,2022-03-11@22:00:00@A10PLJX060120210916032804082660259,2022-03-11@22:00:00@A10PLIX060120210916032804207670259,2022-03-11@22:00:00@A10PLIX060120210916032805884513259,2022-03-11@22:00:00@A10PLJX060120210916032806926091259,2022-03-11@22:00:00@A10PLIX060120210916032807078927259,2022-03-11@22:00:00@A10PLJX060120210916032808095833259,2022-03-11@22:00:00@A10PLIX060120210916032810572319259,2022-03-11@22:00:00@A10PLIX060120210916032810767708259,2022-03-11@22:00:00@A10PLIX060120210916032811120943259,2022-03-11@22:00:00@A10PLJX060120210916032811183728259,2022-03-11@22:00:00@A10PLIX060120210916032811289590259,2022-03-11@22:00:00@A10PLIX060120210916032811498746259,2022-03-11@22:00:00@A10PLIX060120210916032811873185259,2022-03-11@22:00:00@A10PLIX070120210916032812413633259,2022-03-11@22:00:00@A10PLIX040120210916032815647424259,2022-03-11@22:00:00@A10PLIX040120210916032817902078259,2022-03-11@22:00:00@A10PLJX050120210916032819355905259

		# remaining: 11
		# done react, done sheet

	(W) 3990	2022-03-11	New CMS	H10	webshot_url		
			select * from view_all_productdata where date='2022-03-11' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com') and webshot_url is NULL order by item_id

			No Webshots		
			Yes	Keeshia

			# solution
				# rerun executed
				# remaining 112
				# 30
				# reexecute rerun
				# done all

	(W) WEBSHOTS NA REGION (H10)
		# re webshots executed
		# done manual 9 items

	------
	(R D-1) 2611	2022-03-11	New CMS	Q00	all	www.sportsmagasinet.no	
		Vindjakke kvinner	
		Auto Copy over for 1 day	
		https://prnt.sc/MhR_uEHWKWYO	
		Yes	Flor

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.sportsmagasinet.no' 
				AND keyword in (N'Vindjakke kvinner')
				GROUP BY keyword, ritem_id, country, SOURCE	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@Q00@5630b012-80ce-453a-8eb0-a0ff98646760@15ae38d9-e3d8-476d-8870-76a5a46f8fc3
			# successful rerun
			# done react, done sheet

	(R D-1) 2612	2022-03-11	New CMS	Q00	all	www.outdoorxperten.dk	
		regnbukser dame	
		Auto copy over for 1 day	
		https://prnt.sc/vRqyZ1F_sEVs	
		Yes	renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.outdoorxperten.dk' 
				AND keyword in (N'regnbukser dame')
				GROUP BY keyword, ritem_id, country, SOURCE	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@Q00@be88aabd-33be-473a-9526-43df471caf09@b086e668-f791-45c2-8126-b7cc0f5e0733
			# rerun successful
			# done react, done sheet

	(R D-1) 2614	2022-03-11	New CMS	Q00	all	www.addnature.com	
		'Skalbyxor dam',
		'Skaljackor dam'

		Auto copy over for 1 day

		https://prnt.sc/Bxyc9gjdYWYT
		https://prnt.sc/Gk3hJAtqSfGS	
		Yes	renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.addnature.com' 
				AND keyword in (N'Skalbyxor dam',
						'Skaljackor dam')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@Q00@b2a7e921-5dfd-4dcd-afe2-b0efe510c64e@4852137c-3a49-42a2-b39e-dca60f54f8a8,2022-03-11@22:00:00@Q00@87b33842-97af-440f-ba6f-b14c50d431de@4852137c-3a49-42a2-b39e-dca60f54f8a8

			# rerun successful
			# done react, done sheet

	(R D-1) 2617	2022-03-11	New CMS	610	all	www.bagarenochkocken.se	
		karaff
		stekpannor
		1 day auto copy over
		https://prnt.sc/EAT38o4xPYL2
		https://prnt.sc/zmAwYsngl7-v
		Yes	Yev
		
		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.bagarenochkocken.se' 
				AND keyword in (N'karaff',
					'stekpannor')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@610@da4236d4-8788-439e-b4ec-e77cf373a8a6@fa8feddc-0176-46a8-a009-a493cd47be61,2022-03-11@22:00:00@610@ccbc00a2-d263-4f91-9bbe-67a7129678e5@fa8feddc-0176-46a8-a009-a493cd47be61

			# multiple reruns successful
			# done react, done sheets

	(R D-1) 2622	2022-03-11	New CMS	Q00	all	www.bergzeit.de
		Regenhosen männer	
			Kamik Kinder Muddy Hose
			https://prnt.sc/WwnUqEeWqCBb

			Vaude Herren Drop MTB Hose kurz
			https://prnt.sc/QRmSyrSzlIWH
		duplicate product_website		
		https://prnt.sc/hJDDi9hYknjI	
		Yes	KEEN

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bergzeit.de' and 
				var2.keyword in (N'Regenhosen männer') and 
				var2.[date] = '2022-03-11' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@Q00@bacd12f8-a731-4d49-9184-f29f60438b9a@bd8b190f-904c-4b92-aeb9-b1657b260972

	(R D-1) 2623	2022-03-11	New CMS	Q00	all	www.bergzeit.de	
		Shelljacken frauen	
		Auto Copy Over for 3 Days		
		https://prnt.sc/tngbES0-9xxf	
		Yes	KEEN

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.bergzeit.de' 
				AND keyword in (N'Shelljacken frauen')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-11@22:00:00@Q00@66e9e9f0-f30d-4f03-a1a3-dc856e9cba74@bd8b190f-904c-4b92-aeb9-b1657b260972
			# check local: 
			# valid deadlink: https://prnt.sc/bow7kniPiV7U
			# done react, done sheet
			# issue re raised: not valid deadlink 

	(R D-1) 2624	2022-03-11	New CMS	200	all	www.dns-shop.ru
		гарнитура
		Duplicate Product Website and product Url
		https://prnt.sc/GHrdNvhwnUqH
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'гарнитура') and 
				var2.[date] = '2022-03-11' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-11@21:00:00@200@bee38489-f39a-4402-9a45-4b7a61706873@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-1) 2625	2022-03-11	New CMS	200	all	www.technopark.ru
		портативная колонка с bluetooth
		Duplicate Product Website and product Url
		https://prnt.sc/UHUvUVYVYyvC
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.technopark.ru' and 
				var2.keyword in (N'портативная колонка с bluetooth') and 
				var2.[date] = '2022-03-11' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-11@21:00:00@200@464302c9-2bae-4430-9574-e3f1484e9b10@d0bb62ad-8919-4942-a84e-09748f674d32
			# rerun successful
			# done react, done sheet
	-----
	(F D-2) 890	2022-03-11	New CMS	200	product_website	www.fnac.com	
		'Headphones Bluetooth In Ear',
			https://www.fnac.com/Ecouteur-Bluetooth-sans-fil/Ecouteur-par-usage/nsh450543/w-4#bl=MMson
		'Headphones Bluetooth On Ear',
		'Headphones Noise Cancelling',
		'Speakers Bluetooth',
		'Speakers Multiroom'

		Invalid Product Title	
		https://prnt.sc/bUjXR59YivT6
		https://prnt.sc/5MLHNypVY1HS	
		Yes	Jairus

		# solution	
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.fnac.com' and 
				var2.category in (N'Headphones Bluetooth In Ear',
						'Headphones Bluetooth On Ear',
						'Headphones Noise Cancelling',
						'Speakers Bluetooth',
						'Speakers Multiroom') and 
				var2.[date] = '2022-03-11' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-11@22:00:00@200@210@94806edf-d0c0-4bbe-9ecb-00973b545392,2022-03-11@22:00:00@200@210@e6598c67-e6dd-4d9e-9ec1-074493e529c6,2022-03-11@22:00:00@200@210@de69106e-2556-4657-873c-7c4c62c870e9,2022-03-11@22:00:00@200@210@47f04f2e-3cbe-436b-83b8-a3a1c05da456,2022-03-11@22:00:00@200@210@f185f8c7-aa4f-4d26-8950-fed9a8c9bfbb

			# fixed. for rebuild
			# successful rerun
			# done react, done sheets

	---- performance stats ----
	commits:
		890
		890

	Stats:
		Rerun: 13 (16) = 16/13 = 
		Webshot: 3
		Fix: 1[1]

03 12 22
	0 (R D-1) Trello 1
		2623	2022-03-11	New CMS	Q00	all	www.bergzeit.de

		Issue Details: Auto Copy Over for 3 Days
		cause : need further investigation
		Date: 3 /11/2022
		Retailer: www.bergzeit.de
		Keyword(s):
		Shelljacken frauen
		Screenshot(s):
		https://prnt.sc/tngbES0-9xxf

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.bergzeit.de' 
				AND keyword in (N'Shelljacken frauen')
				GROUP BY keyword, ritem_id, country, source

			# Q00@Haglofs@11:00AM@10:30AM@Prod2

	-----
	(R D-1) 4000	2022-03-11	New CMS	P00	all	krefel.be/fr	
		'P00BESU01L000001420540202105180758',
		'P00BESU01L000001420500202105180758',
		'P00BESU01L000001420560202105180758'
		Spider Issue -1 Values		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.krefel.be/fr'	AND item_id in ('P00BESU01L000001420540202105180758',
			'P00BESU01L000001420500202105180758',
			'P00BESU01L000001420560202105180758')

			# successful rerun
			# done react, done sheet

	(R D-1) 4009	2022-03-12	New CMS	200	all	heureka.cz	
		200CZ9D070000000550290201904050558	
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.heureka.cz'	AND item_id in ('200CZ9D070000000550290201904050558')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200CZ9D070000000550290201904050558
			# rerun successful
			# done react, done sheet

	(R D-1) 4010	2022-03-12	New CMS	200	all	fnac.com/3P	
		'200FRDU0H0000001381180202104140811',
		'200FRDU070000001381360202104140811',
		'200FRDU0P0000001381540202104141007',
		'200FRDU0Q0000001381620202104141007',
		'200FRDU080000001381680202104141007',
		'200FRDU080000001381690202104141007'
		invalid deadlink		
		Yes	Louie
		
		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.fnac.com/3P'	AND item_id in ('200FRDU0H0000001381180202104140811',
				'200FRDU070000001381360202104140811',
				'200FRDU0P0000001381540202104141007',
				'200FRDU0Q0000001381620202104141007',
				'200FRDU080000001381680202104141007',
				'200FRDU080000001381690202104141007')			

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200FRDU0H0000001381180202104140811,2022-03-12@22:00:00@200FRDU070000001381360202104140811,2022-03-12@22:00:00@200FRDU0P0000001381540202104141007,2022-03-12@22:00:00@200FRDU0Q0000001381620202104141007,2022-03-12@22:00:00@200FRDU080000001381680202104141007,2022-03-12@22:00:00@200FRDU080000001381690202104141007

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200FRDU0H0000001381180202104140811,2022-03-12@22:00:00@200FRDU0Q0000001381620202104141007,2022-03-12@22:00:00@200FRDU080000001381690202104141007

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200FRDU0Q0000001381620202104141007,2022-03-12@22:00:00@200FRDU080000001381690202104141007

			# https://www.fnac.com/Enceinte-Bluetooth-Ultimate-Ears-MEGABOOM-Charcoal-Black/a7962569/w-4?omnsearchpos=11
			# https://www.fnac.com/Enceinte-portable-Urbanears-Ralis-Gris/a13522449/w-4#omnsearchpos=3

			# as per validator: no 3p data: valid deadlink
			# 

	(R D-1) 4011	2022-03-12	New CMS	200	all	amazon.es/3P	
		200ESFF080000000801720201906210132	
		invalid deadlink		
		Yes	Louie

		# solution	
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.amazon.es/3P'	AND item_id in ('200ESFF080000000801720201906210132')				

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200ESFF080000000801720201906210132

			# multiple reruns succesful
			# doen react, done sheet

	(R D-1) 4012	2022-03-12	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000557370201904220937',
		'200ITGF070000000557970201904221001',
		'200ITGF080000000556780201904220839'
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.amazon.it/3P'	AND item_id in ('200ITGF070000000557370201904220937',
			'200ITGF070000000557970201904221001',
			'200ITGF080000000556780201904220839')			

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200ITGF070000000557370201904220937,2022-03-12@22:00:00@200ITGF070000000557970201904221001,2022-03-12@22:00:00@200ITGF080000000556780201904220839

			# successful rerun
			# done react, done sheet

	(R D-1) 4013	2022-03-12	New CMS	200	all	elgiganten.dk	
		200DK10070000000104540201902071109	
		spider issue -1 values		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK10070000000104540201902071109')				

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200DK10070000000104540201902071109
			# rerun successful
			# done react, done sheet

	(R D-3) 4019	2022-03-12	New CMS	X00	all	walmart.com.mx	
		'X00MXO90OL000001421250202105190424',
		'X00MXO90OL000001339040202103010159',
		'X00MXO90OL000001475170202107060855',
		'X00MXO90OL000001339000202103010159',
		'X00MXO90OL000001338970202103010159',
		'X00MXO90SV020210920061125239827263',
		'X00MXO90SV020211207055751170362341',
		'X00MXO90OL000001421630202105190424'
		ACO site is up 		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.walmart.com.mx'	AND item_id in ('X00MXO90OL000001421250202105190424',
				'X00MXO90OL000001339040202103010159',
				'X00MXO90OL000001475170202107060855',
				'X00MXO90OL000001339000202103010159',
				'X00MXO90OL000001338970202103010159',
				'X00MXO90SV020210920061125239827263',
				'X00MXO90SV020211207055751170362341',
				'X00MXO90OL000001421630202105190424')

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90OL000001421250202105190424,2022-03-12@05:00:00@X00MXO90OL000001339040202103010159,2022-03-12@05:00:00@X00MXO90OL000001475170202107060855,2022-03-12@05:00:00@X00MXO90OL000001339000202103010159,2022-03-12@05:00:00@X00MXO90OL000001338970202103010159,2022-03-12@05:00:00@X00MXO90SV020210920061125239827263,2022-03-12@05:00:00@X00MXO90SV020211207055751170362341,2022-03-12@05:00:00@X00MXO90OL000001421630202105190424

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90SV020210920061125239827263,2022-03-12@05:00:00@X00MXO90OL000001339040202103010159,2022-03-12@05:00:00@X00MXO90OL000001421630202105190424,2022-03-12@05:00:00@X00MXO90SV020211207055751170362341,2022-03-12@05:00:00@X00MXO90OL000001338970202103010159,2022-03-12@05:00:00@X00MXO90OL000001339000202103010159

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90OL000001421630202105190424,2022-03-12@05:00:00@X00MXO90OL000001338970202103010159

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90OL000001338970202103010159

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90OL000001338970202103010159

			# multiple reruns
			# done react, done sheet

	(R D-2) 4020	2022-03-12	New CMS	100	delivery	elgiganten.dk	
		'100DK100ZQ020220124090753449368024',
		'100DK100ZQ020220124090754197223024',
		'100DK100ZQ020220124090754523897024',
		'100DK100ZQ020220124090754656295024',
		'100DK100ZQ020220124090754985166024',
		'100DK100ZQ020220124090755315930024',
		'100DK100ZQ020220124090754819827024',
		'100DK100ZQ020220124093126211631024',
		'100DK100ZQ020220124090756117378024',
		'100DK10010000001248210202011250630',
		'100DK10010000001324440202102090603',
		'100DK10010000001659370202111162228',
		'100DK10010000001659410202111162228',
		'100DK100ZQ020220124093144115061024',
		'100DK100ZQ020220124093144395897024',
		'100DK10010000001248230202011250630',
		'100DK10010000001438730202106080416',
		'100DK10010000001248250202011250630',
		'100DK10010000001659670202111162228',
		'100DK10010000001248290202011250630',
		'100DK100ZQ020220124093145806437024',
		'100DK100ZQ020220124093146643070024',
		'100DK100ZQ020220124093147433839024',
		'100DK10010000001659910202111162228',
		'100DK10010000001660000202111162228',
		'100DK100ZQ020220124093148982275024',
		'100DK100ZQ020220124093149319561024',
		'100DK100ZQ020220124093149555986024',
		'100DK10010000001400700202105040816',
		'100DK10010000001400740202105040816',
		'100DK10010000001400780202105040816',
		'100DK100ZQ020220214062204402797045',
		'100DK100ZQ020220214062205360488045',
		'100DK100ZQ020220214062210619118045',
		'100DK100ZQ020220214062211529844045',
		'100DK10010000001222720202010280537',
		'100DK10010000001324630202102090603',
		'100DK100ZQ020220124093124632102024',
		'100DK10010000001222770202010280537',
		'100DK10010000001222780202010280537',
		'100DK10010000001222790202010280537',
		'100DK10010000001326370202102090603',
		'100DK10010000001326440202102090603',
		'100DK10010000001222810202010280537',
		'100DK10010000001222840202010280537',
		'100DK100ZQ020220124093131854524024',
		'100DK100ZQ020220207085518362413038',
		'100DK10010000001222820202010280537',
		'100DK10010000000819000201908021008',
		'100DK10010000000821000201908050425'

		Incorrect scraped Delivery Quote (DQ)	
		https://prnt.sc/rNvFxRzayk0Y	
		Yes	Jeremy

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-11' AND website = 'www.elgiganten.dk' AND item_id in ('100DK100ZQ020220124090753449368024',
			'100DK100ZQ020220124090754197223024',
			'100DK100ZQ020220124090754523897024',
			'100DK100ZQ020220124090754656295024',
			'100DK100ZQ020220124090754985166024',
			'100DK100ZQ020220124090755315930024',
			'100DK100ZQ020220124090754819827024',
			'100DK100ZQ020220124093126211631024',
			'100DK100ZQ020220124090756117378024',
			'100DK10010000001248210202011250630',
			'100DK10010000001324440202102090603',
			'100DK10010000001659370202111162228',
			'100DK10010000001659410202111162228',
			'100DK100ZQ020220124093144115061024',
			'100DK100ZQ020220124093144395897024',
			'100DK10010000001248230202011250630',
			'100DK10010000001438730202106080416',
			'100DK10010000001248250202011250630',
			'100DK10010000001659670202111162228',
			'100DK10010000001248290202011250630',
			'100DK100ZQ020220124093145806437024',
			'100DK100ZQ020220124093146643070024',
			'100DK100ZQ020220124093147433839024',
			'100DK10010000001659910202111162228',
			'100DK10010000001660000202111162228',
			'100DK100ZQ020220124093148982275024',
			'100DK100ZQ020220124093149319561024',
			'100DK100ZQ020220124093149555986024',
			'100DK10010000001400700202105040816',
			'100DK10010000001400740202105040816',
			'100DK10010000001400780202105040816',
			'100DK100ZQ020220214062204402797045',
			'100DK100ZQ020220214062205360488045',
			'100DK100ZQ020220214062210619118045',
			'100DK100ZQ020220214062211529844045',
			'100DK10010000001222720202010280537',
			'100DK10010000001324630202102090603',
			'100DK100ZQ020220124093124632102024',
			'100DK10010000001222770202010280537',
			'100DK10010000001222780202010280537',
			'100DK10010000001222790202010280537',
			'100DK10010000001326370202102090603',
			'100DK10010000001326440202102090603',
			'100DK10010000001222810202010280537',
			'100DK10010000001222840202010280537',
			'100DK100ZQ020220124093131854524024',
			'100DK100ZQ020220207085518362413038',
			'100DK10010000001222820202010280537',
			'100DK10010000000819000201908021008',
			'100DK10010000000821000201908050425')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@100DK100ZQ020220124090753449368024,2022-03-12@22:00:00@100DK100ZQ020220124090754197223024,2022-03-12@22:00:00@100DK100ZQ020220124090754523897024,2022-03-12@22:00:00@100DK100ZQ020220124090754656295024,2022-03-12@22:00:00@100DK100ZQ020220124090754985166024,2022-03-12@22:00:00@100DK100ZQ020220124090755315930024,2022-03-12@22:00:00@100DK100ZQ020220124090754819827024,2022-03-12@22:00:00@100DK100ZQ020220124093126211631024,2022-03-12@22:00:00@100DK100ZQ020220124090756117378024,2022-03-12@22:00:00@100DK10010000001248210202011250630,2022-03-12@22:00:00@100DK10010000001324440202102090603,2022-03-12@22:00:00@100DK10010000001659370202111162228,2022-03-12@22:00:00@100DK10010000001659410202111162228,2022-03-12@22:00:00@100DK100ZQ020220124093144115061024,2022-03-12@22:00:00@100DK100ZQ020220124093144395897024,2022-03-12@22:00:00@100DK10010000001248230202011250630,2022-03-12@22:00:00@100DK10010000001438730202106080416,2022-03-12@22:00:00@100DK10010000001248250202011250630,2022-03-12@22:00:00@100DK10010000001659670202111162228,2022-03-12@22:00:00@100DK10010000001248290202011250630,2022-03-12@22:00:00@100DK100ZQ020220124093145806437024,2022-03-12@22:00:00@100DK100ZQ020220124093146643070024,2022-03-12@22:00:00@100DK100ZQ020220124093147433839024,2022-03-12@22:00:00@100DK10010000001659910202111162228,2022-03-12@22:00:00@100DK10010000001660000202111162228,2022-03-12@22:00:00@100DK100ZQ020220124093148982275024,2022-03-12@22:00:00@100DK100ZQ020220124093149319561024,2022-03-12@22:00:00@100DK100ZQ020220124093149555986024,2022-03-12@22:00:00@100DK10010000001400700202105040816,2022-03-12@22:00:00@100DK10010000001400740202105040816,2022-03-12@22:00:00@100DK10010000001400780202105040816,2022-03-12@22:00:00@100DK100ZQ020220214062204402797045,2022-03-12@22:00:00@100DK100ZQ020220214062205360488045,2022-03-12@22:00:00@100DK100ZQ020220214062210619118045,2022-03-12@22:00:00@100DK100ZQ020220214062211529844045,2022-03-12@22:00:00@100DK10010000001222720202010280537,2022-03-12@22:00:00@100DK10010000001324630202102090603,2022-03-12@22:00:00@100DK100ZQ020220124093124632102024,2022-03-12@22:00:00@100DK10010000001222770202010280537,2022-03-12@22:00:00@100DK10010000001222780202010280537,2022-03-12@22:00:00@100DK10010000001222790202010280537,2022-03-12@22:00:00@100DK10010000001326370202102090603,2022-03-12@22:00:00@100DK10010000001326440202102090603,2022-03-12@22:00:00@100DK10010000001222810202010280537,2022-03-12@22:00:00@100DK10010000001222840202010280537,2022-03-12@22:00:00@100DK100ZQ020220124093131854524024,2022-03-12@22:00:00@100DK100ZQ020220207085518362413038,2022-03-12@22:00:00@100DK10010000001222820202010280537,2022-03-12@22:00:00@100DK10010000000819000201908021008,2022-03-12@22:00:00@100DK10010000000821000201908050425

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@100DK10010000001222810202010280537,2022-03-12@22:00:00@100DK10010000001248250202011250630,2022-03-12@22:00:00@100DK10010000001324630202102090603,2022-03-12@22:00:00@100DK10010000001326440202102090603,2022-03-12@22:00:00@100DK10010000001400740202105040816,2022-03-12@22:00:00@100DK10010000001438730202106080416,2022-03-12@22:00:00@100DK10010000001659910202111162228,2022-03-12@22:00:00@100DK100ZQ020220124090753449368024,2022-03-12@22:00:00@100DK100ZQ020220124090754197223024,2022-03-12@22:00:00@100DK100ZQ020220124090754523897024,2022-03-12@22:00:00@100DK100ZQ020220124090756117378024,2022-03-12@22:00:00@100DK100ZQ020220124093124632102024,2022-03-12@22:00:00@100DK100ZQ020220124093126211631024,2022-03-12@22:00:00@100DK100ZQ020220124093145806437024,2022-03-12@22:00:00@100DK100ZQ020220124093146643070024,2022-03-12@22:00:00@100DK100ZQ020220207085518362413038,2022-03-12@22:00:00@100DK100ZQ020220214062211529844045

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@100DK10010000001222810202010280537,2022-03-12@22:00:00@100DK10010000001248250202011250630,2022-03-12@22:00:00@100DK10010000001324630202102090603,2022-03-12@22:00:00@100DK10010000001326440202102090603,2022-03-12@22:00:00@100DK10010000001400740202105040816,2022-03-12@22:00:00@100DK10010000001438730202106080416,2022-03-12@22:00:00@100DK10010000001659910202111162228,2022-03-12@22:00:00@100DK100ZQ020220124090753449368024,2022-03-12@22:00:00@100DK100ZQ020220124090754197223024,2022-03-12@22:00:00@100DK100ZQ020220124090754523897024,2022-03-12@22:00:00@100DK100ZQ020220124090756117378024,2022-03-12@22:00:00@100DK100ZQ020220124093124632102024,2022-03-12@22:00:00@100DK100ZQ020220124093126211631024,2022-03-12@22:00:00@100DK100ZQ020220124093145806437024,2022-03-12@22:00:00@100DK100ZQ020220124093146643070024,2022-03-12@22:00:00@100DK100ZQ020220207085518362413038,2022-03-12@22:00:00@100DK100ZQ020220214062211529844045

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@100DK10010000001248230202011250630

			# multiple reruns failed
			# done react, done sheet

	(R D-1) 4021	2022-03-12	New CMS	X00	all	walmart.com.mx	
		X00MXO90OL000001421790202105190424	
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-12' AND website = 'www.walmart.com.mx'	AND item_id in ('X00MXO90OL000001421790202105190424')

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00MXO90OL000001421790202105190424
			# multiple reruns successful
			# done react, done sheet

	-----

	(R D-1) 2632	2022-03-11	New CMS	110	all	www.toychamp.be	
		spelcomputer	
		1 Day Auto Copy Over	
		https://prnt.sc/RJJnJ4bfbjAf	Yes	Rod

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.toychamp.be' 
				AND keyword in (N'spelcomputer')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@22:00:00@110@e772bc6b-f949-4561-a365-1de2e82b0bef@bca263e9-2da1-4fff-b403-d93788a79674
			# rerun successful

	(R D-1) 2633	2022-03-11	New CMS	110	all	www.bol.com	
		spelcomputer	
		Data Count Mismatch 2 (Fetched Data is Greater than expected)	
		https://prnt.sc/U5UfUoLT4Dgu	
		Yes	Flor

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.bol.com' 
				AND keyword in (N'spelcomputer')
				GROUP BY keyword, ritem_id, country, source		

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@110@e772bc6b-f949-4561-a365-1de2e82b0bef@UAjszpOqEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2634	2022-03-12	New CMS	200	all	www.dns-shop.ru	
		наушники внутриканальные беспроводные bluetooth	
		invalid deadlink	
		https://prnt.sc/vGn2AatQPbkQ	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники внутриканальные беспроводные bluetooth') and 
				var2.[date] = '2022-03-12' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@21:00:00@200@0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done rect, done sheet

	(R D-1) 2635	2022-03-12	New CMS	200	all	www.amazon.co.uk	
		wireless in ear headphones	
		Duplicate Product Website and product Url
		https://prnt.sc/eHzXuAeNsMQh
		https://prnt.sc/NB2-a4TTV4LO	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.amazon.co.uk' and 
				var2.keyword in (N'wireless in ear headphones') and 
				var2.[date] = '2022-03-12' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@23:00:00@200@36d33801-55e7-4a22-b1e8-54042429caa8@rolsig6iEeeOEgANOrFolw
			# rerun successful
			# done react, done sheet

	(R D-1) 2636	2022-03-12	New CMS	200	all	www.mediamarkt.de	
		kopfhörer in ear	
		Duplicate Product Website and product Url
		https://prnt.sc/Or
		4MkJjBwqz2	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.mediamarkt.de' and 
				var2.keyword in (N'kopfhörer in ear') and 
				var2.[date] = '2022-03-12' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source		

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200@dcb7c03b-0163-462e-b76c-f19e407fe3c0@M3YPUApIEem-gAANOiOHiA
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2637	2022-03-12	New CMS	200	all	www.bol.com	
		hoofdtelefoon
			94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA
		spatwaterdichte speaker	
			069deba5-45a9-4199-8edb-302967c084c1@UAjszpOqEei-eQANOiOHiA

		Duplicate Product Website and product Url
		https://prnt.sc/BapYpJhibT42
		https://prnt.sc/Wu2tA9Ha5kE8	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'hoofdtelefoon') and 
				var2.[date] = '2022-03-12' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200@94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA,2022-03-12@22:00:00@200@069deba5-45a9-4199-8edb-302967c084c1@UAjszpOqEei-eQANOiOHiA

			# successful rerun
			# done react, done sheet

	(R D-1) 2638	2022-03-12	New CMS	200	all	www.dns-shop.ru	
		наушники накладные беспроводные с bluetooth	
		Duplicate Product Website and product Url
		https://prnt.sc/kJIKg7oEIufR	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники накладные беспроводные с bluetooth') and 
				var2.[date] = '2022-03-12' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@21:00:00@200@e4842b4e-bd1e-4aaa-8054-33faad7516b6@BzxplkXeEeeMawANOiZi-g
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@21:00:00@200@e4842b4e-bd1e-4aaa-8054-33faad7516b6@BzxplkXeEeeMawANOiZi-g
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2639	2022-03-12	New CMS	200	all	www.darty.com	
		casque audio reducteur de bruit	- 11
		Data Count Mismatch 10 (Fetched Data is Lesser than expected)	
		https://prnt.sc/Iu9lPv6gHU6P	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.darty.com' 
				AND keyword in (N'casque audio reducteur de bruit')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@ds73aApHEem-gAANOiOHiA
			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2640	2022-03-12	New CMS	200	all	www.fnac.com	
		casque audio reducteur de bruit - 25
			6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA
		casque bluetooth reducteur de bruit - 24
			0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 4 (Fetched Data is Lesser than expected)

		https://prnt.sc/yZl4-y7B7W95
		https://prnt.sc/vAnA3hKKe4Ix	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio reducteur de bruit')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-12@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-12@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2641	2022-03-12	New CMS	U00	all	www.americanas.com.br	
		aparelho de video game - 25
			55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g
		pro controller	- 25
			afd31eea-4072-46c6-85c5-33921ae1871a@mFIWHkasEeeMawANOiZi-g

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	

		https://prnt.sc/jEgvSNcE0Y7c
		https://prnt.sc/lOFLQucV0BDk	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.americanas.com.br' 
				AND keyword in (N'pro controller')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-12@02:00:00@U00@55c4e3bb-f6ee-4208-ab1e-3b1ddc13d0d3@mFIWHkasEeeMawANOiZi-g,2022-03-12@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@mFIWHkasEeeMawANOiZi-g

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2642	2022-03-12	New CMS	U00	all	www.magazineluiza.com.br	
		console nintendo switch	
		Auto Copy Over 1 Day	
		https://prnt.sc/8uQiBJ4Mxcqc	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-12' 
				AND website = 'www.magazineluiza.com.br' 
				AND keyword in (N'console nintendo switch')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-12@02:00:00@U00@003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@G3u21LZqEeidIgANOiZi-g
			# rerun successful
			# done react, done sheet


	-----
	(R D-1) 897	2022-03-12	New CMS	X00	all	www.mercadolibre.com.mx	
		Accesorios	
		Auto Copy Over for 1 Day	
		https://prnt.sc/MtFUtNdxBlFQ	
		Yes	Kristian

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-12' 
				AND website = 'www.mercadolibre.com.mx' 
				AND category in (N'Accesorios')
				GROUP BY category, litem_id, country, category_url, source	

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-12@05:00:00@X00@EV0@09232fc0-b357-43bd-bdb4-acac11c154c2
			# successful rerun
			# done react, done sheet

	----- stats -----
	Stats
		Rerun: 22 (26)

03 13 22
	(F D-3) Trello 1
		2623	2022-03-11	New CMS	Q00	all	www.bergzeit.de

		Issue Details: Auto Copy Over for 3 Days
		cause : need further investigation
		Date: 3 /11/2022
		Retailer: www.bergzeit.de
		Keyword(s):
		Shelljacken frauen
		Screenshot(s):
		https://prnt.sc/tngbES0-9xxf
		# https://trello.com/c/GS4cKo9H/3454-haglofs-rankings-auto-copy-over-in-wwwbergzeitde

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-11' 
				AND website = 'www.bergzeit.de' 
				AND keyword in (N'Shelljacken frauen')
				GROUP BY keyword, ritem_id, country, source

			# Q00@Haglofs@11:00AM@10:30AM@Prod2

			# search:
				# bergzeit merino socken
				
			# fix
				# data extraction
				# oop and cleanup
				# move to rebuild
				# * test after rebuild: cant test due to copy override
					# * moved to for retest instead

	0 (R D-1) Trello 2
		[Samsung CE] Compliance: Incomplete Specifications in www.expert.nl
		Date: 2022-03-10
		Issue: incomplete specifications in expert.nl
		Retailer: www.expert.nl
		Item IDs: All
		# https://trello.com/c/1mHPwR9S/3445-samsung-ce-compliance-incomplete-specifications-in-wwwexpertnl

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-10' AND website = 'www.expert.nl' 
			# investigate, duplicate keys
			# https://prnt.sc/Es0_l4W4j6uC

	(F D-2) Trello 3
		[SONOS] Rankings: Auto Copy Over in www.costco.ca
		Date: March 11, ,2022
		Issue description: Auto Copy Over for 3 days
		Retailer: www.costco.ca

		Keywords:
		alexa
			2022-03-11@04:00:00@Z00@9224e01c-1747-4192-b1da-275c02b60406@IMq4iEauEeeMawANOiZi-g
		amp
			2022-03-11@04:00:00@Z006dc581b7-a9b8-4e7f-a198-270a8593c35e@IMq4iEauEeeMawANOiZi-g
		amplifier
		Bluetooth Speaker
		home theater receiver
		home theater system
		portable bluetooth speaker
		portable speaker
		receiver
		smart speaker
		Sound bar
		Soundbar
		soundbars
		Speaker
		speakers bluetooth wireless
		stereo receiver
		surround sound
		wireless speaker

		Screenshots:
		https://prnt.sc/WWzzBTRf2S-f
		https://prnt.sc/6E0mto1VQoCL
		https://prnt.sc/3riRkkyM9Zbz
		https://prnt.sc/Lilu5u0tS1u5
		https://prnt.sc/tHO6F33agma9
		https://prnt.sc/oQvxGnI1dFk-
		https://prnt.sc/w68lJ675l0Xk
		# https://trello.com/c/RDeNL07V/3452-sonos-rankings-auto-copy-over-in-wwwcostcoca

		# solution
			# * move ticket > in progress > to rebuild
			* test after rebuild
			# moved to for retest though not tested

	(F D-2) Trello 4
		[SONOS] Listings: 3 Days Auto Copy Over in www.costco.ca
		Issues: Site is under maintenance
		Date: 3/11/2022
		Retailer: www.costco.ca

		Categories:
		Home theater - Soundbars
		Speaker - Wireless Audio

		Listings UUID:
		5728ee70-a700-443c-9e0d-99974ce6cac7
		4b5fd34a-99f4-4644-b11c-4cb4fb4decf4

		Screenshots:
		https://prnt.sc/FMX_GaWp7pa3
		# https://trello.com/c/POc17WN1/3453-sonos-listings-3-days-auto-copy-over-in-wwwcostcoca

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-11' AND website = 'www.costco.ca' AND category in ('Home theater - Soundbars')

			# https://www.costco.ca/home-theatre.html
			# * move ticket > in progress > to rebuild
			# moved to for retest though not tested

	-----
	(R D-1) 4031	2022-03-13	New CMS	P00	all	intersport.no	
		'100NOGI010000001648990202111090811',
		'100NOGI010000001649000202111090811'
		Spider Issue		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.intersport.no' AND item_id in ('100NOGI010000001648990202111090811',
			'100NOGI010000001649000202111090811')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@100NOGI010000001648990202111090811,2022-03-13@22:00:00@100NOGI010000001649000202111090811

			# done sheet, done react

	(R D-1) 4035	2022-03-13	New CMS	200	all	heureka.cz	
		'200CZ9D080000000804630201907080644',
		'200CZ9D070000000804320201907080402',
		'200CZ9D080000000804570201907080636',
		'200CZ9D080000000804580201907080637'
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.heureka.cz'	AND item_id in ('200CZ9D080000000804630201907080644',
			'200CZ9D070000000804320201907080402',
			'200CZ9D080000000804570201907080636',
			'200CZ9D080000000804580201907080637')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200CZ9D080000000804630201907080644,2022-03-13@22:00:00@200CZ9D070000000804320201907080402,2022-03-13@22:00:00@200CZ9D080000000804570201907080636,2022-03-13@22:00:00@200CZ9D080000000804580201907080637

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200CZ9D080000000804630201907080644,2022-03-13@22:00:00@200CZ9D080000000804570201907080636

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 4036	2022-03-13	New CMS	200	all	bol.com	
			200NLN10D0000000908890202001291001	
			incorrect ratings and review		
			Yes	Louie

			# solution
				# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.bol.com'	AND item_id in ('200NLN10D0000000908890202001291001')	

				# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200NLN10D0000000908890202001291001

				# rerun successful
				# done react, done sheet

	(R D-1) 4037	2022-03-13	New CMS	200	all	dns-shop.ru	
		200RUL80P0000000907220202001290728	
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.dns-shop.ru'	AND item_id in ('200RUL80P0000000907220202001290728')	

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-13@21:00:00@200RUL80P0000000907220202001290728
			# multiple reruns successful
			# done react, done sheets

	(R D-1) 4038	2022-03-13	New CMS	200	all	elgiganten.dk	
		200DK100O0000000036230201901180329	
		spider issue -1 values		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK100O0000000036230201901180329')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200DK100O0000000036230201901180329

			# rerun successful
			# done react, done sheet

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200DK100P0000000031760201901180326
			# rerun successful
			# already reacted and commented on sheet

	(R D-1) 4039	2022-03-13	New CMS	200	price_value	elgiganten.dk	
		200DK100P0000000031760201901180326	
		wrong price		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-13' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK100P0000000031760201901180326')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200DK100P0000000031760201901180326
			# successful rerun
			# done react, done sheet

	-----
	(R D-1) 2647	2022-03-13	New CMS	200	all	www.dns-shop.ru	
		портативная акустика	
		invalid deadlink	
		https://prnt.sc/vX6pWSSTDj8R	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-13' 
			AND website = 'www.dns-shop.ru' 
			AND keyword in (N'портативная акустика')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-13@21:00:00@200@eeda0758-a7e5-4f73-a323-3b8016de133d@BzxplkXeEeeMawANOiZi-g
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2648	2022-03-13	New CMS	200	all	www.dns-shop.ru	
		беспроводная акустика	
		Duplicate Product Website and product Url
		https://prnt.sc/KpT2_JJoOCX0	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'беспроводная акустика') and 
				var2.[date] = '2022-03-13' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-13@21:00:00@200@56587abe-ec6a-4464-b129-8218839eebce@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react and sheet

	(R D-1) 2649	2022-03-13	New CMS	200	all	www.darty.com	
		'casque bluetooth reducteur de bruit',
			# https://prnt.sc/ABUeEhfG6VMG
		'enceinte bluetooth portable',
		'mini enceinte bluetooth',
		'mini-enceinte'

		Data Count Mismatch 9 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/LxuC9ZDSFhqg
		https://prnt.sc/cFrI-3vdp5X7
		https://prnt.sc/vyUxYy7LTrvE
		https://prnt.sc/jQjFZ_rrttD1	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-13' 
				AND website = 'www.darty.com' 
				AND keyword in (N'casque bluetooth reducteur de bruit',
						'enceinte bluetooth portable',
						'mini enceinte bluetooth',
						'mini-enceinte')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@ds73aApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@51405791-5c01-4b1a-9f24-1e5f24dd5bbf@ds73aApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@4c50f1e9-6a50-42ba-96ab-ff8cf9d8ce9a@ds73aApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@ds73aApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2650	2022-03-13	New CMS	200	all	www.bol.com	
		actief noice cancelling hoofdtelefoon
			https://prnt.sc/cZ9fsb_P_tZy	
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	
		https://prnt.sc/2OFNdVz1qUXq	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-13' 
				AND website = 'www.bol.com' 
				AND keyword in (N'actief noice cancelling hoofdtelefoon')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200@414eef51-ec0e-413c-bd65-2108945fc3d4@UAjszpOqEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2651	2022-03-13	New CMS	200	all	www.fnac.com	
		'enceinte bluetooth portable', - 25
		'mini enceinte bluetooth', - 25
		'mini-enceinte' - 25

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/cFrI-3vdp5X7
		https://prnt.sc/vyUxYy7LTrvE
		https://prnt.sc/jQjFZ_rrttD1	

		Yes	Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200@51405791-5c01-4b1a-9f24-1e5f24dd5bbf@LzP6XApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@4c50f1e9-6a50-42ba-96ab-ff8cf9d8ce9a@LzP6XApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-13@22:00:00@200@51405791-5c01-4b1a-9f24-1e5f24dd5bbf@LzP6XApHEem-gAANOiOHiA,2022-03-13@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-13@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-13@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@LzP6XApHEem-gAANOiOHiA
			# multiple reruns
			# done react, done sheet

	----- stats ------
	commits:
		(2 commits) https://trello.com/c/GS4cKo9H/3454-haglofs-rankings-auto-copy-over-in-wwwbergzeitde
		(3 commits) https://trello.com/c/RDeNL07V/3452-sonos-rankings-auto-copy-over-in-wwwcostcoca
		(1 commit) https://trello.com/c/POc17WN1/3453-sonos-listings-3-days-auto-copy-over-in-wwwcostcoca

	trello without fix
		(investigate and comment) https://trello.com/c/1mHPwR9S/3445-samsung-ce-compliance-incomplete-specifications-in-wwwexpertnl

	Stats:
		Rerun: 12 (12/12) = 1
		Fix: 3[2.33]

03 14 22
	[MOTOROLA] Listings: 2 Days Consecutive Auto Copy Over in www.telekom.sk
	Issue Description: data is now on API as per BEs investigation

	Retailer: www.telekom.sk

	Category:
	All smartphones
		https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806
	5G Smartphones
		https://eshop.telekom.sk/category/all-devices/list/product_listing?filter%5B%5D=filter.device_category%5B%5D=5g&currentPage=2&tariffId=RP1140&itemPerPage=16&bp=acquisition

	Listings UUID:
	d81ade30-369d-4675-93d9-380cbe0912a8
	1b81bdeb-d684-4a50-aca1-df4b70bb538d

	Screenshot:
	https://prnt.sc/26mqpgt

	Date:
	Feb 1, 2022

	# solution
		# SELECT * FROM view_all_listings_data WHERE date = '2022-03-14' AND website = 'www.telekom.sk' AND category in ('All smartphones')
		# 2022-03-14@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

		# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

03 16 22
	(F D-1) Trello 1
		[MOTOROLA] Listings: 2 Days Consecutive Auto Copy Over in www.telekom.sk
		Issue Description: data is now on API as per BEs investigation
		# https://trello.com/c/izEDNfUk/3197-motorola-listings-2-days-consecutive-auto-copy-over-in-wwwtelekomsk

		Retailer: www.telekom.sk

		Category:
		All smartphones
			https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806
		5G Smartphones
			https://eshop.telekom.sk/category/all-devices/list/product_listing?filter%5B%5D=filter.device_category%5B%5D=5g&currentPage=2&tariffId=RP1140&itemPerPage=16&bp=acquisition

		Listings UUID:
		d81ade30-369d-4675-93d9-380cbe0912a8
		1b81bdeb-d684-4a50-aca1-df4b70bb538d

		Screenshot:
		https://prnt.sc/26mqpgt

		Date:
		Feb 1, 2022

		# solution (03-15-22)
			SELECT * FROM view_all_listings_data WHERE date = '2022-03-15' AND website = 'www.telekom.sk' AND category in ('All smartphones')
			2022-03-14@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-15@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

		# solution (03-16-22)
			# raised to BE team, under investigation
			# to observe on friday; update to sir Jon

	(R D-1) 4111	2022-03-16	New CMS	K00	all	power.fi	
		'K00FI0X0CG000001487890202107130336',
		'K00FI0X0CG000001487940202107130336'	
		missing description, specifications and image_count		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-16' AND website = 'www.power.fi'	AND item_id in ('K00FI0X0CG000001487890202107130336',
			'K00FI0X0CG000001487940202107130336')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-16@21:00:00@K00FI0X0CG000001487890202107130336,2022-03-16@21:00:00@K00FI0X0CG000001487940202107130336

			# multiple reruns successful
			# done react, done sheet
	
	(R D-1) 4113	2022-03-16	New CMS	K00	specifications	power.fi	
		'K00FI0X0CG000001488010202107130336',
		'K00FI0X0CG000001488520202107130336',
		'K00FI0X0CG000001488340202107130336',
		'K00FI0X0CG000001488290202107130336',
		'K00FI0X0CG000001488330202107130336',
		'K00FI0X07U020211116100230655676320',
		'K00FI0X07U020220222101541696674053'
		missing specifications		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-16' AND website = 'www.power.fi'	AND item_id in ('K00FI0X0CG000001488010202107130336',
			'K00FI0X0CG000001488520202107130336',
			'K00FI0X0CG000001488340202107130336',
			'K00FI0X0CG000001488290202107130336',
			'K00FI0X0CG000001488330202107130336',
			'K00FI0X07U020211116100230655676320',
			'K00FI0X07U020220222101541696674053')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-16@21:00:00@K00FI0X0CG000001488010202107130336,2022-03-16@21:00:00@K00FI0X0CG000001488520202107130336,2022-03-16@21:00:00@K00FI0X0CG000001488340202107130336,2022-03-16@21:00:00@K00FI0X0CG000001488290202107130336,2022-03-16@21:00:00@K00FI0X0CG000001488330202107130336,2022-03-16@21:00:00@K00FI0X07U020211116100230655676320,2022-03-16@21:00:00@K00FI0X07U020220222101541696674053

			# rerun successful
			# done react, done sheet

	(F D-2) 4146	2022-03-16	New CMS	K00	description	fnac.com	
		'K00FR2103F000001306840202101210448'
			https://www.fnac.com/Webcam-Logitech-C922-Pro-Stream-Noir/a10122817/w-4
		'K00FR2105K000001540140202108110154',
			https://jeux-video.fnac.com/STEELSERIES-APEX-PRO-KEYBOARD-AZERTY-FR/a13653141/w-4#omnsearchpos=1
		missing description		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-16' AND website = 'www.fnac.com'	AND item_id in ('K00FR2105K000001540140202108110154',
			'K00FR2103F000001306840202101210448')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00FR2103F000001306840202101210448
				# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00FR2103F000001306840202101210448
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00FR2105K000001540140202108110154

			# for trello

			# done fixed: pushed, rebuild

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00FR2103F000001306840202101210448
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00FR2105K000001540140202108110154

			# multiple reruns successful
			# done react, done sheet


	(R D-1) 4148	2022-03-16	New CMS	H10	all	lowes.com	
		select * from view_all_productdata where date='2022-03-16' and website='www.lowes.com'and source='auto_copy_over' order by item_id	

		Auto copy Over (site is up). Request INTERVAL RERUN. 121 total item_id affected.		
		Yes	Maryeeel

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-16@04:00:00@H10US8X0Q8220220202031744882509033,2022-03-16@04:00:00@H10US8X0Q8220220202031745859265033,2022-03-16@04:00:00@H10US8X0Q8220220202031746367072033,2022-03-16@04:00:00@H10US8X0Q8220220202031746746114033,2022-03-16@04:00:00@H10US8X0Q8220220202031746931721033,2022-03-16@04:00:00@H10US8X0Q8220220202031747311859033,2022-03-16@04:00:00@H10US8X0Q8220220202031749118826033,2022-03-16@04:00:00@H10US8X0Q8220220202031749305699033,2022-03-16@04:00:00@H10US8X0Q8220220202031749471186033,2022-03-16@04:00:00@H10US8X0Q8220220202031749672778033,2022-03-16@04:00:00@H10US8X0Q8220220202031749994610033,2022-03-16@04:00:00@H10US8X0Q8220220202031750436066033,2022-03-16@04:00:00@H10US8X0Q8220220202031751254623033,2022-03-16@04:00:00@H10US8X0Q8220220202031751432976033,2022-03-16@04:00:00@H10US8X0Q8220220202031751726674033,2022-03-16@04:00:00@H10US8X0Q8220220202031752077696033,2022-03-16@04:00:00@H10US8X0Q8220220202031752825772033,2022-03-16@04:00:00@H10US8X0Q8220220202031754604859033,2022-03-16@04:00:00@H10US8X0Q8220220202031754896255033,2022-03-16@04:00:00@H10US8X0Q8220220202031757483480033,2022-03-16@04:00:00@H10US8X0Q8220220202031757684834033,2022-03-16@04:00:00@H10US8X0Q8220220202031758173393033,2022-03-16@04:00:00@H10US8X0Q8220220202031758325498033,2022-03-16@04:00:00@H10US8X0Q8220220202031758795043033,2022-03-16@04:00:00@H10US8X0Q8220220202031759267922033,2022-03-16@04:00:00@H10US8X0Q8220220202031759950850033,2022-03-16@04:00:00@H10US8X0Q8220220202031800151861033,2022-03-16@04:00:00@H10US8X0Q8220220202031800353083033,2022-03-16@04:00:00@H10US8X0Q8220220202031800850223033,2022-03-16@04:00:00@H10US8X0Q8220220202031801393708033,2022-03-16@04:00:00@H10US8X0Q8220220202031802057918033,2022-03-16@04:00:00@H10US8X0Q8220220202031802560178033,2022-03-16@04:00:00@H10US8X0Q8220220202031803308187033,2022-03-16@04:00:00@H10US8X0Q8220220202031803811021033,2022-03-16@04:00:00@H10US8X0Q8220220202031803963256033,2022-03-16@04:00:00@H10US8X0Q8220220202031804313920033,2022-03-16@04:00:00@H10US8X0Q8220220202031805198164033,2022-03-16@04:00:00@H10US8X0Q8220220202031805529771033,2022-03-16@04:00:00@H10US8X0Q8220220202031805907282033,2022-03-16@04:00:00@H10US8X0Q8220220202031806230830033,2022-03-16@04:00:00@H10US8X0Q8220220202031806394955033,2022-03-16@04:00:00@H10US8X0Q8220220202031806860201033,2022-03-16@04:00:00@H10US8X0Q8220220202031807191734033,2022-03-16@04:00:00@H10US8X0Q8220220202031808870898033,2022-03-16@04:00:00@H10US8X0Q8220220202031809034459033,2022-03-16@04:00:00@H10US8X0Q8220220202031810263753033,2022-03-16@04:00:00@H10US8X0Q8220220202031810759523033,2022-03-16@04:00:00@H10US8X0Q8220220202031811258237033,2022-03-16@04:00:00@H10US8X0Q8220220202031811905440033,2022-03-16@04:00:00@H10US8X0Q8220220202031812229847033,2022-03-16@04:00:00@H10US8X0Q8220220202031812383184033,2022-03-16@04:00:00@H10US8X0Q8220220202031812689830033,2022-03-16@04:00:00@H10US8X0Q8220220202031813261501033,2022-03-16@04:00:00@H10US8X0Q8220220202031813420837033,2022-03-16@04:00:00@H10US8X0Q8220220202031814375260033,2022-03-16@04:00:00@H10US8X0Q8220220202031814540852033,2022-03-16@04:00:00@H10US8X0Q8220220202031815036068033,2022-03-16@04:00:00@H10US8X0Q8220220202031815330592033

			# successful rerun

			# done react, done sheet

	(R D-1) 4149	2022-03-16	New CMS	X00	webshot_url		
		select * from view_all_productdata where date = '2022-03-16' and webshot_url is NULL	
		missing webshots
		Yes	Caesa

		# solution
			# python manual_cmp_webshots.py -c X00 -i 2022-03-16@05:00:00@X00MXVC0OL000001421190202105190424,2022-03-16@05:00:00@X00MXVC0SV020211207055749043272341,2022-03-16@05:00:00@X00MXVC0SV020211207055748245065341,2022-03-16@05:00:00@X00MXVC0OL000001339160202103010523,2022-03-16@05:00:00@X00MXVC0OL000001339060202103010159,2022-03-16@05:00:00@X00MXVC0OL000001339080202103010159,2022-03-16@05:00:00@X00MXVC0PL000001461160202106300705,2022-03-16@05:00:00@X00MXVC0X0120211207055747696731341,2022-03-16@05:00:00@X00MXTR0OL000001339070202103010159,2022-03-16@05:00:00@X00MXTR0OL000001421500202105190424,2022-03-16@05:00:00@X00MXHC0OL000001327380202102090753,2022-03-16@05:00:00@X00MXHC0SV020211207055749402383341,2022-03-16@05:00:00@X00MXHC0OL000001421440202105190424,2022-03-16@05:00:00@X00MXHC0PL000001461200202106300705,2022-03-16@05:00:00@X00MXO90SV020211207055751932476341,2022-03-16@05:00:00@X00MXWC0OL000001454090202106210433,2022-03-16@05:00:00@X00MXVC0SV020211207055749793830341,2022-03-16@05:00:00@X00MXTR0SV020211207055749878332341,2022-03-16@05:00:00@X00MXWC0QL000001461370202106300705,2022-03-16@05:00:00@X00MXQS0QL000001461290202106300705,2022-03-16@05:00:00@X00MXTR0SV020211207055752996570341,2022-03-16@05:00:00@X00MXTR0OL000001421270202105190424,2022-03-16@05:00:00@X00MXVC0QL000001461300202106300705,2022-03-16@05:00:00@X00MXVC0QL000001461250202106300705,2022-03-16@05:00:00@X00MXVC0SV020211207055752086282341,2022-03-16@05:00:00@X00MXWC0OL000001454170202106210433,2022-03-16@05:00:00@X00MXHC0OL000001339130202103010159,2022-03-16@05:00:00@X00MXWC0SV020211207055749962180341,2022-03-16@05:00:00@X00MXVC0OL000001421720202105190424,2022-03-16@05:00:00@X00MXWC0OL000001454180202106210433,2022-03-16@05:00:00@X00MXHC0Y0120211207055747461462341,2022-03-16@05:00:00@X00MXQS0OL000001475190202107060855,2022-03-16@05:00:00@X00MXTR0OL000001421650202105190424,2022-03-16@05:00:00@X00MXHC0OL000001421750202105190424,2022-03-16@05:00:00@X00MXN90OL000001327290202102090753,2022-03-16@05:00:00@X00MXVC0SV020211207055751333683341,2022-03-16@05:00:00@X00MXQS0QL000001461420202106300705,2022-03-16@05:00:00@X00MXTR0OL000001421730202105190424,2022-03-16@05:00:00@X00MXTR0SV020211207055751400771341,2022-03-16@05:00:00@X00MXVC0OL000001339120202103010159,2022-03-16@05:00:00@X00MXVC0SV020211207055750589684341,2022-03-16@05:00:00@X00MXVC0OL000001421640202105190424,2022-03-16@05:00:00@X00MXPS0OL000001421530202105190424,2022-03-16@05:00:00@X00MXN90SV020211207055753158619341,2022-03-16@05:00:00@X00MXN90SV020211207055749313094341,2022-03-16@05:00:00@X00MXPS0SV020211207055749491649341,2022-03-16@05:00:00@X00MXHC0OL000001421520202105190424,2022-03-16@05:00:00@X00MXVC0OL000001421330202105190424,2022-03-16@05:00:00@X00MXVC0OL000001421410202105190424,2022-03-16@05:00:00@X00MXVC0QL000001461360202106300705,2022-03-16@05:00:00@X00MXHC0OL000001421220202105190424,2022-03-16@05:00:00@X00MXVC0SV020211207055752903258341,2022-03-16@05:00:00@X00MXTR0PL000001461170202106300705,2022-03-16@05:00:00@X00MXTR0OL000001421340202105190424,2022-03-16@05:00:00@X00MXHC0OL000001338990202103010159,2022-03-16@05:00:00@X00MXHC0OL000001421290202105190424,2022-03-16@05:00:00@X00MXVC0OL000001421560202105190424,2022-03-16@05:00:00@X00MXHC0OL000001339090202103010159

			# 3 remaining: for manual: successful rerun
			# done react done sheet

	(R D-1) 4150	2022-03-16	New CMS	K00	rating_score	alza.cz	
		'K00CZ360AD000001526760202108020954',
		'K00CZ3604K000001526790202108020954',
		'K00CZ3604K000001526880202108020954',
		'K00CZ3603F000001526950202108020954',
		'K00CZ3603F000001526970202108020954',
		'K00CZ3603F000001527010202108020954',
		'K00CZ360CG000001485760202107130335',
		'K00CZ360CG000001485870202107130335',
		'K00CZ360CG000001486000202107130335',
		'K00CZ360CG000001486010202107130335',
		'K00CZ360CG000001486060202107130335',
		'K00CZ360CG000001486160202107130335',
		'K00CZ360FO000001527040202108020954'
		Incorrect ratings fetched 		
		Yes	Arfen

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-16' AND website = 'www.alza.cz'	AND item_id in ('K00CZ360AD000001526760202108020954',
			'K00CZ3604K000001526790202108020954',
			'K00CZ3604K000001526880202108020954',
			'K00CZ3603F000001526950202108020954',
			'K00CZ3603F000001526970202108020954',
			'K00CZ3603F000001527010202108020954',
			'K00CZ360CG000001485760202107130335',
			'K00CZ360CG000001485870202107130335',
			'K00CZ360CG000001486000202107130335',
			'K00CZ360CG000001486010202107130335',
			'K00CZ360CG000001486060202107130335',
			'K00CZ360CG000001486160202107130335',
			'K00CZ360FO000001527040202108020954')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00CZ360AD000001526760202108020954,2022-03-16@22:00:00@K00CZ3604K000001526790202108020954,2022-03-16@22:00:00@K00CZ3604K000001526880202108020954,2022-03-16@22:00:00@K00CZ3603F000001526950202108020954,2022-03-16@22:00:00@K00CZ3603F000001526970202108020954,2022-03-16@22:00:00@K00CZ3603F000001527010202108020954,2022-03-16@22:00:00@K00CZ360CG000001485760202107130335,2022-03-16@22:00:00@K00CZ360CG000001485870202107130335,2022-03-16@22:00:00@K00CZ360CG000001486000202107130335,2022-03-16@22:00:00@K00CZ360CG000001486010202107130335,2022-03-16@22:00:00@K00CZ360CG000001486060202107130335,2022-03-16@22:00:00@K00CZ360CG000001486160202107130335,2022-03-16@22:00:00@K00CZ360FO000001527040202108020954

			# rerun successful
			# done react, done sheet

	-----
	(R D-1) 2698	2022-03-16	New CMS	K00	all	bol.com	
		'Gaming stoel',
			# f duplicate because the site is behaviour stupid
			# https://prnt.sc/t1CUC6d5ZByc
			# https://prnt.sc/lOUxB6hAt35A

		'ANC-koptelefoon'
			# f duplicate because the site is behaviour stupid
			# https://prnt.sc/YURfv9XhZi0-

			Duplicate prod_website	
			https://prnt.sc/Cr9EdkWccn_u
			https://prnt.sc/adb7rHq0Dfio	
			Yes	Kristian

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'Gaming stoel',
						'ANC-koptelefoon') and 
				var2.[date] = '2022-03-16' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source


			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00@06630cd2-1da8-4090-928b-591101b65e55@UAjszpOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00@66eaf3d4-14bd-4ead-93f7-5a4ab84e6087@UAjszpOqEei-eQANOiOHiA

			# multiple reruns failed, create f screenshot as duplicate

			# done react, done sheet

	(R D-1) 2706	2022-03-16	New CMS	K00	all	www.altex.ro	
		Wireless controller	
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/nkfBTiknKNRF	
		Yes	ailyn

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.altex.ro' and 
				var2.keyword in (N'Wireless controller') and 
				var2.[date] = '2022-03-16' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@21:00:00@K00@b61e85cf-e76d-4343-ba39-8a6d0a909094@aa65086a-c5d1-4571-8371-9aa2b6e290df

			# successful rerun
			# done react, done sheet

	(R D-3) 2704	2022-03-16	New CMS	K00	all	fnac.com	
		'CLAVIER PC', - 25
		'SOURIS SANS FIL', - 25
		'ORDINATEUR PORTABLE DE JEU', - 25
		'Gaming Keypad', - 21
		'Keypad' - 25

		Data Count Mismatch  5(Fetched Data is Lesser than expected)
		Data Count Mismatch  1(Fetched Data is Lesser than expected)	

		https://prnt.sc/riYOSvmjaA4c
		https://prnt.sc/xdJzaVrDo0ec
		https://prnt.sc/hxRAEriGErZP
		https://prnt.sc/YAjh0i8lVWLe
		https://prnt.sc/7tLwMPN66Oxw	

		michael

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-16' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'CLAVIER PC',
						'SOURIS SANS FIL',
						'ORDINATEUR PORTABLE DE JEU',
						'Gaming Keypad',
						'Keypad')
				GROUP BY keyword, ritem_id, country, source

				2e2f9905-a4be-4608-94c2-d6ac8ea9fd1b@LzP6XApHEem-gAANOiOHiA
				8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00@c6b4c04f-d51d-4256-b159-fcff8fbab6db@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@2e2f9905-a4be-4608-94c2-d6ac8ea9fd1b@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@c6b4c04f-d51d-4256-b159-fcff8fbab6db@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@2e2f9905-a4be-4608-94c2-d6ac8ea9fd1b@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@22:00:00@K00@c6b4c04f-d51d-4256-b159-fcff8fbab6db@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@2e2f9905-a4be-4608-94c2-d6ac8ea9fd1b@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@2e2f9905-a4be-4608-94c2-d6ac8ea9fd1b@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@8da72bf1-3b31-4cf0-8f84-ab38fef55839@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2713	2022-03-16	New CMS	K00	all	citilink.ru	
		/ 'БЕСПРОВОДНАЯ МЫШЬ',
			5751b825-32ac-4dd8-95c9-00bf2a9cc045@8THUOEXdEeeMawANOiZi-g
		/ 'ИГРОВАЯ МЫШЬ',
			058373d8-5da5-4d72-8015-0f27efa98941@8THUOEXdEeeMawANOiZi-g
		/ 'Игровой коврик для мыши'
			0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@8THUOEXdEeeMawANOiZi-g

		Auto copyover for 1 day
		https://prnt.sc/SSDGcS77LEc3
		https://prnt.sc/8yb4VLs8rgpj
		https://prnt.sc/A2dXh3dFx-lp
		yes	Mich

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-16' 
				AND website = 'www.citilink.ru' 
				AND keyword in (N'Игровой коврик для мыши')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@5751b825-32ac-4dd8-95c9-00bf2a9cc045@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@058373d8-5da5-4d72-8015-0f27efa98941@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@8THUOEXdEeeMawANOiZi-g

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2714	2022-03-16	New CMS	K00	all	citilink.ru	
		/ Игровые колонки - 2
			9ac00807-dfb4-41be-82b7-97425f392504@8THUOEXdEeeMawANOiZi-g
		/ PS Контроллер - 17
			f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g
		/ Контроллер PlayStation - 10
			f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g
		/ Наушники ANC - 23
			f122a9bc-031d-464a-8917-9b5f342cdeeb@8THUOEXdEeeMawANOiZi-g
		/ лучшие наушники - 19
			f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@8THUOEXdEeeMawANOiZi-g

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 15 (Fetched Data is Lesser than expected)
		Data Count Mismatch  1(Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Greater than expected)
		Data Count Mismatch  4 (Fetched Data is Lesser than expected)

		https://prnt.sc/5xY8PCrUvtpN
		https://prnt.sc/_nf79V40m0Hz
		https://prnt.sc/jsb9y0gD3En6
		https://prnt.sc/YF_C_f56iJ92
		https://prnt.sc/B9Jo0h-iOACc
		yes	Mich

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-16' 
				AND website = 'www.citilink.ru' 
				AND keyword in (N'Игровые колонки')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f4b38090-8593-426e-a62f-630e83b51d8c@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@8THUOEXdEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@8THUOEXdEeeMawANOiZi-g

	(R D-3) 2715	2022-03-16	New CMS	K00	all	dns-shop.ru	
		БЕСПРОВОДНАЯ ГАРНИТУРА
			d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g
		ГАРНИТУРА ДЛЯ ПК
			4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g
		ИГРОВАЯ ГАРНИТУРА
			b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g
		ИГРОВАЯ КЛАВИАТУРА
			f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		КЛАВИАТУРА
			5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		МЕХАНИЧЕСКАЯ КЛАВИАТУРА
			2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		БЕСПРОВОДНАЯ МЫШЬ
			5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g
		ИГРОВАЯ МЫШЬ
			058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g
		КОМПЬЮТЕРНАЯ МЫШКА
			85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g
		МЫШЬ
			29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g
		ИГРОВОЙ НОУТБУК
			6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g
		Игровые колонки
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
		Игровой стул
			a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g
		PS Контроллер
			f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g
		Контроллер Xbox
			bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g
		Контроллер PS4
			784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g
		Игровой коврик для мыши
			0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g
		Коврик для мыши
			9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g
		наушники
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
		Наушники ANC
			f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g
		лучшие наушники	
			f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@BzxplkXeEeeMawANOiZi-g

		1 day auto copy over 
		2 days auto copy over 
		3 days auto copy over
	
		https://prnt.sc/6mX4gVfH35gW
		https://prnt.sc/cDp0TIN004G_
		https://prnt.sc/GFsFLjT4bG9G
		https://prnt.sc/lkNU-Fcv2To6
		https://prnt.sc/siN3flRyd6Jg
		https://prnt.sc/AEgl1li8O25-
		https://prnt.sc/JjGIXhCHRsEr
		https://prnt.sc/lE_r1LWa_VGB
		https://prnt.sc/ADsk5mMh6tU6
		https://prnt.sc/SnV-XO9JIEBd
		https://prnt.sc/S4Re9GPZy8rQ
		https://prnt.sc/FvPcFSVgdR5y
		https://prnt.sc/yqKoph8P4gcK
		https://prnt.sc/PJrlHfAmoNn1
		https://prnt.sc/G9UR_R4nhLU8
		https://prnt.sc/hP0Yg__0uvct
		https://prnt.sc/4m9qL8X07gY8
		https://prnt.sc/fNQeBswkQjSi
		https://prnt.sc/Cybl1g0hH1Ss
		https://prnt.sc/mkO0-FuUhbiL
		https://prnt.sc/3S4j08z5PPHU		

		KEEN

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-16' 
				AND website = 'www.dns-shop.ru' 
				AND ritem_id in ('d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g',
				'4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g',
				'b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g',
				'f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g',
				'5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g',
				'2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g',
				'5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g',
				'058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g',
				'85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g',
				'29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g',
				'6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g',
				'9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g',
				'a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g',
				'f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g',
				'bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g',
				'784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g',
				'0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g',
				'9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g',
				'3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g',
				'f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g',
				'f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@BzxplkXeEeeMawANOiZi-g')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g,2022-03-16@20:00:00@K00@29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2716	2022-03-16	New CMS	K00	all	dns-shop.ru	
		Офисный стул	
		invalid deadlink	
		https://prnt.sc/QwI3mA25ZIJS		
		keen

		# solution
			# SELECT * FROM view_all_rankingsdata var where date='2022-03-16' AND website = 'www.dns-shop.ru'AND keyword in (N'Офисный стул')	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@20:00:00@K00@6ec5bb27-69a4-4fa4-a106-2aa17f5f1b28@BzxplkXeEeeMawANOiZi-g

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2717	2022-03-16	New CMS	K00	all	fnac.com	
		/ 'CLAVIER DE JEU', - 25
		/ 'PORTABLE', - 25
		/ 'Mechanical Keypad' - 25

		Data Count Mismatch 6 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/Ka17hFqyAACQ
		https://prnt.sc/3ooNo_f4i9MC
		https://prnt.sc/C072xs5amuzg	

		Yes	michael

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-16' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'CLAVIER DE JEU',
						'PORTABLE',
						'Mechanical Keypad')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@aa32c42f-257c-47d8-8570-6bec0194024f@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@af0f30f1-87f0-47eb-8c2f-8bcf77523493@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@03bedf20-a48b-4b61-9ef4-9ba8b6900034@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@aa32c42f-257c-47d8-8570-6bec0194024f@LzP6XApHEem-gAANOiOHiA,2022-03-16@22:00:00@K00@03bedf20-a48b-4b61-9ef4-9ba8b6900034@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-16@22:00:00@K00@aa32c42f-257c-47d8-8570-6bec0194024f@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, done sheet

	-----
	(R D-1) 920	2022-03-16	New CMS	200	all	www.re-store.ru	
		headphones wired in ear	
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/wfT7njm_AgFS	
		Yes	kurt

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-16' 
				AND website = 'www.re-store.ru' 
				AND category in (N'headphones wired in ear')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-16@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# multiple reruns successful
			# done react, done sheet
	-----
	stats:
		2 commits (4146)

		Rerun: 14 (21/14) = 1.5
		Fix: 1[1] 1[2] = 1.5
		
03 17 22
	(F D-1) Trello 1
		[RAZER] Compliance - Error image_url in www.inet.se
		All image_urls in www.inet.se are returning 404 error. Please fix the spyder

		date: 2022-03-17
		retailer: www.inet.se
		item_id: all
		sample item_id: K00SEM103F000001307560202101210525
		sample image_url: https://inetimg3.se/product/1600x900/6608553_10.jpg
		# https://trello.com/c/8s5aXfYW/3475-razer-compliance-error-imageurl-in-wwwinetse

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-17' AND website = 'www.inet.se' AND item_id in ('K00SEM103F000001307560202101210525')

			# fixed > pushed > added comment and moved to 'for rebuild'
			
	(R D-1) Trello 2
		1.Incorrect data:
			Ticket : https://trello.com/c/CWOu500j/3468-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-en-us-2

			Issue still exist.
			Affected item Id:

			# 1
				# A00US440KT020211025061829945567298
				# https://www.microsoft.com/en-us/store/configure/Surface-Go-3/904H27D0CBWN?crosssellid=fbt-f4c&selectedColor=86888a&preview=&previewModes=
					# 469.9900 > 469.99
					"variant": {
						"uid": "8WND1N567X2T",
						"color": "Platinum",
						"storage": "128GB SSD",
						"processor": "Intel Pentium 6500Y - Wi-Fi"
					}

					# no issue in site
					# check site
						# idSelector = .tileproductplacement button[data-aid="8WND1N567X2T"]
						# data-pid=904H27D0CBWN

						# data-inventoryid="8V6-00001"
						# data-inventoryid="8VA-00001"


			# 2
				# A00US440RB000001239020202011060650
				# https://www.microsoft.com/en-us/store/configure/Surface-Laptop-Go/94FC0BDGQ7WV?crosssellid=fbt-h1c&selectedColor=85919B&preview=&previewModes=
					# 699.9900 > 699.99
					"variant": {
				    	"uid": "2ZQF-0-5",
				    	"color": "Platinum",
				    	"storage": "256GB SSD",
				    	"processor": "Intel Core i5"
				    }

				    # no issue on site

			# 3
				# A00US440RB000000868260201911050252
				# https://www.microsoft.com/en-us/p/surface-pro-7/8n17j0m5zzqs/j9pz?activetab=techspecs
					# 469.99 > 479.99

			# 4
				# A00US440RB000001413470202105120254
				# https://www.microsoft.com/en-us/store/configure/surface-laptop-4/946627fb12t1?selectedColor=&preview=&previewModes=
					# 699.99 > 1299.99
					"variant": {
				      "uid": "CN78-0-14",
				      "color": "Sandstone (metal)",
				      "storage": "512GB SSD",
				      "screen_size": "13.5 inch",
				      "processor": "Intel Core i5"
				    }

			# SOLUTION
				# SELECT * FROM view_all_productdata where date='2022-03-17' AND website = 'www.microsoft.com/en-us'	AND item_id in ('A00US440KT020211025061829945567298',
				'A00US440RB000001239020202011060650',
				'A00US440RB000000868260201911050252',
				'A00US440RB000001413470202105120254')

		2: Incorrect Stocks:

			Trello ticket: https://trello.com/c/5nzeNODB/3460-ms-price-comparisons-incorrect-stocks-in-microsoftcom-en-us


			Affected item Ids:
			A00US440KT020211025061838771274298
			A00US440KT020211025061833277226298
			A00US440KT020211025061840422276298
			A00US440KT020211025061832088265298
			A00US440KT020211025061837377749298
			A00US440KT020211025061835168934298
			A00US440KT020211025061839755295298

	-----
	(W) EMEA REGION WEBSHOTS
		# 260
		# 154
		# 143
		# 134
		# 48
		# validator updated
	-----
	(R D-1) 2729	2022-03-17	New CMS	200	all	www.target.com
		alexa speaker
			aad973cd-a877-425a-b584-c404d66ee80c@0l0XDL-yEei-eQANOiOHiA
			https://prnt.sc/7OmTgZXnA-eg
		SMART SPEAKER
			30f9dc2a-2a90-4381-8bc4-37deb248b8f4@0l0XDL-yEei-eQANOiOHiA

		Alexa Speaker - Duplicate Data(Rank 2 and 10 : Not Valid)
		Smart Speaker - Duplicate Data (rank 1 and 6 : Not Valid)

		alexa Speaker - https://prnt.sc/myAwevI8EPum
		smart speaker - https://prnt.sc/6dGhZ8AgdmmA
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.target.com' and 
				var2.keyword in (N'SMART SPEAKER') and 
				var2.[date] = '2022-03-17' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-17@04:00:00@200@aad973cd-a877-425a-b584-c404d66ee80c@0l0XDL-yEei-eQANOiOHiA,2022-03-17@04:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@0l0XDL-yEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-17@04:00:00@200@aad973cd-a877-425a-b584-c404d66ee80c@0l0XDL-yEei-eQANOiOHiA

			# done react, done sheet
	-----
	0 (R D-1)925	2022-03-17	New CMS	O00	all	www.coolblue.nl	
		Wash/Dry Combo	
			# https://www.coolblue.nl/was-droogcombinaties
		2 day auto copy over	
		https://prnt.sc/K8GXcBAeBpeU	
		Yes	Yev

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-17' 
				AND website = 'www.coolblue.nl' 
				AND category in (N'Wash/Dry Combo')
				GROUP BY category, litem_id, country, category_url, source

			# 

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-17@22:00:00@O00@O10@7973e1f7-270a-4ae1-a087-cf0a99577a77

			# multiple reruns failed
			# check site: top level category as advised my sir Ryan: https://prnt.sc/RIO7kMTEmGFY
			# done react, done sheet

	(R D-2) 926	2022-03-17	New CMS	O00	all	www.krefel.be/nl	
		Soundbar 2020
			# https://prnt.sc/366dARX4ZDtV	
		duplicate prod_web/prod_url	
		https://prnt.sc/9ri6KusiOyPC	
		Yes	Yev

		# solution
			# 

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-17@22:00:00@O00@IS0@fe2bd69d-c769-48a6-aa58-4aad40af871c
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-17@22:00:00@O00@IS0@fe2bd69d-c769-48a6-aa58-4aad40af871c
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-17@22:00:00@O00@IS0@fe2bd69d-c769-48a6-aa58-4aad40af871c
			# multiple reruns successful: duplicate
			# done react, done sheet

	(R D-1) 927	2022-03-17	New CMS	O00	all	www.krefel.be/nl	
		Vacuum	
		1 day auto copy over	
		https://prnt.sc/kvh-ODzvhTif	
		Yes	Yev

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-17' 
				AND website = 'www.krefel.be/nl' 
				AND category in (N'Vacuum')
				GROUP BY category, litem_id, country, category_url, source	

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-17@22:00:00@O00@IS0@34cdcab6-2bda-4712-a2be-d19474c79ddb
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 928	2022-03-17	New CMS	200	all	www.re-store.ru	
		'Headphones Bluetooth On Ear', - 25
		'Speakers Voice' - 3

		Mismatched Data	
		https://prnt.sc/cENCKiCRPx13
		https://prnt.sc/dV46LFQNgh1j	
		Yes	Jairus

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-17' 
				AND website = 'www.re-store.ru' 
				AND category in (N'Headphones Bluetooth On Ear',
						'Speakers Voice')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-17@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-17@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-17@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd
			# multiple reruns successful
			# done react, done sheet
	-----
	stats:
		1 commit (Trello 1 - inet.se)

		Rerun: 6 (7) = 1.16
		Webshots: 1 
		Fix: 1[1]

03 18 22
	(F D-3) Trello 1
		[MOTOROLA] Listings: 2 Days Consecutive Auto Copy Over in www.telekom.sk
		Issue Description: data is now on API as per BEs investigation

		Retailer: www.telekom.sk

		Category:
		All smartphones
			https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806
		5G Smartphones
			https://eshop.telekom.sk/category/all-devices/list/product_listing?filter%5B%5D=filter.device_category%5B%5D=5g&currentPage=2&tariffId=RP1140&itemPerPage=16&bp=acquisition

		Listings UUID:
		d81ade30-369d-4675-93d9-380cbe0912a8
		1b81bdeb-d684-4a50-aca1-df4b70bb538d

		Screenshot:
		https://prnt.sc/26mqpgt

		Date:
		Feb 1, 2022

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-14' AND website = 'www.telekom.sk' AND category in ('All smartphones')
			# 2022-03-14@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8,2022-03-18@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d

			2022-03-18@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8
			2022-03-18@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-18@22:00:00@P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8, 2022-03-18@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-18@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d
			# done fix for deadlink
			# done fix for pagination
			# done fix for product ount
			# done 

			# 
	-----
	(W) EMEA REGION (A00)
		# 800+
		# 633
		# 509
		# 402
		# 153
		# update validator
		# done

	(W) EMEA REGION (A00)
		# 1294
		# 1218
		# 1179
		# 1118
		# 1036
		# 762
		# 743
		# 687
		# 630
		# 540
		# 563
		# 513
		# 460
		# 434
		# 375
		# 255

	(W) 4178	2022-03-18	New CMS	610	webshot_url	all	
		select * from view_all_productdata where date='2022-03-18' and webshot_url is null order by item_id	
		No Webshots		
		Yes	Keeshia
		
		# solution
			# 600+
			# 120
			# 86
			# 8
			# update validator
			# done

	(W) 4179	2022-03-18	New CMS	F00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-18' and webshot_url is NULL	
		Missing webshots		
		Yes	Maryeeeeel

		# solution
			# 1533
			# 1335
			# 1067
			# 792
			# 383
			# 310
			# 140
			# 136
			# 89
			# 67
			# 25
			# 5
			# updated validator
			# done react, done sheet

	(W) 4180	2022-03-18	New CMS	100	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-18' and webshot_url is NULL	
		Missing webshots		
		Yes	Turki

		# solution
			# 1211
			# 1184
			# 1093
			# 1091
			# 1086
			# 716
			# 521
			# 99
			# 1
			# done update validator
			# done react, done sheet

	(W) 4187	2022-03-18	New CMS	K00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-18' and webshot_url is NULL	
		Missing webshots		
		Yes	Maryeeeeel

		# solution
			# 1718
			# 1658
			# 1321
			# 1232
			# 1215
			# 1144
			# 707
			# 500+
			# 456
			# 360
			# 312

	(W) 4194	2022-03-18	New CMS	A10	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-18' and webshot_url is NULL	No Webshot		Yes	Have

		# solution
			# 49
			# 38
			# 5 
			# 1
			# updated validator: 
			# done react, done sheet

	0 (R D-1) 4200	2022-03-18	New CMS	200	all	fr.toppreise.ch	
		'200FR7D070000000550110201904050540',
		'200FR7D080000000551730201904050650',
		'200FR7D080000000551550201904050645'
		Invalid deadlink		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-18' AND website = 'fr.toppreise.ch'	AND item_id in ('200FR7D070000000550110201904050540',
			'200FR7D080000000551730201904050650',
			'200FR7D080000000551550201904050645')	

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@200FR7D070000000550110201904050540,2022-03-18@22:00:00@200FR7D080000000551730201904050650,2022-03-18@22:00:00@200FR7D080000000551550201904050645

			# trello

	-----
	(R D-1) 2730	2022-03-17	New CMS	Q00	all	www.outnorth.se	
		'Mockasiner herr',
		'Gore tex skor herr'
		Auto Copy Over for 1 Day	

		https://prnt.sc/g9WUoaA5G1GN
		https://prnt.sc/BTUqjlUuENYb

		Yes	rod

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-18' 
				AND website = 'www.outnorth.se' 
				AND keyword in (N'Mockasiner herr',
						'Gore tex skor herr')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@Q00@79c8a057-c93c-4403-b6b0-7dfde0651746@257c4048-6837-4e5c-8e31-b443baccb408,2022-03-18@22:00:00@Q00@029e14af-2ece-465a-b69f-5dd548db926b@257c4048-6837-4e5c-8e31-b443baccb408

			# rerun successful
			# done react, done sheet

	------
	(R D-1) 931	2022-03-18	New CMS	Q00	all	www.bever.nl	
		'Jackets Women',
		'Trousers Women'
		Auto copyover for 1 day 	
		https://prnt.sc/jdoGC16vaxPs
		https://prnt.sc/oAySv5r8DzbC
		yes	Mich

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-18' 
				AND website = 'www.bever.nl' 
				AND category in (N'Jackets Women',
						'Trousers Women')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-18@22:00:00@Q00@8W0@93d3f263-6478-46fa-a0dd-e8db7c5dd3e3,2022-03-18@22:00:00@Q00@8W0@96fb886a-fae6-4d33-8955-7093c28ac7e9

			# rerun successful
			# done react, done sheet

	Stats:
		Rerun: 3
		webshots: 7
		Fix: 1[3]

03 19 22
	------
	(W) 4204	2021-03-19	New CMS	610	webshot_url		
		select * from view_all_productdata where date='2022-03-19'and webshot_url is null	No Webshots		
		Yes	Keeshia

		# solution
			# 638
			# 566
			# 360
			# 254
			# 239
			# 171
			# 141
			# 125
			# 111
			# 94
			# updated validator
			# done react, done sheet

	(W) EMEA REGION (A00)
		# 977
		# 745
		# 730
		# 659
		# 627
		# 621
		# 578
		# 558
		# 537
		# 541
		# updated validator
		# done react

	(W) NA REGION (A00)
		# 500+
		# 471
		# 399
		# 375
		# 262 (75.3%)
		# 162 (100%)
		# 90 (72%)
		# 25 (65%)
		# done updated validator
		# 

	(R D-1) 4225	2022-03-19	New CMS	200	all	cdiscount.com/3P	
		200FRMS070000001398360202105040415	
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-19s' AND website = 'www.cdiscount.com/3P'	AND item_id in ('200FRMS070000001398360202105040415')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200FRMS070000001398360202105040415
			# successful rerun
			# done react, done sheet

	0 (R D-1) 4226	2022-03-19	New CMS	200	in_stock	elgiganten.dk	
		200DK10070000001560310202108230658	
		ETL Failure in stocks		
		Yes	Louie
		
		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-19' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK10070000001560310202108230658')			

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200DK10070000001560310202108230658
			# check local: 
				# https://www.elgiganten.dk/product/tv-lyd-billede/horetelefoner-tilbehor/horetelefoner/marshall-motif-anc-true-wireless-in-ear-horetelefoner-sort/357756
			# trello

	(R D-1) 4227	2022-03-19	New CMS	200	delivery	elgiganten.dk	
		200DK100B0000000031520201901180326	
		wrong DQ		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-19' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK100B0000000031520201901180326')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200DK100B0000000031520201901180326
			# multiple reruns successful
			# don react, done sheet

	(R D-2) 4241	2022-03-19	New CMS	100	delivery	elgiganten.dk	
		'100DK100ZQ020220124093149795094024',
		'100DK10010000001660410202111162228',
		'100DK100ZQ020220124090753862646024',
		'100DK100ZQ020220124090754027240024',
		'100DK100ZQ020220124090754197223024',
		'100DK100ZQ020220124090754985166024',
		'100DK100ZQ020220124090755127099024',
		'100DK100ZQ020220124090755315930024',
		'100DK100ZQ020220124090754819827024',
		'100DK100ZQ020220124093126046040024',
		'100DK10010000001659240202111162228',
		'100DK100ZQ020220124093126379703024',
		'100DK100ZQ020220124090756117378024',
		'100DK10010000001324310202102090603',
		'100DK100ZQ020220124093144653882024',
		'100DK10010000001324400202102090603',
		'100DK10010000001324440202102090603',
		'100DK10010000001659410202111162228',
		'100DK10010000000813290201908020245',
		'100DK10010000001248210202011250630',
		'100DK10010000001659390202111162228',
		'100DK100ZQ020220124093144395897024',
		'100DK100ZQ020220124093144115061024',
		'100DK10010000000814140201908020341',
		'100DK10010000000813760201908020319',
		'100DK100ZQ020220124093147433839024',
		'100DK100ZQ020220124093147040174024',
		'100DK100ZQ020220124093146643070024',
		'100DK10010000001248290202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001248230202011250630',
		'100DK10010000001438730202106080416',
		'100DK10010000001248330202011250630',
		'100DK10010000001248310202011250630',
		'100DK10010000001248360202011250630',
		'100DK10010000001659910202111162228',
		'100DK100ZQ020220214062211529844045',
		'100DK100ZQ020220124090759827858024',
		'100DK100ZQ020220214062210619118045',
		'100DK100ZQ020220124093149555986024',
		'100DK100ZQ020220124093149795094024',
		'100DK100ZQ020220124093149319561024',
		'100DK10010000001222780202010280537',
		'100DK10010000001222770202010280537',
		'100DK10010000001248390202011250630',
		'100DK10010000001222790202010280537',
		'100DK10010000001324630202102090603',
		'100DK10010000001400780202105040816',
		'100DK10010000001222720202010280537',
		'100DK100ZQ020220124093125637325024',
		'100DK10010000001397790202105040251',
		'100DK10010000001397940202105040251',
		'100DK100ZQ020220124093123601621024',
		'100DK10010000001326370202102090603',
		'100DK10010000001326510202102090603',
		'100DK10010000001326440202102090603',
		'100DK100ZQ020220124093132423284024',
		'100DK10010000001222810202010280537',
		'100DK10010000000818230201908020936',
		'100DK10010000001327030202102090604',
		'100DK100ZQ020220124093133398639024',
		'100DK10010000001222820202010280537'
		wrong delivery		
		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@100DK100ZQ020220124093149795094024,2022-03-19@22:00:00@100DK10010000001660410202111162228,2022-03-19@22:00:00@100DK100ZQ020220124090753862646024,2022-03-19@22:00:00@100DK100ZQ020220124090754027240024,2022-03-19@22:00:00@100DK100ZQ020220124090754197223024,2022-03-19@22:00:00@100DK100ZQ020220124090754985166024,2022-03-19@22:00:00@100DK100ZQ020220124090755127099024,2022-03-19@22:00:00@100DK100ZQ020220124090755315930024,2022-03-19@22:00:00@100DK100ZQ020220124090754819827024,2022-03-19@22:00:00@100DK100ZQ020220124093126046040024,2022-03-19@22:00:00@100DK10010000001659240202111162228,2022-03-19@22:00:00@100DK100ZQ020220124093126379703024,2022-03-19@22:00:00@100DK100ZQ020220124090756117378024,2022-03-19@22:00:00@100DK10010000001324310202102090603,2022-03-19@22:00:00@100DK100ZQ020220124093144653882024,2022-03-19@22:00:00@100DK10010000001324400202102090603,2022-03-19@22:00:00@100DK10010000001324440202102090603,2022-03-19@22:00:00@100DK10010000001659410202111162228,2022-03-19@22:00:00@100DK10010000000813290201908020245,2022-03-19@22:00:00@100DK10010000001248210202011250630,2022-03-19@22:00:00@100DK10010000001659390202111162228,2022-03-19@22:00:00@100DK100ZQ020220124093144395897024,2022-03-19@22:00:00@100DK100ZQ020220124093144115061024,2022-03-19@22:00:00@100DK10010000000814140201908020341,2022-03-19@22:00:00@100DK10010000000813760201908020319,2022-03-19@22:00:00@100DK100ZQ020220124093147433839024,2022-03-19@22:00:00@100DK100ZQ020220124093147040174024,2022-03-19@22:00:00@100DK100ZQ020220124093146643070024,2022-03-19@22:00:00@100DK10010000001248290202011250630,2022-03-19@22:00:00@100DK10010000001248260202011250630,2022-03-19@22:00:00@100DK10010000001248230202011250630,2022-03-19@22:00:00@100DK10010000001438730202106080416,2022-03-19@22:00:00@100DK10010000001248330202011250630,2022-03-19@22:00:00@100DK10010000001248310202011250630,2022-03-19@22:00:00@100DK10010000001248360202011250630,2022-03-19@22:00:00@100DK10010000001659910202111162228,2022-03-19@22:00:00@100DK100ZQ020220214062211529844045,2022-03-19@22:00:00@100DK100ZQ020220124090759827858024,2022-03-19@22:00:00@100DK100ZQ020220214062210619118045,2022-03-19@22:00:00@100DK100ZQ020220124093149555986024,2022-03-19@22:00:00@100DK100ZQ020220124093149795094024,2022-03-19@22:00:00@100DK100ZQ020220124093149319561024,2022-03-19@22:00:00@100DK10010000001222780202010280537,2022-03-19@22:00:00@100DK10010000001222770202010280537,2022-03-19@22:00:00@100DK10010000001248390202011250630,2022-03-19@22:00:00@100DK10010000001222790202010280537,2022-03-19@22:00:00@100DK10010000001324630202102090603,2022-03-19@22:00:00@100DK10010000001400780202105040816,2022-03-19@22:00:00@100DK10010000001222720202010280537,2022-03-19@22:00:00@100DK100ZQ020220124093125637325024,2022-03-19@22:00:00@100DK10010000001397790202105040251,2022-03-19@22:00:00@100DK10010000001397940202105040251,2022-03-19@22:00:00@100DK100ZQ020220124093123601621024,2022-03-19@22:00:00@100DK10010000001326370202102090603,2022-03-19@22:00:00@100DK10010000001326510202102090603,2022-03-19@22:00:00@100DK10010000001326440202102090603,2022-03-19@22:00:00@100DK100ZQ020220124093132423284024,2022-03-19@22:00:00@100DK10010000001222810202010280537,2022-03-19@22:00:00@100DK10010000000818230201908020936,2022-03-19@22:00:00@100DK10010000001327030202102090604,2022-03-19@22:00:00@100DK100ZQ020220124093133398639024,2022-03-19@22:00:00@100DK10010000001222820202010280537

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@100DK10010000001248290202011250630,2022-03-19@22:00:00@100DK10010000001248310202011250630,2022-03-19@22:00:00@100DK10010000001222820202010280537,2022-03-19@22:00:00@100DK10010000001660410202111162228,2022-03-19@22:00:00@100DK10010000001659240202111162228

			# multiple reruns successful: 
			# 1 valid for this item_id:
				# 100DK10010000001660410202111162228
				# https://prnt.sc/PFslukT5DlsM 

			# done react, done sheet

	(R D-1) 4242	2022-03-19	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659250202111162228',
		'100DK10010000001659670202111162228',
		'100DK100ZQ020220124093127658423024'
		spider issue -1 values		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-19' and item_id in ('100DK10010000001659250202111162228',
			'100DK10010000001659670202111162228',
			'100DK100ZQ020220124093127658423024')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@100DK10010000001659250202111162228,2022-03-19@22:00:00@100DK10010000001659670202111162228,2022-03-19@22:00:00@100DK100ZQ020220124093127658423024

			# successful rerun
			# done react, sheet

	(R D-1) 4243	2022-03-19	New CMS	200	all	fr.toppreise.ch	
		'200FR7D070000000552360201904050759',
		'200FR7D070000000551790201904050655',
		'200FR7D080000000551730201904050650',
		'200FR7D080000000551550201904050645'
		failed to scrape 3p / Invalid deadlinks		
		Yes	Jeremy

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-19' AND website = 'fr.toppreise.ch'	AND item_id in ('200FR7D070000000552360201904050759',
			'200FR7D070000000551790201904050655',
			'200FR7D080000000551730201904050650',
			'200FR7D080000000551550201904050645')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200FR7D070000000552360201904050759,2022-03-19@22:00:00@200FR7D070000000551790201904050655,2022-03-19@22:00:00@200FR7D080000000551730201904050650,2022-03-19@22:00:00@200FR7D080000000551550201904050645
			# manual override, cant verify
			#  done react, done sheet

	-----
	(R D-3) 2749	2022-03-19	New CMS	200	all	www.fnac.com	
		'casque audio',
		'casque bluetooth reducteur de bruit',
		'ecouteur sans fil',
		'ecouteurs sans fil bluetooth',
		'enceinte bluetooth portable',
		'enceinte marshall',
		'mini-enceinte'

		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 4 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	

		https://prnt.sc/yWpg_TTfD34g - 25
		https://prnt.sc/uTMQz3P_x9Zl - 24
		https://prnt.sc/Z44S6tIZFr7j - 25
		https://prnt.sc/hRwkwuZ6vpqo - 25
		https://prnt.sc/QzT9dbjuLyzg - 25 
		https://prnt.sc/0Sp0IRqdsYt9 - 25
		https://prnt.sc/L1--Sc6_51X2 - 25

		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-19' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio',
						'casque bluetooth reducteur de bruit',
						'ecouteur sans fil',
						'ecouteurs sans fil bluetooth',
						'enceinte bluetooth portable',
						'enceinte marshall',
						'mini-enceinte')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@1c543fd3-086d-4bbd-ae53-94ac7e543eab@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@ae24c711-4727-4acf-a2a7-6d912e2df6d6@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@51405791-5c01-4b1a-9f24-1e5f24dd5bbf@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@8c43e5e8-285f-4942-8d6f-d67d3c3c3bb0@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@72d341d9-394c-4dc6-8e1b-d0e942069ebd@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@1c543fd3-086d-4bbd-ae53-94ac7e543eab@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@8c43e5e8-285f-4942-8d6f-d67d3c3c3bb0@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@1c543fd3-086d-4bbd-ae53-94ac7e543eab@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA,2022-03-19@22:00:00@200@0fd85a28-a7cf-48df-af91-1786141e328e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@200@aed18697-1b5e-4f41-8112-6011c487aba9@LzP6XApHEem-gAANOiOHiA

			# trello

	(R D-2) 2750	2022-03-19	New CMS	200	all	www.re-store.ru	
		'наушники вкладыши' - 2
			513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab
		'наушники с проводом' - 18
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		'проводные наушники' - 16
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab


		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 8 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	

		https://prnt.sc/YVrcMGDDihPk
		https://prnt.sc/TGIwx__C_n1m
		https://prnt.sc/5MmzPBHCBw1Y	

		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.re-store.ru' and 
				var2.keyword in (N'проводные наушники') and 
				var2.[date] = '2022-03-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@21:00:00@200@513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-19@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-19@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# rerun successful
			# done react, done sheet

	(R D-2) 2751	2022-03-19	New CMS	200	all	www.mediamarkt.de	
		/ 'bluetooth kopfhörer',
		/ 'lautsprecher bluetooth'

		Duplicate product_website & product_url	

		https://prnt.sc/I9d0msupIofo
		https://prnt.sc/L7zjdH5wS-po
		https://prnt.sc/T5BbdBNe_4BY	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.mediamarkt.de' and 
				var2.keyword in (N'lautsprecher bluetooth') and 
				var2.[date] = '2022-03-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200@9f66bde9-a88f-40a3-9970-1c4305890a7c@M3YPUApIEem-gAANOiOHiA

	(R D-1) 2752	2022-03-19	New CMS	200	all	www.bol.com	
		smart speaker	
		Duplicate product_website & product_url	
		https://prnt.sc/jPLE2x6FixiG	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'smart speaker') and 
				var2.[date] = '2022-03-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@22:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@UAjszpOqEei-eQANOiOHiA
			# rerun successful
			# done react, done sheet

	(R D-1) 2753	2022-03-19	New CMS	200	all	www.dns-shop.ru	
		наушники с проводом	
		Duplicate product_website & product_url	
		https://prnt.sc/lFhBpkqvI880		

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'наушники с проводом') and 
				var2.[date] = '2022-03-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-1) 2754	2022-03-19	New CMS	110	all	www.gamemania.be/nl	
		Nintendo Switch rood/blauw	
		Auto copy over for 1 day	
		https://prnt.sc/fZ7D2yiScRGT	
		Yes	michael

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-19' 
				AND website = 'www.gamemania.be/nl' 
				AND keyword in (N'Nintendo Switch rood/blauw')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@22:00:00@110@5084f045-4388-4158-b464-26cea5f48278@a11f30ac-e99d-4651-abcb-951f733ab599
			# successful rerun
			# done react, done sheet

	(R D-1) 2755	2022-03-19	New CMS	U00	all	www.havan.com.br	
		'console switch',
		'Mini game portatil'

		Auto copy over for 1 day	
		https://prnt.sc/CPqw_EWQzmWA
		https://prnt.sc/T6_zOaIX06gl	
		Yes	kurt
		
		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-19' 
				AND website = 'www.havan.com.br' 
				AND keyword in (N'console switch','Mini game portatil')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@02:00:00@U00@013044db-0c1d-44de-8e6e-bbf725de15fd@3ba329db-8335-4f0c-8dc7-abc56f8eb367,2022-03-19@02:00:00@U00@453fd5ac-8edd-4d04-98b5-15c3eb669ca8@3ba329db-8335-4f0c-8dc7-abc56f8eb367

			# rerun successful
			# done react, done sheet

	(R D-1) 2756	2022-03-19	New CMS	U00	all	www.shoptime.com.br	
		'controle nintendo switch',
		'pro controller',
		'aparelho de game',
		'controle game sem fio'

		Data Count Mismatch 1 (Fetched Data is Greater than expected)	

		https://prnt.sc/4mSwN3LtPSDR
		https://prnt.sc/7hU8NFuG8LoZ
		https://prnt.sc/ubVQXKkmb28C
		https://prnt.sc/b72IJ3Li8zlE	

		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-19' 
				AND website = 'www.shoptime.com.br' 
				AND keyword in (N'controle nintendo switch',
						'pro controller',
						'aparelho de game',
						'controle game sem fio')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@02:00:00@U00@493085f2-179f-4689-b55f-8a2cb7d036a4@1QomMkasEeeMawANOiZi-g,2022-03-19@02:00:00@U00@210ffe59-5594-407f-86df-5fb3a0fe2d38@1QomMkasEeeMawANOiZi-g,2022-03-19@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@1QomMkasEeeMawANOiZi-g,2022-03-19@02:00:00@U00@afd31eea-4072-46c6-85c5-33921ae1871a@1QomMkasEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	0 (R D-1) 2757	2022-03-19	New CMS	200	all	www.target.com	
		'alexa speaker',
			# https://prnt.sc/lCU8gWoZ-X4-
		/ 'smart speaker'
		Duplicate product_website & product_url	

		https://prnt.sc/nOwUA8_1wcSI
		https://prnt.sc/CXpLUigHfz5Q	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.target.com' and 
				var2.keyword in (N'alexa speaker',
						'smart speaker') and 
				var2.[date] = '2022-03-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@04:00:00@200@aad973cd-a877-425a-b584-c404d66ee80c@0l0XDL-yEei-eQANOiOHiA,2022-03-19@04:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@0l0XDL-yEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-19@04:00:00@200@aad973cd-a877-425a-b584-c404d66ee80c@0l0XDL-yEei-eQANOiOHiA
			# check site: 7 rerun attempts failed
			# one react, done sheet

	0 (R D-1) 2758	2022-03-19	New CMS	X00	all	www.sears.com.mx	
		Nintendo Pro Controller	
		Invalid Deadlink	
		https://prnt.sc/laYWzCK7_1Wy	
		Yes	Kristian

		# solution
			# SELECT * FROM view_all_rankingsdata var WHERE date = '2022-03-19' AND website = 'www.sears.com.mx' AND keyword in (N'Nintendo Pro Controller')

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@05:00:00@X00@795f8f41-ea92-42dc-83ae-34f175697537@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# check local: 
			# Trello

	0 (R D-1) 2759	2022-03-19	New CMS	X00	all	www.sears.com.mx	
		'Accesorios para Nintendo Switch',
		'Consola',
		'Consola Nintendo Lite',
		'Consola Nintendo Switch',
		'Consola Nintendo Switch Lite',
		'Consola Switch',
		'Consola Switch Lite',
		'Joy Con',
		'Nintendo Joy Con',
		'Nintendo Switch',
		'Nintendo Switch Lite',
		'Nintendo Switch Pro Controller',
		'Switch Nintendo',
		'Switch Nintendo Lite',
		'Switch OLED',
		'Nintendo switch OLED'

		Auto Copy Over for 3 Days
		Auto Copy Over for 2 Days
		Auto Copy Over for 1 Day	

		https://prnt.sc/RyJ2u9V4YsBs
		https://prnt.sc/JYV5WQ_hyWYE
		https://prnt.sc/_gaxnsvSSZJx	

		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-19' 
				AND website = 'www.sears.com.mx' 
				AND keyword in (N'Accesorios para Nintendo Switch',
						'Consola',
						'Consola Nintendo Lite',
						'Consola Nintendo Switch',
						'Consola Nintendo Switch Lite',
						'Consola Switch',
						'Consola Switch Lite',
						'Joy Con',
						'Nintendo Joy Con',
						'Nintendo Switch',
						'Nintendo Switch Lite',
						'Nintendo Switch Pro Controller',
						'Switch Nintendo',
						'Switch Nintendo Lite',
						'Switch OLED',
						'Nintendo switch OLED')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-19@05:00:00@X00@8304e3b7-b53a-44d3-9082-dfc4c6170fbc@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@79cd174a-b94b-405e-97a5-e36568e39106@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@49d3844d-b750-462b-8951-06dfb60fcb21@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@3d9c0a46-e754-4fda-87ae-8675bf610e1d@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@4bc89052-7d33-4a6b-bda4-23c1ae037d9d@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@6a0f6d9b-7fd8-47f0-b384-dfafbf60ff5c@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@f47e7d8d-9235-4033-87fb-00b7466e702f@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@86c5be64-e098-4678-beda-fd4d8895e764@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@c3b588ab-2bbc-4c43-831d-1f84455db1d9@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@36ca1602-96f4-4497-bfff-a93bd2deb067@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@687f71ee-e5e7-427b-8ccd-6013c0d07916@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@13b8d32f-378a-4430-934d-966f5f30aee2@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@22a3e243-e46a-4fee-bef4-14cf3fe7cdaa@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@674037ed-5a15-4c2f-a51b-8e4cd65d1c09@a2e04412-f8ee-4aa8-b5b0-69bd076338f7,2022-03-19@05:00:00@X00@a2920dd5-d8f3-4050-94a7-a3cf19c28c9a@a2e04412-f8ee-4aa8-b5b0-69bd076338f7 

			# trello

	Stats:
		Rerun: 17 (22) = 1.29
		Webshots: 3

03 20 22
	0 (R D-1) Trello 2
		1.Incorrect data:
			Ticket : https://trello.com/c/CWOu500j/3468-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-en-us-2

			Issue still exist.
			Affected item Id:

			# 1
				# A00US440KT020211025061829945567298
				# https://www.microsoft.com/en-us/store/configure/Surface-Go-3/904H27D0CBWN?crosssellid=fbt-f4c&selectedColor=86888a&preview=&previewModes=
					# 469.9900 > 469.99
					"variant": {
						"uid": "8WND1N567X2T",
						"color": "Platinum",
						"storage": "128GB SSD",
						"processor": "Intel Pentium 6500Y - Wi-Fi"
					}

					# check site
						# idSelector = .tileproductplacement button[data-aid="8WND1N567X2T"]
						# data-pid=904H27D0CBWN

						# shoud add inventoryid to be unique
							# data-inventoryid="8V6-00001"
								# the other product
							# data-inventoryid="8VA-00001"
								# the right product

						# idSelector
							# let selector2 = `.tileproductplacement button[data-pid="${variantId}"][data-inventoryid="${inventoryId}"]`

			# 2
				# A00US440RB000001239020202011060650
				# https://www.microsoft.com/en-us/store/configure/Surface-Laptop-Go/94FC0BDGQ7WV?crosssellid=fbt-h1c&selectedColor=85919B&preview=&previewModes=
					# 699.9900 > 699.99
					"variant": {
				    	"uid": "2ZQF-0-5",
				    	"color": "Platinum",
				    	"storage": "256GB SSD",
				    	"processor": "Intel Core i5"
				    }

				    # idSelector
				    	# let selector4 = `.tileproductplacement button[id="${variantId}"]`

				    # get_price
				   		# `${idSelector} span:nth-child(4) > span:nth-child(3)`

			# 3
				# A00US440RB000000868260201911050252
				# https://www.microsoft.com/en-us/p/surface-pro-7/8n17j0m5zzqs/j9pz?activetab=techspecs
					# 749.99 > 749.99

					# no issues, requires rerun

			# 4
				# A00US440RB000001413470202105120254
				# https://www.microsoft.com/en-us/store/configure/surface-laptop-4/946627fb12t1?selectedColor=&preview=&previewModes=
					# 1299.99 > 1299.99
					"variant": {
				      "uid": "CN78-0-14",
				      "color": "Sandstone (metal)",
				      "storage": "512GB SSD",
				      "screen_size": "13.5 inch",
				      "processor": "Intel Core i5"
				    }

				# no issue

			# SOLUTION
				# SELECT * FROM view_all_productdata where date='2022-03-17' AND website = 'www.microsoft.com/en-us'	AND item_id in ('A00US440KT020211025061829945567298',
				'A00US440RB000001239020202011060650',
				'A00US440RB000000868260201911050252',
				'A00US440RB000001413470202105120254')

		2: Incorrect Stocks:

			Trello ticket: https://trello.com/c/5nzeNODB/3460-ms-price-comparisons-incorrect-stocks-in-microsoftcom-en-us

			Affected item Ids:
			A00US440KT020211025061838771274298
			A00US440KT020211025061833277226298
			A00US440KT020211025061840422276298
			A00US440KT020211025061832088265298
			A00US440KT020211025061837377749298
			A00US440KT020211025061835168934298
			A00US440KT020211025061839755295298

	-----	
	(W) 4246	2022-03-20	New CMS	610	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-20' and webshot_url is NULL	
		No Webshots		
		Yes	Keeshia

		# solution
			# 639
			# 539 (100%) 100 sent
			# 433 (88%) 120 sent
			# 299 (67%) 200 sent
			# 202 (97%) 100 sent
			# 72 (65%) 200 sent
			# 46 (36%) 72 sent
			# 29 (36%) 46 sent
			# 1 (97%) 29 sent
			# updated validator
			# done react, done sheet

	(W) EMEA REGION (A00)
		# 984 
		# 931 (63%) 100 sent
		# 824 (11.5%) 931 sent 
		# 748 (76%) 100 sent, +10mins
		# 712 (36%) 100 sent, +10mins
		# 682 () 100 sent, 
		# updated validator
		# done react

	(R D-1) 4264	2022-03-20	New CMS	200	all	amazon 3p retailer	
		'200ESFF080000000801720201906210132',
		'200ITGF070000000801930201906210247',
		'200ITGF080000000801730201906210132',
		'200ITGF070000000557970201904221001'
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-20' AND item_id in ('200ESFF080000000801720201906210132',
			'200ITGF070000000801930201906210247',
			'200ITGF080000000801730201906210132',
			'200ITGF070000000557970201904221001')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@200ESFF080000000801720201906210132,2022-03-20@22:00:00@200ITGF070000000801930201906210247,2022-03-20@22:00:00@200ITGF080000000801730201906210132,2022-03-20@22:00:00@200ITGF070000000557970201904221001
			# multiple reruns failed

			# done react, done sheet

	(w) 4265	2022-03-20	New CMS	200	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-20' and webshot_url is NULL	
		No Webshot		
		Yes	Louie

		# solution
			# 684 
			# 615 (69%) 100 sent +10mins
			# 297 (51.7%) 615 sent +15mins
			# 199 (49%) 198 sent
			# 145 (27%) 199 sent
			# 113 (22%) 145 sent
			# 100 (11%) 113 sent
			# 61 (39%) 100 sent
			# 48 (21%) 61 sent
			# updated validator

	(w) 4266	2022-03-20	New CMS	U00	webshot_url		SELECT * FROM view_all_productdata where date='2022-03-20' and webshot_url is NULL	
		No Webshot		
		Yes	Louie

		# solution
			# 4 
			# 0 (100%) 4 sent
			# done react, done sheet

	(w) 4268	2022-03-20	New CMS	H10	webshot_url		
		select * from view_all_productdata where date='2022-03-20' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com') and webshot_url is NULL order by item_id	
		No Webshots		
		Yes	Keeshia

		# solution
			# 429
			# 329 (100%) 101 sent, 
			# 228 (100%) 101 sent
			# 17 (92%) 228 sent
			# updated validator
			# done react

	(R D-1) 4272	2022-03-20	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001248330202011250630',
		'100DK10010000001326440202102090603',
		'100DK10010000001400740202105040816',
		'100DK100ZQ020220124093126379703024',
		'100DK100ZQ020220124090756117378024',
		'100DK100ZQ020220124090755127099024',
		'100DK100ZQ020220124093126046040024',
		'100DK100ZQ020220124090754819827024',
		'100DK100ZQ020220124090754027240024',
		'100DK100ZQ020220124093144653882024',
		'100DK10010000001324400202102090603',
		'100DK10010000001659410202111162228',
		'100DK100ZQ020220124093123122384024',
		'100DK10010000001659250202111162228',
		'100DK100ZQ020220124093144395897024',
		'100DK100ZQ020220124093144115061024',
		'100DK10010000001659370202111162228',
		'100DK100ZQ020220124093123963399024',
		'100DK100ZQ020220124090748845248024',
		'100DK10010000001659300202111162228',
		'100DK100ZQ020220124093147433839024',
		'100DK100ZQ020220124093146643070024',
		'100DK100ZQ020220124093145806437024',
		'100DK10010000001248290202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001248250202011250630',
		'100DK10010000001659670202111162228',
		'100DK10010000001248330202011250630',
		'100DK10010000001413660202105130512',
		'100DK10010000001248360202011250630',
		'100DK100ZQ020220124093148982275024',
		'100DK10010000001659910202111162228',
		'100DK100ZQ020220124090759827858024',
		'100DK100ZQ020220214062204402797045',
		'100DK100ZQ020220124093149319561024',
		'100DK10010000001222780202010280537',
		'100DK10010000001248390202011250630',
		'100DK10010000001400780202105040816',
		'100DK100ZQ020220124093124884947024',
		'100DK100ZQ020220124093125637325024',
		'100DK10010000001397790202105040251',
		'100DK10010000001397490202105040251',
		'100DK100ZQ020220124093123601621024',
		'100DK10010000001248400202011250630',
		'100DK10010000001326510202102090603',
		'100DK10010000001326440202102090603',
		'100DK10010000001222800202010280537',
		'100DK10010000001222840202010280537',
		'100DK10010000000818120201908020935',
		'100DK10010000001222810202010280537',
		'100DK100ZQ020220124093133398639024',
		'100DK10010000000819000201908021008',
		'100DK10010000001222820202010280537'
		wrong delivery		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-20' and item_id in ('100DK10010000001248330202011250630',
			'100DK10010000001326440202102090603',
			'100DK10010000001400740202105040816',
			'100DK100ZQ020220124093126379703024',
			'100DK100ZQ020220124090756117378024',
			'100DK100ZQ020220124090755127099024',
			'100DK100ZQ020220124093126046040024',
			'100DK100ZQ020220124090754819827024',
			'100DK100ZQ020220124090754027240024',
			'100DK100ZQ020220124093144653882024',
			'100DK10010000001324400202102090603',
			'100DK10010000001659410202111162228',
			'100DK100ZQ020220124093123122384024',
			'100DK10010000001659250202111162228',
			'100DK100ZQ020220124093144395897024',
			'100DK100ZQ020220124093144115061024',
			'100DK10010000001659370202111162228',
			'100DK100ZQ020220124093123963399024',
			'100DK100ZQ020220124090748845248024',
			'100DK10010000001659300202111162228',
			'100DK100ZQ020220124093147433839024',
			'100DK100ZQ020220124093146643070024',
			'100DK100ZQ020220124093145806437024',
			'100DK10010000001248290202011250630',
			'100DK10010000001248260202011250630',
			'100DK10010000001248250202011250630',
			'100DK10010000001659670202111162228',
			'100DK10010000001248330202011250630',
			'100DK10010000001413660202105130512',
			'100DK10010000001248360202011250630',
			'100DK100ZQ020220124093148982275024',
			'100DK10010000001659910202111162228',
			'100DK100ZQ020220124090759827858024',
			'100DK100ZQ020220214062204402797045',
			'100DK100ZQ020220124093149319561024',
			'100DK10010000001222780202010280537',
			'100DK10010000001248390202011250630',
			'100DK10010000001400780202105040816',
			'100DK100ZQ020220124093124884947024',
			'100DK100ZQ020220124093125637325024',
			'100DK10010000001397790202105040251',
			'100DK10010000001397490202105040251',
			'100DK100ZQ020220124093123601621024',
			'100DK10010000001248400202011250630',
			'100DK10010000001326510202102090603',
			'100DK10010000001326440202102090603',
			'100DK10010000001222800202010280537',
			'100DK10010000001222840202010280537',
			'100DK10010000000818120201908020935',
			'100DK10010000001222810202010280537',
			'100DK100ZQ020220124093133398639024',
			'100DK10010000000819000201908021008',
			'100DK10010000001222820202010280537')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@100DK10010000001248330202011250630,2022-03-20@22:00:00@100DK10010000001326440202102090603,2022-03-20@22:00:00@100DK10010000001400740202105040816,2022-03-20@22:00:00@100DK100ZQ020220124093126379703024,2022-03-20@22:00:00@100DK100ZQ020220124090756117378024,2022-03-20@22:00:00@100DK100ZQ020220124090755127099024,2022-03-20@22:00:00@100DK100ZQ020220124093126046040024,2022-03-20@22:00:00@100DK100ZQ020220124090754819827024,2022-03-20@22:00:00@100DK100ZQ020220124090754027240024,2022-03-20@22:00:00@100DK100ZQ020220124093144653882024,2022-03-20@22:00:00@100DK10010000001324400202102090603,2022-03-20@22:00:00@100DK10010000001659410202111162228,2022-03-20@22:00:00@100DK100ZQ020220124093123122384024,2022-03-20@22:00:00@100DK10010000001659250202111162228,2022-03-20@22:00:00@100DK100ZQ020220124093144395897024,2022-03-20@22:00:00@100DK100ZQ020220124093144115061024,2022-03-20@22:00:00@100DK10010000001659370202111162228,2022-03-20@22:00:00@100DK100ZQ020220124093123963399024,2022-03-20@22:00:00@100DK100ZQ020220124090748845248024,2022-03-20@22:00:00@100DK10010000001659300202111162228,2022-03-20@22:00:00@100DK100ZQ020220124093147433839024,2022-03-20@22:00:00@100DK100ZQ020220124093146643070024,2022-03-20@22:00:00@100DK100ZQ020220124093145806437024,2022-03-20@22:00:00@100DK10010000001248290202011250630,2022-03-20@22:00:00@100DK10010000001248260202011250630,2022-03-20@22:00:00@100DK10010000001248250202011250630,2022-03-20@22:00:00@100DK10010000001659670202111162228,2022-03-20@22:00:00@100DK10010000001248330202011250630,2022-03-20@22:00:00@100DK10010000001413660202105130512,2022-03-20@22:00:00@100DK10010000001248360202011250630,2022-03-20@22:00:00@100DK100ZQ020220124093148982275024,2022-03-20@22:00:00@100DK10010000001659910202111162228,2022-03-20@22:00:00@100DK100ZQ020220124090759827858024,2022-03-20@22:00:00@100DK100ZQ020220214062204402797045,2022-03-20@22:00:00@100DK100ZQ020220124093149319561024,2022-03-20@22:00:00@100DK10010000001222780202010280537,2022-03-20@22:00:00@100DK10010000001248390202011250630,2022-03-20@22:00:00@100DK10010000001400780202105040816,2022-03-20@22:00:00@100DK100ZQ020220124093124884947024,2022-03-20@22:00:00@100DK100ZQ020220124093125637325024,2022-03-20@22:00:00@100DK10010000001397790202105040251,2022-03-20@22:00:00@100DK10010000001397490202105040251,2022-03-20@22:00:00@100DK100ZQ020220124093123601621024,2022-03-20@22:00:00@100DK10010000001248400202011250630,2022-03-20@22:00:00@100DK10010000001326510202102090603,2022-03-20@22:00:00@100DK10010000001326440202102090603,2022-03-20@22:00:00@100DK10010000001222800202010280537,2022-03-20@22:00:00@100DK10010000001222840202010280537,2022-03-20@22:00:00@100DK10010000000818120201908020935,2022-03-20@22:00:00@100DK10010000001222810202010280537,2022-03-20@22:00:00@100DK100ZQ020220124093133398639024,2022-03-20@22:00:00@100DK10010000000819000201908021008,2022-03-20@22:00:00@100DK10010000001222820202010280537

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@100DK10010000001248250202011250630
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 4273	2022-03-20	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659210202111162228',
		'100DK100ZQ020220124093149795094024',
		'100DK100ZQ020220124093131854524024'
		spider issue -1 values		
		Yes	Turki

		# solution
			# select * from view_all_productdata where date='2022-03-20' and item_id in ('100DK10010000001659210202111162228',
			'100DK100ZQ020220124093149795094024',
			'100DK100ZQ020220124093131854524024')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@100DK10010000001659210202111162228,2022-03-20@22:00:00@100DK100ZQ020220124093131854524024,2022-03-20@22:00:00@100DK100ZQ020220124093149795094024

			# rerun successful
			# done react, done sheet

	-----
	(R D-3) 2761	2022-03-20	New CMS	200	all	www.fnac.com	
		'casque audio reducteur de bruit',
		'ecouteurs sans fil bluetooth',
		'enceinte connectée'

		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 5 (Fetched Data is Less than expected)	

		https://prnt.sc/6S-4uvIkZb-4
		https://prnt.sc/PpPWjECFYE7e
		https://prnt.sc/KWSxsCuFJGbn	

		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-20' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'casque audio reducteur de bruit',
						'ecouteurs sans fil bluetooth',
						'enceinte connectée')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@ae24c711-4727-4acf-a2a7-6d912e2df6d6@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@ae24c711-4727-4acf-a2a7-6d912e2df6d6@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@ae24c711-4727-4acf-a2a7-6d912e2df6d6@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@6c9dadaf-40e5-4322-a18a-3dc02112397e@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA

			# multiple reruns successful
			# done react, sheet

	(R D-2) 2762	2022-03-20	New CMS	200	all	www.re-store.ru	
		/ акустика wi-fi - 8
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники вкладыши - 2
			513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники с проводом - 18
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3	
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 1 (Fetched Data is Less than expected)
		Data Count Mismatch 9 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)	

		https://prnt.sc/8Zvcnb3Ctb-_
		https://prnt.sc/Xs2o95WFP-U5
		https://prnt.sc/2Ryxq6o6yXaa
		https://prnt.sc/9tDKP9RglfP9	

		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-20' 
				AND website = 'www.re-store.ru' 
				AND keyword in (N'портативная акустика wi-fi')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-20@21:00:00@200@513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-20@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-20@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-20@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l  2022-03-20@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple rerun successful
			# done react, done sheet

	(R D-2) 2763	2022-03-20	New CMS	200	all	www.fnac.com	
		/ 'ecouteur bluetooth sans fil',
		/ 'enceinte bluetooth'
		Auto Copy Over for 3 Days	

		https://prnt.sc/7IDueCtjFokS
		https://prnt.sc/i9-IbgdeWG01	

		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-20' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'ecouteur bluetooth sans fil',
						'enceinte bluetooth')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@200@0f6c899b-566b-4912-9e18-b72e7fd8ec84@LzP6XApHEem-gAANOiOHiA,2022-03-20@22:00:00@200@0e47abb4-4b56-4668-b6ca-21842b1b99be@LzP6XApHEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@200@0e47abb4-4b56-4668-b6ca-21842b1b99be@LzP6XApHEem-gAANOiOHiA
			# rerun successful
			# done react, done sheet

	(R D-1) 2765	2022-03-20	New CMS	200	all	www.bestbuy.com	
		'bluetooth headphones',	
			77334dbc-cbf7-449c-948e-f69cc9a2a25c@33sMLvcNEeaOEQANOrFolw
		'waterproof bluetooth speaker'
			087eb575-20cd-49f2-a055-db104d556256@33sMLvcNEeaOEQANOrFolw

		Duplicate product_website & product_url
		https://prnt.sc/MAxXHn1Io8C7
		https://prnt.sc/4a3TezcLUOON	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bestbuy.com' and 
				var2.keyword in (N'waterproof bluetooth speaker') and 
				var2.[date] = '2022-03-20' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-20@04:00:00@200@77334dbc-cbf7-449c-948e-f69cc9a2a25c@33sMLvcNEeaOEQANOrFolw,2022-03-20@04:00:00@200@087eb575-20cd-49f2-a055-db104d556256@33sMLvcNEeaOEQANOrFolw

			# rerun succcessful
			# done react, done sheet

	-----
	(R D-1) 937	2022-03-20	New CMS	100	all	www.dustinhome.se	
		Wearables	
		Invalid Deadlink	
		https://prnt.sc/QzdhX5Qy7TIB	Yes	Rhunick

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-20' 
				AND website = 'www.dustinhome.se' 
				AND category in (N'Wearables')
				GROUP BY category, litem_id, country, category_url, source		

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@100@L10@f227664b-0f94-480c-882c-31e0e2412087
			# successful rerun
			# done react, done sheet

	(R D-1) 938	2022-03-20	New CMS	100	all	www.elgiganten.se	
		Smartwatch	
		1 Day Auto Copy Over	
		https://prnt.sc/qRtYayqv_0ZN	
		Yes	Rhunick

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-20' 
				AND website = 'www.elgiganten.se' 
				AND category in (N'Smartwatch')
				GROUP BY category, litem_id, country, category_url, source				

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-20@22:00:00@100@300@67e7095b-8e39-4c88-8300-8a292de5b2d5
			# rerun successful
			# done react, done sheet
	-----
	Stats:
		No commits
		Rerun: 10 (14) = 1.4
		webshots = 5

03 23 22
	(F D-1) Trello 1:
		Please fix spyder to add spacing on the next line text

		[NINTENDO BR - U00] Compliance - Spacing issue in description in 

		date: 2022-03-19
		retailer: www.shoptime.com.br
		item_id: all
		sample item_id: U00BRF90OL000001302610202101200146
		# https://trello.com/c/W8QYfwnj/3495-nintendo-br-compliance-spacing-issue-in-description-in-wwwshoptimecombr
		# https://www.shoptime.com.br/produto/3452217999?pfm_carac=nintendo-switch-com-joy-con-cinza-a&pfm_index=6&pfm_page=search&pfm_pos=grid&pfm_type=search_page


		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-22' AND website = 'www.shoptime.com.br'	AND item_id in ('U00BRF90OL000001302610202101200146')
		
			# fixed through stripped_strings
			# pushed
			# trello moved commented > moved to rebuild
			# 

	(F D-1) Trello 2:
		[NINTENDO BR - U00] Compliance - Special character issue in description in 
		Please fix spyder to add spacing on the next line text

		Please fix the special character issue in description

		date: 2022-03-19
		retailer: www.kabum.com.br
		sample item_id: U00BRUW0TV020211109125509480824313

		# https://trello.com/c/CP6ps0P4/3494-nintendo-br-compliance-special-character-issue-in-description-in-wwwkabumcombr
		# https://www.kabum.com.br/produto/135586/nintendo-switch-32gb-1x-joycon-neon-azul-vermelho-hbdskaba1

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-22' AND website = 'www.kabum.com.br'	AND item_id in ('U00BRUW0TV020211109125509480824313')

			# fixed through stripped_strings
			# pushed
			# trello moved commented > moved to rebuild

	(F D-1) Trello 3:
		[NINTENDO MEXICO X00] : RANKINGS - Auto Copy Over in 
		Issue Description:
		Auto Copy Over for 3 Days
		Auto Copy Over for 2 Days
		Auto Copy Over for 1 Day

		As Per BE:
		Multiple Reruns / Investigate
		Requires more time to fix

		Date: 3-19-2022

		Retailer:
		www.sears.com.mx

		Keyword(s):
		Accesorios para Nintendo Switch
		Consola
		Consola Nintendo Lite
		Consola Nintendo Switch
		Consola Nintendo Switch Lite
		Consola Switch
		Consola Switch Lite
		Joy Con
		Nintendo Joy Con
		Nintendo Switch
		Nintendo Switch Lite
		Nintendo Switch Pro Controller
		Switch Nintendo
		Switch Nintendo Lite
		Switch OLED
		Nintendo switch OLED
		Nintendo Pro Controller

		Screenshot(s):
		https://prnt.sc/RyJ2u9V4YsBs
		https://prnt.sc/JYV5WQ_hyWYE
		https://prnt.sc/_gaxnsvSSZJx
		https://prnt.sc/99BI-lfdNL84

		# solution
			# fixed > rebuild
			
			# python rerun_rnk.py CRAWL_FAILED -l 2022-03-19@05:00:00@X00@8304e3b7-b53a-44d3-9082-dfc4c6170fbc@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# tested, good
			# moved to for rebuild > for retest

	-----
	(R D-1) 4402	2022-03-23	New CMS	K00	image_count	www.power.no	
		'K00NOZ204K000001554900202108180340',
		'K00NOZ20CG000001473500202107060432',
		'K00NOZ20CG000001473600202107060432'
		Missing image_count		
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-23' AND website = 'www.power.no'	AND item_id in ('K00NOZ204K000001554900202108180340',
			'K00NOZ20CG000001473500202107060432',
			'K00NOZ20CG000001473600202107060432')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-23@22:00:00@K00NOZ204K000001554900202108180340,2022-03-23@22:00:00@K00NOZ20CG000001473500202107060432,2022-03-23@22:00:00@K00NOZ20CG000001473600202107060432

			# rerun successful
			# done react, done sheet

	(R D-1) 4404	2022-03-23	New CMS	K00	image_count	www.power.se	
		K00SEY20CG000001473740202107060432
		K00SEY20CG000001473810202107060432	
		Missing image_count		
		Yes	Jurena

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-23' AND website = 'www.power.se'	AND item_id in ('K00SEY20CG000001473740202107060432',
			'K00SEY20CG000001473810202107060432')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-23@22:00:00@K00SEY20CG000001473740202107060432,2022-03-23@22:00:00@K00SEY20CG000001473810202107060432

			# rerun successful
			# done react, done sheet

	(R D-1) 4409	2022-03-23	New CMS	200	all	amazon 3p	
		'200ESFF080000000556470201904220811',
		'200ESFF080000000556590201904220816',
		'200ESFF070000000556830201904220841',
		'200ESFF080000000801620201906200930',
		'200ITGF080000000556600201904220816',
		'200ITGF080000000556480201904220811',
		'200ITGF080000000801650201906200930'	
		Invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-23' AND item_id in ('200ESFF080000000556470201904220811',
				'200ESFF080000000556590201904220816',
				'200ESFF070000000556830201904220841',
				'200ESFF080000000801620201906200930',
				'200ITGF080000000556600201904220816',
				'200ITGF080000000556480201904220811',
				'200ITGF080000000801650201906200930')

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-23@22:00:00@200ESFF080000000556470201904220811,2022-03-23@22:00:00@200ITGF080000000556480201904220811,2022-03-23@22:00:00@200ESFF080000000556590201904220816,2022-03-23@22:00:00@200ITGF080000000556600201904220816,2022-03-23@22:00:00@200ESFF070000000556830201904220841,2022-03-23@22:00:00@200ESFF080000000801620201906200930,2022-03-23@22:00:00@200ITGF080000000801650201906200930

			# multiple reruns failed
			# check site: valid deadlink, no 3p button
			# check local: 
			# valid deadlink: 
				# https://prnt.sc/SvVwYSlJ3fRZ
				# assisted by sir Ryan

	0 (R D-1) 4413	2022-03-23	New CMS	100	delivery	elgiganten.dk	
		all item_ids except: 
		'100DK10010000001222840202010280537',
		'100DK10010000000821000201908050425',
		'100DK10010000001397940202105040251'
		-1 delivery		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-23' AND website = 'www.elgiganten.dk'AND item_id not in ('100DK10010000001222840202010280537',
			'100DK10010000000821000201908050425',
			'100DK10010000001397940202105040251')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-23@22:00:00@100DK100ZQ020220124090753325822024,2022-03-23@22:00:00@100DK100ZQ020220124090753449368024,2022-03-23@22:00:00@100DK100ZQ020220124090753577760024,2022-03-23@22:00:00@100DK100ZQ020220124090753733277024,2022-03-23@22:00:00@100DK100ZQ020220124090753862646024,2022-03-23@22:00:00@100DK100ZQ020220124090754027240024,2022-03-23@22:00:00@100DK100ZQ020220124090754197223024,2022-03-23@22:00:00@100DK100ZQ020220124090754523897024,2022-03-23@22:00:00@100DK100ZQ020220124090754656295024,2022-03-23@22:00:00@100DK100ZQ020220124090754819827024,2022-03-23@22:00:00@100DK100ZQ020220124090754985166024,2022-03-23@22:00:00@100DK100ZQ020220124090755127099024,2022-03-23@22:00:00@100DK100ZQ020220124090755315930024,2022-03-23@22:00:00@100DK100ZQ020220124090756117378024,2022-03-23@22:00:00@100DK100ZQ020220124090759827858024,2022-03-23@22:00:00@100DK100ZQ020220124093123122384024,2022-03-23@22:00:00@100DK100ZQ020220124093123601621024,2022-03-23@22:00:00@100DK100ZQ020220124093123963399024,2022-03-23@22:00:00@100DK100ZQ020220124093124632102024,2022-03-23@22:00:00@100DK100ZQ020220124093124884947024,2022-03-23@22:00:00@100DK100ZQ020220124093125206089024,2022-03-23@22:00:00@100DK100ZQ020220124093125637325024,2022-03-23@22:00:00@100DK100ZQ020220124093126046040024,2022-03-23@22:00:00@100DK100ZQ020220124093126211631024,2022-03-23@22:00:00@100DK100ZQ020220124093126379703024,2022-03-23@22:00:00@100DK100ZQ020220124093127658423024,2022-03-23@22:00:00@100DK100ZQ020220124093131854524024,2022-03-23@22:00:00@100DK100ZQ020220124093132423284024,2022-03-23@22:00:00@100DK100ZQ020220124093132905871024,2022-03-23@22:00:00@100DK100ZQ020220124093133398639024,2022-03-23@22:00:00@100DK100ZQ020220124093144115061024,2022-03-23@22:00:00@100DK100ZQ020220124093144395897024,2022-03-23@22:00:00@100DK100ZQ020220124093144653882024,2022-03-23@22:00:00@100DK100ZQ020220124093145806437024,2022-03-23@22:00:00@100DK100ZQ020220124093146232901024,2022-03-23@22:00:00@100DK100ZQ020220124093146643070024,2022-03-23@22:00:00@100DK100ZQ020220124093147040174024,2022-03-23@22:00:00@100DK100ZQ020220124093147433839024,2022-03-23@22:00:00@100DK100ZQ020220124093147829564024,2022-03-23@22:00:00@100DK100ZQ020220124093148226755024,2022-03-23@22:00:00@100DK100ZQ020220124093148982275024,2022-03-23@22:00:00@100DK100ZQ020220124093149555986024,2022-03-23@22:00:00@100DK100ZQ020220124093149795094024,2022-03-23@22:00:00@100DK100ZQ020220207085518362413038,2022-03-23@22:00:00@100DK100ZQ020220214062204402797045,2022-03-23@22:00:00@100DK100ZQ020220214062205360488045,2022-03-23@22:00:00@100DK100ZQ020220214062210619118045,2022-03-23@22:00:00@100DK100ZQ020220214062211529844045,2022-03-23@22:00:00@100DK10010000000813290201908020245,2022-03-23@22:00:00@100DK10010000000813760201908020319,2022-03-23@22:00:00@100DK10010000000814140201908020341,2022-03-23@22:00:00@100DK10010000000816740201908020830,2022-03-23@22:00:00@100DK10010000000818120201908020935,2022-03-23@22:00:00@100DK10010000000818230201908020936,2022-03-23@22:00:00@100DK10010000000819000201908021008,2022-03-23@22:00:00@100DK10010000001222720202010280537,2022-03-23@22:00:00@100DK10010000001222770202010280537,2022-03-23@22:00:00@100DK10010000001222780202010280537,2022-03-23@22:00:00@100DK10010000001222790202010280537,2022-03-23@22:00:00@100DK10010000001222800202010280537,2022-03-23@22:00:00@100DK10010000001222810202010280537,2022-03-23@22:00:00@100DK10010000001222820202010280537,2022-03-23@22:00:00@100DK10010000001248210202011250630,2022-03-23@22:00:00@100DK10010000001248230202011250630,2022-03-23@22:00:00@100DK10010000001248250202011250630,2022-03-23@22:00:00@100DK10010000001248260202011250630,2022-03-23@22:00:00@100DK10010000001248290202011250630,2022-03-23@22:00:00@100DK10010000001248310202011250630,2022-03-23@22:00:00@100DK10010000001248330202011250630,2022-03-23@22:00:00@100DK10010000001248360202011250630,2022-03-23@22:00:00@100DK10010000001248390202011250630,2022-03-23@22:00:00@100DK10010000001248400202011250630,2022-03-23@22:00:00@100DK10010000001324180202102080716,2022-03-23@22:00:00@100DK10010000001324310202102090603,2022-03-23@22:00:00@100DK10010000001324400202102090603,2022-03-23@22:00:00@100DK10010000001324440202102090603,2022-03-23@22:00:00@100DK10010000001324630202102090603,2022-03-23@22:00:00@100DK10010000001326370202102090603,2022-03-23@22:00:00@100DK10010000001326440202102090603,2022-03-23@22:00:00@100DK10010000001326510202102090603,2022-03-23@22:00:00@100DK10010000001327030202102090604,2022-03-23@22:00:00@100DK10010000001397490202105040251,2022-03-23@22:00:00@100DK10010000001397640202105040251,2022-03-23@22:00:00@100DK10010000001397790202105040251,2022-03-23@22:00:00@100DK10010000001400700202105040816,2022-03-23@22:00:00@100DK10010000001400740202105040816,2022-03-23@22:00:00@100DK10010000001400780202105040816,2022-03-23@22:00:00@100DK10010000001413660202105130512,2022-03-23@22:00:00@100DK10010000001438730202106080416,2022-03-23@22:00:00@100DK10010000001659210202111162228,2022-03-23@22:00:00@100DK10010000001659240202111162228,2022-03-23@22:00:00@100DK10010000001659250202111162228,2022-03-23@22:00:00@100DK10010000001659300202111162228,2022-03-23@22:00:00@100DK10010000001659370202111162228,2022-03-23@22:00:00@100DK10010000001659390202111162228,2022-03-23@22:00:00@100DK10010000001659410202111162228,2022-03-23@22:00:00@100DK10010000001659670202111162228,2022-03-23@22:00:00@100DK10010000001659910202111162228,2022-03-23@22:00:00@100DK10010000001660000202111162228,2022-03-23@22:00:00@100DK10010000001660410202111162228,2022-03-23@22:00:00@100DK100ZQ020220124090748845248024

			# check site: no issues or delivery details has value
			# check local: 
				# querySelectors in local for delivery has no data
				# created new query selector but cant properly fix in local because the site wont load properly (timeout issue in local end)
				# pushed instead and try to rerun after rebuild because the site might properly load in main: causing it get data from the newly created query selector
				# fix unsuccessul after rerun. Delivery not fetched
				# added to trello

	-----
	(R D-1) 2812	2022-03-23	New CMS	K00	all	www.box.co.uk	
		'Streaming Microphone',
		'Microphone',
		'Streaming',
		'best headphones',
		'anc headphones',
		'Ps4 Controller',
		'chair',
		'Office Chair',
		'Gaming Chair',
		'Mousemat',
		'2.1 Speakers',
		'Gaming Speakers',
		'Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Notebook',
		'Laptop',
		'mechanical keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Gaming Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse',
		'Gaming Mouse'
		Auto copy over for 1 day	

		https://prnt.sc/YhV9kEa-YXjT
		https://prnt.sc/dVJ09J5hR2Sg
		https://prnt.sc/URkYe900FQep
		https://prnt.sc/5ElnFJ6TIiTu
		https://prnt.sc/VFFGNrzYIhzZ
		https://prnt.sc/U0nMaePQ9_rZ
		https://prnt.sc/Ae_ijfvGKYud
		https://prnt.sc/GoCfjvZRsmOA
		https://prnt.sc/CC6bGPgdsbdi	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
			AND website = 'www.box.co.uk' 
			AND keyword in (N'Streaming Microphone',
					'Microphone',
					'Streaming',
					'best headphones',
					'anc headphones',
					'Ps4 Controller',
					'chair',
					'Office Chair',
					'Gaming Chair',
					'Mousemat',
					'2.1 Speakers',
					'Gaming Speakers',
					'Speakers',
					'Keypad',
					'PC Keypad',
					'Gaming Keypad',
					'Notebook',
					'Laptop',
					'mechanical keyboard',
					'Keyboard',
					'PC Keyboard',
					'Gaming Keyboard',
					'Wireless Headset',
					'PC Headset',
					'Gaming Headset',
					'Wireless Mouse',
					'Mouse',
					'PC Mouse',
					'Gaming Mouse')
			GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-23@23:00:00@K00@f54708f2-399f-43af-bfb8-20d00c1f294b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@ef6e28af-4210-4946-b21a-daeaa5c4ed38@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@d29b8e26-ffcc-4bda-a5ec-5681de160607@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@918d322f-2c44-4c9c-aeed-5d7376531f44@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@b73ac43d-b22d-4fa8-9dd4-58e266c9e7f1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@52860e11-50ef-4f3e-923f-7ad398628342@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@22bf8c3a-b678-43b0-811b-5aa60c02eb08@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@88d127ad-cc16-4b9e-9746-7f2fda21d7ed@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@e6b610ec-314c-44b2-81b0-6934b661917c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@a88e5833-304b-411f-b354-3e02fbd9486a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@4ece2ecd-1087-42f9-a425-0435928a0b47@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@c2607c9f-2e9e-4e0f-b4ec-0b14c67e9345@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@287f5d68-29fb-4230-8e11-b4ec207c0b9a@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@e88ea211-dbf7-43af-afbd-c122b70031b6@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@bae74ad9-7e9d-4ed4-a99c-497ef7383e1e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@d66f0c45-669a-40f6-a642-79e628732743@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@d57f49ea-2101-46e4-9b69-85c55112ad1b@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@05d84a59-bd95-4bf2-903a-ef0f90c004ff@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@cedc7874-47f7-41e8-81c6-002faff89723@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@b20c5fbe-7824-4b8e-b054-857117d42119@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@d951b742-2201-4753-b46a-871f3615bb69@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@b82d2b40-abf7-49b9-9a73-680f0ffbe1e1@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@21959252-c63d-402e-babc-d9467671756c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@e89d5de5-2856-4a30-ad6f-bc60e531eb6e@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@c9312ba4-36f7-4945-8368-fa16aceef83c@f12e58ca-40a5-4a03-90cc-7ed815f99306,2022-03-23@23:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@f12e58ca-40a5-4a03-90cc-7ed815f99306

			# successful rerun
			# done react, done sheet

	(R D-1) 2813	2022-03-23	New CMS	K00	all	www.emag.ro	
		Casti Pc	
		Auto copy over for 1 day	
		https://prnt.sc/eR5k_qIc5I9j	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
				AND website = 'www.emag.ro' 
				AND keyword in (N'Casti Pc')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-23@21:00:00@K00@fb31340c-fa75-4e97-a9ba-ade0ca9194a9@qCsk7EXdEeeMawANOiZi-g
			# successful rerun
			# done react, done sheet

	(R D-1) 2814	2022-03-23	New CMS	K00	all	www.alza.cz
		'Wireless Mouse',
		'Klávesnice'
		Data Count Mismatch 1 (Fetched Data is Less than expected)	
		https://prnt.sc/o1KEKXTpFN26
		https://prnt.sc/Q-loz4iBRL7X	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
				AND website = 'www.alza.cz' 
				AND keyword in (N'Wireless Mouse',
						'Klávesnice')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-23@22:00:00@K00@b13dc855-fe78-4a78-97c2-cde983ecb2e8@1cAhckXZEeeMawANOiZi-g,2022-03-23@22:00:00@K00@66a5569d-9a0c-49a9-9206-fb473087a7af@1cAhckXZEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	(R D-1) 2815	2022-03-23	New CMS	200	all	www.dns-shop.ru
		наушники накладные беспроводные с bluetooth	
		invalid deadlink 	
		https://prnt.sc/GVVEzr_k-F1X	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
				AND website = 'www.dns-shop.ru' 
				AND keyword in (N'наушники накладные беспроводные с bluetooth')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-23@21:00:00@200@e4842b4e-bd1e-4aaa-8054-33faad7516b6@BzxplkXeEeeMawANOiZi-g
			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2816	2022-03-23	New CMS	200	all	www.re-store.ru	
		'портативная акустика wi-fi', - 3 
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		'портативная колонка wi-fi', - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		'проводные наушники' - 16
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 1 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)	

		https://prnt.sc/SZkIAfApPujO
		https://prnt.sc/g7kbkBm9kgrG
		https://prnt.sc/yhrjMRVK3-Fm	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
				AND website = 'www.re-store.ru' 
				AND keyword in (N'портативная акустика wi-fi',
						'портативная колонка wi-fi',
						'проводные наушники')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-23@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-23@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-23@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# rerun successful
			# done react, done sheet

	(R D-1) 2817	2022-03-23	New CMS	200	all	www.dns-shop.ru	
		'наушники внутриканальные беспроводные bluetooth',
			0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g
		'наушники накладные bluetooth',
			4df6b44c-9ad7-40e1-bca8-3569af8ba83e@BzxplkXeEeeMawANOiZi-g
		'проводные наушники'
			bfed8a03-9385-4010-9dec-9360fe56bc5b@BzxplkXeEeeMawANOiZi-g

		Duplicate product_website & product_url	
		https://prnt.sc/PvAjM9sd7Od8	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'проводные наушники') and 
				var2.[date] = '2022-03-23' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-23@21:00:00@200@0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g,2022-03-23@21:00:00@200@4df6b44c-9ad7-40e1-bca8-3569af8ba83e@BzxplkXeEeeMawANOiZi-g,2022-03-23@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	(R D-1) 2818	2022-03-23	New CMS	200	all	www.bol.com	
		smart speaker	
		Duplicate product_website & product_url	
		https://prnt.sc/wNxxzOpOKeGu	
		Yes	Phil

		# soilution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.bol.com' and 
				var2.keyword in (N'smart speaker') and 
				var2.[date] = '2022-03-23' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-23@22:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@UAjszpOqEei-eQANOiOHiA
			# successful rerun
			# done react, done sheet

	(R D-1) 2827	2022-03-23	New CMS	K00	all	euro.com.pl	
		Klawiatura PC
		Auto copyover for 1 day 	
		https://prnt.sc/SUesgEAGXfIr	
		Yes	keen

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-23' 
				AND website = 'www.euro.com.pl' 
				AND keyword in (N'Klawiatura PC')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-23@22:00:00@K00@fe2a98b5-4cb9-4f9f-8be2-3550a66832b9@EQvPbkXbEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet	
	----
	(R D-1) 965	2022-03-23	New CMS	200	all	www.re-store.ru	
		'headphones bluetooth on ear', - 25
		'headphones noise cancelling', - 25
		'headphones wired in ear', - 7
		'speakers multiroom', - 25
		'speakers portable' - 25

		Data Count Mismatch 13 (Fetched Data is Less than expected)
		Data Count Mismatch 1 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 4 (Fetched Data is Less than expected)
		Data Count Mismatch 1 (Fetched Data is Less than expected)	

		https://prnt.sc/5u6pEQt8Suup
		https://prnt.sc/c9kWH1-9IIGc
		https://prnt.sc/iPgGoYO7s9Ae
		https://prnt.sc/xcEJmcF82gxg
		https://prnt.sc/ZOlJV-Dq2SyP	

		Yes	kurt

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-23'
				AND website = 'www.re-store.ru' 
				AND category in (N'headphones bluetooth on ear',
						'headphones noise cancelling',
						'headphones wired in ear',
						'speakers multiroom',
						'speakers portable')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-23@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-23@21:00:00@200@LQ0@9a67dfe8-7a48-4a2e-a92e-76526cff38c0,2022-03-23@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-03-23@21:00:00@200@LQ0@a2d70d9e-fd80-4527-91d6-f1d64b5f903d,2022-03-23@21:00:00@200@LQ0@1bd997f1-d6fc-4227-aded-7344868512eb

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-23@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-23@21:00:00@200@LQ0@a2d70d9e-fd80-4527-91d6-f1d64b5f903d,2022-03-23@21:00:00@200@LQ0@1bd997f1-d6fc-4227-aded-7344868512eb

			# multiple reruns successful
			# done react, done sheet
	----
	STATS:
	commits: 
		(1) # https://trello.com/c/W8QYfwnj/3495-nintendo-br-compliance-spacing-issue-in-description-in-wwwshoptimecombr
		(1) # https://trello.com/c/CP6ps0P4/3494-nintendo-br-compliance-special-character-issue-in-description-in-wwwkabumcombr
		(1) # https://trello.com/c/Qk8sqDyp/3496-nintendo-mexico-rankings-auto-copy-over-in-wwwsearscommx

		Rerun: 13
		Fix: 3[1]

03 24 22
	(F D-3) Trello 1
		[MOTOROLA - P00] Listings: Invalid product website and url in telekom.sk
		Issue Description: Some of the data of these categories have -1 on product website & url.

		Retailer:
		www.telekom.sk

		Categories:
		All smartphones
			# https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806

			# P00@XU0@d81ade30-369d-4675-93d9-380cbe0912a8
		5G Smartphones
			# https://eshop.telekom.sk/category/all-devices/list/product_listing/product/iPhone-13?variantId=RP1140&agreementId=agreement24&bp=acquisition&variantId=HW3209&filter%5B%5D=filter.device_category%5B%5D%3D5g&deviceCategoryName=

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-22@22:00:00@P00@XU0@1b81bdeb-d684-4a50-aca1-df4b70bb538d

		Listings UUID:
		d81ade30-369d-4675-93d9-380cbe0912a8
		1b81bdeb-d684-4a50-aca1-df4b70bb538d

		Screenshot:
		https://prnt.sc/cL5lH-12lPjq
		https://prnt.sc/7izJaoLkXbk9

		Date:
		March 22, 2022
		# https://trello.com/c/JTvOcNin/3510-motorola-listings-invalid-product-website-and-url-in-telekomsk

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-22' AND website = 'www.telekom.sk' AND category in ('All smartphones') order by rank
			# fix: resort to api
			# moved to for rebuild
			
			# test
				# All smartphones category works well
				# 5G Smartphones category > fix > rebuild > for test
					# successful rerun

	-----

	(w) EMEA region (A00) [08:35 started]
		# 1127
		# 1031	100	96.00%
		# 978	101	52.48%
		# 917	278	21.94%
		# 809	108	100.00%
		# 708	101	100.00%
		# 634	101	73.27%
		# 547	101	86.14%
		# 523	101	23.76%
		# 450	73	100.00%
		# 378	74	97.30%
		# 286	94	97.87%
		# 261	101	24.75%
		# 192	202	34.16%
		# 134	167	34.73%
		# 107	75	36.00%
		# 85	25	88.00%
		# updated validator
		# done react

	-----
	(w) 4438	2022-03-24	New CMS	Q00	webshot_url		
		select * from view_all_productdata where date = '2022-03-24' and webshot_url is NULL	
		No webshots		
		Yes	MARK

		# solution
			# 89
			# 0

			# done react, done sheet
			
	(w) 4439	2022-03-24	New CMS	P00	webshot_url		
		select * from view_all_productdata where date = '2022-03-24' and webshot_url is NULL	
		No webshots		
		Yes	MARK

		# solution
			# 222
			# 43
			# 20
			# updated validator
			# done react, done sheet

	(w) 4440	2022-03-24	New CMS	K00	webshot_url		
		select * from view_all_productdata where date = '2022-03-24' and webshot_url is NULL	
		No webshots		
		Yes	mARYEEEEL

		# solution
			# 1423
			# 1336
			# 1099
			# 913
			# 823
			# 703
			# 672
			# 564
			# 484
			# 438
			# 346
			# 215
			# 208
			# 125
			# 112
			# updated validator
			# done react, done sheet

	(w) 4445	2022-03-24	New CMS	610	webshot_url		
		select * from view_all_productdata where date = '2022-03-24' and webshot_url is NULL	
		No webshots		
		Yes	mARYEEEEL

		# solution
			# 599
			# 548
			# 465
			# 163
			# 94
			# updated validator
			# done react, done sheet

	(w) 4446	2022-03-24	New CMS	H10	webshot_url		
		select * from view_all_productdata where date='2022-03-24' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com') and webshot_url is NULL order by item_id	No Webshots		
		Yes	Maryeeel

		# solution
			# select item_id, source from view_all_productdata where date='2022-03-24' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com') and webshot_url is NULL and source = 'etl' order by item_id

			# 0
			# done react, done sheet

	---------+
	stats:
		commits:
			# (1) https://trello.com/c/JTvOcNin/3510-motorola-listings-invalid-product-website-and-url-in-telekomsk
				# ws and dl strat (api)
			# (1) https://trello.com/c/JTvOcNin/3510-motorola-listings-invalid-product-website-and-url-in-telekomsk
				# category url update

		Fix: 1[3]
		webshots: 6 


03 25 22
	
	(w) NA (A00)
		# 503
		# 437
		# 380
		# 328
		# 261

	-----
	(w) 4455	2022-03-25	New CMS	O00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL	
		No webshots		
		Yes	Have

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL order by item_id
			
			# 272
			# 31
			# 9
			# 3
			# 3
			# updated validator
			# done react, done sheet

	(w) 4456	2022-03-25	New CMS	100	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL	
		no webshots		
		Yes	Turki

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL order by item_id 
			
			# 1048
			# 947
			# 846
			# 647
			# 107
			# 0
			# done react, done sheet

	(w) 4457	2022-03-25	New CMS	F00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL
		no webshots		
		Yes	Maryeeel

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL order by item_id 
			
			# 1335
			# 1002
			# 723
			# 505
			# 319
			# 63
			# 3
			# 0
			# done react, done sheet

	(w) 4459	2022-03-25	New CMS	P00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL	
		no webshots		
		Yes	Caesa	

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL order by item_id 

			# 197
			# 22
			# 1
			# 1
			# updated validator
			# done react, done sheet

	(w) 4461	2022-03-25	New CMS	K00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL	
		no webshots		
		Yes	Maryeeel

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL and source = 'etl' order by item_id 

			# 1432
			# 1164
			# 855
			# 678
			# 634
			# 557
			# 464
			# 406
			# 355
			# 283
			# 234
			# 208
			# updated validator
			# done react, done sheet

	(w) 4462	2022-03-25	New CMS	A10	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL	
		No Webshots		
		Yes	Have

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-25' and webshot_url is NULL and source = 'etl' order by item_id 

			# 53
			# 5
			# 0
			# done react, done sheet

	(R D-1) 4468	2022-03-25	New CMS	200	all	fnac.com/3P	
		'200FRDU0H0000001381180202104140811',
		'200FRDU070000001381320202104140811'
		invalid deadlink		
		Yes	Mojo

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-25' AND website = 'www.fnac.com/3P'	AND item_id in ('200FRDU0H0000001381180202104140811',
			'200FRDU070000001381320202104140811')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-25@22:00:00@200FRDU0H0000001381180202104140811,2022-03-25@22:00:00@200FRDU070000001381320202104140811

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-25@22:00:00@200FRDU0H0000001381180202104140811,2022-03-25@22:00:00@200FRDU070000001381320202104140811

			# multiple reruns with sir Ryan
			# done react, done sheet

	(w) 4473	2022-03-25	New CMS	H10	webshot_url		
		select * from view_all_productdata where date='2022-03-25' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com')and webshot_url is NULL order by item_id	
		No Webshots		
		Yes	Keeshia

		# solution
			# select item_id, source from view_all_productdata where date='2022-03-25' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com')and webshot_url is NULL order by item_id	

			# 293
			# 193
			# 18
			# 0
			# done react, done sheet

	(R D-1) 4476	2022-03-25	New CMS	F00	webshot_url	mobiel.nl	
		all item_ids	
		delivery on webshots does not match scraped and live data.		
		Yes	Turki

		# solution
			# 

			# just execute re webshots
			# 
	-----
	0 (R D-1) 2850	2022-03-25	New CMS	Q00	all	www.bergzeit.de	
		'Freizeithosen frauen',
		'Regenhosen frauen',
		'Skijacken frauen',
		'Windbreaker frauen'

		Auto copyover for 3 days 

		https://prnt.sc/ilXdhCNtA5E1
		https://prnt.sc/uUs-cLK0Z5vk
		https://prnt.sc/MkiCHXWVcCeE
		https://prnt.sc/ycVr11foHvz8	
		Yes	keen

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-25' 
				AND website = 'www.bergzeit.de' 
				AND keyword in (N'Freizeithosen frauen',
						'Regenhosen frauen',
						'Skijacken frauen',
						'Windbreaker frauen')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-25@22:00:00@Q00@dcb6bf19-708b-4c58-98f7-3bf94f3dc2ed@bd8b190f-904c-4b92-aeb9-b1657b260972,2022-03-25@22:00:00@Q00@c354249d-6d80-4593-be25-1cba453eba33@bd8b190f-904c-4b92-aeb9-b1657b260972,2022-03-25@22:00:00@Q00@b1866e72-917a-458c-9f73-b1bcfbf36d6d@bd8b190f-904c-4b92-aeb9-b1657b260972,2022-03-25@22:00:00@Q00@2f4d9d4f-f5d3-4964-bb1f-c10c34c36ee7@bd8b190f-904c-4b92-aeb9-b1657b260972
			# trello
			# 

	(R D-1) 2859	2022-03-25	New CMS	Z00	all	www.walmart.com	
		receiver	
		Auto Copy Over for 1 Day	
		https://prnt.sc/SAXIWQo5q8W3	
		yes	rod

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-25' 
				AND website = 'www.walmart.com' 
				AND keyword in (N'receiver')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-25@04:00:00@Z00@c2488cd9-a106-4aa2-9f0f-52e1154aed13@rPZW6r-yEei-eQANOiOHiA
			# multiple reruns successful
			# done react, done sheet
	-----
	(R D-1) 981	2022-03-25	New CMS	200	all	www.amazon.co.uk	
		'Headphones Bluetooth In Ear',
			# https://www.amazon.co.uk/s/ref=lp_4085751_nr_p_n_style_browse-bin_0?fst=as%3Aoff&rh=n%3A560798%2Cn%3A%21560800%2Cn%3A1345741031%2Cn%3A560882%2Cn%3A4085731%2Cn%3A4085751%2Cp_n_style_browse-bin%3A1601093031&bbn=4085751&ie=UTF8&qid=1547558922&rnid=1600975031
			# 200@G00@bd91be1f-2495-49d5-8220-906f55242bd6
		'Headphones Bluetooth On Ear'
			# https://www.amazon.co.uk/s/s/ref=sr_nr_p_n_style_browse-bin_2?fst=as%3Aoff&rh=n%3A560798%2Cn%3A%21560800%2Cn%3A1345741031%2Cn%3A560882%2Cn%3A4085731%2Cn%3A4085751%2Cp_n_style_browse-bin%3A1601094031%7C1601095031&bbn=4085751&ie=UTF8&qid=1547558993&rnid=1600975031
			# 200@G00@a3b64247-6b9d-4f54-818d-b88889214619

		Fetching data even if its deadlink	

		https://prnt.sc/A9tFRbYgoq9f
		https://prnt.sc/oreCjkauZrgc	

		Yes	Jairus

		# solution
			# SELECT * FROM view_all_listings_data WHERE date = '2022-03-25' AND website = 'www.amazon.co.uk' AND category in ('Headphones Bluetooth On Ear')	

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-25@23:00:00@200@G00@a3b64247-6b9d-4f54-818d-b88889214619,2022-03-25@23:00:00@200@G00@bd91be1f-2495-49d5-8220-906f55242bd6

			# f
			# to fix later
	-----

	stats:
		Rerun: 5
		webshots: 8 

03 26 22
	(R D-1) Setup 1
		RANK-356	2022-03-22	2022-03-24	Zound	Auto copy over for 1 day	otto.de	
			google lautsprecher	
			2022-04-01	
			https://detailsupport.atlassian.net/browse/PRODUCTS-3528 	
			https://prnt.sc/cdOGi6pNgC8n	

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@xhsDgt_tEee8oQANOijygQ
	-----
	(R D-1) 4479	2022-03-26	New CMS	P00	all	krefel.be/fr	
		'P00BESU01L000001420450202105180758',
		'P00BESU01L000001420530202105180758',
		'P00BESU01L000001420480202105180758',
		'P00BESU01L000001420470202105180758'
		Spider Issue -1 values		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.krefel.be/fr'	AND item_id in ('P00BESU01L000001420450202105180758',
		'P00BESU01L000001420530202105180758',
		'P00BESU01L000001420480202105180758',
		'P00BESU01L000001420470202105180758')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@P00BESU01L000001420450202105180758,2022-03-26@22:00:00@P00BESU01L000001420530202105180758,2022-03-26@22:00:00@P00BESU01L000001420480202105180758,2022-03-26@22:00:00@P00BESU01L000001420470202105180758

			# rerun successful
			# done react, done sheet

	(R D-1) 4480	2022-03-26	New CMS	P00	all	krefel.be/nl	
		'P00BEIS01L000001396070202105030459',
		'P00BEIS01L000001396020202105030459'
		Spider Issue -1 values		
		Yes	Keeshia

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.krefel.be/nl'	AND item_id in ('P00BEIS01L000001396070202105030459',
		'P00BEIS01L000001396020202105030459')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@P00BEIS01L000001396070202105030459,2022-03-26@22:00:00@P00BEIS01L000001396020202105030459

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@P00BEIS01L000001396020202105030459

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@P00BEIS01L000001396020202105030459,2022-03-26@22:00:00@P00BEIS01L000001396070202105030459

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 4489	2022-03-26	New CMS	200	in_stock	prisjakt.se	
		'200SED0070000001398430202105040415',
		'200SED0070000000552250201904050753',
		'200SED0070000000550300201904050558',
			# https://www.prisjakt.nu/produkt.php?p=4924847
		'200SED0070000001398310202105040415'
		not found in avalilability		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.prisjakt.se'	AND item_id in ('200SED0070000001398430202105040415',
				'200SED0070000000552250201904050753',
				'200SED0070000000550300201904050558',
				'200SED0070000001398310202105040415')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@200SED0070000001398430202105040415,2022-03-26@22:00:00@200SED0070000000552250201904050753,2022-03-26@22:00:00@200SED0070000000550300201904050558,2022-03-26@22:00:00@200SED0070000001398310202105040415

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@200SED0070000000550300201904050558,2022-03-26@22:00:00@200SED0070000001398310202105040415

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@200SED0070000000550300201904050558

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 4490	2022-03-26	New CMS	200	in_stock	hintaopas.fi	
		200FIF0070000000551080201904050633	
		not found in avalilability		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.hintaopas.fi'	AND item_id in ('200FIF0070000000551080201904050633')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@21:00:00@200FIF0070000000551080201904050633
			# rerun successful
			# done react, done sheet

	(R D-1) 4491	2022-03-26	New CMS	200	in_stock	elgiganten.dk	
		200DK100N0000000039810201901180331	
		not found in avalilability		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.elgiganten.dk'	AND item_id in ('200DK100N0000000039810201901180331')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@200DK100N0000000039810201901180331
			# rerun successful
			# done react, done sheet
			
	(R D-1) 4492	2022-03-26	New CMS	200	all	heureka.cz	
		'200CZ9D080000000804610201907080642',
		'200CZ9D070000000804270201907080350',
		'200CZ9D070000000551040201904050624',
		'200CZ9D080000000804570201907080636'
		Invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-26' AND website = 'www.heureka.cz'	AND item_id in (	'200CZ9D080000000804610201907080642',
			'200CZ9D070000000804270201907080350',
			'200CZ9D070000000551040201904050624',
			'200CZ9D080000000804570201907080636')		

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@200CZ9D080000000804610201907080642,2022-03-26@22:00:00@200CZ9D070000000804270201907080350,2022-03-26@22:00:00@200CZ9D070000000551040201904050624,2022-03-26@22:00:00@200CZ9D080000000804570201907080636

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-26@22:00:00@200CZ9D070000000551040201904050624
			# multiple reruns successful
			# done react, done sheet

	(w) 4503	2022-03-26	New CMS	Q00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-26' and webshot_url is NULL	
		No Webshots		
		Yes	markeeeeeee

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-26' and webshot_url is NULL and source = 'etl' order by item_id 

			# 83
			# 0
			# done react, done sheet

	(w) 4504	2022-03-26	New CMS	A10	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-26' and webshot_url is NULL	
		No Webshots		
		Yes	Have

		# solution
			# SELECT item_id, source FROM view_all_productdata where date='2022-03-26' and webshot_url is NULL and source = 'etl' order by item_id

			# 80
			# 7
			# 2
			# 0
			# done react, done sheet

	(w) NA A00 WEBSHOTS
		# 644
		# 606
		# 536
		# 447
		# 325
		# 231
		# 143
		# 18
 
	-----	
	
	(R D-1) 2868	2022-03-26	New CMS	110	all	www.fnac.be/fr	
		Nintendo Switch rouge/bleu	
		Data Count Mismatch 1(Fetched Data is Greater than expected)	
		https://prnt.sc/3_SfzHS9aPVu	
		Yes	Flor

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.fnac.be/fr' 
				AND keyword in (N'Nintendo Switch rouge/bleu')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-26@22:00:00@110@2cd65bf0-faea-4d88-bd15-1ec07d394012@Hhe47uOhEeeyugANOiZi-g
			# multiple reruns successful
			# done react, done sheet

	(R D-1)2869	2022-03-26	New CMS	U00	all	www.amazon.com.br	
		controle nintendo switch	
		Auto Copy Over for 1 Day	
		https://prnt.sc/NW1A0xvZfDAt	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.amazon.com.br' 
				AND keyword in (N'controle nintendo switch')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@02:00:00@U00@1398bc6a-9453-421d-96c0-0204b23f38e7@maMXRrZqEeidIgANOiZi-g

			# rerun successful
			# done react, done sheet

	(R D-1) 2870	2022-03-26	New CMS	U00	all	www.extra.com.br	
		mini video game	
		Auto Copy Over for 1 Day	
		https://prnt.sc/tWL8yvwKhoNi	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.extra.com.br' 
				AND keyword in (N'mini video game')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@VqLIREasEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-1) 2871	2022-03-26	New CMS	U00	all	www.nagem.com.br	
		console
		Auto Copy Over for 1 Day	
		https://prnt.sc/EdRP0AQOZ6H7	
		Yes	kurt

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.nagem.com.br' 
				AND keyword in (N'console')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@02:00:00@U00@995db543-ac2f-4fe6-884a-e29cb9034332@1a589c20-8c9d-4451-a65a-593c3640dba0
			# rerun successful
			# done react, done sheet

	(R D-1) 2875	2022-03-26	New CMS	X00	all	www.sanborns.com.mx
		'Consola Nintendo Lite'
		Auto Copy Over for 1 Day	
		https://prnt.sc/-uHVWVg83Rh-	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.sanborns.com.mx' 
				AND keyword in (N'Consola Nintendo Lite',
						'Nintendo Lite')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@05:00:00@X00@49d3844d-b750-462b-8951-06dfb60fcb21@9e1f1f13-cce5-4e30-811e-a7a6a90e612e
			# rerun successful
			# done react, done sheet

	(R D-1) 2876	2022-03-26	New CMS	X00	all	www.walmart.com.mx	
		Nintendo Pro Controller	
		Auto Copy Over for 1 Day	
		https://prnt.sc/NpAJbBHB4JCf	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.walmart.com.mx' 
				AND keyword in (N'Nintendo Pro Controller')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@05:00:00@X00@795f8f41-ea92-42dc-83ae-34f175697537@4msOTkatEeeMawANOiZi-g
			# successful rerun
			# done react, done sheet

	(R D-1) 2877	2022-03-26	New CMS	X00	all	www.amazon.com.mx	
		'Consola',
		'Nintendo Switch Lite'
		Auto Copy Over for 1 Day	
		https://prnt.sc/rxcKe--ne4yf
		https://prnt.sc/gGJgQWw7dsAA	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-26' 
				AND website = 'www.amazon.com.mx' 
				AND keyword in (N'Consola',
						'Nintendo Switch Lite')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@05:00:00@X00@79cd174a-b94b-405e-97a5-e36568e39106@w-qFgLZpEeidIgANOiZi-g,2022-03-26@05:00:00@X00@36ca1602-96f4-4497-bfff-a93bd2deb067@w-qFgLZpEeidIgANOiZi-g

			# rerun successful
			# done react, done sheet

	-----

			
	(R D-1) 984	2022-03-26	New CMS	200	all	www.re-store.ru	
		headphones bluetooth on ear	- 25

		Data Count Mismatch 9 (Fetched Data is Lesser than expected)	
		https://prnt.sc/AfMG6qTHNqMy	
		Yes	kurt

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-26' 
				AND website = 'www.re-store.ru' 
				AND category in (N'headphones bluetooth on ear')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-26@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 986	2022-03-26	New CMS	M00	all	www.maxizoo.dk	
		Wet food cat	
		Auto copyover for 1 day 	
		https://prnt.sc/xxY2L7KGyMT1	
		Yes	Michael

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-26' 
				AND website = 'www.maxizoo.dk' 
				AND category in (N'Wet food cat')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-26@22:00:00@M00@3T0@39bcdc20-7201-49bb-b97d-6c79eddec526
			# rerun successful
			# done react, done sheet
	-----
	STATS:
		Rerun: 16
		Webshots: 3 

03 27 22
	(R D-1) Setup 1
		RANK-356	2022-03-22	2022-03-24	Zound	Auto copy over for 1 day	otto.de	
			google lautsprecher	
			2022-04-01	
			https://detailsupport.atlassian.net/browse/PRODUCTS-3528 	
			https://prnt.sc/cdOGi6pNgC8n	

			# solution
				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-26@22:00:00@200@dcb121a0-6342-4922-905c-4a470f7c3696@xhsDgt_tEee8oQANOijygQ

	-----
	(w) 4511	2022-03-27	New CMS	K00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-03-27' and webshot_url is NULL	
		no webshots		
		Yes	Turki

		# solution
			# 1450
			# 1384
			# 1222
			# 1108
			# 1026
			# 779
			# 677
			# 628
			# 617
			# 573
			# 467
			# 360
			# 297
			# 277

	(R D-1) 4519	2022-03-27	New CMS	A10	all	allegro.pl	
		'A10PLIX000120210916032741319110259',
		'A10PLIX000120210916032738946168259',
		'A10PLIX000120210916032743233073259',
		'A10PLIX000120210916032739818251259',
		'A10PLIX000120210916032738371344259',
		'A10PLIX000120210916032738721261259',
		'A10PLIX000120210916032742391499259',
		'A10PLIX020120210916032744660433259',
		'A10PLIX000120210916032743432595259',
		'A10PLIX000120210916032741924208259',
		'A10PLIX000120210916032741503988259',
		'A10PLIX040120210916032816224897259',
		'A10PLIX000120210916032738560006259',
		'A10PLIX000120210916032743605902259',
		'A10PLIX000120210916032741711135259',
		'A10PLIX060120210916032811289590259',
		'A10PLIX000120210916032740551734259',
		'A10PLIX000120210916032742590789259',
		'A10PLIX000120210916032740772814259',
		'A10PLIX000120210916032740002751259'

		Invalid deadlink		
		Yes	Neil

		# solution
			# SELECT * FROM view_all_productdata where date='2022-01-23' AND website = 'www.allegro.pl'	AND item_id in ('A10PLIX000120210916032741319110259',
				'A10PLIX000120210916032738946168259',
				'A10PLIX000120210916032743233073259',
				'A10PLIX000120210916032739818251259',
				'A10PLIX000120210916032738371344259',
				'A10PLIX000120210916032738721261259',
				'A10PLIX000120210916032742391499259',
				'A10PLIX020120210916032744660433259',
				'A10PLIX000120210916032743432595259',
				'A10PLIX000120210916032741924208259',
				'A10PLIX000120210916032741503988259',
				'A10PLIX040120210916032816224897259',
				'A10PLIX000120210916032738560006259',
				'A10PLIX000120210916032743605902259',
				'A10PLIX000120210916032741711135259',
				'A10PLIX060120210916032811289590259',
				'A10PLIX000120210916032740551734259',
				'A10PLIX000120210916032742590789259',
				'A10PLIX000120210916032740772814259',
				'A10PLIX000120210916032740002751259')

			# rerun successful
			# done react, done sheet

	0 (R D-1) 4524	2022-03-27	New CMS	200	all	iport.ru	
		all items	
		spider issues -1 and -3 values		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-27' AND website = 'www.iport.ru'

			#  python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200RULU070000001389150202104260350,2022-03-27@21:00:00@200RULU0NC000001594310202109210326,2022-03-27@21:00:00@200RULU070000001389190202104260350,2022-03-27@21:00:00@200RULU070000001568610202108300838,2022-03-27@21:00:00@200RULU0NC000001594070202109210326,2022-03-27@21:00:00@200RULU070000001389160202104260350,2022-03-27@21:00:00@200RULU070000001389180202104260350,2022-03-27@21:00:00@200RULU070000001389200202104260350,2022-03-27@21:00:00@200RULU070000001389210202104260350,2022-03-27@21:00:00@200RULU070000001389170202104260350,2022-03-27@21:00:00@200RULU070000001389120202104260350,2022-03-27@21:00:00@200RULU070000001389130202104260350,2022-03-27@21:00:00@200RULU070000001389230202104260350,2022-03-27@21:00:00@200RULU070000001389220202104260350,2022-03-27@21:00:00@200RULU070000001568550202108300838,2022-03-27@21:00:00@200RULU070000001560090202108230658,2022-03-27@21:00:00@200RULU070000001568570202108300838,2022-03-27@21:00:00@200RULU070000001560410202108230658

			# etl
				# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200RULU070000001389150202104260350,2022-03-27@21:00:00@200RULU070000001389190202104260350,2022-03-27@21:00:00@200RULU070000001568610202108300838,2022-03-27@21:00:00@200RULU070000001389160202104260350,2022-03-27@21:00:00@200RULU070000001389180202104260350,2022-03-27@21:00:00@200RULU070000001389200202104260350,2022-03-27@21:00:00@200RULU070000001389210202104260350,2022-03-27@21:00:00@200RULU070000001389170202104260350,2022-03-27@21:00:00@200RULU070000001389120202104260350,2022-03-27@21:00:00@200RULU070000001389130202104260350,2022-03-27@21:00:00@200RULU070000001389220202104260350,2022-03-27@21:00:00@200RULU070000001568570202108300838

			# deadlink or ACO
				# 

	0 (R D-1)4525	2022-03-27	New CMS	200	all	fnac.com	
		200FR210B0000000105640201902071110	
		invalid deadlink		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-27' AND website = 'www.fnac.com' AND item_id in ('200FR210B0000000105640201902071110')	

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-27@22:00:00@200FR210B0000000105640201902071110
			# check site: oos
			# check local: has data
			# check et local: has data, oos

			# for Trello

	(R D-1) 4534	2022-03-27	New CMS	H10	all	lowes.com	
		select * from view_all_productdata where date='2022-03-27' and website='www.lowes.com' and source='auto_copy_over' order by item_id	
		ACO not dead. 
		Request INTERVAL rerun.		
		Yes	Keeshia

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-27@04:00:00@H10US8X0Q8220220202031745659583033,2022-03-27@04:00:00@H10US8X0Q8220220202031746746114033,2022-03-27@04:00:00@H10US8X0Q8220220202031747472617033,2022-03-27@04:00:00@H10US8X0Q8220220202031747800441033,2022-03-27@04:00:00@H10US8X0Q8220220202031748132374033,2022-03-27@04:00:00@H10US8X0Q8220220202031748563583033,2022-03-27@04:00:00@H10US8X0Q8220220202031748723124033,2022-03-27@04:00:00@H10US8X0Q8220220202031749305699033,2022-03-27@04:00:00@H10US8X0Q8220220202031749994610033,2022-03-27@04:00:00@H10US8X0Q8220220202031751432976033,2022-03-27@04:00:00@H10US8X0Q8220220202031752265662033,2022-03-27@04:00:00@H10US8X0Q8220220202031753565262033,2022-03-27@04:00:00@H10US8X0Q8220220202031754254804033,2022-03-27@04:00:00@H10US8X0Q8220220202031754436340033,2022-03-27@04:00:00@H10US8X0Q8220220202031754896255033,2022-03-27@04:00:00@H10US8X0Q8220220202031755441512033,2022-03-27@04:00:00@H10US8X0Q8220220202031755972655033,2022-03-27@04:00:00@H10US8X0Q8220220202031756147104033,2022-03-27@04:00:00@H10US8X0Q8220220202031756469891033,2022-03-27@04:00:00@H10US8X0Q8220220202031756802709033,2022-03-27@04:00:00@H10US8X0Q8220220202031757286551033,2022-03-27@04:00:00@H10US8X0Q8220220202031758173393033,2022-03-27@04:00:00@H10US8X0Q8220220202031758634730033,2022-03-27@04:00:00@H10US8X0Q8220220202031759135954033,2022-03-27@04:00:00@H10US8X0Q8220220202031759792533033,2022-03-27@04:00:00@H10US8X0Q8220220202031800353083033,2022-03-27@04:00:00@H10US8X0Q8220220202031800526690033,2022-03-27@04:00:00@H10US8X0Q8220220202031801393708033,2022-03-27@04:00:00@H10US8X0Q8220220202031803043624033,2022-03-27@04:00:00@H10US8X0Q8220220202031803547167033,2022-03-27@04:00:00@H10US8X0Q8220220202031803963256033,2022-03-27@04:00:00@H10US8X0Q8220220202031805361604033,2022-03-27@04:00:00@H10US8X0Q8220220202031805529771033,2022-03-27@04:00:00@H10US8X0Q8220220202031805704593033,2022-03-27@04:00:00@H10US8X0Q8220220202031806394955033,2022-03-27@04:00:00@H10US8X0Q8220220202031807037484033,2022-03-27@04:00:00@H10US8X0Q8220220202031808721848033,2022-03-27@04:00:00@H10US8X0Q8220220202031808870898033,2022-03-27@04:00:00@H10US8X0Q8220220202031809034459033,2022-03-27@04:00:00@H10US8X0Q8220220202031810929630033,2022-03-27@04:00:00@H10US8X0Q8220220202031811109414033,2022-03-27@04:00:00@H10US8X0Q8220220202031811736309033,2022-03-27@04:00:00@H10US8X0Q8220220202031812229847033,2022-03-27@04:00:00@H10US8X0Q8220220202031812993646033,2022-03-27@04:00:00@H10US8X0Q8220220202031813261501033,2022-03-27@04:00:00@H10US8X0Q8220220202031813420837033,2022-03-27@04:00:00@H10US8X0Q8220220202031813576068033,2022-03-27@04:00:00@H10US8X0Q8220220202031813854192033,2022-03-27@04:00:00@H10US8X0Q8220220202031814007187033,2022-03-27@04:00:00@H10US8X0Q8220220202031814195213033,2022-03-27@04:00:00@H10US8X0Q8220220202031814540852033,2022-03-27@04:00:00@H10US8X0Q8220220202031814709480033,2022-03-27@04:00:00@H10US8X0Q8220220202031814872262033,2022-03-27@04:00:00@H10US8X0Q8220220202031815036068033,2022-03-27@04:00:00@H10US8X0Q8220220202031815206314033,2022-03-27@04:00:00@H10US8X0Q8220220202031815330592033,2022-03-27@04:00:00@H10US8X0Q8220220202031815490190033

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-27@04:00:00@H10US8X0Q8220220202031747472617033,2022-03-27@04:00:00@H10US8X0Q8220220202031748132374033,2022-03-27@04:00:00@H10US8X0Q8220220202031748563583033,2022-03-27@04:00:00@H10US8X0Q8220220202031749305699033,2022-03-27@04:00:00@H10US8X0Q8220220202031749994610033,2022-03-27@04:00:00@H10US8X0Q8220220202031753565262033,2022-03-27@04:00:00@H10US8X0Q8220220202031754436340033,2022-03-27@04:00:00@H10US8X0Q8220220202031754896255033,2022-03-27@04:00:00@H10US8X0Q8220220202031755972655033,2022-03-27@04:00:00@H10US8X0Q8220220202031756147104033,2022-03-27@04:00:00@H10US8X0Q8220220202031756469891033,2022-03-27@04:00:00@H10US8X0Q8220220202031756802709033,2022-03-27@04:00:00@H10US8X0Q8220220202031757286551033,2022-03-27@04:00:00@H10US8X0Q8220220202031759135954033,2022-03-27@04:00:00@H10US8X0Q8220220202031800353083033,2022-03-27@04:00:00@H10US8X0Q8220220202031800526690033,2022-03-27@04:00:00@H10US8X0Q8220220202031801393708033,2022-03-27@04:00:00@H10US8X0Q8220220202031805361604033,2022-03-27@04:00:00@H10US8X0Q8220220202031805704593033,2022-03-27@04:00:00@H10US8X0Q8220220202031808721848033,2022-03-27@04:00:00@H10US8X0Q8220220202031808870898033,2022-03-27@04:00:00@H10US8X0Q8220220202031810929630033,2022-03-27@04:00:00@H10US8X0Q8220220202031811109414033,2022-03-27@04:00:00@H10US8X0Q8220220202031813420837033,2022-03-27@04:00:00@H10US8X0Q8220220202031814007187033,2022-03-27@04:00:00@H10US8X0Q8220220202031814540852033,2022-03-27@04:00:00@H10US8X0Q8220220202031814709480033,2022-03-27@04:00:00@H10US8X0Q8220220202031815036068033,2022-03-27@04:00:00@H10US8X0Q8220220202031815206314033,2022-03-27@04:00:00@H10US8X0Q8220220202031815490190033

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-27@04:00:00@H10US8X0Q8220220202031749994610033,2022-03-27@04:00:00@H10US8X0Q8220220202031755972655033,2022-03-27@04:00:00@H10US8X0Q8220220202031801393708033,2022-03-27@04:00:00@H10US8X0Q8220220202031814007187033,2022-03-27@04:00:00@H10US8X0Q8220220202031814540852033,2022-03-27@04:00:00@H10US8X0Q8220220202031815036068033,2022-03-27@04:00:00@H10US8X0Q8220220202031815490190033

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-27@04:00:00@H10US8X0Q8220220202031749994610033,2022-03-27@04:00:00@H10US8X0Q8220220202031814540852033

			# multiple reruns successful

			# done react, done sheet

	(w) 4535	2022-03-27	New CMS	H10	webshot_url		
			select * from view_all_productdata where date='2022-03-27' and website in ('www.amazon.com', 'www.homedepot.com', 'www.lowes.com', 'www.fastenal.com', 'www.grainger.com') and webshot_url is NULL order by item_id	
			No WEbshots		
			Yes	Keeshia

			# 8
			# 0
			# done react, done sheet

	(R D-1) 4536	2022-03-27	New CMS	K00	all		
		'K00RUP80AD000001449540202106150452',
		'K00RUP807O000001449500202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP803F000001449440202106150452',
		'K00RUP803F000001449400202106150452',
		'K00RUP803F000001449180202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000001245120202011170413',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001311070202101250600',
		'K00RUP80CG000001310920202101250600',
		'K00RUP80CG000001310880202101250600',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000001592510202109200556',
		'K00RUP80CG000001373070202104050819',
		'K00RUP80CG000001310850202101250600',
		'K00RUP80CG000001310960202101250600',
		'K00RUP807U020220214043148528186045',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000001310950202101250600',
		'K00RUP80IO000001449640202106150452',
		'K00RUP80YO000001449670202106150452',
		'K00RUP805K000001527540202108020954',
		'K00RUP805K000001527500202108020954',
		'K00RUP80YO000001449690202106150452',
		'K00RUP805K000001527570202108020954',
		'K00RUP80CG000001373010202104050819'
		wrong price and availability		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-27' AND item_id in ('K00RUP80AD000001449540202106150452',
				'K00RUP807O000001449500202106150452',
				'K00RUP80AD000001449550202106150452',
				'K00RUP80AD000001527640202108020954',
				'K00RUP803F000001449440202106150452',
				'K00RUP803F000001449400202106150452',
				'K00RUP803F000001449180202106150452',
				'K00RUP80DO000001449630202106150452',
				'K00RUP80CG000001245120202011170413',
				'K00RUP80CG000001311060202101250600',
				'K00RUP80CG000001311070202101250600',
				'K00RUP80CG000001310920202101250600',
				'K00RUP80CG000001310880202101250600',
				'K00RUP80CG000001310970202101250600',
				'K00RUP80CG000001592510202109200556',	
				'K00RUP80CG000001373070202104050819',
				'K00RUP80CG000001310850202101250600',
				'K00RUP80CG000001310960202101250600',
				'K00RUP807U020220214043148528186045',
				'K00RUP80CG000000980450202006250902',
				'K00RUP80CG000001310950202101250600',
				'K00RUP80IO000001449640202106150452',
				'K00RUP80YO000001449670202106150452',
				'K00RUP805K000001527540202108020954',
				'K00RUP805K000001527500202108020954',
				'K00RUP80YO000001449690202106150452',
				'K00RUP805K000001527570202108020954',
				'K00RUP80CG000001373010202104050819')	
			# rerun successful
			# done react, done sheet

	(R D-1) 4537	2022-03-27	New CMS	K00	rating_score		
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP803F000001528840202108020955',
		'K00RUP803F000001449160202106150452',
		'K00RUP804K000001449120202106150452',
		'K00RUP803F000001449180202106150452',
		'K00RUP803F000001527440202108020954',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001527310202108020954',
		'K00RUP803F000001449330202106150452',
		'K00RUP80CG000000980360202006250902',
		'K00RUP80CG000001311070202101250600',
		'K00RUP80CG000001592510202109200556',
		'K00RUP807U020220214043148827160045',
		'K00RUP807U020220214043148827160045',
		'K00RUP80CG000001524980202108020807'
		wrong ratings		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-27' AND item_id in ('K00RUP804K000001449090202106150452',
				'K00RUP804K000001528820202108020955',
				'K00RUP803F000001528840202108020955',
				'K00RUP803F000001449160202106150452',
				'K00RUP804K000001449120202106150452',
				'K00RUP803F000001449180202106150452',
				'K00RUP803F000001527440202108020954',
				'K00RUP803F000001449420202106150452',
				'K00RUP803F000001449420202106150452',
				'K00RUP803F000001527310202108020954',
				'K00RUP803F000001449330202106150452',
				'K00RUP80CG000000980360202006250902',
				'K00RUP80CG000001311070202101250600',
				'K00RUP80CG000001592510202109200556',
				'K00RUP807U020220214043148827160045',
				'K00RUP807U020220214043148827160045',
				'K00RUP80CG000001524980202108020807')
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@K00RUP804K000001449090202106150452,2022-03-27@21:00:00@K00RUP804K000001528820202108020955,2022-03-27@21:00:00@K00RUP803F000001528840202108020955,2022-03-27@21:00:00@K00RUP803F000001449160202106150452,2022-03-27@21:00:00@K00RUP804K000001449120202106150452,2022-03-27@21:00:00@K00RUP803F000001449180202106150452,2022-03-27@21:00:00@K00RUP803F000001527440202108020954,2022-03-27@21:00:00@K00RUP803F000001449420202106150452,2022-03-27@21:00:00@K00RUP803F000001449420202106150452,2022-03-27@21:00:00@K00RUP803F000001527310202108020954,2022-03-27@21:00:00@K00RUP803F000001449330202106150452,2022-03-27@21:00:00@K00RUP80CG000000980360202006250902,2022-03-27@21:00:00@K00RUP80CG000001311070202101250600,2022-03-27@21:00:00@K00RUP80CG000001592510202109200556,2022-03-27@21:00:00@K00RUP807U020220214043148827160045,2022-03-27@21:00:00@K00RUP807U020220214043148827160045,2022-03-27@21:00:00@K00RUP80CG000001524980202108020807

			# successful rerun
			# done react, done sheet

	(R D-1) 4538	2022-03-27	New CMS	K00	all		
		K00RUP80CG000001311080202101250600	spider issue -1 values		
		Yes	Turki

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-27' AND item_id in ('K00RUP80CG000001311080202101250600')

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@K00RUP80CG000001311080202101250600

			# done react, done sheet

	-----
	(R D-1) 2878	2022-03-27	New CMS	200	all	www.dns-shop.ru
		наушники внутриканальные беспроводные bluetooth	
		invalid deadlink	
		https://prnt.sc/DWSdZrN8hiYE	
		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-27' 
				AND website = 'www.dns-shop.ru' 
				AND keyword in (N'наушники внутриканальные беспроводные bluetooth')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-27@21:00:00@200@0cf0a790-fb34-49b8-b00b-ed76653e3ebd@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-1) 2879	2022-03-27	New CMS	200	all	www.dns-shop.ru	
		колонка блютуз	
		Duplicate product_website & product_url
		https://prnt.sc/pPur3RYSdk9J	
		Yes	Phil

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'колонка блютуз') and 
				var2.[date] = '2022-03-27' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source 

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@047cab82-7af7-4f55-943c-8f044566b246@BzxplkXeEeeMawANOiZi-g
			# rerun successful
			# done react, done sheet

	(R D-2) 2880	2022-03-27	New CMS	200	all	www.re-store.ru	
		/ 'наушники вкладыши', - 2
			513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'наушники с проводом', - 16
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'портативная акустика wi-fi', - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ 'проводные наушники'- 14 
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 9 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	

		https://prnt.sc/hGm69Ym8XC9G
		https://prnt.sc/8aJoB9m1Oca0
		https://prnt.sc/w-0OwehrereY
		https://prnt.sc/InRXQSVxuaxW

		Yes	Phil

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-27' 
				AND website = 'www.re-store.ru' 
				AND keyword in (N'проводные наушники')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-27@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-27@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-27@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-27@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# 
	-----
	(R D-1) 991	2022-03-27	New CMS	100	all	www.milrab.fi	
		Älyrannekkeet	
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/Qf3vwM1exvcU	
		Yes	kurt

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.milrab.fi' and 
				var2.category in (N'Älyrannekkeet') and 
				var2.[date] = '2022-03-27' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source	

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-27@22:00:00@100@II0@93073494-3b48-4eb8-8628-996f2b3bde27
			# successful rerun
			# done react, done sheet

	(R D-1) 993	2022-03-27	New CMS	200	all	www.re-store.ru	
		'Headphones Bluetooth On Ear', - 25
		'Headphones Wired In Ear', - 6
		'Speakers Bluetooth', - 25 
		'Speakers Voice' - 3

		Data Count Mismatch (Fetched Data is Lesser than expected)	

		https://prnt.sc/0vLAfer8W-IS
		https://prnt.sc/gk7dD2vUpn07
		https://prnt.sc/9eaiqtTnlC2z
		https://prnt.sc/953QINN3_pkR	

		Yes	Michael

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-27' 
				AND website = 'www.re-store.ru' 
				AND category in (N'Headphones Bluetooth On Ear',
						'Headphones Wired In Ear',
						'Speakers Bluetooth',
						'Speakers Voice')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-27@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-03-27@21:00:00@200@LQ0@b2aeae9a-1b7e-4765-afd3-65fcb106dd58,2022-03-27@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-27@21:00:00@200@LQ0@de13da03-7df6-465b-a01a-7188cf738b90,2022-03-27@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# multiple reruns successful
			# done react, done sheet

	-----
	Stats
		Rerun: 13 (14)
		Webshots: 2

03 28 22
	Special Fix (Voluntary)
		995	2022-03-28	New CMS	200	all	www.iport.ru	
			'Headphones Bluetooth In Ear',
			'Headphones Bluetooth On Ear',
			'Headphones Noise Cancelling',
			'Headphones Wired In Ear',
			'Headphones Wired On Ear',
			'Speakers Bluetooth',
			'Speakers Portable'

			Auto copyover for 3 days 
			https://prnt.sc/hDGsivGHFx1k	
			Yes	Rhunick

			# solution
				# 

				#	

03 30 22
	(F D-3) Trello 1:
		[ZURN] Compliance - Missing Video Count in www.lowes.com
		Please update spyder to scrape videos found in pdp

		date: 2022-03-29
		retailer: www.lowes.com

		sample item_id:
		H10US8X0Q8220220202031744332466033
		H10US8X0Q8220220202031744689111033
		# https://trello.com/c/lUAfjBV0/3549-zurn-compliance-missing-video-count-in-wwwlowescom

		# solution
			# check local: fix
				# difficult to test
			# pushed
			# for rebuild

			# prepared but not executed
				# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-29@04:00:00@H10US8X0Q8220220202031744332466033,2022-03-29@04:00:00@H10US8X0Q8220220202031744689111033

	(F D-2) Trello 2:
		[RAZER] Compliance - Incomplete description in www.alternate.de
		Please fix the spyder to fetch the complete description

		Date: 2022-03-30
		Retailer: www.alternate.de
		Item id: all
		sample item id: K00DETF07U020220328081008089709087
			# https://www.alternate.de/Razer/Blade-14-(RZ09-0427NGA3-R3G1)-Gaming-Notebook/html/product/1819261
		# https://trello.com/c/AvxEhkln/3553-razer-compliance-incomplete-description-in-wwwalternatede

		# solution
			#  SELECT * FROM view_all_productdata where date='2022-03-30' AND website = 'www.alternate.de'	AND item_id in ('K00DETF07U020220328081008089709087')

			# fix: takes time to fix because of formatting issue: utf-8
			# pushed > updated trello ticket

	-----
	(R D-2) 4589	2022-03-30	New CMS	K00	all	fnac.com	
		'K00FR210CG000001355830202103290839',
		'K00FR2107O000001432570202106020354',
		'K00FR2105K000001540140202108110154'
		Invalid Dealink		
		Yes	Switzell

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-30' AND website = 'www.fnac.com'	AND item_id in ('K00FR210CG000001355830202103290839',
			'K00FR2107O000001432570202106020354',
			'K00FR2105K000001540140202108110154')

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-30@22:00:00@K00FR210CG000001355830202103290839,2022-03-30@22:00:00@K00FR2107O000001432570202106020354,2022-03-30@22:00:00@K00FR2105K000001540140202108110154

			# [7] python rerun_cmp.py -s CRAWL_FAILED -l 2022-03-30@22:00:00@K00FR2105K000001540140202108110154

			# for manual
			# done react, done sheet

	(R D-1) 4613	2022-03-30	New CMS	200	all	amazon.com	
		200USW0070000000038080201901180330	
		spider issue -1 values		
		Yes	Louie

		# solution
			# SELECT * FROM view_all_productdata where date='2022-03-30' AND website = 'www.amazon.com'	AND item_id in ('200USW0070000000038080201901180330')	

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-03-30@04:00:00@200USW0070000000038080201901180330

			# multiple reruns successful
			# done react, done sheet
	-----
	0 (R D-3) 2915	2022-03-30	New CMS	K00	all	fnac.com	
		meilleurs écouteurs	
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/kuTj1i2IMNAR	
		Yes	michael

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.fnac.com' 
				AND keyword in (N'meilleurs écouteurs')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-30@22:00:00@K00@47eb875b-f711-40ba-8425-f5f789f58cbe@LzP6XApHEem-gAANOiOHiA
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@22:00:00@K00@47eb875b-f711-40ba-8425-f5f789f58cbe@LzP6XApHEem-gAANOiOHiA
			# (9) python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-30@22:00:00@K00@47eb875b-f711-40ba-8425-f5f789f58cbe@LzP6XApHEem-gAANOiOHiA
			# Trello
			
	(R D-1) 2916	2022-03-30	New CMS	K00	all	otto.de	
		HEADSET	
		1 Day Auto Copy Over	
		https://prnt.sc/8Z5tiLqWPG6Q	
		Yes	keen

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.otto.de' 
				AND keyword in (N'HEADSET')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@22:00:00@K00@9ce256ab-4183-4ec9-b02e-35f70b3310c9@xhsDgt_tEee8oQANOijygQ

			# successful rerun
			# done react, done sheet

	(R D-1) 2917	2022-03-30	New CMS	K00	all	www.alza.cz	
		'Gamepady Playstation',
		'Herní sluchátka'

		Auto Copy Over for 1 day	
		https://prnt.sc/vN96Hslvv-8o
		https://prnt.sc/v1wSUWhDSgqw	
		Yes	kurt

		# solution
			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.alza.cz' 
				AND keyword in (N'Gamepady Playstation',
						'Herní sluchátka')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@22:00:00@K00@29ae9dd0-1091-4849-a1c6-15270b32f7e3@1cAhckXZEeeMawANOiZi-g,2022-03-30@22:00:00@K00@1ce05d3a-ae04-40d8-b08d-0a06910b17cc@1cAhckXZEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	(R D-1) 2918	2022-03-30	New CMS	K00	all	www.pcdiga.com	
		Gaming Headset	
		Invalid Deadlink	
		https://prnt.sc/HmTKv3Wk_HkG	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.pcdiga.com' 
				AND keyword in (N'Gaming Headset')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@23:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@9da6a206-89f0-4464-b067-6931614ba09b
			# rerun successful
			# done react, done sheet

	(R D-1) 2919	2022-03-30	New CMS	K00	all	www.pcdiga.com	
		Tapetes	
		Auto Copy Over for 3 days	
		https://prnt.sc/gf8GPwtDOwAB	
		Yes	Kristian

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.pcdiga.com' 
				AND keyword in (N'Gaming Headset')
				GROUP BY keyword, ritem_id, country, source	

			# (2) python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@23:00:00@K00@850a97a6-58af-4825-a547-ee14be05e68c@9da6a206-89f0-4464-b067-6931614ba09b

			# multiple reruns successful
			# done react, done sheet

	(R D-2) 2920	2022-03-30	New CMS	K00	all	www.pcdiga.com	
		/ 'Gaming Controller',
		/ 'Teclado mecanico'

		Duplicate product_website & product_url	

		https://prnt.sc/Ew6gtP9f-ZdJ
		https://prnt.sc/1GG26yRrdmdv	
		Yes	Kristian

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.pcdiga.com' and 
				var2.keyword in (N'Gaming Controller',
				'Teclado mecanico') and 
				var2.[date] = '2022-03-30' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# [2] python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-30@23:00:00@K00@4b5135ed-31a3-436d-8b08-d6bf94572c14@9da6a206-89f0-4464-b067-6931614ba09b

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@23:00:00@K00@4b5135ed-31a3-436d-8b08-d6bf94572c14@9da6a206-89f0-4464-b067-6931614ba09b

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-30@23:00:00@K00@edca00b4-c316-492b-9d0f-f06295f3a545@9da6a206-89f0-4464-b067-6931614ba09b

			# multiple reruns successful
			# done react, done sheet

	(R D-1) 2924	2022-03-30	New CMS	K00	all	ebuyer.com	
		KEYBOARD	
		Auto copy over for 1 day	
		https://prnt.sc/33ZLm1vwJXUM	
		Yes	renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.ebuyer.com' 
				AND keyword in (N'KEYBOARD')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@23:00:00@K00@8874e516-54f6-4d11-9a25-bf0e1efc23b9@qcWnsMefEei-eQANOiOHiA
			# rerun successful
			# done react, done sheet

	(R D-1) 2925	2022-03-30	New CMS	K00	all	ebuyer.com	
		Gaming Keypad	
		Invalid Deadlink	
		https://prnt.sc/A5vBu8R2DzVO	
		Yes	renz

		# solution
			# valid deadlink as investigated by validator
			# done react, done sheet

	(R D-1) 2926	2022-03-30	New CMS	K00	all	ebuyer.com	
		PC Keypad
		Mechanical Keypad
		2.1 Speakers
		Ergonomic Chair
		Playstation Controller	

		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 16 (Fetched Data is Lesser than expected)
		Data Count Mismatch 18(Fetched Data is Lesser than expected)
		Data Count Mismatch 17 (Fetched Data is Lesser than expected)	

		https://prnt.sc/zF5dg8hsNXqS
		https://prnt.sc/jxQ7ipZndG2P
		https://prnt.sc/F3UNfPskO83p
		https://prnt.sc/Ry6E40lEgDgw
		https://prnt.sc/nRjR2PsXUYBw	

		Yes	renz

		# solution
			# valid deadlink as investigated by validator
			# done react, done sheet

	(R D-2) 2927	2022-03-30	New CMS	K00	all	www.box.co.uk	
		'Mechanical Keyboard',
		'Microphone',
		'Mouse',
		'Mousemat',
		'Notebook',
		'Office Chair',
		'PC Headset',
		'PC Keyboard',
		'PC Keypad',
		'PC Mouse',
		'Playstation Controller',
		'Ps4 Controller',
		'Speakers',
		'Wireless Headset',
		'Wireless Mouse',
		'Xbox Controller'

		'Gaming Mouse',
		'Gaming Notebook',
		'Laptop',
		'Keyboard',
		'Gaming Headset',
		'Gaming Keyboard',
		'Gaming Laptop',
		'Keypad',
		'Gaming Keypad',
		'Gaming Speakers',
		'Ergonomic Chair',
		'Gaming Chair',
		'chair'

		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day

		https://prnt.sc/bC1s1G5CFywW
		https://prnt.sc/tWa-JLbV5kdH
		https://prnt.sc/S93oVB7BuLqy
		https://prnt.sc/lSB3tVqb1fl0
		https://prnt.sc/SITH1kJkuMKQ
		https://prnt.sc/WJe-eJN4Eyoc
		https://prnt.sc/_WZ-QsutUf6a
		https://prnt.sc/4jCFDlqr9bhI
		https://prnt.sc/Kjwy7JHDFX54
		https://prnt.sc/xISSDP17Tmzn
		https://prnt.sc/Zs5_h76vcuQq
		https://prnt.sc/3D_jvq1T3myZ
		https://prnt.sc/UNw39-HNs9wp
		https://prnt.sc/1GjBHwHytx-D
		https://prnt.sc/K37It1ok0q_y
		https://prnt.sc/5tGDeuMa1GkL

		Yes	richelle

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.box.co.uk' 
				AND keyword in (N'Mechanical Keyboard',
						'Microphone',
						'Mouse',
						'Mousemat',
						'Notebook',
						'Office Chair',
						'PC Headset',
						'PC Keyboard',
						'PC Keypad',
						'PC Mouse',
						'Playstation Controller',
						'Ps4 Controller',
						'Speakers',
						'Wireless Headset',
						'Wireless Mouse',
						'Xbox Controller')
				GROUP BY keyword, ritem_id, country, source	

			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.box.co.uk' 
				AND keyword in (N'Gaming Mouse',
								'Gaming Notebook',
								'Laptop',
								'Keyboard',
								'Gaming Headset',
								'Gaming Keyboard',
								'Gaming Laptop',
								'Keypad',
								'Gaming Keypad',
								'Gaming Speakers',
								'Ergonomic Chair',
								'Gaming Chair',
								'chair')
				GROUP BY keyword, ritem_id, country, source	


			# rerun successful
			# done react, done sheet

	(R D-1) 2929	2022-03-30	New CMS	K00	all	dns-shop.ru
		МЫШЬ - 25
			29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g
		Игровые колонки - 25
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
		наушники - 25
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)
	 	Data Count Mismatch 7 (Fetched Data is Lesser than expected) 
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)

		https://prnt.sc/15b9Z2hxe3t9
		https://prnt.sc/ZbjxDA7w4RQu
		https://prnt.sc/B7hC6Qm8Py_u
		Yes	Yev

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
			# 	AND website = 'www.dns-shop.ru' 
			# 	AND keyword in (N'наушники')
			# 	GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-30@20:00:00@K00@29e047b5-0453-430b-839c-7ca93ff8d356@BzxplkXeEeeMawANOiZi-g,2022-03-30@20:00:00@K00@9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g,2022-03-30@20:00:00@K00@3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	(R D-1) 2930	2022-03-30	New CMS	K00	all	dns-shop.ru
		Наушники ANC
		Invalid Deadlink	
		https://prnt.sc/IX9si844wNu2	
		Yes	Yev

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-30' 
				AND website = 'www.dns-shop.ru' 
				AND keyword in (N'Наушники ANC')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-30@20:00:00@K00@f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g
			# successful rerun
			# done react, done sheet

	-----
	(R D-1) 1002	2022-03-30	New CMS	K00	all	www.citilink.ru	
		Клавиатуры	
		1 Day Auto Copy Over	
		https://prnt.sc/RvLRuwPPUwDh	
		Yes	Jairus

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-30' 
				AND website = 'www.citilink.ru' 
				AND category in (N'Клавиатуры')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-30@20:00:00@K00@K80@a960c67e-a303-4b8c-adb0-e62a34e02fef
			# successful rerun
			# done react, done sheet

	-----
	Stats: 
		Rerun: 15 (20) = 1.33
		Fix: 2[3]

03 31 22
	0 (F D-1) Trello 1:
		[MS PRICE] Comparisons: Incorrect price in gamestop.com
		Date: March 28, 2022
		Company: MS Price (A00)
		Retailer: gamestop.com
		Issue: Incorrect price, pre-owned products should, price= -1.

		Sample Item ID:
		'A00USM50RB000001247550202011230449',
		'A00USM50RB000001247560202011230449',
		'A00USM50GC000000153140201903040503',
		'A00USM50RB000001247430202011230449'
			# https://www.gamestop.com/products/microsoft-xbox-series-x/224744.html?bt=true&u1=s1636539220702r3vpa53055

		# https://trello.com/c/jHJ4j3m1/3541-ms-price-comparisons-incorrect-price-in-gamestopcom

		# solution
			# 

			# 

	(R D-1) 2939 2022-03-31 New CMS O00 all www.artencraft.be
		was-droogcombinatie
		Auto copy over for 2 days
		https://prnt.sc/tfgBaBqIbefy
		Yes renz

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-31' 
				AND website = 'www.artencraft.be' 
				AND keyword in (N'was-droogcombinatie')
				GROUP BY keyword, ritem_id, country, source	

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-03-31@22:00:00@O00@b78aa926-43ea-4c10-bdc0-943201617b2c@db168a76-36e1-49dd-bd85-0c79a3e5e15b
			# valid deadlink 
			# done react, done sheet

	(R D-1) 2937	2022-03-31	New CMS	200	all	www.dns-shop.ru	
		портативная колонка с bluetooth	
		Duplicate Data	
		https://prnt.sc/DdECHdsNBmWF	
		Yes	Rayyan

		# solution
			# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.dns-shop.ru' and 
				var2.keyword in (N'портативная колонка с bluetooth') and 
				var2.[date] = '2022-03-31' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-31@21:00:00@200@464302c9-2bae-4430-9574-e3f1484e9b10@BzxplkXeEeeMawANOiZi-g

			# successful rerun
			# done react, done sheet

	(R D-3) 2942	2022-03-31	New CMS	200	all	www.re-store.ru	
		/ акустика wi-fi - 7
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ НАУШНИКИ С ПРОВОДОМ - 16
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		/ ПОРТАТИВНАЯ АКУСТИКА - 25
			eeda0758-a7e5-4f73-a323-3b8016de133d@a4059226-f529-46c6-8a35-c120219fbbab
		/ ПОРТАТИВНАЯ КОЛОНКА - 25
			6889e93b-60e8-4b27-a39e-690c8afbe53d@a4059226-f529-46c6-8a35-c120219fbbab
		/ ПОРТАТИВНАЯ КОЛОНКА WI-FI - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		/ ПРОВОДНЫЕ НАУШНИКИ - 14
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab
		/ СТАЦИОНАРНАЯ АКУСТИКА - 25
			4a3b4d25-e0df-44a0-a8a8-bd78e8693371@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники вкладыши - 2
			513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4(Fetched Data is Lesser than expected)
		Data Count Mismatch 7(Fetched Data is Lesser than expected)
		Data Count Mismatch 2(Fetched Data is Lesser than expected)
		Data Count Mismatch 1(Fetched Data is Lesser than expected)
		Data Count Mismatch 2(Fetched Data is Lesser than expected)
		Data Count Mismatch 2(Fetched Data is Lesser than expected)
		Data Count Mismatch 5(Fetched Data is Lesser than expected)
		Data Count Mismatch 1(Fetched Data is Lesser than expected)	

		https://prnt.sc/Mq0SnWbwtPnb
		https://prnt.sc/ZLuUKBenwQk2
		https://prnt.sc/cIJGaLTB-PDM
		https://prnt.sc/Nd_Z18u1V84t
		https://prnt.sc/CwTsadenpeBp
		https://prnt.sc/6PTz4Eg-IUtV
		https://prnt.sc/7wtLIuSG6y0L
		https://prnt.sc/y4urvA1ciIJU	

		Yes	Rayyan

		# solution
			# SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-03-31' 
				AND website = 'www.re-store.ru' 
				AND keyword in (N'наушники вкладыши')
				GROUP BY keyword, ritem_id, country, source

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-31@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@eeda0758-a7e5-4f73-a323-3b8016de133d@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@6889e93b-60e8-4b27-a39e-690c8afbe53d@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@4a3b4d25-e0df-44a0-a8a8-bd78e8693371@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@513b67c4-41e3-4399-9732-44e159c4bd79@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-31@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@6889e93b-60e8-4b27-a39e-690c8afbe53d@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@4a3b4d25-e0df-44a0-a8a8-bd78e8693371@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-31@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab,2022-03-31@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

			# [2] python rerun_rnk.py -s CRAWL_FINISHED -l 2022-03-31@21:00:00@200@b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns successful
			# 

	-----
	(R D-3) 1005	2022-03-31	New CMS	O00	all	www.krefel.be/nl	
		/ 'Dryer',
			O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a
		/ 'Side By Side',
			O00@IS0@4811175a-2f33-41c2-abb1-471ece3bfbd2
		/ 'Washer',
			O00@IS0@30bc2af4-294d-49d5-a32f-bb81cdbf2d89
		/ 'Vacuum'
			O00@IS0@34cdcab6-2bda-4712-a2be-d19474c79ddb

		Duplicate Product Website/Product Url	

		https://prnt.sc/iTO46PdZEVK3
		https://prnt.sc/TKvg9wZKB_Se
		https://prnt.sc/NvGY5H0uBi0I
		https://prnt.sc/pHYmdoRwLUUl	

		Yes	Joemike

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.krefel.be/nl' and 
				var2.category in (N'Dryer') and 
				var2.[date] = '2022-03-31' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a,2022-03-31@22:00:00@O00@IS0@4811175a-2f33-41c2-abb1-471ece3bfbd2,2022-03-31@22:00:00@O00@IS0@30bc2af4-294d-49d5-a32f-bb81cdbf2d89,2022-03-31@22:00:00@O00@IS0@34cdcab6-2bda-4712-a2be-d19474c79ddb

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-31@22:00:00@O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a,2022-03-31@22:00:00@O00@IS0@34cdcab6-2bda-4712-a2be-d19474c79ddb

			# python rerun_lst.py -s CRAWl_FINISHED -l 2022-03-31@22:00:00@O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a,2022-03-31@22:00:00@O00@IS0@34cdcab6-2bda-4712-a2be-d19474c79ddb

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a,2022-03-31@22:00:00@O00@IS0@4811175a-2f33-41c2-abb1-471ece3bfbd2

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@IS0@f2f7855a-974c-46db-88f5-d5115352b66a,2022-03-31@22:00:00@O00@IS0@4811175a-2f33-41c2-abb1-471ece3bfbd2

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-31@22:00:00@O00@IS0@4811175a-2f33-41c2-abb1-471ece3bfbd2

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@IS0@30bc2af4-294d-49d5-a32f-bb81cdbf2d89

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-31@22:00:00@O00@IS0@30bc2af4-294d-49d5-a32f-bb81cdbf2d89

			# multiple reruns successful

			# done react, done sheet

	(R D-1) 1006	2022-03-31	New CMS	O00	all	www.mediamarkt.be/nl/	
		Washer	
		Duplicate Product Website/Product Url	
		https://prnt.sc/MDpILNlGvomY	
		Yes	Joemike

		# solution
			#SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.mediamarkt.be/nl/' and 
				var2.category in (N'Washer') and 
				var2.[date] = '2022-03-31' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@2D0@20e8ffad-f301-4a3a-a565-4f7c05b52de6
			# successful rerun
			# done react, done sheet

	(F D-2) 1007	2022-03-31	New CMS	O00	all	www.artencraft.be	
		Vacuum	
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/ZmeynMIfW3UZ	
		Yes	Joemike

		# solution
			# SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.artencraft.be' and 
				var2.category in (N'Vacuum') and 
				var2.[date] = '2022-03-31' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source	

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-03-31@22:00:00@O00@QT0@397188eb-8cb7-4217-8948-b72b377c24e2	

			# fix:
				# update pagination
				# for rebuild> trello commented and moved
				# tested ? trello commented and moved

	(R D-1) 1008	2022-03-31	New CMS	O00	all	www.wehkamp.nl	
		TV 2020	
		1 Day Auto Copy Over	
		https://prnt.sc/0McfLSpb2Il4	
		Yes	Joemike

		# solution
			# SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-03-31' 
				AND website = 'www.wehkamp.nl' 
				AND category in (N'TV 2020')
				GROUP BY category, litem_id, country, category_url, source

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-03-31@22:00:00@O00@H50@ac532354-d708-446f-b4d5-81333885cf10
			# successful rerun
			# done rect, done sheet

	Stats:
		Rerun: 6 [10]
		Fix: 2[1.5]


-----------------------------------------------------
# Learnings
	# always check couch for ppt
		# if ppt, rerun atleast 6 times with interval
		# if not, ask teams for trello recommendation

	# causes why visible = 0
		# -1 availability, -1 ratings, -1 price
		# write jobs

	# causes why visible = 0 in 3p (3rd parties)
		# 0 visibility if 3p has no 3p items

	# (N'russian')
		# national language character set

	# trello retest
		# copy override leading to not work in rerun

	# COMMENTS
		# comment in sheet if maximum reruns but no spider issues
			# Please request for copy over for these item ids as of the moment: 
			# 200CZ9D080000000552060201904050701
			# 200CZ9D080000000804580201907080637

		# auto copy over but site is up and if valid deadlink
			# Remaining ACO item ids are valid deadlink; Request to set to deadlink for these item ids

		# 3p items; good in local but not fetched during rerun; blocked
			# Request copy over for now. Data is now ETL but 3P items are not fetched due to being blocked. Issue under observation

	# Opt not to Trello because no issues in spiders and site but blocked:
		# request item id for Manual Override
		# but Manual Override is not valid if the issue is data count mismatchk   