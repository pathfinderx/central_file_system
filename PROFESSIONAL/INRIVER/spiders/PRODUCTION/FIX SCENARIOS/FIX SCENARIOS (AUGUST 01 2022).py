08 01 22
	(W) EMEA region

	(W) 8200	2022-08-01	100	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-08-01' and webshot_url is NULL	
		Missing Webshot		
		Yes	Jeremy

	(W) NA region

	(F D-1) KEY-371	2022-07-29	2022-07-29	Razer (K00)	Missing description in www.materiel.net	
		K00FRN604N020220725054818554283206	
		05-aug-22	
		PRODUCTS-4003	
		https://prnt.sc/ThSXNuL9BuXd

		# SELECT * FROM view_all_productdata WHERE date='2022-08-01' AND website = 'www.materiel.net' AND item_id in ('K00FRN604N020220725054818554283206')

		# fixed: unhandled tag and new description source

	(F D-1) KEY-367	2022-07-29	2022-07-29	3M (A10)	invalid html tag in description in e-zikoapteka.pl	
		all item_ids	
		05-aug-22	
		PRODUCTS-3994	
		https://prnt.sc/7Cu92CySmzR_

		# SELECT * FROM view_all_productdata where date='2022-08-01' AND website = 'www.e-zikoapteka.pl' ORDER BY description DESC

		# fixed: filtration of relevant description

	SQL updates
		(SP 3) Request 1:
			Request to Copy Over Garmin Listings

			DB: 100

			Table: view_all_listingsdata

			Source Date: 2022-7-31
			Target Date: 2022-8-1

			# executed (res: https://prnt.sc/YVR6rxYvd9HQ)
				-- LISTINGS: COPY OVER BY DATE --
				DECLARE @source_date DATE,
						@target_date DATE;

				-------------------------------------------------------------
				-- Set source date
				SET @source_date = '2022-07-31'

				-- Set target date
				SET @target_date = '2022-08-01'

				-------------------------------------------------------------

				PRINT CHAR(13) + 'Deleted backdoor data:'
				DELETE FROM backdoor_etl_listings_data
				WHERE record_date = @target_date

				PRINT CHAR(13) + 'Deleted dead links:'
				DELETE FROM listings_data_dead_links
				WHERE record_date = @target_date

				PRINT CHAR(13) + 'Deleted copy over data:'
				DELETE o FROM listings_data_override o
				JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
				WHERE o.recorded = @target_date

				PRINT CHAR(13) + 'Deleted ETL data:'
				DELETE i FROM listings_data_element_image_lookup i
				JOIN listings_data_elements e ON i.element_id = e.id
				JOIN listings_data_thread t ON e.thread_id = t.id
				JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
				WHERE t.recorded = @target_date

				DELETE e FROM listings_data_elements e
				JOIN listings_data_thread t ON e.thread_id = t.id
				JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
				WHERE t.recorded = @target_date

				PRINT CHAR(13) + 'Copied over from backdoor data:'
				INSERT INTO backdoor_etl_listings_data(
					record_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
					description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
					promo_description_id, promo_price, promo_currency, availability, page_number, source)
				SELECT 
					@target_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
					description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
					promo_description_id, promo_price, promo_currency, availability, page_number, 'be_copy_override'
				FROM backdoor_etl_listings_data
				WHERE record_date = @source_date

				PRINT CHAR(13) + 'Copied over from dead links:'
				INSERT INTO listings_data_dead_links(record_date, postal_code, cms_listings_list_id, is_confirmed)
				SELECT 
					@target_date, postal_code, cms_listings_list_id, is_confirmed FROM listings_data_dead_links
				WHERE record_date = @source_date

				PRINT CHAR(13) + 'Copied over from copy over data:'
				INSERT INTO listings_data_override(
					recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
					currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
					[availability], page_number, thread_id, element_id, copy_type)
				SELECT 
					@target_date, o.listings_data_yoke_id, o.is_published, o.url_id, o.title_id, o.description_id, o.[rank], o.price,
					o.currency, o.unit_price, o.unit_currency, o.unit_metric, o.promo_description_id, o.promo_price, o.promo_currency,
					o.[availability], o.page_number, o.thread_id, o.element_id, 'manual_copy_over' 
				FROM listings_data_override o
				JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
				WHERE o.recorded = @source_date

				PRINT CHAR(13) + 'Copied over from ETL data:'
				INSERT INTO listings_data_override(
					recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
					currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
					[availability], page_number, thread_id, element_id, copy_type)
				SELECT 
					@target_date, t.listings_data_yoke_id, t.is_published, e.url_id, e.title_id, e.description_id, e.[rank], e.price,
					e.currency, e.unit_price, e.unit_currency, e.unit_metric, e.promo_description_id, e.promo_price, e.promo_currency,
					e.[availability], e.page_number, e.thread_id, e.id, 'manual_copy_over' 
				FROM listings_data_elements e
				JOIN listings_data_thread t ON e.thread_id = t.id
				JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
				WHERE t.recorded = @source_date

				SELECT [date], source, count(1) FROM view_all_listings_data
				WHERE [date] in (@source_date, @target_date)
				GROUP BY [date], source
				ORDER BY date, source

		(SP 2) Request 2:
			Request to Set Deadlink Rankings Data in Garmin

			DB: 100

			Table: view_all_rankingsdata 

			Date: 08/01/2022

			Retailer:
			www.fjellsport.no

			Keywords:
			HUNDEPEILER

			Screenshot:
			https://prnt.sc/wKE3_yk5F9qu

			Reason: 
			Fetching data even if it''s deadlink (PDP).

			# SELECT * FROM view_all_rankingsdata var where date='2022-08-01' AND website = 'www.fjellsport.no'AND keyword in (N'HUNDEPEILER')

			# executed (res: https://prnt.sc/pZ3oKebqsvNQ)
				-- RANKINGS: SET TO DEAD LINK BY DATE + RETAILER + KEYWORD --
				DECLARE @target_date DATE,
						@website_url VARCHAR(255),
						@website_id INT,
						@is_confirmed BIT = 1;
				DECLARE @keyword_id_tbl AS TABLE(keyword_id int);

				-------------------------------------------------------------
				-- Set target date
				SET @target_date = '2022-08-01' 

				-- Set target website URL with 'www.' prefix
				SET @website_url = 'www.fjellsport.no'

				-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
				PRINT CHAR(13) + 'Number of selected keywords:'
				INSERT INTO @keyword_id_tbl(keyword_id)
				SELECT pg_id FROM rcms_keywords
				WHERE [name] Collate SQL_Latin1_General_CP850_BIN2 
				in ( 
				N'HUNDEPEILER'
				)
				------------------------------------------------------------

				SELECT TOP 1 @website_id = pg_id FROM rcms_websites
				WHERE website_url = @website_url

				PRINT CHAR(13) + 'Deleted backdoor data:'
				DELETE FROM backdoor_etl_rankingsdata
				WHERE record_date = @target_date AND
					cms_website_id = @website_id AND
					cms_keyword_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted ETL data:'
				DELETE e FROM etl_rankings_yoke_elements e
				JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
				JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
				WHERE CAST(t.recorded AS DATE) = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted copy over data:'
				DELETE o FROM etl_rankingsdata_override o
				JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
				WHERE recorded = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted dead link data:'
				DELETE FROM rankingsdata_dead_links
				WHERE record_date = @target_date AND
					cms_websites_id = @website_id AND
					cms_keywords_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Inserted dead link data:'
				INSERT INTO rankingsdata_dead_links(record_date, cms_websites_id, cms_keywords_id, is_confirmed)
				SELECT 
					@target_date, @website_id, keyword_id, @is_confirmed
				FROM @keyword_id_tbl

				SELECT
					[date], website, keyword, source 
				FROM view_all_rankingsdata
				WHERE [date] = @target_date AND
					website_id = @website_id AND
					keyword_id in (
						select keyword_id from @keyword_id_tbl
					)

		(SP 2) Request 3:
			Request to Set Deadlink Rankings Data in Garmin Rankings

			DB: 100

			Table: view_all_rankingsdata

			Date: 08/01/2022

			Retailer:
			www.dustinhome.se

			Keywords:
			BIL GPS

			Screenshot:

			https://prnt.sc/VaJYQk81k-ya

			Reason: Fetching data even if it''s deadlink (PDP)

			# SELECT * FROM view_all_rankingsdata var where date='2022-08-01' AND website = 'www.dustinhome.se' AND keyword in (N'BIL GPS')

			# execution (res: https://prnt.sc/0fU__gSxqDDC)
				-- RANKINGS: SET TO DEAD LINK BY DATE + RETAILER + KEYWORD --
				DECLARE @target_date DATE,
						@website_url VARCHAR(255),
						@website_id INT,
						@is_confirmed BIT = 1;
				DECLARE @keyword_id_tbl AS TABLE(keyword_id int);

				-------------------------------------------------------------
				-- Set target date
				SET @target_date = '2022-08-01' 

				-- Set target website URL with 'www.' prefix
				SET @website_url = 'www.dustinhome.se'

				-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
				PRINT CHAR(13) + 'Number of selected keywords:'
				INSERT INTO @keyword_id_tbl(keyword_id)
				SELECT pg_id FROM rcms_keywords
				WHERE [name] Collate SQL_Latin1_General_CP850_BIN2 
				in ( 
				N'BIL GPS'
				)
				------------------------------------------------------------

				SELECT TOP 1 @website_id = pg_id FROM rcms_websites
				WHERE website_url = @website_url

				PRINT CHAR(13) + 'Deleted backdoor data:'
				DELETE FROM backdoor_etl_rankingsdata
				WHERE record_date = @target_date AND
					cms_website_id = @website_id AND
					cms_keyword_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted ETL data:'
				DELETE e FROM etl_rankings_yoke_elements e
				JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
				JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
				WHERE CAST(t.recorded AS DATE) = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted copy over data:'
				DELETE o FROM etl_rankingsdata_override o
				JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
				WHERE recorded = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted dead link data:'
				DELETE FROM rankingsdata_dead_links
				WHERE record_date = @target_date AND
					cms_websites_id = @website_id AND
					cms_keywords_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Inserted dead link data:'
				INSERT INTO rankingsdata_dead_links(record_date, cms_websites_id, cms_keywords_id, is_confirmed)
				SELECT 
					@target_date, @website_id, keyword_id, @is_confirmed
				FROM @keyword_id_tbl

				SELECT
					[date], website, keyword, source 
				FROM view_all_rankingsdata
				WHERE [date] = @target_date AND
					website_id = @website_id AND
					keyword_id in (
						select keyword_id from @keyword_id_tbl
					)

		(SP 2) Request 4: 
			Request to Set Deadlink Listings Data in Garmin

			DB: 100

			Table: view_all_listingsdata

			Date: 08/01/2022

			Retailer: www.eventyrsport.dk

			Category: HÅNDHOLDTE GPS''ER

			Listing uuid: 
			e77be6c2-4204-4b7b-aee0-716984254d2b

			Screenshot:
			https://prnt.sc/92hMPdV1R_Ea

			Reason: 
			Fetching data even if it''s deadlink

			# SELECT * FROM view_all_listings_data WHERE date = '2022-08-01' AND website = 'www.eventyrsport.dk' AND listings_uuid in ('e77be6c2-4204-4b7b-aee0-716984254d2b')

			# execution (https://prnt.sc/U4SmeLhUu6mP) 
				-- LISTINGS: SET TO DEAD LINK BY DATE + LISTING_UUID + POSTAL_CODE --
				DECLARE @target_date DATE,
						@postal_code VARCHAR(10),
						@is_confirmed BIT = 1;
				DECLARE @listings_uuid_id_tbl AS TABLE(listing_uuid_id INT);

				-------------------------------------------------------------
				-- Set postal code, default = '-1' (no postal code)
				SET @postal_code = '-1'

				-- Set target date
				SET @target_date = '2022-08-01' 

				-- Set target listing_uuid
				PRINT CHAR(13) + 'Number of selected listing_uuid:'
				INSERT INTO @listings_uuid_id_tbl(listing_uuid_id)
				SELECT pg_id FROM rcms_listings
				WHERE listing_uuid in (
				'e77be6c2-4204-4b7b-aee0-716984254d2b'
				)
				-------------------------------------------------------------

				PRINT CHAR(13) + 'Deleted backdoor data:'
				DELETE FROM backdoor_etl_listings_data
				WHERE record_date = @target_date
					AND postal_code = @postal_code
					AND cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)

				PRINT CHAR(13) + 'Deleted dead links:'
				DELETE FROM listings_data_dead_links
				WHERE record_date = @target_date
					AND postal_code = @postal_code
					AND cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)

				PRINT CHAR(13) + 'Deleted copy over data:'
				DELETE o FROM listings_data_override o
				JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
				WHERE o.recorded = @target_date
					AND y.postal_code = @postal_code
					AND y.cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)

				PRINT CHAR(13) + 'Deleted ETL data:'
				DELETE i FROM listings_data_element_image_lookup i
				JOIN listings_data_elements e ON i.element_id = e.id
				JOIN listings_data_thread t ON e.thread_id = t.id
				JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
				WHERE t.recorded = @target_date
					AND y.postal_code = @postal_code
					AND y.cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)

				DELETE e FROM listings_data_elements e
				JOIN listings_data_thread t ON e.thread_id = t.id
				JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
				WHERE t.recorded = @target_date
					AND y.postal_code = @postal_code
					AND y.cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)

				PRINT CHAR(13) + 'Set to dead links:'
				INSERT INTO listings_data_dead_links(record_date, cms_listings_list_id, postal_code,is_confirmed)
				SELECT 
					@target_date, listing_uuid_id, @postal_code, @is_confirmed
				FROM @listings_uuid_id_tbl

				SELECT [date], listings_uuid, source, count(1) FROM view_all_listings_data
				WHERE [date] = @target_date
					AND postal_code = @postal_code
					AND cms_listings_list_id IN (
						SELECT listing_uuid_id FROM @listings_uuid_id_tbl
					)
				GROUP BY [date], listings_uuid, source
				ORDER BY listings_uuid, date, source

		(SP 2) Request 5:
			Request for update in ZOUND BUYBOX

			DB: 200
			Table: view_buybox
			Date: 2022-08-01
			Field for update: bb_price

			Reason: invalid -1 value

			# SELECT * FROM view_buybox where date='2022-08-01' AND website = 'www.amazon.de'	AND item_id in ('200DEY007R020220524073425969025144')		
			# executed (res: https://prnt.sc/1Pkg9X1Ul11W)
				-- BUYBOX: UPDATE PRICE VALUE --
				declare @target_date date,
						@item_id varchar(255),
						@sku_id int,
						@price_value money;

				------------------------------------
				-- Set target date
				set @target_date = '2022-08-01' 

				-- Set target item_id
				set @item_id = '200DEY007R020220524073425969025144' 

				-- Set retailer_name
				set @price_value = 45.99
				------------------------------------

				select top 1 @sku_id = pg_id from rcms_product_skus
				where item_id = @item_id


				print char(13) + 'Updated ETL data:'
				update eb set eb.price = @price_value
				from etl_buyboxdata eb
				join etl_productdata ep on eb.etl_productdata_id = ep.id
				where ep.record_date = @target_date
				and ep.cms_product_sku_id = @sku_id

				print char(13) + 'Updated copy over data:'
				update etl_buyboxdata_override set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Updated backdoor data:'
				update backdoor_etl_buyboxdata set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Result:'
				select [date], item_id, price_value from view_buybox
				where [date] = @target_date
				and item_id = @item_id

	Special Spider Development
		* microsoft.com.fr/fr

08 02 22
	(W) EMEA region

	(W) 8272	2022-08-02	H10	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-08-02' and webshot_url is NULL	
		Missing Webshots. Request Rerun Interval.		
		Yes	Keeshia

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-02' and webshot_url is NULL and source = 'etl' order by item_id

	(W) NA region

	Special Rerun
		(SP D-2) request 1:
			Good morning!

			Request to rerun Auto copy over in amazon.it

			DB: A00
			Retailer: amazon.it
			Issue: Auto copy over

			Affected item id: 

			A00ITL40RB000000993830202007290904
			A00ITL40RB000000993840202007290904
			A00ITL40RB000000993850202007290904
			A00ITL40RB000000993870202007290904
			A00ITL40RB000001235210202011060456
			A00ITL40KT020211025061817490563298
			A00ITL40KT020211115034840600189319
			A00ITL40KT020220502203424547916122
			A00ITL40KT020220502203424987263122
			A00ITL40KT020220502203425792145122
			A00ITL40KT020220502203426227137122
			A00ITL40KT020220502203427056445122
			A00ITL40KT020220502203428264178122
			A00ITL40KT020220502203428926621122
			A00ITL40KT020220502203429896627122
			A00ITL40KT020220502203430213663122
			A00ITL40KT020220502203430512672122
			A00ITL40KT020220502203430839618122
			A00ITL40KT020220502203431139513122
			A00ITL40KT020220502203431467826122
			A00ITL40KT020220502203431902590122
			A00ITL40KT020220502203432338459122
			A00ITL40KT020220502203432658461122
			A00ITL40KT020220502203432932682122
			A00ITL40KT020220502203433305823122
			A00ITL40KT020220502203433711925122
			A00ITL40KT020220502203433993006122
			A00ITL40KT020220502203434405715122
			A00ITL40KT020220502203434646458122
			A00ITL40KT020220502203434976835122

			# SELECT * FROM view_all_productdata where date='2022-08-02' AND item_id in ('A00ITL40RB000000993830202007290904',
				'A00ITL40RB000000993840202007290904',
				'A00ITL40RB000000993850202007290904',
				'A00ITL40RB000000993870202007290904',
				'A00ITL40RB000001235210202011060456',
				'A00ITL40KT020211025061817490563298',
				'A00ITL40KT020211115034840600189319',
				'A00ITL40KT020220502203424547916122',
				'A00ITL40KT020220502203424987263122',
				'A00ITL40KT020220502203425792145122',
				'A00ITL40KT020220502203426227137122',
				'A00ITL40KT020220502203427056445122',
				'A00ITL40KT020220502203428264178122',
				'A00ITL40KT020220502203428926621122',
				'A00ITL40KT020220502203429896627122',
				'A00ITL40KT020220502203430213663122',
				'A00ITL40KT020220502203430512672122',
				'A00ITL40KT020220502203430839618122',
				'A00ITL40KT020220502203431139513122',
				'A00ITL40KT020220502203431467826122',
				'A00ITL40KT020220502203431902590122',
				'A00ITL40KT020220502203432338459122',
				'A00ITL40KT020220502203432658461122',
				'A00ITL40KT020220502203432932682122',
				'A00ITL40KT020220502203433305823122',
				'A00ITL40KT020220502203433711925122',
				'A00ITL40KT020220502203433993006122',
				'A00ITL40KT020220502203434405715122',
				'A00ITL40KT020220502203434646458122',
				'A00ITL40KT020220502203434976835122')

			{
			    "response": {
			        "task_uuid": "fa365018-a5a6-4a98-8724-335198678a99"
			    },
			    "version": "1.0"
			}

			# A00ITL40KT020220502203425792145122
			{
			    "response": {
			        "task_uuid": "1c76a19b-3315-4bad-a377-adda2daf4176"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "acfbeed9-77bb-4e82-b58c-cbc08f48cab7"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "0923941b-f483-48f0-b739-f286b3b762aa"
			    },
			    "version": "1.0"
			}

	Sql updates
		(SP D-2) Request 1:
			Request for copy over in ZURN

			DB: H10
			Table: productdata (IMAGES ONLY)
			Source Date: 2022-06-19
			Target Date: 2022-08-02

			Item_id: H10US8X0Q8220220202031743134606033

			Reason: missing image_count 

			# SELECT * FROM view_all_productdata where date='2022-06-19' AND item_id in ('H10US8X0Q8220220202031743134606033')

			# exec (res: https://prnt.sc/Tcn4JhJ5KGzG) 
				-- PROD: IMAGE URL COPY OVER BY DATE + ITEM_ID --
				declare @sku_id_tbl as table(id int);
				declare @search_sku_id_tbl as table(id int);
				declare @source_image_url_tbl as table(record_date DATE, cms_product_skus_id INT, atheneum_id INT);
				declare @target_date date, @source_date date, @cursor int = 1, @row_count INT = 0,
						@sku_id int, @etl_id int, @source varchar(50), @override_id int;

				---------------------------------------------------------
				-- Set source date
				set @source_date = '2022-06-19'

				-- Set target date
				set @target_date = '2022-08-02' 

				-- Set target item_ids:
				print 'Number of selected item_id/s:'
				insert into @sku_id_tbl(id)
				select pg_id from rcms_product_skus
				where item_id in (
				'H10US8X0Q8220220202031743134606033'
				)
				---------------------------------------------------------

				insert into @search_sku_id_tbl(id)
				select id from @sku_id_tbl

				select @row_count = count(1) from @sku_id_tbl;

				while @cursor <= @row_count
				begin
					select top 1 @sku_id = id from @sku_id_tbl

					select top 1 @etl_id = id from etl_productdata
					where cms_product_sku_id = @sku_id and
						record_date = @source_date

					if exists (
						select 1 from etl_productdata_image_primer
						where etl_productdata_id = @etl_id
					)
					begin
						print ''
						print 'Etl source data:'
						insert @source_image_url_tbl(record_date,cms_product_skus_id,atheneum_id)
						select @target_date, @sku_id, atheneum_id FROM etl_productdata_image_primer
						where etl_productdata_id = @etl_id
					end
					else if exists(
						select 1 from productdata_image_url_override
						where cms_product_skus_id = @sku_id and
							record_date = @source_date
					)
					begin
						print ''
						print 'Copy over source data:'
						insert @source_image_url_tbl(record_date,cms_product_skus_id,atheneum_id)
						select @target_date, @sku_id, atheneum_id FROM productdata_image_url_override
						where cms_product_skus_id = @sku_id and
							record_date = @source_date
					end
					else if exists(
						select 1 from backdoor_productdata_image_url
						where cms_product_skus_id = @sku_id and
							record_date = @source_date
					)
					begin
						print ''
						print 'Backdoor source data:'
						insert @source_image_url_tbl(record_date,cms_product_skus_id,atheneum_id)
						select @target_date, @sku_id, atheneum_id FROM backdoor_productdata_image_url
						where cms_product_skus_id = @sku_id and
							record_date = @source_date
					end
					else
					begin
						print ''
						print 'No source data. Copy over aborted.'
					end
						
					select top 1 @source = [source] from view_all_productdata
					where fk_sku_pgid = @sku_id and
						[date] = @target_date

					if @source = 'etl'
					begin
						print ''
						print 'deleted data on etl.'
						delete pmr from etl_productdata_image_primer pmr
						join etl_productdata etl on pmr.etl_productdata_id = etl.id
						where etl.record_date = @target_date and
							etl.cms_product_sku_id = @sku_id

						print ''
						print 'Done copy over on etl.'
						insert etl_productdata_image_primer(etl_productdata_id, atheneum_id)
						select etl.id, img.atheneum_id 
						from @source_image_url_tbl img
						join etl_productdata etl on img.cms_product_skus_id = etl.cms_product_sku_id and
							img.record_date = etl.record_date

					end
					else if @source like '%_copy_over'
					begin
						print ''
						print 'deleted data on copy over.'
						delete from productdata_image_url_override
						where record_date = @target_date and
							cms_product_skus_id = @sku_id

						print ''
						print 'Done copy over.'
						insert productdata_image_url_override(record_date,cms_product_skus_id,atheneum_id,[source])
						select record_date,cms_product_skus_id,atheneum_id,'manual_copy_over' from @source_image_url_tbl
					end
					else if @source like '%_override'
					begin
						print ''
						print 'deleted data on backdoor.'
						delete from backdoor_productdata_image_url
						where record_date = @target_date and
							cms_product_skus_id = @sku_id

						print ''
						print 'Done copy over on backdoor.'
						insert backdoor_productdata_image_url(record_date,cms_product_skus_id,atheneum_id,[source])
						select record_date,cms_product_skus_id,atheneum_id,'manual_override' from @source_image_url_tbl
					end
					else
					begin
						print ''
						print 'Cannot find data on target date. Copy over aborted.'
					end

					print ''
					print 'Cleared item_id temporary table.'
					delete from @sku_id_tbl
					where id = @sku_id

					print 'Cleared image URL temporary table.'
					delete from @source_image_url_tbl

					set @cursor = @cursor + 1
				end

				select item_id, date, image_count from view_all_productdata
				where date in (@source_date, @target_date)
				and fk_sku_pgid in (
					select id from @search_sku_id_tbl
				)
				order by item_id, date, image_count

		(SP D-3) Request 2:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2022-08-02
			Field for update: is_new

			Reason:, -2 values

	fr_rebuild (F D-3) (R D-3) KEY-368	2022-07-29	2022-07-29	Garmin (100) Spacing issue in description in golfsky.fi/fi/	
		All item_ids
		sample item_id:
		100FI331ZQ020220725025256893471206
		05-aug-22	PRODUCTS-4008	
		https://prnt.sc/La3tnSdiyW96	

		# SELECT * FROM view_all_productdata where date='2022-08-02' AND website = 'www.golfsky.fi/fi/'	AND item_id in ('100FI331ZQ020220725025256893471206')

		# fixed modified shit 

	fr_rebuild(F D-3) (R D-3) KEY-369	2022-07-29	2022-07-29	Garmin (100)	Incompleted specifications in golfsky.fi/fi/	
		'100FI331ZQ020220725025256893471206',
		'100FI331ZQ020220725025257053870206',
		'100FI331ZQ020220725025257198540206',
		'100FI331ZQ020220725025257515426206',
		'100FI331ZQ020220725025257925374206',
		'100FI331ZQ020220725025257641372206'
		05-aug-22	
		PRODUCTS-4008	
		https://prnt.sc/sxXBnBg-jaDv

		# SELECT * FROM view_all_productdata where date='2022-08-02' AND website = 'www.golfsky.fi/fi/'	AND item_id in ('100FI331ZQ020220725025256893471206',
		'100FI331ZQ020220725025257053870206',
		'100FI331ZQ020220725025257198540206',
		'100FI331ZQ020220725025257515426206',
		'100FI331ZQ020220725025257925374206',
		'100FI331ZQ020220725025257641372206')


08 03 22
	(W) EMEA region

	(W) 8306 2022-08-03	K00	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-08-03' and webshot_url is NULL	
		Missing Webshots		
		Yes	Jeremy 

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-03' and webshot_url is NULL and source = 'etl' order by item_id

	(W) NA region

	SQL updates
		(R D-3) Request 1:
			Request to Remove Duplicate Data in Razer

			DB: K00

			Table: view_all_listingsdata

			Date: 2022-7-27

			Retailer:
			www.ebuyer.com

			Category:
			Gaming Laptops
			Gaming Headsets
			Gaming Mice
			Gaming Keyboards

			Listings UUID:
			'7e77dbe9-5d4b-4d75-aebe-a6cb8278d94b',
			'b9fdcad8-cf5e-4d36-babd-881623a7a746',
			'3270c547-102f-4cd0-b7a7-96f75b444405',
			'4a59a353-ae2e-4dc5-8cf9-b759a990b329'

			Screenshot:
			https://prnt.sc/4tRMf0EmdBWw

			Reason: 
			Duplicate Data

			# exec (res: https://prnt.sc/HVQWrpq4Dy2R)
				-- DELETE DEAD LINK DATA BY DATE + POSTAL_CODE + LISTINGS_UUID --
				declare @postal_code VARCHAR(255),
						@target_date DATE;
				declare @cms_listing_id_tbl as table(id int)

				-----------------------------------------------------------------
				-- Set target date
				set @target_date = '2022-07-27'

				-- Set target postal code, no postal = '-1'
				set @postal_code = '-1'

				-- Set target listings_uuid, comma separated
				insert into @cms_listing_id_tbl(id)
				select pg_id from rcms_listings
				where listing_uuid in ('7e77dbe9-5d4b-4d75-aebe-a6cb8278d94b',
				'b9fdcad8-cf5e-4d36-babd-881623a7a746',
				'3270c547-102f-4cd0-b7a7-96f75b444405',
				'4a59a353-ae2e-4dc5-8cf9-b759a990b329')

				-----------------------------------------------------------------

				print ''
				print 'Deleted dead link:'
				delete from listings_data_dead_links
				where 
					record_date = @target_date and
					postal_code = @postal_code and
					cms_listings_list_id in (
						select id from @cms_listing_id_tbl
					)

				select date, listings_uuid, source, count(1) from view_all_listings_data
				where date = @target_date and 
					cms_listings_list_id in (
						select id from @cms_listing_id_tbl
					)
				group by date, listings_uuid, source

		(R D-3) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-03
			Field for update: is_new

			Reason:, -2 values

			# exec:
				-- 3P UPDATE IS_NEW --
				declare @target_date date,
						@item_id varchar(255),
						@sku_id int,
						@position int,
						@is_new smallint;

				------------------------------------
				-- Set target date
				set @target_date = '2022-08-03' 

				-- Set target item_id
				set @item_id = '200DE52080000001398790202105040415' 

				-- Set target position
				set @position = 2

				-- Set retailer_name
				set @is_new = 0
				------------------------------------

				update p set p.is_new = 0
				from etl_3pdata p
				join etl_productdata e on p.etl_productdata_id = e.id
				join rcms_product_skus s on e.cms_product_sku_id = s.pg_id 
				where e.record_date = @target_date and 
					s.item_id = @item_id and
					p.[rank] = @position

				update e3o set e3o.is_new = 0
				from etl_3pdata_override e3o
				join rcms_product_skus s on e3o.cms_product_sku_id = s.pg_id 
				where e3o.record_date = @target_date
				and s.item_id = @item_id
				and [rank] = @position

	Special
		(R D-10) update variants

	fr_rerun (F D-1) KEY-376	2022-08-01	2022-08-01	Razer (K00)	incorrect description in materiel.net	
		all item_ids

		sample id: K00FRN607U020220725034118281819206
		05-aug-22	PRODUCTS-4003	
		https://prnt.sc/GtYQwODywlXm	
		08/01 Switzell: the underline words are not found in pdp

		# SELECT * FROM view_all_productdata WHERE date='2022-08-01' AND website = 'www.materiel.net' AND item_id in ('K00FRN607U020220725034118281819206')

		# fixed: description unwanted characters and other unwanted description

	fr_rerun (F D-1) KEY-377	2022-08-01	2022-08-01	Razer (K00)	invalid html tag in description in topachat.com	
		'K00FR8317U020220725034139111066206',
		'K00FR8317U020220725034128743629206',
		'K00FR831P1020220725054823793310206',
		'K00FR8317U020220725034135494880206',
		'K00FR8317U020220725034130975924206',
		'K00FR831P1020220725054825000419206',
		'K00FR8317U020220725034125786280206',
		'K00FR831P1020220725054825093289206',
		'K00FR831U1020220725054825273047206',
		'K00FR8317U020220725034136882224206',
		'K00FR831P1020220725054828119942206',
		'K00FR8317U020220725034128355813206',
		'K00FR8317U020220725034127829060206',
		'K00FR8317U020220725034133471398206',
		'K00FR8317U020220725034130678846206',
		'K00FR8317U020220725034138458102206',
		'K00FR8317U020220725034136475083206',
		'K00FR8317U020220725034131234462206',
		'K00FR8317U020220725034131518340206'
		05-aug-22	PRODUCTS-4003	
		https://prnt.sc/obKvydTAVK3O

			# SELECT * FROM view_all_productdata WHERE date='2022-08-01' AND website = 'www.topachat.com' AND item_id in ('K00FR8317U020220725034139111066206',
			'K00FR8317U020220725034128743629206',
			'K00FR831P1020220725054823793310206',
			'K00FR8317U020220725034135494880206',
			'K00FR8317U020220725034130975924206',
			'K00FR831P1020220725054825000419206',
			'K00FR8317U020220725034125786280206',
			'K00FR831P1020220725054825093289206',
			'K00FR831U1020220725054825273047206',
			'K00FR8317U020220725034136882224206',
			'K00FR831P1020220725054828119942206',
			'K00FR8317U020220725034128355813206',
			'K00FR8317U020220725034127829060206',
			'K00FR8317U020220725034133471398206',
			'K00FR8317U020220725034130678846206',
			'K00FR8317U020220725034138458102206',
			'K00FR8317U020220725034136475083206',
			'K00FR8317U020220725034131234462206',

			'K00FR8317U020220725034131518340206')	
		(ref: KEY-377) KEY-379	2022-08-02	2022-08-02	Razer (K00)	invalid html tag in description in topachat.com	K00FR8317U020220725034135553863206	05-aug-22	PRODUCTS-4003	https://prnt.sc/EkXepBcxap3y	08/03: Kurt: unhandled special characters	For Rebuild

08 06 22
	(W) EMEA region

	(W) EMEA region

	SQL updates
		(SP D-3) Request 1:
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110

			Table: view_all_listingsdata
			Date: 08/06/2022
			Retailer: www.fnac.be/fr

			category:
			Software

			Listings UUID(s):
			d1104ee1-3f4a-42a1-ad43-7b4b4aea5f45

			Screenshot:
			https://prnt.sc/4yLPqBSOxF3d

			Reason: Fetching data even if it''s deadlink(Top level category)

			# SELECT * FROM view_all_listings_data WHERE date = '2022-08-06' AND website = 'www.fnac.be/fr' AND category in ('Software')

			# exec ()

		(SP D-2) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-06
			Field for update: visible,price_value

	(F D-3) (R D-3) KEY-382	2022-08-04	2022-08-04	Sasmung CE (O00)	incomplete description in hellotv.nl	
		all item_ids	
		12-aug-22	
		PRODUCTS-4017	
		https://prnt.sc/FdQszTM3SPUx

		# SELECT * FROM view_all_productdata where date='2022-08-06' AND website = 'www.hellotv.nl'

		# Added description extraction using multiple api request

08 07 22
	(W) EMEA region

	(W) NA region

	SQL updates:
		(SP D-2) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-08-07
			Target Date: 2022-08-06

			ItemID:
			200FR7D070000000551790201904050655

			# SELECT * from view_all_productdata vpd WHERE item_id in ('200FR7D070000000551790201904050655') AND DATE = '2022-08-06'

			# exec product_data (https://prnt.sc/DZvC7MTifaZs)

			# exec 3p (https://prnt.sc/hp2iOXlWP8kD)

		(SP D-2) Request 2:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2022-08-07
			Field for update: retailer_name
			Reason:, -2 values

			Thank you

		(SP D-3) Request 3:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-07
			Field for update: is_new

			Reason:, -2 values

		(SP D-2) Request 4:
			Request for copy over in inRiver-Testclient4

			DB: G10
			Table: productdata (DESCRIPTION ONLY)
			Source Date: 2022-07-31
			Target Date: 2022-08-07

			Retailer: www.amazon.com

			Item_id:

			'G10USW0008120220103042125662837003',
			'G10USW0008120220103042125895753003',
			'G10USW0008120220103042126086717003',
			'G10USW0008120220103042126268040003',
			'G10USW0008120220103042126453493003',
			'G10USW0008120220113091006345679013',
			'G10USW0008120220113091006582679013',
			'G10USW0008120220113091006775677013',
			'G10USW000A220220401082351331374091',
			'G10USW000A220220401082351974382091'


			Reason: Incomplete descriptions


	(F D-3) (R D-3) KEY-382	2022-08-04	2022-08-04	Sasmung CE (O00)	incomplete description in hellotv.nl	
		all item_ids	
		12-aug-22	
		PRODUCTS-4017	
		https://prnt.sc/FdQszTM3SPUx

		# SELECT * FROM view_all_productdata where date='2022-08-07' AND website = 'www.hellotv.nl'

		# Added dynamic description api pre-requisites

08 08 22
	(W) EMEA region

	SQL updates
		(SP D-3) Request 1:
		Request to Copy Over Garmin Listings
		DB: 100

		Table: view_all_listingsdata

		Source Date: 2022-8-7
		Target Date: 2022-8-8

		# resolving duplicate issue with sir Jon
		# exec (https://prnt.sc/9Nrk3qLgBPgX)
			-- LISTINGS: DELETE COPY OVER BY DATE + POSTAL_CODE + LISTINGS_UUID --
			declare @postal_code VARCHAR(255),
					@target_date DATE,
					@cms_listing_id INT;
			declare @cms_listing_id_tbl as table(id int)

			-----------------------------------------------
			-- Set target date
			set @target_date = '2022-08-07'

			-- Set target postal code, no postal = '-1'
			set @postal_code = '-1'

			-- Set target listings_uuid, comma separated
			print 'Number of selected listings_uuid:'
			insert into @cms_listing_id_tbl(id)
			select pg_id from rcms_listings
			where listing_uuid in (
			'07d4ac0c-443e-499d-80cc-db5de067b0bc'
			)
			-----------------------------------------------

			print char(13) + 'Deleted copy over:'
			delete [copy] from listings_data_override [copy]
			join listings_data_yoke yoke on [copy].listings_data_yoke_id = yoke.id
			where 
				[copy].recorded = @target_date and
				yoke.postal_code = @postal_code and
				yoke.cms_listings_list_id in (
					select id from @cms_listing_id_tbl
				)

			select date, listings_uuid, source, count(1) from view_all_listings_data
			where date = @target_date and 
				cms_listings_list_id in (
					select id from @cms_listing_id_tbl
				)
			group by date, listings_uuid, source

		# exec (https://prnt.sc/9zt6ytKmhiM2)
			-- LISTINGS: COPY OVER BY DATE --
			DECLARE @source_date DATE,
					@target_date DATE;

			-------------------------------------------------------------
			-- Set source date
			SET @source_date = '2022-08-07'

			-- Set target date
			SET @target_date = '2022-08-08'

			-------------------------------------------------------------

			PRINT CHAR(13) + 'Deleted backdoor data:'
			DELETE FROM backdoor_etl_listings_data
			WHERE record_date = @target_date

			PRINT CHAR(13) + 'Deleted dead links:'
			DELETE FROM listings_data_dead_links
			WHERE record_date = @target_date

			PRINT CHAR(13) + 'Deleted copy over data:'
			DELETE o FROM listings_data_override o
			JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
			WHERE o.recorded = @target_date

			PRINT CHAR(13) + 'Deleted ETL data:'
			DELETE i FROM listings_data_element_image_lookup i
			JOIN listings_data_elements e ON i.element_id = e.id
			JOIN listings_data_thread t ON e.thread_id = t.id
			JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
			WHERE t.recorded = @target_date

			DELETE e FROM listings_data_elements e
			JOIN listings_data_thread t ON e.thread_id = t.id
			JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
			WHERE t.recorded = @target_date

			PRINT CHAR(13) + 'Copied over from backdoor data:'
			INSERT INTO backdoor_etl_listings_data(
				record_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
				description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
				promo_description_id, promo_price, promo_currency, availability, page_number, source)
			SELECT 
				@target_date, cms_listings_list_id, postal_code, is_published, url_id, title_id,
				description_id, rank, price, currency, unit_price, unit_currency, unit_metric,
				promo_description_id, promo_price, promo_currency, availability, page_number, 'be_copy_override'
			FROM backdoor_etl_listings_data
			WHERE record_date = @source_date

			PRINT CHAR(13) + 'Copied over from dead links:'
			INSERT INTO listings_data_dead_links(record_date, postal_code, cms_listings_list_id, is_confirmed)
			SELECT 
				@target_date, postal_code, cms_listings_list_id, is_confirmed FROM listings_data_dead_links
			WHERE record_date = @source_date

			PRINT CHAR(13) + 'Copied over from copy over data:'
			INSERT INTO listings_data_override(
				recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
				currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
				[availability], page_number, thread_id, element_id, copy_type)
			SELECT 
				@target_date, o.listings_data_yoke_id, o.is_published, o.url_id, o.title_id, o.description_id, o.[rank], o.price,
				o.currency, o.unit_price, o.unit_currency, o.unit_metric, o.promo_description_id, o.promo_price, o.promo_currency,
				o.[availability], o.page_number, o.thread_id, o.element_id, 'manual_copy_over' 
			FROM listings_data_override o
			JOIN listings_data_yoke y ON o.listings_data_yoke_id = y.id
			WHERE o.recorded = @source_date

			PRINT CHAR(13) + 'Copied over from ETL data:'
			INSERT INTO listings_data_override(
				recorded, listings_data_yoke_id, is_published, url_id, title_id, description_id, [rank], price,
				currency, unit_price, unit_currency, unit_metric, promo_description_id, promo_price, promo_currency,
				[availability], page_number, thread_id, element_id, copy_type)
			SELECT 
				@target_date, t.listings_data_yoke_id, t.is_published, e.url_id, e.title_id, e.description_id, e.[rank], e.price,
				e.currency, e.unit_price, e.unit_currency, e.unit_metric, e.promo_description_id, e.promo_price, e.promo_currency,
				e.[availability], e.page_number, e.thread_id, e.id, 'manual_copy_over' 
			FROM listings_data_elements e
			JOIN listings_data_thread t ON e.thread_id = t.id
			JOIN listings_data_yoke y ON t.listings_data_yoke_id = y.id
			WHERE t.recorded = @source_date

			SELECT [date], source, count(1) FROM view_all_listings_data
			WHERE [date] in (@source_date, @target_date)
			GROUP BY [date], source
			ORDER BY date, source

		(SP D-2) Request 2:
			Request to Set Deadlink Rankings Data in Garmin
			DB: 100
			Table: view_all_rankingsdata 
			Date: 08/08/2022

			Retailer:
			www.fjellsport.no

			Keywords:
			HUNDEPEILER

			Screenshot:
			https://prnt.sc/9uCtfJos5zjB
			Reason: 
			Fetching data even if it''s deadlink (PDP).

			# exec (https://prnt.sc/EiCw3mEihs9B)
				-- RANKINGS: SET TO DEAD LINK BY DATE + RETAILER + KEYWORD --
				DECLARE @target_date DATE,
						@website_url VARCHAR(255),
						@website_id INT,
						@is_confirmed BIT = 1;
				DECLARE @keyword_id_tbl AS TABLE(keyword_id int);

				-------------------------------------------------------------
				-- Set target date
				SET @target_date = '2022-08-08' 

				-- Set target website URL with 'www.' prefix
				SET @website_url = 'www.fjellsport.no'

				-- Set target keywords, and COMMENT OUT Collate SQL_Latin1_General_CP850_BIN2 if needed
				PRINT CHAR(13) + 'Number of selected keywords:'
				INSERT INTO @keyword_id_tbl(keyword_id)
				SELECT pg_id FROM rcms_keywords
				WHERE [name] Collate SQL_Latin1_General_CP850_BIN2 
				in ( 
				N'HUNDEPEILER'
				)
				------------------------------------------------------------

				SELECT TOP 1 @website_id = pg_id FROM rcms_websites
				WHERE website_url = @website_url

				PRINT CHAR(13) + 'Deleted backdoor data:'
				DELETE FROM backdoor_etl_rankingsdata
				WHERE record_date = @target_date AND
					cms_website_id = @website_id AND
					cms_keyword_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted ETL data:'
				DELETE e FROM etl_rankings_yoke_elements e
				JOIN etl_rankings_yoke_thread t ON e.etl_rankings_yoke_thread_id = t.id
				JOIN etl_rankings_yoke y ON t.etl_rankings_yoke_id = y.id
				WHERE CAST(t.recorded AS DATE) = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted copy over data:'
				DELETE o FROM etl_rankingsdata_override o
				JOIN etl_rankings_yoke y ON o.etl_rankings_yoke_id = y.id
				WHERE recorded = @target_date AND
					y.cms_websites_pgid = @website_id AND
					y.cms_keywords_pgid IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Deleted dead link data:'
				DELETE FROM rankingsdata_dead_links
				WHERE record_date = @target_date AND
					cms_websites_id = @website_id AND
					cms_keywords_id IN (
						SELECT keyword_id FROM @keyword_id_tbl
					)

				PRINT CHAR(13) + 'Inserted dead link data:'
				INSERT INTO rankingsdata_dead_links(record_date, cms_websites_id, cms_keywords_id, is_confirmed)
				SELECT 
					@target_date, @website_id, keyword_id, @is_confirmed
				FROM @keyword_id_tbl

				SELECT
					[date], website, keyword, source 
				FROM view_all_rankingsdata
				WHERE [date] = @target_date AND
					website_id = @website_id AND
					keyword_id in (
						select keyword_id from @keyword_id_tbl
					)

	(SP 10) Variant Update

08 09 22
	(W) EMEA region

	(W) NA region

	(SP 5) Azure Devops Setup

	(SP 10) Variant Update

	SQL update
		(SP 2) Request 1:
			Request for update in ZOUND BUYBOX

			DB: 200
			Table: view_buybox
			Date: 2022-08-09
			Field for update: bb_price

			Reason: invalid -1 value

			# exec (https://prnt.sc/2ySj44v3ntGW)
				-- BUYBOX: UPDATE PRICE VALUE --
				declare @target_date date,
						@item_id varchar(255),
						@sku_id int,
						@price_value money;

				------------------------------------
				-- Set target date
				set @target_date = '2022-08-09' 

				-- Set target item_id
				set @item_id = '200ITL4070000000531080201903270618' 

				-- Set retailer_name
				set @price_value = 240
				------------------------------------

				select top 1 @sku_id = pg_id from rcms_product_skus
				where item_id = @item_id


				print char(13) + 'Updated ETL data:'
				update eb set eb.price = @price_value
				from etl_buyboxdata eb
				join etl_productdata ep on eb.etl_productdata_id = ep.id
				where ep.record_date = @target_date
				and ep.cms_product_sku_id = @sku_id

				print char(13) + 'Updated copy over data:'
				update etl_buyboxdata_override set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Updated backdoor data:'
				update backdoor_etl_buyboxdata set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Result:'
				select [date], item_id, price_value from view_buybox
				where [date] = @target_date
				and item_id = @item_id

		(SP 2) Request 2:

		(SP 3) Request 3:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-09
			Field for update: is_new

			Reason:, -2 values

	iteration
		* setup -> hellotv nl
		/ webshots trello [f]
		* variants 
		/ ppt development
f
08 10 22
	(W) EMEA region

	SQL update
		(SP D-2) Request 1:
			Request to set copy over in ZOUND
			DB: 200
			Table: View_3P_data and view_all_productdata
			Source Date: 2022-08-08
			Target DAte: 2022-08-09
			ItemID: 200FRMS070000001220960202010261317
			Reason: rerun failed, Spider being blocked

		(SP D-2) Request 2:
			Request for Removal in Razer
			DB: K00
			Table: productdata
			Date: 2022-08-10
			Retailer:

			www.scan.co.uk
			Reason: These are test date and should not be published in production. Compliance data have no separate publishing flag yet.

			# exec (https://prnt.sc/ypd5geXMBCix)
				# product > delete > prod_delete_all_by_date+retailer.sql

		(SP D-3) Request 3:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-10
			Field for update: is_new

			Reason:, -2 values

	Setup
		(F D-1) KEY-385	2022-08-08	2022-08-08	Sasmung CE (O00)	missing description in hellotv.nl	
			O00NL9313R020220801021641505732213
			O00NL9313R020220801021642553035213
			O00NL9313R020220801021642986631213
			O00NL9313R020220801021643734737213
			O00NL9313R020220801021642396722213
			O00NL9313R020220801021644587181213
			O00NL9313R020220801021641321388213
			12-aug-22	PRODUCTS-4017	
			https://prnt.sc/LTaOQArFYM04

			# SELECT * FROM view_all_productdata where date='2022-08-10' AND website = 'www.hellotv.nl'
			# fixed: added filtration

	(SP D-10) Variants update

	(D) Special Spider Development

08 13 22
	(W EMEA region

	SQL updates:
		(SP D-2) Request 1:
			Request to Set Deadlink Rankings Data in Nintendo Benelux Rankings
			DB: 110
			Table: view_all_rankingsdata
			Date: 08/13/2022
			Retailer:
			www.mediamarkt.nl
			Keywords:
			spelcomputer

			Screenshot:

			https://prnt.sc/xdAogAbZUX_D


			Reason: Fetching data even if it''s deadlink

			# exec (https://prnt.sc/qVRpfTGvUyTh)

		(SP D-2) Request 2: 
			Request to Set Deadlink Rankings Data in Nintendo Benelux Rankings
			DB: 110
			Table: view_all_rankingsdata
			Date: 08/13/2022
			Retailer:
			www.nedgame.nl
			Keywords:
			switch software
			Screenshot:
			https://prnt.sc/vklzijbLDvpj


			Reason: Fetching data even if it''s deadlink

			# exec (https://prnt.sc/vAhu1NR7eZKJ)

		(SP D-2) Request 2:
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110

			Table: view_all_listingsdata

			Date: 08/13/2022
			Retailer: www.fnac.be/fr
			category:
			Software
			Hardware

			Listings UUID(s):
			d1104ee1-3f4a-42a1-ad43-7b4b4aea5f45
			32c5b7d1-abc6-4185-a4c8-7b38155a0a26

			Screenshot:
			https://prnt.sc/MmppmiZUewzL
			https://prnt.sc/7QlbW_Kz4Zcv

			Reason: Fetching data even if it''s deadlink(Top level category/re direct url)

			# exec (https://prnt.sc/mG4XEPK6c6kJ, https://prnt.sc/xBCtDc9aGCdm)

		(SP D-2) Rquest 3:
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110

			Table: view_all_listingsdata

			Date: 08/13/2022

			Retailer: www.fnac.be/nl

			category:
			Software

			Listings UUID(s):
			d438865e-7705-45d2-ba93-16df3e3cb3e4

			Screenshot:
			https://prnt.sc/MlIrZW6g4b7f

			Reason: Fetching data even if it''s deadlink(re direct url)

			# exec (https://prnt.sc/IIgMG974HIyp)

		(SP D-3) Request 4:
			Request for Copy Over in SAMSUNG CE

			DB: O00
			Table: view_all_productdata (please include the webshots)
			Source Date: 2022-08-12
			Target Date: 2022-08-13
			Retailers:

			mediamarkt.be/nl/
			vandenborre.be/nl
			bcc.nl 
			bol.com
			coolblue.nl 
			electroworld.nl
			ep.nl
			expert.nl
			mediamarkt.nl
			wehkamp.nl

			Reason: Intermittent internet connection.

			# exec (https://prnt.sc/o0chfCG8uw8h)

			# exec (webshots copy over)
				# python webshot_copy_over.py -c O00 -s 2022-08-12 -t 2022-08-13

		(SP D-3) Request 5:
			Request for Copy Over in Samsung Mobile

			DB: F00
			Table: view_all_productdata (please include the webshots)
			Source Date: 2022-08-12
			Target Date: 2022-08-13
			Retailers:

			fnac.be/nl
			krefel.be/nl
			mediamarkt.be/nl/
			orange.be/nl/ 
			orange.lu/fr 
			post.lu/fr
			proximus.be/nl/ 
			samsung.com/be/
			selexion.be/nl
			tango.lu/fr 
			telenet.be/nl 
			vandenborre.be/nl
			bcc.nl
			belsimpel.nl
			bol.com
			coolblue.nl 
			kpn.com
			mediamarkt.nl
			mobiel.nl
			t-mobile.nl
			vodafone.nl

			Reason: Intermittent internet connection.

			# exec (https://prnt.sc/HW4FqTX2Cc31)

			# exec (webshots copy over)
				# python webshot_copy_over.py -c F00 -s 2022-08-12 -t 2022-08-13

	(W) 8633	2022-08-13	X00	webshot_url		
		SELECT * FROM view_all_productdata where
		date='2022-08-13' and webshot_url is NULL
		no webshot		
		Yes	Have

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-13' and webshot_url is NULL and source = 'etl' order by item_id

		# 5 item ids left

	(F D-1) (R D-1) 8634	2022-08-13	X00	price_value	sanborns.com.mx	
		'X00MXPS0OL000001327610202102090753',
		'X00MXPS0OL000001421380202105190424',
		'X00MXPS0SV020211207055750241942341',
		'X00MXPS0SV020211207055751019711341',
		'X00MXPS0OL000001421460202105190424',
		'X00MXPS0SV020220607022500251386158',
		'X00MXPS0SV020211207055752501344341',
		'X00MXPS0SV020211207055751751836341',
		'X00MXPS0SV020220607022500952915158',
		'X00MXPS0OL000001421530202105190424',
		'X00MXPS0OL000001421770202105190424',
		'X00MXPS0SV020211207055753242711341',
		'X00MXPS0PL000001461220202106300705',
		'X00MXPS0PL000001461450202106300705'
		wrong price fetched	
		https://prnt.sc/lg8kin4BDA-W	
		Yes	Have

		# SELECT * FROM view_all_productdata where date='2022-08-13' AND website = 'www.sanborns.com.mx'	AND item_id in (	'X00MXPS0OL000001327610202102090753',
		'X00MXPS0OL000001421380202105190424',
		'X00MXPS0SV020211207055750241942341',
		'X00MXPS0SV020211207055751019711341',
		'X00MXPS0OL000001421460202105190424',
		'X00MXPS0SV020220607022500251386158',
		'X00MXPS0SV020211207055752501344341',
		'X00MXPS0SV020211207055751751836341',
		'X00MXPS0SV020220607022500952915158',
		'X00MXPS0OL000001421530202105190424',
		'X00MXPS0OL000001421770202105190424',
		'X00MXPS0SV020211207055753242711341',
		'X00MXPS0PL000001461220202106300705',
		'X00MXPS0PL000001461450202106300705')

		{
		    "response": {
		        "task_uuid": "f0128147-3e9b-4709-ab38-31169191c7b0"
		    },
		    "version": "1.0"
		}

		# fixed: new object property source

		{
		    "response": {
		        "task_uuid": "e0982a91-8554-4c06-b212-3d690781e24b"
		    },
		    "version": "1.0"
		}

	(R D-1) 8635	2022-08-13	X00	all	liverpool.com.mx	
		'X00MXHC0OL000001475140202107060855',
		'X00MXHC0OL000001421290202105190424'
		auto copy over (site is up)		
		Yes	Have

		# SELECT * FROM view_all_productdata where date='2022-08-13' AND website = 'www.liverpool.com.mx'	AND item_id in ('X00MXHC0OL000001475140202107060855',
		'X00MXHC0OL000001421290202105190424')

		{
		    "response": {
		        "task_uuid": "601732bf-fbf3-4c28-a652-f3aedd3e06f8"
		    },
		    "version": "1.0"
		}

	(R D-1) 8636	2022-08-13	X00	all	gameplanet.com	
		'X00MXN90OL000001475150202107060855',
		'X00MXN90QL000001461270202106300705'
		auto copy over (site is up) and invalid deadlink		
		Yes	Have

		# SELECT * FROM view_all_productdata where date='2022-08-13' AND website = 'www.gameplanet.com'	AND item_id in ('X00MXN90OL000001475150202107060855',
		'X00MXN90QL000001461270202106300705')

		{
		    "response": {
		        "task_uuid": "2c39326a-14ef-4bde-b065-6602ec0b87a8"
		    },
		    "version": "1.0"
		}

08 14 22
	(W) EMEA region

	(W) NA region

	Fix Development
		(F D-1) (R D-1) Fix 1:
			/ hellotv_nl
				/ pagination

	SQL update
		(SP D-3) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-13
			Field for update: is_new

			Reason:, -2 values

08 15 22
	(W) EMEA region

	(W) 8690	2022-08-14	100	webshot_url		
		select * from view_all_productdata where date='2022-08-15'
		and webshot_url is NULL
		order by item_id
		Missing webshots		
		Yes	Keeshia

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-15' and webshot_url is NULL and source = 'etl' order by item_id

	SQL updates
		(SP D-2) Request 1:
			Request to Copy Over Garmin Listings

			DB: 100

			Table: view_all_listingsdata

			Source Date: 2022-8-14
			Target Date: 2022-8-15

			# exec (https://prnt.sc/fjYM9_SRWXrn) (listings > copy_over > list_copy_over_by_date.sql)

		(SP D-2) Request 2:
			Request to Delete Deadlink in Zound Listings
			Date: August 15, 2022
			Retailer:
			www.amazon.co.uk
			Category:
			Headphones Noise Cancelling

			Listings uuid:
			5514919e-e6cd-482d-b23a-c25f5cdb8f33
			Screenshot: https://prnt.sc/Vsmyejq6ELQN
			Reason: Double Source

			# exec (https://prnt.sc/CMvccW71E0nB) (listings > delete > list_delet_dead_linnk_by_date+postal+listings_uuid)

		(SP D-3) Request 3:
			Request to remove duplicate in Zound Listings
			DB:200
			Date: 08/14/2022
			Retailer:    
			www.amazon.co.uk
			Category:
			headphones noise cancelling

			Listings UUID:
			5514919e-e6cd-482d-b23a-c25f5cdb8f33
			Screenshot:
			https://prnt.sc/zUI7CpPIQVCy

			# exec (https://prnt.sc/hB3vrv-D778Q) (listings > delete > list_delete_dead_link_by_date+postal+listings_uuid.sql)
				# use deadlink as advised by sir Jon (https://prnt.sc/yibqbIs1hcsT)

			# upon checking the sql, there are 50 results and data that have duplicate values have ACO source: then delete listings with ACO source
			# exec (list_delete_copy_over_by_date+postal+listings_uuid.sql)

		(SP D-3) Request 4:
			Request for copy over in GARMIN
			DB: 100
			Table: productdata (DESCRIPTION & SPECS ONLY)
			Source Date: 2022-08-08
			Target Date: 2022-08-15

			Retailer: www.elgiganten.dk

			Item_ids:

			100DK10010000001397790202105040251
			100DK10010000001659910202111162228

			Reason: Missing description and Specs

			# execute 2 copy_over queries for description and specification
				# exec (https://prnt.sc/Vd24-4AFNnCi) (prod_copy_over_description_by_date+item_id.sql)
				# exec (https://prnt.sc/dl4ZqurA7BCR) (prod_copy_over_specification_by_date+item_id.sql)

	(SP D-10) Variants update

08 16 22
	/ backup all envs and test
		/ ppt-rankings get test.js
		/ ppt-listings get test.js

	(W) EMEA region

	(W) 8737	2022-08-16	200	webshot_url		
		SELECT * FROM view_all_productdata where date='2022-08-16' and webshot_url is NULL 	Missing Webshots		
		Yes	Mojo

	(W) 8762	2022-08-16	H10	webshot_url		
		select * from view_all_productdata where date='2022-08-16' 
		and webshot_url is NULL
		order by item_id
		Missing Webshots. Request Interval Rerun.	
			Yes	Keeshia

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-16' and webshot_url is NULL and source = 'etl' order by item_id

	SQL updates
		(SP D-3) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-15
			Field for update: is_new

			Reason:, -2 values

08 17 22
	/ iteration
		/ setup and test

	SQL updates
		(SP D-3) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-16
			Field for update: is_new

			Reason:, -2 values

			# (3p > 3p_update_is_new.sql) 

		(SP D-3) Request 2:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2022-08-17
			Field for update: is_new

			Reason:, -2 values

	(R D-2) Trello 1:
		# https://trello.com/c/35DFYOBE/4142-garmin-compliance-missing-description-3-value-in-wwwelgigantendk
		# [GARMIN] Compliance - Missing description ; -3 value in www.elgiganten.dk
		Issue: Please fix spyder to fetch the description found on pdp

		date: 2022-08-15
		retailer: www.elgiganten.dk
		item_ids: 
		100DK10010000001397790202105040251
		100DK10010000001659910202111162228

		# exec

		# SELECT * FROM view_all_productdata where date='2022-08-17' AND website = 'www.elgiganten.dk'	AND item_id in ('100DK10010000001397790202105040251','100DK10010000001659910202111162228')

		{
		    "response": {
		        "task_uuid": "c4927230-bede-4920-9b56-b443acc3821e"
		    },
		    "version": "1.0"
		}

		# fixed with rerun

	(R D-2) Trello 2:
		# https://trello.com/c/4k9tgdEk/4138-nintendo-brazil-rankings-auto-copy-over-for-1-day-in-wwwkabumcombr
		# [Nintendo Brazil] Rankings: Auto Copy Over for 1 day in www.kabum.com.br
		Retailer:
		www.kabum.com.br

		Keyword:
		console switch

		Issue:
		Auto Copy over for 1 day

		Screenshot:
		https://prnt.sc/D_hQfpuKHNY_

		as per be:
		max rerun attempts even after implementing cloudscraper

		request to implement unblocker

		# SELECT * FROM view_all_rankingsdata var where date='2022-08-14' AND website = 'www.kabum.com.br' AND keyword in (N'console switch')

		# Investigate: Rene Matt, implemented unblocker

	(R D-3) Trello 3:
		# https://trello.com/c/mh5uavsO/4137-nintendo-benelux-rankings-data-count-mismatch-in-wwwfnacbe-nl
		# [NINTENDO BENELUX] Rankings: Data Count Mismatch in www.fnac.be/nl
		Date: August 13, 2022

		Issue description: blocked, Multiple reruns failed

		Data Count Mismatch 16(Fetched Data is Lesser than expected)

		Retailer: www.fnac.be/nl

		Keyword:
		Switch software

		Screenshot:
		https://prnt.sc/w1EfUncFfZom

		# SELECT * FROM view_all_rankingsdata var WHERE date = '2022-08-13' AND website = 'www.fnac.be/nl' AND keyword in ('Switch software')

		# investigate: valid product count as parallel to the items on site

	Trello 4:
		# https://trello.com/c/tPNeErhY/4145-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-fr-fr
		# [MS PRICE] Comparisons: Incorrect data for retailer microsoft.com/fr-fr
		Date: August 17, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/fr-fr
		Sample item Id''s:
		Incorrect data
		https://prnt.sc/SWfChX7lb-Jy

		A00FRC40RB000001237590202011060526
		A00FRC40KT020220808041345110364220
		A00FRC40KT020220808042452655748220

		# 
08 19 22
	* iteration
		* microsoft.com/fr-fr
		* target webshot investigate

	(W) EMEA region 

	(W) 8866	2022-08-19	610	webshot_url		
		select * from view_all_productdata where date='2022-08-19'and webshot_url is NULL order by item_id
		Missing Webshots		Yes	Keeshia

	(W) 8868	2022-08-19	Q00	all		
		SELECT * FROM view_all_productdata where date='2022-08-19' and webshot_url is NULL order by item_id	Missing webshots (Premium Day)		
		Yes	Mark

	(W) 8887	2022-08-19	Z00	all	
		All	SELECT * FROM view_all_productdata where date='2022-08-19' and webshot_url is NULL order by item_id	Missing webshots (premium day)		
		Yes	Mark

	---

	SQL updates:
		(SP D-2) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-18
			Field for update: is_new

			Reason:, -2 values

	---

	trello (ccontinue this) Trello 1:
		# https://trello.com/c/tPNeErhY/4145-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-fr-fr
		# [MS PRICE] Comparisons: Incorrect data for retailer microsoft.com/fr-fr
		Date: August 17, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/fr-fr 
		Sample item Id''s:
		Incorrect data
		https://prnt.sc/SWfChX7lb-Jy

		A00FRC40RB000001237590202011060526
		A00FRC40KT020220808041345110364220
		A00FRC40KT020220808042452655748220
		A00FRC40KT020220808042452797666220
		A00FRC40KT020220808042452945079220

		# 

	---

	(F D-1) (R D-1) 8884	2022-08-19	Q00	description	friluftslageret.dk	All item_ids	
		Incorrect price fetched -1
		Yes	Mark

		# SELECT * FROM view_all_productdata where date='2022-08-19' AND website = 'www.friluftslageret.dk'

		{
		    "response": {
		        "task_uuid": "f72a687a-b251-4a1d-bdc2-552f5a380bed"
		    },
		    "version": "1.0"
		}

		# for rebuild

		{
		    "response": {
		        "task_uuid": "5b8c3a3d-c204-4515-bb3c-b864429dc427"
		    },
		    "version": "1.0"
		}

	(F D-1) (R D-1) (R D-1) 8874	2022-08-19	200	price_value	heureka.cz	
		200CZ9D080000000804600201907080640
		200CZ9D080000000550480201904050601
		200CZ9D070000000804640201907080646
		200CZ9D080000000804630201907080644
		200CZ9D080000000804610201907080642
		200CZ9D070000000804260201907080347
		200CZ9D070000000804350201907080416
		200CZ9D070000000550290201904050558
		200CZ9D070000000551040201904050624
		200CZ9D080000000552060201904050701
		200CZ9D080000000804490201907080629
		200CZ9D080000000804570201907080636
		200CZ9D080000000804580201907080637
		Price not fetch		
		Yes	Mojo

		# SELECT * FROM view_all_productdata where date='2022-08-19' AND website = 'www.heureka.cz'	AND item_id in ('200CZ9D080000000804600201907080640',
		'200CZ9D080000000550480201904050601',
		'200CZ9D070000000804640201907080646',
		'200CZ9D080000000804630201907080644',
		'200CZ9D080000000804610201907080642',
		'200CZ9D070000000804260201907080347',
		'200CZ9D070000000804350201907080416',
		'200CZ9D070000000550290201904050558',
		'200CZ9D070000000551040201904050624',
		'200CZ9D080000000552060201904050701',
		'200CZ9D080000000804490201907080629',
		'200CZ9D080000000804570201907080636',
		'200CZ9D080000000804580201907080637')

		{
		    "response": {
		        "task_uuid": "01769d19-a651-4263-8665-b4ae95c4716d"
		    },
		    "version": "1.0"
		}

		# for rebuild

		{
		    "response": {
		        "task_uuid": "796d1264-d768-43ac-93c0-b2aeaabb14ae"
		    },
		    "version": "1.0"
		}

	(R D-2) 4919	2022-08-19	New CMS	200	all	www.saturn.de	
		anc kopfhörer
		bluetooth in ear kopfhörer
		bluetooth kopfhörer
		kopfhörer
		mobile lautsprecher
		multiroom lautsprecher

		Duplicate Data	
		https://prnt.sc/j7VIMy-YOq0Y	
		yes	renz

		# select date, website, keyword, product_website, product_url, ritem_id, count(1) from view_all_rankingsdata var WHERE date = '2022-08-19'
			GROUP BY date, website, keyword, product_website, product_url, ritem_id 
			HAVING count(1)>1

		{
		    "response": {
		        "task_uuid": "a41c139d-e5f8-41c6-98e1-1001a9865db9"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "742b022f-d0d9-463a-9574-aaa7683d38b0"
		    },
		    "version": "1.0"
		}

	trello (R D-1) 4920	2022-08-19	New CMS	200	all	www.zalando.de	
		bluetooth kopfhörer
		kabellose kopfhörer
		kopfhörer bluetooth

		Duplicate Data	
		https://prnt.sc/ZuMAjMGp2kQx	
		yes	renz

		# select date, website, keyword, product_website, product_url, ritem_id, count(1) from view_all_rankingsdata var WHERE date = '2022-08-19'
			GROUP BY date, website, keyword, product_website, product_url, ritem_id 
			HAVING count(1)>1

		{
		    "response": {
		        "task_uuid": "7c3ef361-ce78-4799-903a-98a81dd82409"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "8438bf1b-0e22-4110-a02f-3896bd2fdfc3"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "3220f488-709b-4713-9b1d-8f2af3330260"
		    },
		    "version": "1.0"
		}

	---

	SQL update
		(SP D-3) Request 1:
			Request for copy over in SONOS

			DB: Z00
			Table: productdata (DESCRIPTION, SPECS and IMAGE COUNT ONLY)
			Source Date: 2022-08-12
			Target Date: 2022-08-19

			Retailer: www.bhphotovideo.com
			Item_ids:

			Z00USC10O0000001362830202103310655
			Reason: Missing description, Specs and image count

08 20 22
	(W) EMEA region

	(F D-3) (R D-3) Trello 1:
		# https://trello.com/c/1ilt5cbA/4153-zound-rankings-duplicate-data-in-wwwzalandode
		# [ Zound Rankings ] Duplicate Data in www.zalando.de
		Retailer:
		www.zalando.de

		Date: August 19, 2022

		Keywords:
		bluetooth kopfhörer
		kabellose kopfhörer
		kopfhörer bluetooth

		Screenshot:
		Screenshot

		Issue: Duplicate Data

		# select date, website, keyword, product_website, product_url, ritem_id, count(1) from view_all_rankingsdata var WHERE date = '2022-08-19'
			GROUP BY date, website, keyword, product_website, product_url, ritem_id 
			HAVING count(1)>1

		# fixed: sponsored and duplicate

	(F D-1) Trello 2:
		# https://trello.com/c/fdOJKnov/4157-haglofs-compliance-incomplete-description-in-wwwfjellsportno
		# [HAGLOFS] Compliance - Incomplete description in www.fjellsport.no
		date: 2022-08-19
		retailer: www.fjellsport.no
		item_ids: All item id

		# SELECT * FROM view_all_productdata where date='2022-08-19' AND website = 'www.fjellsport.no' order by price_value 

		# fixed: description

	(F D-1) Trello 3:
		# https://trello.com/c/zXXFqmUJ/4156-samsung-mobile-comparisons-all-itemids-auto-copy-over-on-belsimpelnl
		# [SAMSUNG MOBILE] Comparisons: all item_ids auto copy over on belsimpel.nl
		Date: August 19, 2022
		Company: Samsung Mobile (F00)
		Retailer: belsimpel.nl
		Issue: all item_ids auto copy over (site is up)

		# SELECT TOP 5 * FROM view_all_productdata where date='2022-08-20' AND website = 'www.belsimpel.nl'

		# fixed: async approach

	(R D-2) Trello 4:
		# https://trello.com/c/3kQH0MmA/4155-sonos-compliance-missing-imagecount-description-and-specs-in-wwwbhphotovideocom
		# [SONOS] Compliance - Missing image_count, description and specs in www.bhphotovideo.com
		Issue: please fix the spyder to fetch the image count, description and specs found on pdp

		Note: As of 2022-08-19, data has been copied over

		date: 2022-08-19
		retailer: www.bhphotovideo.com
		item_ids:
		Z00USC10O0000001362830202103310655

		# SELECT * FROM view_all_productdata where date='2022-08-20' AND website = 'www.bhphotovideo.com' AND item_id IN ('Z00USC10O0000001362830202103310655')

		# can be fixed with rerun

	(R D-2) Trello 5:
		# https://trello.com/c/xv7dG2Wp/4154-sonos-rankings-auto-copy-over-for-1-day-in-wwwwalmartcom
		# [ SONOS rankings ] Auto Copy Over for 1 day in www.walmart.com`
		Retailer:
		www.walmart.com

		Date: August 19, 2022

		Keyword:
		amp

		Screenshot:
		Screenshot

		Issue: Auto Copy Over for 1 day

		# has data and tested with local. Can be fixed with rerun

	(F D-2) Trello 6:
		# https://trello.com/c/CLIBu4yd/4151-haglofs-rankings-data-count-mismatch-in-wwwxxlno
		# [HAGLOFS] Rankings: Data Count Mismatch in www.xxl.no
		Date: August 19, 2022

		Issue description: Need further investigation

		Issue Details: Data Count Mismatch 9(Fetched Data is Greater than expected)

		Retailer: www.xxl.no

		Keyword:
		Lettvektsjakke menn

		Screenshot:
		https://prnt.sc/6vx-CZi1zovr

		# fixed: api pre requisites for recommended products

	SQL Update
		(SP D-2) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-20

			Field for update: is_new

			Reason:, -2 values

			# exec (3p > 3p_update_is_new.sql)

		(SP D-2) Request 2:
			DB: 200
			Table: view_buybox
			Date: 2022-08-20
			Field for update: bb_price

			Reason: incorrect value

			# exec (buybox > buybox_update_price_by_date+item_id.sql)
				-- BUYBOX: UPDATE PRICE VALUE --
				declare @target_date date,
						@item_id varchar(255),
						@sku_id int,
						@price_value money;

				------------------------------------
				-- Set target date
				set @target_date = '2022-08-01' 

				-- Set target item_id
				set @item_id = '200DEY007R020220524073425969025144' 

				-- Set retailer_name
				set @price_value = 45.99
				------------------------------------

				select top 1 @sku_id = pg_id from rcms_product_skus
				where item_id = @item_id


				print char(13) + 'Updated ETL data:'
				update eb set eb.price = @price_value
				from etl_buyboxdata eb
				join etl_productdata ep on eb.etl_productdata_id = ep.id
				where ep.record_date = @target_date
				and ep.cms_product_sku_id = @sku_id

				print char(13) + 'Updated copy over data:'
				update etl_buyboxdata_override set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Updated backdoor data:'
				update backdoor_etl_buyboxdata set price = @price_value
				where record_date = @target_date
				and cms_product_sku_id = @sku_id

				print char(13) + 'Result:'
				select [date], item_id, price_value from view_buybox
				where [date] = @target_date
				and item_id = @item_id

08 21 22
	(R D-2) Trello 1:
		# https://trello.com/c/a6CAR5HL/4143-zurn-rankings-duplicate-data-in-wwwhomedepotcom
		# [Zurn] Rankings : Duplicate data in www.homedepot.com
		Issue : Duplicate data

		Cause : Proxy Issue

		Retailer : www.homedepot.com

		Keyword : metering faucet

		Screenshot:
		https://prnt.sc/QxI-COxVyxzV

		# SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.homedepot.com' and 
				var2.keyword in (N'metering faucet') and 
				var2.[date] = '2022-08-19' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

		{
		    "response": {
		        "task_uuid": "d972cac4-dae6-4eb0-b475-382101d39222"
		    },
		    "version": "1.0"
		}

	(F D-2) (SP D-10) Trello 2 (wth variants update):
		# https://trello.com/c/tPNeErhY/4145-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-fr-fr
		# [MS PRICE] Comparisons: Incorrect data for retailer microsoft.com/fr-fr
		Date: August 17, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/fr-fr
		Sample item Id''s:
		Incorrect data
		https://prnt.sc/SWfChX7lb-Jy

		# A00FRC40KT020220808042452382961220
		{
		    "response": {
		        "task_uuid": "644ed236-4edc-4747-8784-cd49f120e2a4"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "5c6dcb9e-52ab-42d3-a4d1-d904134c2e11"
		    },
		    "version": "1.0"
		}

	(R D-2) 8970	2022-08-21	G10	description	amazon.com	
		'G10USW0008120220103042125895753003',
		'G10USW0008120220103042126453493003',
		'G10USW000A220220401082352159991091',
		'G10USW000A220220401082352305958091'
		Incomplete descriptions	
		https://prnt.sc/JC6nU8MJmu6i
		https://prnt.sc/wDBXk-IYDf8H
		Yes	Neil

		# SELECT * FROM view_all_productdata where date='2022-08-21' AND website = 'www.amazon.com'	AND item_id in ('G10USW0008120220103042125895753003',
		'G10USW0008120220103042126453493003',
		'G10USW000A220220401082352159991091',
		'G10USW000A220220401082352305958091')

		{
		    "response": {
		        "task_uuid": "f20e14b4-83b5-4881-9a1f-a1bd6e54d476"
		    },
		    "version": "1.0"
		}

	(R D-2) 8971	2022-08-21	R10	image_count	lowes.com	
		R10US8X0MC220220727035441849812208	
		Missing image_count		
		Yes	Neil

		# SELECT * from view_all_productdata vap WHERE website = 'www.lowes.com' AND date = '2022-08-21' AND item_id in ('R10US8X0MC220220727035441849812208')
	
		{
		    "response": {
		        "task_uuid": "2fad021d-3051-4624-923c-967e32522ae9"
		    },
		    "version": "1.0"
		}
		{
		    "response": {
		        "task_uuid": "9d9a349c-8e80-44d3-987c-8be1f143d432"
		    },
		    "version": "1.0"
		}

	SQL updates
		(SP D-2) Request 1:
			Request to copy over in ZOUND

			DB: 200

			Table: View_3P_data and view_all_productdata

			Source Date: 2022-08-20

			Target DAte: 2022-08-21

			ItemID: 200ITGF070000000557480201904220941

			# exec (3p > 3p_copy_over_by_date+item_id.sql and product > prod_copy_over_by_date+item_id.sql)

		(SP D-2) Request 2:
			Request for update in ZOUND BUYBOX
			DB: 200
			Table: view_buybox
			Date: 2022-08-21
			Field for update: bb_price

			Reason: incorrect value

			# exec (buybox > buybox_update_price_by_date+item_id.sql)

		(SP D-2) Request 3:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2022-08-21

			Field for update: is_new

			Reason:, -2 values

			# exec (3p > 3p_update_is_new.sql)

08 22 22
	(W) EMEA region

	(W) 8975	2022-08-22	100	webshot_url		
		select * from view_all_productdata where date='2022-08-22' and webshot_url is NULL
		Missing Webshots		
		Yes	Keeshia

	(F D-2) (R D-1) Trello 1:
		# https://trello.com/c/vTtggkzj/4113-zurn-rankings-data-count-mismatch-in-wwwlowescom
		# [ZURN ] : RANKINGS -Data Count Mismatch in www.lowes.com
		Issue Details: Data Count Mismatch 2(Fetched Data is Greater than expected)

		cause : Need Further investigation

		Date: 08/02/2022

		Retailer: www.lowes.com

		Keyword(s):
		water pressure regulator

		Screenshot(s):
		# https://prnt.sc/KMHvmd4XW6tv

		# fixed but needs to be tested on local. Postal code needed

		{
		    "response": {
		        "task_uuid": "3041477d-6761-4ddb-8c36-20fd3dc78b71"
		    },
		    "version": "1.0"
		}

	(F D-1) Trello 2:
		Date: August 17, 2022
		Company: MS Price (A00)
		Retailer: amazon.ca
		Issue: Incorrect data (Wrong price and stocks)

		Sample item Id''s:

		A00CA540RB000000868800201911050420
		A00CA540KT020211025061822298432298
		A00CA540RB000000869410201911060234
		A00CA540RB000000868660201911050412

		# SELECT * FROM view_all_productdata where date='2022-08-21' AND website = 'www.amazon.ca' AND item_id in ('A00CA540RB000000868800201911050420',
		'A00CA540KT020211025061822298432298',
		'A00CA540RB000000869410201911060234',
		'A00CA540RB000000868660201911050412')

		# fixed: selector order and added new selector

		{
		    "response": {
		        "task_uuid": "cc0fa2fb-fdca-4c68-846e-519971375c10"
		    },
		    "version": "1.0"
		}

	(R D-2) Trello 3:
		# https://trello.com/c/BxiDAOtd/4161-g10-manufacturing-demolistings-invalid-product-url-scrape-in-wwwzalandode
		# [G10 MANUFACTURING DEMO]Listings: Invalid Product URL scrape in www.zalando.de
		Date- 08-21-22
		Reatiler-www.zalando.de
		Category-Men''s Polo Shirts
		Issue Description-Invalid Product URL scrape
		Screenshot - https://prnt.sc/jcU3Pu2MvDN6

		# SELECT * FROM view_all_listings_data WHERE date = '2022-08-22' AND website = 'www.zalando.de' AND category in ('Men''s Polo Shirts')

		# fixed with today's run

	DCS-2628
		(R D-1) Request 1 - Microsoft.com/fr-fr:
			Request to rerun ACO data in microsoft.com/fr-fr
			A00FRC40GC000000288270201903040955
			A00FRC40RB000000875980201911070758
			A00FRC40KT020220411152046665296101
			A00FRC40KT020220717222331903481198

			# SELECT * FROM view_all_productdata where date='2022-08-22' AND item_id in ('A00FRC40GC000000288270201903040955',
				'A00FRC40RB000000875980201911070758',
				'A00FRC40KT020220411152046665296101',
				'A00FRC40KT020220717222331903481198')

			{
			    "response": {
			        "task_uuid": "26142e52-2f1a-470d-aaa8-dacef094ef73"
			    },
			    "version": "1.0"
			}

		(R D-2) Request 2 - Microsoft.com/fr-fr:
			Update for microsoft.com/fr-fr as of August 22, 2022:

			https://trello.com/c/tPNeErhY/4145-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-fr-fr

			Incorrect stocks status:
			https://prnt.sc/ZDbipanVeB-k
			A00FRC40RB000001237590202011060526

			Wrong price:
			https://prnt.sc/4ffxARiSRhQC
			A00FRC40KT020220808041345110364220
			A00FRC40KT020220808042452655748220
			A00FRC40KT020220808042452797666220
			A00FRC40KT020220808042452945079220
			A00FRC40KT020220808042453087604220
			A00FRC40KT020220808042453221013220
			A00FRC40KT020220808042453341675220
			A00FRC40KT020220808042453482461220
			A00FRC40KT020220808041345205728220
			A00FRC40KT020220808042453630189220
			A00FRC40KT020220808042453761128220
			A00FRC40KT020220808042453894916220
			A00FRC40KT020220808042454020154220
			A00FRC40KT020220808041345268770220
			A00FRC40KT020220808042454176306220
			A00FRC40KT020220808042454298861220
			A00FRC40KT020220808042452261092220
			A00FRC40KT020220808041344909769220
			A00FRC40KT020220808042452382961220
			A00FRC40KT020220808042452514836220
			A00FRC40KT020220808041345372078220
			A00FRC40KT020220808041345466051220
			A00FRC40KT020220808041345562610220
			A00FRC40KT020220808041345650221220
			A00FRC40KT020220808041345754601220
			A00FRC40KT020220808041345826987220
			A00FRC40KT020220808041346025570220
			A00FRC40KT020220808041345922227220
			A00FRC40KT020220808042454433711220
			A00FRC40KT020220808042454581311220
			A00FRC40KT020220808041344999449220
			A00FRC40RB000001345260202103100635
			A00FRC40RB000001345270202103100635

			4 ACO items: fixed by rerun

			# test: A00FRC40KT020220808041345562610220
			{
			    "response": {
			        "task_uuid": "6737a869-9bda-4665-89d4-1f4fa3acedb6"
			    },
			    "version": "1.0"
			}

	SQL update:
		(SP D-2) Request 1:
			Request to Copy Over Garmin Listings

			DB: 100
			Table: view_all_listingsdata

			Source Date: 2022-8-21
			Target Date: 2022-8-22

			# exec (listings > list_copy_over_by_date)

		(SP D-1) Request 2:	
			Request to Set Deadlink Rankings Data in Garmin
			DB: 100
			Table: view_all_rankingsdata
			Date: 08/22/2022
			Retailer:
			www.fjellsport.no

			Keywords:
			HUNDEPEILER
			Screenshot:
			https://prnt.sc/IgDV0WPd84Do
			Reason:
			Fetching data even if it''s deadlink (PDP).

			# exec (rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer.sql)

		(SP D-1) Request 3:
			Request for update in ZOUND BUYBOX
			DB: 200
			Table: view_buybox
			Date: 2022-08-22
			Field for update: bb_price

			Reason: invalid -1 value

			# exec (buybox > buybox_update_price_by_date+item_id.sql)

		(SP D-2) Request 4:
			Request for copy over in GARMIN

			DB: 100
			Table: productdata (DESCRIPTION ONLY)
			Source Date: 2022-08-15
			Target Date: 2022-08-22

			Retailer: www.milrab.fi

			Item_ids:

			100FIII010000000838550201908290701
			100FIII010000000880720201911130949
			100FIII010000000880770201911130952
			100FIII010000000880810201911130958
			100FIII010000000882230201911140212
			100FIII010000000883410201911140357
			100FIII010000001646280202111090740
			100FIII010000001646300202111090740
			100FIII010000001646310202111090740
			100FIII010000001646360202111090740
			100FIII010000001646370202111090740
			100FIII010000001646380202111090740
			100FIII010000001646390202111090740
			100FIII010000001646400202111090740
			100FIII010000001646420202111090740
			100FIII010000001646440202111090740
			100FIII010000001646480202111090741
			100FIII010000001646490202111090741
			100FIII010000001648090202111090811
			100FIII010000001648120202111090811
			100FIII010000001648140202111090811
			100FIII010000001648160202111090811
			100FIII010000001648170202111090811
			100FIII010000001648190202111090811
			100FIII010000001648220202111090811
			100FIII010000001648240202111090811
			100FIII010000001648260202111090811
			100FIII010000001648280202111090811
			100FIII010000001648310202111090811
			100FIII010000001648320202111090811
			100FIII010000001648330202111090811
			100FIII010000001648370202111090811
			100FIII010000001648390202111090811
			100FIII010000001648400202111090811
			100FIII010000001648410202111090811
			100FIII010000001648420202111090811
			100FIII010000001648430202111090811
			100FIII010000001648440202111090811
			100FIII010000001648450202111090811
			100FIII010000001648460202111090811
			100FIII010000001648480202111090811
			100FIII010000001648500202111090811
			100FIII010000001648540202111090811
			100FIII010000001648560202111090811
			100FIII010000001648590202111090811
			100FIII010000001648610202111090811
			100FIII010000001648620202111090811
			100FIII010000001648640202111090811
			100FIII0ZQ020220103074423557358003
			100FIII0ZQ020220103074424002461003
			100FIII0ZQ020220103074424620375003
			100FIII0ZQ020220124093134349440024
			100FIII0ZQ020220124093134891225024
			100FIII0ZQ020220124093136096216024
			100FIII0ZQ020220124093136675519024
			100FIII0ZQ020220124093137316507024
			100FIII0ZQ020220124093138657386024
			100FIII0ZQ020220124093139213001024
			100FIII0ZQ020220124093139650019024
			100FIII0ZQ020220124093141211834024
			100FIII0ZQ020220124093142545357024
			100FIII0ZQ020220124093143005327024
			100FIII0ZQ020220124093143494202024
			100FIII0ZQ020220207085519139304038
			100FIII0ZQ020220207085519689584038
			100FIII0ZQ020220214062203060112045
			100FIII0ZQ020220214062214976664045
			100FIII0ZQ020220214062217020562045
			100FIII0ZQ020220214062217728363045
			100FIII0ZQ020220214062218841409045
			100FIII0ZQ020220214062219530249045
			100FIII0ZQ020220214062220009052045
			100FIII0ZQ020220214062220924299045
			100FIII0ZQ020220214062221758287045
			100FIII0ZQ020220214062222478202045
			100FIII0ZQ020220214062222967594045
			100FIII0ZQ020220411044003348022101
			100FIII0ZQ020220411044003949677101
			100FIII0ZQ020220411044004442851101



			Reason: Incomplete descriptions



			Thank you.

08 23 22
	# good afternoon everyone. so, to start
	(SP D-3) BE stand-up Reporting:
		newly developed Microsoft retailers:
			# we have issues microsoft.com/fr-fr
			# mostly caused by incorrect variants and was resolved through variants update and some minor code updates
			# for the new ticket under DCS-2628, ongoing ang investigation and fix

		Setup Buglist:
			# we have successfully catered the tickets for for the past days this month
			# and so far today, we have Invalid deadlink issue for hellotv.nl under sa rankings na metrics
			# fix ongoing

		Webshots:
			# we have catered all webshots request with decent success rate and of course, 
			# MSPrice is always the priority especially when there are companies na nag parequest with webshot

		SQL updates:
			# so far, no tickets left behind. As long as there are request, we cater it right away

		Trello:
			# I would like to thank BE members who made a great contribution for clearing sa trello tickets, I hope we consistently do this
			### reminders ko lang for those members na nagpapa trello, please resolve it even if kaya or hinde. Anyways, we could ask help from the brilliant minds of BE
			### its actually a good practice to resolve issues out of our comfort zone, it helps us learn a lot

		# thats all from the 3rd team, thank you and good afternoon

	(W) EMEA region

	(W) 9032	2022-08-23	200	webshot_url		
		SELECT count (*)  FROM view_all_productdata where date='2022-08-23' and webshot_url is NULL 	
		Missing Webshots		
		Yes	Mojo

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-23' and webshot_url is NULL and source = 'etl' order by item_id

	(W) 9073	2022-08-23	200	webshot_url	target.com	all items	
		no webshots		
		Yes	Louie

	(W) 9077	2022-08-23	200	webshot_url	amazon.com	
		SELECT * FROM view_all_productdata where date='2022-08-23' and website='www.amazon.com' and webshot_url is NULL 
			Yes	Louie

	(W) 9074	2022-08-23	200	webshot_url	marshallheadphones.com/us/en	
		200USCV070000001436770202106070746
		200USCV07R020220627031432354025178
		200USCV07R020220627031433145896178
		200USCV070000001436830202106070746
		200USCV070000001436860202106070746
		200USCV070000001436920202106070746
		200USCV070000001437130202106070746
		200USCV070000001437160202106070746
		200USCV070000001560500202108230658
		200USCV070000001437340202106070746
		200USCV070000001437400202106070746
		200USCV07R020220627031433576940178
		200USCV070000001437430202106070746
		200USCV070000001437460202106070746
		200USCV070000001437520202106070746
		200USCV07R020220627031434221949178
		no webshots		
		Yes	Louie

	DCS-2628
		Experimental 1 (microsoft.com/fr-fr):
			{
			    "response": {
			        "task_uuid": "d0fdc558-03f5-493c-a601-b91887963bd3"
			    },
			    "version": "1.0"
			}

			# A00FRC40KT020220808041345466051220
			{
			    "response": {
			        "task_uuid": "c5ef37d2-97df-4064-8677-d4efa21701b9"
			    },
			    "version": "1.0"
			}

		(R D-5) Request 1:
			Hello, update for microsoft.com/fr-fr as of August 23, 2022

			https://trello.com/c/tPNeErhY/4145-ms-price-comparisons-incorrect-data-for-retailer-microsoftcom-fr-fr

			Wrong price
			https://prnt.sc/uVJ4VMA2SeKt

			A00FRC40KT020220808041345110364220
			A00FRC40KT020220808042453087604220
			A00FRC40KT020220808041345205728220
			A00FRC40KT020220808042453630189220
			A00FRC40KT020220808042454020154220
			A00FRC40KT020220808041345268770220
			A00FRC40KT020220808041345268770220
			A00FRC40KT020220808042454298861220
			A00FRC40KT020220808041344999449220
			A00FRC40RB000001345270202103100635

			https://prnt.sc/KhiGZV-gnKEA
			Incorrect data
			A00FRC40KT020220808042452261092220
			A00FRC40KT020220808041344909769220
			A00FRC40KT020220808041344909769220
			A00FRC40KT020220808041345466051220
			A00FRC40KT020220808041345754601220
			A00FRC40KT020220808042454433711220
			A00FRC40KT020220808042454581311220
			A00FRC40KT020220808041344999449220

			# with variant update

			# -- microsoft.com/fr-fr; DCS-2628 issue;  08-23-2022
			SELECT * FROM view_all_productdata where date='2022-08-23' AND website = 'www.microsoft.com/fr-fr' AND item_id in ('A00FRC40KT020220808041345110364220',
			'A00FRC40KT020220808042453087604220',
			'A00FRC40KT020220808041345205728220',
			'A00FRC40KT020220808042453630189220',
			'A00FRC40KT020220808042454020154220',
			'A00FRC40KT020220808041345268770220',
			'A00FRC40KT020220808041345268770220',
			'A00FRC40KT020220808042454298861220',
			'A00FRC40KT020220808041344999449220',
			'A00FRC40RB000001345270202103100635',
			'A00FRC40KT020220808042452261092220',
			'A00FRC40KT020220808041344909769220',
			'A00FRC40KT020220808041344909769220',
			'A00FRC40KT020220808041345466051220',
			'A00FRC40KT020220808041345754601220',
			'A00FRC40KT020220808042454433711220',
			'A00FRC40KT020220808042454581311220',
			'A00FRC40KT020220808041344999449220'
			)

			select * from rcms_product_skus where item_id in (
			    'A00FRC40KT020220808042453087604220',
			    'A00FRC40KT020220808042453630189220',
			    'A00FRC40KT020220808042454020154220',
			    'A00FRC40KT020220808042454298861220',
			    'A00FRC40KT020220808041344999449220',
			    'A00FRC40KT020220808041345110364220',
			    'A00FRC40KT020220808041345205728220',
			    'A00FRC40KT020220808041345268770220',
			    'A00FRC40KT020220808042454433711220',
			    'A00FRC40KT020220808042454581311220',
			    'A00FRC40KT020220808041345466051220',
			    'A00FRC40KT020220808041345754601220',
			    'A00FRC40KT020220808042452261092220',
			    'A00FRC40RB000001345270202103100635',
			    'A00FRC40KT020220808041344909769220'
			)

	(R D-2) Trello 1:
		Issue Details: Data Count Mismatch 2(Fetched Data is Greater than expected)

		cause : Need Further investigation

		Date: 08 /02/2022

		Retailer: www.lowes.com

		Keyword(s):
		water pressure regulator

		# SELECT * FROM view_all_rankingsdata var where date='2022-08-22' AND website = 'www.lowes.com' AND keyword in (N'water pressure regulator')

		# Issue: Product result varies from geographical location
		Action: Use proxy with corresponding country for this retailer
		Screenshot: https://prnt.sc/IgyMo0eFOoN1

	(R D-2) Trello 2:
		# https://trello.com/c/gK4S1kt8/3945-samsung-mobile-compliance-missing-specification-in-wwwbasebe-nl
		# [SAMSUNG MOBILE] Compliance - Missing specification in www.base.be/nl/
		Issue: Please fix spyder to fetch the specification found on pdp.

		date: 2022-06-16
		retailer: www.base.be/nl/
		item_ids:
		F00BEQX0KB020211207020301540066341
		F00BEQX0KB020211207020306838874341
		F00BEQX05R020220103055430233811003
		F00BEQX05R020220214072844451069045
		F00BEQX05R020220214072858571599045
		F00BEQX05R020220214072901310139045
		F00BEQX05R020220214072903928713045
		F00BEQX0KB020220412082146539566102

		# SELECT * FROM view_all_productdata where date='2022-08-18' AND item_id in ('F00BEQX0KB020211207020301540066341',
		'F00BEQX0KB020211207020306838874341',
		'F00BEQX05R020220103055430233811003',
		'F00BEQX05R020220214072844451069045',
		'F00BEQX05R020220214072858571599045',
		'F00BEQX05R020220214072901310139045',
		'F00BEQX05R020220214072903928713045',
		'F00BEQX0KB020220412082146539566102') 

		# no specs found: 
			F00BEQX05R020220214072858571599045: https://prnt.sc/0EmZe0gfyHlT
			F00BEQX05R020220214072903928713045: https://prnt.sc/rYCpakgVPO8A

	not_prio() Trello 3:
		# https://trello.com/c/N4rmOPtt/4149-ms-price-comparisons-missing-webshots-in-targetcom
		# [MS PRICE] Comparisons : Missing webshots in target.com
		Date: August 18, 2022
		Company: A00
		Retailer: target.com
		Country: US
		Item ID : All items

		A00USX000G020220301072500270851060
		A00USX000G020220301072500780971060
		A00USX000G020220301072501244876060
		A00USX000G020220301072501629030060
		A00USX000G020220301072502089707060
		A00USX000G020220301072502535595060
		A00USX000G020220301072502993031060
		A00USX000G020220301072503407191060

		# 

	SQL update:
		(SP D-1) Request 1:
			Request for copy over in ZURN
			DB: H10
			Table: productdata (VIDEO ONLY)
			Source Date: 2022-08-16
			Target Date: 2022-08-23

			Retailer: www.amazon.com

			Item_ids:

			H10USW00Q8220220203184127457996034
			H10USW00Q8220220203184127620084034
			H10USW00Q8220220203184127987144034

		# exec (product > copy_over > prod_copy_over_video_url_by_date+item_id.txt)

08 24 22
	(W) EMEA region

	(W) 9091	2022-08-24	K00	webshot_url		
		select * from view_all_productdata where date = '2022-08-24' and webshot_url is NULL	
		MISSING WEBSHOT		
		Yes	Maryeeel

		# SELECT item_id, country, source FROM view_all_productdata where date='2022-08-24' and webshot_url is NULL and source = 'etl' order by item_id

	(W) NA region

	DCS-2628
		(R D-2) Experimental 1 (microsoft.com/fr-fr)
			{
			    "response": {
			        "task_uuid": "b86a7e4b-5462-4eb8-be1c-f0cb13d254f7"
			    },
			    "version": "1.0"
			}

		(SP D-5) Request 1:
			Good morning !

			Request to rerun Incorrect data and wrong price in microsoft.com/fr-fr .

			Wrong price
			https://prnt.sc/MM4rAFC2s3N1
			A00FRC40RB000001412950202105120254
			A00FRC40KT020220808042454176306220

			Incorrect data

			https://prnt.sc/wgQhfXq0YNim
			A00FRC40KT020220808041345372078220

			https://prnt.sc/0ih0Gkrfpvne
			A00FRC40KT020220808042454433711220

			https://prnt.sc/RtkAJOmogslk
			A00FRC40RB000001345260202103100635

			SELECT * FROM view_all_productdata where date='2022-08-24' AND website = 'www.microsoft.com/fr-fr' AND item_id in ('A00FRC40RB000001412950202105120254',
			'A00FRC40KT020220808042454176306220',
			'A00FRC40KT020220808041345372078220',
			'A00FRC40KT020220808042454433711220',
			'A00FRC40RB000001345260202103100635'
			)

			select * from rcms_product_skus where item_id in (
			'A00FRC40RB000001412950202105120254',
			'A00FRC40KT020220808042454176306220',
			'A00FRC40KT020220808041345372078220',
			'A00FRC40KT020220808042454433711220',
			'A00FRC40RB000001345260202103100635'
			)

			{
			    "response": {
			        "task_uuid": "ed20bb1e-8947-43ac-a978-5e3359f446be"
			    },
			    "version": "1.0"
			}

			# investigate: variant incorrect, requires variant update

	Setup
		(F D-2) RANK- 385	2022-08-23	2022-08-23	Samsung CE (O00)	Invalid deadlink	www.hellotv.nl	
			4K Beamers
			4k televisie
			8k televisie
			Amerikaanse koelkast
			beamer
			Bluetooth soundbars
			clean station
			Dolby Atmos soundbars
			draadloze stofzuiger
			Droger
			jet

			2022-09-02	

			https://detailsupport.atlassian.net/browse/PRODUCTS-4018 	
			https://prnt.sc/LmgM6CypcqCF
			https://prnt.sc/bWMIHn5PP9Zc
			https://prnt.sc/bT-rmQIIAe0h
			https://prnt.sc/-6YrcOtbnWqv
			https://prnt.sc/tSpUJo5Y008n
			https://prnt.sc/C_GOGOSXEgz8
			https://prnt.sc/MSfdia3VLfix
			https://prnt.sc/Q4s3CK4U_Z76
			https://prnt.sc/WGuHitbqjZTf
			https://prnt.sc/B1lqp3FmJj3a
			https://prnt.sc/Nx3PtcUFvVv9

			# test run without rebuild
				{
				    "response": {
				        "task_uuid": "ec7f05c7-a172-40bf-9997-a2253850c987"
				    },
				    "version": "1.0"
				}

				# 1 keyword (4K Beamers) still invalid deadlink 

			fix_reference (RANK- 385) RANK- 386	2022-08-24	2022-08-24	Samsung CE (O00)	Incorrect product url	www.hellotv.nl	
				4k televisie
				8k televisie
				Amerikaanse koelkast
				beamer
				Bluetooth soundbars
				clean station
				Dolby Atmos soundbars
				draadloze stofzuiger
				Droger
				jet
					2022-09-02	https://detailsupport.atlassian.net/browse/PRODUCTS-4018 	
					https://prnt.sc/E4PN6wJ3LrUg
				https://prnt.sc/NI8MygXKuQw0
				https://prnt.sc/TWDVr13oACXx
				https://prnt.sc/Gy50LNqI3Do0
				https://prnt.sc/o-NMi6AdzBwH
				https://prnt.sc/xmI13EC-vaxY
				https://prnt.sc/wBGY2dlZjLhk
				https://prnt.sc/6O9jm8wxZnbx
				https://prnt.sc/pzb6Zn6lNUp9
				https://prnt.sc/8Mvf_GQRTFNI
					08/24 Kurt Kevin: Add guard clause and alternative selector	For rebuild

		LIST 439 2022-08-24	2022-08-24	Samsung CE (O00)	Duplicate data	www.hellotv.nl	
			TV UHD 2022
			TV QLED 2022
			TV OLED 2022
			Soundbar 2022

			898d0b7c-57cd-45a5-8d12-3ca8e931609b
			0c8fdde4-b9ca-4213-bfe3-e941c38f0d1d
			95a702c1-a3fe-417e-9953-27d70896ca2f
			99acf622-9315-421a-80f3-b9683966b867

			2022-09-02	

			https://detailsupport.atlassian.net/browse/PRODUCTS-4019	
			https://prnt.sc/VG4b5TFmZUl5
			https://prnt.sc/CqPey-pnWXKY
			https://prnt.sc/yluXdTAeMZ0H
			https://prnt.sc/sXB7I7VZQ1NG

			{
			    "response": {
			        "task_uuid": "207c2ad1-b17c-410e-8bce-ddedf3c37893"
			    },
			    "version": "1.0"
			}

	(R D-1) 4983	2022-08-24	New CMS	200	all	www.target.com	
		bluetooth speaker
		earbuds
		smart speaker
		wireless speaker

		Duplicate data	
		https://prnt.sc/blbABcgCHTNU
		https://prnt.sc/WjXp7Ze2Im1u
		https://prnt.sc/Lz5izjSUu7ii
		https://prnt.sc/Yv4wMwGWYpar
		Yes	Phil

		# select date, website, keyword, product_website, product_url, ritem_id, count(1) from view_all_rankingsdata var WHERE date = '2022-08-24'
			GROUP BY date, website, keyword, product_website, product_url, ritem_id 
			HAVING count(1)>1

		{
		    "response": {
		        "task_uuid": "7f7300fc-e2b2-4897-a3c7-aea5f628a5a4"
		    },
		    "version": "1.0"
		}

	(R D-1) 9121	2022-08-24	R10	all	lowes.com	
		SELECT * from view_all_productdata where date='2022-08-24' and website= 'www.lowes.com' and source = 'auto_copy_over'	
		auto copy over (site is up)		
		Yes	Caesa

		# SELECT * from view_all_productdata where date='2022-08-24' and website= 'www.lowes.com' and source = 'auto_copy_over'

		{
		    "response": {
		        "task_uuid": "3f683e3f-9af9-49f6-a6f5-7494b9d00da9"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "03d195f5-165f-47e4-8e4c-3d58370e4377"
		    },
		    "version": "1.0"
		}

	(R D-1) 9122	2022-08-24	H10	in_stock	homedepot.com	
		H10USAX0Q8220220125025724687053025
		H10USAX0Q8220220125025733155385025
		H10USAX0Q8220220125025734462332025
		H10USAX0Q8220220125025735785256025
		H10USAX0Q8220220125025736238693025
		H10USAX0Q8220220310035807300491069
		H10USAX0Q8220220310035813941128069
		H10USAX0Q8220220310035824266117069
		H10USAX0Q8220220316091350744564075
		H10USAX0Q8220220125025729026390025
		H10USAX0Q8220220125025731503727025
		H10USAX0Q8220220125025735657306025
		H10USAX0Q8220220310035811805152069
		H10USAX0Q8220220310035822843279069
		H10USAX0Q8220220310035825608591069
		Spider issue in availability		
		Yes	Glenn

		{
		    "response": {
		        "task_uuid": "60b99df1-0c78-414a-8952-86cb8c12ef35"
		    },
		    "version": "1.0"
		}
		{
		    "response": {
		        "task_uuid": "09668c92-026d-4076-a997-048d37d0adaf"
		    },
		    "version": "1.0"
		}

	(R D-1) 9124	2022-08-24	K00	all	ebuyer.com	
		K00GB3203F000001541410202108110154
		K00GB3205K000000987300202007060750
		Auto Copy Over (Site is up)		
		Yes	Jeremy

		# SELECT * FROM view_all_productdata WHERE date='2022-08-24' AND website = 'www.ebuyer.com' AND item_id in ('K00GB3203F000001541410202108110154',
			'K00GB3205K000000987300202007060750')

		{
		    "response": {
		        "task_uuid": "6e67e96c-54b9-4213-bae4-9018660639d0"
		    },
		    "version": "1.0"
		}

	(R D-2) 9125	2022-08-24	K00	all	scan.co.uk	
		K00GB670Q1020220801034106943632213
		K00GB670Q1020220801034109599888213
		K00GB6702N020220801034106224415213
		K00GB670U1020220801034104260477213
		K00GB670P1020220801034104136397213
		K00GB670P1020220801034104076534213
		K00GB6707U020220801034054908423213
		K00GB6707U020220801034102389879213
		K00GB6707U020220801034056856845213
		K00GB6707U020220801034052202201213
		K00GB6707U020220801034101116180213
		K00GB6707U020220801034100853739213
		K00GB6707U020220801034052445250213
		K00GB6707U020220801034052672157213
		K00GB6707U020220801034053067956213
		K00GB6707U020220801034053123224213
		K00GB6707U020220801034101206065213
		K00GB6707U020220801034059804395213
		K00GB6707U020220801034059463287213
		K00GB6707U020220801034100258503213
		K00GB6707U020220801034048689178213
		K00GB6707U020220801034051871512213
		K00GB670JN020220801034105958730213
		Auto Copy Over (Site is up)
		Yes	Jeremy

		# SELECT * FROM view_all_productdata WHERE date='2022-08-24' AND website = 'www.scan.co.uk' AND item_id in ('K00GB3205K000000987300202007060750')
			'K00GB670Q1020220801034106943632213',
			'K00GB670Q1020220801034109599888213',
			'K00GB6702N020220801034106224415213',
			'K00GB670U1020220801034104260477213',
			'K00GB670P1020220801034104136397213',
			'K00GB670P1020220801034104076534213',
			'K00GB6707U020220801034054908423213',
			'K00GB6707U020220801034102389879213',
			'K00GB6707U020220801034056856845213',
			'K00GB6707U020220801034052202201213',
			'K00GB6707U020220801034101116180213',
			'K00GB6707U020220801034100853739213',
			'K00GB6707U020220801034052445250213',
			'K00GB6707U020220801034052672157213',
			'K00GB6707U020220801034053067956213',
			'K00GB6707U020220801034053123224213',
			'K00GB6707U020220801034101206065213',
			'K00GB6707U020220801034059804395213',
			'K00GB6707U020220801034059463287213',
			'K00GB6707U020220801034100258503213',
			'K00GB6707U020220801034048689178213',
			'K00GB6707U020220801034051871512213',
			'K00GB670JN020220801034105958730213')

		{
		    "response": {
		        "task_uuid": "1249eec8-7d2d-440a-a1b3-9d2269249189"
		    },
		    "version": "1.0"
		}
		{
		    "response": {
		        "task_uuid": "50afcd7e-936e-442b-a0f5-999cbad7e176"
		    },
		    "version": "1.0"
		}

		# 2 item_ids left:
			K00GB6707U020220801034052445250213
			K00GB6707U020220801034100258503213

	(R D-1) 9126	2022-08-24	K00	in_stock	elgiganten.se	
		K00SE3003F000001307460202101210507
		K00SE300CG000001525860202108020807
		K00SE300CG000001297700202101180342
		K00SE3005K000001556200202108180340
		Not_found in stock		
		Yes	Jeremy

		# SELECT * FROM view_all_productdata WHERE date='2022-08-24' AND website = 'www.elgiganten.se' AND item_id in ('K00SE3003F000001307460202101210507',
			'K00SE300CG000001525860202108020807',
			'K00SE300CG000001297700202101180342',
			'K00SE3005K000001556200202108180340')

	(R D-1) 9127	2022-08-24	K00	all	power.se	
		K00SEY20CG000001468470202107050619
		K00SEY20CG000001468580202107050619
		Auto Copy Over (Site is up)		
		Yes	Jeremy

		# SELECT * FROM view_all_productdata WHERE date='2022-08-24' AND website = 'www.power.se' AND item_id in ('K00SEY20CG000001468470202107050619',
		'K00SEY20CG000001468580202107050619')

	(R D-1) 9128	2022-08-24	K00	all	virginmegastore.ae	
		K00AEGR08N020220614065045257384165
		K00AEGR0AD000001556300202108180340
		K00AEGR0Q1020220607015135780625158
		K00AEGR0Q1020220607015135486767158
		K00AEGR0AD000001556380202108180340
		K00AEGR0MD000001452930202106160816
		K00AEGR04K000001556440202108180340
		K00AEGR04K000001556410202108180340
		K00AEGR04K000001556470202108180340
		K00AEGR0P1020220607015136224522158
		K00AEGR03F000001452940202106160816
		K00AEGR0P1020220607015135927145158
		K00AEGR03F000001556590202108180340
		K00AEGR03F000001556670202108180340
		K00AEGR03F000001556710202108180340
		K00AEGR03F000001556800202108180340
		K00AEGR07U020220627051304123371178
		K00AEGR07U020220627051304371709178
		K00AEGR07U020220614065045400188165
		K00AEGR07U020220627051305872062178
		K00AEGR07U020220614065046297892165
		K00AEGR07U020220614065045689699165
		K00AEGR07U020220614065045842054165
		K00AEGR07U020220614065045986538165
		K00AEGR07U020220614065046128878165
		K00AEGR0CG000001245410202011170417
		K00AEGR07U020220328083139026340087
		K00AEGR07U020220328083139161331087
		K00AEGR07U020220328083139866049087
		K00AEGR07U020220328083140872969087
		K00AEGR0CG000001525060202108020807
		K00AEGR07U020220614065047050357165
		K00AEGR07U020211115112915430247319
		K00AEGR07U020220614065047616146165
		K00AEGR07U020220614065047761246165
		K00AEGR07U020220614065047901052165
		K00AEGR07U020220222101536631845053
		K00AEGR07U020211115112921405298319
		K00AEGR0CG000001372770202104050819
		K00AEGR07U020220614065048500764165
		K00AEGR07U020220627051304766305178
		K00AEGR07U020220627051305421266178
		K00AEGR07U020220627051303699370178
		K00AEGR07U020220614065049103319165
		K00AEGR07U020211115112921237800319
		K00AEGR07U020220614065049393403165
		K00AEGR07U020220614065049684571165
		K00AEGR0CG000001372750202104050819
		K00AEGR07U020220222101534981874053
		K00AEGR07U020220222101535169724053
		K00AEGR07U020220614065050136413165
		K00AEGR07U020220614065050435551165
		K00AEGR07U020220222101535713059053
		K00AEGR07U020220222101535991324053
		K00AEGR07U020211115112915967668319
		K00AEGR07U020220614065050565915165
		K00AEGR07U020211115112915690652319
		K00AEGR07U020220222101537134976053
		K00AEGR07U020220620055718492109171
		K00AEGR07U020220620055718353961171
		K00AEGR07U020220620051759093633171
		K00AEGR07U020220620051756238865171
		K00AEGR07U020220614065051455312165
		K00AEGR07U020220627051303390807178
		K00AEGR07U020211115112922073650319
		K00AEGR0SB220220607015134755236158
		K00AEGR0V1020220607015135337746158
		K00AEGR05K000001556880202108180340
		K00AEGR0V1020220607015135193627158
		K00AEGR05K000001556920202108180340
		K00AEGR0TB220220607015135048623158
		K00AEGR04K000001452720202106160816
		K00AEGR04K000001452920202106160816
		Auto Copy Over (Site is up)		
		Yes	Jeremy

		# script too long

		{
		    "response": {
		        "task_uuid": "4c53ed88-2ced-4670-964f-b3c9aeaf89f4"
		    },
		    "version": "1.0"
		}

	(R D-2) 9129	2022-08-24	K00	all	noon.com/saudi-en/	
		K00SAVX0Q1020220614065043556608165
		K00SAVX0U1020220613054711576240164
		K00SAVX0P1020220613054712824755164
		K00SAVX0P1020220613054708276250164
		K00SAVX0P1020220613054712223357164
		K00SA5X03F000001556650202108180340
		K00SAVX0P1020220613054709017171164
		K00SAVX0P1020220613054712674890164
		K00SAVX0P1020220613054708124469164
		K00SAVX0P1020220613054707982690164
		K00SA5X0CG000001519340202107260605
		K00SAVX07U020220328081045240490087
		K00SAVX07U020220613054720526866164
		K00SA5X0CG000001519410202107260605
		K00SAVX07U020220613054720939494164
		K00SA5X0CG000001519960202107260605
		K00SAVX07U020220613054721371977164
		K00SAVX07U020220222101530550085053
		K00SA5X0CG000001519660202107260605
		K00SA5X0CG000001519920202107260605
		K00SAVX07U020220613054721573493164
		K00SAVX0V1020220613063353132496164
		K00SAVX0V1020220613054708863291164
		K00SAVX0V1020220613054708725485164

		Incorrect Price fetched and and it should be Out of Stock(OOS)	
		https://prnt.sc/NSyBsthAH0_v	
		Yes	Jeremy

08 27 22
	(W) EMEA region

	(W) NA region

	Trello 1:
		# https://trello.com/c/yDEwi9sm/4170-razer-rankings-auto-copy-over-in-wwwmediaexpertpl
		# [Razer] RANKINGS: Auto copy over in www.mediaexpert.pl
		Retailer: www.mediaexpert.pl

		Issue:  Auto copy over for 3 days / Need more time to fix

		keywords:

		Klawiatura do gier
		Klawiatura PC
		Klawiatura mechaniczna
		Głośniki
		2.1 Głośniki
		Podkładka pod mysz
		Gaming Mousemat
		Fotel do gier
		Krzesło biurowe
		Krzesło
		Ergonomiczne krzesło
		Kontroler PS
		Kontroler Xbox One X.
		Kontroler Xbox
		Kontroler Ps4
		Kontroler Playstation
		słuchawki
		Słuchawki ANC

		Screenshot:
		https://prnt.sc/vmWpUSzf0FUN

		# 

	DCS-2628
		# A00FRC40RB000001345270202103100635
		Experimental 1:
			{
		    "response": {
		        "task_uuid": "964da42b-e12c-4a0c-aaa8-c9a17bf4e542"
		    },
		    "version": "1.0"
		}


	SQL updates:
		(SP D-2) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-26

			Field for update: is_new

			Reason:, -2 values

			# 

		(SP D-1) Request 2:
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110

			Table: view_all_listingsdata

			Date: 08/27/2022

			Retailer: www.fnac.be/nl

			category:
			hardware

			Listings UUID(s):
			d438865e-7705-45d2-ba93-16df3e3cb3e4

			Screenshot:
			https://prnt.sc/hZXCCpkY9Wb5

			Reason: Fetching data even if it''s deadlink(re direct url)

			Thank you

			# (listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql) 

		(SP D-1) Request 3:
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110

			Table: view_all_listingsdata

			Date: 08/27/2022

			Retailer: www.fnac.be/fr

			category:
			hardware
			software

			Listings UUID(s):
			32c5b7d1-abc6-4185-a4c8-7b38155a0a26
			d1104ee1-3f4a-42a1-ad43-7b4b4aea5f45

			Screenshot:
			https://prnt.sc/gFpLv1yL99tH
			https://prnt.sc/BS4K7xKkMimh

			Reason: Fetching data even if it''s deadlink(Top level category/re direct url)

			# (listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql) 

		(SP D-1) Request 4:
			Request to Set Deadlink Rankings Data in Nintendo Benelux

			DB: 110

			Table: view_all_rankingsdata

			Date: 08/27/2022

			Retailer: nedgame.nl

			Keyword:
			switch software

			Screenshot:
			https://prnt.sc/EwIgw_1VK2VV

			Reason: Fetching data even if it''s deadlink

			Thanks!

		(SP D-3) Request 5:
			Hello, Good morning!
			Request to rerun in mediamarkt.de. Thank you


			DB: A00
			Retailer: mediamarkt.de
			Reason: ACO
			Item id: all items

			https://prnt.sc/ui03u55Rm2Q3

			# 130 item_ids
			{
			    "response": {
			        "task_uuid": "49502eb5-1cc2-484c-b2f6-32b597ede2fd"
			    },
			    "version": "1.0"
			}

			# 50 item_ids
			{
			    "response": {
			        "task_uuid": "738f34ac-f0d1-429e-878f-460d31d59b7f"
			    },
			    "version": "1.0"
			}

			# 50 item_ids
			{
			    "response": {
			        "task_uuid": "65617873-841c-4a9c-8129-79a10a4ad6cb"
			    },
			    "version": "1.0"
			}

			# 94 item_ids
			{
			    "response": {
			        "task_uuid": "e6cd1546-3d4f-4eef-9e0e-2f74c5ff1b1a"
			    },
			    "version": "1.0"
			}

			# 87
			{
			    "response": {
			        "task_uuid": "5d0ad06b-cb71-4f2c-9d48-44f34cf6b779"
			    },
			    "version": "1.0"
			}

			# 87
			{
			    "response": {
			        "task_uuid": "9c03ed9e-71e1-43dc-9c71-88f9e857ccea"
			    },
			    "version": "1.0"
			}

		(SP D-2) Request 6:
			Hello, Good afternoon!
			Request to rerun target.com. Thank you.

			DB: A00
			Retailer: target.com
			Reason: Incorrect data
			Item ID: All items

			https://prnt.sc/ZTYlbUtGWUyZ
			{
			    "response": {
			        "task_uuid": "f5d9af7b-709d-4930-a606-4812d37a969c"
			    },
			    "version": "1.0"
			}

			# fixed api and extract price

			{
			    "response": {
			        "task_uuid": "f40411a4-0a99-4dd5-a7fd-d5fae067cd68"
			    },
			    "version": "1.0"
			}

			# item_ids
			{
			    "response": {
			        "task_uuid": "97bc4abf-b25b-4ee2-8c8a-7a7193dc47eb"
			    },
			    "version": "1.0"
			}

08 28 22
	(W) NA region

	(SP D-2) Request 1:
		Request for update in ZOUND SDA

		DB: 200
		Table: view_3P_data
		Date: 2022-08-27

		Field for update: is_new

		Reason:, -2 values

		# (3p > 3p_update_is_new.sql)

	Special Rerun:
		(SP D-3) Request 1:
			Hello, Good morning!
			Request to rerun mediaworld.it. Thank you.

			DB: A00
			Retailer: mediaworld.it
			Reason: Invalid deadlink
			Item ID: All items

			https://prnt.sc/Q0jwQmk1R3bg

			{
			    "response": {
			        "task_uuid": "2983bc18-b4e6-4637-afdd-7f5d8b3e56e7"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "e5a6d22a-375d-4b75-8544-361008561007"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "cf4b5afa-e6a6-40e9-a46f-beb2e88f27f8"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "a6810f30-cda0-45be-8b21-5b94d0f8a77d"
			    },
			    "version": "1.0"
			}

		(SP D-3) Request 2:
			Hello, Good morning!
			Request to rerun saturn.de. Thank you.


			DB: A00
			Retailer: saturn.de
			Reason: Invalid deadlink
			Item ID: All items

			https://prnt.sc/p-_VXWM-dGgT

			{
			    "response": {
			        "task_uuid": "a6a21e08-4220-4ce0-854d-1b02d46fb33d"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "581f4fe2-6819-4a0c-a6bc-238676fc9af8"
			    },
			    "version": "1.0"
			}

		(SP D-3) Request 3:
			Hello, Good morning!
			Request to rerun mediamarkt.de. Thank you.


			DB: A00
			Retailer: mediamarkt.de
			Reason: Invalid deadlink
			Item ID: All items

			https://prnt.sc/AW-K2Bjgk4QC

			{
			    "response": {
			        "task_uuid": "920131ee-2402-4b04-b47b-aaec5e31c878"
			    },
			    "version": "1.0"
			}
			
		(SP D-3) Request 4:
			Hello, Good morning!
			Request to rerun amazon.it. Thank you.


			DB: A00
			Retailer: amazon.it
			Reason: Invalid deadlink
			Item ID: All items

			https://prnt.sc/wpQMbnSdPc8S

			{
			    "response": {
			        "task_uuid": "7d4cb62d-b981-413a-8ac5-21950d278bca"
			    },
			    "version": "1.0"
			}

		(SP D-2) Request 5:
			Hello, Good morning!
			Request to rerun amazon.co.uk

			Thank you.


			DB: A00
			Retailer: amazon.it
			Reason: Invalid deadlink

			https://prnt.sc/r3RtwBwWkLzf


			{
			    "response": {
			        "task_uuid": "8fcb308a-dd52-499e-97b8-3b817d22af7c"
			    },
			    "version": "1.0"
			}

		(SP D-2) Requst 6:
			Hello, Good morning!
			Request to rerun addtn''l INVALID DEADLINK data in EMEA Region. Thank you.

			# 280 all invalid deadlink rerun

			# GB
			{
			    "response": {
			        "task_uuid": "a0c38254-b42f-4df0-8918-39b4e14d9edd"
			    },
			    "version": "1.0"
			}

			# IT
			{
			    "response": {
			        "task_uuid": "7cfdd1a3-02bc-4c03-92b7-4ae20fdb85f2"
			    },
			    "version": "1.0"
			}

			# NL
			{
			    "response": {
			        "task_uuid": "d8c795d5-516f-43a1-8b72-2901ac8ec77c"
			    },
			    "version": "1.0"
			}

			# DE
			{
			    "response": {
			        "task_uuid": "bc34f246-7dd1-4b1c-85c4-c92098c0c499"
			    },
			    "version": "1.0"
			}

			# FR
			{
			    "response": {
			        "task_uuid": "cb570ef3-0490-4f34-b027-74ace76e5354"
			    },
			    "version": "1.0"
			}

			# ES
			{
			    "response": {
			        "task_uuid": "ccdfc295-336b-420f-afa6-61844a6c7aba"
			    },
			    "version": "1.0"
			}

	(F D-1) (R D-1) Trello 1:
		# https://trello.com/c/fdOJKnov/4157-haglofs-compliance-incomplete-description-in-wwwfjellsportno
		# [HAGLOFS] Compliance - Incomplete description in www.fjellsport.no
		date: 2022-08-19
		retailer: www.fjellsport.no
		item_ids: All item id

		# SELECT * FROM view_all_productdata where date='2022-08-28' AND website = 'www.fjellsport.no' order by price_value 

		# fixed 

	(R D-3) 9267	2022-08-28	100	all	power.no	
		100NOZ20ZQ020220718044719999102199
		100NOZ20ZQ020220718044720382382199
		100NOZ2010000001330110202102111449
		100NOZ2010000001662010202111170057
		100NOZ2010000000813810201908020321
		100NOZ2010000000814210201908020341
		100NOZ2010000001662040202111170057
		100NOZ2010000001662060202111170057
		100NOZ2010000001249750202011251841
		100NOZ20ZQ020220124094306891539024
		100NOZ20ZQ020220718044733866986199
		100NOZ2010000001249770202011251841
		100NOZ2010000001331650202102111449
		100NOZ2010000001438880202106080416
		100NOZ20ZQ020220124094331095313024
		100NOZ2010000001401470202105040816
		100NOZ20ZQ020220214062229340926045
		100NOZ2010000001330170202102111449
		100NOZ2010000001228090202010290703
		100NOZ2010000001398000202105040251
		100NOZ2010000000818280201908020936
		100NOZ2010000000931180202004210710
		100NOZ2010000001228120202010290703
		spider issue -3 values		
		Yes	Have

		# script too long

		{
		    "response": {
		        "task_uuid": "feafd0a7-07f1-4cc0-94a5-fa5e85ce2f7f"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "d7bc84cf-8a74-48b5-bb8a-ed77bb13cee2"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "b80a3257-8019-4abf-a30c-6d772bc9c95a"
		    },
		    "version": "1.0"
		}

	---
	(R D-1) 9268	2022-08-28	100	all	elgiganten.se	
		100SE300ZQ020220124090817925865024
		100SE30010000001319740202102040906
		100SE30010000001248500202011250759
		100SE30010000001397570202105040251
		100SE300ZQ020220502042249244770122
		Not_Found in stock and Incorrect price fetched and Delivery Quote(DQ)	
		https://prnt.sc/v8YZO_kJyrsC	
		Yes	Jeremy

		# SELECT * FROM view_all_productdata where date='2022-08-28' and website='www.elgiganten.se' and item_id in (
			'100SE300ZQ020220124090817925865024',
			'100SE30010000001319740202102040906',
			'100SE30010000001248500202011250759',
			'100SE30010000001397570202105040251',
			'100SE300ZQ020220502042249244770122')

		{
		    "response": {
		        "task_uuid": "28260528-96eb-49a1-983e-70e7724a2a3c"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "07716386-7a73-4b96-b640-0ecb81c70dc9"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "3fe0fdb6-0f0c-4ad6-8f50-6ad399670f21"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "de09e0a7-aaf9-438b-bde1-f633cf85c240"
		    },
		    "version": "1.0"
		}

	(R D-1) 9269	2022-08-28	100	all	elkjop.no	
		100NO500ZQ020220124094302273327024
		100NO500ZQ020220124094327160417024
		spider issue -3 values		
		Yes	Have

		# SELECT * FROM view_all_productdata where date='2022-08-28' and website='www.elkjop.no' and item_id in
			'100NO500ZQ020220124094302273327024',
			'100NO500ZQ020220124094327160417024')

		{
		    "response": {
		        "task_uuid": "c2c7c394-88a9-40dc-be8d-d6787de6d93a"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "72f4d559-125e-45c1-bba7-53f582308124"
		    },
		    "version": "1.0"
		}

	(R D-2) 9273	2022-08-28	F00	all	belsimpel.nl	
		F00NLWG040000001256180202012100148
		F00NLWG040000001313290202102020203
		F00NLWG0WJ000001388320202104210551
		F00NLWG0EL000001460650202106280542
		F00NLWG0EL000001658060202111152217
		F00NLWG0EL000001460630202106280542
		F00NLWG0EL000001530410202108030256
		F00NLWG0EL000001530500202108030256
		F00NLWG040000001550730202108170144
		F00NLWG05R020220110050219196836010
		F00NLWG05R020220214072849565419045
		F00NLWG05R020220214072854543076045
		F00NLWG05R020220815020420495000227
		F00NLWG05R020220412075015368572102
		F00NLWG05R020220412075032563751102
		F00NLWG05R020220412075041617252102
		F00NLWG05R020220815020414363457227

		invalid deadlink		
		Yes	Crestine

		# -- belsimple.nl
		SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in (
		'F00NLWG040000001256180202012100148',
		'F00NLWG040000001313290202102020203',
		'F00NLWG0WJ000001388320202104210551',
		'F00NLWG0EL000001460650202106280542',
		'F00NLWG0EL000001658060202111152217',
		'F00NLWG0EL000001460630202106280542',
		'F00NLWG0EL000001530410202108030256',
		'F00NLWG0EL000001530500202108030256',
		'F00NLWG040000001550730202108170144',
		'F00NLWG05R020220110050219196836010',
		'F00NLWG05R020220214072849565419045',
		'F00NLWG05R020220214072854543076045',
		'F00NLWG05R020220815020420495000227',
		'F00NLWG05R020220412075015368572102',
		'F00NLWG05R020220412075032563751102',
		'F00NLWG05R020220412075041617252102',
		'F00NLWG05R020220815020414363457227'
		) ORDER BY in_stock 

		{
		    "response": {
		        "task_uuid": "f7461934-8bd1-4c46-bd1c-d6922134eb03"
		    },
		    "version": "1.0"
		}

	(R D-1) 9274	2022-08-28	O00	all	krefel.be/nl	
		O00BEIS040000001233400202011030616
		O00BEIS040000001219680202010210733
		O00BEIS03R020220530033919878142150
		O00BEIS040000001612970202110040547
		O00BEIS040000001612870202110040526
		O00BEIS040000001516020202107260202
		O00BEIS040000001375150202104120812
		O00BEIS03R020220404073826415129094
		O00BEIS03R020220404073826252749094
		O00BEIS03R020220404073826123069094
		O00BEIS03R020220404073825735397094
		O00BEIS040000001219620202010210733
		O00BEIS03R020220523024208272255143
		O00BEIS03R020220329062401681840088
		O00BEIS03R020220329062409867626088
		O00BEIS03R020220329062408510033088
		O00BEIS03R020220329062409033367088
		O00BEIS03R020220404073826558885094
		O00BEIS03R020220404073825315587094
		O00BEIS03R020220516032836083509136
		O00BEIS03R020220404073825611554094
		O00BEIS040000001233390202011030616
		O00BEIS040000001339570202103020554
		O00BEIS03R020220329062403064505088
		O00BEIS040000001473960202107060502
		invalid deadlink		
		Yes	Have

		# SELECT * FROM view_all_productdata where date='2022-08-28' AND website = 'www.krefel.be/nl' AND item_id in ('O00BEIS040000001233400202011030616',
			'O00BEIS040000001219680202010210733',
			'O00BEIS03R020220530033919878142150',
			'O00BEIS040000001612970202110040547',
			'O00BEIS040000001612870202110040526',
			'O00BEIS040000001516020202107260202',
			'O00BEIS040000001375150202104120812',
			'O00BEIS03R020220404073826415129094',
			'O00BEIS03R020220404073826252749094',
			'O00BEIS03R020220404073826123069094',
			'O00BEIS03R020220404073825735397094',
			'O00BEIS040000001219620202010210733',
			'O00BEIS03R020220523024208272255143',
			'O00BEIS03R020220329062401681840088',
			'O00BEIS03R020220329062409867626088',
			'O00BEIS03R020220329062408510033088',
			'O00BEIS03R020220329062409033367088',
			'O00BEIS03R020220404073826558885094',
			'O00BEIS03R020220404073825315587094',
			'O00BEIS03R020220516032836083509136',
			'O00BEIS03R020220404073825611554094',
			'O00BEIS040000001233390202011030616',
			'O00BEIS040000001339570202103020554',
			'O00BEIS03R020220329062403064505088',
			'O00BEIS040000001473960202107060502')

	(R D-1) 9275	2022-08-28	F00	all	kpn.com	
		F00NLXG05R020220214072838628435045
		F00NLXG0JA000001423390202105250316
		F00NLXG0JA000001415960202105170845

		invalid deadlink		
		Yes	Crestine

		# SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('F00NLXG05R020220214072838628435045',
			'F00NLXG0JA000001423390202105250316',
			'F00NLXG0JA000001415960202105170845')

	(R D-1) 9276	2022-08-28	F00	title	base.be/nl/	
		F00BENX040000001636520202111040406
		F00BENX040000001636690202111040406
		F00BENX040000001636730202111040406
		F00BEQX05R020220214072847792427045
		F00BEQX05R020220214072903928713045
		-1 product website		
		Yes	Glenn

		# -- base.be/nl/	
		SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('F00BENX040000001636520202111040406',
		'F00BENX040000001636690202111040406',
		'F00BENX040000001636730202111040406',
		'F00BEQX05R020220214072847792427045',
		'F00BEQX05R020220214072903928713045')

	(R D-1) 9277	2022-08-28	O00	all	mediamarkt.be/nl/	
		O00BE2D03R020220614132422039565165
		O00BE2D040000001210280202009290256
		O00BE2D040000001218800202010210151
		O00BE2D03R020220516032832534781136
		O00BE2D040000001210310202009290259
		O00BE2D040000001233510202011030643
		O00BE2D03R020220516032833077773136
		O00BE2D040000001233580202011030652
		O00BE2D040000001339200202103010526
		O00BE2D03R020220516032834400808136
		invalid deadlink		
		Yes	Have

		# -- mediamarkt.be/nl/	
			SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('O00BE2D03R020220614132422039565165',
			'O00BE2D040000001210280202009290256',
			'O00BE2D040000001218800202010210151',
			'O00BE2D03R020220516032832534781136',
			'O00BE2D040000001210310202009290259',
			'O00BE2D040000001233510202011030643',
			'O00BE2D03R020220516032833077773136',
			'O00BE2D040000001233580202011030652',
			'O00BE2D040000001339200202103010526',
			'O00BE2D03R020220516032834400808136')

	(R D-1) 9278	2022-08-28	F00	all	mediamarkt.nl	
		F00NLQ10ME020220228063413829866059
		F00NLQ10ME020220228063413829866059
		F00NLQ10WJ000001260910202012110829
		F00NLQ10WJ000001530000202108030256
		F00NLQ10WJ000001260310202012110829
		F00NLQ10WJ000001530080202108030256
		F00NLQ10WJ000001388430202104210551
		F00NLQ105R020220412075021759764102
		F00NLQ105R020220412075021759764102
		F00NLQ105R020220412075022411670102
		F00NLQ105R020220412075022595798102
		F00NLQ105R020220412075022595798102
		F00NLQ1040000001256930202012100148
		F00NLQ1040000001256930202012100148
		F00NLQ1040000001312960202102020203
		F00NLQ105R020220117024911700850017
		F00NLQ105R020220214072852720115045
		F00NLQ105R020220214072851443870045
		F00NLQ105R020220214072859324481045
		F00NLQ105R020220214072901934755045
		F00NLQ105R020220214072832005822045
		F00NLQ105R020220214072836381640045
		F00NLQ105R020220214072841014638045
		F00NLQ105R020220214072844062047045
		F00NLQ105R020220412075028151124102
		F00NLQ105R020220412075029928560102
		F00NLQ105R020220412075033135007102
		F00NLQ105R020220412075035753800102
		F00NLQ105R020220412075040813887102
		F00NLQ105R020220412082127614637102
		F00NLQ105R020220412082128510743102
		F00NLQ105R020220412082130918398102
		F00NLQ1040000001550760202108170144
		F00NLQ105R020220815020413035792227
		F00NLQ105R020220815020411036223227
		F00NLQ105R020220815020409640633227
		F00NLQ105R020220815020411873718227
		F00NLQ1040000001549730202108170144
		F00NLQ105R020220815020417402524227
		F00NLQ10EL000001621190202110180628
		F00NLQ10EL000001450490202106150903

		invalid deadlink		
		Yes	Crestine

		# -- mediamarkt.nl
			SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('F00NLQ10ME020220228063413829866059',
			'F00NLQ10ME020220228063413829866059',
			'F00NLQ10WJ000001260910202012110829',
			'F00NLQ10WJ000001530000202108030256',
			'F00NLQ10WJ000001260310202012110829',
			'F00NLQ10WJ000001530080202108030256',
			'F00NLQ10WJ000001388430202104210551',
			'F00NLQ105R020220412075021759764102',
			'F00NLQ105R020220412075021759764102',
			'F00NLQ105R020220412075022411670102',
			'F00NLQ105R020220412075022595798102',
			'F00NLQ105R020220412075022595798102',
			'F00NLQ1040000001256930202012100148',
			'F00NLQ1040000001256930202012100148',
			'F00NLQ1040000001312960202102020203',
			'F00NLQ105R020220117024911700850017',
			'F00NLQ105R020220214072852720115045',
			'F00NLQ105R020220214072851443870045',
			'F00NLQ105R020220214072859324481045',
			'F00NLQ105R020220214072901934755045',
			'F00NLQ105R020220214072832005822045',
			'F00NLQ105R020220214072836381640045',
			'F00NLQ105R020220214072841014638045',
			'F00NLQ105R020220214072844062047045',
			'F00NLQ105R020220412075028151124102',
			'F00NLQ105R020220412075029928560102',
			'F00NLQ105R020220412075033135007102',
			'F00NLQ105R020220412075035753800102',
			'F00NLQ105R020220412075040813887102',
			'F00NLQ105R020220412082127614637102',
			'F00NLQ105R020220412082128510743102',
			'F00NLQ105R020220412082130918398102',
			'F00NLQ1040000001550760202108170144',
			'F00NLQ105R020220815020413035792227',
			'F00NLQ105R020220815020411036223227',
			'F00NLQ105R020220815020409640633227',
			'F00NLQ105R020220815020411873718227',
			'F00NLQ1040000001549730202108170144',
			'F00NLQ105R020220815020417402524227',
			'F00NLQ10EL000001621190202110180628',
			'F00NLQ10EL000001450490202106150903')

	(R D-2) 9279	2022-08-28	H10	all	grainger.com	
		H10US421Q8220220207035943163365038
		H10US421Q8220220207035949847952038
		H10US421C9220220316085512688959075
		H10US421C9220220316085514443685075
		H10US421C9220220316085519242500075
		H10US421Q8220220207035945354005038
		H10US421C9220220316085519541048075
		H10US421Q8220220207035959124637038
		H10US421Q8220220207035943806844038
		H10US421Q8220220207035949271550038
		H10US421Q8220220207035946270927038
		H10US421Q8220220207040623381948038
		H10US421C9220220316085522690625075
		H10US421Q8220220207035955478080038
		H10US421Q8220220316091329390011075
		H10US421Q8220220316091329682699075
		H10US421Q8220220316091340879903075
		H10US421Q8220220316091338316080075
		H10US421Q8220220207035955772025038
		H10US421Q8220220316091340575291075
		H10US421Q8220220316091340257384075
		H10US421Q8220220207035953982425038
		H10US421Q8220220316091339267353075
		H10US421Q8220220207035953646353038
		H10US421Q8220220316091339413835075
		H10US421Q8220220316085523717493075
		H10US421Q8220220207035958343420038
		H10US421Q8220220207035958959405038
		H10US421Q8220220207040619630374038
		H10US421Q8220220207040619794474038
		H10US421Q8220220207040621801301038
		H10US421Q8220220207035944700779038
		H10US421Q8220220207035944357881038
		H10US421Q8220220207035950509123038
		H10US421Q8220220207035948289043038
		H10US421Q8220220207035944166864038
		H10US421C9220220316085518940861075
		H10US421C9220220316085521711406075
		H10US421Q8220220207035950274821038
		H10US421Q8220220207035950059678038
		H10US421C9220220316085512160269075
		H10US421Q8220220207035958126681038
		H10US421Q8220220316085525772118075
		H10US421Q8220220316085534990952075
		H10US421Q8220220316085534208359075
		H10US421Q8220220207035959907965038
		H10US421Q8220220207040614918002038
		H10US421Q8220220207040000231453038
		H10US421Q8220220207040000718148038
		H10US421Q8220220207040615048989038
		H10US421Q8220220207040616097233038
		H10US421Q8220220207040615811106038
		H10US421Q8220220207040615949226038
		H10US421Q8220220207040617366489038
		H10US421Q8220220316085525261701075
		H10US421Q8220220207040618214640038
		H10US421Q8220220316085531705297075
		H10US421Q8220220207040618389313038
		H10US421Q8220220316085533566601075
		H10US421Q8220220207040618707821038
		H10US421Q8220220316085533120232075
		H10US421Q8220220316085532951161075
		H10US421Q8220220316085531871587075
		H10US421Q8220220207040620457536038
		H10US421Q8220220316085529442203075
		H10US421Q8220220207040620890158038
		H10US421Q8220220207040621080197038
		H10US421Q8220220207040620658472038
		H10US421Q8220220316085535156813075
		H10US421Q8220220316085531571813075
		H10US421Q8220220316085531255728075
		H10US421Q8220220316085530040574075
		H10US421Q8220220316085529749955075
		H10US421Q8220220316091328395280075
		H10US421Q8220220207040622451576038
		H10US421Q8220220316091328654966075
		H10US421Q8220220207040623101873038
		H10US421Q8220220207040622787519038
		invalid deadlink		
		Yes	Glenn

		# query too long

		{
		    "response": {
		        "task_uuid": "5334aa4c-e647-4576-9ae0-3118d75a2e8a"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "1233681a-1539-4c0f-bae1-0b26bf8f7bf3"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "baa372ae-d203-4eba-ae0c-24eb2197bcad"
		    },
		    "version": "1.0"
		}

	(R D-1) 9280	2022-08-28	O00	all	mediamarkt.nl	
		O00NLQ1040000001248100202011250417
		O00NLQ1040000001564440202108230711
		O00NLQ1040000001564380202108230711
		O00NLQ1040000001564500202108230711
		O00NLQ103R020220421094037943562111
		O00NLQ103R020220421094037803637111
		O00NLQ103R020220421094037258449111
		O00NLQ1040000001218580202010201009
		O00NLQ1040000001216650202010090731
		O00NLQ1040000001233560202011030647
		O00NLQ103R020220421094037125848111
		O00NLQ103R020220504022404476492124
		O00NLQ1040000001241960202011121227
		O00NLQ1040000001425450202105311000
		O00NLQ103R020220421094042829419111
		O00NLQ103R020220421094048745628111
		O00NLQ103R020220421094048438346111
		O00NLQ103R020220421094047843114111
		invalid deadlink		
		Yes	Have

		# -- mediamarkt.nl	
		SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in (
		'O00NLQ1040000001248100202011250417',
		'O00NLQ1040000001564440202108230711',
		'O00NLQ1040000001564380202108230711',
		'O00NLQ1040000001564500202108230711',
		'O00NLQ103R020220421094037943562111',
		'O00NLQ103R020220421094037803637111',
		'O00NLQ103R020220421094037258449111',
		'O00NLQ1040000001218580202010201009',
		'O00NLQ1040000001216650202010090731',
		'O00NLQ1040000001233560202011030647',
		'O00NLQ103R020220421094037125848111',
		'O00NLQ103R020220504022404476492124',
		'O00NLQ1040000001241960202011121227',
		'O00NLQ1040000001425450202105311000',
		'O00NLQ103R020220421094042829419111',
		'O00NLQ103R020220421094048745628111',
		'O00NLQ103R020220421094048438346111',
		'O00NLQ103R020220421094047843114111')

	(R D-1) 9281	2022-08-28	H10	all	amazon.com	
		H10USW00Q8220220125025711263539025
		invalid deadlink		
		Yes	Keeshia

		# -- amazon.com			
		select * from view_all_productdata where date='2022-08-28' AND item_id in ('H10USW00Q8220220125025711263539025')

	(R D-1) 9301	2022-08-28	200	all	amazon.es/3P	
		200ESFF070000000557750201904220952
		200ESFF080000000556470201904220811
		200ESFF080000000557070201904220902
		200ESFF080000000801720201906210132
		invalid deadlink		
		Yes	Louie

		# SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('200ESFF070000000557750201904220952',
			'200ESFF080000000556470201904220811',
			'200ESFF080000000557070201904220902',
			'200ESFF080000000801720201906210132')

		{
		    "response": {
		        "task_uuid": "2531ad3f-9a1d-49d5-8992-d8dc36078c80"
		    },
		    "version": "1.0"
		}

	(R D-1) 9302	2022-08-28	200	all	cdiscount.com/3P	
		200FRMS070000001382730202104150735	
		invalid deadlink		
		Yes	Louie

		{
		    "response": {
		        "task_uuid": "2e71ac81-f462-4d70-9a5f-6078fa3b19ee"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "6ce4bf64-b6f5-4777-9e28-fba07af5c9fc"
		    },
		    "version": "1.0"
		}

	(R D-2) 9303	2022-08-28	200	all	darty.com/3P	
		200FRNS070000001220780202010261317	
		invalid deadlink		
		Yes	Louie

		{
		    "response": {
		        "task_uuid": "692e205e-7192-4a11-b964-e18fc7bc4c1a"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "7a01b5ee-7674-4abf-b935-717e2cb99b8b"
		    },
		    "version": "1.0"
		}

	(R D-1) 9304	2022-08-28	200	all	trovaprezzi.it	
		200ITID080000000790320201906140150	
		invalid deadlink		
		Yes	Louie

		# SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('200ITID080000000790320201906140150')

	(R D-1) 9305	2022-08-28	200	all	amazon.it/3P	
		200ITGF070000000557480201904220941
		200ITGF080000000556780201904220839
		200ITGF080000000801690201906210116
		200ITGF070000000557970201904221001
		invalid deadlink		
		Yes	Louie

		SELECT * FROM view_all_productdata where date='2022-08-28' AND item_id in ('200ITGF070000000557480201904220941',
		'200ITGF080000000556780201904220839',
		'200ITGF080000000801690201906210116',
		'200ITGF070000000557970201904221001')

		{
		    "response": {
		        "task_uuid": "493398f9-b74e-4d21-9531-5b047022feeb"
		    },
		    "version": "1.0"
		}

	(R D-1) 9306	2022-08-28	200	all	amazon.it	
		200ITL4070000000530780201903270510	
		invalid deadlink		
		Yes	Louie

	(R D-2) 9307	2022-08-28	200	all	mediamarkt.de	
		all items	
		ACO site is up		
		Yes	Louie

		{
		    "response": {
		        "task_uuid": "a665dba5-05fa-4f7f-922d-8bd3e738c3b0"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "4f0e617a-1918-4f54-bc39-dc3b56a62eac"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "40dd7939-0cad-4f36-a588-b2234b373299"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "836ecbda-66c4-4630-8e10-1967090446a0"
		    },
		    "version": "1.0"
		}

	(R D-1) 9312	2022-08-28	K00	all	power.se	
		K00SEY207U020220627051301675293178 
		K00SEY20CG000001467990202107050618
		K00SEY20CG000001473370202107060432
		K00SEY207U020220222033749767610053
		K00SEY207U020211116100226634244320
		invalid deadlink		
		Yes	Ash

		# SELECT * FROM view_all_productdata WHERE date='2022-08-28' AND item_id in (
			'K00SEY207U020220627051301675293178',
			'K00SEY20CG000001467990202107050618',
			'K00SEY20CG000001473370202107060432',
			'K00SEY207U020220222033749767610053',
			'K00SEY207U020211116100226634244320')


		{
		    "response": {
		        "task_uuid": "d6e020b7-48bf-4eda-9f55-fc0c59563095"
		    },
		    "version": "1.0"
		}

	---
	0 fr_trello (R D-2) 5030	2022-08-29	New CMS	200	all	www.target.com	
		portable bluetooth speaker
		portable speaker
		wireless earbuds
		
		Duplicate data	

		https://prnt.sc/LKuXEleKUAlj
		https://prnt.sc/Q8rl_Gq-p08d
		https://prnt.sc/iNJREN7dRdsE
		Yes	Phil

		# select date, website, keyword, product_website, product_url, ritem_id, count(1) from view_all_rankingsdata var WHERE date = '2022-08-28'
			GROUP BY date, website, keyword, product_website, product_url, ritem_id 
			HAVING count(1)>1

		{
		    "response": {
		        "task_uuid": "57055ea7-c53d-4447-b3fe-6a6820de165b"
		    },
		    "version": "1.0"
		}

	---
	(R D-2) 1474	2022-08-28	New CMS	010	all	www.currys.co.uk	
		Mice
		Docking stations

		Invalid Deadlink	
		https://prnt.sc/mD9B7XIToVat
		https://prnt.sc/3Zj8T4GG_K34
		Yes	Michael

		# SELECT * FROM view_all_listings_data WHERE date = '2022-08-28' AND website = 'www.currys.co.uk' AND category in ('Mice')

		{
		    "response": {
		        "task_uuid": "7e70b214-7c34-40e7-bb2e-90e66014aae5"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "3b858b6a-c6ca-45e4-bebf-7861a0555119"
		    },
		    "version": "1.0"
		}

	SQL update:
		(R D-2) Request 1:
			Request for copy over in inRiver-Testclient4

			DB: G10
			Table: productdata (DESCRIPTION AND SPECIFICATION ONLY)
			Source Date: 2022-08-21
			Target Date: 2022-08-28

			Retailer: www.amazon.com

			Item_ids:

			G10USW000A220220401082351331374091
			G10USW000A220220401082352884665091

			Reason: Missing description and specification

			# exec (product > copy_over > prod_copy_over_description_by_date+item_id.sql)
			# exec (product > copy_over > prod_copy_over_specification_by_date+item_id.sql)

		(R D-2) Request 2:
			Request to unpublish data in InRiver listings

			DB: 010
			Table: view_all_listingsdata
			Date: 08/28/2022

			Retailer:www.currys.co.uk
			category:

			docking stations

			Listings UUID(s):

			c15e3ac5-ba27-433e-bde2-e526d1fc1030

			Screenshot:https://prnt.sc/KNhI6aPV5lyB
			Reason: Can''t unpublish tru Backdoor

		(R D-2) Request 3:
			Request for copy over in inRiver-Testclient4

			DB: G10
			Table: productdata (DESCRIPTION ONLY)
			Source Date: 2022-08-21
			Target Date: 2022-08-28

			Retailer: www.amazon.com

			Item_ids:

			G10USW000A220220401082353089867091
			G10USW000A220220401082353682908091
			G10USW000A220220401082353228756091
			G10USW000A220220401082353816924091
			G10USW000A220220401082354290266091

			Reason: Incomplete description

			# exec (product > copy_over > prod_copy_over_description_by_date+item_id.sql)

		(R D-2) Request 4:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-08-28

			Field for update: is_new

			Reason:, -2 values

			# 

08 29 22
	(W) EMEA 
		Good morning! Request to rerun missing webshots in EMEA region.
		Excluded retailer: 
		fnac.com & fnac.es

		# 

	(W) 9313	2022-08-29	100	webshot_url		
		select * from view_all_productdata where date='2022-08-29' and webshot_url is NULL
		Missing Webshot		Yes	Keeshia

	(W) NA
		Good afternoon!
		Request to rerun missing webshots in NA Region. Thank you.

		Excluded retailers:

		amazon.ca/direct

		bestbuy.ca

		walmart.com

		walmart.ca

	Trello 1:
		# https://trello.com/c/pHp0sIOt/4181-zound-rankings-invalid-duplicate-data-in-targetcom
		# [Zound] Rankings : Invalid duplicate data in target.com
		Issue: Duplicate Data

		cause: requires more time to investigate

		retailer: www.target.com

		keywords:

		portable bluetooth speaker
		portable speaker
		wireless earbuds

		screenshots: 

		https://prnt.sc/yuX8G5CQnpHd
		https://prnt.sc/c2NwNTXDKJx6
		https://prnt.sc/7u4Dzuev50Ms

	SQL updates:
		(SP D-1) Request 1:
			Request to Copy Over Garmin Listings

			DB: 100

			Table: view_all_listingsdata
			Source Date: 2022-8-28
			Target Date: 2022-8-29

			# exec (listings > copy_over > list_copy_over_by_date.sql)

		(SP D-1) Request 2:
			Request to Set Deadlink Rankings Data in Garmin

			DB: 100
			Table: view_all_rankingsdata
			Date: 08/29/2022
			Retailer: www.dna.fi

			Keywords:
			SYTEKELLO

			Screenshot:
			https://prnt.sc/z23tRz5yyAcZ

			Reason: Fetching data even if it''s deadlink

			# exec (rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql)

		 Request 3:

		(SP D-1) Request 3:
			Request to Set Deadlink Rankings Data in Garmin

			DB: 100
			Table: view_all_rankingsdata
			Date: 08/29/2022

			Retailer: www.dustinhome.se

			Keywords:

			BIL GPS

			Screenshot:
			ttps://prnt.sc/ShO8PaYyG5v8


			Reason: Fetching data even if it''s PDP

			# exec (rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword)

		(SP D-1) Request 4:
			Request to Set Deadlink Rankings Data in Garmin
			DB: 100
			Table: view_all_rankingsdata

			Date: 08/29/2022

			Retailer:
			www.fjellsport.no
			Keywords:

			HUNDEPEILER
			Screenshot:
			https://prnt.sc/HMLnf2-dvF_N
			Reason:
			Fetching data even if it''s deadlink (PDP)

			# exec (rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword)

	(R D-1) 9319	2022-08-29	100	all	www.elgiganten.dk	
		100DK100ZQ020220502042253596723122	
		invalid deadlink		
		Yes	Charlemagne

		{
		    "response": {
		        "task_uuid": "cb2e77b0-d591-4e51-b4a0-fc40a5523edb"
		    },
		    "version": "1.0"
		}

	(R D-1) 9320	2022-08-29	100	all	www.elgiganten.dk	
		100DK10010000001248400202011250630	
		missing data		
		Yes	Charlemagne

		{
		    "response": {
		        "task_uuid": "cb2e77b0-d591-4e51-b4a0-fc40a5523edb"
		    },
		    "version": "1.0"
		}

	(R D-1) 9321	2022-08-29	100	image_count	power.fi	
		100FI0X0ZQ020220124094257509562024
		Missing image_count		
		Yes	Neil

		# SELECT * FROM view_all_productdata where date='2022-08-29' AND website = 'www.power.fi'	AND item_id in ('100FI0X0ZQ020220124094257509562024')

		{
		    "response": {
		        "task_uuid": "81c0e169-0bd3-495c-bad3-6965b8a4f9c3"
		    },
		    "version": "1.0"
		}

	(R D-1) 9322	2022-08-29	100	all	elkjop.no	
		100NO50010000000858090201910100845
		100NO500ZQ020220124090808447264024
		Invalid deadlink		
		Yes	Neil

		SELECT * FROM view_all_productdata where date='2022-08-29' AND website = 'www.elkjop.no'	AND item_id in ('100NO50010000000858090201910100845',
			'100NO500ZQ020220124090808447264024')

		{
		    "response": {
		        "task_uuid": "4c37058c-9daf-4a4b-8ee8-9a1bc6ca0bb9"
		    },
		    "version": "1.0"
		}

	(R D-1) 9329	2022-08-29	100	title	power.dk	
		100DKX2010000001413670202105130512
		100DKX2010000001659350202111162228
		Missing title,description and specs	
		https://prnt.sc/6Xsan8MD_iMt	
		Yes	Jahar

		SELECT * FROM view_all_productdata where date='2022-08-29' AND website = 'www.power.dk'	AND item_id in ('100DKX2010000001413670202105130512',
		'100DKX2010000001659350202111162228')

		{
		    "response": {
		        "task_uuid": "a7ec1dc6-f628-4527-a86c-0be1525126cf"
		    },
		    "version": "1.0"
		}
		{
		    "response": {
		        "task_uuid": "c3b437a6-3a0d-4fac-ad48-b3a906a38ea0"
		    },
		    "version": "1.0"
		}

	(R D-2) 9330	2022-08-29	100	image_count	gigantti.fi	
		100FI60010000001401290202105040816
		100FI600ZQ020220718044729588443199
		Missing image count 	
		https://prnt.sc/VFizLPfKP6i3	
		Yes	Jahar

		SELECT * FROM view_all_productdata where date='2022-08-29' AND website = 'www.gigantti.fi'	AND item_id in ('100FI60010000001401290202105040816',
			'100FI600ZQ020220718044729588443199')

		{
		    "response": {
		        "task_uuid": "853fef0d-71b0-41ef-9efe-17d82c98700c"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "1dadfcb0-c370-411a-811b-55cc45f1fbf7"
		    },
		    "version": "1.0"
		}

	(R D-1) 9335	2022-08-29	K00	rating_reviewers	pcdiga.com	
		K00PT3Q0CG000001582550202109130423 
		K00PT3Q0CG000001582270202109130423
		K00PT3Q0CG000001582120202109130423
		K00PT3Q0CG000001582170202109130423
		K00PT3Q0CG000001582060202109130423
		K00PT3Q0CG000001582320202109130423
		K00PT3Q0CG000001582070202109130423
		K00PT3Q07U020220222101515293383053
		K00PT3Q0CG000001582590202109130423
		K00PT3Q0CG000001581980202109130423
		incorrect rating and reviews fetched		
		Yes	Ash

		SELECT * FROM view_all_productdata WHERE date='2022-08-29' AND item_id in (
			'K00PT3Q0CG000001582550202109130423',
			'K00PT3Q0CG000001582270202109130423',
			'K00PT3Q0CG000001582120202109130423',
			'K00PT3Q0CG000001582170202109130423',
			'K00PT3Q0CG000001582060202109130423',
			'K00PT3Q0CG000001582320202109130423',
			'K00PT3Q0CG000001582070202109130423',
			'K00PT3Q07U020220222101515293383053',
			'K00PT3Q0CG000001582590202109130423',
			'K00PT3Q0CG000001581980202109130423')

		{
		    "response": {
		        "task_uuid": "672364e8-7fc3-415c-b986-53b9450fb80f"
		    },
		    "version": "1.0"
		}

	(R D-1) 9336	2022-08-29	K00	price_value	pcdiga.com	
		K00PT3Q0CG000001582340202109130423 
		K00PT3Q0CG000001582000202109130423
		K00PT3Q07U020211115034618861122319
		ETL issue price		
		Yes	Ash

		SELECT * FROM view_all_productdata WHERE date='2022-08-29' AND item_id in (
			'K00PT3Q0CG000001582340202109130423',
			'K00PT3Q0CG000001582000202109130423',
			'K00PT3Q07U020211115034618861122319')

		{
		    "response": {
		        "task_uuid": "34d8b17c-6c56-4a7d-a2a3-923a156bec56"
		    },
		    "version": "1.0"
		}

	(R D-1) 9337	2022-08-29	K00	all	power.dk	
		K00DKX207U020220627045501740027178
		K00DKX207U020220627045503045272178
		K00DKX207U020220627045503363046178
		Spider Issue -1 and -3 values		
		Yes	Maryeeel

		{
		    "response": {
		        "task_uuid": "6db5e762-d6ae-4540-b8da-347d5ff69877"
		    },
		    "version": "1.0"
		}

	(R D-1) 9343	2022-08-29	100	description	power.se	
		100SEY2010000000905300202001280945
		100SEY20ZQ020220124094254013167024
		100SEY2010000001665590202111170530
		100SEY20ZQ020220718044745489665199
		100SEY20ZQ020220718044721094234199
		100SEY20ZQ020220124094333462822024
		100SEY20ZQ020220718044725377345199
		Missing Description		
		Yes	Benie

		SELECT * FROM view_all_productdata where date='2022-08-29'	AND item_id in (
			'100SEY2010000000905300202001280945',
			'100SEY20ZQ020220124094254013167024',
			'100SEY2010000001665590202111170530',
			'100SEY20ZQ020220718044745489665199',
			'100SEY20ZQ020220718044721094234199',
			'100SEY20ZQ020220124094333462822024',
			'100SEY20ZQ020220718044725377345199')

		{
		    "response": {
		        "task_uuid": "30c481ff-ab5f-4a17-955b-2a1db0af7ee3"
		    },
		    "version": "1.0"
		}

	(R D-2) 9346	2022-08-29	K00	all	mediamarkt.de	  
		select * from view_all_productdata  where date = '2022-08-29' and website = ('www.mediamarkt.de') and source = 'auto_copy_over'	
		ACO site is up		
		Yes	Maryeeel

		# script too long

		{
		    "response": {
		        "task_uuid": "1e383377-3288-4e20-8a41-9316a0f39367"
		    },
		    "version": "1.0"
		}

08 30 22
	(W) EMEA region

	SQL update
		(SP D-2) Request 1:
			Hello,
			Request to rerun data in microsoft.com/fr-fr.

			DB:A00
			Retailer: microsoft.com/fr-fr
			Reason: Affected item id''s should tag as deadlink.
			https://prnt.sc/T6nuHgSVN9vG
			Item ID:

			A00FRC40RB000001345260202103100635
			A00FRC40RB000001345270202103100635
			A00FRC40RB000001237600202011060526
			A00FRC40RB000001237610202011060526
			A00FRC40RB000001237620202011060526
			A00FRC40RB000001237630202011060526
			A00FRC40RB000001237640202011060526
			A00FRC40RB000001237650202011060526
			A00FRC40RB000000948750202005210956
			A00FRC40RB000000948910202005211019
			A00FRC40RB000000948660202005210950

			{
			    "response": {
			        "task_uuid": "3148997f-993b-4bee-9087-318eb3e052b8"
			    },
			    "version": "1.0"
			}

			# exec (product > set_to_dead_link)

	Techvalidation Issues:
		(R D-1) 7020	08/30/2022	No	A00	product_website	amazon.it	
			A00ITL40KT020220502203430839618122
			A00ITL40RB000001052600202008050827
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-30' AND item_id in ('A00ITL40KT020220502203430839618122',
				'A00ITL40RB000001052600202008050827')

			{
			    "response": {
			        "task_uuid": "31bd5677-4a93-482a-8695-1b151737da5d"
			    },
			    "version": "1.0"
			}

		(R D-2) 7021	08/30/2023	No	F00	product_website	base.be/nl/	
			F00BENX0JA000001638910202111040406
			F00BENX0JA000001638580202111040406
			F00BEQX0KB020211207020307638441341
			F00BEQX05R020220214072841509309045
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-30' AND item_id in ('F00BENX0JA000001638910202111040406',
			'F00BENX0JA000001638580202111040406',
			'F00BEQX0KB020211207020307638441341',
			'F00BEQX05R020220214072841509309045')

			{
			    "response": {
			        "task_uuid": "0d954145-6c09-482f-9c8c-c19f1c00a7a5"
			    },
			    "version": "1.0"
			}

		(R D-2) 7022	08/30/2024	No	100	in_stock	elgiganten.dk	
			100DK10010000001397490202105040251
			100DK10010000001659240202111162228
			100DK10010000001659720202111162228
			-1 in_stock

			SELECT * FROM view_all_productdata WHERE date='2022-08-30' AND item_id in (
			'100DK10010000001397490202105040251',
			'100DK10010000001659240202111162228',
			'100DK10010000001659720202111162228')

		(R D-2) 7023	08/30/2025	No	100	in_stock,product_website	elgiganten.dk	
			100DK10010000001397940202105040251	
			-1 in_stock,-3 product_website


			SELECT * FROM view_all_productdata WHERE date='2022-08-30' AND item_id in (
				'100DK10010000001397940202105040251')

			{
			    "response": {
			        "task_uuid": "6800603e-28cb-470c-8d63-0bb441a7e3e3"
			    },
			    "version": "1.0"
			}

		(R D-2) 7024	08/30/2026	No	K00	in_stock	elgiganten.se	
			K00SE300CG000001297740202101180342
			K00SE300CG000000980500202006250916
			K00SE3003F000000925310202002260844
			-1 in_stock

			SELECT * FROM view_all_productdata WHERE date='2022-08-30' AND item_id in ('K00SE300CG000001297740202101180342',
			'K00SE300CG000000980500202006250916',
			'K00SE3003F000000925310202002260844')

			{
			    "response": {
			        "task_uuid": "f5d61ea9-5f20-42b4-b571-7e43d7cb0ce9"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "9956eaee-f25b-43f5-afcf-65abefb87519"
			    },
			    "version": "1.0"
			}

		(R D-2) 7025	08/30/2027	No	110	in_stock,product_website	fnac.be/fr	
			110BEAB0OL000001477960202107120648	
			-1 in_stock,-1 product_website

			-- fnac.be/fr	
			SELECT * FROM view_all_productdata WHERE date='2022-08-30' AND item_id in (
			'110BEAB0OL000001477960202107120648')

			{
			    "response": {
			        "task_uuid": "26eb004f-da9c-41f2-9da2-a420f61f8d61"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "d18ad215-6d25-44e2-8899-1b4a054b38d7"
			    },
			    "version": "1.0"
			}

		(R D-1) 7026	08/30/2028	No	K00	in_stock	fnac.com	
			K00FR210CG000001298530202101180414	
			-1 in_stock

		(R D-1) 7027	08/30/2029	No	O00	delivery	hellotv.nl	
			O00NL9313R020220801021643942731213	
			-3 delivery

		(R D-1) 7028	08/30/2030	No	A00	in_stock	microsoft.com/en-sg	
			A00SG350KT020220329120131403937088
			A00SG350KT020220329120132268615088
			A00SG350KT020220808042456047531220
			A00SG350KT020220329120131028868088
			A00SG350KT020220329120131210793088
			A00SG350KT020220329120131841402088
			A00SG350KT020220808041350162424220
			A00SG350KT020220329120130798215088
			A00SG350KT020220808042456351913220
			A00SG350KT020220329120132458010088
			A00SG350KT020220808042456850092220
			A00SG350KT020220329120131614355088
			A00SG350KT020220329120130556705088
			A00SG350KT020220808042456726053220
			A00SG350KT020220329120132070935088
			A00SG350KT020220808042456599346220
			A00SG350KT020220808042456470318220
			A00SG350KT020220808042456190056220
			-1 in_stock
		
		(R D-1) 7029	08/30/2031	No	A00	in_stock,price_value	microsoft.com/en-sg	
			A00SG350KT020220808041350102217220
			A00SG350KT020220808042455896498220
			A00SG350KT020220808041350007468220
			-1 in_stock,-2 price_value

		(R D-1) 7030	08/30/2032	No	A00	product_website	microsoft.com/es-es	
			A00ESQ40RB000000995030202008040310	
			-1 product_website

		(R D-1) 7031	08/30/2033	No	A00	in_stock,product_website	microsoft.com/fr-fr	
			A00FRC40RB000000948660202005210950
			A00FRC40RB000001345270202103100635
			A00FRC40RB000000948910202005211019
			A00FRC40RB000001237610202011060526
			A00FRC40RB000000948750202005210956
			A00FRC40RB000001237630202011060526
			A00FRC40RB000001345260202103100635
			A00FRC40RB000001237620202011060526
			A00FRC40RB000001237600202011060526
			A00FRC40RB000001237650202011060526
			A00FRC40RB000001237590202011060526
			A00FRC40RB000001237640202011060526
			-1 in_stock,-1 product_website

	Daily Issues
		channeling (R D-1) 9374	2022-08-30	F00	in_stock	mobiel.nl	
			F00NLRU0EL000001642390202111040958
			F00NLRU0WJ000001530120202108030256
			F00NLRU05R020220110050215942920010
			F00NLRU05R020220110050219648101010
			F00NLRU05R020220117024911526093017
			F00NLRU0KB020220329062756041110088
			F00NLRU0JA000001599420202109280434
			F00NLRU0JA000001599690202109280434
			F00NLRU0JA000001602300202109280600
			wrong stock fetch it should be IS	
			Yes	Crestine

			  -- mobiel	
			SELECT * FROM view_all_productdata where date='2022-08-30' AND item_id in ('F00NLRU0EL000001642390202111040958',
			'F00NLRU0WJ000001530120202108030256',
			'F00NLRU05R020220110050215942920010',
			'F00NLRU05R020220110050219648101010',
			'F00NLRU05R020220117024911526093017',
			'F00NLRU0KB020220329062756041110088',
			'F00NLRU0JA000001599420202109280434',
			'F00NLRU0JA000001599690202109280434',
			'F00NLRU0JA000001602300202109280600')

			{
			    "response": {
			        "task_uuid": "38bd333a-0a56-4bee-b13f-ded5938ad0ce"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "e84c04ea-0b18-42b0-af7d-5a55dcd9ae58"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1f92ef01-af1a-4bf4-ac69-0aad5c3fa7da"
			    },
			    "version": "1.0"
			}
		
		(R D-2) 9384	2022-08-30	K00	price_value	pcdiga.com	
			K00PT3Q07U020220222101511768602053
			K00PT3Q0CG000001582250202109130423
			etl issue in price		
			Yes	Maryeeeel

			SELECT * FROM view_all_productdata WHERE date='2022-08-30' AND item_id in ('K00PT3Q07U020220222101511768602053',
				'K00PT3Q0CG000001582250202109130423')

			{
			    "response": {
			        "task_uuid": "1f67f530-947d-4d03-8022-95ca6f66ccf1"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "f00f4100-78ca-4be5-a653-c474506689b0"
			    },
			    "version": "1.0"
			}

		(R D-1) 9390	2022-08-30	H10	all	homedepot.com	
			H10USAX0Q8220220125025724687053025
			H10USAX0Q8220220125025716966096025
			H10USAX0Q8220220125025717296253025
			H10USAX0Q8220220125025718111666025
			H10USAX0Q8220220125025720161843025
			H10USAX0Q8220220125025722193973025
			H10USAX0Q8220220310035814670410069
			H10USAX0Q8220220125025714180701025
			H10USAX0Q8220220125025719453785025
			H10USAX0Q8220220125025721178142025
			H10USAX0Q8220220131092059744593031
			H10USAX0Q8220220310035811632365069
			H10USAX0Q8220220310035805202551069
			H10USAX0Q8220220310035807852452069
			H10USAX0Q8220220310035818091616069
			H10USAX0Q8220220316091349335760075
			H10USAX0Q8220220406015142465870096
			H10USAX0Q8220220316091351865733075
			H10USAX0Q8220220125025735177224025
			H10USAX0Q8220220125025732993013025
			H10USAX0Q8220220125025730997788025
			H10USAX0Q8220220606035804334588157
			H10USAX0Q8220220310035812975479069
			H10USAX0Q8220220310035813177908069
			H10USAX0Q8220220125025724490722025
			H10USAX0Q8220220310035822282270069
			H10USAX0Q8220220125025729026390025
			H10USAX0Q8220220203184129892766034
			H10USAX0Q8220220310035816985930069
			H10USAX0Q8220220310035826322126069
			H10USAX0Q8220220310035822674331069
			H10USAX0Q8220220406015137324729096
			H10USAX0Q8220220316091349903638075
			H10USAX0Q8220220316091349630399075
			H10USAX0Q8220220310035820703556069
			H10USAX0Q8220220310035826980948069
			H10USAX0Q8220220310035821712814069
			H10USAX0Q8220220310035820513008069
			H10USAX0Q8220220310035812581471069
			H10USAX0Q8220220310035807510169069
			H10USAX0Q8220220310035824642876069
			H10USAX0Q8220220310035815751726069
			H10USAX0Q8220220310035826809345069
			H10USAX0Q8220220310035807300491069
			H10USAX0Q8220220316091347359804075
			H10USAX0Q8220220310035807094446069
			H10USAX0Q8220220310035824472682069
			H10USAX0Q8220220310035823417458069
			H10USAX0Q8220220310035825323590069
			H10USAX0Q8220220310035806515672069
			H10USAX0Q8220220310035823734250069
			H10USAX0Q8220220125025723376846025
			H10USAX0Q8220220606035803948952157
			H10USAX0Q8220220125025722533228025
			H10USAX0Q8220220125025734300029025
			H10USAX0Q8220220125025713500632025
			H10USAX0Q8220220125025734462332025
			H10USAX0Q8220220125025720850074025
			H10USAX0Q8220220125025722358341025
			H10USAX0Q8220220125025715271507025
			H10USAX0Q8220220125025722707745025
			H10USAX0Q8220220125025730004635025
			H10USAX0Q8220220125025712565142025
			H10USAX0Q8220220125025716668212025
			H10USAX0Q8220220125025718269225025
			H10USAX0Q8220220125025714492037025
			H10USAX0Q8220220125025718603947025
			H10USAX0Q8220220125025721534023025
			auto copy over (site is up)		
			Yes	Glenn

			# select * from view_all_productdata where date='2022-08-30' and website='www.homedepot.com' AND source = 'auto_copy_over'

			{
			    "response": {
			        "task_uuid": "26e5788c-7819-4729-a9ce-259c9f0d1f1c"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "d7fc5fbf-1e81-4da3-bce3-5515badfd517"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "cba408e2-6738-4ec1-837a-e75b0842de55"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "8b284cdc-3131-44e9-a5cc-e8fd0bcb9921"
			    },
			    "version": "1.0"
			}
		
		(R D-1) 9391	2022-08-30	H10	all	lowes.com	
			H10US8X0Q8220220202031804646471033
			H10US8X0Q8220220202031805529771033
			H10US8X0Q8220220202031754896255033
			H10US8X0Q8220220202031803308187033
			H10US8X0Q8220220202031806230830033
			H10US8X0Q8220220202031757483480033
			H10US8X0Q8220220202031806860201033
			H10US8X0Q8220220202031806530704033
			H10US8X0Q8220220202031754604859033
			H10US8X0C9220220316091356165257075
			H10US8X0Q8220220202031757848052033
			H10US8X0Q8220220202031758173393033
			H10US8X0Q8220220202031753882573033
			H10US8X0C9220220316091352875663075
			H10US8X0Q8220220202031743995690033
			H10US8X0Q8220220202031745075091033
			H10US8X0Q8220220202031743739182033
			H10US8X0Q8220220202031800526690033
			H10US8X0Q8220220316091430400552075
			H10US8X0Q8220220202031752077696033
			H10US8X0Q8220220316091429009390075
			H10US8X0Q8220220202031743868114033
			H10US8X0Q8220220316091416322815075
			H10US8X0Q8220220202031754254804033
			H10US8X0Q8220220202031800151861033
			H10US8X0Q8220220202031800850223033
			H10US8X0Q8220220202031802801092033
			H10US8X0C9220220316091356880001075
			H10US8X0C9220220316091403213968075
			H10US8X0Q8220220202031747965500033
			H10US8X0Q8220220202031748132374033
			H10US8X0C9220220316091359148899075
			H10US8X0Q8220220202031812993646033
			H10US8X0Q8220220316091426907878075
			H10US8X0Q8220220202031814195213033
			H10US8X0C9220220316091353313697075
			H10US8X0C9220220316091353442782075
			H10US8X0Q8220220202031748912877033
			H10US8X0Q8220220202031758634730033
			H10US8X0Q8220220202031747472617033
			H10US8X0Q8220220316091404889258075
			H10US8X0Q8220220202031749118826033
			H10US8X0Q8220220202031749672778033
			H10US8X0C9220220316091359282329075
			H10US8X0Q8220220202031749305699033
			H10US8X0Q8220220202031749471186033
			H10US8X0C9220220316091353175407075
			H10US8X0Q8220220316091410727657075
			H10US8X0Q8220220202031814375260033
			H10US8X0Q8220220202031753395088033
			H10US8X0Q8220220202031751886760033
			H10US8X0Q8220220316091413516833075
			H10US8X0Q8220220316091425040255075
			H10US8X0Q8220220316091412088175075
			H10US8X0C9220220316091359015027075
			H10US8X0C9220220316091359554427075
			H10US8X0C9220220316091357868971075
			H10US8X0C9220220316091356611958075
			H10US8X0C9220220316091358877787075
			H10US8X0Q8220220202031752457736033
			H10US8X0Q8220220202031759636753033
			H10US8X0Q8220220316091428740847075
			H10US8X0Q8220220316091404446346075
			H10US8X0Q8220220316091406786176075
			H10US8X0Q8220220202031815330592033
			H10US8X0Q8220220202031815643190033
			H10US8X0Q8220220202031813576068033
			H10US8X0Q8220220202031759950850033
			H10US8X0Q8220220316091413226378075
			H10US8X0Q8220220316091402481699075
			H10US8X0Q8220220316091408450283075
			H10US8X0C9220220316091403069791075
			H10US8X0C9220220316091401505613075
			H10US8X0Q8220220202031754436340033
			H10US8X0C9220220316091401637410075
			H10US8X0C9220220316091358302733075
			H10US8X0C9220220316091354892465075
			H10US8X0Q8220220316091402344867075
			H10US8X0Q8220220202031753701581033
			H10US8X0Q8220220202031813420837033
			H10US8X0C9220220316091357172814075
			H10US8X0Q8220220202031814709480033
			H10US8X0Q8220220316091411024605075
			H10US8X0Q8220220202031811258237033
			H10US8X0Q8220220316091411322713075
			H10US8X0Q8220220316091410883684075
			H10US8X0Q8220220316091414351693075
			H10US8X0Q8220220316091429580959075
			H10US8X0Q8220220202031808721848033
			H10US8X0Q8220220202031810263753033
			H10US8X0Q8220220316091407463105075
			H10US8X0Q8220220202031812383184033
			H10US8X0Q8220220316091407329837075
			H10US8X0Q8220220316091407193841075
			H10US8X0Q8220220316091404305939075
			H10US8X0Q8220220202031812689830033
			H10US8X0Q8220220316091429856324075
			H10US8X0Q8220220316091408039659075
			H10US8X0Q8220220316091415465068075
			H10US8X0Q8220220316091425754163075
			H10US8X0Q8220220202031755972655033
			H10US8X0Q8220220316091421475747075
			H10US8X0Q8220220316091421735276075
			H10US8X0Q8220220316091419046046075
			H10US8X0Q8220220316091426478773075
			H10US8X0Q8220220316091422145408075
			H10US8X0Q8220220202031756626089033
			H10US8X0Q8220220202031759267922033
			H10US8X0Q8220220202031758011684033
			H10US8X0Q8220220316091416598437075
			auto copy over (site is up)		Yes	Glenn

			# select * from view_all_productdata where date='2022-08-30' and website='www.lowes.com' AND source = 'auto_copy_over'

			{
			    "response": {
			        "task_uuid": "833e0075-7011-4cef-b127-40dadcb2152b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "5a7c2f03-b016-4c09-af95-e57bdee93e18"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "b565a5f4-7dcf-4897-828c-edb031b3fe24"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "3d682e3a-b4d1-428b-80a6-dc892a261be7"
			    },
			    "version": "1.0"
			}
		
		(R D-2) 9392	2022-08-30	H10	all	lowes.com	
			H10US8X0Q8220220316091407898545075	
			Invalid Deadlink		
			Yes	Glenn

			# select * from view_all_productdata where date='2022-08-30' and website='www.lowes.com' AND item_id in ('H10US8X0Q8220220316091407898545075')

			{
			    "response": {
			        "task_uuid": "d62dc5f6-1d9b-4e82-a7b4-58eeacbe5157"
			    },
			    "version": "1.0"
			}

		(R D-2) 9393	2022-08-30	H10	all	lowes.com	
			H10US8X0Q8220220202031752825772033
			H10US8X0Q8220220316091411473988075
			H10US8X0Q8220220316091425477287075
			H10US8X0Q8220220316091402914185075
			H10US8X0C9220220316091358615527075
			H10US8X0Q8220220316091413379241075
			H10US8X0Q8220220202031805065039033
			H10US8X0Q8220220316091411620342075
			H10US8X0Q8220220316091415330960075
			H10US8X0Q8220220316091414913490075
			No data		
			Yes	Glenn

			# normal run
			{
			    "response": {
			        "task_uuid": "b7665a25-b267-4a91-8316-9812bbc87515"
			    },
			    "version": "1.0"
			}

			# rerun
			{
			    "response": {
			        "task_uuid": "069a4f25-0410-4089-8f52-6f57caecadc7"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "98b00f02-ee68-4460-bda7-c5f0599061d4"
			    },
			    "version": "1.0"
			}

			# aco 1 item_id
			{
			    "response": {
			        "task_uuid": "0950c86a-b995-4bd8-9385-862da83b3feb"
			    },
			    "version": "1.0"
			}

			# H10US8X0Q8220220316091411620342075, H10US8X0Q8220220316091411473988075
			{
			    "response": {
			        "task_uuid": "db725628-2254-4247-a5ec-4cdfcd5c315f"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "837aabb2-2558-446b-a82b-852849937ff2"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "5aa45618-baff-459a-8d9a-7572af5e9672"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "84e5f637-966e-495f-8513-750d8f1c1b1b"
			    },
			    "version": "1.0"
			}

		(R D-1) 9394	2022-08-30	H10	description	www.homedepot.com	
			H10USAX0Q8220220316091347951110075
			H10USAX0Q8220220310035809048045069
			H10USAX0Q8220220125025730664070025
			H10USAX0Q8220220310035824266117069
			H10USAX0Q8220220310035815922759069
			H10USAX0Q8220220310035810716550069
			H10USAX0Q8220220310035809766131069
			H10USAX0Q8220220310035807300491069
			H10USAX0Q8220220310035818748209069
			H10USAX0Q8220220125025712725376025
			H10USAX0Q8220220125025721010583025
			missing description		
			Yes	Charlemagne

		(R D-1) 9395	2022-08-30	H10	specifications	www.homedepot.com	
			H10USAX0Q8220220316091347951110075
			H10USAX0Q8220220310035809048045069
			H10USAX0Q8220220125025730664070025
			H10USAX0Q8220220310035824266117069
			H10USAX0Q8220220310035815922759069
			H10USAX0Q8220220310035810716550069
			H10USAX0Q8220220310035809766131069
			H10USAX0Q8220220310035807300491069
			H10USAX0Q8220220310035818748209069
			H10USAX0Q8220220125025712725376025
			H10USAX0Q8220220125025721010583025
			H10USAX0Q8220220310035823902293069
			H10USAX0Q8220220125025716508107025
			H10USAX0T9220220316091345655215075
			missing specification		
			Yes	Charlemagne

		(R D-1) 9398	2022-08-30	200	all	cdiscount.com/3P	
			200FRMS070000001398230202105040415	
			Invalid deadlink		
			Yes	Mojo

			{
			    "response": {
			        "task_uuid": "6b131eac-51ac-421d-932a-5d8bffc7cdc0"
			    },
			    "version": "1.0"
			}

		---
		(R D-1) 5044	2022-08-30	New CMS	H10	all	lowes.com	
			backflow preventer
			pressure reducing valve 3/4
			water pressure reducing valve
		
			Duplicate data	
			https://prnt.sc/2pBzB2mqnN1e
			https://prnt.sc/cKiShkj6qP8H
			https://prnt.sc/NQmuOu15ZoO6
		
			Yes	KEEN

			{
			    
			    "response": {
			        "task_uuid": "381416ce-f044-46af-8ef1-9e680e26fc4a"
			    },
			    "version": "1.0"
			}

		(R D-1) 5045	2022-08-30	New CMS	H10	all	lowes.com	
			metering faucet

			Data Count Mismatch 22(Fetched Data is Lesser than expected)
			https://prnt.sc/vMFUrUmdNKss
		
			Yes	KEEN

			# SELECT * FROM view_all_rankingsdata var where date='2022-08-30' AND website = 'www.lowes.com' AND keyword in (N'metering faucet')

			{
			    "response": {
			        "task_uuid": "8c29a88c-96df-4fb5-9b45-0f6682438d84"
			    },
			    "version": "1.0"
			}

		---
		(R D-1) 1477	2022-08-30	New CMS	H10	all	www.fastenal.com	
			Hand Dryers	
			mismatched data	
			https://prnt.sc/F7-dj4k0_qTM	
			Yes	Yev

			SELECT * FROM view_all_listings_data WHERE date = '2022-08-30' AND website = 'www.fastenal.com' AND category in ('Hand Dryers')

			{
			    "response": {
			        "task_uuid": "97d0ad43-2392-46af-8343-9b48f9cbe410"
			    },
			    "version": "1.0"
			}

		(R D-1) 1478	2022-08-30	New CMS	H10	all	www.lowes.com	
			flush valve repair kits	
			Auto Copy Over for 1 day	
			https://prnt.sc/32TViduBsxtI	
			Yes	kurt

			SELECT category, litem_id, country, category_url, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-08-30' 
				AND website = 'www.lowes.com' 
				AND category in (N'flush valve repair kits')
				GROUP BY category, litem_id, country, category_url, source

			{
			    "response": {
			        "task_uuid": "002465a3-55da-4b8b-88cf-103a10ef9a23"
			    },
			    "version": "1.0"
			}

		(R D-2) 1479	2022-08-30	New CMS	H10	all	www.lowes.com	
			manual flush valves
			pressure reducing valves
			Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
			https://prnt.sc/r9dH-EAIt4oD
			https://prnt.sc/RTG3-h31EzfR
			Yes	kurt

			# this is wrong category (kept for observation purposes)
			{
			    "response": {
			        "task_uuid": "a37b0920-d9ed-4007-bf56-745927d7f50d"
			    },
			    "version": "1.0"
			}

			# correct -> two categories
			{
			    "response": {
			        "task_uuid": "8f7d41d6-bc98-43ed-8d3d-434e6ddb1c76"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "8185cc55-9dd3-435e-9403-9f01aecec5cc"
			    },
			    "version": "1.0"
			}

	Setup
		RANK- 387	2022-08-29	2022-08-29	Samsung CE (O00)	Invalid deadlink	www.hellotv.nl	
			4K Beamers
			4k televisie
			8k televisie
			Amerikaanse koelkast
			beamer
			Bluetooth soundbars
			clean station
			Dolby Atmos soundbars
			draadloze stofzuiger
			Droger
			jet

			2022-09-02	https://detailsupport.atlassian.net/browse/PRODUCTS-4018 	

			https://prnt.sc/O7UWYu0yNPmF
			https://prnt.sc/KJ2Zn-k6Rxqc
			https://prnt.sc/6YOyRLlw7iuB
			https://prnt.sc/QOOmF2RVqBU7
			https://prnt.sc/4yuKrsfL5HQt
			https://prnt.sc/VqnYwxWRT506
			https://prnt.sc/4WzYeRQPOfOD
			https://prnt.sc/N3QvdF-P9_zt
			https://prnt.sc/1KZ8O-VxWDDN
			https://prnt.sc/BePfv36-tN_G
			https://prnt.sc/_8wiExyp6SlT

		RANK- 388	2022-08-30	2022-08-30	Samsung CE (O00)	Invalid deadlink	www.hellotv.nl	
			4K Beamers
			4k televisie
			8k televisie
			Amerikaanse koelkast
			beamer
			Bluetooth soundbars
			clean station
			Dolby Atmos soundbars
			draadloze stofzuiger
			Droger
			jet
			
			2022-09-02	https://detailsupport.atlassian.net/browse/PRODUCTS-4018 	
			https://prnt.sc/dlnuw1_oNRDH
			https://prnt.sc/FD1YMOaBT9zP
			https://prnt.sc/VmcjLw5ZZuGl
			https://prnt.sc/St-nvHyr_JGQ
			https://prnt.sc/DZPKB2CbnaHY
			https://prnt.sc/zTfHKja9C_c-
			https://prnt.sc/qFLd8689IEjn
			https://prnt.sc/1eri_5qPyzDa
			https://prnt.sc/hrxvh6YL-KxR
			https://prnt.sc/8lxKw8WS00J-
			https://prnt.sc/-IvSJOfbcZlz

08 31 22
	(W) NA region

	Techvalidation Issues:
		7054	08/31/2022	No	K00	in_stock	noon.com/saudi-en/	
			K00SAVX0P1020220613054712069252164	
			-2 in_stock

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00SAVX0P1020220613054712069252164')

		(R D-2) 7055	08/31/2022	No	K00	in_stock,product_website	power.dk	
			K00DKX207U020220627045500448610178
			K00DKX207U020220627045503045272178
			-3 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00DKX207U020220627045500448610178', 'K00DKX207U020220627045503045272178')

		7056	08/31/2022	No	100	in_stock,product_website	
			power.dk	
			100DKX20ZQ020220718044735974914199	
			-3 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('100DKX20ZQ020220718044735974914199')

			{
			    "response": {
			        "task_uuid": "2a5602bc-78d6-4966-b5aa-3913301bf011"
			    },
			    "version": "1.0"
			}

		(R D-2) 7057	08/31/2022	No	K00	product_website	power.no	
			K00NOZ20CG000001467370202107050618	
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00NOZ20CG000001467370202107050618')

		(R D-2) 7058	08/31/2022	No	100	in_stock,product_website	power.se	
			100SEY20ZQ020220124094333462822024	
			-1 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('100SEY20ZQ020220124094333462822024')

		(R D-2) 7059	08/31/2022	No	100	product_website	power.se	
			100SEY20ZQ020220214062235241876045	
			-1 product_website

			{
			    "response": {
			        "task_uuid": "4e25deca-d26f-4d56-a1df-602b14234827"
			    },
			    "version": "1.0"
			}

		(R D-1) 7060	08/31/2022	No	F00	in_stock	proximus.be/nl/	
			F00BEJX0WJ000001634990202111040405	
			-3 in_stock

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('F00BEJX0WJ000001634990202111040405')

		(R D-2) 7061	08/31/2022	Yes	K00	description	altex.ro	
			K00ROKV07U020220328081039402007087
			K00ROKV07U020211115112909461007319
			-1 description

			{
			    "response": {
			        "task_uuid": "7f9905db-ac90-4b2e-9efc-ec3315a83d9b"
			    },
			    "version": "1.0"
			}

			# "Description on image:
			# https://prnt.sc/j1PpkxEd0CTy"

		(R D-2) 7062	08/31/2022	Yes	K00	description	boulanger.com	
			K00FR3103F000001306690202101210448
			K00FR3107U020220620051803162838171
			K00FR3104K000001539610202108110154
			K00FR3107U020220627045506862363178
			K00FR3104K000001431340202106020353
			K00FR3107U020220620051754437294171
			K00FR3104K000001539910202108110154
			K00FR3107U020220627045507744334178
			K00FR3107U020211115034603289196319
			K00FR3107U020220620055708469300171
			K00FR3107U020220627045508004948178
			K00FR3105K000000986680202007060725
			K00FR3107U020220627045505669168178
			K00FR3107U020220620051800259918171
			K00FR3107U020220620051751259712171
			K00FR3107U020220627045504828991178
			K00FR3107U020220627045505971624178
			K00FR3107U020220627045505351219178
			K00FR3107U020220627045503962898178
			K00FR3107U020220627045506306955178
			K00FR3107U020220627045507461442178
			K00FR3107U020220627045508263389178
			K00FR3107U020220627045507149082178
			K00FR3107U020220620051757413639171
			K00FR310P1020220606030400178710157
			K00FR310CG000001525430202108020807
			K00FR3107U020220620055708232043171
			K00FR3107U020220620055708725904171
			K00FR3107U020220627045504281915178
			K00FR310Q1020220606030357517462157
			K00FR3107U020220627045506577627178
			K00FR310QB220220606030356936481157
			K00FR3107U020220627045508501473178
			K00FR3107U020220627045508747216178
			K00FR310SB220220606030400315577157
			K00FR310P1020220606030400049516157
			K00FR310V1020220606030400583689157
			-1 description

			# SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00FR3103F000001306690202101210448', 'K00FR3107U020220620051803162838171', 'K00FR3104K000001539610202108110154', 'K00FR3107U020220627045506862363178', 'K00FR3104K000001431340202106020353', 'K00FR3107U020220620051754437294171', 'K00FR3104K000001539910202108110154', 'K00FR3107U020220627045507744334178', 'K00FR3107U020211115034603289196319', 'K00FR3107U020220620055708469300171', 'K00FR3107U020220627045508004948178', 'K00FR3105K000000986680202007060725', 'K00FR3107U020220627045505669168178', 'K00FR3107U020220620051800259918171', 'K00FR3107U020220620051751259712171', 'K00FR3107U020220627045504828991178', 'K00FR3107U020220627045505971624178', 'K00FR3107U020220627045505351219178', 'K00FR3107U020220627045503962898178', 'K00FR3107U020220627045506306955178', 'K00FR3107U020220627045507461442178', 'K00FR3107U020220627045508263389178', 'K00FR3107U020220627045507149082178', 'K00FR3107U020220620051757413639171', 'K00FR310P1020220606030400178710157', 'K00FR310CG000001525430202108020807', 'K00FR3107U020220620055708232043171', 'K00FR3107U020220620055708725904171', 'K00FR3107U020220627045504281915178', 'K00FR310Q1020220606030357517462157', 'K00FR3107U020220627045506577627178', 'K00FR310QB220220606030356936481157', 'K00FR3107U020220627045508501473178', 'K00FR3107U020220627045508747216178', 'K00FR310SB220220606030400315577157', 'K00FR310P1020220606030400049516157', 'K00FR310V1020220606030400583689157', 'K00FR210CG000001524660202108020807', 'K00FR2107U020220627045504907983178', 'K00FR2107U020220627045505179598178')

			# "Valid no description:
			# https://prnt.sc/YYHeWXrbl_1n"

		(R D-2) 7063	08/31/2022	Yes	200	description	boulanger.com	
			200FR3107R020220711022358070103192
			200FR3107R020220711022357196803192
			200FR3108R020220711022359165038192
			200FR3108R020220711022358767960192
			-1 description

			# "Valid no description:
			# https://prnt.sc/YYHeWXrbl_1n"

		(R D-2) 7064	08/31/2022	Yes	K00	description	euro.com.pl	
			K00PLD807U020220620051803777495171
			K00PLD807U020220620051758004743171
			K00PLD807U020220620051755048477171
			K00PLD807U020220627045519079362178
			K00PLD807U020220620051800848145171
			K00PLD807U020220620055713644623171
			K00PLD80CG000001244620202011170356
			K00PLD807U020220627045518775128178
			K00PLD807U020220620055714358369171
			K00PLD807U020220620055714597096171
			K00PLD807U020220627045519586031178
			-1 description

			# SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00PLD807U020220620051803777495171', 'K00PLD807U020220620051758004743171', 'K00PLD807U020220620051755048477171', 'K00PLD807U020220627045519079362178', 'K00PLD807U020220620051800848145171', 'K00PLD807U020220620055713644623171', 'K00PLD80CG000001244620202011170356', 'K00PLD807U020220627045518775128178', 'K00PLD807U020220620055714358369171', 'K00PLD807U020220620055714597096171', 'K00PLD807U020220627045519586031178')

			{
			    "response": {
			        "task_uuid": "1e140021-a0cd-4ed1-9aee-6ac01cde1145"
			    },
			    "version": "1.0"
			}

			# no description on site:
			# https://prnt.sc/mmIIFoUr7AiW

		(R D-2) 7065	08/31/2022	Yes	K00	description	fnac.com	
			K00FR210CG000001524660202108020807
			K00FR2107U020220627045504907983178
			K00FR2107U020220627045505179598178
			-1 description

			{
			    "response": {
			        "task_uuid": "3fc12916-b402-44b1-bcfb-6143168852e0"
			    },
			    "version": "1.0"
			}

			SELECT * FROM view_all_productdata where date='2022-08-31' AND item_id in ('K00FR210CG000001524660202108020807', 'K00FR2107U020220627045504907983178', 'K00FR2107U020220627045505179598178')

			# no description on site:
			# https://prnt.sc/e-ST2msyminI

		(R D-2) 7066	08/31/2022	Yes	K00	description	jarir.com	
			K00SABA07U020220613054706093683164
			K00SABA03O000001449700202106150452
			K00SABA04K000001449860202106150452
			K00SABA03F000001557090202108190345
			K00SABA03F000001557130202108190345
			K00SABA04K000001449810202106150452
			K00SABA04K000001557060202108190345
			K00SABA04K000001449850202106150452
			K00SABA07U020220613054709160571164
			K00SABA07U020220613054713124311164
			K00SABA07U020220222101531689314053
			K00SABA07U020220613054705083301164
			K00SABA04K000001449870202106150452
			K00SABA04K000001449840202106150452
			K00SABA04K000001557070202108190345
			K00SABA07U020220613054704523163164
			K00SABA0AO000001449800202106150452
			K00SABA07U020220613054720386270164
			K00SABA07U020220613054705811406164
			K00SABA07U020220613054707826164164
			K00SABA04K000001555270202108180340
			K00SABA07U020220222101531927617053
			K00SABA07U020220613054705951163164
			K00SABA07Q000001557160202108190345
			K00SABA07U020220613060909004247164
			K00SABA07U020220613054706239844164
			K00SABA0CG000001301820202101180552
			K00SABA07U020220613054720818471164
			K00SABA07U020220613054707346284164
			K00SABA0MD000001449790202106150452
			K00SABA07U020220613054710044952164
			K00SABA0CG000001492580202107190651
			K00SABA0FO000001450000202106150452
			K00SABA0DO000001449990202106150452
			K00SABA07U020220613054713271395164
			K00SABA07U020220613054715560537164
			K00SABA07U020220613054721442962164
			-1 description

			# sql query script too long

			# site maintenance (https://prnt.sc/8kVvsyT7Dyiy

		(R D-2) 7067	08/31/2022	Yes	K00	description	jarir.com	
			K00SABA07U020220613054720593909164	

			# site maintenance (https://prnt.sc/8kVvsyT7Dyiy)

	Daily Issues:
		(R D-2) 9442	2022-08-31	K00	image_count	www.otto.de	
			K00DE0Z07U020220620051757011061171
			incorrect image count		
			Yes	Charlemagne

			{
			    "response": {
			        "task_uuid": "960eb7a4-afba-4a67-89c3-37428cf0edec"
			    },
			    "version": "1.0"
			}

		(R D-2) 9443	2022-08-31	F00	in_stock	mobiel.nl	
			IS
			F00NLRU0ME020220412075013176243102
			F00NLRU0WJ000001529870202108030256
			F00NLRU0WJ000001529910202108030256
			F00NLRU0WJ000001530200202108030256
			F00NLRU0WJ000001417700202105180354
			F00NLRU05R020220110050215942920010
			F00NLRU05R020220110050218551879010
			F00NLRU0JA000001600140202109280434
			F00NLRU0JA000001600590202109280517

			OOS
			F00NLRU040000001566730202108240539
			wrong stock fetch	
			Yes	Crestine

			{
			    "response": {
			        "task_uuid": "02e2457f-78d3-4926-a8bd-ce531b8a0bd2"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "df11ebcd-b716-4f0d-8cbd-8fe92574577d"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1777d7b4-af5a-43b4-bc2f-3686fdce6ea7"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "e54f9186-f854-45c0-9567-15091b69b7cb"
			    },
			    "version": "1.0"
			}

			# "1 item_id unsuccessful after max rerun attempts:
			# F00NLRU0WJ000001529910202108030256"

		maxreruns (R D-1)  9444	2022-08-31	100	all	elgiganten.dk	
			100DK10010000000813290201908020245
			100DK10010000000813760201908020319
			100DK10010000000814140201908020341
			100DK10010000000816740201908020830
			100DK10010000000818120201908020935
			100DK10010000000818230201908020936
			100DK10010000000819000201908021008
			100DK10010000000821000201908050425
			100DK10010000001222770202010280537
			100DK10010000001222780202010280537
			100DK10010000001222800202010280537
			100DK10010000001222820202010280537
			100DK10010000001222840202010280537
			100DK10010000001248210202011250630
			100DK10010000001248230202011250630
			100DK10010000001248250202011250630
			100DK10010000001248260202011250630
			100DK10010000001248290202011250630
			100DK10010000001248310202011250630
			100DK10010000001248330202011250630
			100DK10010000001248400202011250630
			100DK10010000001266350202012230608
			100DK10010000001324180202102080716
			100DK10010000001324400202102090603
			100DK10010000001324440202102090603
			100DK10010000001324630202102090603
			100DK10010000001326370202102090603
			100DK10010000001326440202102090603
			100DK10010000001326510202102090603
			100DK10010000001327030202102090604
			100DK10010000001397640202105040251
			100DK10010000001397940202105040251
			100DK10010000001400700202105040816
			100DK10010000001400740202105040816
			100DK10010000001400760202105040816
			100DK10010000001400780202105040816
			100DK10010000001413660202105130512
			100DK10010000001439200202106080416
			100DK10010000001659210202111162228
			100DK10010000001659240202111162228
			100DK10010000001659300202111162228
			100DK10010000001659370202111162228
			100DK10010000001659410202111162228
			100DK10010000001659720202111162228
			100DK10010000001660000202111162228
			100DK100ZQ020220124090748845248024
			100DK100ZQ020220124090750254630024
			100DK100ZQ020220124090750451224024
			100DK100ZQ020220124090750648044024
			100DK100ZQ020220124090753325822024
			100DK100ZQ020220124090753449368024
			100DK100ZQ020220124090753577760024
			100DK100ZQ020220124090753733277024
			100DK100ZQ020220124090754027240024
			100DK100ZQ020220124090754197223024
			100DK100ZQ020220124090754523897024
			100DK100ZQ020220124090754819827024
			100DK100ZQ020220124090755315930024
			100DK100ZQ020220124090756117378024
			100DK100ZQ020220124093123601621024
			100DK100ZQ020220124093124884947024
			100DK100ZQ020220124093125206089024
			100DK100ZQ020220124093125637325024
			100DK100ZQ020220124093126046040024
			100DK100ZQ020220124093126211631024
			100DK100ZQ020220124093132905871024
			100DK100ZQ020220124093133398639024
			100DK100ZQ020220124093144653882024
			100DK100ZQ020220124093145806437024
			100DK100ZQ020220124093146232901024
			100DK100ZQ020220124093147433839024
			100DK100ZQ020220124093148226755024
			100DK100ZQ020220124093149795094024
			100DK100ZQ020220207085518362413038
			100DK100ZQ020220214062204402797045
			100DK100ZQ020220214062205360488045
			100DK100ZQ020220502042253140705122
			100DK100ZQ020220502042253596723122
			100DK100ZQ020220502042253824315122
			100DK100ZQ020220516035925572818136
			100DK100ZQ020220718044721849801199
			100DK100ZQ020220718044734976587199
			100DK100ZQ020220718044735100407199
			100DK100ZQ020220718044735293521199
			100DK100ZQ020220718044735921349199
			100DK100ZQ020220718044736316467199
			100DK100ZQ020220718044737125708199
			100DK100ZQ020220718044737512761199
			100DK100ZQ020220718044737879175199
			100DK100ZQ020220718044738079042199
			100DK100ZQ020220718044738269101199
			invalid deadlinks		
			Yes	Turki

			# sql query script too long

			{
			    "response": {
			        "task_uuid": "3e642a0c-b119-446e-895c-61bc5d56111c"
			    },
			    "version": "1.0"
			}

		maxreruns (R D-1) 9445	2022-08-31	100	all	power.dk	
			100DKX2010000000816780201908020830
			100DKX2010000000817030201908020841
			100DKX2010000000818270201908020936
			100DKX2010000000858110201910100845
			100DKX2010000000931020202004210311
			100DKX2010000000931460202004210835
			100DKX2010000001227760202010290654
			100DKX2010000001227770202010290654
			100DKX2010000001227780202010290654
			100DKX2010000001227790202010290654
			100DKX2010000001227800202010290654
			100DKX2010000001227810202010290654
			100DKX2010000001227820202010290654
			100DKX2010000001227830202010290654
			100DKX2010000001227840202010290654
			100DKX2010000001250080202011252144
			100DKX2010000001250110202011252144
			100DKX2010000001250140202011252144
			100DKX2010000001250220202011252149
			100DKX2010000001266360202012230608
			100DKX2010000001324330202102090603
			100DKX2010000001324420202102090603
			100DKX2010000001324640202102090603
			100DKX2010000001326230202102090603
			100DKX2010000001326280202102090603
			100DKX2010000001326390202102090603
			100DKX2010000001326530202102090603
			100DKX2010000001326680202102090604
			100DKX2010000001326830202102090604
			100DKX2010000001326870202102090604
			100DKX2010000001397500202105040251
			100DKX2010000001397650202105040251
			100DKX2010000001400720202105040816
			100DKX2010000001400750202105040816
			100DKX2010000001400770202105040816
			100DKX2010000001400790202105040816
			100DKX2010000001403340202105050413
			100DKX2010000001413670202105130512
			100DKX2010000001439180202106080416
			100DKX2010000001659200202111162228
			100DKX2010000001659220202111162228
			100DKX2010000001659230202111162228
			100DKX2010000001659330202111162228
			100DKX2010000001659350202111162228
			100DKX2010000001659360202111162228
			100DKX2010000001659740202111162228
			100DKX2010000001659850202111162228
			100DKX2010000001659920202111162228
			100DKX2010000001660030202111162228
			100DKX2010000001668540202111181517
			100DKX2010000001668580202111181517
			100DKX2010000001668620202111181517
			100DKX20ZQ020220124090748959640024
			100DKX20ZQ020220124090756200028024
			100DKX20ZQ020220124090756487526024
			100DKX20ZQ020220124093123792421024
			100DKX20ZQ020220124093124056727024
			100DKX20ZQ020220124093124944698024
			100DKX20ZQ020220124093125382317024
			100DKX20ZQ020220124093125880108024
			100DKX20ZQ020220124093127745593024
			100DKX20ZQ020220124093132031030024
			100DKX20ZQ020220124093133518512024
			100DKX20ZQ020220124093144808352024
			100DKX20ZQ020220124093145979751024
			100DKX20ZQ020220124093147217825024
			100DKX20ZQ020220124093147587317024
			100DKX20ZQ020220124093147972884024
			100DKX20ZQ020220124093148372703024
			100DKX20ZQ020220124093148665486024
			100DKX20ZQ020220124093148825273024
			100DKX20ZQ020220124093149880606024
			100DKX20ZQ020220124093150826547024
			100DKX20ZQ020220207085518450975038
			100DKX20ZQ020220214062204491217045
			100DKX20ZQ020220214062205420613045
			100DKX20ZQ020220214062206354627045
			100DKX20ZQ020220214062210708504045
			100DKX20ZQ020220214062211619886045
			100DKX20ZQ020220502042253224458122
			100DKX20ZQ020220502042253680896122
			100DKX20ZQ020220516035925624164136
			100DKX20ZQ020220516035925874359136
			100DKX20ZQ020220718044718678436199
			100DKX20ZQ020220718044718860221199
			100DKX20ZQ020220718044719031970199
			100DKX20ZQ020220718044719204151199
			100DKX20ZQ020220718044735370842199
			100DKX20ZQ020220718044735777227199
			100DKX20ZQ020220718044736147659199
			100DKX20ZQ020220718044736557710199
			100DKX20ZQ020220718044737362259199
			100DKX20ZQ020220718044737568139199
			100DKX20ZQ020220718044737740936199
			100DKX20ZQ020220718044737932056199
			100DKX20ZQ020220718044738326062199
			100DKX20ZQ020220718044741713319199
			100DKX20ZQ020220718044742165143199
			100DKX20ZQ020220718044742479991199
			100DKX20ZQ020220718044742624310199
			100DKX20ZQ020220718044742776079199
			auto copy over (site is up)		
			Yes	Turki

			{
			    "response": {
			        "task_uuid": "40d7744f-8bbf-4d31-85b1-b4c9e53a0868"
			    },
			    "version": "1.0"
			}

	Special A00 run:
		(R D-2) Request 1:
			Hello,
			Request to rerun mediamarkt.de

			DB: A00
			Retailer: mediamarkt.de
			Reason: Invalid deadlink

			https://prnt.sc/PSgJC__5bWiT

			Item ID: (please see attached file)
			{
			    "response": {
			        "task_uuid": "5cd7dffe-868e-4562-96a9-825c1c88a6d5"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "d11943d3-45f8-48c3-9c49-253f46fd7ff3"
			    },
			    "version": "1.0"
			}

		(R D-2) Request 2:
			Hello,

			Request to rerun amazon.it

			DB: A00
			Retailer: amazon.it
			Reason: Invalid deadlink

			Item ID: All items

			https://prnt.sc/BeuwF2phN2gA

			{
			    "response": {
			        "task_uuid": "698ad16d-021a-4c81-9c02-823e5906b1cd"
			    },
			    "version": "1.0"
			}

	SQL update
		(SP D-2) Request 1:
			Hello,
			Request to set deadlink in microsoft.com/it-it

			DB:A00
			Retailer: microsoft.com/it-it
			Reason: Affected item id''s should tag deadlink.
			https://prnt.sc/EwGScAiioH9O

			Item ID:
			A00ITN40RB000000995220202008040330
			A00ITN40RB000000995240202008040330
			A00ITN40RB000000995250202008040330
			A00ITN40RB000000995290202008040330
			A00ITN40RB000000995260202008040330
			A00ITN40RB000000995270202008040330
			A00ITN40RB000000995280202008040330
			A00ITN40RB000000995210202008040330
			A00ITN40RB000001237680202011060527
			A00ITN40RB000001237690202011060527
			A00ITN40RB000000995230202008040330

			# exec (product > set_to_dead_link > prod_set_to_dead_link_by_date+item_id.sql)

		Request 2:
			Hello,
			Request to remove duplicate item id in MS LATAM Data

			DB: 510
			Date: 2022-08-31
			Table: view_all_productdata
			Reason: Duplicate Item
			Item ID''s:
			510MXVC0RB000001569730202108300915

	(T D-5) TechVal Reporting
		August 31, 2022 Technical Validation Updates

		# Good morning everyone, for today's techvalidation update

		Numbers of issues last update: 23
		Numbers of issues today: 32

		# So, currently we have no retailers that fixed by rerun

		Fixed by Rerun:
		None

		# So, were proceeding to the next time item which are the retailers that was fixed by code update
	
		Fixed by Code Update:
		Razer K00: # and we have for 
		www.virginmegastore.ae | -1 description # with

		# so proceeding to the last item. It is the work in progress

		Work in progress:
		Zound 200: # proceeding to zound
		www.boulanger.com | -1 description # for both retailers
		www.mediamarkt.be/fr | -1 description

		Samsung Mobile F00: # procceding to samsung mobile
		www.base.be/nl | -1 product_website # we have

		Razer K00: # lastly for 
		www.power.dk | -3 in_stock,-1 product_website # we have
		www.power.no | -1 product_website # next is
		www.altex.ro | -1 description # lastly
		www.euro.com.pl | -1 description
		www.fnac.com | -1 description


09 01 22
	Daily Issues:
		(R D-2) 9475	2022-09-01	F00	all	fnac.be/nl	
			F00BE9B040000001658350202111152217
			F00BE9B040000001658360202111152217
			F00BE9B0EL000001658370202111152217
			F00BE9B0EL000001658390202111152217
			F00BE9B0EL000001658400202111152217
			F00BE9B040000001658440202111152217
			F00BE9B040000001623830202110250510
			F00BE9B040000001624060202110250510
			F00BE9B040000001625280202110250511
			F00BE9B0EL000001625330202110250511
			F00BE9B0EL000001625380202110250511
			F00BE9B0JA000001627420202110250541
			F00BE9B040000001627630202110250541
			F00BE9B040000001627710202110250541
			F00BE9B040000001627950202110250541
			F00BE9B040000001627970202110250541
			F00BE9B040000001628000202110250541
			F00BE9B040000001628050202110250541
			F00BE9B040000001628110202110250541
			F00BE9B040000001628150202110250541
			F00BE9B0EL000001628390202110250541
			F00BE9B0EL000001628420202110250541
			F00BE9B0EL000001628450202110250541
			F00BE9B0EL000001628540202110250541
			F00BE9B0EL000001628570202110250541
			F00BE9B0EL000001628590202110250541
			F00BE9B0EL000001628610202110250541
			F00BE9B0EL000001628640202110250541
			F00BE9B0EL000001628670202110250541
			F00BE9B0JA000001629250202110250541
			F00BE9B040000001629460202110250541
			F00BE9B0EL000001629520202110250541
			F00BE9B0EL000001629530202110250541
			F00BE9B0EL000001629540202110250541
			F00BE9B0EL000001629580202110250541
			F00BE9B0WJ000001641690202111040958
			F00BE9B0WJ000001641720202111040958
			F00BE9B040000001641820202111040958
			F00BE9B0EL000001641870202111040958
			F00BE9B0EL000001641900202111040958
			F00BE9B040000001642050202111040958
			F00BE9B040000001642620202111040959
			F00BE9B05R020211207020255242346341
			F00BE9B05R020211207020257008212341
			F00BE9B05R020220103055430081548003
			F00BE9B05R020220103055430530941003
			F00BE9B05R020220110050219429186010
			F00BE9B05R020220110050220214543010
			F00BE9B05R020220214072831672516045
			F00BE9B05R020220214072836155837045
			F00BE9B05R020220214072838574499045
			F00BE9B05R020220214072839671257045
			F00BE9B05R020220214072840751886045
			F00BE9B05R020220214072842740424045
			F00BE9B05R020220214072843809028045
			F00BE9B05R020220214072846754515045
			F00BE9B05R020220214072849901892045
			F00BE9B05R020220214072851189687045
			F00BE9B05R020220214072853538922045
			F00BE9B05R020220214072855979753045
			F00BE9B05R020220214072905407044045
			F00BE9B0UD020220314065750981577073
			F00BE9B05R020220329062752331704088
			F00BE9B0ME020220412075009379254102
			F00BE9B0ME020220412075010825939102
			F00BE9B0ME020220412075011280509102
			F00BE9B0ME020220412075012068399102
			F00BE9B05R020220412075023097362102
			F00BE9B05R020220412075024528152102
			F00BE9B05R020220412075026251051102
			F00BE9B05R020220412075026829257102
			F00BE9B05R020220412075028571182102
			F00BE9B05R020220412075030175907102
			F00BE9B05R020220412075030844847102
			F00BE9B05R020220412075031447239102
			F00BE9B05R020220412075033269406102
			F00BE9B05R020220412075039751259102
			F00BE9B05R020220412075040244303102
			F00BE9B05R020220412075040955692102
			F00BE9B05R020220412075041328048102
			F00BE9B05R020220412075041678799102
			F00BE9B05R020220412075042107707102
			F00BE9B05R020220412082123591690102
			F00BE9B05R020220412082127110359102
			F00BE9B05R020220412082128844051102
			F00BE9B05R020220412082131219826102
			F00BE9B05R020220412082132956838102
			F00BE9B05R020220412082133833396102
			F00BE9B05R020220412082137247136102
			
			auto copy over (site is up)		
			Yes	Glenn

			# sql script too long

			{
			    "response": {
			        "task_uuid": "2b247157-4715-44ab-a708-b89b1b398688"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "caa7c03b-0a8c-482c-9a73-efc62d1a7a1f"
			    },
			    "version": "1.0"
			}

		(F D-1) (R D-2) 9476	2022-09-01	O00	all	hellotv.nl	
			all items	
			auto copy over (site is up)		
			Yes	Have

			# SELECT * FROM view_all_productdata WHERE date='2022-09-01' AND website = 'www.hellotv.nl' AND source = 'auto_copy_over'

			{
			    "response": {
			        "task_uuid": "f735dbd9-14ed-4998-9e8e-5715d5eb60ca"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "f08ba033-6c76-4988-a3a7-009b8870fd5c"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "fda99c3e-f15a-4496-93ec-3621028d4c5e"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "d24a6eae-cefa-4ced-91ca-28517dfe6010"
			    },
			    "version": "1.0"
			}

		(R D-1) 9477	2022-09-01	O00	all	mediamarkt.be/nl/	
			O00BE2D03R020220614132421590152165	
			invalid deadlink		
			Yes	Have

			{
			    "response": {
			        "task_uuid": "0ba10468-eed9-4d75-8cf7-7cd157f48fac"
			    },
			    "version": "1.0"
			}

		(R D-2) 9478	2022-09-01	O00	all	mediamarkt.be/nl/	
			O00BE2D03R020220504022406326852124
			O00BE2D03R020220516032835918245136
			O00BE2D040000001216770202010090739
			O00BE2D040000001252500202012010444
			O00BE2D040000001210280202009290256
			O00BE2D040000001216760202010090739
			O00BE2D040000001216750202010090739
			O00BE2D040000001218790202010210150
			O00BE2D040000001216740202010090739
			O00BE2D040000001218850202010210155
			O00BE2D040000001218610202010201018
			O00BE2D040000001233600202011030654
			O00BE2D040000001218660202010201018
			O00BE2D040000001375140202104120812
			O00BE2D03R020220516032832803163136
			O00BE2D040000001375320202104120812
			O00BE2D03R020220516032832668560136
			O00BE2D040000001375500202104120812
			O00BE2D03R020220516032832534781136
			O00BE2D03R020220516032832388654136
			O00BE2D040000001375880202104120812
			O00BE2D040000001360300202103300834
			O00BE2D040000001376070202104120812
			O00BE2D040000001218650202010201018
			O00BE2D040000001210310202009290259
			O00BE2D040000001210330202009290301
			O00BE2D040000001218770202010210149
			O00BE2D040000001218760202010210148
			O00BE2D03R020220421094034291083111
			O00BE2D03R020220421094043160224111
			O00BE2D03R020220421094033445438111
			O00BE2D03R020220421094035595723111
			O00BE2D03R020220421094042960979111
			O00BE2D03R020220421094037061728111
			O00BE2D03R020220421094039479629111
			O00BE2D03R020220421094039187958111
			O00BE2D03R020220421094052849663111
			O00BE2D03R020220516032833956406136
			O00BE2D03R020220516032833801855136
			O00BE2D03R020220516032833663393136
			O00BE2D03R020220614132420021316165
			O00BE2D03R020220516032831921397136
			O00BE2D03R020220516032833531041136
			O00BE2D03R020220516032833223102136
			O00BE2D03R020220516032833077773136
			O00BE2D040000001233580202011030652
			O00BE2D040000001242170202011130413
			O00BE2D040000001233490202011030642
			O00BE2D040000001338590202102260339
			O00BE2D040000001339260202103020232
			O00BE2D040000001387820202104210357
			O00BE2D03R020220516032834400808136
			O00BE2D040000001473970202107060502
			O00BE2D03R020220516032835152052136
			O00BE2D03R020220516032834844442136
			O00BE2D03R020220516032834998708136
			O00BE2D03R020220614132421462933165
			O00BE2D03R020220614132421168211165
			O00BE2D03R020220516032835414249136
			auto copy over (site is up)		
			Yes	Have

			{
			    "response": {
			        "task_uuid": "490b741a-cc11-4863-ac12-42d61d191a25"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "ff280426-08d8-416c-80fd-dd352dee8f73"
			    },
			    "version": "1.0"
			}

		(R D-2) 9479	2022-09-01	100	all	gigantti.fi	
			100FI60010000001328650202102100700
			100FI60010000001251290202011260422
			100FI60010000001439210202106080416
			100FI60010000001223920202010280904
			100FI60010000000904600202001280905
		
			ETL ISSUE in price, NO DQ		
			Yes	JIng

			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('100FI60010000001328650202102100700', '100FI60010000001251290202011260422', '100FI60010000001439210202106080416', '100FI60010000001223920202010280904', '100FI60010000000904600202001280905')

			{
			    "response": {
			        "task_uuid": "083e0cda-dffa-4395-a0cc-2e26deda1315"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "85e941d7-3d41-4599-8842-710581da7ad3"
			    },
			    "version": "1.0"
			}

		(R D-2) 9480	2022-09-01	100	all	golfsky.fi/fi	
			100FI331ZQ020220725025259316703206	
			SPIDER ISSUE		
			Yes	JIng

			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('100FI331ZQ020220725025259316703206')

			{
			    "response": {
			        "task_uuid": "d1ff65e8-73a9-465a-bc37-47ebba72262f"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "4b9e4dca-712b-4e08-8373-6d2470ac55e8"
			    },
			    "version": "1.0"
			}

		(R D-2) 9481	2022-09-01	O00	all	mediamarkt.nl	
			O00NLQ103R020220421094050194175111
			O00NLQ103R020220421094049948157111
			O00NLQ103R020220504022406418753124
			O00NLQ103R020220421094049476751111
			O00NLQ103R020220421094051298948111
			O00NLQ103R020220421094050743891111
			O00NLQ103R020220421094050426603111
			O00NLQ1040000001255590202012090939
			O00NLQ1040000001218930202010210159
			O00NLQ1040000001253550202012031200
			O00NLQ1040000001248150202011250417
			O00NLQ1040000001216680202010090731
			O00NLQ1040000001248100202011250417
			O00NLQ1040000001218910202010210158
			O00NLQ1040000001218900202010210158
			O00NLQ1040000001218590202010201009
			O00NLQ1040000001218880202010210157
			O00NLQ1040000001218870202010210156
			O00NLQ1040000001218860202010210155
			O00NLQ1040000001216630202010090731
			O00NLQ1040000001233440202011030637
			O00NLQ1040000001233520202011030644
			O00NLQ1040000001564440202108230711
			O00NLQ1040000001564420202108230711
			O00NLQ1040000001564450202108230711
			O00NLQ1040000001564470202108230711
			O00NLQ1040000001564430202108230711
			O00NLQ1040000001564390202108230711
			O00NLQ1040000001564380202108230711
			O00NLQ1040000001564400202108230711
			O00NLQ1040000001564500202108230711
			O00NLQ1040000001564510202108230711
			O00NLQ1040000001564490202108230711
			O00NLQ1040000001377820202104130328
			O00NLQ103R020220421094037943562111
			O00NLQ1040000001377870202104130328
			O00NLQ103R020220421094037803637111
			O00NLQ1040000001377900202104130328
			O00NLQ103R020220421094037662067111
			O00NLQ103R020220421094037535379111
			O00NLQ1040000001375730202104120812
			O00NLQ103R020220421094037258449111
			O00NLQ1040000001377950202104130328
			O00NLQ103R020220421094033708952111
			O00NLQ103R020220421094034556432111
			O00NLQ1040000001233530202011030645
			O00NLQ1040000001218580202010201009
			O00NLQ1040000001216660202010090731
			O00NLQ1040000001216650202010090731
			O00NLQ1040000001233540202011030646
			O00NLQ1040000001474000202107060502
			O00NLQ1040000001374070202104060658
			O00NLQ1040000001374030202104060658
			O00NLQ1040000001216710202010090731
			O00NLQ1040000001233620202011030655
			O00NLQ1040000001242000202011121227
			O00NLQ1040000001233560202011030647
			O00NLQ1040000001216700202010090731
			O00NLQ1040000001218920202010210159
			O00NLQ1040000001218890202010210158
			O00NLQ1040000001218890202010210158
			O00NLQ1040000001454420202106210718
			O00NLQ1040000001454510202106210718
			O00NLQ1040000001233610202011030654
			O00NLQ103R020220421094040884325111
			O00NLQ103R020220421094034348770111
			O00NLQ103R020220421094033986279111
			O00NLQ103R020220421094035897193111
			O00NLQ103R020220421094043219199111
			O00NLQ103R020220421094033527206111
			O00NLQ103R020220421094032227036111
			O00NLQ103R020220421094037125848111
			O00NLQ103R020220421094040621735111
			O00NLQ103R020220421094039827606111
			O00NLQ103R020220421094033119288111
			O00NLQ103R020220421094039533790111
			O00NLQ103R020220421094039239324111
			O00NLQ103R020220421094038618594111
			O00NLQ103R020220421094038939306111
			O00NLQ103R020220504022404476492124
			O00NLQ103R020220504022406080703124
			O00NLQ103R020220421094041844612111
			O00NLQ103R020220421094038084917111
			O00NLQ103R020220421094036279263111
			O00NLQ103R020220421094041565879111
			O00NLQ103R020220421094040359686111
			O00NLQ103R020220421094041435819111
			O00NLQ103R020220421094041024249111
			O00NLQ103R020220421094036570873111
			O00NLQ1040000001241960202011121227
			O00NLQ1040000001233420202011030616
			O00NLQ1040000001233460202011030641
			O00NLQ1040000001241980202011121227
			O00NLQ1040000001233550202011030646
			O00NLQ1040000001338520202102260243
			O00NLQ1040000001374650202104071118
			O00NLQ1040000001374660202104071118
			O00NLQ1040000001374670202104071118
			O00NLQ1040000001425450202105311000
			O00NLQ103R020220421094042829419111
			O00NLQ103R020220421094042575820111
			O00NLQ103R020220421094035339193111
			O00NLQ103R020220421094035093042111
			O00NLQ103R020220421094034797984111
			O00NLQ103R020220421094048438346111
			O00NLQ103R020220421094047161134111
			O00NLQ103R020220421094047501394111
			O00NLQ103R020220421094052288577111
			O00NLQ103R020220421094049256531111
			auto copy over (site is up)		
			Yes	Have

			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('O00NLQ103R020220421094050194175111', 'O00NLQ103R020220421094049948157111', 'O00NLQ103R020220504022406418753124', 'O00NLQ103R020220421094049476751111', 'O00NLQ103R020220421094051298948111', 'O00NLQ103R020220421094050743891111', 'O00NLQ103R020220421094050426603111', 'O00NLQ1040000001255590202012090939', 'O00NLQ1040000001218930202010210159', 'O00NLQ1040000001253550202012031200', 'O00NLQ1040000001248150202011250417', 'O00NLQ1040000001216680202010090731', 'O00NLQ1040000001248100202011250417', 'O00NLQ1040000001218910202010210158', 'O00NLQ1040000001218900202010210158', 'O00NLQ1040000001218590202010201009', 'O00NLQ1040000001218880202010210157', 'O00NLQ1040000001218870202010210156', 'O00NLQ1040000001218860202010210155', 'O00NLQ1040000001216630202010090731', 'O00NLQ1040000001233440202011030637', 'O00NLQ1040000001233520202011030644', 'O00NLQ1040000001564440202108230711', 'O00NLQ1040000001564420202108230711', 'O00NLQ1040000001564450202108230711', 'O00NLQ1040000001564470202108230711', 'O00NLQ1040000001564430202108230711', 'O00NLQ1040000001564390202108230711', 'O00NLQ1040000001564380202108230711', 'O00NLQ1040000001564400202108230711', 'O00NLQ1040000001564500202108230711', 'O00NLQ1040000001564510202108230711', 'O00NLQ1040000001564490202108230711', 'O00NLQ1040000001377820202104130328', 'O00NLQ103R020220421094037943562111', 'O00NLQ1040000001377870202104130328', 'O00NLQ103R020220421094037803637111', 'O00NLQ1040000001377900202104130328', 'O00NLQ103R020220421094037662067111', 'O00NLQ103R020220421094037535379111', 'O00NLQ1040000001375730202104120812', 'O00NLQ103R020220421094037258449111', 'O00NLQ1040000001377950202104130328', 'O00NLQ103R020220421094033708952111', 'O00NLQ103R020220421094034556432111', 'O00NLQ1040000001233530202011030645', 'O00NLQ1040000001218580202010201009', 'O00NLQ1040000001216660202010090731', 'O00NLQ1040000001216650202010090731', 'O00NLQ1040000001233540202011030646', 'O00NLQ1040000001474000202107060502', 'O00NLQ1040000001374070202104060658', 'O00NLQ1040000001374030202104060658', 'O00NLQ1040000001216710202010090731', 'O00NLQ1040000001233620202011030655', 'O00NLQ1040000001242000202011121227', 'O00NLQ1040000001233560202011030647', 'O00NLQ1040000001216700202010090731', 'O00NLQ1040000001218920202010210159', 'O00NLQ1040000001218890202010210158', 'O00NLQ1040000001218890202010210158', 'O00NLQ1040000001454420202106210718', 'O00NLQ1040000001454510202106210718', 'O00NLQ1040000001233610202011030654', 'O00NLQ103R020220421094040884325111', 'O00NLQ103R020220421094034348770111', 'O00NLQ103R020220421094033986279111', 'O00NLQ103R020220421094035897193111', 'O00NLQ103R020220421094043219199111', 'O00NLQ103R020220421094033527206111', 'O00NLQ103R020220421094032227036111', 'O00NLQ103R020220421094037125848111', 'O00NLQ103R020220421094040621735111', 'O00NLQ103R020220421094039827606111', 'O00NLQ103R020220421094033119288111', 'O00NLQ103R020220421094039533790111', 'O00NLQ103R020220421094039239324111', 'O00NLQ103R020220421094038618594111', 'O00NLQ103R020220421094038939306111', 'O00NLQ103R020220504022404476492124', 'O00NLQ103R020220504022406080703124', 'O00NLQ103R020220421094041844612111', 'O00NLQ103R020220421094038084917111', 'O00NLQ103R020220421094036279263111', 'O00NLQ103R020220421094041565879111', 'O00NLQ103R020220421094040359686111', 'O00NLQ103R020220421094041435819111', 'O00NLQ103R020220421094041024249111', 'O00NLQ103R020220421094036570873111', 'O00NLQ1040000001241960202011121227', 'O00NLQ1040000001233420202011030616', 'O00NLQ1040000001233460202011030641', 'O00NLQ1040000001241980202011121227', 'O00NLQ1040000001233550202011030646', 'O00NLQ1040000001338520202102260243', 'O00NLQ1040000001374650202104071118', 'O00NLQ1040000001374660202104071118', 'O00NLQ1040000001374670202104071118', 'O00NLQ1040000001425450202105311000', 'O00NLQ103R020220421094042829419111', 'O00NLQ103R020220421094042575820111', 'O00NLQ103R020220421094035339193111', 'O00NLQ103R020220421094035093042111', 'O00NLQ103R020220421094034797984111', 'O00NLQ103R020220421094048438346111', 'O00NLQ103R020220421094047161134111', 'O00NLQ103R020220421094047501394111', 'O00NLQ103R020220421094052288577111', 'O00NLQ103R020220421094049256531111')

			{
			    "response": {
			        "task_uuid": "ccf7900c-8361-441a-94c5-4db83d72f72e"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "39aef080-d57b-4e63-8595-098ba9e99814"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "06fc34e2-5574-4e06-875e-e852f10c9b28"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "8f4f3257-bed6-4745-8be7-cb5cc40194e1"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "12cb2319-9d75-4923-816c-862634c1a94d"
			    },
			    "version": "1.0"
			}

		(R D-2) 9482	2022-09-01	O00	description	vandenborre.be/nl	
			O00BEMT03R020220404073829239333094
			O00BEMT03R020220404073829562085094
			O00BEMT03R020220404073829987165094
			O00BEMT03R020220404073830380218094
			O00BEMT040000001343180202103100453
			O00BEMT040000001343230202103100453
			O00BEMT040000001343320202103100453
			O00BEMT040000001374210202104060658
			Missing description	
			https://prnt.sc/pnde-9-tVAJe	
			Yes	Jahar

			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('O00BEMT03R020220404073829239333094', 'O00BEMT03R020220404073829562085094', 'O00BEMT03R020220404073829987165094', 'O00BEMT03R020220404073830380218094', 'O00BEMT040000001343180202103100453', 'O00BEMT040000001343230202103100453', 'O00BEMT040000001343320202103100453', 'O00BEMT040000001374210202104060658')

			{
			    "response": {
			        "task_uuid": "2446da87-3755-47e8-b6b3-4bd5e3ed20e4"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "def26870-491b-4656-a834-655d4171b204"
			    },
			    "version": "1.0"
			}

		(R D-1) 9483	2022-09-01	O00	specifications	vandenborre.be/nl	
			O00BEMT03R020220421094048810202111
			O00BEMT040000001338100202102220913
			O00BEMT040000001343370202103100453
			Missing specifications	
			https://prnt.sc/zUcYLOC-85Pp	
			Yes	Jahar


			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('O00BEMT03R020220421094048810202111', 'O00BEMT040000001338100202102220913', 'O00BEMT040000001343370202103100453')

			{
			    "response": {
			        "task_uuid": "e47360c6-cce0-4f8f-9a30-1e5c5bdd30e2"
			    },
			    "version": "1.0"
			}

		(R D-1) 9484	2022-09-01	O00	all	mediamarkt.be/nl/	
			O00BE2D03R020220614132421590152165	
			Invalid Deadlink	
			https://prnt.sc/t4N3PwpbDm8s	
			Yes	Jahar

			SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('O00BE2D03R020220614132421590152165')

			{
			    "response": {
			        "task_uuid": "155bfc6b-3125-418a-a623-2a05d38c8e76"
			    },
			    "version": "1.0"
			}

		(R D-2) 9485	2022-09-01	200	all	elgiganten.dk	
			200DK100Q0000000032320201901180326	
			invalid deadlink		
			Yes	Mojo

			# SELECT * FROM view_all_productdata where date='2022-09-01' AND item_id in ('200DK100Q0000000032320201901180326')


			{
			    "response": {
			        "task_uuid": "e5f4b28a-6c07-4d17-9c75-a1e691273303"
			    },
			    "version": "1.0"
			}
			{
			    "response": {
			        "task_uuid": "088e512d-caf5-4ad9-a6ca-404d19a054b5"
			    },
			    "version": "1.0"
			}

		fr_trello (R D-1) 9490	2022-09-01	U00	all	25best.com.br	
			ALL item_ids	
			invalid -1 values in all fields and off visibility		
			Yes	Caesa

		fr_trello () 9491	2022-09-01	H10	all	lowes.ccom	
			select * from view_all_productdata where date='2022-09-01' and website='www.lowes.com' 
			and source='auto_copy_over' order by item_id
				ACO (Site is up)		
			Yes	Mark

			{
			    "response": {
			        "task_uuid": "a0aa507f-dd34-40e7-8c73-97f52c51efda"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "32f35bf1-17b8-4259-9806-217e727fe371"
			    },
			    "version": "1.0"
			}

		---

		(R D-1) 5074	2022-09-01	New CMS	200	all	
			www.bol.com
			écouteur filaire

			mismatch data count 1 (fetched data is lesser than expected)	
			https://prnt.sc/H-FrWjLDn-FG	
			Yes	Rayyan

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-09-01' 
				AND website = 'www.bol.com' 
				AND keyword in (N'écouteur filaire')
				GROUP BY keyword, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "f14c3c53-8e53-4e08-adc2-90ea5e8ce991"
			    },
			    "version": "1.0"
			}

		(R D-1) 5075	2022-09-01	New CMS	F00	all	www.bol.com	
			'MOBIEL KOPEN',
			'SMARTPHONE KOPEN',
			'TELEFOON KOPEN'

			1day Auto copy over
			2days Auto copy over

			https://prnt.sc/AG7gSfefiv0v
			https://prnt.sc/B27Bw9yu73x8
			https://prnt.sc/ESgnV1CtvJq6
			Yes	Angel

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-09-01' 
				AND website = 'www.bol.com	' 
				AND keyword in (N'MOBIEL KOPEN',
					'SMARTPHONE KOPEN',
					'TELEFOON KOPEN')
				GROUP BY keyword, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "47dea40e-1fdf-4c0b-8c90-ed682e3d7dc4"
			    },
			    "version": "1.0"
			}

		(R D-2) 5076	2022-09-01	New CMS	F00	all	www.mediamarkt.nl	
			'BESTE SMARTPHONE', 25
			'MOBIEL KOPEN', 25 
			'S21 Plus', 25
			'S21 Ultra', 19
			'SMARTPHONE KOPEN' 25

			Data Count Mismatch 19 (Fetched Data is Lesser than expected) 
			Data Count Mismatch 19 (Fetched Data is Lesser than expected) 
			Data Count Mismatch 7 (Fetched Data is Lesser than expected) 
			Data Count Mismatch 1 (Fetched Data is Lesser than expected) 
			Data Count Mismatch 19 (Fetched Data is Lesser than expected) 

			https://prnt.sc/84Izmpu3-5dx
			https://prnt.sc/oXNbZMXLMcr6
			https://prnt.sc/OwL2P2mdSGoA
			https://prnt.sc/SNRapxBQeq2z
			https://prnt.sc/A44rmkRRxYds
			Yes	Angel

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2022-09-01' 
				AND website = 'www.mediamarkt.nl' 
				AND keyword in (N'BESTE SMARTPHONE',
					'MOBIEL KOPEN',
					'S21 Plus',
					'S21 Ultra',
					'SMARTPHONE KOPEN'
				)
				GROUP BY keyword, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "8cceb3fb-acbe-40fd-874d-bf23c6300a84"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "e482325e-e252-48b9-98f6-8b571e48f054"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "f1ef38c0-c487-45d7-88e3-24fda2e40b7c"
			    },
			    "version": "1.0"
			}

		fr_rebuild (R D-1) 5079	2022-09-01	New CMS	O00	all	www.mediamarkt.nl	
			Droger
			Warmtepomp
			Televisie
			Soundbar
			Smart tv
			Steelstofzuiger
			stofzuiger

			Duplicate data	
			https://prnt.sc/47ouVmjhAPCa 
			https://prnt.sc/7H4OOXbpQima 
			https://prnt.sc/sC6TzwwLQZPd
			https://prnt.sc/YBrEUDFg_6i1 
			https://prnt.sc/nAPuzeUGzpeV 
			https://prnt.sc/MPD3qknNUpn3
			https://prnt.sc/2rSiiRdGoSK4 
			https://prnt.sc/Xs5vkdhX-NYg 
			https://prnt.sc/Bzw1IVuyeYu_ 
			https://prnt.sc/QvDGX1npwkpT 
			https://prnt.sc/mZfi_meHoHjH
			https://prnt.sc/knrj33c89IKp 
			https://prnt.sc/nuB91shXR5Zp
			https://prnt.sc/xVAP-UkqQguN 
			https://prnt.sc/S_POBUypTDze 
			https://prnt.sc/ivyUwD-R2eao
			https://prnt.sc/e8w5dfyrPZUP
			https://prnt.sc/w_FmGMi_ZnsU 
			https://prnt.sc/dg826rlBIfHc 
			https://prnt.sc/CBGWf6fyDm6d
			https://prnt.sc/uyoqcmHB8xPz 
			https://prnt.sc/M7ja8bpdhl11 
			https://prnt.sc/VrgFxY3YfD0k
			
			Yes	ailyn

			{
			    "response": {
			        "task_uuid": "4c9b8121-e44c-4487-8de6-f42b3c401b0c"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "4686169b-f6b2-4cb5-bd58-8a7e1f04205e"
			    },
			    "version": "1.0"
			}

			# fixed: changed api pre requisite

			{
			    "response": {
			        "task_uuid": "376ec2f5-e7c7-46b3-a6bb-2664da42f2bd"
			    },
			    "version": "1.0"
			}D

09 03 22
	(F D-2)  Trello 1:
		# https://trello.com/c/l68SVqzl/4189-nintendo-brazil-comparisons-invalid-1-values-scraped-on-25bestcombr
		# [NINTENDO BRAZIL] Comparisons: invalid -1 values scraped on 25best.com.br
		Date: August 31 ,2022
		Company: NINTENDO BRAZIL (U00)
		Retailer: 25best.com.br
		Issue: -1 values scraped and off visibility
		Cause: Unhandle tags/ Need further investigation

		affected item_ids:

		U00BRZ21TV020220621055622407146172
		U00BRZ21TV020220621055623012411172
		U00BRZ21TV020220621055623409230172
		U00BRZ21TV020220621055623924119172
		U00BRZ21TV020220621055624417336172

	Techvalidation Issues
		(R D-1) 7128	03/09/2022	No	A00	product_website	microsoft.com/en-gb	
			A00GB840RB000000949130202005211138
			A00GB840RB000001237170202011060516
			A00GB840RB000000874070201911070317
			A00GB840GC000000159490201903040511
			A00GB840RB000000873980201911070312
			A00GB840RB000000874690201911070608
			A00GB840GC000000322900201903041009
			A00GB840RB000000876430201911071433
			A00GB840RB000000874400201911070600
			A00GB840RB000000949120202005211138
			A00GB840RB000000874600201911070607
			A00GB840RB000001344850202103100633
			A00GB840RB000001237210202011060516
			A00GB840RB000001237160202011060516
			A00GB840RB000001237200202011060516
			A00GB840RB000001344840202103100633
			-2 product_website
			7129	03/09/2022	No	A00	in_stock	microsoft.com/en-sg	
				A00SG350KT020220329120131403937088
				A00SG350KT020220329120132268615088
				A00SG350KT020220808042456047531220
				A00SG350KT020220329120131028868088
				A00SG350KT020220329120131210793088
				A00SG350KT020220329120131841402088
				A00SG350KT020220808041350162424220
				A00SG350KT020220329120130798215088
				A00SG350KT020220329120132458010088
				A00SG350KT020220808042456351913220
				A00SG350KT020220329120130556705088
				A00SG350KT020220808042456850092220
				A00SG350KT020220329120131614355088
				A00SG350KT020220808042456726053220
				A00SG350KT020220808042456470318220
				A00SG350KT020220808042456599346220
				A00SG350KT020220329120132070935088
				A00SG350KT020220808042456190056220
				-1 in_stock

			7130	03/09/2022	No	A00	in_stock,price_value	microsoft.com/en-sg	
				A00SG350KT020220808041350102217220
				A00SG350KT020220808042455896498220
				A00SG350KT020220808041350007468220
				-1 in_stock,-2 price_value

			7131	03/09/2022	No	A00	in_stock,product_website	microsoft.com/fr-fr	
				A00FRC40RB000000948750202005210956
				A00FRC40RB000000948910202005211019
				-1 in_stock,-1 product_website

			7132	03/09/2022	No	A00	in_stock	microsoft.com/ja-jp	
				A00JPD40KT020220808041347723540220
				A00JPD40KT020211012070247955693285
				A00JPD40KT020211012070247661588285
				A00JPD40KT020211012070245446582285
				A00JPD40KT020211012070246480897285
				A00JPD40KT020211012070246163032285
				A00JPD40KT020211012070245035822285
				A00JPD40RB000001425710202106010136
				A00JPD40RB000001345300202103100636
				A00JPD40KT020211012070245820884285
				A00JPD40KT020211012070246913303285
				A00JPD40RB000001425600202106010136
				-2 in_stock

		(R D-1) 7133	03/09/2022	No	F00	product_website	post.lu/fr	
			F00BEWX040000001656200202111120252
			F00BEXX0KB020220329062757267776088
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('F00BEWX040000001656200202111120252', 'F00BEXX0KB020220329062757267776088')

		(R D-2) 7134	03/09/2022	No	K00	in_stock,product_website	power.dk	
			K00DKX20CG000001487230202107130336	
			-3 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00DKX20CG000001487230202107130336')

			{
			    "response": {
			        "task_uuid": "c792cfe8-b7f6-4e6c-b72a-ce01696871ae"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "87385af7-0bf1-4b92-9492-7feba734b56f"
			    },
			    "version": "1.0"
			}

		(R D-2) 7135	03/09/2022	No	100	product_website	power.se	
			100SEY2010000001666230202111170531	
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('100SEY2010000001666230202111170531')

			{
			    "response": {
			        "task_uuid": "d5962907-247c-4431-bdf0-3dccc058a3a1"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1cd536b8-b935-44fa-ab59-2be04634c9b1"
			    },
			    "version": "1.0"
			}

		(R D-2) 7136	03/09/2022	No	K00	in_stock,product_website	power.se	
			K00SEY20CG000001473820202107060432	
			-1 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00SEY20CG000001473820202107060432')

			{
			    "response": {
			        "task_uuid": "a7009604-6de3-406e-b4e5-96bcb88c2183"
			    },
			    "version": "1.0"
			}

		fr_rebuild (F D-1) (R D-2) 7137	03/09/2022	Yes	110	description	allyourgames.nl	
			110NL311RV020211116232339973026320
			110NL311RV020211116232339529673320
			110NL311RV020211116232328347196320
			110NL311RV020211116232330274501320
			-1 description

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('110NL311RV020211116232339973026320', '110NL311RV020211116232339529673320', '110NL311RV020211116232328347196320', '110NL311RV020211116232330274501320')

			{
			    "response": {
			        "task_uuid": "1a37a8a0-de69-4973-a584-235c2d899dd6"
			    },
			    "version": "1.0"
			}

		(R D-1) 7138	03/09/2022	Yes	200	description,image_count	amazon.it	
			200ITL4070000000530640201903270412	
			-1 description,No image_count

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('200ITL4070000000530640201903270412')

		(R D-2) 7139	03/09/2022	Yes	200	description	boulanger.com	
			200FR3107R020220711022358070103192
			200FR3107R020220711022357196803192
			200FR3108R020220711022359165038192
			200FR3108R020220711022358767960192
			-1 descriptions

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('200FR3107R020220711022358070103192', '200FR3107R020220711022357196803192', '200FR3108R020220711022359165038192', '200FR3108R020220711022358767960192')

			{
			    "response": {
			        "task_uuid": "a7d355e1-4069-4d18-b54e-b91a13c9fd17"
			    },
			    "version": "1.0"
			}

			# valid no description in site

	Daily Issues:
		(R D-1) 9574	2022-09-03	100	all	golfsky.fi/fi	
			100FI331ZQ020220725025257925374206
			100FI331ZQ020220725025257515426206
			100FI331ZQ020220725025258917098206
			100FI331ZQ020220725025259177108206
			100FI331ZQ020220725025256893471206
			100FI331ZQ020220725025257053870206
			100FI331ZQ020220725025257198540206
			100FI331ZQ020220725025258193651206
			100FI331ZQ020220725025259316703206
		
			Invalid deadlinks		
			Yes	Jing

			# SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('100FI331ZQ020220725025257925374206', '100FI331ZQ020220725025257515426206', '100FI331ZQ020220725025258917098206', '100FI331ZQ020220725025259177108206', '100FI331ZQ020220725025256893471206', '100FI331ZQ020220725025257053870206', '100FI331ZQ020220725025257198540206', '100FI331ZQ020220725025258193651206', '100FI331ZQ020220725025259316703206')

			{
			    "response": {
			        "task_uuid": "cf48eb6b-e4d9-4dda-8462-4fb28ed25c7f"
			    },
			    "version": "1.0"
			}

		(R D-2) 9575	2022-09-03	100	all	elkjop.no	all item_ids	
			Invalid deadlinks		
			Yes	Jing

			# SELECT * FROM view_all_productdata where date='2022-09-03' AND website = 'www.elkjop.no' ORDER BY source

			# rerun sent but task_uuid not copied

			{
			    "response": {
			        "task_uuid": "6f443276-baf3-4564-8247-b5e144984d4b"
			    },
			    "version": "1.0"
			}

		(R D-2) 9576	2022-09-03	100	all	power.no	all item_ids	
			Invalid deadlinks		
			Yes	Jing

			# SELECT * FROM view_all_productdata where date='2022-09-03' AND website = 'www.power.no' ORDER BY source

			{
			    "response": {
			        "task_uuid": "6386e0c9-0bdc-4204-a9dd-e662ca6ceba8"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "923caa03-9f28-40c0-b8d5-ece6ae3f5216"
			    },
			    "version": "1.0"
			}

		(R D-2) 9577	2022-09-03	100	all	power.fi	all item_ids	
			Invalid deadlinks		
			Yes	Jing

			SELECT * FROM view_all_productdata where date='2022-09-03' AND website = 'www.power.fi' ORDER BY source

			{
			    "response": {
			        "task_uuid": "1335aad4-de60-461a-8f09-f330d4dbabc4"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "82a7d39b-c579-48df-be63-f6e601376ffb"
			    },
			    "version": "1.0"
			}

		(R D-2) 9578	2022-09-03	F00	all	orange.lu/fr	
			F00BEKX0EL000001643580202111060938
			F00BEJX0ME020220412075010655986102
			F00BEJX0ME020220412075011607704102
			F00BEJX05R020220412075017186644102
			F00BEJX05R020220412075018331790102
			F00BEJX05R020220412075019860560102
			F00BEJX05R020220124031922266882024
			F00BEJX05R020220124031923179781024
			F00BEJX05R020220124031922706762024
			F00BEJX05R020220314065747522679073
			F00BEJX05R020220314065747842815073
			F00BEJX05R020220314065747694229073
			F00BEJX05R020220314065749089331073
			F00BEJX05R020220314065748564851073
			F00BEJX05R020220314065747051660073
			F00BEJX05R020220314065746807672073
			F00BEJX05R020220314065747314443073
			F00BEKX0JA000001644040202111070520
			F00BEJX05R020220412082121195004102
			F00BEKX0JA000001644060202111070520
			F00BEKX0JA000001644080202111070520
			F00BEKX0JA000001644110202111070520
			F00BEKX0JA000001644150202111070520
			F00BEKX0JA000001644210202111070520
			F00BEJX05R020220412082121596751102
			F00BEKX0JA000001644240202111070520
			F00BEKX0JA000001644260202111070520
			F00BEKX0JA000001644270202111070520
			F00BEKX0JA000001644280202111070520
			F00BEKX0JA000001644480202111070520
			F00BEKX0JA000001644500202111070520
			F00BEJX0KB020220214082622090652045
			F00BEKX0JA000001644530202111070520
			F00BEJX0KB020220329062802479752088
			F00BEKX0JA000001644550202111070520
			F00BEKX0JA000001644600202111070520
			F00BEKX0JA000001644620202111070520
			F00BEJX0KB020220329062803664693088
			F00BEKX0JA000001644640202111070520
			F00BEKX0JA000001644660202111070520
			F00BEKX0JA000001644670202111070520
			F00BEKX0JA000001644690202111070520
			F00BEKX0JA000001644710202111070520
			F00BEKX0JA000001644720202111070520
			F00BEKX0JA000001644730202111070520
			F00BEKX0JA000001644750202111070520
			F00BEKX0JA000001644760202111070520
			F00BEKX0JA000001644770202111070520
			F00BEKX0JA000001644790202111070520
			F00BEJX0KB020220329062806212060088
			F00BEKX0JA000001644830202111070520
			F00BEKX0JA000001644870202111070520
			F00BEJX0KB020220329062807440344088
			F00BEKX0JA000001644890202111070520
			F00BEKX0JA000001644940202111070520
			F00BEJX0KB020220329062808398252088
			F00BEKX0JA000001644980202111070521
			F00BEKX0JA000001644300202111070520
			F00BEJX05R020220412082121730546102
			F00BEKX0JA000001644320202111070520
			F00BEKX0JA000001644350202111070520
			F00BEKX0JA000001644380202111070520
			F00BEKX0JA000001644400202111070520
			F00BEKX0JA000001644420202111070520
			F00BEKX0JA000001644440202111070520
			F00BEKX0JA000001644450202111070520
			F00BEJX0KB020220412082139650570102
			F00BEJX0KB020220412082141000278102
			F00BEJX0KB020220412082143439326102
			F00BEJX0KB020220412082144363576102
			F00BEJX0KB020220412082145352185102
			F00BEJX0KB020220412082147883709102
			F00BEJX0KB020220412082149217868102
		
			Invalid Deadlinks		
			Yes	Crestine

			{
			    "response": {
			        "task_uuid": "f18b9ac6-9723-4f7c-b5ee-6c752618290b"
			    },
			    "version": "1.0"
			}

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('F00BEKX0EL000001643580202111060938', 'F00BEJX0ME020220412075010655986102', 'F00BEJX0ME020220412075011607704102', 'F00BEJX05R020220412075017186644102', 'F00BEJX05R020220412075018331790102', 'F00BEJX05R020220412075019860560102', 'F00BEJX05R020220124031922266882024', 'F00BEJX05R020220124031923179781024', 'F00BEJX05R020220124031922706762024', 'F00BEJX05R020220314065747522679073', 'F00BEJX05R020220314065747842815073', 'F00BEJX05R020220314065747694229073', 'F00BEJX05R020220314065749089331073', 'F00BEJX05R020220314065748564851073', 'F00BEJX05R020220314065747051660073', 'F00BEJX05R020220314065746807672073', 'F00BEJX05R020220314065747314443073', 'F00BEKX0JA000001644040202111070520', 'F00BEJX05R020220412082121195004102', 'F00BEKX0JA000001644060202111070520', 'F00BEKX0JA000001644080202111070520', 'F00BEKX0JA000001644110202111070520', 'F00BEKX0JA000001644150202111070520', 'F00BEKX0JA000001644210202111070520', 'F00BEJX05R020220412082121596751102', 'F00BEKX0JA000001644240202111070520', 'F00BEKX0JA000001644260202111070520', 'F00BEKX0JA000001644270202111070520', 'F00BEKX0JA000001644280202111070520', 'F00BEKX0JA000001644480202111070520', 'F00BEKX0JA000001644500202111070520', 'F00BEJX0KB020220214082622090652045', 'F00BEKX0JA000001644530202111070520', 'F00BEJX0KB020220329062802479752088', 'F00BEKX0JA000001644550202111070520', 'F00BEKX0JA000001644600202111070520', 'F00BEKX0JA000001644620202111070520', 'F00BEJX0KB020220329062803664693088', 'F00BEKX0JA000001644640202111070520', 'F00BEKX0JA000001644660202111070520', 'F00BEKX0JA000001644670202111070520', 'F00BEKX0JA000001644690202111070520', 'F00BEKX0JA000001644710202111070520', 'F00BEKX0JA000001644720202111070520', 'F00BEKX0JA000001644730202111070520', 'F00BEKX0JA000001644750202111070520', 'F00BEKX0JA000001644760202111070520', 'F00BEKX0JA000001644770202111070520', 'F00BEKX0JA000001644790202111070520', 'F00BEJX0KB020220329062806212060088', 'F00BEKX0JA000001644830202111070520', 'F00BEKX0JA000001644870202111070520', 'F00BEJX0KB020220329062807440344088', 'F00BEKX0JA000001644890202111070520', 'F00BEKX0JA000001644940202111070520', 'F00BEJX0KB020220329062808398252088', 'F00BEKX0JA000001644980202111070521', 'F00BEKX0JA000001644300202111070520', 'F00BEJX05R020220412082121730546102', 'F00BEKX0JA000001644320202111070520', 'F00BEKX0JA000001644350202111070520', 'F00BEKX0JA000001644380202111070520', 'F00BEKX0JA000001644400202111070520', 'F00BEKX0JA000001644420202111070520', 'F00BEKX0JA000001644440202111070520', 'F00BEKX0JA000001644450202111070520', 'F00BEJX0KB020220412082139650570102', 'F00BEJX0KB020220412082141000278102', 'F00BEJX0KB020220412082143439326102', 'F00BEJX0KB020220412082144363576102', 'F00BEJX0KB020220412082145352185102', 'F00BEJX0KB020220412082147883709102', 'F00BEJX0KB020220412082149217868102')

			{
			    "response": {
			        "task_uuid": "6d91c8bf-3ecf-4d15-b8be-488c26bce54b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1e7b8a17-e340-44a3-93c4-183fe82a98aa"
			    },
			    "version": "1.0"
			}

		(R D-1) 9579	2022-09-03	110	all	mediamarkt.be/nl/	
			110BE2D0OL000001435410202106020647
			110BE2D0OL000001435290202106020647
			110BE2D0RV020210927050534798273270
			110BE2D0OL000001435130202106020647
			110BE2D0RV020220215020815723369046
			110BE2D0OL000001435090202106020647
			110BE2D0OL000001434850202106020647
			110BE2D0OL000001435250202106020647
			110BE2D0OL000001435530202106020647
			110BE2D0OL000001434890202106020647
			110BE2D0OL000001516610202107260528
			110BE2D0OL000001435330202106020647
			110BE2D0OL000001435050202106020647
			110BE2D0RV020211025045126537298298
			110BE2D0OL000001435210202106020647
			110BE2D0RV020211025045128762823298
			110BE2D0OL000001435170202106020647
			110BE2D0RV020220712020126927500193
			110BE2D0OL000001436290202106070737
			110BE2D0RV020220712020128171753193
			110BE2D0OL000001435450202106020647
			110BE2D0OL000001434930202106020647
			110BE2D0OL000001435010202106020647
			110BE2D0OL000001517010202107260528
			110BE2D0OL000001436330202106070737
			110BE2D0OL000001436370202106070737
			110BE2D0OL000001436530202106070737
			110BE2D0OL000001436570202106070737
			110BE2D0OL000001436410202106070737
			110BE2D0OL000001436490202106070737
			110BE2D0RV020220531041651742943151
			110BE2D0RV020220531041654724914151
			Invalid Deadlinks		
			Yes	Royce

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('110BE2D0OL000001435410202106020647', '110BE2D0OL000001435290202106020647', '110BE2D0RV020210927050534798273270', '110BE2D0OL000001435130202106020647', '110BE2D0RV020220215020815723369046', '110BE2D0OL000001435090202106020647', '110BE2D0OL000001434850202106020647', '110BE2D0OL000001435250202106020647', '110BE2D0OL000001435530202106020647', '110BE2D0OL000001434890202106020647', '110BE2D0OL000001516610202107260528', '110BE2D0OL000001435330202106020647', '110BE2D0OL000001435050202106020647', '110BE2D0RV020211025045126537298298', '110BE2D0OL000001435210202106020647', '110BE2D0RV020211025045128762823298', '110BE2D0OL000001435170202106020647', '110BE2D0RV020220712020126927500193', '110BE2D0OL000001436290202106070737', '110BE2D0RV020220712020128171753193', '110BE2D0OL000001435450202106020647', '110BE2D0OL000001434930202106020647', '110BE2D0OL000001435010202106020647', '110BE2D0OL000001517010202107260528', '110BE2D0OL000001436330202106070737', '110BE2D0OL000001436370202106070737', '110BE2D0OL000001436530202106070737', '110BE2D0OL000001436570202106070737', '110BE2D0OL000001436410202106070737', '110BE2D0OL000001436490202106070737', '110BE2D0RV020220531041651742943151', '110BE2D0RV020220531041654724914151')

			{
			    "response": {
			        "task_uuid": "4374d89c-a8cf-4fa2-8aeb-52f4380350dd"
			    },
			    "version": "1.0"
			}

		(Fix reference trello 1) 9583	2022-09-03	U00	all	25best.com.br	
			U00BRZ21TV020220621055622407146172
			U00BRZ21TV020220621055623012411172
			U00BRZ21TV020220621055623409230172
			U00BRZ21TV020220621055623924119172
			U00BRZ21TV020220621055624417336172
			Invalid deadlink		
			Yes	Neil

			# 

		(R D-1) 9601	2022-09-03	K00	all	power.dk 	all items	invalid deadlink		Yes	Ash
		
		(R D-2) 9602	2022-09-03	K00	all	power.fi 	all items	invalid deadlink		Yes	Ash
		
		(R D-2) 9603	2022-09-03	K00	all	mediamarkt.de 	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00DE8103O000001433120202106020354', 'K00DE8103O000001432960202106020354', 'K00DE8103O000001432980202106020354', 'K00DE8103O000001433040202106020354', 'K00DE8103O000001433010202106020354', 'K00DE8103O000001433050202106020354', 'K00DE8103O000001433080202106020354', 'K00DE8107O000001433270202106020354', 'K00DE8107O000001433180202106020354', 'K00DE810AD000001433440202106020354', 'K00DE810AD000001433340202106020354', 'K00DE810AD000001433460202106020354', 'K00DE810AD000000925060202002260844', 'K00DE810AD000001537470202108110153', 'K00DE8109O000001433550202106020354', 'K00DE8109O000001433610202106020354', 'K00DE8109O000001433570202106020354', 'K00DE8109O000001433590202106020354', 'K00DE8108O000001433510202106020354', 'K00DE810MD000001433620202106020354', 'K00DE8104K000001537520202108110153', 'K00DE8104K000001537600202108110153', 'K00DE8104K000001433740202106020354', 'K00DE8104K000001537880202108110153', 'K00DE8104K000001537930202108110153', 'K00DE8104K000001433790202106020354', 'K00DE8104K000001433820202106020354', 'K00DE8103F000001305720202101210359', 'K00DE8103F000001305700202101210359', 'K00DE8103F000001305750202101210359', 'K00DE8103F000001305740202101210359', 'K00DE8103F000000925340202002260844', 'K00DE8103F000000989150202007060853', 'K00DE8103F000000989110202007060853', 'K00DE8103F000001538250202108110153', 'K00DE8103F000001538310202108110153', 'K00DE8103F000000925210202002260844', 'K00DE8103F000000989100202007060853', 'K00DE8103F000001305550202101210359', 'K00DE8103F000001538540202108110153', 'K00DE8103F000001305710202101210359', 'K00DE810FF000001433900202106020354', 'K00DE810RB000001433870202106020354', 'K00DE810DO000001433920202106020354', 'K00DE8107U020220627051228390733178', 'K00DE8107U020211116100219851399320', 'K00DE810CG000001592890202109200649', 'K00DE810CG000001209220202009280432', 'K00DE8107U020220627051232283772178', 'K00DE8107U020220328091600114597087', 'K00DE810CG000001244920202011170410', 'K00DE810CG000001525110202108020807', 'K00DE810CG000001244930202011170410', 'K00DE810CG000001465180202107050547', 'K00DE810CG000001465220202107050547', 'K00DE8107U020220328091600482315087', 'K00DE810CG000001399340202105040624', 'K00DE8107U020220328091601448556087', 'K00DE8107U020220328091601764950087', 'K00DE8107U020220328091602380083087', 'K00DE810CG000001399260202105040624', 'K00DE8107U020220328091603156646087', 'K00DE8107U020220328091603404696087', 'K00DE8107U020220328091603960456087', 'K00DE8107U020220328091604202221087', 'K00DE810CG000001399580202105040624', 'K00DE810CG000001399660202105040624', 'K00DE810CG000001399540202105040624', 'K00DE810CG000001354960202103290839', 'K00DE810CG000001360060202103300725', 'K00DE810CG000001355070202103290839', 'K00DE810CG000001244910202011170410', 'K00DE8107U020211116100222145050320', 'K00DE8107U020211116100220487001320', 'K00DE810CG000001296550202101180224', 'K00DE810CG000001296570202101180224', 'K00DE810CG000001296520202101180224', 'K00DE8107U020220222033738308634053', 'K00DE810CG000001296720202101180225', 'K00DE810CG000001296730202101180225', 'K00DE8107U020220222033737871869053', 'K00DE8107U020220328091604486185087', 'K00DE8107U020211116100225178536320', 'K00DE810CG000001373460202104050819', 'K00DE8107U020220222033730816180053', 'K00DE8107U020220222033731213023053', 'K00DE8107U020220222033730439249053', 'K00DE8107U020220222033731637636053', 'K00DE810CG000001593050202109200649', 'K00DE8107U020220627051229421441178', 'K00DE810CG000001296690202101180225', 'K00DE810CG000001296700202101180225', 'K00DE8107U020220627051231318380178', 'K00DE8107U020220627051230446732178', 'K00DE8107U020211116100224500702320', 'K00DE8107U020220627051226792340178', 'K00DE8107U020220425061224824808115', 'K00DE8107U020211116100224146332320', 'K00DE810CG000001296710202101180225', 'K00DE8107U020211116100223152683320', 'K00DE8107U020211116100222804947320', 'K00DE8107U020211116100222470321320', 'K00DE810CG000000923230202002190707', 'K00DE8107U020220222033728556788053', 'K00DE8107U020220222033729321626053', 'K00DE810CG000001244960202011170410', 'K00DE8107U020220222033735092129053', 'K00DE810CG000001210060202009280712', 'K00DE8107U020220627051227096933178', 'K00DE810CG000001409370202105110503', 'K00DE8107U020220222033735850257053', 'K00DE8107U020220222033736655562053', 'K00DE8107U020211116100223828491320', 'K00DE8107U020211116100223490366320', 'K00DE810CG000001410750202105110851', 'K00DE8107U020220222033738726522053', 'K00DE810CG000001465140202107050547', 'K00DE8107U020220621020739025987172', 'K00DE8107U020220621020738658163172', 'K00DE8107U020220621020736547567172', 'K00DE8107U020220621020735377148172', 'K00DE810CG000001373380202104050819', 'K00DE8107U020220627051232593041178', 'K00DE8107U020220627051226157193178', 'K00DE810FO000001538680202108110154', 'K00DE810GO000001434080202106020354', 'K00DE810GO000001434100202106020354')

			invalid deadlink		
			Yes	Ash

			{
			    "response": {
			        "task_uuid": "7ca81de3-51c3-4598-a230-6d401ccb4bd3"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "59e4d933-4e62-487a-a458-00833a660ba2"
			    },
			    "version": "1.0"
			}

		(R D-2) 9604	2022-09-03	K00	all	saturn.de	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00DE9103O000001433000202106020354', 'K00DE9103O000001433070202106020354', 'K00DE9107O000001433230202106020354', 'K00DE9107O000001433200202106020354', 'K00DE910AD000001433450202106020354', 'K00DE910Q1020220607015123449867158', 'K00DE910AD000001537260202108110153', 'K00DE910Q1020220607015123738833158', 'K00DE910AD000001537350202108110153', 'K00DE910Q1020220607015123891042158', 'K00DE910AD000001433360202106020354', 'K00DE910Q1020220607015124030004158', 'K00DE910AD000001537430202108110153', 'K00DE910AD000001537450202108110153', 'K00DE910AD000001433480202106020354', 'K00DE910AD000000925050202002260844', 'K00DE910AD000001537490202108110153', 'K00DE9109O000001433540202106020354', 'K00DE9109O000001433560202106020354', 'K00DE9109O000001433580202106020354', 'K00DE9109O000001433600202106020354', 'K00DE9108O000001433520202106020354', 'K00DE9108O000001433500202106020354', 'K00DE9102N020220607015124175469158', 'K00DE910P1020220607015124325661158', 'K00DE9104K000001537530202108110153', 'K00DE9104K000001537560202108110153', 'K00DE9104K000001433750202106020354', 'K00DE9104K000001537740202108110153', 'K00DE9104K000001433670202106020354', 'K00DE9104K000001537790202108110153', 'K00DE910U1020220607015124783269158', 'K00DE9104K000001537890202108110153', 'K00DE9104K000001537950202108110153', 'K00DE9104K000001538000202108110153', 'K00DE9104K000001538050202108110153', 'K00DE9104K000001433780202106020354', 'K00DE910K0000001433860202106020354', 'K00DE910P1020220607015124903874158', 'K00DE9103F000001305960202101210411', 'K00DE910P1020220607015125046514158', 'K00DE9103F000000990290202007060921', 'K00DE910P1020220607015125189025158', 'K00DE910P1020220607015125338172158', 'K00DE9103F000001305810202101210411', 'K00DE9103F000001538280202108110153', 'K00DE9103F000000925480202002260844', 'K00DE9103F000000925200202002260844', 'K00DE9103F000000990240202007060921', 'K00DE9103F000001305820202101210411', 'K00DE9103F000001305930202101210411', 'K00DE910P1020220607015125477528158', 'K00DE9103F000001538480202108110153', 'K00DE9103F000001538560202108110154', 'K00DE9103F000001305850202101210411', 'K00DE9103F000001305800202101210411', 'K00DE9103F000001305970202101210411', 'K00DE910FF000001433910202106020354', 'K00DE910DO000001433940202106020354', 'K00DE9107U020220627051229169818178', 'K00DE9107U020220627051227780493178', 'K00DE9107U020220627051228116843178', 'K00DE9107U020220627051228460781178', 'K00DE9107U020211116100220206431320', 'K00DE9107U020211116100219909904320', 'K00DE910CG000001492550202107190651', 'K00DE910CG000000980960202006250959', 'K00DE910CG000001592900202109200649', 'K00DE910CG000001209230202009280432', 'K00DE9107U020220328091600241595087', 'K00DE910CG000001245340202011170417', 'K00DE910CG000001245360202011170417', 'K00DE910CG000001209430202009280448', 'K00DE910CG000001245350202011170417', 'K00DE910CG000001525120202108020807', 'K00DE910CG000001465190202107050547', 'K00DE9107U020220328091600563417087', 'K00DE9107U020220328091600878751087', 'K00DE9107U020220328091601527650087', 'K00DE9107U020220328091601848471087', 'K00DE9107U020220328091602166208087', 'K00DE9107U020220328091602442300087', 'K00DE9107U020220328091602687267087', 'K00DE910CG000001399200202105040624', 'K00DE910CG000001354670202103290839', 'K00DE910CG000001354830202103290839', 'K00DE9107U020220328091603222683087', 'K00DE9107U020220328091603496813087', 'K00DE910CG000001399680202105040624', 'K00DE910CG000001399480202105040624', 'K00DE910CG000001399560202105040624', 'K00DE910CG000001399640202105040624', 'K00DE910CG000001399600202105040624', 'K00DE910CG000001354870202103290839', 'K00DE910CG000001354470202103290839', 'K00DE910CG000001360010202103300725', 'K00DE910CG000001360050202103300725', 'K00DE910CG000001355060202103290839', 'K00DE910CG000001525150202108020807', 'K00DE910CG000001300750202101180540', 'K00DE910CG000001245330202011170417', 'K00DE910CG000000922760202002190707', 'K00DE9107U020211116100221544812320', 'K00DE910CG000001300830202101180540', 'K00DE910CG000001300850202101180540', 'K00DE910CG000001300820202101180540', 'K00DE910CG000001300810202101180540', 'K00DE910CG000001300840202101180540', 'K00DE910CG000001300990202101180540', 'K00DE9107U020220222033737958845053', 'K00DE9107U020220328091604541913087', 'K00DE9107U020220328091604839392087', 'K00DE910CG000001525160202108020807', 'K00DE910CG000001373450202104050819', 'K00DE910CG000001593020202109200649', 'K00DE9107U020211116100225996694320', 'K00DE9107U020220222033730873831053', 'K00DE9107U020220222033730522526053', 'K00DE910CG000001300900202101180540', 'K00DE9107U020220222033731727696053', 'K00DE910CG000001593060202109200649', 'K00DE910CG000001300960202101180540', 'K00DE9107U020220627051229846614178', 'K00DE910CG000001300970202101180540', 'K00DE9107U020220627051231387709178', 'K00DE9107U020220627051231060442178', 'K00DE9107U020220627051230503024178', 'K00DE9107U020211116100224558556320', 'K00DE9107U020220627051226846924178', 'K00DE910CG000001300890202101180540', 'K00DE9107U020220425061224916242115', 'K00DE910CG000001301010202101180540', 'K00DE910CG000000923010202002190707', 'K00DE9107U020211116100223226704320', 'K00DE9107U020211116100222874402320', 'K00DE9107U020211116100222543795320', 'K00DE910CG000001393520202104280059', 'K00DE910CG000000923120202002190707', 'K00DE9107U020220222033728995547053', 'K00DE9107U020220222033729710174053', 'K00DE9107U020220621020739797623172', 'K00DE910CG000001373410202104050819', 'K00DE910CG000001300780202101180540', 'K00DE910CG000001210070202009280712', 'K00DE9107U020220627051227155049178', 'K00DE910CG000001409360202105110503', 'K00DE9107U020211026061940841501299', 'K00DE9107U020220222033730022246053', 'K00DE9107U020220222033736305489053', 'K00DE9107U020220222033737093488053', 'K00DE9107U020220222033736740474053', 'K00DE9107U020220328091605415801087', 'K00DE910CG000001300880202101180540', 'K00DE9107U020220627051233715810178', 'K00DE9107U020220627051233346470178', 'K00DE9107U020211116100223883954320', 'K00DE9107U020211116100223547282320', 'K00DE910CG000001410760202105110909', 'K00DE9107U020220222033739183846053', 'K00DE910CG000001300740202101180540', 'K00DE9107U020220621020739105157172', 'K00DE9107U020220621020738385865172', 'K00DE9107U020220621020736620529172', 'K00DE9107U020220621020733131041172', 'K00DE910CG000000923570202002190707', 'K00DE910CG000001373370202104050819', 'K00DE910CG000000980980202006250959', 'K00DE9107U020220627051232665253178', 'K00DE9107U020220627051226247492178', 'K00DE910CG000001300930202101180540', 'K00DE910EO000001434040202106020354', 'K00DE910FO000001434070202106020354', 'K00DE910GO000001434090202106020354', 'K00DE910JO000001434190202106020354', 'K00DE910SB220220607015125638808158')

			{
			    "response": {
			        "task_uuid": "251e0cc9-d1bf-4a89-8d30-e03dccce72e5"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1627cd4c-4e41-43a7-8331-83a617699333"
			    },
			    "version": "1.0"
			}
			invalid deadlink		
			Yes	Ash

		(R D-2) 9605	2022-09-03	K00	all	elkjop.no	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00NO5007O000001446590202106150451', 'K00NO5007O000001446550202106150451', 'K00NO500AD000001446680202106150451', 'K00NO500Q1020220614060622504943165', 'K00NO500AD000000924880202002260844', 'K00NO500AD000001554520202108180340', 'K00NO500Q1020220614060622805182165', 'K00NO500AD000001554530202108180340', 'K00NO500Q1020220614060622952786165', 'K00NO500AD000001446660202106150451', 'K00NO500AD000001554550202108180340', 'K00NO500AD000000925010202002260844', 'K00NO500AD000001554570202108180340', 'K00NO500RB220220608020517801320159', 'K00NO5002N020220614060623103735165', 'K00NO500MD000001446710202106150451', 'K00NO5004K000001554670202108180340', 'K00NO5004K000001554720202108180340', 'K00NO5004K000001554730202108180340', 'K00NO5004K000001446800202106150451', 'K00NO5004K000001450050202106150601', 'K00NO5004K000001554610202108180340', 'K00NO5004K000001450030202106150556', 'K00NO5004K000001554750202108180340', 'K00NO5004K000001554800202108180340', 'K00NO5004K000001554830202108180340', 'K00NO5004K000001554880202108180340', 'K00NO5004K000001450060202106150608', 'K00NO5004K000001446790202106150451', 'K00NO5003F000001447020202106150451', 'K00NO5003F000001447000202106150451', 'K00NO500P1020220608020517628024159', 'K00NO5003F000000925160202002260844', 'K00NO5003F000000925300202002260844', 'K00NO5003F000001554930202108180340', 'K00NO500P1020220608020517711350159', 'K00NO500P1020220614060623394009165', 'K00NO5003F000001446960202106150451', 'K00NO5003F000001554990202108180340', 'K00NO5003F000000925440202002260844', 'K00NO500P1020220614060623558759165', 'K00NO5003F000001555020202108180340', 'K00NO5003F000001555050202108180340', 'K00NO5003F000001555060202108180340', 'K00NO5003F000001446940202106150451', 'K00NO5003F000001555110202108180340', 'K00NO5003F000001446870202106150451', 'K00NO5003F000001555130202108180340', 'K00NO5003F000001447040202106150451', 'K00NO500FF000001447080202106150451', 'K00NO5007U020220627045518185905178', 'K00NO500CG000001591920202109200556', 'K00NO500CG000001244530202011170356', 'K00NO500CG000001244500202011170356', 'K00NO500CG000001244520202011170356', 'K00NO500CG000001525480202108020807', 'K00NO500CG000001244510202011170356', 'K00NO500CG000001525490202108020807', 'K00NO500CG000001356310202103290840', 'K00NO500CG000001525500202108020807', 'K00NO500CG000001297880202101180353', 'K00NO500CG000001244490202011170356', 'K00NO500CG000000980600202006250925', 'K00NO500CG000001297980202101180353', 'K00NO500CG000001297950202101180353', 'K00NO500CG000001297940202101180353', 'K00NO500CG000001297970202101180353', 'K00NO500CG000001297920202101180353', 'K00NO500CG000001297930202101180353', 'K00NO500CG000001298120202101180353', 'K00NO500CG000001209960202009280649', 'K00NO500CG000001591960202109200556', 'K00NO5007U020211115035452941136319', 'K00NO500CG000001298030202101180353', 'K00NO5007U020220627045515919781178', 'K00NO500CG000001298090202101180353', 'K00NO5007U020220627045516118413178', 'K00NO500CG000001298100202101180353', 'K00NO5007U020220627045516323786178', 'K00NO5007U020220627045516704510178', 'K00NO5007U020220627045516521962178', 'K00NO5007U020211115035456006980319', 'K00NO500CG000001298020202101180353', 'K00NO500CG000000922970202002190707', 'K00NO500CG000001393700202104280059', 'K00NO500CG000000923100202002190707', 'K00NO500CG000001244540202011170356', 'K00NO500CG000001373240202104050819', 'K00NO5007U020220222101507764908053', 'K00NO500CG000001297910202101180353', 'K00NO500CG000001525560202108020807', 'K00NO500CG000001409580202105110503', 'K00NO500CG000001409580202105110503', 'K00NO5007U020220328081028694658087', 'K00NO5007U020220627045518570816178', 'K00NO5007U020220627045518365935178', 'K00NO500CG000001244550202011170356', 'K00NO500CG000001409560202105110503', 'K00NO500CG000001409540202105110503', 'K00NO5007U020220620055712871343171', 'K00NO5007U020220620055712674777171', 'K00NO5007U020220620051751666241171', 'K00NO5007U020220620051800643493171', 'K00NO5007U020220620051757803896171', 'K00NO5007U020220620051754837161171', 'K00NO500CG000001373220202104050819', 'K00NO500CG000000980620202006250925', 'K00NO500CG000000980610202006250925', 'K00NO500FO000001447100202106150451', 'K00NO500SB220220608020517449201159', 'K00NO5005K000000987700202007060802', 'K00NO5005K000001555190202108180340', 'K00NO5005K000001555220202108180340', 'K00NO5005K000001447130202106150451', 'K00NO5005K000001447110202106150451', 'K00NO500TB220220608020517537608159', 'K00NO5007Q000001555250202108180340', 'K00NO5007Q000001555260202108180340')

			{
			    "response": {
			        "task_uuid": "2d5eea54-6572-46d3-a7a3-ce37ab2f6f78"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "629b2f24-17de-4734-8527-b74e03034e87"
			    },
			    "version": "1.0"
			}
			
			invalid deadlink		
			Yes	Ash

		(R D-2) 9606	2022-09-03	K00	all	power.no 	
			all items	
			invalid deadlink		
			Yes	Ash

			SELECT * FROM view_all_productdata where date='2022-09-03' AND website = 'www.power.no'

			{
			    "response": {
			        "task_uuid": "96deabed-1692-4774-8481-792e8f2ad10a"
			    },
			    "version": "1.0"
			}

		(R D-2) 9607	2022-09-03	K00	all	pcdiga.com	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00PT3Q07U020220627045526320852178', 'K00PT3Q07U020220627045526477472178', 'K00PT3Q07U020211115034619511756319', 'K00PT3Q0CG000001582460202109130423', 'K00PT3Q0CG000001582300202109130423', 'K00PT3Q0CG000001592170202109200556', 'K00PT3Q0CG000001582540202109130423', 'K00PT3Q0CG000001582550202109130423', 'K00PT3Q0CG000001582350202109130423', 'K00PT3Q0CG000001582340202109130423', 'K00PT3Q0CG000001582330202109130423', 'K00PT3Q0CG000001582530202109130423', 'K00PT3Q0CG000001582560202109130423', 'K00PT3Q0CG000001582280202109130423', 'K00PT3Q0CG000001582000202109130423', 'K00PT3Q0CG000001582270202109130423', 'K00PT3Q07U020211115034618861122319', 'K00PT3Q07U020211115034619915128319', 'K00PT3Q07U020211115034626415341319', 'K00PT3Q07U020211115034619759912319', 'K00PT3Q0CG000001582120202109130423', 'K00PT3Q0CG000001582140202109130423', 'K00PT3Q0CG000001582110202109130423', 'K00PT3Q0CG000001582100202109130423', 'K00PT3Q0CG000001582130202109130423', 'K00PT3Q0CG000001582380202109130423', 'K00PT3Q0CG000001582470202109130423', 'K00PT3Q07U020220222101517131766053', 'K00PT3Q0CG000001582220202109130423', 'K00PT3Q07U020220328081036574332087', 'K00PT3Q0CG000001582090202109130423', 'K00PT3Q0CG000001592180202109200556', 'K00PT3Q0CG000001592200202109200556', 'K00PT3Q0CG000001582170202109130423', 'K00PT3Q07U020220627045526615692178', 'K00PT3Q0CG000001582050202109130423', 'K00PT3Q07U020220627045526761099178', 'K00PT3Q0CG000001582060202109130423', 'K00PT3Q07U020211115034620446719319', 'K00PT3Q0CG000001582320202109130423', 'K00PT3Q0CG000001582520202109130423', 'K00PT3Q0CG000001582580202109130423', 'K00PT3Q07U020211115034626564636319', 'K00PT3Q0CG000001582230202109130423', 'K00PT3Q0CG000001582360202109130423', 'K00PT3Q0CG000001582070202109130423', 'K00PT3Q0CG000001582440202109130423', 'K00PT3Q0CG000001582080202109130423', 'K00PT3Q07U020220222101511768602053', 'K00PT3Q07U020220222101512020444053', 'K00PT3Q07U020220620055717086993171', 'K00PT3Q0CG000001582020202109130423', 'K00PT3Q07U020220222101515293383053', 'K00PT3Q0CG000001582570202109130423', 'K00PT3Q07U020220222101516466624053', 'K00PT3Q0CG000001582510202109130423', 'K00PT3Q07U020220627045528193809178', 'K00PT3Q07U020220627045528034823178', 'K00PT3Q0CG000001582370202109130423', 'K00PT3Q07U020211115034620050709319', 'K00PT3Q0CG000001582160202109130423', 'K00PT3Q0CG000001582590202109130423', 'K00PT3Q07U020220620055716806328171', 'K00PT3Q07U020220620055716661836171', 'K00PT3Q0CG000001581980202109130423', 'K00PT3Q0CG000001581970202109130423', 'K00PT3Q07U020220627045525245233178', 'K00PT3Q07U020220627045525389799178', 'K00PT3Q0CG000001582180202109130423')
			{
			    "response": {
			        "task_uuid": "28c62df8-e747-4240-8216-c247dbf50005"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "99621506-629a-4cfe-917a-ba32a015f0ce"
			    },
			    "version": "1.0"
			}

			invalid deadlink		
			Yes	Ash

		(R D-2) 9608	2022-09-03	K00	all	pcgarage.ro	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00ROBX0QB220220607015125958723158', 'K00ROBX0Q1020220607015126112075158', 'K00ROBX0Q1020220607015126259334158', 'K00ROBX0Q1020220607015126553906158', 'K00ROBX0Q1020220607015126695518158', 'K00ROBX0RB220220607015126837812158', 'K00ROBX02N020220607015126979007158', 'K00ROBX0P1020220607015127135032158', 'K00ROBX0U1020220607015127436665158', 'K00ROBX0P1020220607015127852147158', 'K00ROBX07U020220627051244652470178', 'K00ROBX07U020220627051246291463178', 'K00ROBX0CG000001559290202108230341', 'K00ROBX07U020211115112859174489319', 'K00ROBX0CG000001559610202108230342', 'K00ROBX0CG000001559580202108230342', 'K00ROBX0CG000001559600202108230342', 'K00ROBX0CG000001559690202108230342', 'K00ROBX07U020211115112911834107319', 'K00ROBX0CG000001559350202108230341', 'K00ROBX0CG000001582840202109130551', 'K00ROBX0CG000001559700202108230342', 'K00ROBX07U020220620055706491434171', 'K00ROBX0CG000001559310202108230341', 'K00ROBX07U020220222101522790823053', 'K00ROBX07U020220222101523079955053', 'K00ROBX0CG000001559330202108230341', 'K00ROBX07U020220222101524522630053', 'K00ROBX07U020220627051246592234178', 'K00ROBX07U020220222101525929707053', 'K00ROBX07U020220222101525653733053', 'K00ROBX07U020220627051243480427178', 'K00ROBX07U020220627051243601097178', 'K00ROBX0CG000001559650202108230342')
			
			invalid deadlink		
			Yes	Ash

			{
			    "response": {
			        "task_uuid": "2837aae4-603c-430c-a047-c575e5ce2449"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "3cb588dd-7c3d-4a0e-aa55-0395fc8944c7"
			    },
			    "version": "1.0"
			}

		(R D-2) 9609	2022-09-03	K00	all	elgiganten.se 	
			all items	
			invalid deadlink		
			Yes	Ash

		(R D-2) 9610	2022-09-03	K00	all	power.se	
			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00SEY20QB220220607015128145292158', 'K00SEY20RB220220607015129013948158', 'K00SEY206Q000001555460202108180340', 'K00SEY204K000001555590202108180340', 'K00SEY204K000001555750202108180340', 'K00SEY20P1020220607015129762886158', 'K00SEY203F000001556100202108180340', 'K00SEY203F000001556160202108180340', 'K00SEY207U020220328091619254086087', 'K00SEY207U020220627051258553328178', 'K00SEY207U020220627051258853882178', 'K00SEY207U020211116100210930812320', 'K00SEY207U020211116100210777477320', 'K00SEY20CG000001492610202107190651', 'K00SEY20CG000001467810202107050618', 'K00SEY207U020220627051302570315178', 'K00SEY20CG000001593370202109200649', 'K00SEY20CG000001467850202107050618', 'K00SEY207U020220627051301980861178', 'K00SEY207U020220328091619437109087', 'K00SEY207U020220627051301675293178', 'K00SEY20CG000001468090202107050618', 'K00SEY207U020220627051301393196178', 'K00SEY20CG000001468110202107050618', 'K00SEY20CG000001468100202107050618', 'K00SEY20CG000001467980202107050618', 'K00SEY20CG000001467990202107050618', 'K00SEY20CG000001468410202107050619', 'K00SEY20CG000001473690202107060432', 'K00SEY20CG000001473700202107060432', 'K00SEY20CG000001473710202107060432', 'K00SEY20CG000001473720202107060432', 'K00SEY20CG000001473740202107060432', 'K00SEY20CG000001473750202107060432', 'K00SEY20CG000001473760202107060432', 'K00SEY20CG000001473800202107060432', 'K00SEY20CG000001473810202107060432', 'K00SEY20CG000001473830202107060432', 'K00SEY20CG000001468350202107050619', 'K00SEY20CG000001468360202107050619', 'K00SEY20CG000001468050202107050618', 'K00SEY20CG000001467860202107050618', 'K00SEY20CG000001467780202107050618', 'K00SEY207U020211116100210165065320', 'K00SEY20CG000001473850202107060432', 'K00SEY207U020211116100211519519320', 'K00SEY207U020211116100211206977320', 'K00SEY207U020211116100211354931320', 'K00SEY207U020211116100211071260320', 'K00SEY20CG000001468490202107050619', 'K00SEY20CG000001468460202107050619', 'K00SEY20CG000001468450202107050619', 'K00SEY20CG000001468430202107050619', 'K00SEY20CG000001473370202107060432', 'K00SEY207U020220222033749600442053', 'K00SEY20CG000001468590202107050619', 'K00SEY207U020220328091621641661087', 'K00SEY207U020211116100211966299320', 'K00SEY207U020211116100212139882320', 'K00SEY20CG000001468120202107050618', 'K00SEY20CG000001593400202109200649', 'K00SEY207U020211116100212313209320', 'K00SEY207U020220222033746704263053', 'K00SEY207U020220222033746429288053', 'K00SEY20CG000001468520202107050619', 'K00SEY207U020220222033746879822053', 'K00SEY20CG000001593410202109200649', 'K00SEY207U020220627051259194949178', 'K00SEY20CG000001468010202107050618', 'K00SEY207U020220627051259533574178', 'K00SEY20CG000001468020202107050618', 'K00SEY207U020220627051301085734178', 'K00SEY207U020220627051300754819178', 'K00SEY207U020220627051300446675178', 'K00SEY207U020220627051300157676178', 'K00SEY207U020211116100226490405320', 'K00SEY20CG000001468530202107050619', 'K00SEY20CG000001468180202107050619', 'K00SEY207U020220425061226289019115', 'K00SEY207U020211116100211671991320', 'K00SEY20CG000001468600202107050619', 'K00SEY20CG000001473330202107060432', 'K00SEY20CG000001473340202107060432', 'K00SEY207U020211116100210621102320', 'K00SEY207U020211116100210455954320', 'K00SEY20CG000001468040202107050618', 'K00SEY20CG000001473360202107060432', 'K00SEY207U020220621020740599065172', 'K00SEY20CG000001467790202107050618', 'K00SEY20CG000001467890202107050618', 'K00SEY207U020220222033748283325053', 'K00SEY207U020220222033728078248053', 'K00SEY207U020220222033748442250053', 'K00SEY20CG000001468130202107050618', 'K00SEY207U020220627051257032694178', 'K00SEY207U020220627051257312769178', 'K00SEY20CG000001467900202107050618', 'K00SEY20CG000001467910202107050618', 'K00SEY207U020220222033748746500053', 'K00SEY207U020220222033749061344053', 'K00SEY207U020220222033749233837053', 'K00SEY207U020220627051302896490178', 'K00SEY207U020211116100226918435320', 'K00SEY207U020211116100226785883320', 'K00SEY20CG000001468510202107050619', 'K00SEY20CG000001468500202107050619', 'K00SEY207U020220222033749938751053', 'K00SEY207U020220222033749767610053', 'K00SEY20CG000001468420202107050619', 'K00SEY20CG000001468380202107050619', 'K00SEY20CG000001468390202107050619', 'K00SEY207U020220621020732743073172', 'K00SEY207U020220621020737463380172', 'K00SEY207U020220621020735094585172', 'K00SEY207U020220621020733953847172', 'K00SEY20CG000001467880202107050618', 'K00SEY20CG000001467830202107050618', 'K00SEY20CG000001467820202107050618', 'K00SEY207U020220627051256163015178', 'K00SEY207U020220627051256466506178', 'K00SEY20CG000001468550202107050619', 'K00SEY207U020211116100226634244320', 'K00SEY20SB220220607015130343810158', 'K00SEY205K000001556190202108180340')

			{
			    "response": {
			        "task_uuid": "8be9b04b-ff23-4fbb-a1b5-0664c232e090"
			    },
			    "version": "1.0"
			}
		
			invalid deadlink		Yes	Ash
	
		(R D-2) 9611	2022-09-03	K00	all	scan.co.uk	
			K00GB6707U020220801034055174838213
			K00GB6707U020220801034055261496213
			K00GB6707U020220801034054908423213
			K00GB6707U020220801034048238894213
			K00GB6707U020220801034057306898213
			K00GB6707U020220801034055433050213
			K00GB6707U020220801034048065677213
		
			invalid deadlink		Yes	Ash

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('K00GB6707U020220801034055174838213', 'K00GB6707U020220801034055261496213', 'K00GB6707U020220801034054908423213', 'K00GB6707U020220801034048238894213', 'K00GB6707U020220801034057306898213', 'K00GB6707U020220801034055433050213', 'K00GB6707U020220801034048065677213')

			{
			    "response": {
			        "task_uuid": "4469d185-b790-4d03-9059-3e34db157c59"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "51686080-0653-4ac4-8181-be4e366f568a"
			    },
			    "version": "1.0"
			}

		(R D-2) 9612	2022-09-03	F00	all	mediamarkt.nl	

			SELECT * FROM view_all_productdata where date='2022-09-03' AND item_id in ('F00NLQ10EL000001642120202111040958', 'F00NLQ10EL000001642190202111040958', 'F00NLQ10ME020220228063413829866059', 'F00NLQ10EL000001642480202111040958', 'F00NLQ10ME020220412075010230206102', 'F00NLQ10ME020220412075011846921102', 'F00NLQ10WJ000001260910202012110829', 'F00NLQ10WJ000001260190202012110829', 'F00NLQ10WJ000001260150202012110829', 'F00NLQ10WJ000001529880202108030256', 'F00NLQ10WJ000001529920202108030256', 'F00NLQ10WJ000001530000202108030256', 'F00NLQ10WJ000001260350202012110829', 'F00NLQ10WJ000001260310202012110829', 'F00NLQ10WJ000001530080202108030256', 'F00NLQ10WJ000001530160202108030256', 'F00NLQ10WJ000001388390202104210551', 'F00NLQ10WJ000001388350202104210551', 'F00NLQ10WJ000001388430202104210551', 'F00NLQ10WJ000001388470202104210551', 'F00NLQ10WJ000001388510202104210551', 'F00NLQ10UD020220314065750100634073', 'F00NLQ10UD020220314065751267887073', 'F00NLQ10UD020220314065752403513073', 'F00NLQ10UD020220314065754900278073', 'F00NLQ10UD020220314065756346906073', 'F00NLQ1040000001477450202107120627', 'F00NLQ1040000001477520202107120627', 'F00NLQ1040000001478520202107120901', 'F00NLQ1040000001426170202106010313', 'F00NLQ1040000001426180202106010313', 'F00NLQ1040000001426190202106010313', 'F00NLQ1040000001426200202106010313', 'F00NLQ1040000001359330202103300413', 'F00NLQ1040000001359370202103300413', 'F00NLQ1040000001359410202103300413', 'F00NLQ1040000001359450202103300413', 'F00NLQ105R020220412075013814832102', 'F00NLQ105R020220412075014554767102', 'F00NLQ105R020220412075015188905102', 'F00NLQ1040000001359530202103300413', 'F00NLQ1040000001359570202103300413', 'F00NLQ1040000001566630202108240539', 'F00NLQ1040000001566700202108240539', 'F00NLQ1040000001566840202108240539', 'F00NLQ1040000001566900202108240539', 'F00NLQ1040000001566960202108240539', 'F00NLQ105R020220412075016283879102', 'F00NLQ105R020220412075017503025102', 'F00NLQ105R020220412075018640620102', 'F00NLQ105R020220412075020054867102', 'F00NLQ105R020220412075020585293102', 'F00NLQ105R020220412075021020510102', 'F00NLQ105R020220412075022042657102', 'F00NLQ105R020220412075021759764102', 'F00NLQ105R020220412075022411670102', 'F00NLQ105R020220412075022595798102', 'F00NLQ1040000001256890202012100148', 'F00NLQ1040000001256930202012100148', 'F00NLQ1040000001257050202012100148', 'F00NLQ1040000001257110202012100148', 'F00NLQ1040000001257150202012100148', 'F00NLQ1040000001312760202102020203', 'F00NLQ1040000001312800202102020203', 'F00NLQ1040000001312880202102020203', 'F00NLQ1040000001312960202102020203', 'F00NLQ1040000001313040202102020203', 'F00NLQ105R020220117024910233142017', 'F00NLQ105R020220117024911395938017', 'F00NLQ105R020220117024911012105017', 'F00NLQ105R020220117024910524981017', 'F00NLQ105R020220117024911700850017', 'F00NLQ105R020220117024911837958017', 'F00NLQ1040000001313080202102020203', 'F00NLQ1040000001313200202102020203', 'F00NLQ1040000001313200202102020203', 'F00NLQ105R020220214072847108742045', 'F00NLQ105R020220214072845381203045', 'F00NLQ105R020220214072850144056045', 'F00NLQ105R020220214072848606390045', 'F00NLQ105R020220214072852720115045', 'F00NLQ105R020220214072851443870045', 'F00NLQ105R020220214072855006433045', 'F00NLQ105R020220214072853795933045', 'F00NLQ105R020220214072857878102045', 'F00NLQ105R020220214072856329187045', 'F00NLQ105R020220214072900617090045', 'F00NLQ105R020220214072903289963045', 'F00NLQ105R020220214072901934755045', 'F00NLQ105R020220214072904493519045', 'F00NLQ105R020220214072833646642045', 'F00NLQ105R020220214072829759163045', 'F00NLQ105R020220214072835102799045', 'F00NLQ105R020220214072837670097045', 'F00NLQ105R020220214072838807051045', 'F00NLQ105R020220214072839903178045', 'F00NLQ105R020220214072841929741045', 'F00NLQ105R020220214072842982543045', 'F00NLQ105R020220214072844062047045', 'F00NLQ105R020220214072841014638045', 'F00NLQ105R020220412075028151124102', 'F00NLQ105R020220412075029061543102', 'F00NLQ105R020220412075029928560102', 'F00NLQ105R020220412075032506187102', 'F00NLQ105R020220412075033135007102', 'F00NLQ105R020220412075034299890102', 'F00NLQ105R020220412075034898049102', 'F00NLQ105R020220412075035753800102', 'F00NLQ105R020220412075036820664102', 'F00NLQ105R020220412075038083726102', 'F00NLQ105R020220412075039219906102', 'F00NLQ105R020220412075038658308102', 'F00NLQ105R020220412075039537715102', 'F00NLQ105R020220412075040006496102', 'F00NLQ105R020220412075040483356102', 'F00NLQ105R020220412075040813887102', 'F00NLQ105R020220412075041182509102', 'F00NLQ105R020220412075041555606102', 'F00NLQ105R020220412075042629419102', 'F00NLQ105R020220412075044877144102', 'F00NLQ105R020220412075045439481102', 'F00NLQ105R020220412075046238169102', 'F00NLQ105R020220412075046795825102', 'F00NLQ105R020220412075047587516102', 'F00NLQ105R020220412082123124504102', 'F00NLQ105R020220412082125724865102', 'F00NLQ105R020220412082126662941102', 'F00NLQ105R020220412082128510743102', 'F00NLQ105R020220412082129245401102', 'F00NLQ105R020220412082129955153102', 'F00NLQ105R020220412082130918398102', 'F00NLQ105R020220412082131655305102', 'F00NLQ105R020220412082132585918102', 'F00NLQ105R020220412082133502131102', 'F00NLQ105R020220412082134364608102', 'F00NLQ105R020220412082134809403102', 'F00NLQ105R020220412082136943780102', 'F00NLQ105R020220412082137806609102', 'F00NLQ1040000001550140202108170144', 'F00NLQ1040000001550410202108170144', 'F00NLQ1040000001550570202108170144', 'F00NLQ1040000001550650202108170144', 'F00NLQ1040000001550500202108170144', 'F00NLQ1040000001550760202108170144', 'F00NLQ1040000001550320202108170144', 'F00NLQ105R020220815020413035792227', 'F00NLQ105R020220815020414908464227', 'F00NLQ105R020220815020411036223227', 'F00NLQ105R020220815020413720950227', 'F00NLQ105R020220815020415562570227', 'F00NLQ105R020220815020409640633227', 'F00NLQ105R020220815020411873718227', 'F00NLQ105R020220822032852885498234', 'F00NLQ105R020220822032852797170234', 'F00NLQ1040000001549730202108170144', 'F00NLQ1040000001549980202108170144', 'F00NLQ1040000001549890202108170144', 'F00NLQ1040000001549820202108170144', 'F00NLQ1040000001550710202108170144', 'F00NLQ1040000001550070202108170144', 'F00NLQ105R020220815020418136893227', 'F00NLQ105R020220815020416693201227', 'F00NLQ105R020220815020419561562227', 'F00NLQ105R020220815020418800941227', 'F00NLQ105R020220815020417402524227', 'F00NLQ105R020220815020420272712227', 'F00NLQ10EL000001621140202110180628', 'F00NLQ10EL000001530310202108030256', 'F00NLQ10EL000001388810202104210551', 'F00NLQ10EL000001530360202108030256', 'F00NLQ10EL000001388850202104210551', 'F00NLQ10EL000001388900202104210551', 'F00NLQ10WJ000001621340202110180628', 'F00NLQ10WJ000001621430202110180628', 'F00NLQ10WJ000001621580202110180628', 'F00NLQ10JA000000859720201910110343', 'F00NLQ10JA000000860380201910110408', 'F00NLQ10JA000000859930201910110355', 'F00NLQ10JA000000858620201910110308', 'F00NLQ10JA000000859390201910110332', 'F00NLQ10JA000000858950201910110318', 'F00NLQ10JA000000858790201910110315', 'F00NLQ10JA000001258220202012110819', 'F00NLQ10JA000001258130202012110819', 'F00NLQ10JA000001258160202012110819', 'F00NLQ10JA000001258100202012110819', 'F00NLQ10JA000001423380202105250316', 'F00NLQ10JA000001258190202012110819', 'F00NLQ10JA000001258370202012110819', 'F00NLQ10JA000001258280202012110819', 'F00NLQ10JA000001258310202012110819', 'F00NLQ10JA000001258250202012110819', 'F00NLQ10JA000001258340202012110819', 'F00NLQ10JA000001258430202012110819', 'F00NLQ10JA000001258460202012110819', 'F00NLQ10JA000001258400202012110819', 'F00NLQ10JA000001423550202105250316', 'F00NLQ10JA000001258490202012110819', 'F00NLQ10JA000001259000202012110819', 'F00NLQ10JA000001259030202012110819', 'F00NLQ10JA000001259060202012110819', 'F00NLQ10JA000001259090202012110819', 'F00NLQ10JA000001259180202012110819', 'F00NLQ10JA000001259210202012110819', 'F00NLQ10JA000001259240202012110819', 'F00NLQ10JA000001259300202012110819', 'F00NLQ10JA000001259390202012110819', 'F00NLQ10JA000001259360202012110819', 'F00NLQ10JA000001259420202012110819', 'F00NLQ10JA000001259450202012110819', 'F00NLQ10JA000001259480202012110819', 'F00NLQ10JA000001259510202012110819', 'F00NLQ10JA000001259540202012110819', 'F00NLQ10JA000001259570202012110819', 'F00NLQ10JA000001259600202012110819', 'F00NLQ10JA000001259630202012110819', 'F00NLQ10JA000001259660202012110819', 'F00NLQ10JA000001259690202012110819', 'F00NLQ10JA000001258670202012110819', 'F00NLQ10JA000001258580202012110819', 'F00NLQ10JA000001258610202012110819', 'F00NLQ10JA000001258550202012110819', 'F00NLQ10JA000001423640202105250316', 'F00NLQ10JA000001258820202012110819', 'F00NLQ10JA000001258730202012110819', 'F00NLQ10JA000001258760202012110819', 'F00NLQ10JA000001423730202105250316', 'F00NLQ10JA000001258970202012110819', 'F00NLQ10JA000001258790202012110819', 'F00NLQ10JA000001258880202012110819', 'F00NLQ10JA000001258910202012110819', 'F00NLQ10JA000001258850202012110819', 'F00NLQ10JA000001423820202105250316', 'F00NLQ10JA000001258940202012110819', 'F00NLQ10JA000001599500202109280434', 'F00NLQ105R020220314065757029044073', 'F00NLQ10JA000001599590202109280434', 'F00NLQ10JA000001599410202109280434', 'F00NLQ10JA000001599770202109280434', 'F00NLQ10JA000001599680202109280434', 'F00NLQ10JA000001599950202109280434', 'F00NLQ105R020220314065757519677073', 'F00NLQ10JA000001600040202109280434', 'F00NLQ10JA000001599860202109280434', 'F00NLQ10JA000001600220202109280434', 'F00NLQ10JA000001600400202109280434', 'F00NLQ105R020220314065757968073073', 'F00NLQ10JA000001600490202109280434', 'F00NLQ10JA000001600580202109280517', 'F00NLQ10JA000001602020202109280559', 'F00NLQ10JA000001602110202109280600', 'F00NLQ105R020220314065759807169073', 'F00NLQ10JA000001601930202109280559', 'F00NLQ10JA000001600940202109280518', 'F00NLQ10JA000001601030202109280518', 'F00NLQ105R020220314065800366407073', 'F00NLQ10JA000001600760202109280517', 'F00NLQ10JA000001600850202109280517', 'F00NLQ10JA000001601390202109280518', 'F00NLQ105R020220314065800778868073', 'F00NLQ10JA000001601120202109280518', 'F00NLQ10JA000001601210202109280518', 'F00NLQ10JA000001601660202109280518', 'F00NLQ10JA000001601750202109280518', 'F00NLQ105R020220314065801225690073', 'F00NLQ10JA000001601480202109280518', 'F00NLQ10JA000001603550202109280600', 'F00NLQ105R020220314065801789566073', 'F00NLQ10JA000001603280202109280600', 'F00NLQ10JA000001603370202109280600', 'F00NLQ10JA000001602470202109280600', 'F00NLQ105R020220314065802435086073', 'F00NLQ10JA000001602200202109280600', 'F00NLQ10JA000001602290202109280600', 'F00NLQ10JA000001602740202109280600', 'F00NLQ105R020220314065802908088073', 'F00NLQ10JA000001602560202109280600', 'F00NLQ10JA000001602650202109280600', 'F00NLQ10JA000001603100202109280600', 'F00NLQ10JA000001603190202109280600', 'F00NLQ105R020220314065803329989073', 'F00NLQ10JA000001602920202109280600', 'F00NLQ10JA000001603010202109280600', 'F00NLQ10JA000001598150202109280410', 'F00NLQ105R020220314065758439994073', 'F00NLQ10JA000001598240202109280410', 'F00NLQ10JA000001598060202109280410', 'F00NLQ10JA000001598420202109280410', 'F00NLQ10JA000001598330202109280410', 'F00NLQ105R020220314065758933219073', 'F00NLQ10JA000001598690202109280410', 'F00NLQ10JA000001598510202109280410', 'F00NLQ10JA000001598870202109280410', 'F00NLQ10JA000001598780202109280410', 'F00NLQ105R020220314065759362825073', 'F00NLQ10JA000001598960202109280410', 'F00NLQ10JA000001599230202109280411', 'F00NLQ10KB020220412082138495548102', 'F00NLQ10KB020220412082139977849102', 'F00NLQ10KB020220412082141385447102', 'F00NLQ10KB020220412082142689609102', 'F00NLQ10KB020220412082143690535102', 'F00NLQ10KB020220412082145633222102', 'F00NLQ10KB020220412082146978942102', 'F00NLQ10KB020220412082148240732102')
		
			Invalid Deadlink		
			Yes	Crestine

			{
			    "response": {
			        "task_uuid": "737481ea-fbdf-438c-8836-37742b55e74b"
			    },
			    "version": "1.0"
			}

	# BRIGHT DATA SCRIPT
		# Sorry for the inconvenience. We cannot cater rerun for now due to brightdata subscription issue. We will update once the issue is fixed

		# Brightdata subscription issue was resolved, we can now accept rerun requests with love 

09 04 22
	TEchvalidation Issues:
		7150	9/4/2022	No	110	in_stock	amazon.nl	
			110NLWQ0RV020211109092305622466313	
			-2 in_stock

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('110NLWQ0RV020211109092305622466313')

			{
			    "response": {
			        "task_uuid": "7dc95d92-1111-4a23-8d02-1f3eff53e6f6"
			    },
			    "version": "1.0"
			}

		(R D-1) 7151	9/4/2022	No	F00	product_website	base.be/nl/	
			F00BEQX0KB020220412082139512717102	
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('F00BEQX0KB020220412082139512717102')

			{
			    "response": {
			        "task_uuid": "d631fda1-e5c8-41c6-a5e0-12ccc8cd166f"
			    },
			    "version": "1.0"
			}

		(R D-2) 7152	9/4/2022	No	K00	in_stock	ebuyer.com	
			K00GB320CG000001358780202103290840
			K00GB3203F000001453320202106160816
			K00GB320CG000001244350202011170352
			K00GB320CG000001360220202103300725
			K00GB320CG000001400430202105040624
			K00GB3204K000001541270202108110154
			K00GB320U1020220606030426556981157
			-1 in_stock

			{
			    "response": {
			        "task_uuid": "6550a083-b0b3-4249-af45-0a0a04f803c0"
			    },
			    "version": "1.0"
			}

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00GB320CG000001358780202103290840', 'K00GB3203F000001453320202106160816', 'K00GB320CG000001244350202011170352', 'K00GB320CG000001360220202103300725', 'K00GB320CG000001400430202105040624', 'K00GB3204K000001541270202108110154', 'K00GB320U1020220606030426556981157')

			{
			    "response": {
			        "task_uuid": "10524efc-3aed-499d-8666-892e60056039"
			    },
			    "version": "1.0"
			}

		(R D-1) 7153	9/4/2022	No	K00	in_stock	elgiganten.se	
			K00SE3003F000001307520202101210507	
			-1 in_stock

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00SE3003F000001307520202101210507')

			{
			    "response": {
			        "task_uuid": "56a14b61-741d-405c-99a3-4f23105a0074"
			    },
			    "version": "1.0"
			}

		(R D-1) 7154	9/4/2022	No	200	in_stock	elgiganten.se	
			200SE30080000001246110202011200116	
			-1 in_stock

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('200SE30080000001246110202011200116')

			{
			    "response": {
			        "task_uuid": "3bf425d8-7221-451a-8f73-a5f8e395463c"
			    },
			    "version": "1.0"
			}

		(R D-2) 7155	9/4/2022	No	K00	in_stock	euro.com.pl	
			K00PLD803F000001308170202101210525
			K00PLD80CG000001298310202101180406
			K00PLD807U020220620051758004743171
			K00PLD803F000001308530202101210525
			K00PLD80CG000001409710202105110503
			K00PLD80CG000001298320202101180406
			K00PLD80KO000001448060202106150451
			K00PLD807U020220620055714358369171
			K00PLD807U020220627045522233866178
			-1 in_stock

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00PLD803F000001308170202101210525', 'K00PLD80CG000001298310202101180406', 'K00PLD807U020220620051758004743171', 'K00PLD803F000001308530202101210525', 'K00PLD80CG000001409710202105110503', 'K00PLD80CG000001298320202101180406', 'K00PLD80KO000001448060202106150451', 'K00PLD807U020220620055714358369171', 'K00PLD807U020220627045522233866178')

			{
			    "response": {
			        "task_uuid": "b1cc34ce-00d7-4fac-9227-614762f9b9f0"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "72d261c2-6f5e-465b-9519-ba6a44553c36"
			    },
			    "version": "1.0"
			}

		(R D-2) 7156	9/4/2022	No	K00	in_stock,product_website	jarir.com	
			K00SABA0CG000001492580202107190651	
			-1 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00SABA0CG000001492580202107190651')

			{
			    "response": {
			        "task_uuid": "e77266ce-d3af-4d80-83e4-e85408a657b5"
			    },
			    "version": "1.0"
			}

		(R D-1) 7157	9/4/2022	No	A00	product_website	microsoft.com/en-au	
			A00AUE40RB000000877900201911080805	
		
			-1 product_website
			7158	9/4/2022	No	A00	product_website	microsoft.com/en-gb	
				A00GB840RB000000876430201911071433
				A00GB840GC000000322900201903041009
				A00GB840RB000001237170202011060516
				A00GB840RB000000874070201911070317
				A00GB840RB000000874290201911070556
				A00GB840RB000001237200202011060516
				-2 product_website
			7159	9/4/2022	No	A00	in_stock	microsoft.com/en-sg	
				A00SG350KT020220329120131403937088
				A00SG350KT020220329120132268615088
				A00SG350KT020220808042456047531220
				A00SG350KT020220329120131028868088
				A00SG350KT020220329120131210793088
				A00SG350KT020220329120131841402088
				A00SG350KT020220329120130798215088
				A00SG350KT020220808041350162424220
				A00SG350KT020220329120132458010088
				A00SG350KT020220808042456351913220
				A00SG350KT020220808042456850092220
				A00SG350KT020220329120130556705088
				A00SG350KT020220808042456726053220
				A00SG350KT020220329120131614355088
				A00SG350KT020220808042456599346220
				A00SG350KT020220808042456470318220
				A00SG350KT020220329120132070935088
				A00SG350KT020220808042456190056220
				-1 in_stock
			7160	9/4/2022	No	A00	in_stock,price_value	microsoft.com/en-sg	
				A00SG350KT020220808041350102217220
				A00SG350KT020220808042455896498220
				A00SG350KT020220808041350007468220
				-1 in_stock,-2 price_value
			7161	9/4/2022	No	A00	in_stock	microsoft.com/ja-jp	
				A00JPD40KT020211012070247955693285
				A00JPD40KT020220808041347723540220
				A00JPD40KT020211012070246163032285
				A00JPD40KT020211012070247661588285
				A00JPD40KT020211012070245446582285
				A00JPD40KT020211012070246480897285
				A00JPD40KT020211012070245035822285
				A00JPD40RB000001425710202106010136
				A00JPD40RB000001345300202103100636
				A00JPD40KT020211012070245820884285
				A00JPD40KT020211012070246913303285
				A00JPD40RB000001425600202106010136
				-2 in_stock

			7162	9/4/2022	No	A00	product_website	microsoft.com/nl-nl	
				A00NLP40RB000001237020202011060514	
				-1 product_website

			7166	9/4/2022	Yes	200	description	mediamarkt.be/fr	
				200BES80NC000001594350202109210326
				200BES80PT020220726014824786539207
				200BES80NC000001594110202109210326
				200BES8070000001474450202107060614
				-1 description

		(R D-1) 7163	9/4/2022	No	F00	product_website	mobiel.nl	
			F00NLRU05R020220214072903353438045
			F00NLRU05R020220214072848693678045
			F00NLRU0JA000001418710202105180355
			-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('F00NLRU05R020220214072903353438045', 'F00NLRU05R020220214072848693678045', 'F00NLRU0JA000001418710202105180355')

			{
			    "response": {
			        "task_uuid": "2bc419d7-1e5e-4970-81ab-410ae0a0ad05"
			    },
			    "version": "1.0"
			}

		(R D-1) 7164	9/4/2022	No	100	in_stock,product_website	power.dk	
			100DKX20ZQ020220718044741557055199	
			-3 in_stock,-1 product_website

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('100DKX20ZQ020220718044741557055199')

			{
			    "response": {
			        "task_uuid": "bb7e663c-50c5-4909-9002-a48299603236"
			    },
			    "version": "1.0"
			}

		(R D-2) 7165	9/4/2022	Yes	200	description	boulanger.com	
			200FR3107R020220711022357196803192
			200FR3107R020220711022358070103192
			200FR3108R020220711022358767960192
			200FR3108R020220711022359165038192
			-1 description

			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('200FR3107R020220711022357196803192', '200FR3107R020220711022358070103192', '200FR3108R020220711022358767960192', '200FR3108R020220711022359165038192')

			{
			    "response": {
			        "task_uuid": "2c2d2e48-03a2-4a3a-bcac-d42ff6805471"
			    },
			    "version": "1.0"
			}

			# no description found
			# https://prnt.sc/PUxUhVr9kLe-

	(T D-5) Techvalidation Report:
		September 04, 2022 Technical Validation Updates

		Numbers of issues last update: 21
		Numbers of issues today: 17

		FIXED BY RERUN:
		Razer K00:
		www.ebuyer.com | -1 in_stock
		www.elgiganten.se | -1 in_stock
		www.elgiganten.se | -1 in_stock
		www.euro.com.pl | -1 in_stock

		Samsung Mobile F00:
		www.base.be/nl/ | -1 product_website
		www.mobiel.nl | Proxy Issue

		Garmin 100:
		www.power.dk | -3 in_stock,-1 product_website

		Zound 200:
		www.boulanger.com | -1 description

		FIXED BY CODE UPDATE:
		Nintendo Benelux 110: 
		www.amazon.nl | -2 in_stock

		Work in progress:
		None

	Daily Issues:
		(R D-3) 9632	2022-09-04	F00	all	base.be/nl/	
			All Item_IDs EXCEPT
			F00BENX040000001636690202111040406
			F00BEQX05R020220214072844451069045
			F00BEQX05R020220214072853307321045
			F00BEQX05R020220214072855536950045
			F00BEQX05R020220214072828821302045
			F00BEQX05R020220214072841509309045
			F00BEQX05R020220412082121376343102
			F00BENX0JA000001637980202111040406
			F00BENX0JA000001639830202111040407
			F00BENX0JA000001638380202111040406
			F00BEQX0KB020211207020301540066341
			F00BEQX0KB020220412082139512717102
			invalid deadlink		
			Yes	Glenn
			
			SELECT * FROM view_all_productdata where date='2022-09-04' AND website in ('www.base.be/nl/') AND item_id not in ('F00BENX040000001636690202111040406', 'F00BEQX05R020220214072844451069045', 'F00BEQX05R020220214072853307321045', 'F00BEQX05R020220214072855536950045', 'F00BEQX05R020220214072828821302045', 'F00BEQX05R020220214072841509309045', 'F00BEQX05R020220412082121376343102', 'F00BENX0JA000001637980202111040406', 'F00BENX0JA000001639830202111040407', 'F00BENX0JA000001638380202111040406', 'F00BEQX0KB020211207020301540066341', 'F00BEQX0KB020220412082139512717102') ORDER BY SOURCE

			{
			    "response": {
			        "task_uuid": "73b0c12c-26c0-4a00-8925-849f23db0e17"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1eeb753f-9bee-4e16-a610-07437df07b51"
			    },
			    "version": "1.0"
			}

			# 20 item_ids
			{
			    "response": {
			        "task_uuid": "dd027168-68e1-4f84-b92f-fb4bbfaaa3ca"
			    },
			    "version": "1.0"
			}

			# 20 item_Ids
			{
			    "response": {
			        "task_uuid": "6f781346-a534-4435-84ef-4cc224ccb1cd"
			    },
			    "version": "1.0"
			}

			# 20 item_ids
			{
			    "response": {
			        "task_uuid": "6173786c-6c7a-401d-b620-0a43a76a1b22"
			    },
			    "version": "1.0"
			}

			# 20 item_ids sent
			{
			    "response": {
			        "task_uuid": "915e3911-d874-45ea-9f08-714fd9421b21"
			    },
			    "version": "1.0"
			}

			# 20 item_ids
			{
			    "response": {
			        "task_uuid": "022cb0cd-50ed-4ed5-ad98-960ad2752151"
			    },
			    "version": "1.0"
			}
			# 10 item_ids

			{
			    "response": {
			        "task_uuid": "50c16fd8-15e1-42c6-b90d-87a344190326"
			    },
			    "version": "1.0"
			}		

			# 10 item_ids left
			{
			    "response": {
			        "task_uuid": "f2d06c6c-9b00-439b-a69d-dd75e974e290"
			    },
			    "version": "1.0"
			}

			# 37 item_ids

			{
			    "response": {
			        "task_uuid": "a4ff4a63-2a55-4874-b2a1-977740c1a58a"
			    },
			    "version": "1.0"
			}

		(R D-1) 9633	2022-09-04	610	price_value	imerco.dk	
			610DKNW01P000001478780202107130121
			610DKNW01P000001479240202107130121
			610DKNW01P000001479300202107130121
			610DKNW01P000001480880202107130122
			610DKNW01P000001480900202107130122
			610DKNW01P000001480920202107130122
			610DKNW01P000001480580202107130122
			610DKNW00P000001617390202110110431
			610DKNW00P000001616990202110110431
			610DKNW00P000001484080202107130154
			610DKNW00P000001483840202107130154
			610DKNW00P000001617090202110110431
			610DKNW00P000001484100202107130154
			610DKNW00P000001483860202107130154
			610DKNW00P000001484120202107130154
			610DKNW00P000001617190202110110431
			610DKNW00P000001483880202107130154
			610DKNW00P000001617290202110110431
			610DKNW00P000001484700202107130154
			610DKNW00P000001484720202107130154
			610DKNW01P000001479440202107130122
			610DKNW01P000001480620202107130122
			610DKNW00P000001483420202107130154
			610DKNW00P000001483440202107130154
			610DKNW00P000001483460202107130154
			610DKNW00P000001483480202107130154
			610DKNW00P000001483760202107130154
			610DKNW00P000001483780202107130154
			610DKNW00P000001617590202110110431
			610DKNW00P000001617690202110110431
			610DKNW00P000001617790202110110431
			610DKNW00P000001617890202110110431
			610DKNW01P000001480380202107130122
			610DKNW01P000001480180202107130122
			610DKNW01P000001479880202107130122
			610DKNW01P000001479800202107130122
			610DKNW01P000001480140202107130122
			610DKNW01P000001479260202107130121
			610DKNW01P000001481080202107130122
			610DKNW01P000001480860202107130122
			610DKNW01P000001479460202107130122
			610DKNW01P000001479480202107130122
			610DKNW01P000001479500202107130122
			610DKNW00P000001484260202107130154
			610DKNW00P000001484300202107130154
			610DKNW00P000001484480202107130154
			610DKNW00P000001484320202107130154
			610DKNW00P000001484340202107130154
			610DKNW00P000001484360202107130154
			610DKNW00P000001484500202107130154
			610DKNW00P000001484520202107130154
			610DKNW01P000001479280202107130121
			610DKNW01P000001480320202107130122
			610DKNW00P000001484460202107130154
			610DKNW00P000001484240202107130154
			610DKNW00P000001616790202110110431
			610DKNW00P000001616890202110110431
			610DKNW00P000001483820202107130154
			610DKNW00P000001484280202107130154
			610DKNW00P000001483800202107130154
			610DKNW00P000001617490202110110431
			610DKNW00P000001484640202107130154
			610DKNW01P000001479840202107130122
			610DKNW01P000001479600202107130122
			610DKNW01P000001478680202107130121
			610DKNW01P000001480520202107130122
			610DKNW01P000001480280202107130122
			610DKNW01P000001479320202107130121
			610DKNW01P000001479560202107130122
			610DKNW00P000001484760202107130154
			Incorrect Price Scraped. Should Scrape NOT A MEMBER Price.	
			https://prnt.sc/5y5bzrQeJwCA	
			Yes	Keeshia

			{
			    "response": {
			        "task_uuid": "dff31486-abbd-41da-a2a8-0e3c8667e67e"
			    },
			    "version": "1.0"
			}

		9634	2022-09-04	F00	all	orange.be/nl/	
			All Item_IDs EXCEPT
			F00BEOX0ME020220412075010591764102
			F00BELX040000001635600202111040406
			F00BELX040000001635670202111040406
			F00BELX040000001637030202111040406
			F00BELX040000001637100202111040406
			F00BEOX0KB020220329062758017417088
			F00BELX0JA000001639410202111040406
			F00BELX0JA000001639710202111040407
			F00BELX0JA000001638660202111040406
			F00BELX0JA000001638870202111040406
			invalid deadlink		
			Yes	Glenn

			SELECT * FROM view_all_productdata where date='2022-09-04' AND website = 'www.orange.be/nl/' AND item_id not in ('F00BEOX0ME020220412075010591764102', 'F00BELX040000001635600202111040406', 'F00BELX040000001635670202111040406', 'F00BELX040000001637030202111040406', 'F00BELX040000001637100202111040406', 'F00BEOX0KB020220329062758017417088', 'F00BELX0JA000001639410202111040406', 'F00BELX0JA000001639710202111040407', 'F00BELX0JA000001638660202111040406', 'F00BELX0JA000001638870202111040406') 

			{
			    "response": {
			        "task_uuid": "8f49d56f-beac-40b8-a543-41ff50ab68e0"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "5365dbfa-7180-4442-8778-4331807ef73d"
			    },
			    "version": "1.0"
			}

			# 36 item_ids
			{
		    "response": {
			        "task_uuid": "fd576637-2b5a-4ab8-a582-325eae4e5a1e"
			    },
			    "version": "1.0"
			}

		9660	2022-09-04	K00	all	power.dk	
			SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00DKX204K000001554290202108180340', 'K00DKX203F000001554300202108180340', 'K00DKX204K000001554280202108180340', 'K00DKX20P1020220607015131520664158', 'K00DKX20P1020220607015130788518158', 'K00DKX20QB220220607015131086149158', 'K00DKX206Q000001554270202108180340', 'K00DKX20P1020220607015131379672158', 'K00DKX20RB220220607015131667579158', 'K00DKX203F000001554330202108180340', 'K00DKX207U020211115034617585894319', 'K00DKX203F000001554320202108180340', 'K00DKX203F000001554340202108180340', 'K00DKX207U020220627045501445569178', 'K00DKX207U020220328081013610063087', 'K00DKX207U020220328081013738174087', 'K00DKX207U020220627045503191793178', 'K00DKX20CG000001591700202109200556', 'K00DKX207U020211115034617459941319', 'K00DKX20CG000001492480202107190605', 'K00DKX207U020220627045503045272178', 'K00DKX20CG000001487100202107130336', 'K00DKX207U020220627045503507865178', 'K00DKX20CG000001521820202107270631', 'K00DKX20CG000001487070202107130336', 'K00DKX207U020220627045502856508178', 'K00DKX20CG000001487080202107130336', 'K00DKX20CG000001487130202107130336', 'K00DKX20CG000001487140202107130336', 'K00DKX20CG000001487180202107130336', 'K00DKX20CG000001487150202107130336', 'K00DKX20CG000001487170202107130336', 'K00DKX20CG000001487230202107130336', 'K00DKX20CG000001487200202107130336', 'K00DKX20CG000001487210202107130336', 'K00DKX20CG000001487220202107130336', 'K00DKX20CG000001489680202107130337', 'K00DKX20CG000001487270202107130336', 'K00DKX20CG000001487240202107130336', 'K00DKX20CG000001487250202107130336', 'K00DKX20CG000001487260202107130336', 'K00DKX20CG000001487300202107130336', 'K00DKX20CG000001487280202107130336', 'K00DKX20CG000001487290202107130336', 'K00DKX20CG000001487350202107130336', 'K00DKX20CG000001487320202107130336', 'K00DKX20CG000001487340202107130336', 'K00DKX20CG000001487310202107130336', 'K00DKX20CG000001487330202107130336', 'K00DKX207U020211115034617953127319', 'K00DKX207U020211115034617710894319', 'K00DKX20CG000001487370202107130336', 'K00DKX20CG000001487360202107130336', 'K00DKX20CG000001487380202107130336', 'K00DKX20CG000001487390202107130336', 'K00DKX20CG000001521850202107270631', 'K00DKX20CG000001487400202107130336', 'K00DKX207U020220222101458237368053', 'K00DKX20CG000001489860202107130337', 'K00DKX207U020220328081016040271087', 'K00DKX20CG000001591730202109200556', 'K00DKX20CG000001591710202109200556', 'K00DKX207U020211115034625803001319', 'K00DKX207U020211115034625957914319', 'K00DKX20CG000001487440202107130336', 'K00DKX207U020220627045501740027178', 'K00DKX20CG000001487460202107130336', 'K00DKX207U020211115034626116820319', 'K00DKX20CG000001591740202109200556', 'K00DKX20CG000001487450202107130336', 'K00DKX207U020220222101454994525053', 'K00DKX207U020220222101455312646053', 'K00DKX207U020220222101455144478053', 'K00DKX207U020211115034618609909319', 'K00DKX207U020220627045502193632178', 'K00DKX207U020220627045502038459178', 'K00DKX207U020220627045502646415178', 'K00DKX207U020220627045502480204178', 'K00DKX207U020220627045502326390178', 'K00DKX20CG000001487470202107130336', 'K00DKX207U020220627045501892461178', 'K00DKX207U020211115034618346439319', 'K00DKX20CG000001487480202107130336', 'K00DKX20CG000001487490202107130336', 'K00DKX20CG000001582810202109130551', 'K00DKX20CG000001487560202107130336', 'K00DKX20CG000001487550202107130336', 'K00DKX20CG000001487530202107130336', 'K00DKX20CG000001487510202107130336', 'K00DKX207U020211115034625499698319', 'K00DKX207U020211115034625341761319', 'K00DKX20CG000001487520202107130336', 'K00DKX207U020211115034617341801319', 'K00DKX20CG000001487580202107130336', 'K00DKX207U020220222101456698472053', 'K00DKX207U020220222101456877323053', 'K00DKX20CG000001487600202107130336', 'K00DKX207U020220627045500719610178', 'K00DKX20CG000001487570202107130336', 'K00DKX207U020220620055717755990171', 'K00DKX207U020220222101457040487053', 'K00DKX207U020220222101457728323053', 'K00DKX207U020220222101457903969053', 'K00DKX207U020220222101457379719053', 'K00DKX207U020220627045500879304178', 'K00DKX20CG000001487620202107130336', 'K00DKX20CG000001487610202107130336', 'K00DKX207U020220222101457215622053', 'K00DKX207U020220222101457559740053', 'K00DKX20CG000001487700202107130336', 'K00DKX20CG000001487690202107130336', 'K00DKX207U020220222101458584044053', 'K00DKX207U020220627045503817276178', 'K00DKX207U020220627045503663698178', 'K00DKX207U020211115034618091536319', 'K00DKX207U020211115034618215840319', 'K00DKX20CG000001487660202107130336', 'K00DKX207U020220222101458403890053', 'K00DKX20CG000001487710202107130336', 'K00DKX20CG000001487730202107130336', 'K00DKX20CG000001487720202107130336', 'K00DKX20CG000001487760202107130336', 'K00DKX20CG000001487750202107130336', 'K00DKX207U020220627045500277888178', 'K00DKX20CG000001487770202107130336', 'K00DKX207U020220627045503363046178', 'K00DKX20FO000001554350202108180340', 'K00DKX20SB220220607015130926413158', 'K00DKX20CG000001487780202107130336', 'K00DKX205K000001554360202108180340', 'K00DKX207U020220627045500448610178', 'K00DKX205K000001554370202108180340', 'K00DKX207U020211115034626270624319', 'K00DKX20TB220220607015131223689158')
				invalid deadlink		
				Yes	Ash

			9661	2022-09-04	K00	all	power.fi	all items	
				invalid deadlink		
				Yes	Ash

				SELECT * FROM view_all_productdata where date='2022-09-04' AND website = 'www.power.fi' and source != 'etl'

			9662	2022-09-04	K00	all	mediamarkt.de	
				SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('03O000001433040202106020354')SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00DE8103O000001433080202106020354', 'K00DE8107O000001433240202106020354', 'K00DE8103O000001433120202106020354', 'K00DE8103O000001432960202106020354', 'K00DE8103O000001432980202106020354', 'K00DE8107O000001433270202106020354', 'K00DE810AD000001537360202108110153', 'K00DE810AD000001537420202108110153', 'K00DE810AD000001433440202106020354', 'K00DE8107O000001433210202106020354', 'K00DE810AD000001537330202108110153', 'K00DE810AD000001433340202106020354', 'K00DE8107O000001433180202106020354', 'K00DE810AD000001537240202108110153', 'K00DE8109O000001433530202106020354', 'K00DE8109O000001433610202106020354', 'K00DE8108O000001433510202106020354', 'K00DE810AD000001537470202108110153', 'K00DE810AD000000925060202002260844', 'K00DE8109O000001433550202106020354', 'K00DE810AD000001433460202106020354', 'K00DE8109O000001433590202106020354', 'K00DE8109O000001433570202106020354', 'K00DE8104K000001537550202108110153', 'K00DE8104K000001537520202108110153', 'K00DE8104K000001537730202108110153', 'K00DE8104K000001433740202106020354', 'K00DE8104K000001537600202108110153', 'K00DE8104K000001433710202106020354', 'K00DE810MD000001433620202106020354', 'K00DE8104K000001537880202108110153', 'K00DE8104K000001537830202108110153', 'K00DE8104K000001537980202108110153', 'K00DE8104K000001537900202108110153', 'K00DE8104K000001433760202106020354', 'K00DE8104K000001433790202106020354', 'K00DE8103F000001538250202108110153', 'K00DE8103F000001305700202101210359', 'K00DE8103F000000989150202007060853', 'K00DE8103F000001305660202101210359', 'K00DE8103F000000925340202002260844', 'K00DE8103F000001305720202101210359', 'K00DE8103F000000925210202002260844', 'K00DE8103F000001538310202108110153', 'K00DE8103F000001305550202101210359', 'K00DE8103F000001305560202101210359', 'K00DE8103F000000989100202007060853', 'K00DE8103F000001538460202108110153', 'K00DE8103F000001305600202101210359', 'K00DE8103F000001538540202108110153', 'K00DE8107U020211116100220150117320', 'K00DE8107U020220627051227709683178', 'K00DE8107U020220627051228390733178', 'K00DE8107U020220627051228748462178', 'K00DE8107U020220627051229096428178', 'K00DE8107U020220328091559811595087', 'K00DE810FF000001433900202106020354', 'K00DE810DO000001433920202106020354', 'K00DE8107U020220627051232283772178', 'K00DE810CG000001209630202009280507', 'K00DE810CG000001244940202011170410', 'K00DE8107U020220627051231670659178', 'K00DE810CG000001465180202107050547', 'K00DE8107U020220328091600482315087', 'K00DE810CG000001465220202107050547', 'K00DE810CG000001399340202105040624', 'K00DE8107U020220328091601764950087', 'K00DE8107U020220328091602059849087', 'K00DE8107U020220328091601134708087', 'K00DE8107U020220328091601448556087', 'K00DE810CG000001399220202105040624', 'K00DE810CG000001399260202105040624', 'K00DE8107U020220328091603156646087', 'K00DE8107U020220328091602629365087', 'K00DE810CG000001354840202103290839', 'K00DE810CG000001399540202105040624', 'K00DE810CG000001399500202105040624', 'K00DE810CG000001354880202103290839', 'K00DE810CG000001399580202105040624', 'K00DE810CG000001399620202105040624', 'K00DE810CG000001354920202103290839', 'K00DE8107U020220328091603960456087', 'K00DE810CG000001525140202108020807', 'K00DE810CG000001360020202103300725', 'K00DE810CG000001360060202103300725', 'K00DE810CG000001354440202103290839', 'K00DE810CG000001354960202103290839', 'K00DE810CG000001296480202101180224', 'K00DE810CG000001296560202101180224', 'K00DE8107U020211116100222145050320', 'K00DE810CG000001296580202101180224', 'K00DE810CG000001244910202011170410', 'K00DE8107U020211116100220487001320', 'K00DE8107U020220222033738308634053', 'K00DE810CG000001296550202101180224', 'K00DE810CG000001296720202101180225', 'K00DE8107U020220328091604758720087', 'K00DE810CG000001593010202109200649', 'K00DE8107U020220627051229421441178', 'K00DE8107U020211116100225927115320', 'K00DE810CG000001593050202109200649', 'K00DE810CG000001296630202101180225', 'K00DE8107U020220222033731637636053', 'K00DE8107U020220222033731213023053', 'K00DE8107U020220222033730439249053', 'K00DE8107U020220222033730816180053', 'K00DE8107U020220627051230446732178', 'K00DE8107U020211116100224500702320', 'K00DE810CG000001296700202101180225', 'K00DE8107U020220627051229777250178', 'K00DE810CG000001296640202101180225', 'K00DE8107U020211116100224146332320', 'K00DE8107U020220425061224824808115', 'K00DE8107U020220328091605055809087', 'K00DE810CG000000923230202002190707', 'K00DE8107U020220222033728556788053', 'K00DE810CG000001393510202104280059', 'K00DE810CG000000923030202002190707', 'K00DE810CG000001296740202101180225', 'K00DE8107U020211116100223152683320', 'K00DE8107U020220222033729321626053', 'K00DE810CG000001296710202101180225', 'K00DE8107U020211116100222470321320', 'K00DE8107U020220222033728935954053', 'K00DE810CG000001210060202009280712', 'K00DE8107U020220222033729654321053', 'K00DE810CG000001296510202101180224', 'K00DE8107U020220621020739744206172', 'K00DE8107U020220222033737037876053', 'K00DE8107U020220221145418946227052', 'K00DE8107U020220221145418505624052', 'K00DE8107U020220627051227392168178', 'K00DE810CG000001409400202105110503', 'K00DE810CG000001409370202105110503', 'K00DE8107U020220222033736217661053', 'K00DE810CG000001410770202105110909', 'K00DE8107U020220222033739123963053', 'K00DE8107U020220627051233270915178', 'K00DE810CG000001244970202011170410', 'K00DE8107U020220328091605334599087', 'K00DE8107U020211116100223490366320', 'K00DE8107U020211116100223828491320', 'K00DE810CG000001296610202101180225', 'K00DE8107U020220621020738658163172', 'K00DE8107U020220621020731833546172', 'K00DE8107U020220621020738309469172', 'K00DE810CG000001465140202107050547', 'K00DE8107U020220621020735377148172', 'K00DE810CG000000923580202002190707', 'K00DE810EO000001433960202106020354', 'K00DE810CG000001296660202101180225', 'K00DE810FO000001538680202108110154', 'K00DE810GO000001434080202106020354', 'K00DE810EO000001434020202106020354', 'K00DE810GO000001434100202106020354', 'K00DE810HO000001434140202106020354')
				invalid deadlink		
				Yes	Ash

				# 34 item_ids
				{
				    "response": {
				        "task_uuid": "d8fa1ab3-be7f-4e09-b8e8-a072e6d2d4ef"
				    },
				    "version": "1.0"
				}

			9663	2022-09-04	K00	all	saturn.de	
				SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00DE910CG000000922760202002190707', 'K00DE910CG000000923010202002190707', 'K00DE910CG000000923120202002190707', 'K00DE910CG000000923570202002190707', 'K00DE9103F000000925200202002260844', 'K00DE9103F000000925330202002260844', 'K00DE9103F000000925480202002260844', 'K00DE910CG000000980960202006250959', 'K00DE910CG000000980970202006250959', 'K00DE910CG000000980980202006250959', 'K00DE9103F000000990240202007060921', 'K00DE9103F000000990290202007060921', 'K00DE910CG000001209230202009280432', 'K00DE910CG000001209430202009280448', 'K00DE910CG000001245350202011170417', 'K00DE910CG000001245360202011170417', 'K00DE910CG000001245370202011170417', 'K00DE910CG000001245390202011170417', 'K00DE910CG000001300740202101180540', 'K00DE910CG000001300780202101180540', 'K00DE910CG000001300800202101180540', 'K00DE910CG000001300810202101180540', 'K00DE910CG000001300820202101180540', 'K00DE910CG000001300830202101180540', 'K00DE910CG000001300850202101180540', 'K00DE910CG000001300890202101180540', 'K00DE910CG000001300900202101180540', 'K00DE910CG000001300910202101180540', 'K00DE910CG000001300930202101180540', 'K00DE910CG000001300960202101180540', 'K00DE910CG000001300980202101180540', 'K00DE910CG000001300990202101180540', 'K00DE910CG000001301010202101180540', 'K00DE9103F000001305820202101210411', 'K00DE9103F000001305830202101210411', 'K00DE9103F000001305850202101210411', 'K00DE9103F000001305910202101210411', 'K00DE9103F000001305960202101210411', 'K00DE910CG000001354870202103290839', 'K00DE910CG000001354950202103290839', 'K00DE910CG000001355060202103290839', 'K00DE910CG000001355100202103290839', 'K00DE910CG000001355140202103290839', 'K00DE910CG000001373370202104050819', 'K00DE910CG000001373410202104050819', 'K00DE910CG000001393520202104280059', 'K00DE910CG000001399160202105040624', 'K00DE910CG000001399240202105040624', 'K00DE910CG000001399320202105040624', 'K00DE910CG000001399360202105040624', 'K00DE910CG000001399400202105040624', 'K00DE910CG000001399440202105040624', 'K00DE910CG000001399480202105040624', 'K00DE910CG000001399520202105040624', 'K00DE910CG000001399560202105040624', 'K00DE910CG000001399600202105040624', 'K00DE910CG000001399640202105040624', 'K00DE910CG000001399680202105040624', 'K00DE910CG000001409360202105110503', 'K00DE910CG000001409390202105110503', 'K00DE910CG000001410740202105110851', 'K00DE9103O000001432970202106020354', 'K00DE9103O000001433000202106020354', 'K00DE9103O000001433030202106020354', 'K00DE9103O000001433070202106020354', 'K00DE9103O000001433110202106020354', 'K00DE9103O000001433140202106020354', 'K00DE9107O000001433230202106020354', 'K00DE9107O000001433260202106020354', 'K00DE910AD000001433450202106020354', 'K00DE9108O000001433500202106020354', 'K00DE9109O000001433560202106020354', 'K00DE9109O000001433580202106020354', 'K00DE9104K000001433670202106020354', 'K00DE9104K000001433730202106020354', 'K00DE9104K000001433750202106020354', 'K00DE9104K000001433780202106020354', 'K00DE9104K000001433810202106020354', 'K00DE910RB000001433880202106020354', 'K00DE910EO000001433980202106020354', 'K00DE910EO000001434010202106020354', 'K00DE910EO000001434040202106020354', 'K00DE910FO000001434070202106020354', 'K00DE910GO000001434090202106020354', 'K00DE910GO000001434130202106020354', 'K00DE910HO000001434150202106020354', 'K00DE910JO000001434170202106020354', 'K00DE910JO000001434190202106020354', 'K00DE9104O000001434420202106020354', 'K00DE910CG000001465150202107050547', 'K00DE910CG000001492550202107190651', 'K00DE910CG000001506720202107200728', 'K00DE910CG000001525160202108020807', 'K00DE910AD000001537300202108110153', 'K00DE910AD000001537380202108110153', 'K00DE910AD000001537430202108110153', 'K00DE910AD000001537450202108110153', 'K00DE910AD000001537490202108110153', 'K00DE9104K000001537560202108110153', 'K00DE9104K000001537620202108110153', 'K00DE9104K000001537790202108110153', 'K00DE9104K000001537850202108110153', 'K00DE9104K000001537950202108110153', 'K00DE9104K000001538000202108110153', 'K00DE9103F000001538480202108110153', 'K00DE9103F000001538560202108110154', 'K00DE910FO000001538750202108110154', 'K00DE910CG000001592900202109200649', 'K00DE910CG000001592940202109200649', 'K00DE910CG000001593020202109200649', 'K00DE910CG000001593060202109200649', 'K00DE9107U020211026061940841501299', 'K00DE9107U020211116100219909904320', 'K00DE9107U020211116100220206431320', 'K00DE9107U020211116100220568622320', 'K00DE9107U020211116100220924393320', 'K00DE9107U020211116100221243171320', 'K00DE9107U020211116100221891133320', 'K00DE9107U020211116100222210752320', 'K00DE9107U020211116100222543795320', 'K00DE9107U020211116100222874402320', 'K00DE9107U020211116100223226704320', 'K00DE9107U020211116100223547282320', 'K00DE9107U020211116100223883954320', 'K00DE9107U020211116100224207986320', 'K00DE9107U020211116100224558556320', 'K00DE9107U020211116100224888957320', 'K00DE9107U020211116100225263752320', 'K00DE9107U020211116100225640872320', 'K00DE9107U020211116100225996694320', 'K00DE9107U020211116100226360040320', 'K00DE9107U020220222033728646224053', 'K00DE9107U020220222033728995547053', 'K00DE9107U020220222033729375385053', 'K00DE9107U020220222033729710174053', 'K00DE9107U020220222033730522526053', 'K00DE9107U020220222033730873831053', 'K00DE9107U020220222033731300993053', 'K00DE9107U020220222033731727696053', 'K00DE9107U020220222033732162166053', 'K00DE9107U020220222033732569074053', 'K00DE9107U020220222033732986357053', 'K00DE9107U020220222033733325035053', 'K00DE9107U020220222033733725792053', 'K00DE9107U020220222033734143550053', 'K00DE9107U020220222033734467952053', 'K00DE9107U020220222033734780730053', 'K00DE9107U020220222033735580840053', 'K00DE9107U020220222033735918605053', 'K00DE9107U020220222033736305489053', 'K00DE9107U020220222033736740474053', 'K00DE9107U020220222033737442793053', 'K00DE9107U020220222033738397549053', 'K00DE9107U020220222033738794826053', 'K00DE9107U020220222033739183846053', 'K00DE9107U020220328091559897958087', 'K00DE9107U020220328091600563417087', 'K00DE9107U020220328091601224733087', 'K00DE9107U020220328091601527650087', 'K00DE9107U020220328091601848471087', 'K00DE9107U020220328091602166208087', 'K00DE9107U020220328091602442300087', 'K00DE9107U020220328091602687267087', 'K00DE9107U020220328091603774813087', 'K00DE9107U020220328091604019774087', 'K00DE9107U020220328091604260621087', 'K00DE9107U020220328091604541913087', 'K00DE9107U020220328091604839392087', 'K00DE9107U020220328091605118108087', 'K00DE9107U020220328091605415801087', 'K00DE9107U020220425061224916242115', 'K00DE910QB220220607015123308892158', 'K00DE910Q1020220607015123597089158', 'K00DE910Q1020220607015124030004158', 'K00DE9102N020220607015124175469158', 'K00DE910P1020220607015124325661158', 'K00DE910U1020220607015124468916158', 'K00DE910U1020220607015124613329158', 'K00DE910U1020220607015124783269158', 'K00DE910P1020220607015124903874158', 'K00DE910P1020220607015125189025158', 'K00DE910P1020220607015125338172158', 'K00DE910P1020220607015125477528158', 'K00DE910SB220220607015125638808158', 'K00DE9107U020220621020731905744172', 'K00DE9107U020220621020733131041172', 'K00DE9107U020220621020736620529172', 'K00DE9107U020220621020737788391172', 'K00DE9107U020220621020739797623172', 'K00DE9107U020220627051226247492178', 'K00DE9107U020220627051226846924178', 'K00DE9107U020220627051227155049178', 'K00DE9107U020220627051227450511178', 'K00DE9107U020220627051227780493178', 'K00DE9107U020220627051228116843178', 'K00DE9107U020220627051228460781178', 'K00DE9107U020220627051229496746178', 'K00DE9107U020220627051229846614178', 'K00DE9107U020220627051230174880178', 'K00DE9107U020220627051230503024178', 'K00DE9107U020220627051230771015178', 'K00DE9107U020220627051231060442178', 'K00DE9107U020220627051231387709178', 'K00DE9107U020220627051231740332178', 'K00DE9107U020220627051232040799178', 'K00DE9107U020220627051232331280178', 'K00DE9107U020220627051232665253178', 'K00DE9107U020220627051233013683178')
				invalid deadlink		
				Yes	Ash

			9664	2022-09-04	K00	all	elkjop.no	
				SELECT * FROM view_all_productdata where date='2022-09-04' AND item_id in ('K00NO500CG000000922970202002190707', 'K00NO500CG000000923100202002190707', 'K00NO500AD000000924880202002260844', 'K00NO500AD000000925010202002260844', 'K00NO5003F000000925160202002260844', 'K00NO5003F000000925300202002260844', 'K00NO5003F000000925440202002260844', 'K00NO500CG000000980600202006250925', 'K00NO500CG000000980610202006250925', 'K00NO500CG000000980620202006250925', 'K00NO5005K000000987700202007060802', 'K00NO500CG000001244490202011170356', 'K00NO500CG000001244500202011170356', 'K00NO500CG000001244510202011170356', 'K00NO500CG000001244520202011170356', 'K00NO500CG000001244530202011170356', 'K00NO500CG000001244540202011170356', 'K00NO500CG000001244550202011170356', 'K00NO500CG000001297870202101180353', 'K00NO500CG000001297880202101180353', 'K00NO500CG000001297910202101180353', 'K00NO500CG000001297930202101180353', 'K00NO500CG000001297940202101180353', 'K00NO500CG000001297950202101180353', 'K00NO500CG000001297960202101180353', 'K00NO500CG000001297970202101180353', 'K00NO500CG000001297980202101180353', 'K00NO500CG000001298010202101180353', 'K00NO500CG000001298020202101180353', 'K00NO500CG000001298030202101180353', 'K00NO500CG000001298040202101180353', 'K00NO500CG000001298060202101180353', 'K00NO500CG000001298090202101180353', 'K00NO500CG000001298100202101180353', 'K00NO500CG000001298110202101180353', 'K00NO500CG000001298120202101180353', 'K00NO500CG000001298130202101180353', 'K00NO500CG000001298140202101180353', 'K00NO500CG000001356310202103290840', 'K00NO500CG000001356330202103290840', 'K00NO500CG000001356370202103290840', 'K00NO500CG000001356450202103290840', 'K00NO500CG000001356530202103290840', 'K00NO500CG000001356550202103290840', 'K00NO500CG000001356570202103290840', 'K00NO500CG000001356630202103290840', 'K00NO500CG000001356650202103290840', 'K00NO500CG000001356670202103290840', 'K00NO500CG000001356710202103290840', 'K00NO500CG000001356730202103290840', 'K00NO500CG000001360120202103300725', 'K00NO500CG000001373220202104050819', 'K00NO500CG000001373240202104050819', 'K00NO500CG000001373260202104050819', 'K00NO500CG000001393700202104280059', 'K00NO500CG000001409540202105110503', 'K00NO500CG000001409560202105110503', 'K00NO500CG000001409580202105110503', 'K00NO500CG000001409600202105110503', 'K00NO5007O000001446530202106150451', 'K00NO5007O000001446570202106150451', 'K00NO5007O000001446590202106150451', 'K00NO500AD000001446660202106150451', 'K00NO500AD000001446680202106150451', 'K00NO500MD000001446690202106150451', 'K00NO500MD000001446710202106150451', 'K00NO5004K000001446770202106150451', 'K00NO5004K000001446790202106150451', 'K00NO5004K000001446800202106150451', 'K00NO5003F000001446870202106150451', 'K00NO5003F000001446910202106150451', 'K00NO5003F000001446930202106150451', 'K00NO5003F000001446940202106150451', 'K00NO5003F000001446960202106150451', 'K00NO5003F000001447000202106150451', 'K00NO5003F000001447020202106150451', 'K00NO5003F000001447040202106150451', 'K00NO5003F000001447060202106150451', 'K00NO500FF000001447080202106150451', 'K00NO500FO000001447100202106150451', 'K00NO5005K000001447110202106150451', 'K00NO5005K000001447130202106150451', 'K00NO5004K000001450030202106150556', 'K00NO5004K000001450050202106150601', 'K00NO5004K000001450060202106150608', 'K00NO500CG000001465600202107050547', 'K00NO500CG000001465620202107050547', 'K00NO500CG000001465640202107050547', 'K00NO500CG000001492350202107190605', 'K00NO500CG000001506850202107200728', 'K00NO500CG000001525460202108020807', 'K00NO500CG000001525470202108020807', 'K00NO500CG000001525480202108020807', 'K00NO500CG000001525490202108020807', 'K00NO500AD000001554500202108180340', 'K00NO500AD000001554550202108180340', 'K00NO500AD000001554570202108180340', 'K00NO5004K000001554610202108180340', 'K00NO5004K000001554640202108180340', 'K00NO5004K000001554670202108180340', 'K00NO5004K000001554700202108180340', 'K00NO5004K000001554730202108180340', 'K00NO5004K000001554750202108180340', 'K00NO5004K000001554800202108180340', 'K00NO5004K000001554830202108180340', 'K00NO5004K000001554850202108180340', 'K00NO5004K000001554880202108180340', 'K00NO5003F000001554930202108180340', 'K00NO5003F000001554970202108180340', 'K00NO5003F000001554990202108180340', 'K00NO5003F000001555020202108180340', 'K00NO5003F000001555050202108180340', 'K00NO5003F000001555060202108180340', 'K00NO5003F000001555110202108180340', 'K00NO5003F000001555130202108180340', 'K00NO5005K000001555190202108180340', 'K00NO5005K000001555220202108180340', 'K00NO5007Q000001555250202108180340', 'K00NO5007Q000001555260202108180340', 'K00NO500CG000001582780202109130551', 'K00NO500CG000001591920202109200556', 'K00NO500CG000001591940202109200556', 'K00NO500CG000001591960202109200556', 'K00NO500CG000001591980202109200556', 'K00NO500CG000001592000202109200556', 'K00NO5007U020211115035450992952319', 'K00NO5007U020211115035451120375319', 'K00NO5007U020211115035451242231319', 'K00NO5007U020211115035451367557319', 'K00NO5007U020211115035451486007319', 'K00NO5007U020211115035451618974319', 'K00NO5007U020211115035451753440319', 'K00NO5007U020211115035451875218319', 'K00NO5007U020211115035451998432319', 'K00NO5007U020211115035452119683319', 'K00NO5007U020211115035452245752319', 'K00NO5007U020211115035452373204319', 'K00NO5007U020211115035452500890319', 'K00NO5007U020211115035452626366319', 'K00NO5007U020211115035452818871319', 'K00NO5007U020211115035453076522319', 'K00NO5007U020211115035455737436319', 'K00NO5007U020211115035456006980319', 'K00NO5007U020220222101502781833053', 'K00NO5007U020220222101503165315053', 'K00NO5007U020220222101503438315053', 'K00NO5007U020220222101503775757053', 'K00NO5007U020220222101504028616053', 'K00NO5007U020220222101504333281053', 'K00NO5007U020220222101504634760053', 'K00NO5007U020220222101504932074053', 'K00NO5007U020220222101505306323053', 'K00NO5007U020220222101505610534053', 'K00NO5007U020220222101505882123053', 'K00NO5007U020220222101506148197053', 'K00NO5007U020220222101506393065053', 'K00NO5007U020220222101506665739053', 'K00NO5007U020220222101506935236053', 'K00NO5007U020220222101507255781053', 'K00NO5007U020220222101507521490053', 'K00NO5007U020220222101507764908053', 'K00NO5007U020220222101508089478053', 'K00NO5007U020220222101508405802053', 'K00NO5007U020220222101508700567053', 'K00NO5007U020220222101509013767053', 'K00NO5007U020220222101509317226053', 'K00NO5007U020220222101509633867053', 'K00NO5007U020220222101509994669053', 'K00NO5007U020220222101510294583053', 'K00NO5007U020220222101510634654053', 'K00NO5007U020220222101510921163053', 'K00NO5007U020220328081024778650087', 'K00NO5007U020220328081024981222087', 'K00NO5007U020220328081025173887087', 'K00NO5007U020220328081025411759087', 'K00NO5007U020220328081025600261087', 'K00NO5007U020220328081025780298087', 'K00NO5007U020220328081025971199087', 'K00NO5007U020220328081026197658087', 'K00NO5007U020220328081026381168087', 'K00NO5007U020220328081026566377087', 'K00NO5007U020220328081026756208087', 'K00NO5007U020220328081026961328087', 'K00NO5007U020220328081027169053087', 'K00NO5007U020220328081027394606087', 'K00NO5007U020220328081027590752087', 'K00NO5007U020220328081027781482087', 'K00NO5007U020220328081027985081087', 'K00NO5007U020220328081028236423087', 'K00NO5007U020220328081028483082087', 'K00NO5007U020220328081028694658087', 'K00NO500TB220220608020517537608159', 'K00NO500P1020220608020517628024159', 'K00NO500P1020220608020517711350159', 'K00NO500RB220220608020517801320159', 'K00NO500QB220220608020517881537159', 'K00NO500Q1020220614060622504943165', 'K00NO500Q1020220614060622649273165', 'K00NO500Q1020220614060622805182165', 'K00NO500Q1020220614060622952786165', 'K00NO5002N020220614060623103735165', 'K00NO500P1020220614060623394009165', 'K00NO500P1020220614060623558759165', 'K00NO500P1020220614060623713191165', 'K00NO5007U020220620051751666241171', 'K00NO5007U020220620051754837161171', 'K00NO5007U020220620051757803896171', 'K00NO5007U020220620051800643493171', 'K00NO5007U020220620051803554351171', 'K00NO5007U020220620051806549962171', 'K00NO5007U020220620055712505850171', 'K00NO5007U020220620055712674777171', 'K00NO5007U020220620055713044104171', 'K00NO5007U020220620055713244078171', 'K00NO5007U020220627045513933116178', 'K00NO5007U020220627045514141966178', 'K00NO5007U020220627045514363867178', 'K00NO5007U020220627045514545512178', 'K00NO5007U020220627045514764993178', 'K00NO5007U020220627045514961912178', 'K00NO5007U020220627045515159642178', 'K00NO5007U020220627045515353031178', 'K00NO5007U020220627045515550632178', 'K00NO5007U020220627045515728693178', 'K00NO5007U020220627045515919781178', 'K00NO5007U020220627045516118413178', 'K00NO5007U020220627045516521962178', 'K00NO5007U020220627045516704510178', 'K00NO5007U020220627045516886365178', 'K00NO5007U020220627045517097872178', 'K00NO5007U020220627045517275393178', 'K00NO5007U020220627045517457814178', 'K00NO5007U020220627045517740288178', 'K00NO5007U020220627045517960717178', 'K00NO5007U020220627045518185905178', 'K00NO5007U020220627045518365935178', 'K00NO5007U020220627045518570816178')
				invalid deadlink		
				Yes	Ash

		---

		(R D-2) 5111	2022-09-04	New CMS	200	all	www.fnac.com	
			casque audio reducteur de bruit - 24
			ecouteur intra auriculaire - 25
			enceinte bluetooth - 25

			Data Count Mismatch 1 (Fetched Data is Greater than expected)
			Data Count Mismatch 5 (Fetched Data is Lesser than expected)
			Data Count Mismatch 5 (Fetched Data is Lesser than expected)

			https://prnt.sc/tvW2lqgBdcA-
			https://prnt.sc/cQDY6KdN1cb6
			https://prnt.sc/lpkKfteTJFm-
			yes	phil

			{
			    
			    "response": {
			        "task_uuid": "38539e3f-b51f-42c3-bba2-51d7218c8412"
			    },
			    "version": "1.0"
			}
			
			# issue
				keyword: casque audio reducteur de bruit
				25 count
				https://prnt.sc/TEkkpWVNvdTJ

		(R D-1) 5112	2022-09-04	New CMS	200	all	www.saturn.de	
			in ear kopfhörer	
			Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
			https://prnt.sc/XwWAPjv7Go0q		

			{
			    "response": {
			        "task_uuid": "b6985936-fd90-43a4-8acb-3ac7603420b3"
			    },
			    "version": "1.0"
			}


# ----- """""" Arresting Wire -----

# REBUILD
	# target directory to rebuild
		'cd /spiders/<target directory>'
		'git fetch && git pull' or in GUI

		# no need to rebuild if latest commit is yesterday and no new commit today for specific spider
			# comparison
			# '. deploy-production.sh 202209031459-update-wsstrat-for-twentyfivebest-com-br'

			# '. deploy-production.sh 202209031506-update-price-tranformer-for-25best-com-br'

			# '. deploy-production.sh 202208191510-variant-for-microsoft-com-de-de'

			# rankings
			# '. deploy-production.sh 202209011623-update-dl-strat-for-mediamarkt-nl'

			# '. deploy-production.sh 202208141504-add-build-pipelines'

			# '. deploy-production.sh 202208141511-update-pagination-for-hellotv-nl'

			# listings
			# '. deploy-production.sh 202209031235-fun-be-update-title-and-url'

			# '. deploy-production.sh 202209031241-implement-et-for-goldentime-dk'

			# '. deploy-production.sh 202208141536-update-pagination-for-hellotv-nl'

				# . deploy-production.sh <yyyy-mm-hhmm>-<commit name>
					# commit name should be all lower case and replace spaces with dashes
				# this is the rebuild command
				# it should be always up to date

			# webhshots (webshot-nodejs)
			# . deploy-production.sh 202208211612-sanborns-com-mx-update-custom-actions

			# . deploy-production-override.sh 202208211646-sanborns-com-mx-update-custom-actions

	# check logs
		'df -h'
			# watch on /dev/sdal
			# watch on use %
				# it shouldnt reach 99%, mostly reaches 1 month to 99%
				# if it reaches greater than 94%, it needs to be deleted, 
					* '<remove image command>'
						# only intermediates do this command

	# config (go to cluster if r)
		'az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2'
			# tunong and gusto tanawun
			# maoy tanawun nga metric
		# OTHER COMMANDS
			# az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
			# az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
			az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
			az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction

			# az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
			az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction

			# az aks get-credentials --resource-group crawlers --name AKSListingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSListingsETProduction

			az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSListingsLProduction

			az aks get-credentials --resource-group crawlers --name AKSWebshotProduction2

	# check pods
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=webshot-nodejs'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=ws-ov-nodejs'

	# remove image
		# ask permission first to sir Rav or seniors
		# cd to spider (only to comparison, rankings and listings, no need et and special spiders (puppeteer))
			# Remove images - change grep "app" 
			'docker images | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "listing" | awk '{print $1 ":" $2}' | xargs docker rmi'