12 03 22
	SQL update:
		(SP D-1) Request 1:
			Goodmorning
			Request to Set Deadlink Listings Data in Nintendo Benelux

			DB: 110
			Table: view_all_listingsdata
			Date: 12/03/2022
			Retailer: www.fnac.be/fr
			category:
			software

			Listings UUID(s):
			d1104ee1-3f4a-42a1-ad43-7b4b4aea5f45


			Screenshot:


			Reason: Fetching data even if its deadlink(Top level category)

			# exec:
				# listings > list_set_to_dead_link_by_date+listings_uuid+postal.sql

		(SP D-1) Request 2:
			Hello!
			Request to Set Deadlink Rankings Data in Nintendo Mexico
			DB: X00
			Table: view_all_rankingsdata
			Date: 12/03/2022
			Retailer:
			www.liverpool.com.mx

			Keywords:
			Consola Nintendo Lite
			Consola Nintendo Switch Lite
			Consola Switch Lite

			Screenshot(s):
			https://prnt.sc/T2kFACXCD-YH
			https://prnt.sc/g22sruScemHV
			https://prnt.sc/p_g2-cfG9XHt

			Reason: Fetching data even if its PDP

			# exec:
				# rankings > rank_set_to_dead_link_by_date+retailer+keyword.sql 

	(F D-1) Trello 3:
		# https://trello.com/c/3YCxsr0L/4405-samsung-mobile-comparisons-wrong-dq-scraped-in-coolbluenl
		# [SAMSUNG MOBILE] Comparisons: wrong DQ scraped in coolblue.nl
		Date: 2022-10-27
		DB: F00
		Website: coolblue.nl
		Issue: wrong delivery quote scraped

		affected item_ids: **INSTOCK  with the following DQ**

		Morgen bij jou
		Morgen bij jou in huis
		Morgen bij jou bezorgd
		Morgen bezorgd

		# F00NLO10JA000000858610201910110308
		{
		    "response": {
		        "task_uuid": "0eea2432-86d6-43c9-8066-bf701023803f"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "3576d280-da63-4a21-9356-30bd3bd0a1e3"
		    },
		    "version": "1.0"
		}

		{
		    "response": {
		        "task_uuid": "65eeaa23-e0d5-414c-8805-ef30592e592d"
		    },
		    "version": "1.0"
		}

	Setup:
		(F D-1) IMG-190	2022-11-29	2022-11-29	Miele (P10)
			missing video count in www.ao.com	
			P10GBJ00JI220221128071423047278332	09-dec-22	
			PRODUCTS-4478	
			https://prnt.sc/S6NOLzfaBwH5	

			SELECT * FROM view_all_productdata where date='2022-12-03' AND item_id in ('P10GBH00JI220221128071423364221332') ORDER BY source

12 04 2022
	SQL update:
		(SP D-2) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-03
			Target Date: 2022-12-04
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec 
				# 3p_copy_over_by_date+item_id.sql
				# prod_copy_over_by_date+item_id.sql

		(SP D-2) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-04

			Field for update: is_new, and condition

			Reason:, -2 values

			# exec
				# 

	Setup:
		(F D-3) PROD-862	2022-11-30	2022-11-30	No DQ fetched in johnlewis.com	MIELE (P10)	
			P10GBX10JI220221128071423201793332	
			December 09, 2022	
			PRODUCT-4478	
			https://prnt.sc/EjIra3gj0_K7

		(F D-1) PROD-863	2022-12-01	2022-12-01	Incorrect price fetched in johnlewis.com	MIELE (P10)	
			P10GBX10JI220221128071423201793332	
			December 09, 2022	PRODUCT-4478	
			https://prnt.sc/ZHPlKr7F_cTF

	Daily:
		(F D-2) 13978	2022-12-04	OCR - 100	delivery	varuste.net	
			100FIZH0ZQ020220124093136008614024
			100FIZH0ZQ020220124093136613359024
			100FIZH0ZQ020220124093137231755024
			100FIZH0ZQ020220124093142460127024
			100FIZH0ZQ020220124093142908714024
			100FIZH0ZQ020220124093143404630024
			100FIZH0ZQ020220214062218561945045
			100FIZH0ZQ020220214062219255404045
			100FIZH010000001329500202102100700
			100FIZH010000000828040201908200306
			100FIZH010000000828330201908200318
			100FIZH010000000828410201908200323
			100FIZH010000001413710202105130512
			100FIZH0ZQ020220905024209007608248
			100FIZH010000000828170201908200312
			100FIZH010000000828200201908200313
			100FIZH010000001230660202010290804
			100FIZH010000001230690202010290804
			100FIZH010000001663480202111170221
			100FIZH010000001663500202111170221
			100FIZH010000001663590202111170221
			100FIZH010000001663610202111170221
			100FIZH010000001663690202111170221
			100FIZH010000001663700202111170221
			100FIZH0ZQ020220124093134261795024
			100FIZH0ZQ020220124093134797951024
			100FIZH0ZQ020220124093138553140024
			100FIZH0ZQ020220124093139109225024
			100FIZH0ZQ020220207085518918365038
			100FIZH0ZQ020220606021040713448157
			100FIZH010000001438750202106080416
			100FIZH010000001439440202106080557
			100FIZH010000001663830202111170221
			100FIZH010000001663850202111170221

			Incorrect DQ. No DQ statement in PDP. Set to -1.	

			https://prnt.sc/Q38RFrCvAeo2
			https://prnt.sc/0ckhsLJK2-N0
			https://prnt.sc/Y6EV1Ivho9R5
			Yes	Keeshia

			SELECT * FROM view_all_productdata where date='2022-12-04' AND item_id in ('100FIZH0ZQ020220124093136008614024', '100FIZH0ZQ020220124093136613359024', '100FIZH0ZQ020220124093137231755024', '100FIZH0ZQ020220124093142460127024', '100FIZH0ZQ020220124093142908714024', '100FIZH0ZQ020220124093143404630024', '100FIZH0ZQ020220214062218561945045', '100FIZH0ZQ020220214062219255404045', '100FIZH010000001329500202102100700', '100FIZH010000000828040201908200306', '100FIZH010000000828330201908200318', '100FIZH010000000828410201908200323', '100FIZH010000001413710202105130512', '100FIZH0ZQ020220905024209007608248', '100FIZH010000000828170201908200312', '100FIZH010000000828200201908200313', '100FIZH010000001230660202010290804', '100FIZH010000001230690202010290804', '100FIZH010000001663480202111170221', '100FIZH010000001663500202111170221', '100FIZH010000001663590202111170221', '100FIZH010000001663610202111170221', '100FIZH010000001663690202111170221', '100FIZH010000001663700202111170221', '100FIZH0ZQ020220124093134261795024', '100FIZH0ZQ020220124093134797951024', '100FIZH0ZQ020220124093138553140024', '100FIZH0ZQ020220124093139109225024', '100FIZH0ZQ020220207085518918365038', '100FIZH0ZQ020220606021040713448157', '100FIZH010000001438750202106080416', '100FIZH010000001439440202106080557', '100FIZH010000001663830202111170221', '100FIZH010000001663850202111170221') ORDER BY source

			{
			    "response": {
			        "task_uuid": "fddb9587-b547-44a5-b17c-13bc55b60977"
			    },
			    "version": "1.0"
			}

		(F D-2) 13979	2022-12-04	OCR - 100	delivery	varuste.net	
			100FIZH010000000827260201908200209
			100FIZH010000001329810202102100737
			100FIZH010000001401390202105040816
			100FIZH010000001663000202111170220
			100FIZH010000001664300202111170221
			100FIZH0ZQ020220905024218831732248
			100FIZH010000000826690201908191021
			100FIZH010000000827270201908200211
			100FIZH010000000883730201911140549
			100FIZH010000001230520202010290804
			100FIZH010000001230800202010290804
			100FIZH010000001230810202010290804
			100FIZH010000001230820202010290804
			100FIZH010000001329170202102100700
			100FIZH010000001329230202102100700
			100FIZH010000001329290202102100700
			100FIZH010000001329340202102100700
			100FIZH010000001329410202102100700
			100FIZH010000001329730202102100737
			100FIZH010000001329740202102100737
			100FIZH010000001329970202102100825
			100FIZH010000001335350202102190820
			100FIZH010000001401220202105040816
			100FIZH010000001438740202106080416
			100FIZH010000001662970202111170220
			100FIZH010000001663410202111170221
			100FIZH010000001663730202111170221
			100FIZH010000001664280202111170221
			100FIZH010000001664320202111170221
			100FIZH0ZQ020220613022520961597164
			100FIZH0ZQ020220613022521174660164
			100FIZH0ZQ020220905024243189512248

			Incorrect DQ scraped.	

			https://prnt.sc/kdkiR5pgpG9l
			https://prnt.sc/xXYpTsZqAyBP
		
			Yes	Keeshia

			SELECT * FROM view_all_productdata where date='2022-12-04' AND item_id in ('100FIZH010000000827260201908200209', '100FIZH010000001329810202102100737', '100FIZH010000001401390202105040816', '100FIZH010000001663000202111170220', '100FIZH010000001664300202111170221', '100FIZH0ZQ020220905024218831732248', '100FIZH010000000826690201908191021', '100FIZH010000000827270201908200211', '100FIZH010000000883730201911140549', '100FIZH010000001230520202010290804', '100FIZH010000001230800202010290804', '100FIZH010000001230810202010290804', '100FIZH010000001230820202010290804', '100FIZH010000001329170202102100700', '100FIZH010000001329230202102100700', '100FIZH010000001329290202102100700', '100FIZH010000001329340202102100700', '100FIZH010000001329410202102100700', '100FIZH010000001329730202102100737', '100FIZH010000001329740202102100737', '100FIZH010000001329970202102100825', '100FIZH010000001335350202102190820', '100FIZH010000001401220202105040816', '100FIZH010000001438740202106080416', '100FIZH010000001662970202111170220', '100FIZH010000001663410202111170221', '100FIZH010000001663730202111170221', '100FIZH010000001664280202111170221', '100FIZH010000001664320202111170221', '100FIZH0ZQ020220613022520961597164', '100FIZH0ZQ020220613022521174660164', '100FIZH0ZQ020220905024243189512248') ORDER BY source

			{
			    "response": {
			        "task_uuid": "208fea8e-d752-4d15-8bdf-4164caaf1899"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "153d9232-3223-46d4-926f-a39c1e1f8901"
			    },
			    "version": "1.0"
			}

		---
		to_fix() 1731	2022-12-04	New CMS	OCR-R10	all	www.lowes.com	
			Exterior Caulk	
			Duplicate data	
			https://prnt.sc/VZfg1-wtz1yZ	
			Yes	Michael

			SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
				where var2.website = 'www.lowes.com' and 
				var2.category in (N'Exterior Caulk') and 
				var2.[date] = '2022-12-04' 
				GROUP BY date, website, category, product_website, category_url, litem_id, country, source
	
				
			SELECT * FROM view_all_listings_data WHERE date = '2022-12-04' AND website = 'www.lowes.com' AND category in ('Exterior Caulk')

			{
			    "response": {
			        "task_uuid": "8c0c28df-da56-49db-8339-93a5b0bbdf89"
			    },
			    "version": "1.0"
			}

12 07 2022
	(R D-1) Trello 1:
		# https://trello.com/c/FOzoj3Pr/4553-razer-rankings-duplicate-data-in-wwwnotebooksbilligerde
		# [RAZER] : RANKINGS - Duplicate Data in www.notebooksbilliger.de
		Issue Details: Inconsistent site result, requires observation

		Date: 11 /30/2022

		Retailer:www.notebooksbilliger.de

		Keyword(s): 
		MECHANISCHE TASTATUR

		Screenshot(s): 
		https://prnt.sc/xWxWTz2EZTwP

	Trello 2:
		# https://trello.com/c/N0nno5UN/3899-ms-price-comparisons-incorrect-stocks-status-in-microsoftcom-nl-nl
		# [MS PRICE] Comparisons: Incorrect stocks status in microsoft.com/nl-nl
		Date: June 10, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/nl-nl
		Issue: Incorrect stocks status, should be INSTOCK.

		Sample Item IDs:
		A00NLP40KT020220502203444238633122
		A00NLP40KT020220502203444016727122
		A00NLP40KT020220502203441779522122
		A00NLP40KT020220502203444930702122
		A00NLP40RB000000995650202008040335

	---
	SQL update
		(SP D-2) Request 1:	
			Request for copy over in ZOUN

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-06
			Target Date: 2022-12-07
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec
				# 3P (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql 

		(SP D-1) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-07
			Field for update: in_stock

			Reason: -2 values

			# exec
				# 3p (SDA) > update_etl_3pdata_field.sql

	--- 
	Daily Issues:	
		(F D-2) 13991	2022-12-07	0CR- K00	
			description	pcgarage.ro	
			All item ids	
			Incorrect descrption	
			https://prnt.sc/o5EMmZG_1MiE	
			Yes	Jimmy

			SELECT * FROM view_all_productdata where date='2022-12-07' AND website = 'www.pcgarage.ro' AND product_url != ''

			{
			    "response": {
			        "task_uuid": "ecd96467-49fc-4013-839d-62c30ffac49e"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "9e334f7e-95f2-41fe-b7e3-9b1ab953b624"
			    },
			    "version": "1.0"
			}

12 08 2022
	Daily Issues:
		(ref sir micheal) (R D-1) 13995	2022-12-08	OCR-K00	rating_reviewers	
			overclockers.co.uk	
			All items	
			-3 ratings and reviews		
			Yes	
			Ash	12/08/2022

			{
			    "response": {
			        "task_uuid": "a0a83cf8-e1c5-4be8-87e7-5838360468b3"
			    },
			    "version": "1.0"
			}

			SELECT * FROM view_all_productdata where date='2022-12-08' AND website = 'www.overclockers.co.uk' AND source != 'dead_link'

		(F D-1) (R D-1) 13996	2022-12-08	O00	description	vandenborre.be/nl	
			O00BEMT040000001473940202107060502	
			No description found ;set value to -1	
			https://prnt.sc/7flKmkbHwYch
			Yes	Jahar

			SELECT * FROM view_all_productdata where date='2022-12-08' AND website = 'www.vandenborre.be/nl' AND item_id not in ('O00BEMT040000001473940202107060502') ORDER BY source

			{
			    "response": {
			        "task_uuid": "0e51334f-ec87-4206-a83f-49c7cddd9545"
			    },
			    "version": "1.0"
			}
	
		(F D-2) (R D-1) 5973	2022-12-08	New CMS	OCR-O00	all	www.hellotv.nl	
			8k televisie
			Droger
			Wrong prod_url	
			https://prnt.sc/Mc_XHrvf4k8a
			https://prnt.sc/2h4ze17KjVys
			Yes	renz

			SELECT * FROM view_all_rankingsdata var where date='2022-12-08' AND website = 'www.hellotv.nl' AND keyword in (N'8k televisie')

			# 8k televisie
			{
			    "response": {
			        "task_uuid": "4a1a1a2e-d2b0-4ab0-b57f-0368aeab3af5"
			    },
			    "version": "1.0"
			}

			# Droger
			{
			    "response": {
			        "task_uuid": "161253f9-48c7-4a7e-9e1a-6e333521fab6"
			    },
			    "version": "1.0"
			}

		fr_trello (F D-2) (R D-1)  1737	2022-12-08	New CMS	OCR-O00	all	www.hellotv.nl
			TV UHD 2022
			TV QLED 2022
			TV OLED 2022
			Soundbar 2022
			Beamer

			Invalid Deadlink	
			https://prnt.sc/iDndT3cP_6TH
			Yes	keen

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-12-08' 
				AND website = 'www.hellotv.nl' 
				AND category in (N'TV UHD 2022',
					'TV QLED 2022',
					'TV OLED 2022',
					'Soundbar 2022',
					'Beamer')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source

			# SELECT * FROM data_points dp WHERE data_point_id = '2022-12-08@22:00:00@O00@931@898d0b7c-57cd-45a5-8d12-3ca8e931609b'

			{
			    "response": {
			        "task_uuid": "a5ccb33c-d974-453e-b439-13d17f7a7e35"
			    },
			    "version": "1.0"
			}

		---
	
	Webshots:
		(W) Request 1:
			NA region

12 09 2022
	SQL update:
		(SP D-1) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-08
			Target Date: 2022-12-09
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-2) Request 2:
			Request to Set Deadlink Rankings Data in Eva Solo

			DB: 610

			Table: view_all_rankingsdata

			Date: 12/09/2022

			Retailer: www.bagarenochkocken.se

			Keywords:
			termoskanna bäst i test

			Screenshots:
			https://prnt.sc/PjfiU8kA92li

			# exec:	
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-2) Request 3:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-08
			Target Date: 2022-12-09
			ItemID:

			200DE52070000001568580202108300838

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-2) Request 4:
			Request to remove 3P data in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-09
			item_id:

			200ITGF070000000557970201904221001
			200ITGF070000000557970201904221001
			200ITGF070000000557970201904221001
			200ES8D080000000551750201904050650

			# exec:
				# 3p (SDA) > 3p_set_to_deadlink+item_id.sql

		(SP D-2) Request 5:
			Request for update in ZOUND SDA


			DB: 200
			Table: view_3P_data
			Date: 2022-12-09
			Field for update: in_stock

			Reason: -2 values

			# exec:
				# 3p (SDA) > 3p_update_condition_and_is_new.sql
				# 3p (SDA) > 3p_update_is_new.sql

	---
	(F D-2) (R D-2) Trello 2:
		Date: June 10, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/nl-nl
		Issue: Incorrect stocks status, should be INSTOCK.

		Sample Item IDs:
		A00NLP40KT020220502203444238633122
		A00NLP40KT020220502203444016727122
		A00NLP40KT020220502203441779522122
		A00NLP40KT020220502203444930702122
		A00NLP40RB000000995650202008040335

		SELECT * FROM view_all_productdata where date='2022-12-09' AND item_id in ('A00NLP40KT020220502203444238633122', 'A00NLP40KT020220502203444016727122', 'A00NLP40KT020220502203441779522122', 'A00NLP40KT020220502203444930702122', 'A00NLP40RB000000995650202008040335') ORDER BY source

	---
	MSPrice:
		(R D-5) Request 1:
			# rerun: A00NLP40KT020221118093942226273322
			{
			    "response": {
			        "task_uuid": "af7ccb75-9556-4383-9892-b16e4e7f44b7"
			    },
			    "version": "1.0"
			}


			# rerun A00NLP40KT020221118093942226273322

			{
			    "response": {
			        "task_uuid": "c998e27f-28a8-43e9-aa1e-1301c59891b9"
			    },
			    "version": "1.0"
			}

			# normal run for A00NLP40KT020221118093942226273322

			{
			    "response": {
			        "task_uuid": "d345945d-d1bf-463b-82bb-c5ab84e332fc"
			    },
			    "version": "1.0"
			}

			# rerun for A00NLP40KT020221118093942226273322

			{
			    "response": {
			        "task_uuid": "9e9c2e8f-72fc-4077-8c13-796b483d3b91"
			    },
			    "version": "1.0"
			}

			# A00NLP40KT020221118093942226273322

			{
			    "response": {
			        "task_uuid": "3b1e7058-d87b-4e46-81db-5b365a1bb7c3"
			    },
			    "version": "1.0"
			}

			# all item ids except: A00NLP40KT020221118093942226273322
			{
			    "response": {
			        "task_uuid": "2c0b492c-0886-48a9-8ac3-4df4268fb18a"
			    },
			    "version": "1.0"
			}

	---
	Daily Issues:	
		(F D-1) 13998	2022-12-09	OCR-610	description	magasin.dk	
			610DKHX00P000001603750202109280746
			610DKHX00P000001605810202109280746
			610DKHX01P000001604180202109280746
			610DKHX01P000001604580202109280746
			HTML/CSS tags issue	https://prnt.sc/QJOZooMoUOJn	
			Yes	Jahar

			# SELECT * FROM view_all_productdata where date='2022-12-09' AND item_id in ('610DKHX00P000001603750202109280746', '610DKHX00P000001605810202109280746', '610DKHX01P000001604180202109280746', '610DKHX01P000001604580202109280746') ORDER BY source	

			{
			    "response": {
			        "task_uuid": "6a82d6ea-264d-44ef-8163-3ecb7e24f98c"
			    },
			    "version": "1.0"
			}

		(R D-3) 14000	2022-12-09	OCR-200	all	amazon.de/3P	
			200DE52070000001568580202108300838	
			Invalid deadlink	
			https://prnt.sc/V-u6liYgH_yM	
			Yes	Mojo

			# SELECT * FROM view_all_productdata where date='2022-12-09' AND item_id in ('200DE52070000001568580202108300838') ORDER BY source

			{
			    "response": {
			        "task_uuid": "09b96eb9-fb51-4658-a46b-2bb7963d479b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "82b234c0-58d6-4bdd-af3f-b9da0cf89523"
			    },
			    "version": "1.0"
			}

		--- 

		(R D-1) 5975	2022-12-09	New CMS	OCR-200	all	www.target.com
			PORTABLE BLUETOOTH SPEAKER

			Duplicate data(rank 24 and 25)	
			https://prnt.sc/gcDrUTkCsVot	
			Yes	Rayyan

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.target.com' and 
				var2.keyword in (N'PORTABLE BLUETOOTH SPEAKER') and 
				var2.[date] = '2022-12-09' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source 

			{
			    "response": {
			        "task_uuid": "885edfea-f747-4cb7-904f-d3b4c4072584"
			    },
			    "version": "1.0"
			}

12 10 2022
	Trello 4:
		Date: December 09, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/nl-nl
		Issue: Incorrect Data.

		Incorrect price and stocks status - (should be -1 then OOS):
		https://prnt.sc/Cyp0wvg4wAmC
		A00NLP40RB000001237010202011060514
		A00NLP40RB000001237020202011060514
		A00NLP40RB000001237030202011060514
		A00NLP40RB000000996180202008040335

		Should be tag s -3/deadlink: 
		https://prnt.sc/wmRUUckn8iTb
		Sample Item IDs:
		A00NLP40RB000000996030202008040335
		A00NLP40RB000000996040202008040335
		A00NLP40RB000000996050202008040335
		A00NLP40RB000000996080202008040335
		A00NLP40RB000001237050202011060514
		A00NLP40RB000001237060202011060514
		A00NLP40RB000000995810202008040335
		A00NLP40RB000001203770202009010237
		A00NLP40RB000000996010202008040335
		A00NLP40RB000000996020202008040335
		A00NLP40RB000000995820202008040335
		A00NLP40RB000001205420202009082020
		A00NLP40KT020220502203438850326122
		A00NLP40KT020220502203438530458122
		A00NLP40RB000001237040202011060514

	---
	Webshots:	
		(W) Request 1:
			EMEA region

		Request 2:
			NA region

	---
	SQL updates:	
		(SP D-1) Request 1:
			Request to Set Deadlink Rankings Data in Nintendo Brazil

			DB: U00

			Table: view_all_rankingsdata

			Date:
			12/10/2022

			Retailer:
			www.bemol.com.br

			Keywords:
			Console Nintendo Switch
			Console Switch
			Joy Con

			Screenshots:
			https://prnt.sc/gJjxCn3vYJqV
			https://prnt.sc/HZRctxQMi8Jo
			https://prnt.sc/3zq7kTaJir9N

			Reason: Fetching data even if its deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql 

		(SP D-1) Request 2:
			Hello!

			Request to Set Deadlink Rankings Data in Nintendo Mexico

			DB: X00

			Table: view_all_rankingsdata

			Date: 12/10/2022

			Retailer:
			www.liverpool.com.mx

			Keywords:
			Consola Nintendo Lite
			Consola Nintendo Switch Lite
			Consola Switch Lite

			Screenshot(s):
			https://prnt.sc/hnhda_6BVAB1
			https://prnt.sc/j4ydCCUgBpOe
			https://prnt.sc/3T_z8md2HrK8

			Reason: Fetching data even if its PDP

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

12 11 22
	SQL update:	
		(R D-2) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-09
			Target Date: 2022-12-10
			ItemID:

			200DE52070000001568580202108300838
			200DE52070000001568580202108300838
			200DE52070000001568580202108300838
			200ES8D080000000551750201904050650

			# exec:
				# product > copy_over > prod_copy_over_by_date+item_id.sql
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql

		(R D-5) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-10

			Field for update: is_new, retailer_name and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_condition.sql
				# 3p (SDA) > 3p_update_is_new.sql

		(R D-1) Request 3:
			Request to remove 3P data in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-11
			item_id:

			200ES8D080000000551750201904050650

			# exec:
				# 3p (SDA) > 3p_set_to_deadlink+item_id.sql

	---
	Webshots
		(W) Request 1:
			NA region

12 14 22
	Daily Issues:
		5984	2022-12-14	New CMS	OCR- K00	all	notebooksbilliger.de	
			headset
			Mechanische Tastatur
		
			Duplicate Data	
			https://prnt.sc/oygt2xlhyvXH
			https://prnt.sc/rV4UT2XQNC5e
		
			Yes	keen

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.notebooksbilliger.de' and 
				var2.keyword in (N'headset') and 
				var2.[date] = '2022-12-14' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "a97687de-1b6f-4d90-889e-20bd80d02236"
			    },
			    "version": "1.0"
			}

			# after rebuild

			{
			    "response": {
			        "task_uuid": "9e0aa49b-bfaa-4ec1-ad06-e716c3138a08"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "15ca6b3e-bc11-4e08-b14a-ffa8f902f182"
			    },
			    "version": "1.0"
			}

	Webshots:
		NA region

12 17 22
	SQL update:
		(SP D-1) Request 1:
			Request to remove 3P data in ZOUND SDA


			DB: 200
			Table: view_3P_data
			Date: 2022-12-16
			item_id:

			200FR7D080000000551730201904050650

			# exec 
				# 3p (SDA) > 3p_set_to_deadlink+item_id.sql

		(SP D-2) Request 2:
			Request for update in ZOUND SDA


			DB: 200
			Table: view_3P_data
			Date: 2022-12-16

			Field for update: retailer_name

			Reason:, -2 values, incorrect values

			200DE4D070000000550770201904050611

			Tek4life

		(SP D-2) Request 3:
			Request for update in ZOUND SDA


			DB: 200
			Table: view_3P_data
			Date: 2022-12-16

			Field for update: is_new and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_is_new.sql
				# 3p (SDA) > 3p_update_condition_and_is_new.sql

		(SP D-1) Request 4:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-16
			Target Date: 2022-12-17
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec:
				# product > copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-1) Request 5:
			Request to Set Deadlink Rankings Data in Nintendo Brazil

			DB:U00

			Table: view_all_rankingsdata

			Date:12/17/2022

			Retailer:
			www.bemol.com.br

			Keywords:
			Controle Nintendo Switch
			Controle sem fio Nintendo Switch
			Joy Con

			Screenshot:
			https://prnt.sc/6xF6ufLLvUQq
			https://prnt.sc/fdTGmjk7s6s_
			https://prnt.sc/x86-65zP_3qB

			Reason: Fetching Data even if its Deadlink (PDP)

			# exec:
				# 

		(SP D-1) Request 6:
			Hello!

			Request to Set Deadlink Rankings Data in Nintendo Mexico

			DB: X00

			Table: view_all_rankingsdata

			Date: 12/17/2022

			Retailer:
			www.liverpool.com.mx

			Keywords:
			Consola Nintendo Lite
			Consola Nintendo Switch Lite
			Consola Switch Lite

			Screenshot(s):
			https://prnt.sc/ZXPWUPzcVLMM
			https://prnt.sc/BJ43Q-8PyPgF
			https://prnt.sc/Cb_Q9xMsTr71

			Reason: Fetching data even if its PDP

		(SP D-1) Request 7:
			Request to remove 3P data in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-17
			item_id:

			200FR7D080000000551730201904050650

			# exec:
				# 3p (SDA) > 3p_set_to_deadlink+item_id.sql

	Webshots:
		(w) Request 1:
			NA region

	Daily Issues:
		(R D-1) 5992	2022-12-17	New CMS	OCR-U00	product_website	www.bemol.com.br	
			Console
			Console Nintendo Switch
			Console Switch
			Controle Joy con
			Controle Switch
			Nintendo Switch
			Pro Controller
			Mini Video Game
			Aparelho de Game
			Aparelho de Video Game
			Mini game portatil

			wrong product website fetched

			https://prnt.sc/j2HL6uOEs_lp
			https://prnt.sc/FANpyrIkn7Mz
			https://prnt.sc/mgiIRfHO1Pej
			https://prnt.sc/SWv1Zuv278TY
			https://prnt.sc/gy2cWC1gnX1g
			https://prnt.sc/1hdB-a3gibEm
			https://prnt.sc/kNP7HXwVysT2
			https://prnt.sc/lZe71o5RX6mn
			https://prnt.sc/VzZ66QZu0Kvf
			https://prnt.sc/Mjlg-Hj--2fr
			https://prnt.sc/csbIBoqEcOYR
			Yes	kurt

			# valid issue

12 18 22
	(F D-2) Trello:	
		# [MS PRICE] Comparisons: Incorrect data in microsoft.com/nl-nl
		# https://trello.com/c/SvjCkoUg/4568-ms-price-comparisons-incorrect-data-in-microsoftcom-nl-nl
		Date: December 09, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/nl-nl
		Issue: Incorrect Data.

		Incorrect price and stocks status - (should be -1 then OOS):
		https://prnt.sc/Cyp0wvg4wAmC
		A00NLP40RB000001237010202011060514
		A00NLP40RB000001237020202011060514
		A00NLP40RB000001237030202011060514
		A00NLP40RB000000996180202008040335

		Should be tag -3/deadlink: 
		https://prnt.sc/wmRUUckn8iTb
		Sample Item IDs:
		A00NLP40RB000000996030202008040335
		A00NLP40RB000000996040202008040335
		A00NLP40RB000000996050202008040335
		A00NLP40RB000000996080202008040335
		A00NLP40RB000001237050202011060514
		A00NLP40RB000001237060202011060514
		A00NLP40RB000000995810202008040335
		A00NLP40RB000001203770202009010237
		A00NLP40RB000000996010202008040335
		A00NLP40RB000000996020202008040335
		A00NLP40RB000000995820202008040335
		A00NLP40RB000001205420202009082020
		A00NLP40KT020220502203438850326122
		A00NLP40KT020220502203438530458122
		A00NLP40RB000001237040202011060514

		-- microsoft.com/nl-nl Incorrect Data.
		SELECT * FROM view_all_productdata where date='2022-12-17' AND item_id in ('A00NLP40RB000001237010202011060514', 'A00NLP40RB000001237020202011060514', 'A00NLP40RB000001237030202011060514', 'A00NLP40RB000000996180202008040335') ORDER BY source

	---
	SQL update:
		(SP D-1) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-17
			Target Date: 2022-12-18
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql



	---
	Webshots

	---

	Daily Issues:
		1742	2022-12-18	New CMS	OCR-100	all	www.olssonsfiske.se	
			Ekolod
			Ekolod Plotter Kombi
			Elmotorer

			Invalid Deadlink	
			https://prnt.sc/_NzeE1e8YZWm
			https://prnt.sc/KaiFfir1pzWB
			https://prnt.sc/RHy0kFhfHkI1
			Yes	Michael

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-12-18' 
				AND website = 'www.olssonsfiske.se' 
				AND category in (N'Elmotorer')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source

		1743	2022-12-18	New CMS	OCR-100	all	www.widforss.se	
			ekolod/fishfinder
			gps, hundpejl & klockor

			Invalid Deadlink	
			https://prnt.sc/T09epdXQBukc
			https://prnt.sc/QXDWyLqnTorC

			Yes	Rod

12 21 22
	(F D-3) Trello:
		# https://trello.com/c/rZwf2CoJ/4588-ocr-garmin-listings-invalid-deadlink-in-wwwolssonsfiskese
		# [OCR-Garmin]: LISTINGS Invalid Deadlink in www.olssonsfiske.se
		Issue Description: Invalid Deadlink in www.olssonsfiske.se

		Cause:

		Retailer: www.olssonsfiske.se

		Categories:
		Ekolod
		Ekolod Plotter Kombi
		Elmotorer

		Screenshot:
		Screenshot
		Screenshot
		Screenshot

		Date:
		December 18, 2022

		SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2022-12-19' 
				AND website = 'www.olssonsfiske.se' 
				AND category in (N'Ekolod')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source

	---
	SQL update:
		(SP D-1	) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-20
			Target Date: 2022-12-21
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

		 	# exec:
		 		# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
		 		# product > copy_over > prod_copy_over_by_date+item_id.sql

	---
	Webshots
		Request 1:
			NA region

12 22 22
	Webshots
		(W) Request 1:
			EMEA region

	--- 
	SQL update:
		(SP D-2) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2022-12-21
			Target Date: 2022-12-22
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS070000001398230202105040415
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001220880202010261317
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-1) Request 2:
			Request to Set Deadlink Listings Data in Samsung WG

			DB: O00

			Table: view_all_listingsdata

			Date: 12/22/2022

			Retailer: www.krefel.be/nl

			Category(ies):
			TV 2020

			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63

			Screenshot(s):
			https://prnt.sc/5GCK7t3ngLMJ

			Reason: Fetching Data even if its Deadlink (Top Level Category)

			# exec:
				# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

		(SP D-1) Request 3:
			Request to Set Deadlink Rankings Data in Samsung CE/WG

			DB: O00

			Table: view_all_rankingsdata

			Date: 12/22/2022

			Retailer:
			http://www.krefel.be/nl

			Keywords:

			Energiezuinige koelkast
			 
			Screenshot(s):

			https://prnt.sc/QQ8J5m1i5Wzs

			Reason: Fetching data even if its deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 5:
			DB: O00
			Table: view_all_rankingsdata

			Date:12/22/2022

			Retailer: 

			www.vandenborre.be/nl

			Keywords:

				Dolby Atmos soundbars

				/ Wifi soundbar

				/ grote tv

				/ mega scherm

				Stofzuiger zonder zak

			Screenshot(s):
			https://prnt.sc/U_MwwzPoytLb
			https://prnt.sc/w0IOOfGDGl1P
			https://prnt.sc/C1iMYqRFZlYs
			https://prnt.sc/GsNMTwN6Isa0
			https://prnt.sc/y9WYpqSBI8Or

			Reason: Fetching data even if its Deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

	-- 

12 23 22
	Techval Issues:
		(R D-2) 9118	2022-12-23	No	K00	in_stock	euro.com.pl	
			K00PLD803F000001308480202101210525
			K00PLD803F000001447940202106150451
			K00PLD807U020211115034606663236319
			K00PLD803F000000987770202007060804
			K00PLD804K000001447750202106150451
			K00PLD80CG000000980830202006250948
			K00PLD803F000001308250202101210525
			K00PLD807U020211115034607022964319
			K00PLD807U020220214041206211211045
			K00PLD804K000001528330202108020955
			K00PLD807U020211115034607722283319
			K00PLD803F000000987800202007060804
			K00PLD804K000001447800202106150451
			K00PLD80CG000001298190202101180406
			K00PLD803F000001308530202101210525
			K00PLD807U020220620051755048477171
			K00PLD807U020220627045524386675178
			K00PLD805K000001448090202106150451
			K00PLD803F000001308600202101210525
			K00PLD804K000001528350202108020955
			K00PLD804K000001450160202106150633
			K00PLD807U020220627045521391858178
			K00PLD80CG000001209500202009280448
			K00PLD808O000001450080202106150618
			K00PLD805K000001528550202108020955
			K00PLD807U020220627045522482808178
			K00PLD80CG000001373110202104050819
			K00PLD80CG000000980880202006250948
			K00PLD807U020220214041205764883045
			K00PLD807U020220627045523827476178
			K00PLD807U020220214041205918486045
			K00PLD80CG000001298220202101180406
			K00PLD80CG000000980850202006250948
			K00PLD80CG000000980840202006250948
			K00PLD80CG000001525630202108020807
			K00PLD80CG000001298230202101180406
			K00PLD80CG000001244580202011170356
			K00PLD80CG000001592080202109200556
			K00PLD80KO000001448060202106150451
			-1 in_stock

			-- 12 23 22 -1 in_stock
			SELECT * FROM view_all_productdata where date='2022-12-23' AND item_id in ('K00PLD803F000001308480202101210525', 'K00PLD803F000001447940202106150451', 'K00PLD807U020211115034606663236319', 'K00PLD803F000000987770202007060804', 'K00PLD804K000001447750202106150451', 'K00PLD80CG000000980830202006250948', 'K00PLD803F000001308250202101210525', 'K00PLD807U020211115034607022964319', 'K00PLD807U020220214041206211211045', 'K00PLD804K000001528330202108020955', 'K00PLD807U020211115034607722283319', 'K00PLD803F000000987800202007060804', 'K00PLD804K000001447800202106150451', 'K00PLD80CG000001298190202101180406', 'K00PLD803F000001308530202101210525', 'K00PLD807U020220620051755048477171', 'K00PLD807U020220627045524386675178', 'K00PLD805K000001448090202106150451', 'K00PLD803F000001308600202101210525', 'K00PLD804K000001528350202108020955', 'K00PLD804K000001450160202106150633', 'K00PLD807U020220627045521391858178', 'K00PLD80CG000001209500202009280448', 'K00PLD808O000001450080202106150618', 'K00PLD805K000001528550202108020955', 'K00PLD807U020220627045522482808178', 'K00PLD80CG000001373110202104050819', 'K00PLD80CG000000980880202006250948', 'K00PLD807U020220214041205764883045', 'K00PLD807U020220627045523827476178', 'K00PLD807U020220214041205918486045', 'K00PLD80CG000001298220202101180406', 'K00PLD80CG000000980850202006250948', 'K00PLD80CG000000980840202006250948', 'K00PLD80CG000001525630202108020807', 'K00PLD80CG000001298230202101180406', 'K00PLD80CG000001244580202011170356', 'K00PLD80CG000001592080202109200556', 'K00PLD80KO000001448060202106150451') ORDER BY source


			{
			    "response": {
			        "task_uuid": "7e1c9e84-8aff-4b3e-8a65-a594f333418d"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "604db867-7d8d-4398-ad00-dad99e798d9d"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "d4deb27e-6180-4a78-bef7-3b41037146ab"
			    },
			    "version": "1.0"
			}

		(R D-1) 9119	2022-12-23	No	K00	in_stock,price_value	euro.com.pl	
			K00PLD806O000001447400202106150451	
			-3 in_stock,-3 price_value

			-- euro.com.pl	 -3 in_stock,-3 price_value
			SELECT * FROM view_all_productdata where date='2022-12-23' AND item_id in ('K00PLD806O000001447400202106150451') ORDER BY source

			{
			    "response": {
			        "task_uuid": "67b1ab0c-495d-4a6a-8dea-f17b391e217b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "42990fc8-53be-4c92-8ec7-c190225373c9"
			    },
			    "version": "1.0"
			}

		(R D-1) 9120	2022-12-23	No	A00	in_stock	jbhifi.com.au	
			A00AU2V0KT020220807132949087959219	
			-1 in_stock

			-- jbhifi.com.au	 -1 in_stock
			SELECT * FROM view_all_productdata where date='2022-12-23' AND item_id in ('A00AU2V0KT020220807132949087959219') ORDER BY source
 
	-- 
	(SP D-20) CCEP

	--
	SQL update:
		(SP D-1) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-23
			Field for update: in_stock
			Reason:, -2 values

			# exec
				# 3p (SDA) > update_etl_3pdata_field.sql

		(SP D-1) Request 2:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2022-12-23

			Field for update: is_new and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_is_new.sql
				# 3p (SDA) > 3p_update_condition_and_is_new.sql

12 28 22
	CCEP runs and validation


12 29 22
	SQL update:	
		(SP D-1) Request 1:
			Request to Set Deadlink Listings Data in Samsung WG

			DB: O00

			Table: view_all_listingsdata

			Date: 12/29/2022

			Retailer: www.krefel.be/nl

			Category(ies):
			TV 2020

			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63

			Screenshot(s):
			https://prnt.sc/4GdU2H6S3jwE

			Reason: Fetching Data even if its Deadlink (Top Level Category)

			# exec:
				# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql


# ----- """""" Arresting Wire -----

# REBUILD
	# target directory to rebuild
		'cd /spiders/<target directory>'
		'git fetch && git pull' or in GUI

		# no need to rebuild if latest commit is yesterday and no new commit today for specific spider
			# comparison
				# '. deploy-production.sh 202212091526-update-video-urls-for-abt-com'

				# '. deploy-production.sh 202212091535-update-description-for-magasin-dk'

				# '. deploy-production.sh 202211201055-add-hard-timeout-for-elgiganten-se'

			# rankings
				# '. deploy-production.sh 202212091513-dl-strat-for-nordicnest-se'

				# '. deploy-production.sh 202208141504-add-build-pipelines'

				# '. deploy-production.sh 202210131544-add-unblocker-for-pcgarage-ro'

			# listings
				# '. deploy-production.sh 202212081448-update-hash-for-elgiganten-dk'

				# '. deploy-production.sh 202210231330-update-ratings-and-desription-transformer-for-mediamarkt-de'

				# '. deploy-production.sh 202212081455-update-product-query-selector-for-hellotv-nl'

			# . deploy-production.sh <yyyy-mm-hhmm>-<commit name>
			# commit name should be all lower case and replace spaces with dashes
			# this is the rebuild command
			# it should be always up to date

			# webhshots (webshot-nodejs)
				# . deploy-production.sh 202208211612-sanborns-com-mx-update-custom-actions

				# . deploy-production-override.sh 202208211646-sanborns-com-mx-update-custom-actions

			# ps
				# . deploy-production2.sh 202210131500-update-checker-for-gigantti-fi

	# check logs
		'df -h'
			# watch on /dev/sdal
			# watch on use %
				# it shouldnt reach 99%, mostly reaches 1 month to 99%
				# if it reaches greater than 94%, it needs to be deleted, 
					* '<remove image command>'
						# only intermediates do this command

	# config (go to cluster if r)
			'az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2'
			# tunong and gusto tanawun
			# maoy tanawun nga metric
		# OTHER COMMANDS
			# az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
			# az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
			az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
			az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction

			# az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
			az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction

			# az aks get-credentials --resource-group crawlers --name AKSListingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSListingsETProduction

			az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSListingsLProduction

			az aks get-credentials --resource-group crawlers --name AKSWebshotProduction2

			az aks get-credentials --resource-group crawlers --name AKSPhysicalStoreProduction2

	# check pods
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=webshot-nodejs'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=ws-ov-nodejs'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=physstores'

	# remove image
		# ask permission first to sir Rav or seniors
		# cd to spider (only to comparison, rankings and listings, no need et and special spiders (puppeteer))
			# Remove images - change grep "app" 
			'docker images | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "listing" | awk '{print $1 ":" $2}' | xargs docker rmi'	