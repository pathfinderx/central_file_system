# pag daily ka kani ang dapat nimo tandaan.
	# w0w = acknowledge
	# Like = Done
	# sad = trello/blocked
	# heart = quick fix ( less than 30 minutes ok na sya)
	# angry = for rebuild/Fixing

# check for issue codes:
	# -1 Not Found
	# 1 instock
	# 0 out stock
	# -2 ET error
	# -3 Spider Error
	# auto-copy-over

# what to write in cause of issue? 
	# multiple reruns till get it
		# blocked
	# rerun once
		# proxy issue

# check other keyword search for country
	#SELECT date,source,website,keyword,ritem_id,product_url,country,count(1) from view_all_rankingsdata where website ='www.aptekagemini.pl' and date = '2021-11-12' GROUP BY date,source,website,keyword,ritem_id,product_url,country

# check other keyword search for country
	# SELECT * FROM view_all_rankingsdata WHERE date = '2021-11-13' AND website = 'www.netonnet.no' AND source != 'etl'

# for duplicates
	# SELECT date,website, keyword, product_website,product_url, ritem_id , source, COUNT(1) from view_all_rankingsdata var2 where var2.website = 'www.target.com' and var2.keyword in (N'') and var2.[date] = '2021-11-22' GROUP BY date,website, keyword, product_website,product_url, ritem_id , source

# runner bulk commands: 
	# cmd: cd C:\Users\kurt kevin nebril\Desktop\runner_tool
	# cmd: python app.py
	# paste the item_ids here or r_item_ids
		# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\input_item_ids.txt
	# cmd: 
		# how to generate webshots?
		# rerun_webshots -c A00 -s etl -d 2021-01-26

		# how to generate comparisons ?
		# rerun_comparison -c CRAWL_FINISHED -d 2021-01-26
	# rerun code generated here
		# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\rerun_cmd_generated.txt

	# identify the company code if prod1 or prod2
	# putty prod(n)
		# paste the generated file at putty
	# check rabbit mq or queues
	# rerun the sql in dbeaver if theres any changes

# Common Issues
	# auto copy over
		# always check local
		# check browser if empty value is valid
			# if yes, lightshot
		# rerun

		# crawl_failed
			# rerun

	# if needs further fixes
		# for trello

	# deadlink
		# check visible

	# 

# Fixes
	11 10 21
	# 1059 : www.webtogs.com : Q00GBGX0IJ000001565680202108230711 : -1 price
		# check dbeaver for price if -1
		# check couch
			# director-comparison
				# check for attribute: "runtime_statistics"
				# check for attribute: "status"
			# company-q00-productdata
				# check for the issues in tech val and then check for attributes in productdata
		# open vscode
			# investigate
				# source tree pull
				# product url on dbeaver
			# if spiders has data, go to et
			# if et has data: RERUN
			# if not data in et spiders and et: investigate code and site
		# if valid in site and not found in data
			# make screenshot
			# copy link of screenshot
		# VERDICT in tech val
			# cause: valid - 1
			# action: investigate
			# status: for verfication
			# remarks: https://prntscr.com/1yyqxa5
				# the screenshot of the page

	# 1060 : Old CMS : Q00 : description : www.xxl.fi : Q00FI800IJ000001607690202109290108 : -1 description
		# for Trello if needs more time to fix
		# verdict:
			# status: for trello

	# 1061	2021-11-10	Yes	Old CMS	O00	description	www.ep.nl "O00NLOT040000001335780202102220759, O00NLOT040000001388060202104210357"	-3 description
		# vscode
			# too many data extracted
		# verdict: 
			# lahi nga ticket sa validator
			# rerun

	# New CMS	K00	all	dns-shop.ru	"K00RUL80CG000001209300202009280432', 'K00RUL80CG000001524910202108020807','K00RUL80CG000001310680202101250559','K00RUL80CG000000979790202006250707','K00RUL80CG000000979770202006250707','K00RUL80CG000001310710202101250559'"	auto copy over
		# dbeaver
			# auto copy over
		# couch
			# runtime_statistics = "CRAWL_FAILED" 
		# rerun
			# python rerun_cmp.py -l 2021-11-10@21:00:00@K00RUL80CG000001209300202009280432 -s CRAWL_FAILED, 2021-11-10@21:00:00@K00RUL80CG000001524910202108020807, 2021-11-10@21:00:00@K00RUL80CG000001310680202101250559, 2021-11-10@21:00:00@K00RUL80CG000000979790202006250707, 2021-11-10@21:00:00@K00RUL80CG000000979770202006250707, 2021-11-10@21:00:00@K00RUL80CG000001310710202101250559 -s CRAWL_FAILED

			# python rerun_cmp.py -l 2021-11-10@21:00:00@K00RUL80CG000000979790202006250707, 2021-11-10@21:00:00@K00RUL80CG000000979770202006250707, 2021-11-10@21:00:00@K00RUL80CG000001310710202101250559 -s CRAWL_FAILED
		# Verdict
			# causes: proxy issues
			# remarks: multiple reruns; 5 mins interval

	# 783	2021-11-10	New CMS	P00	all	ozon.ru	'P00RUXZ01L000001575860202109060525', 'P00RUXZ01L000001575870202109060525', 'P00RUXZ01L000001575880202109060525', 'P00RU1W01L000001477080202107120510', 'P00RU1W01L000001460570202106280511', 'P00RU1W01L000001454890202106220209', 'P00RUXZ01L000001593540202109200719'	Spider Issue (-1) ; all fields

		# couch, not found
			# K00RUL80CG000001524910202108020807
			# 
		# compa, good
			# retest, all good
				# https://www.ozon.ru/product/smartfon-motorola-moto-g9-plus-4-128gb-siniy-225212411/
				# https://www.ozon.ru/product/smartfon-motorola-moto-e7-plus-4-64gb-siniy-225212403/
				# https://www.ozon.ru/product/smartfon-motorola-g9-play-xt2083-3-4-64gb-siniy-225212399/
				# https://www.ozon.ru/product/smartfon-motorola-moto-g-5g-4-64gb-temno-seryy-295058314/?asb=wCXcb0kB271f5uO2k7DFwZGG1MjXM9blJfiowBnfPZg%253D&asb2=CHIx5Pxo0FgOT_PWgat0v75jsJUJbs6Zge36JHfCrb0
				# https://www.ozon.ru/product/smartfon-motorola-moto-g10-4-64gb-temno-seryy-295059001/?asb=j4FHs72vp5SX9%252F6y8aa3S5QR5oOhmMQHMVvHteDnnOc%253D&asb2=9w8qInccwdo-WrpmN3HI3R70RR6nVWcJ1_tsRnLB_tg
				# https://www.ozon.ru/product/smartfon-motorola-moto-g30-6-128gb-chernyy-305536903/?asb=GIqqTDk9zj%252FPFVilNwR4FzWZBrQ7844XSdBSAZ2Jb78%253D&asb2=91siqwJlH4CMNqntZAbXqrTGUY_8UYT2Erhj5gFGbaY
				# https://www.ozon.ru/product/smartfon-motorola-edge-6-128gb-chernyy-293571191/?asb=r58StaiIj3EtpYo0n5R%252FGbGLdG3tyGoN7IHheKQrE7U%253D&asb2=3CTYoOTYfmehcfl2Ec-Y-i-DjtTgyayDUV3GMZfN2Ps&sh=pI6F54EG
		# et, good
		# decision: rerun with interval more than 10 mins

	#778 	2021-11-10	New CMS	Z00	in_stock	bhphotovideo.com 'Z00USC10O0000001362630202103310649', 'Z00USC10O0000001362510202103310636', 'Z00USC10O0000001362590202103310645', 'Z00USC10O0000001362310202103310629'	Not Found in Availability
		# compa - dl strat
			# self.scraper = cloudscraper.create_scraper()
				# used to penetrate cloudfare
				# try with complete headers
				# if not successful then for trello
			# verdict
				# cause - blocked
				# action - for trello
				# status - for trello

	# 796	2021-11-10	New CMS	K00	webshot_url	boulanger.com	all items	No webshot
		# use runner tool if bulk null webshot
		# rerun
			python manual_cmp_webshots.py -c K00 -i 2021-11-10@22:00:00@K00FR310CG000001209450202009280448,2021-11-10@22:00:00@K00FR310CG000001209650202009280507,2021-11-10@22:00:00@K00FR310CG000001215820202010080554

			python manual_cmp_webshots.py -c K00 -i 2021-11-10@22:00:00@K00FR310CG000001209450202009280448,2021-11-10@22:00:00@K00FR310CG000001209650202009280507,2021-11-10@22:00:00@K00FR310CG000001215820202010080554

11 11 21
	0 (R D-1) # 797	2021-11-11	Old CMS	610	webshot_url	imerco.dk	SELECT * FROM view_all_productdata where date='2021-11-11' and webshot_url is NULL	No Webshot
		# SELECT * FROM view_all_productdata where date='2021-11-11' AND webshot_url is NULL AND  country = 'GB'
			# www.currys.co.uk GB
				# python manual_cmp_webshots.py -c K00 -i 2021-11-11@23:00:00@K00GBI003F000000925190202002260844,2021-11-11@23:00:00@K00GBI003F000000925470202002260844,2021-11-11@23:00:00@K00GBI00CG000001297250202101180320,2021-11-11@23:00:00@K00GBI00CG000001297260202101180320,2021-11-11@23:00:00@K00GBI003F000001306930202101210507,2021-11-11@23:00:00@K00GBI003F000001307030202101210507,2021-11-11@23:00:00@K00GBI00MD000001453150202106160816,2021-11-11@23:00:00@K00GBI00FF000001453510202106160816,2021-11-11@23:00:00@K00GBI00CG000001526050202108020807,2021-11-11@23:00:00@K00GBI00AD000001541030202108110154,2021-11-11@23:00:00@K00GBI004K000001541180202108110154,2021-11-11@23:00:00@K00GBI00CG000001616130202110070649
			# www.fnac.com
				# test
					# K00FR210CG000001298430202101180414
						# python manual_cmp_webshots.py -c K00 -i 2021-11-11@22:00:00@K00FR210CG000001298430202101180414

					# python manual_cmp_webshots.py -c K00 -i 2021-11-11@22:00:00@K00FR210CG000001298570202101180414,2021-11-11@22:00:00@K00FR2103F000001306210202101210448,2021-11-11@22:00:00@K00FR2103F000001306450202101210448,2021-11-11@22:00:00@K00FR2103F000001539950202108110154,2021-11-11@22:00:00@K00FR2103F000001540070202108110154,2021-11-11@22:00:00@K00FR2105K000001540180202108110154

					# K00FR2105K000001540180202108110154
						# python manual_cmp_webshots.py -c K00 -i 2021-11-11@22:00:00@K00FR2105K000001540180202108110154
				# result:
					# after several reruns with interval (after queue in rabbit mq is 0), query result: returns to its original row numbers;
				# verdict
					# cause: blocked
					# action: rerun
					# status: for trello

	(R D-1) # 802	2021-11-11	New CMS	Z00	in_stock	bhphotovideo.com	Z00USC10O0000001362910202103310657	Not Found in Availability
		# SELECT * FROM view_all_productdata WHERE date = '2021-11-11' AND item_id = 'Z00USC10O0000001362910202103310657'
		# couch 
			# good in director-copmarison
			# company-z00
				# price - not found
				# availability - not found
			# compa
				# https://www.bhphotovideo.com/c/product/1501429-REG/sonos_oneslus1_one_sl_wireless_speaker.html
				
			# rerun

	(R D-1) # 808	2021-11-11	Old CMS	F00	all	fnac.be/nl	'F00BE9B040000001625250202110250511'F00BE9B040000001627830202110250541', 'F00BE9B040000001642070202111040958', 'F00BE9B040000001628230202110250541', 'F00BE9B0WJ000001641720202111040958', 'F00BE9B0JA000001627450202110250541', 'F00BE9B0JA000001627480202110250541', 'F00BE9B0JA000001629190202110250541', 'F00BE9B0JA000001629270202110250541'"	auto copy over (not dead) site is up

		# couch - 
			# F00BE9B040000001625250202110250511
			# F00BE9B0JA000001629190202110250541

	(R D-1) # 809	2021-11-11	Old CMS	200	all	heureka.cz	'200CZ9D070000000804640201907080646','200CZ9D080000000804630201907080644','200CZ9D070000000804260201907080347','200CZ9D070000000804270201907080350','200CZ9D070000000804320201907080402','200CZ9D070000000804350201907080416','200CZ9D070000000550290201904050558','200CZ9D070000000551040201904050624','200CZ9D080000000552060201904050701','200CZ9D080000000804490201907080629' ,'200CZ9D080000000804580201907080637' 
		# Invalid Deadlink

		# 200CZ9D070000000550290201904050558
		# 200CZ9D070000000551040201904050624
		# 200CZ9D080000000552060201904050701
		# 200CZ9D070000000804260201907080347
		# 200CZ9D070000000804270201907080350
		# 200CZ9D070000000804320201907080402
		# 200CZ9D070000000804350201907080416
		# 200CZ9D080000000804490201907080629
		# 200CZ9D080000000804580201907080637
		# 200CZ9D080000000804630201907080644
		# 200CZ9D070000000804640201907080646

		# 200CZ9D070000000550290201904050558
		# 200CZ9D080000000552060201904050701
		# 200CZ9D070000000804270201907080350
		# 200CZ9D070000000804350201907080416
		# 200CZ9D080000000804490201907080629

		# 200CZ9D070000000550290201904050558
		# 200CZ9D080000000552060201904050701
		# 200CZ9D070000000804270201907080350

		# 200CZ9D070000000550290201904050558

		# verdict:
			# causes: blocked
			# action: rerun

	Stats:
		Rerun: 4
			
11 12 21
	(F D-1) # RANK-299	11/9/2021	11/10/2021	Garmin	Invalid Deadlink	www.cykloteket.se	CYKELTRAINER	19-Nov-21	https://detailsupport.atlassian.net/browse/PRODUCTS-2966	https://prnt.sc/1yz6p7m
		# www.cykloteket.se
			# CYKELTRAINER
			# LÖPARKLOCKA
			# PULSKLOCKA
			# CYKELDATOR
			# RADAR
			# SMART WATCH
			# SMARTKLOCKA
			# SMARTWATCH
			# STEGRÄKNARE
			# TRÄNINGSKLOCKA

		# https://www.cykloteket.se/sok?q=RADAR#{%2277c30a0f0bf0%22:{%22sorting%22:[],%22filters%22:[],%22search_term%22:%22cykeltrainer%22,%22offsets%22:{%22category%22:10,%22brand%22:10,%22blog_post%22:10,%22product%22:10},%22tab%22:%22product%22}}

	(F D-1) # RANK-302	11/9/2021	11/10/2021	3M	Invalid Deadlink	www.allegro.pl	
		# apteczka
		# apteczka pierwszej pomocy
		# bandaż
		# cold hot
		# command
		# dekorowanie
		# futuro
		# haczyk
		# haczyk samoprzylepny
		# kompres
		# okład żelowy
		# opaska
		# orteza
		# pierwsza pomoc
		# plaster
		# plastry
		# plastry na odciski
		# stabilizator
		# stabilizator kolana
		# stabilizator nadgarstka
		# stabilizator stawu skokowego
		# stabilizatory
		# viscoplast
		# wieszanie obrazów

		# check pdp for all keywords

		# requester error 
		# add self.requester = requester in dl strategy

	(F D-1) RANK-303	11/9/2021	11/10/2021	3M	Invalid Deadlink	www.aptekagemini.pl	
		# bandaż
		# okład żelowy
		# orteza

		# issues
			# Proxy Error. Reason: Unhandled exception.
				# solution: requester request and check unblocker
				# or wait or just rerun
			# error in requester.post or requester.get
				# change headers
				# change form-data
				# in debug console:
					# just change the headers or form data
					# run the request.get or request.post

					# latin error in body
						# change the data = data.encode('utf-8')
			# solution
				# change the data = data.encode('utf-8') from requester.post(data=data)
				# tested on 3 invalid keyword and 1 valid keyword
	Stats:
		fix: 3[1]


11 13 21 
	(F D-1) # RANK-300	11/9/2021	11/10/2021	Garmin	"Data Count Mismatch 23(Fetched Data is Greater than expected) 
		# Data Count Mismatch 18(Fetched Data is Greater than expected) 
		# Data Count Mismatch 
		# 22(Fetched Data is Greater than expected)"	
		# www.xxl.se	
		# "HUNDHALSBAND HUNDPEJL PEJL" hundhalsband
		
		# Issues
			# mismatch in count
			# change of api version or search-api-v2
				# https://api.xxlsports.com/search-api-v2/sites/xxl-se/search
				# https://api.xxlsports.com/search-api-v3/sites/xxl-se/search
			# solution
				# search_string in ['_keywords' , ...] 	# depending on keywords
					# payload['provider'] = 'loop'

	(Reference fix: RANK-300) # RANK-307	11/9/2021	11/10/2021	Garmin	No Data	www.xxl.se	JAKTKLOCKA
		# Issues
			# list index out of range
			# solution
				# add on provider keyword
				# search_string in ['_keywords' , ...] 	# depending on keywords
					# payload['provider'] = 'loop'

	(Reference fix: RANK-300) # RANK-320	11/9/2021	11/12/2021	Garmin	Auto Copy Over for 1 day	www.xxl.se	HUNDPEJL
		# auto copy over
			# solution
				# add on provider keyword
				# search_string in ['_keywords' , ...] 	# depending on keywords
					# payload['provider'] = 'loop'

	(R D-1) # RANK-315	11/9/2021	11/12/2021	Garmin	Auto Copy Over for 1 day	www.netonnet.no	GPS BIL
		# Investigation
			# scraped 25

			# Solution
				# rerun
				# dbeaver query: 
					# SELECT * FROM view_all_rankingsdata WHERE date = '2021-11-13' AND website = 'www.netonnet.no' AND source != 'etl'

					# use this
						# SELECT date,source,website,keyword,ritem_id,country,count(1) from view_all_rankingsdata where website ='www.netonnet.com' and date = '2021-11-13' and keyword IN ('GPS BIL') GROUP BY date,source,website,keyword,ritem_id,country
				# runner bulk commands: 
					# cmd: cd C:\Users\kurt kevin nebril\Desktop\runner_tool
					# cmd: python app.py
					# paste the item_ids here or r_item_ids
						# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\input_item_ids.txt
					# cmd: 
						# how to generate webshots?
						# rerun_webshots -c A00 -s etl -d 2021-01-26

						# how to generate comparisons ?
						# rerun_comparison -c CRAWL_FINISHED -d 2021-01-26
					# rerun code generated here
						# C:\Users\kurt kevin nebril\Desktop\runner_tool\docs\rerun_cmd_generated.txt

					# identify the company code if prod1 or prod2
					# putty prod(n)
						# paste the generated file at putty
					# check rabbit mq or queues
					# rerun the sql in dbeaver if theres any changes

	(R D-1) # RANK-305	11/9/2021	11/10/2021	Garmin	Invalid Auto Copy Over 	
		# - Fetching Data even it's deadlink (PDP)
		# www.intersport.no	
		# SMARTKLOKKE
		# https://prnt.sc/1z03p0t

		# dbeaver query
			# SELECT date,source,website,keyword,ritem_id,product_url,country,count(1) from view_all_rankingsdata where website ='www.intersport.no' and date = '2021-11-13' GROUP BY date,source,website,keyword,ritem_id,product_url,country

		# valid deadlink, must find identifier to stop

	Stats:
		Rerun: 2 
		FIx: 1[1]

11 15 21
	(R D-2) # KEY-306	11/11/2021	11/11/2021	InRiver Staging (D10)	
		# Incomplete specs in www.coolblue.nl	D10NLO10H3120211109024004072491313	
		# 19-Nov-21	PRODUCTS-2905	
		# https://prnt.sc/1z3wk1v	

		# SELECT * FROM view_all_productdata vap WHERE date = '2021-11-17' AND website = 'www.coolblue.nl'
		# solution
			# duplicate keys evidence
				# https://prnt.sc/1zusl41

	(F D-1) # KEY-310	11/12/2021	11/12/2021	 Garmin (100)	
		# Incomplete specs in www.komplett.dk	
		# "All item_ids   Sample items 
			# 100DK93010000001646980202111090811
			# 100DK93010000001647030202111090811
			# 100DK93010000001647050202111090811"	
		# 19-Nov-21	PRODUCTS-2966
		# https://prnt.sc/1z8zsfc

		# https://www.komplett.dk/product/1145465/mobil/smartwatches/garmin-fenix-6-pro-sportsklokke
		# https://www.komplett.dk/product/1145461/mobil/smartwatches/garmin-fenix-6-sportsklokke
		# https://www.komplett.dk/product/1145462/mobil/smartwatches/garmin-fenix-6s-pro-sportsur-42mm-sort

		# Issues:
			# wrong scraping logic, requires loop inside the attributes of the table
				# solution change the logic
			# deepcopy dict
				# solution: source = (str)variable
		# tests
			# https://www.komplett.dk/product/1145465/mobil/smartwatches/garmin-fenix-6-pro-sportsklokke
			# https://www.komplett.dk/product/1145461/mobil/smartwatches/garmin-fenix-6-sportsklokke
				# duplicate keys
					# Beskyttelse
					# Type
					# Materiale
			# https://www.komplett.dk/product/1145462/mobil/smartwatches/garmin-fenix-6s-pro-sportsur-42mm-sort

	(F D-1) # KEY-313	11/12/2021	11/12/2021	 Nintendo Brazil (U00)	
		# Missing specs in www.kabum.com.br	
		# U00BRUW0TV020211109125509480824313
		# U00BRUW0TV020211109125509681522313
		# U00BRUW0TV020211109125509881543313	
		# 19-Nov-21	PRODUCTS-2962	
		# https://prnt.sc/1z9wzzs

		# https://www.kabum.com.br/produto/135586/nintendo-switch-32gb-1x-joycon-neon-azul-vermelho-hbdskaba1
		# https://www.kabum.com.br/produto/153548/nintendo-switch-32gb-1x-joycon-cinza-hbdskaaa1
		# https://www.kabum.com.br/produto/178104/controle-nintendo-switch-joy-con-vermelho-e-azul-hbcajaea1

		# SELECT * FROM view_all_productdata vap WHERE website = 'www.kabum.com.br'

		# Issues
			# no scraped data
				# solution
					# fix and update the function

	Stats:
		Rerun: 1[2]
		Fix: 2[1]

11 19 21
	(R D-2) # KEY-310	11/12/2021	11/12/2021	 Garmin (100)	Incomplete specs in www.komplett.dk	
		# All item_ids   Sample items 
		# 100DK93010000001646980202111090811
		# 100DK93010000001647030202111090811
		# 100DK93010000001647050202111090811
		# 19-Nov-21	PRODUCTS-2966	https://prnt.sc/1z8zsfc	"11/17 Kurt: Update spider
		# 11/18 Jaharra: Still an issue. https://prnt.sc/1zyv02b

		# SELECT * FROM view_all_productdata vap WHERE website = 'www.komplett.dk'
			# https://www.komplett.dk/product/1145461/mobil/smartwatches/garmin-fenix-6-sportsklokke
			# {"Producentens garanti (mneder)": "12", "Kapacitet": "64 MB", "Type": "Berringsknapper", "Trdls grnseflade": "Bluetooth, ANT+", "Teknologi": "Litiumion", "Etuiform": "Rund", "Navn": "Bnd", "Detaljer": "Kun etui: 57 g", "Pr-loadet software": "Garmin Pay", "Maks. dybde for vandmodstand": "100 m"}
			
				# 3 duplicates
					# Type
					# Materiale
					# Beskyttelse

			# https://www.komplett.dk/product/1145462/mobil/smartwatches/garmin-fenix-6s-pro-sportsur-42mm-sort
			# {"Producentens garanti (mneder)": "12", "Kapacitet": "32 GB", "Type": "Memory-in-Pixel LCD - farve - transflektiv", "Trdls grnseflade": "Bluetooth, Wi-Fi, ANT+", "Teknologi": "Litiumion", "Etuiform": "Rund", "Navn": "Bnd", "Bredde": "42 mm", "Vibratoralarm": "Ja", "Maks. dybde for vandmodstand": "100 m"}
				# Beskyttelse
				# Materiale
		
			# https://www.komplett.dk/product/1145462/mobil/smartwatches/garmin-fenix-6s-pro-sportsur-42mm-sort
			# {"Producentens garanti (mneder)": "12", "Kapacitet": "32 GB", "Type": "Memory-in-Pixel LCD - farve - transflektiv", "Trdls grnseflade": "Bluetooth, Wi-Fi, ANT+", "Teknologi": "Litiumion", "Etuiform": "Rund", "Navn": "Bnd", "Bredde": "42 mm", "Vibratoralarm": "Ja", "Maks. dybde for vandmodstand": "100 m"}
				# 
		# solution
			# 

	(R D-2) # RANK-309	11/9/2021	11/11/2021	Garmin
		# Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		# Data Count Mismatch 2 (Fetched Data is Lesser than expected)	
		# www.komplett.no

		# keyword
			# PULSKLOKKE
			# SKRITTELLER	

		# screenshots
			# https://prnt.sc/1z3moe1
			# https://prnt.sc/1z3mr1e
			# https://prnt.sc/200zuqs

	(R D-1) # KEY-313 KEY-314 KEY-320 KEY-321
		# U00@Nintendo Brazil@12:00NN@12:25AM@Prod2
		# 2021-11-19@02:00:00@
			# U00BRUW0TV020211109125509480824313
			# U00BRUW0TV020211109125509681522313
			# U00BRUW0TV020211109125509881543313

	(R D-2) # rank-309
		# SELECT * FROM view_all_rankingsdata WHERE date = '2021-11-19' AND website = 'www.komplett.no' and keyword = 'PULSKLOKKE'
		# SELECT date,source,website,keyword,ritem_id,product_url,country,count(1) from view_all_rankingsdata where website ='www.komplett.no' and date = '2021-11-19' and keyword in ('PULSKLOKKE','SKRITTELLER') GROUP BY date,source,website,keyword,ritem_id,product_url,country

		# www.komplett.no
		# 814983b9-75b6-4455-83ff-a11824f9f4d7@sWDIkgihEeeOEgANOrFolw
		# e3042c30-44e1-4e93-b42a-94de0f80687e@sWDIkgihEeeOEgANOrFolw

		# 2021-11-19@22:00:00@
		# 100@Garmin@12:00NN@12:35AM@Prod1

		# python rerun_rnk.py -l 2021-11-19@22:00:00@100@814983b9-75b6-4455-83ff-a11824f9f4d7@sWDIkgihEeeOEgANOrFolw -s CRAWLED_FINISHED
		# python rerun_rnk.py -s CRAWLED_FINISHED -l 2021-11-19@22:00:00@100@e3042c30-44e1-4e93-b42a-94de0f80687e@sWDIkgihEeeOEgANOrFolw

		# ---
		# python rerun_rnk.py -s CRAWL_FINISHED -l 2021-11-19@22:00:00@814983b9-75b6-4455-83ff-a11824f9f4d7@sWDIkgihEeeOEgANOrFolw,2021-11-19@22:00:00@e3042c30-44e1-4e93-b42a-94de0f80687e@sWDIkgihEeeOEgANOrFolw

	Stats:
		Rerun: 4[1.75]

11 20 21
	(F D-1) # KEY-323	11/19/2021	11/19/2021	Nintendo Benelux (110)	Update description in www.allyourgames.nl
		# '110NL311RV020211116232321793779320','110NL311RV020211116232322772884320','110NL311RV020211116232323183552320','110NL311RV020211116232324123691320','110NL311RV020211116232324612548320','110NL311RV020211116232325552945320','110NL311RV020211116232326015044320','110NL311RV020211116232326489415320','110NL311RV020211116232326942112320','110NL311RV020211116232327398563320','110NL311RV020211116232327884342320','110NL311RV020211116232328842850320','110NL311RV020211116232329310241320','110NL311RV020211116232329778965320','110NL311RV020211116232331716279320','110NL311RV020211116232332191127320','110NL311RV020211116232332685396320','110NL311RV020211116232333186545320','110NL311RV020211116232333572758320','110NL311RV020211116232333993980320','110NL311RV020211116232334509800320','110NL311RV020211116232334978386320','110NL311RV020211116232335439487320','110NL311RV020211116232335909568320','110NL311RV020211116232336859614320','110NL311RV020211116232337349890320','110NL311RV020211116232338579702320','110NL311RV020211116232339062324320','110NL311RV020211116232339529673320','110NL311RV020211116232339973026320'
		
		# 26-Nov-21	PRODUCTS-3005	https://prnt.sc/20143o6

		# urls to test
			# https://www.allyourgames.nl/nintendo-switch-miitopia.html
			# https://www.allyourgames.nl/nintendo-nintendo-switch-mario-kart-8-deluxe.html
			# https://www.allyourgames.nl/nintendo-nintendo-switch-super-mario-odyssey.html
			# https://www.allyourgames.nl/nintendo-switch-super-smash-bros-switch.html
			# https://www.allyourgames.nl/nintendo-switch-new-super-mario-bros-u-deluxe.html
			# https://www.allyourgames.nl/nintendo-switch-luigis-mansion-3.html

			# https://www.allyourgames.nl/nintendo-switch-pokemon-legends-arceus.html
			# https://www.allyourgames.nl/nintendo-switch-pokemon-shining-pearl.html

		# iteration
			* test atleast 8/8 urls

	(Fix reference) (R D-1) # KEY-324	11/19/2021	11/19/2021	Nintendo Benelux (110)	No scrapable desc, value shoud be -1 in www.allyourgames.nl	
		# '110NL311RV020211116232325061316320','110NL311RV020211116232328347196320','110NL311RV020211116232330274501320'

		# 26-Nov-21	PRODUCTS-3005	
		# https://prnt.sc/2014fuu

		# urls to test
			# https://www.allyourgames.nl/nintendo-switch-dr-kawashimas-brain-training.html
			# https://www.allyourgames.nl/nintendo-switch-51-worldwide-games.html
			# https://www.allyourgames.nl/nintendo-switch-ring-fit-adventure.html

		# Iteration
			/ test 3/3 urls if not found

	(F D-1) # LIST-391	11/17/2021	11/18/2021	Nintendo Benelux (110)	Invalid Deadlink	
		# www.nedgame.nl 
		# hardware
		# https://prnt.sc/1zz8yy7

		# https://www.nedgame.nl/zoek/systeem:nintendo-switch/type:spelcomputers/
		# https://www.nedgame.nl/zoek/systeem:nintendo-switch/type:games/

		# iteration
			/ test line by line
			/ test one working url
			/ test on the test subject

	Stats:
		Rerun: 1[1]
		Fix: 2[1]

11 21 21
	(F D-1) LIST-392	11/17/2021	11/18/2021	Nintendo Benelux (110)	
		# "Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		# Data Count Mismatch 10 (Fetched Data is Lesser than expected)"	

		# www.toychamp.nl
		# "https://prnt.sc/1zz9dgi
		# https://prnt.sc/1zz9f8f"

		# https://www.toychamp.nl/categorieen/games-multimedia/spelconsoles-accessoires/consoles/producten
		# https://www.toychamp.nl/categorieen/games-multimedia/games-spellen/games-voor-consoles/producten

		# iterations
			/ has_next()
				/ compare
					# <a aria-label="Volgende" class="pagination__next" data-pagination-next="" rel="next"><span>Volgende</span>
					# <svg class="svg-icon -arrow-left" role="img">
					# <use xlink:href="/assets/website/components/svg-icon/icon-set.008062c3.svg#arrow-left"></use>
					# </svg>
					# </a>

					# <a aria-label="Volgende" class="pagination__next" data-pagination-next="" data-pagination-page="2" href="/categorieen/games-multimedia/spelconsoles-accessoires/consoles/producten?page=2" rel="next"><span>Volgende</span>
					# <svg class="svg-icon -arrow-left" role="img">
					# <use xlink:href="/assets/website/components/svg-icon/icon-set.008062c3.svg#arrow-left"></use>
					# </svg>
					# </a>

				/ make identifier and handler
					#   elif not next_pagination_tag.get('data-pagination-page'):
						# return False

	(F D-1 (et)) LIST-395	11/17/2021	11/18/2021	Nintendo Benelux (110)	
		# -1 availability	www.toychamp.nl	
		# https://prnt.sc/1zza9v0
		# https://prnt.sc/1zzabzh

		# https://www.toychamp.nl/categorieen/games-multimedia/spelconsoles-accessoires/consoles/producten
		# https://www.toychamp.nl/categorieen/games-multimedia/games-spellen/games-voor-consoles/producten

		/ iteration
			/ ws strat
			/ tests
				/ software
				/ hardware
			/ check et
				/ availability
				/ rating
				/ reviews
				/ description

			/ push
				/ listings
					/ backup 
					/ fetch pull
					/ retain
					/ push
				/ et listings
					/ backup
					/ fetch pull
					/ push

	(F D-1) LIST-393	11/17/2021	11/18/2021	Nintendo Benelux (110)	
		# -1 availability	www.allyourgames.nl	
		# https://prnt.sc/1zz9r1o1
		# https://prnt.sc/1zza2tn

		# https://www.allyourgames.nl/nintendo-switch/nintendo-switch-consoles/
		# https://www.allyourgames.nl/nintendo-switch/nintendo-switch-games/

		# iteration
			# get_availability
				# defaulted

	(F D-1) (F D-1) LIST-394	11/17/2021	11/18/2021	Nintendo Benelux (110)	
		# -1 availability
		# www.bcc.nl

		# https://www.bcc.nl/gaming/nintendo/nintendo-games
		# https://www.bcc.nl/gaming/nintendo/nintendo-switch

		/ iteration	
			/ spiders
				/ product_tag.select_one('.gray-text')
				/ product_tag.select_one('.lister-product-offer__cta')

			/ et
				/ instock
				/ outstock
				* restest with the url

	Stats:
		Fix: 5[1]

11 25 21

	(R D-1) KEY-330	11/25/2021	11/25/2021	Garmin (100)	Incomplete description in www.jaktia.no	
		'100NOXS010000001438770202106080416','100NOXS010000001402820202105050324','100NOXS010000001597920202109270411','k100NOXS010000001330870202102111449','100NOXS010000001241430202011100358','100NOXS010000001266310202012230559','100NOXS010000001266490202012230651','100NOXS010000001241740202011100723','100NOXS010000001241400202011100358','100NOXS010000001330880202102111449','100NOXS010000001402830202105050324','100NOXS010000001241360202011100358','100NOXS010000001438780202106080416'
		# 


	(R D-1) RANK-339	11/17/2021	11/26/2021	Nintendo Benelux	Data Count Mismatch 1 (Fetched Data is Lesser than expected)	www.allyourgames.nl	spelcomputer	26-Nov-21	https://detailsupport.atlassian.net/browse/PRODUCTS-3011	https://prnt.sc/20x4308

		# Issues
			# json.loads() unhandled

			# solution
				# use pdp checker instead of json.loads handler
				# try fetch pull and check for changes

				# 110@Nintendo Benelux@12:00NN@12:25AM@Prod2

				# 2021-11-26@22:00:00@110@e772bc6b-f949-4561-a365-1de2e82b0bef@199a1daa-74e3-4ad2-9b85-089cd36d3866
		# can be fixed with rerun


	(R D-1) LIST-405	11/17/2021	11/26/2021	Nintendo Benelux (110)	Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		# hardware 26-Nov-21	
		# www.allyourgames.nl
		# https://prnt.sc/20x4bo9

		# 2021-11-26@22:00:00@110@311@72a29cdf-fe25-4323-9861-72abdb1ac878

		# # solution
		# 	# fix with rerun

	Stats:
		Rerun: 3[1]

11 27 21
	0 (R D-1) LIST-406	11/23/2021	11/26/2021	Nintendo Benelux  (110)	No Data	www.maxitoys.be	
		# "hardware
		# software"	
		# -2/3/2021
		# https://prnt.sc/20x4gtn

		# solution
			# check latest date to couch
			# check on ultima
				# search listings through listings-uuid
				# if not found in ultima or key word not active
					# verdict: 
						# For discussion
						# 11/27 Jave: retailer is valid no data because the run is only available at monday tuesday and wednsday as per sir rav investigation. wrong schedule set. please ask setup team regarding this issue"

	(R D-1) LIST-405	11/17/2021	11/26/2021	Nintendo Benelux (110)	Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		# www.allyourgames.nl	hardware	
		# 72a29cdf-fe25-4323-9861-72abdb1ac878	
		# 26-Nov-21	
		# https://detailsupport.atlassian.net/browse/PRODUCTS-3012	
		# https://prnt.sc/20x4bo9

	(R D-1) RANK-339	11/17/2021	11/26/2021	Nintendo Benelux	Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		# www.allyourgames.nl	spelcomputer	
		# 26-Nov-21	https://detailsupport.atlassian.net/browse/PRODUCTS-3011	
		# https://prnt.sc/20x4308

	(F D-1 (et)) PROD-701	11/24/2021	11/24/2021	Incorrect DQ scraped in fribikeshop.dk	GARMIN (100)	
		# select * from view_all_productdata where date='2021-11-24' and website='www.fribikeshop.dk' and in_stock=0	
		# December 3, 2021	
		# PRODUCTS-2994	https://prnt.sc/20r10x1- OOS DQ	

		# '100DKUH010000001400880202105040816','100DKUH010000001400920202105040816','100DKUH010000001400960202105040816','100DKUH010000001402590202105050323','100DKUH010000001402600202105050323',

		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-rs100-pedal-power-meter-96-010-02388-03
		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-rs200-pedal-power-meter-96-010-02388-02
		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-xc100-pedal-power-meter-96-010-02388-05
		# https://www.fribikeshop.dk/prod/96-010-02388-01/garmin-rally-rk100-pedal-power-meter
		# https://www.fribikeshop.dk/prod/96-010-02388-00/garmin-rally-rk200-pedal-power-meter

		# issues
			# if out of stock, delivery must be -1 (not found)
			# solution: 
				# modify in et
				# self.currentAvailability == Availability.OUT_OF_STOCK.value:
					# new_value = ETDefaults.NOT_FOUND.value

	(F D-1) IMG-162	11/25/2021	11/25/2021	Garmin (100)	Missing video count in  www.fribikeshop.dk	
		'100DKUH010000001223800202010280900','100DKUH010000000880760201911130951','100DKUH010000001400960202105040816','100DKUH010000000880710201911130948','100DKUH010000001223780202010280900','100DKUH010000001400920202105040816','100DKUH010000001400880202105040816','100DKUH010000001402600202105050323','100DKUH010000001402590202105050323','100DKUH010000001401000202105040816'	
		3-Dec-21	PRODUCTS-2994	https://prnt.sc/20vceos

		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-rs100-pedal-power-meter-96-010-02388-03
		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-rs200-pedal-power-meter-96-010-02388-02
		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-xc100-pedal-power-meter-96-010-02388-05
		# https://www.fribikeshop.dk/cykeludstyr/til-cyklen/pedaler/garmin-rally-xc200-pedal-power-meter-96-010-02388-04
		# https://www.fribikeshop.dk/prod/96-010-02388-01/garmin-rally-rk100-pedal-power-meter
		# https://www.fribikeshop.dk/prod/96-010-02388-00/garmin-rally-rk200-pedal-power-meter

	Stats:
		Rerun: 3[1]
		Fix: 2[1]

11 28 21
	(R D-2) 284	2021-11-28	Old CMS	100	all	www.verkkokauppa.com/fi	
			'AKTIIVISUUSKELLOT','AKTIIVISUUSRANNEKKEET','ÄLYKELLOT','NAVIGOINTI','URHEILUKELLOT',	
			# Auto Copy Over for 3 Days	
			# https://prnt.sc/20xrybo


			# ÄLYKELLOT	https://www.verkkokauppa.com/fi/catalog/1509b/Alykellot
			# 2021-11-28@21:00:00@020@cd5d4625d4b3fd0add80832441caacc0

			# solution
			# for rebuid as per action of sir Paul Ryan

			# "AKTIIVISUUSKELLOT
				020@41b79d480a47da11f00c23d27734b257
			# AKTIIVISUUSRANNEKKEET
				020@fb11c30d5990c88de0fc18899b608613
			# ÄLYKELLOT
				020@cd5d4625d4b3fd0add80832441caacc0
			# NAVIGOINTI
				020@4986df38f021a586914fa11f22847536
			# URHEILUKELLOT"
				020@fb31e1e691a6cad45bfed1e190398e09

			python rerun_lst.py -s CRAWL_FAILED -l 2021-11-28@21:00:00@020@41b79d480a47da11f00c23d27734b257,2021-11-28@21:00:00@020@fb11c30d5990c88de0fc18899b608613,2021-11-28@21:00:00@020@cd5d4625d4b3fd0add80832441caacc0,2021-11-28@21:00:00@020@4986df38f021a586914fa11f22847536,2021-11-28@21:00:00@020@fb31e1e691a6cad45bfed1e190398e09

	(R D-1) 285	2021-11-28	Old CMS	100	all	www.tbu.dk	
		# GARMIN Instrumenter	Auto Copy Over for 1 Day	
		# https://prnt.sc/20xs1o1

		# 2021-11-28@22:00:00@7J0@bab8f51f10ab1dd3a053c20dc1df44d7
		# https://tbu.dk/shop/30-garmin-instrumenter/

		# issues:
			# checked on spiders: have results and parallel to pdp
			# verdict: rerun
				# please careful next time with cd /opt/job-manager with cd /opt/job-manager-production

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2021-11-28@22:00:00@7J0@bab8f51f10ab1dd3a053c20dc1df44d7
			# 

	(R D-1) 287	2021-11-28	Old CMS	100	all	www.power.dk	
		# Smartwatches	
		# Auto Copy Over for 1 Day	
		# https://prnt.sc/20xs8di

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
				# id: 2021-11-28@22:00:00@X20@8a7bbc3c51d256b8c00f19280a7d69af
				# url: https://www.power.dk/mobil-og-mobil-tilbehoer/smartwatches/pl-1113/?from=categorypage&category=fitness&camplink=smartwatches
			# / check couch
			# / check pdp
			# / set local
			# / check local result and pdp
		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2021-11-28@22:00:00@X20@8a7bbc3c51d256b8c00f19280a7d69af
			# please careful next time with cd /opt/job-manager with cd /opt/job-manager-production
	
	(R D-1) 1406	2021-11-28	Old CMS	200		amazon.fr	
		'200FR71080000001384700202104200133','200FR710Q0000000104020201902071109','200FR71070000001384680202104200133','200FR710D0000000107020201902071112'
		# No Data

		# iteration
			# / copy on sheet
			# dbeaver
			# dbeaver get relevant info
				# id: 
				# url: 
			# check couch
			# check pdp
			# set local
			# check local result and pdp

		# issue
			# found on couch but not in sql
				# Write Jobs Issue
			# solution: re etl

		# solution:


			# python manual_cmp_et.py -c 200 -i 2021-11-28@22:00:00@200FR71080000001384700202104200133,2021-11-28@22:00:00@200FR710Q0000000104020201902071109,2021-11-28@22:00:00@200FR71070000001384680202104200133,2021-11-28@22:00:00@200FR710D0000000107020201902071112

	(R D-2) 1410	2021-11-28	Old CMS	200		amazon.it	all item_ids	no data
		# iteration
				# / copy on sheet
				# dbeaver
					# not found in dbeaver, search in previous date then get all the item ids then do re-etl
				# dbeaver get relevant info
					# id: 
					# url: 
				# check couch
				# check pdp
				# set local
				# check local result and pdp

		# 11 27 21
			# '200ITL4070000000530660201903270416',
			# '200ITL4070000000530740201903270425',
			# '200ITL4070000000530780201903270510',
			# '200ITL4070000000530820201903270524',
			# '200ITL4070000000530840201903270528',
			# '200ITL4070000000530880201903270537',
			# '200ITL4080000000530890201903270540',
			# '200ITL4070000000530920201903270547',
			# '200ITL4070000000530940201903270552',

			# python manual_cmp_et.py -c 200 -i 2021-11-28@22:00:00@200ITL4070000000530660201903270416,2021-11-28@22:00:00@200ITL4070000000530740201903270425,2021-11-28@22:00:00@200ITL4070000000530780201903270510,2021-11-28@22:00:00@200ITL4070000000530820201903270524,2021-11-28@22:00:00@200ITL4070000000530840201903270528,2021-11-28@22:00:00@200ITL4070000000530880201903270537,2021-11-28@22:00:00@200ITL4080000000530890201903270540,2021-11-28@22:00:00@200ITL4070000000530920201903270547,2021-11-28@22:00:00@200ITL4070000000530940201903270552

			# '200ITL4070000000530960201903270555',
			# '200ITL4080000000530980201903270556',
			# '200ITL4080000000530990201903270559',
			# '200ITL4080000000531010201903270601',
			# '200ITL4070000000531040201903270603',
			# '200ITL4080000000531050201903270618',
			# '200ITL4080000000531090201903270620',
			# '200ITL4070000000531120201903270621',
			# '200ITL4070000000531160201903270631',
			# '200ITL4070000000531080201903270618',
			# '200ITL4070000000531140201903270624',

			# python manual_cmp_et.py -c 200 -i 2021-11-28@22:00:00@200ITL4070000000530960201903270555,2021-11-28@22:00:00@200ITL4080000000530980201903270556,2021-11-28@22:00:00@200ITL4080000000530990201903270559,2021-11-28@22:00:00@200ITL4080000000531010201903270601,2021-11-28@22:00:00@200ITL4070000000531040201903270603,2021-11-28@22:00:00@200ITL4080000000531050201903270618,2021-11-28@22:00:00@200ITL4080000000531090201903270620,2021-11-28@22:00:00@200ITL4070000000531120201903270621,2021-11-28@22:00:00@200ITL4070000000531160201903270631

		# 2021-11-28@22:00:00@200ITL4070000000530660201903270416

	(R D-1) 783	2021-11-28	Old CMS	200	all	www.target.com	
		# amazon speaker
		# wireless headphones
		# wireless speakers

		# "Duplicate product_website & product_url

		# https://prnt.sc/20xt69p
		# https://prnt.sc/20xt71r
		# https://prnt.sc/20xt7af

		# iteration
			# / copy on sheet
			# / dbeaver
			# dbeaver get relevant info
				# id: 
				# url: 
			# check couch
			# / check pdp
			# / set local
			# / check local result and pdp

		# solution
			# rerun
			# cause: time difference

			# amazon speaker
				# 2021-11-28@04:00:00@b369e967-64f2-44f7-afaa-f9cbfe9f215b@0l0XDL-yEei-eQANOiOHiA

			# wireless headphones
				# 2021-11-28@04:00:00@4aa5c2a4-19b6-4c88-931a-2af886815006@0l0XDL-yEei-eQANOiOHiA

			# wireless speakers
				# 2021-11-28@04:00:00@dd65125a-d57c-4cbd-bbef-5944361a33e7@0l0XDL-yEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2021-11-28@04:00:00@@b369e967-64f2-44f7-afaa-f9cbfe9f215b@0l0XDL-yEei-eQANOiOHiA,2021-11-28@04:00:00@4aa5c2a4-19b6-4c88-931a-2af886815006@0l0XDL-yEei-eQANOiOHiA,2021-11-28@04:00:00@dd65125a-d57c-4cbd-bbef-5944361a33e7@0l0XDL-yEei-eQANOiOHiA

	(R D-1) 784	2021-11-28	Old CMS	200	all	www.electronicexpress.com	
		# earbuds with mic	
		# Data Count Mismatch 13 (Fetched Data is Less than expected)	
		# https://prnt.sc/20xt9nz

		# 2021-11-28@04:00:00@75a48477-ec80-4917-9b22-a16590fa3130@7a4aac63-7c10-4b23-8e1b-efc898766c5e

		# iteration
			# / copy on sheet
			# / dbeaver
			# dbeaver get relevant info
				# id: 75a48477-ec80-4917-9b22-a16590fa3130@7a4aac63-7c10-4b23-8e1b-efc898766c5e
				# url: 
			# check couch
				# if crawl failed
				# if ppt
			# / check pdp
			# / set local
			# / check local result and pdp

	(R D-1) PROD-697	11/24/2021	11/24/2021	Invalid DeadLink in PlayerOne.be	NINTENDO BENELUX(110)	
		'110BE911RV020211123023456800151327',
		'110BE911RV020211123023457230698327',
		'110BE911RV020211123023457697075327',
		'110BE911RV020211123023454702201327',
		'110BE911RV020211123023450362042327'

		110BE911RV020211123023454702201327
		110BE911RV020211123023456800151327
		110BE911RV020211123023457230698327
		110BE911RV020211123023457697075327
		110BE911RV020211123023454702201327
		110BE911RV020211123023456800151327
		110BE911RV020211123023457230698327
		110BE911RV020211123023457697075327
			
		December 3, 2021	
		PRODUCTS-3022	https://prnt.sc/20qnkul

		# iteration
			# / copy on sheet
			# / dbeaver
			# dbeaver get relevant info
				# id: 75a48477-ec80-4917-9b22-a16590fa3130@7a4aac63-7c10-4b23-8e1b-efc898766c5e
				# url: 
			# check couch
				# if crawl failed
				# if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

			https://playerone.be/SHIN-MEGAMI-TENSEI-V-199057.html
			https://playerone.be/POKEMON-BRILLIANT-DIAMOND-195558.html
			https://playerone.be/POKEMON-LEGENDS-ARCEUS-195564.html
			https://playerone.be/POKEMON-SHINING-PEARL-195561.html
			https://playerone.be/SHIN-MEGAMI-TENSEI-V-199057.html
			https://playerone.be/POKEMON-BRILLIANT-DIAMOND-195558.html
			https://playerone.be/POKEMON-LEGENDS-ARCEUS-195564.html
			https://playerone.be/POKEMON-SHINING-PEARL-195561.html

	Stats:
		Rerun: 8[10] == 1.25

11 30 21
	(R D-3) 815	2021-11-30	New CMS	P00	all	www.saturn.de	
		# Moto G10
		# Moto E7	
		# duplicate product_website	

		# https://prnt.sc/211fkyf
		# https://prnt.sc/211ezn6

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
				# id: c76c9aa9-bf2c-45cb-b6f2-d994e2ff2e32@UbbyaApIEem-gAANOiOHiA
				# url: https://www.saturn.de/de/search.html?page=9&query=Moto%20G10
			# check couch
				# / if crawl failed
				# ** if ppt
			# / check pdp
			# / set local
			# / check local result and pdp

		# solution
			# valid duplicate
			# evidence
				# https://prnt.sc/211rvb5
				# https://prnt.sc/211s21h

		# 2021-11-30@22:00:00@P00@c76c9aa9-bf2c-45cb-b6f2-d994e2ff2e32@UbbyaApIEem-gAANOiOHiA

		# final_list[0][0].get('title').get('value')

		# for x in final_list[2]:
    		# print(x.get('title').get('value'))

  		# local result
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	MOTOROLA G10 64 GB Weiss Dual SIM
			# 	MOTOROLA G10  64 GB Grau Dual SIM
			# 	FLAT DESIGN BY MAREIKE KRIESTEN LISBOA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN LISBOA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN LISBOA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN LISBOA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN LISBOA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit
			# 	FLAT DESIGN BY MAREIKE KRIESTEN COIMBRA Handyhülle Schutz Hülle Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, grau
			# 	FLAT DESIGN BY MAREIKE KRIESTEN EVORA Handyhülle Schutz Hülle veganer Filz für Motorola Moto G10 Power, Sleeve, Motorola, Moto G10 Power, anthrazit

	(R D-1) 305	2021-11-30	New CMS	P00	all	www.telenor.rs	
		# iteration
		# / copy on sheet
		# / dbeaver
		# / dbeaver get relevant info
			# id: P00@PU0@ac224946-640f-4a31-8142-720c6a24b1bc
			# url: https://www.telenor.rs/webshop/sr/Privatni-korisnici/Mobilni-telefoni/?purchase=cosmos&tpackage=368
		# check couch
			# / if crawl failed
			# * if ppt
		# / check pdp
		# / set local
		# / check local result and pdp

		# All phones offer with tariff	
		# Auto Copy Over for 1 Day	
		# https://prnt.sc/211rjls

		# 2021-11-30@22:00:00@P00@PU0@ac224946-640f-4a31-8142-720c6a24b1bc

		# solution
			# rerun 

	(R D-1) 306	2021-11-30	New CMS	P00	all	www.vodafone.co.uk	
		# Business 5G Phones	
		# Auto Copy Over for 1 Day	
		# https://prnt.sc/211uca8

		# iteration
			# / copy on sheet
			# * dbeaver
			# * dbeaver get relevant info
			# id: P00@FS0@9b32cdae-b83f-4b88-80b2-3aeffaed8a82
			# url: https://www.vodafone.co.uk/business/business-mobile-phones/5g
			# check couch
				# * if crawl failed
				# * if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

	(R D-1) 1497	2021-11-30	Old CMS	200	all	fnac.com/3P	
		# 200FRDU0J0000001381160202104140811	
		# Invalid Deadlink

	(R D-2) 1504	2021-11-30	Old CMS	200	video_count	www.netonnet.se	
		# '200SE90070000000030040201901180326','200SE90070000000034450201901180329','200SE900H0000000041620201901180334','200SE90070000000958300202006051220','200SE900D0000000036660201901180329',	
		# incorrect video count

		# requested item_ids
		1	# https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/utan-batteri/marshall-stanmore-ii-bt-black/1004816.15256/
				# 4 good
		1	# https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/marshall-kilburn-ii-black/1004822.13770/
				#  4 good
		0	# https://www.netonnet.se/art/ljud-och-bild/horlurar/over-ear/bose-soundlink-ae-ii-black/222946.9293/

		1	# https://www.netonnet.se/art/ljud-och-bild/horlurar/tradlosahorlurar/jabra-move-style-edition-titanium-black/1007358.9296/
		1	# https://www.netonnet.se/art/ljud-bild/hogtalare/bluetooth-hogtalare/utan-batteri/marshall-acton-ii-bt-white/1004815.15256/

		# from other unrequested item_ids
		0	# https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/bose-soundlink-iii/199508.13770/
				# valid discontinued
		1	# https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/ultimate-ears-megaboom-black/239978.13770/
				# valid discontinued
		4	# https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/utan-batteri/marshall-woburn-ii-bt-black/1004818.15256/
				# instock
		# test
			# check all availability
			
		# 4:'https://youtu.be/yUYPQ9AXHYU'
		# 3:'https://youtu.be/lzZ40xWRya4'
		# 2:'https://youtu.be/lvVeu52V24U'
		# 1:'https://youtu.be/RiYdxdu3V9A'
		# 0:'

	Stats:
		Rerun: 5 [1.6]
12 04 21
	(R D-1) 910	2021-12-04	Old CMS	200	all	www.saturn.de	
		# voice lautsprecher	
		# Duplicate product_website & product_url	
		# https://prnt.sc/21msyk2

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
			# id: c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA
			# url: 
			# check couch
				# / if crawl failed
				# / if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

		# solution
			# valid dubplicate in pdp
				# send screenshot

	0 (R D-1) 911	2021-12-04	Old CMS	200	all	www.netonnet.se	
		'bluetooth hörlurar in ear','google högtalare','hörlurar in ear',	

		# "Data Count Mismatch 24 (Fetched Data is Less than expected)
		# Data Count Mismatch 1 (Fetched Data is Less than expected)
		# Data Count Mismatch 24 (Fetched Data is Less than expected)"

		# "https://prnt.sc/21miohz
		# https://prnt.sc/21mitjz
		# https://prnt.sc/21miwb5"

		# 83d7d04b-5283-4720-a0a1-5a13fc854947@HxbbnONtEeaOEAANOrFolw
		# df95e8bc-7743-410e-9d36-eb57cf4a3c7c@HxbbnONtEeaOEAANOrFolw
		# 75c80c5e-2387-4b0a-b566-17b30a3135b1@HxbbnONtEeaOEAANOrFolw

		# python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-04@22:00:00@83d7d04b-5283-4720-a0a1-5a13fc854947@HxbbnONtEeaOEAANOrFolw,2021-12-04@22:00:00@df95e8bc-7743-410e-9d36-eb57cf4a3c7c@HxbbnONtEeaOEAANOrFolw,2021-12-04@22:00:00@75c80c5e-2387-4b0a-b566-17b30a3135b1@HxbbnONtEeaOEAANOrFolw

		# solution
			# after 2 attempts, source not changed to etl
				# for trello

	(R D-1) 912	2021-12-04	Old CMS	200	all	www.re-store.ru	
		# наушники с проводом	
		# Data Count Mismatch 6 (Fetched Data is Less than expected)	
		# https://prnt.sc/21mn19n

		# count mismatch in local, sometimes, it matches with same number of products in pdp
			# but after several run in local and sevaral reruns, product counts are inconsistent

	Stats:
		Rerun: 3

12 05 21
	(R D-2) 327	2021-12-05	Old CMS	200	all	www.re-store.ru	
		# "Headphones Wired In Ear
		# Speakers Voice"	

		# "Data Count Mismatch 4(Fetched Data is Lesser than expected)
		# Data Count Mismatch 1(Fetched Data is Lesser than expected)"	

		# "https://prnt.sc/21s3l2x
		# https://prnt.sc/21s3abb"

		# LQ0@a61f2fb3db13dc1626f1cb8a4d858a4f
		# LQ0@febf3b860184fee171b291cbe59886f1

		# https://www.re-store.ru/accessories/acoustics/at_portable-speakers/osob_golos-podskazki/

		# solution
			# after 5 attempts, on Speakers Voice keyword, proxy issue

	(R D-1) 922	2021-12-05	Old CMS	200	all	www.elgiganten.se	
		'hörlurar in ear','trådlösa hörlurar in ear','trådlösa hörlurar träning','vattentät högtalare'

		# Duplicate product_website & product_url	

		# "https://prnt.sc/21s3n70
		# https://prnt.sc/21s40rd
		# https://prnt.sc/21s4wq1
		# https://prnt.sc/21s56w7
		# https://prnt.sc/21s5c5l"

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
			# id: 
			# url: 
			# check couch
				# / if crawl failed
				# / if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

		# hörlurar in ear	Sony trådlösa in ear-hörlurar WF-1000XM3 (silver)	https://www.elgiganten.se/product/ljud-hifi/horlurar/42699/sony-tradlosa-in-ear-horlurar-wf-1000xm3-silver	75c80c5e-2387-4b0a-b566-17b30a3135b1@fVQR1tMrEeaOEAANOrFolw
		# 	https://prnt.sc/21sc9wb
		# hörlurar in ear	Sony trådlösa in ear-hörlurar WF-1000XM3 (svarta)	https://www.elgiganten.se/product/ljud-hifi/horlurar/42700/sony-tradlosa-in-ear-horlurar-wf-1000xm3-svarta	75c80c5e-2387-4b0a-b566-17b30a3135b1@fVQR1tMrEeaOEAANOrFolw
		# 	https://prnt.sc/21sccn2
		# hörlurar in ear	Sony trådlösa in ear-hörlurar WI1000XM2 (silver)	https://www.elgiganten.se/product/ljud-hifi/horlurar/120822/sony-tradlosa-in-ear-horlurar-wi1000xm2-silver	75c80c5e-2387-4b0a-b566-17b30a3135b1@fVQR1tMrEeaOEAANOrFolw
		# 	https://prnt.sc/21scgkx
		# trådlösa hörlurar in ear	Trådlösa Hörlurar Jib True TWS In-Ear Svart	https://www.elgiganten.se/product/ljud-hifi/horlurar/288202/tradlosa-horlurar-jib-true-tws-in-ear-svart	47580afb-3c40-4e7e-bdba-a84ee624f879@fVQR1tMrEeaOEAANOrFolw


		# trådlösa hörlurar träning	Jabra Elite Active 65T trådlösa hörlurar (blå)	https://www.elgiganten.se/product/ljud-hifi/horlurar/JELITEAC65TBL/jabra-elite-active-65t-tradlosa-horlurar-bla	5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw
		# trådlösa hörlurar träning	Jabra Elite Active 65T trådlösa hörlurar (titan)	https://www.elgiganten.se/product/wearables-och-traning/sporthorlurar/11883/jabra-elite-active-65t-tradlosa-horlurar-titan	5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw

		# -----------
		# trådlösa hörlurar träning	Jabra Elite Active 65T trådlösa hörlurar (blå)
		# trådlösa hörlurar träning	Jabra Elite Active 65T trådlösa hörlurar (titan)


	(R D-1) 1712	2021-12-04	New CMS	P00	all	alta.cz	
		P00CZ3601L000001429890202106010821	
		spider issue -1 values 

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
				# id: P00CZ3601L000001429890202106010821
				# url: https://www.alza.cz/motorola-moto-g9-plus?dq=6145971
			# * check couch
				# * if crawl failed
				# * if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

	(R D-1) 1720	2021-12-05	New CMS	100	all	elgiganten.dk	
		'100DK10010000001248290202011250630','100DK10010000001397640202105040251','100DK10010000001397790202105040251','100DK10010000001222810202010280537'

		invalid -1 values

		# iteration
			# / copy on sheet
			# / dbeaver
			# / dbeaver get relevant info
			# id: c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA
			# url: 
			# check couch
				# / if crawl failed
				# / if ppt
			# * check pdp
			# * set local
			# * check local result and pdp

	(R D-1) 1718	2021-12-05	Old CMS	200	all	heureka.cz	
		# '200CZ9D080000000804600201907080640','200CZ9D080000000804610201907080642','200CZ9D070000000804320201907080402','200CZ9D070000000550290201904050558','200CZ9D070000000551040201904050624','200CZ9D080000000804490201907080629' 
		# invalid deadlink

		# Issues:
			# visible field is 0
		# solution
			# rerun

	(R D-1) 1719	2021-12-05	Old CMS	200	all	amazon.de/3p	
		# 200DE52080000000557040201904220902	
		# invalid deadlink

		# Issues:
			# visible field is 0
		# solution
			# rerun

	(R D-1) 1716	2021-12-05	Old CMS	200	all	amazon.com.mx	
		200MXVC080000001245880202011200116	
		invalid deadlink

		# Issues:
			# visible field is 0
		# solution
			# rerun

	(R D-1) 1717	2021-12-05	Old CMS	200	all	amazon.com.mx	
		# 200MXVC080000001245880202011200116

		# 200MXVC070000000900800202001200600
		# 200MXVC070000000900840202001200608
		# 200MXVC070000000900860202001200613
		# 200MXVC070000000900890202001200622
		# 200MXVC070000000901060202001200650
		# 200MXVC070000000901100202001200700
		# 200MXVC070000000901120202001200705
		# 200MXVC070000000901140202001200708
		# 200MXVC0NC000000901350202001200751
		# 200MXVC0NC000000901370202001200753
		# 200MXVC070000000901420202001200943
		# 200MXVC070000000901440202001200946
		# 200MXVC070000000901460202001200951
		# 200MXVC070000000901480202001200953
		# 200MXVC0B0000000901500202001210307
		# 200MXVC0B0000000901510202001210309
		# 200MXVC0B0000000901520202001210310
		# 200MXVC0B0000000901530202001210315
		# 200MXVC0B0000000901540202001210319
		# 200MXVC0B0000000901560202001210322
		# 200MXVC0B0000000901570202001210329
		# 200MXVC0B0000000901580202001210337
		# 200MXVC0B0000000901600202001210342
		# 200MXVC0B0000000901620202001210404
		# 200MXVC0C0000000901630202001210411
		# 200MXVC0D0000000901680202001210548
		# 200MXVC0D0000000901710202001210603
		# 200MXVC0D0000000901720202001210609
		# 200MXVC0D0000000901750202001210617
		# 200MXVC0D0000000901780202001210626
		# 200MXVC0D0000000901800202001210629
		# 200MXVC0D0000000901810202001210636
		# 200MXVC0D0000000901840202001210646
		# 200MXVC0D0000000901870202001210658
		# 200MXVC0G0000000901900202001210725
		# 200MXVC0G0000000901910202001210730
		# 200MXVC0G0000000901930202001210733
		# 200MXVC0H0000000901950202001210738
		# 200MXVC0J0000000901960202001210749
		# 200MXVC0M0000000901980202001210756
		# 200MXVC0N0000000902010202001210812
		# 200MXVC0N0000000902020202001210817
		# 200MXVC0O0000000902030202001210822
		# 200MXVC0O0000000902040202001210823
		# 200MXVC0P0000000902060202001210831
		# 200MXVC0P0000000902080202001210837
		# 200MXVC0P0000000902090202001210839
		# 200MXVC0P0000000902100202001210844
		# 200MXVC0B0000000902120202001210910
		# 200MXVC0B0000000902130202001210912
		# 200MXVC0B0000000902160202001210914
		# 200MXVC0D0000000902170202001210917
		# 200MXVC0D0000000902190202001210919
		# 200MXVC0D0000000902220202001210925
		# 200MXVC0D0000000902250202001210930
		# 200MXVC070000000991480202007100118
		# 200MXVC070000000991500202007100122
		# 200MXVC070000000991520202007100151
		# 200MXVC070000000991530202007100153
		# 200MXVC080000001245880202011200116
		# 200MXVC070000001397430202105040205
		# 200MXVC070000001560030202108230658
		# 200MXVC070000001560350202108230658
		# 200MXVC0NC000001575570202109060525
		# 200MXVC0NC000001575650202109060525

	stats:
		Rerun: 8 (9) = 1.13

12 08 21
	(R D-1) 341	2021-12-06	Old CMS	200	all	www.bol.com	
		Speakers Bluetooth	
		1 Day Auto Copy over 	
		https://prnt.sc/228h8z8

		N10@4fef0bb8ea61b716ad620c815da6f0f7

		# solution: rerun

	(R D-1) 345	2021-12-08	New CMS	K00	all	www.inet.se	
		Tangentbord Gaming	
		1 Day Auto Copy over 	
		https://prnt.sc/228r565

		# solution: rerun

	(R D-1) 1800	2021-12-08	New CMS	K00	description	power.fi	
		# "K00FI0X05K000001554480202108180340
		# K00FI0X0CG000001487930202107130336"	

		# Missing description	https://prnt.sc/228pdy4 

		# solution
			# dbeaver check, data already filled, no rerun executed

	(R D-1) 1801	2021-12-08	New CMS	K00	all	www.mediamarkt.de	
		'K00DE8103O000001433040202106020354','K00DE8103O000001433010202106020354','K00DE810AD000001433340202106020354','K00DE810CG000000923580202002190707','K00DE810CG000001244930202011170410','K00DE8109O000001433550202106020354','K00DE8104K000001433740202106020354','K00DE810CG000001492530202107190651','K00DE8103F000000925340202002260844','K00DE810CG000001296700202101180225','K00DE810CG000001296590202101180224','K00DE810CG000001296480202101180224','K00DE810CG000001209630202009280507','K00DE8103F000000989100202007060853','K00DE810CG000001373420202104050819','K00DE810AD000001537470202108110153','K00DE810AD000001537440202108110153','K00DE8103F000001305550202101210359','K00DE810AD000001537240202108110153','K00DE8104K000001538030202108110153','K00DE810CG000001399180202105040624','K00DE8103F000001305700202101210359'
			# missing data

		# unsolved > solved
			K00DE810CG000001373420202104050819
			K00DE810CG000001399180202105040624
			K00DE8103O000001433040202106020354
			K00DE8109O000001433550202106020354
			K00DE8104K000001433740202106020354

		# solution
			# rerun

	(R D-1) 1803	2021-12-08	New CMS	K00	description	power.no	
		# K00NOZ20CG000001467420202107050618	
		# missing description

		# solution
			# rerun

	(R D-1) 1804	2021-12-08	New CMS	K00	specifications	power.no	
		'K00NOZ20CG000001467420202107050618','K00NOZ203F000001555040202108180340','K00NOZ20CG000001467550202107050618','K00NOZ20CG000001467620202107050618','K00NOZ207U020211116100215317630320','K00NOZ20CG000001467160202107050618','K00NOZ20CG000001467070202107050618','K00NOZ207U020211116100216734030320','K00NOZ20CG000001466870202107050618','K00NOZ20CG000001466940202107050618','K00NOZ20CG000001466830202107050618','K00NOZ20CG000001466880202107050618','K00NOZ20CG000001467390202107050618'

		# unsolved
			K00NOZ20CG000001467390202107050618
			# checked on pdp, unavailable
			# not checked on local

		# missing specifications

		# solution
			# rerun

	(R D-1) 970	2021-12-08	New CMS	K00	all	virginmegastore.ae	
		# PS Controller	
		# Auto copy over for 1 day	
		# https://prnt.sc/228oeyj

		# solution: rerun

	(R D-1) 971	2021-12-08	New CMS	K00	all	otto.de	
		# "GAMING LAPTOP
			# https://prnt.sc/2294uji
		# GAMING-NOTEBOOK
			# fb134120-09bc-4aa4-a14b-bb8abc9ac0f4@xhsDgt_tEee8oQANOijygQ
			# https://prnt.sc/229668q
		# Kopfhörer"
			# https://prnt.sc/229570v	

		# Duplicate product_website & product_url	

		# "https://prnt.sc/228isoh
		# https://prnt.sc/228jxj9
		# https://prnt.sc/228lvyv"

		# solution
			# investigate and rerun
				# find duplicate for each keyword (rankings) in sql and search the keyword in pdp if duplicate
				# if duplicate in sql but not in pdp, do rerun
			# send evidences for duplicate keywords and sql result (for rerun result (with no duplicates in sql))

	(R D-1) 972	2021-12-08	New CMS	K00	all	otto.de	
		# "Gaming-Lautsprecher
			68d80d26-dddb-4eda-a59e-79cccb8da770@xhsDgt_tEee8oQANOijygQ
		# PS-Controller
			df63ba3c-a4c8-40b1-a500-5e595e95aa84@xhsDgt_tEee8oQANOijygQ
		# ANC-Kopfhörer
			4ee23cfe-9324-4c10-b786-729306516638@xhsDgt_tEee8oQANOijygQ	

		# invalid deadlink	
		# "https://prnt.sc/228nc7v 
		# https://prnt.sc/228nqti
		# https://prnt.sc/228nwyq"

		# issues:
			# checked in local, 'get_product_tags' returns no result

		# action:
			# for trello since it will require more time to fix the problem

	(R D-1) 984	2021-12-08	Old CMS	200	all	
		www.boulanger.com

		'casque à réduction de bruit','enceinte bluetooth','enceinte bluetooth portable','mini enceinte bluetooth'

		# 8a1b70f9-4eb2-47ea-ae5b-782ef2545e4f@WKYSvApHEem-gAANOiOHiA
		0e47abb4-4b56-4668-b6ca-21842b1b99be@WKYSvApHEem-gAANOiOHiA
		51405791-5c01-4b1a-9f24-1e5f24dd5bbf@WKYSvApHEem-gAANOiOHiA
		4c50f1e9-6a50-42ba-96ab-ff8cf9d8ce9a@WKYSvApHEem-gAANOiOHiA

		Day 3 auto copy over
		https://prnt.sc/228gndh

		# issue
			# tested on local, no data scraped

			# 'casque à réduction de bruit' > this keyword is valid deadlink
				# https://prnt.sc/229tvvd

		# solution
			# update dl strategy
			# do rerun after 6pm rebuld

	Stats:
		Rerun: 10

12 09 21

	(F D-1) 1828	2021-12-08	New cms	100	price_value	
		# komplett.dk	

		# '100DK93010000001647790202111090811','100DK93010000001647990202111090811','100DK93010000001647970202111090811'
		
		# https://www.komplett.dk/product/1168444/mobil/smartwatches/garmin-venu-sq-smartur-whitelight-gold
		# https://www.komplett.dk/product/1168792/mobil/smartwatches/garmin-vivomove-3s-sport-hybrid-sportsur-39mm-guld
		# https://www.komplett.dk/product/1158542

		# price should be -1 not 0

		# Solution
			# fix et
			# rerun after rebuild
			# done rerun, 
				# but questionnable value:
				# -1.0 instead of -1?
				# goods since -1.0 still considered as not found

	(R D-1) 985	2021-12-09	New CMS	O00	all	www.vandenborre.be/nl
		# "Addwash
			6ae95e83-d5de-4c8c-b63c-0d500e73935c@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d
		# Smart tv"	
			62d18374-bf09-434c-aedd-321b3b4697e0@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d

		# "Data Count Mismatch 5 (Fetched Data is Greater than expected)
		# Data Count Mismatch 12 (Fetched Data is Greater than expected)"	

		# "https://prnt.sc/22dytfp
		# https://prnt.sc/22e2xb9"

		# iteration
			# checked on site and local > match 
			# sometimes mismatch local

		# solution
			# rerun > caused for proxy issue or blocked

	(R D-1) 986	2021-12-09	New CMS	O00	all	www.vandenborre.be/nl	
		# Energiezuinige koelkast
			28bc7394-ef99-494a-a32a-3f3f6b900220@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d
		# was-droogcombinatie
			b78aa926-43ea-4c10-bdc0-943201617b2c@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d
		# was-droogcombinatie aanbieding
			76554b56-3f3a-4899-b3a2-2bc303c1fac5@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d
		# Bluetooth soundbars
			343e3a55-828f-4ad0-9f6c-a90be384c1df@2cedb1b5-2a13-4dba-98c1-c7f3b5d5914d

		# Auto copy over for 1 day	

		# "https://prnt.sc/22dzsx9
		# https://prnt.sc/22e1z3y
		# https://prnt.sc/22e26jg
		# https://prnt.sc/22e3idi"

		# solution
			# rerun

	(R D-1) 991	2021-12-09	New CMS	O00	all	www.mediamarkt.be/nl/	
		# Droger	  
		# Data Count Mismatch 4 (Fetched Data is Lesser than expected)	
		# https://prnt.sc/22e98ys

		# iteration
			# local and site > matched count
			# but when counted manually per products in site, it gets 18 counts
				# overcount means sponsored items

		# solution
			# check if it has trello ticket
			# if it has, use the ticket and add this 

	(R D-1) 1834	2021-12-09	Old CMS	200	all	amazon.it/3P	
		'200ITGF080000000556420201904220805','200ITGF070000000557480201904220941','200ITGF080000000557420201904220941','200ITGF070000000557870201904220956'
		# Invalid Deadlink

		# unsolved
			# 200ITGF080000000557420201904220941
				# this is CRAWL_FAILED different from other piers as CRAWL_FINISHED
			# solved by rerun status CRAWL_FAILED

		# solution
			# rerun

	(R D-1) 1836	2021-12-09	New CMS	P00	all	mts.rs	
		# P00RSXV01L000001622700202110180740	
		# invalid deadlink

		# issues
			# no issues since visible is 1

		# rerun?
			# waiting for verfication since no action was executed
			# goods

	0 (R D-1) 992	2021-12-09	Old CMS	200	all	www.electronicexpress.com	
		# multiroom speaker	
		# Data Count Mismatch 20 (Fetched Data is Lesser than expected)	
		# https://prnt.sc/22ejdq7

		# issues:
			# mismatch in local and pdp

		# solution
			# for trello since they need to submit it before 15:00
			# Started: 14:00

	(R D-1) 1842	2021-12-09	New CMS	K00	all	power.dk	
		# K00DKX20CG000001489870202107130337	
		# spider issue -1 value	

		# https://www.power.dk/gaming-og-underholdning/pc/gaming-headset/razer-nari-essential-gamingheadset/p-974066/

		# issues
			# unhandled tags in compa
			# checked for source> no tags found and text query for 
				# Ikke længere tilgængelig ("no longer available")
			# checked couch > ppt

		# solution
			# rerun

	(R D-1) 1843	2021-12-09	New CMS	K00	price_value	komplett.no	
		'K00NO120AD000001447240202106150451',
		'K00NO120AD000001447230202106150451',
		'K00NO1204K000001554660202108180340',
		'K00NO120CG000000922830202002190707',
		'K00NO120CG000001299190202101180427',
		'K00NO1204K000001554680202108180340',
		'K00NO1204K000001447270202106150451',
		'K00NO1204K000001554710202108180340',
		'K00NO1204K000001554620202108180340',
		'K00NO1204K000001554650202108180340',
		'K00NO1204K000001554760202108180340',
		'K00NO1204K000001554890202108180340',
		'K00NO1205K000001555200202108180340',
		'K00NO1204K000001554770202108180340',
		'K00NO1204K000001554780202108180340',
		'K00NO120CG000001356720202103290840',
		'K00NO1204K000001554790202108180340',
		'K00NO120CG000001244830202011170408',
		'K00NO1204K000001554820202108180340',
		'K00NO1204K000001554840202108180340',
		'K00NO1204K000001446780202106150451',
		'K00NO120CG000001356580202103290840',
		'K00NO120FF000001447090202106150451',
		'K00NO120CG000001399810202105040624',
		'K00NO120CG000001492360202107190605',
		'K00NO120CG000001356560202103290840',
		'K00NO120CG000001356320202103290840',
		'K00NO120CG000001399830202105040624',
		'K00NO120CG000001356340202103290840',
		'K00NO120CG000001399800202105040624',
		'K00NO120CG000001360130202103300725',
		'K00NO120CG000001399820202105040624',
		'K00NO120CG000001399780202105040624',
		'K00NO120CG000001399740202105040624',
		'K00NO120CG000001399760202105040624',
		'K00NO120CG000001399770202105040624',
		'K00NO120CG000001399750202105040624',
		'K00NO120CG000001399720202105040624',
		'K00NO120CG000001399730202105040624',
		'K00NO120CG000001356460202103290840',
		'K00NO120CG000001399790202105040624',
		'K00NO120CG000001399710202105040624'

		# 0 value in price 	

		# https://prnt.sc/22eyhu7

		# https://www.komplett.no/product/987036/gaming/gaming-utstyr/gamingstoler/corsair-gaming-t1-race-gamingstol-sortb?feature=freightwidget
		# https://www.komplett.no/product/1100369/gaming/gaming-utstyr/gaming-tastatur/razer-huntsman-elite-gaming-tastatur
		# https://www.komplett.no/product/987037/gaming/gaming-utstyr/gamingstoler/corsair-gaming-t2-road-gamingstol-sort?feature=freightwidget
		# https://www.komplett.no/product/1156941/gaming/gaming-utstyr/gaming-headset/hyperx-cloud-stinger-core-traadloes-71-gaming-headset?q=Hyper+X+Cloud+Stinger+Core+7.1

		# issue
			# 0 price should be -1

		# solution
			# update et
			# rerun

	(R D-1) 1850	2021-12-09	New CMS	X00	webshot_url	amazon.com.mx	
		# all item_ids	
		# no webshots

		# no issues, webshot has values

	(R D-1) 1853	2021-12-09	New CMS	U00	all	shoptime.com.br	
		# U00BRF90OL000001302900202101200146
		# U00BRF90OL000001302750202101200146	
		# invalid deadlinks

		# goods
			# U00BRF90OL000001302900202101200146
			# https://www.shoptime.com.br/produto/1459350936/nintendo-joy-con-l-r-verde-e-rosa-switch

		# issues
			# U00BRF90OL000001302750202101200146
			# https://www.shoptime.com.br/produto/1497217431/nintendo-switch-32gb-neon-gray-aceita-desbloqueio
			# no data scraped

			# price
			# availability
			# ratings
			# reviews

	Stats:
		Rerun: 10
		Fix: 1[1]

12 10 21
	(R D-3) 993	2021-12-09	Old CMS	Q00	all	www.bergzeit.de	
		Gore-Tex Schuhe herren
			585030ac-eb7a-4fb5-a8fc-9ed7b970ebd6@bd8b190f-904c-4b92-aeb9-b1657b260972

			Meindl Herren Ontario GTX Schuhe
				https://prnt.sc/22jlb3k
			Scarpa Herren Rush GTX Schuhe
				https://prnt.sc/22jle7h


		leichte Jacken herren
			0afe5e68-6062-4cef-8b1b-b9fb810bebd6@bd8b190f-904c-4b92-aeb9-b1657b260972

			adidas Terrex Herren Light Down Hoodie Jacke
				https://prnt.sc/22jgsb4

			Dynafit Herren Speed Polartec Jacke
				https://prnt.sc/22jguwg

			Montura Herren Save Duvet Jacket black
				https://prnt.sc/22jgxg7

			Mountain Hardwear Herren Mt Eyak/2 Jacke
				https://prnt.sc/22jha8p


			Patagonia Herren Isthmus Anorak
				https://prnt.sc/22jh1ct

			Patagonia Herren Micro Puff Jacke
				https://prnt.sc/22jh3yi

			Salewa Herren Aqua 3.0 PTX Jacke
				https://prnt.sc/22jh89b

		Regenhosen männer	
			bacd12f8-a731-4d49-9184-f29f60438b9a@bd8b190f-904c-4b92-aeb9-b1657b260972

			Karpos Herren Storm Sz Hose
			La Sportiva Herren Crizzle Hose
			Peak Performance Herren Vertical 3l Hose
			Rab Herren Kinetic Alpine 2.0 Hose
			Vaude Herren Monviso 3l Hose

		# duplicate product_website	

		# https://prnt.sc/22j77d3
		# https://prnt.sc/22j8rei
		# https://prnt.sc/22j9ctv

		# unsolved
			# this keyword 'Regenhosen männer' no duplicates on site, need further investigation on local

		# solution
			# send screenshots for 
				# Gore-Tex Schuhe herren
				# leichte Jacken herren

	(R D-1) 994	2021-12-10	Old CMS	Q00	all	www.campz.de	
		Trekkingschuhe herren	
		Auto Copy Over for 1 Day	
		https://prnt.sc/22jaezk

		# solution
			# rerun

	(R D-1) 995	2021-12-10	Old CMS	Q00	all	www.navasport.no	
		Regnjakke kvinner	
		Auto Copy Over for 1 Day	
		https://prnt.sc/22jcdpu

		# solution
			# rerun

	(R D-1) 999	2021-12-10	Old CMS	Q00	all	www.wiggle.co.uk	
		Waterproof trousers men	
		Auto Copy Over for 1 Day	
		https://prnt.sc/22jgxk1

		# solution
			# rerun

	(R D-1) 1000	2021-12-10	Old CMS	Q00	all	www.scandinavianoutdoor.fi	
		Miesten Untuvatakit	
		Auto Copy Over for 1 Day	
		https://prnt.sc/22jgyji

		# solution
			# rerun

	(R D-1) 1001	2021-12-10	Old CMS	Q00	all	www.xxl.se	
		Skaljackor dam	
		Auto Copy Over for 1 Day	
		https://prnt.sc/22jiet9

		# solution
			# rerun

	(R D-1) 1002	2021-12-10	Old CMS	Q00	all	www.xxl.fi	
		Miesten Kevyttakki	
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/22jid02

		# solution
			# rerun

	(R D-1) 1003	2021-12-10	Old CMS	Q00	all	www.addnature.com	
		Mockasiner herr	
		Data Count Mismatch 1(Fetched Data is Greater than expected)	
		https://prnt.sc/22jjno4

		# solution
			# rerun

	(R D-1) 1004	2021-12-10	New CMS	200	all	www.mediamarkt.de	
		voice lautsprecher
		Duplicate product_website & product_url	

		CONCORD Glen Campbell, Jimmy Webb - In Session  - (CD + DVD Video)
			https://prnt.sc/22kmkur
		ORIGIN Anthony Branker & Word Play - The Forward (Towards Equality) Suite  - (CD)
			https://prnt.sc/22kmopr

		https://prnt.sc/22jfwjx

		# solution
			# run in local

	(R D-2) 1005	2021-12-10	New CMS	200	all	www.elkjop.no	
		bluetooth ørepropper
			134fcd48-a112-42c1-a395-caaea7e87e91@EGO2juO8EeaOEAANOrFolw

			JVC Øreplugger EBT5 Bluetooth In-Ear Blå
			JVC Øreplugger EBT5 Bluetooth In-Ear Rød
			JVC Øreplugger EBT5 Bluetooth In-Ear Rosa
			JVC Øreplugger EBT5 Bluetooth In-Ear Svart

		google høyttaler
			23b7a17a-a045-4982-ab87-0656aeafd85f@EGO2juO8EeaOEAANOrFolw

		ørepropper musikk
			4850daa2-066c-4d62-8860-b541ebdcd3fa@EGO2juO8EeaOEAANOrFolw

		trådløse hodetelefoner trening
			212293db-b2b3-4fbb-8196-3883c42a4e24@EGO2juO8EeaOEAANOrFolw

			JBL Endurance PEAK 2 helt trådløse hodetelefoner (hvit)
			Thomson WEAR8500BT helt trådløse hodetelefoner (grå/oransje)

		trådløse øreplugger	
			2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw

		Duplicate product_website & product_url	

		https://prnt.sc/22jf2vf

		# solution
			# check site
				# if no duplicates, do multiple reruns until fixed
				# if not, just mark the message angry and put 6 reruns attempts
			# multiple reruns

	(R D-2) 1006	2021-12-10	New CMS	200	all	www.elgiganten.se	
		bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw

			Sony SBH24 Stereo Bluetooth Headset, Vit
			https://prnt.sc/22kdx79
		
		portabel högtalare
			6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw

			Braven BVR-Mini portabel högtalare (svart)
				https://prnt.sc/22khrj3
			Sudio Femtio trådlös portabel högtalare (silver)
				https://prnt.sc/22khwss
			Sudio Femtio trådlös portabel högtalare (svart)
				https://prnt.sc/22khzky

		Duplicate product_website & product_url	
		https://prnt.sc/22jgg01

		# solution
			# check sql rerun once, if persists,
				# get duplicate keywords
			# check site, check every keywords for duplicate
				# provide screenshot for each duplicate keywords
			# send screenshot

	(R D-1) 1007	2021-12-10	New CMS	200	all	www.netonnet.se	
		bluetooth högtalare

		Data Count Mismatch 15 (Fetched Data is Lesser than expected)	
		https://prnt.sc/22jhldo

		# solution
			# rerun
	
	(R D-1) 1008	2021-12-10	New CMS	200	all	www.dns-shop.ru	
		беспроводные наушники	
		Invalid Deadlink

		https://prnt.sc/22jk5hh

		# issues
			# ppt
			# CRAWL_FAILED 

		# solution
			# multiple rerun

	(R D-1) 1009	2021-12-10	New CMS	200	all	www.dns-shop.ru	
		наушники накладные беспроводные с bluetooth
		проводные наушники

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)

		https://prnt.sc/22jkou7
		https://prnt.sc/22jkt3y

		# solution 
			# rerun

	(R D-1) 1010	2021-12-10	New CMS	200	all	www.electronicexpress.com	
		multiroom speaker
			a63d7d81-1273-4273-a069-c75b26bf3e12@7a4aac63-7c10-4b23-8e1b-efc898766c5e
		earbuds with mic	
			75a48477-ec80-4917-9b22-a16590fa3130@7a4aac63-7c10-4b23-8e1b-efc898766c5e

		Data Count Mismatch 20 (Fetched Data is Lesser than expected)
		Data Count Mismatch 13 (Fetched Data is Lesser than expected)
	
		https://prnt.sc/22jyij8
		https://prnt.sc/22jysef

		# solution
			# rerun

	=======

	(R D-1) 1858	2021-12-10	New CMS	200	all	heureka.cz
		'200CZ9D070000000804260201907080347',
		'200CZ9D070000000550290201904050558',
		'200CZ9D070000000551040201904050624',
		'200CZ9D070000000804340201907080409'

		Invalid Deadlinks

		# unsolved
			200CZ9D070000000804340201907080409

		# solution
			# saved by sir Jave
				# 200CZ9D070000000804340201907080409 valid deadlink since theres no 3p data: https://prnt.sc/22js5bx

	(R D-1) 1859	2021-12-10	Old CMS	Q00	all	navasport.no	
		'Q00NOHW0IJ000001568160202108300607',
		'Q00NOHW0IJ000001579190202109080312',
		'Q00NOHW0IJ000001583350202109130622',
		'Q00NOHW0IJ000001595510202109220248',
		'Q00NOHW0IJ000001596090202109220248',
		'Q00NOHW0IJ000001596110202109220248',
		'Q00NOHW0IJ000001611780202110040508',
		'Q00NOHW0IJ000001654650202111091222',
		'Q00NOHW0IJ000001674640202112060317'

		Invalid deadlinks	

		https://prnt.sc/22jbhl7

		# solution
			# rerun

	(R D-1) 1864	2021-12-10	New CMS	U00	price_value	casasbahia.com.br	
		U00BR890OL000001302510202101200146	
		wrong price scraped

		# solution
			# rerun after 3pm  rebuild
			# rerun, then check site and sql

	=======
		
	(R D-1) 354	2021-12-10	New CMS	200	all	www.re-store.ru	
		Headphones Wired In Ear	
		Data Count Mismatch (Fetched Data is Lesser than expected)	
		https://prnt.sc/22jgssm

		# solution
			# no need since sql and site are parallel, maybe time difference

	(R D-1) 355	2021-12-10	New CMS	Z00	all	www.bestbuy.com	
		Home theater - Soundbars	

		(Data Count Mismatch 1(Fetched Data is Lesser than expected)	
		
		# solution
			# rerun

	Stats:
		Rerun: 20(24) == 1.2

12 11 21
	(R D-1) 1888	2021-12-11	New CMS	100	all	hjertmans.se	
		'100SEHH010000001224090202010280928','100SEHH010000001401920202105040817','100SEHH010000000822120201908060221','100SEHH010000000932710202004220308'

		Etl_failure instock	

		# solution
			# update compa et, add identifier for oos (out of stock)
			# rerun

	(R D-1)1889	2021-12-11	New CMS	100	all	hylte-lantman.com	
		100SEDH010000000821270201908050821
		100SEDH010000001322730202102051007
		100SEDH010000001224210202010280943
		100SEDH010000001321330202102050343
		100SEDH010000001321380202102050343
		100SEDH010000001666720202111170531
		100SEDH010000001403100202105050324
		100SEDH010000001224330202010280943	

		incorrect scraping of title, affecting other essential data such as Availabiliy	
		https://prnt.sc/22ok53p

		# solution
			# rerun by sir jave

	=======

	(R D-1) 1014	2021-12-11	New CMS	200	all	www.re-store.ru	
		наушники с проводом
			18
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		портативная акустика Wi-Fi
			3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		портативная колонка wi-fi
			3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 1(Fetched Data is Less than expected)
		Data Count Mismatch 1(Fetched Data is Greater than expected)
		Data Count Mismatch 2(Fetched Data is Less than expected)

		https://prnt.sc/22o7ldm
		https://prnt.sc/22o7s9b
		https://prnt.sc/22o80b6	

		Phil

		# solution
			# rerun

	0 (R D-1) 1015	2021-12-11	New CMS	200	all	www.fnac.com	
		casque audio reducteur de bruit
			239b21d0-d2b3-4189-929d-ab4e4d2e3967@LzP6XApHEem-gAANOiOHiA
		enceinte amazon	
			6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA
		
		Data Count Mismatch 6(Fetched Data is Less than expected)
		Data Count Mismatch 19(Fetched Data is Less than expected)

		https://prnt.sc/22oas7x
		https://prnt.sc/22oad3u	

		Phil

		python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-11@22:00:00@200@6b3b1268-5204-41fc-8a2e-d562136f3623@LzP6XApHEem-gAANOiOHiA,2021-12-11@22:00:00@200@239b21d0-d2b3-4189-929d-ab4e4d2e3967@LzP6XApHEem-gAANOiOHiA

		# solution
			# after 4 reruns attempts, data cound still unchanged
			# check local res.text maybe its wrong

			# for trello
				# takes more time to fix

	(R D-1)1016	2021-12-11	New CMS	200	all	www.mediamarkt.de	
		/ bluetooth lautsprecher
			a7f7ff19-c7ff-47ae-99f8-61864de8ed91@M3YPUApIEem-gAANOiOHiA
		tragbare lautsprecher
			1920ce34-ce4e-4f00-a793-b23312c238f8@M3YPUApIEem-gAANOiOHiA
			MARSHALL Emberton BT Bluetooth Lautsprecher, Schwarz, Wasserfest
			SONY SRS-XB13 Bluetooth Lautsprecher, Schwarz, Wasserfest

		Duplicate product_website & product_url	

		https://prnt.sc/22ocbru
		https://prnt.sc/22ocsuo

		Phil

		# solution
			# multiple reruns

	(R D-2) 1017	2021-12-11	New CMS	200	all	www.elkjop.no	
		/ google høyttaler
			23b7a17a-a045-4982-ab87-0656aeafd85f@EGO2juO8EeaOEAANOrFolw
			Belkin SoundForm Elite Hi-Fi smart høyttaler (sort)
		in ear hodetelefoner
			bbf40a90-31dc-48b5-aee1-7ac98887ac46@EGO2juO8EeaOEAANOrFolw
			Sudio Tio trådløse in-ear hodetelefoner (grønn)
			https://prnt.sc/22ozkk3
		/ ørepropper trening
			10e3c133-5f5e-463b-8b61-234212b35d87@EGO2juO8EeaOEAANOrFolw
			Bose Sport Earbuds helt trådløse ørepropper (baltic blue)
			Bose Sport Earbuds helt trådløse ørepropper (triple black)
		støydempende hodetelefoner
			fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw
			JBL LIVE Pro+ helt trådløse in-ear hodetelefoner (hvit)
			https://prnt.sc/22ozdp5
		/ trådløs høyttaler wifi
			77786a55-9ea9-4477-bdd4-8d085dfa4c5a@EGO2juO8EeaOEAANOrFolw
			Yamaha MusicCast 50 trådløs høyttaler (sort)

		Duplicate product_website & product_url	

		https://prnt.sc/22odwaa
		https://prnt.sc/22oe5jw
		https://prnt.sc/22oe7g5
		https://prnt.sc/22oecdx
		https://prnt.sc/22oeoal
		https://prnt.sc/22of71v

		Phil

		# solution
			# multiple reruns
			# if reruns ineffective, check site for valid duplicates

	(R D-1) 1018	2021-12-11	New CMS	200	all	www.elgiganten.se	
		bluetooth hörlurar
			bI6h1lKhEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw
		hörlurar in ear
			75c80c5e-2387-4b0a-b566-17b30a3135b1@fVQR1tMrEeaOEAANOrFolw
		portabel högtalare
			6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		trådlös högtalare
			6f1UrFKcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		trådlösa hörlurar träning
			5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw	

		Duplicate product_website & product_url	

		https://prnt.sc/22og6r9
		https://prnt.sc/22ogmdb
		https://prnt.sc/22ohg9y
		https://prnt.sc/22oid24
		https://prnt.sc/22oijmt
		https://prnt.sc/22oiuwk

		Phil

		# solution
			# rerun

	(R D-1) 1019	2021-12-11	New CMS	200	all	www.bol.com	
		enceinte sans fil
			2f0553c1-fd39-4b36-a989-f4b46cb5be7a@UAjszpOqEei-eQANOiOHiA
			Sans Auteur Mere Et Fille, Ou La Premiere Deception

		Duplicate product_website & product_url	

		https://prnt.sc/22oj90z		
		Phil

		# solution
			# rerun

	(R D-1) 1023	2021-12-11	New CMS	M00	all	www.vetzoo.se	
		Cocker	
		Auto copy over for 1 day	
		https://prnt.sc/22omfxa		
		Michael

		# solution
			# rerun
	
	(R D-1) 1024	2021-12-11	New CMS	M00	all	www.petenkoiratarvike.com	
		Cavalier Kingcharlesinspanieli	
		Auto copy over for 1 day

		https://prnt.sc/22onlc2

		# solution
			# rerun

	(R D-2) 1026	2021-12-11	New CMS	U00	all	www.magazineluiza.com.br	
		'Nintendo Switch',
		'Pro Controller',
		'Joy Controle',
		'Console Nintendo Switch',
		'Controle Nintendo Switch',
		'Console',
		'Controle sem fio Nintendo Switch',
		'Controle Joy con',
		'Console Switch',
		'Controle Switch',
		'Mini Video Game',
		'Aparelho de Game',
		'Aparelho de Video Game',
		'Mini game portatil',
		'controle game sem fio',

		Invalid Deadlink

		https://prnt.sc/22oqg4z	

		kurt

		# Issues
			# unsolved keywords (still deadlink with 6 rerun attempts):
				# aparelho de video game
				# pro controller
				# mini video game
				# aparelho de game
				# console switch
				# controle joy con
				# nintendo switch
				# controle switch
				# controle nintendo switch
				# console nintendo switch
				# controle sem fio nintendo switch
				# console
				# mini game portatil

			# for retest in trello 
				# since: it was copy override leading to not work in rerun

	(R D-1) 1027	2021-12-11	New CMS	U00	all	www.shoptime.com.br	
		nintendo switch
			71b71767-6a5d-4a57-aa86-b6faa8d6bd9d@1QomMkasEeeMawANOiZi-g
		console nintendo switch
			003f6ead-42f1-4ecb-9b72-e7a79a4b7ead@1QomMkasEeeMawANOiZi-g
		aparelho de game
			493085f2-179f-4689-b55f-8a2cb7d036a4@1QomMkasEeeMawANOiZi-g
		mini video game
			94b59958-8189-4a66-a160-a1e724e3ad52@1QomMkasEeeMawANOiZi-g
		console switch
			013044db-0c1d-44de-8e6e-bbf725de15fd@1QomMkasEeeMawANOiZi-g
		controle game sem fio
			210ffe59-5594-407f-86df-5fb3a0fe2d38@1QomMkasEeeMawANOiZi-g

		Auto Copy Over for 2 days
		Auto Copy Over for 1 day
		Auto Copy Over for 3 days

		https://prnt.sc/22or89p
		https://prnt.sc/22ora6n
		https://prnt.sc/22orc4u

		https://prnt.sc/22oregl

		https://prnt.sc/22orj0d
		https://prnt.sc/22orjmt

		# solution
			# multiple reruns

	(R D-1) 1028	2021-12-11	New CMS	U00	all	www.nagem.com.br	
		pro controller
			afd31eea-4072-46c6-85c5-33921ae1871a@1a589c20-8c9d-4451-a65a-593c3640dba0
		console switch
			013044db-0c1d-44de-8e6e-bbf725de15fd@1a589c20-8c9d-4451-a65a-593c3640dba0

		Auto copy over for 1 day

		https://prnt.sc/22or00e
		https://prnt.sc/22or2wu

		kurt

		# solution
			# rerun

	(R D-2) 1030	2021-12-11	New CMS	200	all	www.electronicexpress.com	
		/ amazon speaker
			23
			b369e967-64f2-44f7-afaa-f9cbfe9f215b@7a4aac63-7c10-4b23-8e1b-efc898766c5e
		/ earbuds with mic
			25
			75a48477-ec80-4917-9b22-a16590fa3130@7a4aac63-7c10-4b23-8e1b-efc898766c5e
		/ Multiroom speaker
			25
			a63d7d81-1273-4273-a069-c75b26bf3e12@7a4aac63-7c10-4b23-8e1b-efc898766c5e
		/ wifi speaker
			2
			e447257e-9396-4735-9af7-68d864a12052@7a4aac63-7c10-4b23-8e1b-efc898766c5e
		/ wifi speakers
			2
			17510dfe-6842-468b-b2e7-081748369ed0@7a4aac63-7c10-4b23-8e1b-efc898766c5e

		Data Count Mismatch 2(Fetched Data is Greater than expected)
		Data Count Mismatch 13(Fetched Data is less than expected)
		Data Count Mismatch 9(Fetched Data is less than expected)
		Data Count Mismatch 23(Fetched Data is Greater than expected)

		https://prnt.sc/22pcy4j
		https://prnt.sc/22pbx83
		https://prnt.sc/22pcads
		https://prnt.sc/22peosw
		https://prnt.sc/22peuzy

		Phil

	(R D-1) 1031	2021-12-11	New CMS	200	all	www.electronicexpress.com	
		/ bluetooth headphones
			77334dbc-cbf7-449c-948e-f69cc9a2a25c@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pdeiq

		/ bluetooth speakers
			18KcljsFEeeFswANOiOHiA@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pdlpn

		/ in ear headphones
			c7ba4833-f04a-43a2-8874-712a0fae1c69@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pdv5p

		/ noise cancelling headphones
			f9682f2c-ddc5-4a8d-ab33-90f805cec945@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pe1tl

		/ portable bluetooth speakers
			5489b3c3-6386-4225-822b-cb7704c142fd@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pec7n

		/ portable speaker
			42e6294a-567d-40b0-b773-06d6e745ab46@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pel1y

		/ smart speaker
			30f9dc2a-2a90-4381-8bc4-37deb248b8f4@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pel1y

		wireless bluetooth earbuds
			58117609-86fd-409a-97fb-fe757a6b0a47@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			https://prnt.sc/22pezbt

		wrong scraped of data

		Phil

		# solution
			# rerun since it is ppt

	Stats:
		Rerun: 15(18) == 1.2

12 12 21
	(R D-1) 1923	2021-12-12	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659250202111162228',
			https://www.elgiganten.dk/product/wearables-sport-og-fitness/sportsur-og-pulsur/48597/garmin-fenix-6x-pro-gps-sportsur-51-mm-sort
		'100DK10010000001248290202011250630'
			https://www.elgiganten.dk/product/sport-fritid-hobby/bil-motorcykel/gps/garmin-campervan-gps-til-mindre-varevogne-sort/330245

		spider issue -1 values

		# solution
			# rerun

	(R D-1) 1928	2021-12-12	New CMS	K00	all	mvideo.ru	
		K00RUP80CG000001310860202101250600 
		spider issue -1 values		

		Yes	Mayeieie

		# solution
			# rerun

	(R D-1) 1929	2021-12-12	New CMS	K00	all	alza.cz	
		K00CZ360CG000001485940202107130335	
		spider issue -1 values		
		Yes	Mayeieie

		# solution
			# rerun

	(R D-1)1932	2021-12-12	New CMS	K00	all	power.dk	
		'K00DKX20CG000001487130202107130336',
		'K00DKX20CG000001489870202107130337',
		'K00DKX20CG000001487640202107130336'

		spider issue -1 values

		# solution
			# rerun

	(R D-1) 1933	2021-12-12	New CMS	K00	webshot_url		
		SELECT * FROM view_all_productdata where date='2021-12-12' and webshot_url is NULL	no webshot

		# solution
			# rerun webshots

	(R D-1) 1944	2021-12-12	New CMS	X00	price_value	amazon.com.mx	
		'X00MXVC0OL000001339180202103010523',
		'X00MXVC0OL000001339160202103010523',
		'X00MXVC0OL000001339060202103010159',
		'X00MXVC0OL000001339160202103010523',
		'X00MXVC0OL000001339060202103010159',
		'X00MXVC0SV020211207055750589684341',
		'X00MXVC0SV020211207055751333683341',
		'X00MXVC0OL000001421490202105190424',
		'X00MXVC0OL000001421410202105190424',
		'X00MXVC0OL000001421330202105190424',
		'X00MXVC0OL000001421260202105190424',
		'X00MXVC0OL000001421190202105190424',
		'X00MXVC0PL000001461160202106300705',
		'X00MXVC0OL000001421800202105190424',
		'X00MXVC0OL000001421720202105190424',
		'X00MXVC0OL000001421640202105190424',
		'X00MXVC0OL000001421560202105190424',
		'X00MXVC0SV020211207055752903258341'

		wrong price scraped		

		Yes	Turki

		# solution
			# fixed by sir Ryan
			# for rerun

	=======

	(R D-1)1032	2021-12-12	New CMS	200	all	www.dns-shop.ru	
		портативная колонка
			6889e93b-60e8-4b27-a39e-690c8afbe53d@BzxplkXeEeeMawANOiZi-g

		invalid deadlink	

		https://prnt.sc/22rdqnx		

		Phil

		# solution
			# rerun

	(R D-1) 1033	2021-12-12	New CMS	200	all	www.boulanger.com	
		mini-enceinte	

		Data Count Mismatch 15 (Fetched Data is Lesser than expected)	

		https://prnt.sc/22rdtzz		

		Phil

		# solution
			# https://prnt.sc/22rfpfv
			# no changes

	(R D-1) 1034	2021-12-12	New CMS	200	all	www.mediamarkt.de	
		voice lautsprecher	
			c63413b9-7d0f-4909-a4e4-aa051eaa908c@M3YPUApIEem-gAANOiOHiA
			EUROARTS John Eliot Gardiner, Monteverdi Choir, The English Baroque Soloists - Handel: Semele  - (Blu-ray)
			EUROARTS John Eliot Gardiner, Monteverdi Choir, The English Baroque Soloists - Handel: Semele  - (DVD)

		Duplicate product_website & product_url		

		https://prnt.sc/22re6a4
		https://prnt.sc/22re6jy

		Phil

		# solution
			# rerun

	(R D-1) 1037	2021-12-12	New CMS	200	all	www.dns-shop.ru	
		наушники с проводом		
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@BzxplkXeEeeMawANOiZi-g
			Кабель соединительный Aceline 3.5 mm jack - 3.5 mm jack [вилка - вилка, 2 м]

		Duplicate product_website & product_url	

		https://prnt.sc/22rfwth		

		Phil

		# solution
			# rerun

	(R D-2) 1039	2021-12-12	New CMS	200	all	www.electronicexpress.com	
		/ Multiroom speaker
			a63d7d81-1273-4273-a069-c75b26bf3e12@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			25
		/ alexa speaker
			aad973cd-a877-425a-b584-c404d66ee80c@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			16
		/ amazon speaker
			b369e967-64f2-44f7-afaa-f9cbfe9f215b@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			23	
		/ wifi speaker
			e447257e-9396-4735-9af7-68d864a12052@7a4aac63-7c10-4b23-8e1b-efc898766c5e
			2
		
		Data Count Mismatch 20 (Fetched Data is Lesser than expected)
		Data Count Mismatch 9 (Fetched Data is Greater than expected)
		Data Count Mismatch 2 (Fetched Data is Greater than expected)
		Data Count Mismatch 23 (Fetched Data is Greater than expected)

		https://prnt.sc/22rh17i
		https://prnt.sc/22rh1p1
		https://prnt.sc/22rh2i6
		https://prnt.sc/22rh9ud

		Phil

	(R D-1) 1040	2021-12-12	New CMS	200	all	www.electronicexpress.com	
		'bluetooth earbuds',
		'bluetooth headphones',
		'bluetooth speakers',
		'earbuds',
		'earbuds with mic',
		'google speaker',
		'google speaker',
		'noise cancelling headphones',
		'portable bluetooth speaker',
		'portable bluetooth speakers',
		'portable speaker',
		'portable speakers',
		'smart speaker',
		'wireless bluetooth earbuds',
		'wireless bluetooth speakers',
		'wireless earbuds',
		'wireless headphones',
		'wireless speaker',
		'wireless speakers'

		wrong scraped of data

		https://prnt.sc/22rh32i
		https://prnt.sc/22rh3p0
		https://prnt.sc/22rh49w
		https://prnt.sc/22rh4g0
		https://prnt.sc/22rh58v
		https://prnt.sc/22rh5r0
		https://prnt.sc/22rh6j9
		https://prnt.sc/22rh6r5
		https://prnt.sc/22rh72a
		https://prnt.sc/22rh7ym
		https://prnt.sc/22rh8f7
		https://prnt.sc/22rh8vq
		https://prnt.sc/22rhahq
		https://prnt.sc/22rhasg
		https://prnt.sc/22rhbfh
		https://prnt.sc/22rhbn0
		https://prnt.sc/22rhbwx
		https://prnt.sc/22rhc6qPhil

		waterproof bluetooth speaker
		wireless bluetooth speakers

		# rerun
			# multiple reruns

	=======

	(R D-1) 360	2021-12-12	New CMS	200	all	www.mediamarkt.de	
		Headphones Bluetooth On Ear
			200@810@1e0b3553-fb55-43d8-ab4e-92ea6fd1ab5e
		Speakers Multiroom
			200@810@47d8fb61-0378-4e47-bc98-16ea3ebec924

		Data Count Mismatch 13(Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)

		https://prnt.sc/22rf1i7
		https://prnt.sc/22rf2g9

		Yes	MICHAEL

		# solution
			# rerun

	(R D-1)361	2021-12-12	New CMS	200	all	www.re-store.ru	
		Speakers Voice	
		Data Count Mismatch 1 (Fetched Data is lesser than expected)	
		https://prnt.sc/22rf9tm	

		Yes	MICHAEL

		# solution
			# rerun

	---

	Stats:
		Rerun: 14 (15) == 1.07

12 15 21

	===

	(R D-1) 1093	2021-12-15	New CMS	K00	all	www.alza.cz	
		mysi Gaming	
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/23187j5		
		kurt

		# solution
			# rerun

	(R D-1)1094	2021-12-15	New CMS	K00	all	www.alza.cz	
		Mechanické klávesnice
			411ae2c5-3fe2-45ed-8725-1fcf9a82d27d@1cAhckXZEeeMawANOiZi-g

		kancelářské klávesnice
			b17bfdf5-fbf8-42cd-806a-5b345230aa4d@1cAhckXZEeeMawANOiZi-g

		Auto Copy Over for 1 day	

		https://prnt.sc/2318axj
		https://prnt.sc/2318dd8	

		kurt

		# solution
			# rerun

	(R D-1) 1095	2021-12-15	New CMS	K00	all	www.fnac.com	
		Chaise de jeu	
		Auto copy over  1day	
		https://prnt.sc/2319n8i		
		michael

		# no issue

	(R D-1) 1096	2021-12-15	New CMS	K00	all	www.fnac.com	
		Gaming Keypad	
			609e9763-24a8-4516-a81e-9a968207685c@LzP6XApHEem-gAANOiOHiA
			https://prnt.sc/231gj74

		Data Count Mismatch 22 (Fetched Data is lesser than expected)	
		https://prnt.sc/231a1ns		
		michael

		# solution
			# sponsored?

	(R D-1) 1097	2021-12-15	New CMS	K00	all	otto.de	
		HEADSET
			9ce256ab-4183-4ec9-b02e-35f70b3310c9@xhsDgt_tEee8oQANOijygQ
			Logitech G »PRO X« Gaming-Headset
			https://prnt.sc/231kpyl
		Stuhl
			3b951495-023f-4ed6-b525-c5847d86223c@xhsDgt_tEee8oQANOijygQ	
			RAZER Gaming-Stuhl »Iskur«
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@3b951495-023f-4ed6-b525-c5847d86223c@xhsDgt_tEee8oQANOijygQ

		duplicate product_website	
		https://prnt.sc/2319tqh
		https://prnt.sc/231aw0u
		keen

		# solution
			# investigation
			# screenshot
			# rerun

	(R D-1) 1098	2021-12-15	New CMS	K00	all	otto.de
		Gaming Keypad	
			609e9763-24a8-4516-a81e-9a968207685c@xhsDgt_tEee8oQANOijygQ
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@609e9763-24a8-4516-a81e-9a968207685c@xhsDgt_tEee8oQANOijygQ
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	
		https://prnt.sc/231bpfn		
		keen

		# solution
			# rerun

	(R D-1) 1099	2021-12-15	New CMS	K00	all	www.netonnet.se	
		bästa hörlurarna	
			79b74178-cf4b-47e5-a05d-0d2b35b6a975@HxbbnONtEeaOEAANOrFolw
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@79b74178-cf4b-47e5-a05d-0d2b35b6a975@HxbbnONtEeaOEAANOrFolw
		Data Count Mismatch 15(Fetched Data is Lesser than expected)	
		https://prnt.sc/231azsf	
		Yes	
		Rayyan

		# solution
			# rerun

	(R D-2) 1100	2021-12-15	New CMS	K00	all	elkjop.no	
		/ Ergonomisk stol
			ba0671d1-1fb7-48e7-8566-2b61bcb509c9@EGO2juO8EeaOEAANOrFolw

			Arozzi Inizio gamingstol (blå)
			Arozzi Inizio gamingstol (rød)
			NOS F-250 Junior gamingstol

		/ GAMING HEADSET
			f1fb4333-5849-4599-a566-c8e3f8e06699@EGO2juO8EeaOEAANOrFolw
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@EGO2juO8EeaOEAANOrFolw
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@EGO2juO8EeaOEAANOrFolw

			JBL Quantum 100 gaming headset (blå)
			JBL Quantum 100 gaming headset (hvit)
			JBL Quantum 100 gaming headset (sort)
			Thronmax USB gaming headset

			https://prnt.sc/232h39g

		duplicate product_website	

		https://prnt.sc/231dzjf
		https://prnt.sc/231d9oj

		keen

		# solution
			# GAMING HEADSET
				# no issues in local
				# valid duplicate, on site
				# screenshot

	(R D-1) 1101	2021-12-15	New CMS	K00	all	www.alza.cz
		kancelářské klávesnice	
			b17bfdf5-fbf8-42cd-806a-5b345230aa4d@1cAhckXZEeeMawANOiZi-g
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@22:00:00@K00@b17bfdf5-fbf8-42cd-806a-5b345230aa4d@1cAhckXZEeeMawANOiZi-g
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/231fhjj		
		kurt

		# solution
			# rerun

	(R D-1) 1102	2021-12-15	New CMS	K00	all	www.altex.ro	
		Mousepad gaming	
			77b384f7-71c8-49e0-ae78-28d4be96eada@aa65086a-c5d1-4571-8371-9aa2b6e290df
			python rerun_rnk.py -s CRAWL_FINISHED -l 2021-12-15@21:00:00@K00@77b384f7-71c8-49e0-ae78-28d4be96eada@aa65086a-c5d1-4571-8371-9aa2b6e290df
		Data Count Mismatch 2 (Fetched Data is Greater than expected)	
		https://prnt.sc/231epc7		
		ailyn

		# solution
			# rerun

	(R D-1) 1110	2021-12-15	New CMS	K00	all	elgiganten.se	
		'MEKANISKT TANGENTBORD',
		'PC-TANGENTBORD',
		'PC-MUS',
		'SPEL NOTEBOOK',
		'Gaming Speakers',
		'PS-kontroller',
		'Xbox One X-kontroller',
		'ANC-hörlurar'
		
		Data Count Mismatch 4 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 7 (Fetched Data is Lesser than expected)
		Data Count Mismatch 20 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 15 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 22 (Fetched Data is Lesser than expected)
		
		https://prnt.sc/231hqa6
		https://prnt.sc/231hj92
		https://prnt.sc/231hv2u
		https://prnt.sc/231i3k0
		https://prnt.sc/231ilkg
		https://prnt.sc/231j6p6
		https://prnt.sc/231j0ax
		https://prnt.sc/231jccj
		
		Mich

		# solution
			# multiple reruns

	(R D-1) 1111	2021-12-15	New CMS	K00	all	pccomponentes.com	
		'AURICULARES PARA JUEGOS',
		'RATÓN DE LA PC',
		'RATÓN PARA JUEGOS',
		'PORTÁTILES DE JUEGOS',
		'Controlador PS',
		'Auriculares ANC'

		Data Count Mismatch 21 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 24 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 23 (Fetched Data is Lesser than expected)
		 Data Count Mismatch24 (Fetched Data is Lesser than expected)
		 Data Count Mismatch 15 (Fetched Data is Lesser than expected)

		https://prnt.sc/231g1hw
		https://prnt.sc/231fwpx
		https://prnt.sc/231flq2
		https://prnt.sc/231g56h
		https://prnt.sc/231gaam
		https://prnt.sc/231gfwv
		
		Mich

		# solution
			# rerun

	(R D-1) 1115	2021-12-15	New CMS	200	all	www.mediamarkt.de	
		lautsprecher	
			32528cf1-d6f4-4ea8-b15c-017d43e90b1d@M3YPUApIEem-gAANOiOHiA
			python rerun_rnk.py -s CRAWL_FAILED -l 2021-12-15@22:00:00@200@32528cf1-d6f4-4ea8-b15c-017d43e90b1d@M3YPUApIEem-gAANOiOHiA
		Invalid Deadlink
		https://prnt.sc/231f3gk		
		Christopher

		# solution
			# rerun

	(R D-1) 1116	2021-12-15	New CMS	200	all	www.saturn.de	
		voice lautsprecher	

		Duplicate product_website & product_url	
		https://prnt.sc/231fknh		
		Christopher

		# solution
			# investigate

	(R D-1) 1117	2021-12-15	New CMS	200	all	www.elkjop.no	
		bluetooth hodetelefoner
			cTj0T04NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw
		smarthøyttaler
			86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw

		Duplicate product_website & product_url	
		https://prnt.sc/231i91j		
		Christopher

		# solution
			# rerun

	===

	(R D-1) 379	2021-12-15	New CMS	K00	all	www.noon.com/saudi-en/	
		Gaming Laptops	
			5405f64a-3048-4f38-95c5-70cc9491ddfc
		1 Day Auto Copy Over	
		https://prnt.sc/231dm4t	Yes	Jairus

		# solution
			# rerun

	Stats: 
		Rerun: 16 (17) == 1.06


12 22 21
	/ prod2 in dbeaver
		/ O00
		/ 200

	* runner tool version 2, update
	* fix scenarios 12-15-21 update
		* upload to cloud from pc

	(R D-1) 1212	2021-12-22	New CMS	K00	all	ebuyer.com	
		Gaming Keypad - 21
			609e9763-24a8-4516-a81e-9a968207685c@qcWnsMefEei-eQANOiOHiA 

		Mechanical Keypad - 20
			af0f30f1-87f0-47eb-8c2f-8bcf77523493@qcWnsMefEei-eQANOiOHiA

		Invalid Deadlink

		https://prnt.sc/23y0udg
		https://prnt.sc/23y0ogi

		renz

		# solution
			# multiple reruns - failed
			# rerun

	(R D-2) 1213	2021-12-22	New CMS	K00	all	ebuyer.com	
		GAMING NOTEBOOK - 20
			a5042d62-ba5e-4b40-ac02-fada0b36b92c@qcWnsMefEei-eQANOiOHiA
		PC Keypad - 20
			cedc7874-47f7-41e8-81c6-002faff89723@qcWnsMefEei-eQANOiOHiA
		Keypad - 24
			e6b610ec-314c-44b2-81b0-6934b661917c@qcWnsMefEei-eQANOiOHiA
		# Ergonomic Chair - 23
		# 	d6871bcf-528c-4a32-a19d-c504066e1013@qcWnsMefEei-eQANOiOHiA
		# Playstation Controller - 19
		# 	a63f3f94-b682-4da5-8375-a409d9fd7d97@qcWnsMefEei-eQANOiOHiA
		best headphones - 20
			d29b8e26-ffcc-4bda-a5ec-5681de160607@qcWnsMefEei-eQANOiOHiA

		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 15 (Fetched Data is Lesser than expected)
		Data Count Mismatch 18 (Fetched Data is Lesser than expected)
		Data Count Mismatch 13 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)

		https://prnt.sc/23xzzs3
		https://prnt.sc/23y07yd
		https://prnt.sc/23y0ejs
		https://prnt.sc/23y1g1h
		https://prnt.sc/23y1ui8
		https://prnt.sc/23y26nn

		renz

	(R D-1) 1214	2021-12-22	New CMS	K00	all	virginmegastore.ae	
		Gaming Headset
			f1fb4333-5849-4599-a566-c8e3f8e06699@7548df03-04bb-436a-b531-ebe3d5901cd9
			2021-12-22@19:00:00@K00@f1fb4333-5849-4599-a566-c8e3f8e06699@7548df03-04bb-436a-b531-ebe3d5901cd9

		Auto copy over for 1 day	

		https://prnt.sc/23xz00k		

		renz

		# solution
			# rerun

	(R D-1) 1215	2021-12-22	New CMS	K00	all	www.pcgarage.ro	
		Casti Pc
			fb31340c-fa75-4e97-a9ba-ade0ca9194a9@48ed7393-28da-4ce0-8095-ca1180bfe500	

		Auto Copy Over for 1 Day	

		https://prnt.sc/23y3rwo		
		Kristian Angelo

		# solution
			# rerun

	Stats:
		Rerun: 4 (5) == 1.25

12 26 21
	(R D-1) 425 2021-12-26	New CMS	100	all	www.verkkokauppa.com/fi
		AKTIIVISUUSKELLOT

		Invalid deadlink 
		https://prnt.sc/24kyzwv

		Yes	michael

		# solution
			# multiple reruns failed
			# goods in local
			# crawler failed

	-----

	(R D-1) 1243	2021-12-26	New CMS	200	all	www.mediamarkt.de	
		tragbare lautsprecher	
			1920ce34-ce4e-4f00-a793-b23312c238f8@M3YPUApIEem-gAANOiOHiA

		invalid deadlink	
		https://prnt.sc/24krgys		
		Phil

		# solution
			# Multiple Reruns

	(R D-1) 1244	2021-12-26	New CMS	200	all	www.dns-shop.ru
		проводные внутриканальные наушники
			6c37e71c-f038-438f-84a2-2ba31fcd3323@BzxplkXeEeeMawANOiZi-g

		invalid deadlink	
		https://prnt.sc/24kxzfd		
		Phil

		# solution
			# Multiple Reruns

	(R D-3) 1245	2021-12-26	New CMS	200	all	www.mediamarkt.de	
		/ bluetooth in ear kopfhörer
			d1c77a47-3811-49b8-9453-9b45f687e09b@M3YPUApIEem-gAANOiOHiA
		/ google lautsprecher
			dcb121a0-6342-4922-905c-4a470f7c3696@M3YPUApIEem-gAANOiOHiA
		/ in ear bluetooth
			8ce24b98-3414-4ae6-909f-a4aac32f5bed@M3YPUApIEem-gAANOiOHiA

			APPLE AirPods (3. Generation mit MagSafe Ladecase), In-ear Kopfhörer Bluetooth Weiß
			SAMSUNG SM-R190NZKAEUD GALAXY BUDS PRO , In-ear Kopfhörer Phantom Schwarz
			SAMSUNG SM-R190NZSAEUD GALAXY BUDS PRO , In-ear Kopfhörer Phantom Silver
		/ in ear kopfhörer
			a7a86579-0256-4cd7-a6d8-a27cdd475e9d@M3YPUApIEem-gAANOiOHiA

			APPLE AirPods (3. Generation mit MagSafe Ladecase), In-ear Kopfhörer Bluetooth Weiß
			SAMSUNG SM-R190NZKAEUD GALAXY BUDS PRO , In-ear Kopfhörer Phantom Schwarz
			SAMSUNG SM-R190NZSAEUD GALAXY BUDS PRO , In-ear Kopfhörer Phantom Silver

		/ kabellose kopfhörer
			986d3596-754d-4124-8368-d782c997b2d8@M3YPUApIEem-gAANOiOHiA
		/ Lautsprecher
			32528cf1-d6f4-4ea8-b15c-017d43e90b1d@M3YPUApIEem-gAANOiOHiA	

			N-GEAR The Flash 860 - Tragbar Bluetooth Lautsprecher, Schwarz
			TREVI 0XT10101 Standlautsprecher, weiß)

		duplicate product website & product URL	

		https://prnt.sc/24l3kx2
		https://prnt.sc/24l3p8v		
		Phil

		# solution
			# multiple reruns

	(R D-1) 1246	2021-12-26	New CMS	200	all	www.saturn.de	
		voice lautsprecher	
			c63413b9-7d0f-4909-a4e4-aa051eaa908c@UbbyaApIEem-gAANOiOHiA
		duplicate product website & product URL	
		https://prnt.sc/24l3bi2		
		Phil

		# solution 
			# rerun

	(R D-2) 1247	2021-12-26	New CMS	200	all	www.elgiganten.se	
		/ bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw
		hörlurar bluetooth
			9c1af0b2-63e7-4c5a-894c-1ec88db4d3f2@fVQR1tMrEeaOEAANOrFolw

			Bluetooth Hörlurar Denver Electronics TWE-37 300 mAh Vit
				https://prnt.sc/24lb0vg
			Bluetooth hörlurar M30 TWS Vit
				https://prnt.sc/24lb3ik

		duplicate product website & product URL	

		https://prnt.sc/24l1hp8
		https://prnt.sc/24l25gp
		https://prnt.sc/24l2teu

		Phil

		# solution
			# rerun
			# screenshot

	(R D-1) 1248	2021-12-26	New CMS	200	all	www.bol.com	
		enceinte connectée
			6c9dadaf-40e5-4322-a18a-3dc02112397e@UAjszpOqEei-eQANOiOHiA
		slimme speaker
			d0a10611-dc94-4af3-88a8-05cb322b3bd6@UAjszpOqEei-eQANOiOHiA
		woonkamer speaker
			46a7cc9c-621a-4c71-881e-b678948d48ab@UAjszpOqEei-eQANOiOHiA

		duplicate product website & product URL	

		https://prnt.sc/24l0kdr
		https://prnt.sc/24l10q9
		https://prnt.sc/24l1ev6

		Phil

		# solution
			# rerun

	Stats: 
		Rerun: 7 (10) == 1.42

12 29 21
	(R D-1) 1301	2021-12-29	New CMS	K00	all	www.pcgarage.ro	
		Casti Gaming
		Controller Gaming	

		1 Day Auto Copy Over	

		https://prnt.sc/25192z6
		https://prnt.sc/2518ymp	Yes	Joemike

		# solution 
			# rerun

	(R D-1) 1302	2021-12-29	New CMS	K00	all	euro.com.pl	
		Głośniki do gier	

		1 Day Auto Copy Over	

		https://prnt.sc/25196a5		
		keen

		# solution
			# rerun

	(R D-1)1303	2021-12-29	New CMS	K00	all	notebooksbilliger.de	
		NOTEBOOK	

		1 Day Auto Copy Over	

		https://prnt.sc/251a2ow		
		keen

		# solution
			# rerun

	(R D-1) 1304	2021-12-29	New CMS	K00	all	boulanger.com	
		'PORTABLE DE JEU',
		'PORTABLE JEU',
		'Tapis de souris de jeu'

		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 23 (Fetched Data is Greater than expected)

		https://prnt.sc/251aoml
		https://prnt.sc/251av7o
		https://prnt.sc/251b3ka		
		renz

		# solution 
			# rerun


	(R D-3) 1305	2021-12-29	New CMS	K00	all	www.box.co.uk	
		'Gaming Mouse',
		'PC Mouse',
		'Mouse',
		'Wireless Mouse',
		'Gaming Headset',
		'PC Headset',
		'Wireless Headset',
		'Gaming Keyboard',
		'PC Keyboard',
		'Keyboard',
		'Mechanical Keyboard',
		'Laptop',
		'Gaming Laptop',
		'Notebook',
		'Gaming Notebook',
		'Gaming Keypad',
		'Keypad',
		'Speakers',
		'Gaming Speakers',
		'2.1 Speakers',
		'Mousemat',
		'Gaming Mousemat',
		'Chair',
		'Ergonomic Chair',
		'Xbox Controller',
		'Playstation Controller',
		'best headphones',
		'Microphone'

		1 Day Auto Copy Over	

		https://prnt.sc/2518m2n
		https://prnt.sc/2518q2m
		https://prnt.sc/2518u0f
		https://prnt.sc/2518x2m
		https://prnt.sc/251909d
		https://prnt.sc/25192sf
		https://prnt.sc/25195ne
		https://prnt.sc/25197xi
		https://prnt.sc/2519a9h
		https://prnt.sc/2519d0r
		https://prnt.sc/2519fln
		https://prnt.sc/2519ixg
		https://prnt.sc/2519lsj
		https://prnt.sc/2519qga
		https://prnt.sc/2519svw
		https://prnt.sc/2519ttb
		https://prnt.sc/251a7g6
		https://prnt.sc/251ackv
		https://prnt.sc/251afxq
		https://prnt.sc/251ahwf
		https://prnt.sc/251ajsa
		https://prnt.sc/251am47
		https://prnt.sc/251asv3
		https://prnt.sc/251awo1
		https://prnt.sc/251b3o3
		https://prnt.sc/251b8e2
		https://prnt.sc/251bffk
		https://prnt.sc/251bk6b		ailyn

		# solution
			# rerun

	(R D-3) 1306	2021-12-29	New CMS	K00	all	dns-shop.ru	
		'БЕСПРОВОДНАЯ ГАРНИТУРА',
			d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g
		'ИГРОВАЯ ГАРНИТУРА',
			b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g
		'НАУШНИКИ ANC',
			f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g
		'ИГРОВАЯ КЛАВИАТУРА',
			f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		'КЛАВИАТУРА',
			5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		'КЛАВИАТУРА ПК',
			94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		'МЕХАНИЧЕСКАЯ КЛАВИАТУРА',
			2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		'БЕСПРОВОДНАЯ МЫШЬ',
			5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g
		'ИГРОВАЯ МЫШЬ',
			058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g
		'КОМПЬЮТЕРНАЯ МЫШКА',
			85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g
		'ИГРОВОЙ НОУТБУК',
			6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g
		'ПОРТАТИВНЫЙ КОМПЬЮТЕР',
			32f099e7-e3fa-4fbf-a839-141afe18fd23@BzxplkXeEeeMawANOiZi-g
		'Игровая клавиатура',
			f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		'Клавиатура ПК',
			94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		'Клавиатура',
			5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		'Механическая клавиатура',
			2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		'Динамики',
			0810e6f3-da20-4952-929a-68c5da92ecc6@BzxplkXeEeeMawANOiZi-g
		'Игровые колонки',
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
		'Игровой стул',
			a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g
		'Офисный стул',
			6ec5bb27-69a4-4fa4-a106-2aa17f5f1b28@BzxplkXeEeeMawANOiZi-g
		'Стул',
			e0e53423-cf4b-48ea-8236-77b5ef68b695@BzxplkXeEeeMawANOiZi-g
		'PS Контроллер',
			f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g
		'Контроллер Xbox One X',
			4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g
		'Контроллер Xbox',
			bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g
		'Контроллер PlayStation',
			f4b38090-8593-426e-a62f-630e83b51d8c@BzxplkXeEeeMawANOiZi-g
		'Игровой коврик для мыши',
			0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g
		'наушники',
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
		'Наушники ANC'
			f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g


		/ 'БЕСПРОВОДНАЯ ГАРНИТУРА',
		/ 'ИГРОВАЯ ГАРНИТУРА',
		/ 'НАУШНИКИ ANC',
		/ 'ИГРОВАЯ КЛАВИАТУРА',
		/ 'КЛАВИАТУРА',
		/ 'КЛАВИАТУРА ПК',
		/ 'МЕХАНИЧЕСКАЯ КЛАВИАТУРА',
		/ 'БЕСПРОВОДНАЯ МЫШЬ',
		/ 'ИГРОВАЯ МЫШЬ',
		/ 'КОМПЬЮТЕРНАЯ МЫШКА',
		/ 'ИГРОВОЙ НОУТБУК',
		/ 'ПОРТАТИВНЫЙ КОМПЬЮТЕР',
		/ 'Динамики',
		/ 'Игровые колонки',
		/ 'Игровой стул',
		/ 'Офисный стул',
		/ 'Стул',
		/ 'PS Контроллер',
		/ 'Контроллер Xbox One X',
		/ 'Контроллер Xbox',
		/ 'Контроллер PlayStation',
		/ 'Игровой коврик для мыши',
		/ 'наушники',

		b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g
		f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g
		5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g
		6ec5bb27-69a4-4fa4-a106-2aa17f5f1b28@BzxplkXeEeeMawANOiZi-g
		e0e53423-cf4b-48ea-8236-77b5ef68b695@BzxplkXeEeeMawANOiZi-g
		f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g
		4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g
		3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g

		--

		d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g
		b64a0837-228e-4fe3-afad-cc61389aaf2b@BzxplkXeEeeMawANOiZi-g
		f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g
		f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g
		058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g
		85a65f97-7851-4ad1-b238-70c510a320de@BzxplkXeEeeMawANOiZi-g
		6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g
		32f099e7-e3fa-4fbf-a839-141afe18fd23@BzxplkXeEeeMawANOiZi-g
		f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		0810e6f3-da20-4952-929a-68c5da92ecc6@BzxplkXeEeeMawANOiZi-g
		9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
		a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g
		6ec5bb27-69a4-4fa4-a106-2aa17f5f1b28@BzxplkXeEeeMawANOiZi-g
		e0e53423-cf4b-48ea-8236-77b5ef68b695@BzxplkXeEeeMawANOiZi-g
		f1507b98-92d9-48c1-8c97-792ef1348e84@BzxplkXeEeeMawANOiZi-g
		4273ed37-4e34-40c3-8b36-f7deb57a0c6e@BzxplkXeEeeMawANOiZi-g
		bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g
		f4b38090-8593-426e-a62f-630e83b51d8c@BzxplkXeEeeMawANOiZi-g
		0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g
		3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
		f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g

		Autocopy Over for 3 days
		Autocopy Over for 1 day
		Autocopy Over for 2 days
		Autocopy Over for 2 days
		Autocopy Over for 2 days
		Autocopy Over for 3 days
		Autocopy Over for 3 days
		Autocopy Over for 3 days
		Autocopy Over for 1 day
		Autocopy Over for 3 days
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 2 days
		Autocopy Over for 3 days
		Autocopy Over for 2 days
		Autocopy Over for 3 days
		Autocopy Over for 2 days
		Autocopy Over for 3 days
		Autocopy Over for 3 days
		Autocopy Over for 3 days
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 1 day
		Autocopy Over for 3 days
		Autocopy Over for 2 days	

		https://prnt.sc/251a6rf
		https://prnt.sc/251aehx
		https://prnt.sc/251aipr
		https://prnt.sc/251apx3
		https://prnt.sc/251awga
		https://prnt.sc/251aza2
		https://prnt.sc/251b3fs
		https://prnt.sc/251b7c4
		https://prnt.sc/251bcls
		https://prnt.sc/251bfga
		https://prnt.sc/251cb6z
		https://prnt.sc/251cdww
		https://prnt.sc/251cgeo
		https://prnt.sc/251d4m9
		https://prnt.sc/251d9k6
		https://prnt.sc/251dd5v
		https://prnt.sc/251dh0q
		https://prnt.sc/251dlkk
		https://prnt.sc/251dpyf
		https://prnt.sc/251dt5k
		https://prnt.sc/251dw7p
		https://prnt.sc/251e10z
		https://prnt.sc/251e4ne
		https://prnt.sc/251eazy
		https://prnt.sc/251ehtv
		https://prnt.sc/251elpm
		https://prnt.sc/251f03s
		https://prnt.sc/251f34u	

		yes	richelle

		# solution
			 # multiple reruns

	(R D-1) 1307	2021-12-29	New CMS	K00	all	dns-shop.ru	
		МЫШЬ	

		Invalid deadlink	

		https://prnt.sc/251bmd4	

		yes	richelle

		# solution
			# multiple reruns

	(R D-2) 1308	2021-12-29	New CMS	200	all	www.re-store.ru	
		/ 'акустика wi-fi', 7
		/ 'НАУШНИКИ С ПРОВОДОМ', 18
		/ 'ПОРТАТИВНАЯ АКУСТИКА WI-FI', 3 
		/'ПОРТАТИВНАЯ КОЛОНКА WI-FI' 3

		b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is lesser than expected)         
		Data Count Mismatch 5 (Fetched Data is lesser than expected)

		https://prnt.sc/251eld3
		https://prnt.sc/251er2y
		https://prnt.sc/251e91g
		https://prnt.sc/251djq5

		michael

		# solution
			# multiple reruns failed: product count changes
			# multiple reruns

	(R D-1) 1309	2021-12-29	New CMS	200	all	www.bol.com	
		enceinte connectée
			Cédric Menard Dictionnaire alimentaire de la femme enceinte
			https://prnt.sc/251xfqw

		duplicate product_website & product_url	
		https://prnt.sc/2510ns5

		michael

		# solution
			# rerun

	(R D-1) 1310	2021-12-29	New CMS	200	all	www.mediamarkt.be/fr	
		enceinte assistant
		enceinte à contrôle vocal

		Data Count Mismatch 1 (Fetched Data is Greater than expected)

		https://prnt.sc/251n7vh 12
		https://prnt.sc/251no7d 21

		michael

		# solution
			# rerun

	(R D-2) 1311	2021-12-29	New CMS	200	all	www.bol.com	
		'actief noice cancelling hoofdtelefoon',12
		'casque bluetooth', 6 
		'enceinte sans fil bluetooth' 4

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)

		https://prnt.sc/251nziv 12 
		https://prnt.sc/251nvro 6
		https://prnt.sc/251ok99 4

		Kristian Angelo

		# solution
			# rerun

	(R D-1) 1312	2021-12-29	New CMS	200	all	www.electronicexpress.com	
		'waterproof bluetooth speaker',
		'smart speaker',
		'portable speakers',
		'portable bluetooth speaker',
		'noise cancelling headphones',
		'earbuds'

		wrong fetch of data

		https://prnt.sc/2520eqp
		https://prnt.sc/251zzip
		https://prnt.sc/251t4gg
		https://prnt.sc/251yoye
		https://prnt.sc/251yfrp
		https://prnt.sc/251x870

		michael

		# solution
			# rerun

	(R D-1) 1313	2021-12-29	New CMS	200	all	www.electronicexpress.com	 
		amazon speaker	

		Data Count Mismatch 4 (Fetched Data is Greater than expected)

		https://prnt.sc/251tgxq		
		michael

		# solution
			# rerun

	-----

	(R D-2) 439	2021-12-29	New CMS	200	all	www.target.com	
		speakers voice
			https://prnt.sc/252etgh

		Data Count Mismatch (Fetched Data is lesser than expected)	

		https://prnt.sc/251s5bw	5

		Yes	kurt

		# solution
			# multiple reruns failed
			# duplicate and sponsored
			# No Issues; Additional count is due to sponsored items
			# Valid duplicate

	-----

	(R D-1) 2217	2021-12-29	New CMS	K00	product_website	www.cyberport.de	
		K00DEB103F000001538350202108110153	
		missing title		

		Yes	Charlemagne

		# solution
			# rerun

	0 (R D-1) 2241	2021-12-29	New CMS	200	price_value	amazon.it	
		'200ITL4070000000531120201903270621',
		'200ITL4070000000531080201903270618',
		'200ITL4070000000530640201903270412',
		'200ITL4070000000530780201903270510',
		'200ITL4070000000530820201903270524',
		'200ITL4070000000531040201903270603',
		'200ITL4070000000530940201903270552',
		'200ITL4070000000530960201903270555',
		'200ITL4080000000531090201903270620',
		'200ITL4080000000531050201903270618',
		'200ITL4080000000531010201903270601',
		'200ITL4080000000530990201903270559',
		'200ITL4080000000530980201903270556',
		'200ITL4070000000531140201903270624',
		'200ITL4080000000530890201903270540'

		# for trello
		'200ITL4070000000530820201903270524',
		'200ITL4080000000530890201903270540',
		'200ITL4070000000530960201903270555',
		'200ITL4080000000530980201903270556',
		'200ITL4080000000531010201903270601',
		'200ITL4070000000531040201903270603',
		'200ITL4080000000531050201903270618',
		'200ITL4070000000531080201903270618',
		'200ITL4070000000531120201903270621'

		Wrong price		

		Yes	Louie

		# solution
			# rerun, still same price result for others
			# for trello those item_ids that price field dont alter

	(R D-1) 2242	2021-12-29	New CMS	200	price_value	amazon.es	
		'200ESS4070000000531110201903270621',
		'200ESS4070000000530730201903270425',
		'200ESS4070000000531030201903270603',
		'200ESS4070000000531130201903270624',
		'200ESS4080000000530900201903270540',
		'200ESS4070000000531150201903270631'
		Wrong price


		200ESS4070000000530730201903270425
		200ESS4080000000530900201903270540
		200ESS4070000000531110201903270621

		Yes	Louie

		# solution
			# multiple reruns

	0 (R D-1)2243	2021-12-29	New CMS	200	price_value	amazon.co.uk
		'200GBG00NC000001426020202106010209',
		'200GBG00B0000000105210201902071110',
		'200GBG00B0000000106970201902071112',
		'200GBG00B0000000107290201902071113',
		'200GBG00B0000000106290201902071111',
		'200GBG00B0000000105600201902071110',
		'200GBG00B0000000103590201902071109',
		'200GBG00C0000000105710201902071110',
		'200GBG00C0000000105530201902071110',
		'200GBG00C0000000106780201902071112',
		'200GBG00C0000000104480201902071109',
		'200GBG00D0000000103940201902071109',
		'200GBG00D0000000104160201902071109',
		'200GBG00D0000000105090201902071110',
		'200GBG00D0000000105380201902071110',
		'200GBG00D0000000107040201902071112',
		'200GBG00D0000000104780201902071109',
		'200GBG00D0000000103100201902071109',
		'200GBG00F0000000107230201902071113',
		'200GBG00G0000000103860201902071109',
		'200GBG00J0000000106690201902071112',
		'200GBG00J0000000106480201902071112',
		'200GBG00H0000000106150201902071111',
		'200GBG0070000000034500201901180329',
		'200GBG0070000001474370202107060614',
		'200GBG00L0000000103500201902071109',
		'200GBG0070000000994100202007310521',
		'200GBG0070000001426040202106010209',
		'200GBG0070000000957220202006020224',
		'200GBG0070000001474580202107060614',
		'200GBG0070000001474650202107060614',
		'200GBG00M0000000104650201902071109',
		'200GBG00N0000000107170201902071113',
		'200GBG00N0000000107510201902071113',
		'200GBG00N0000000105880201902071111',
		'200GBG00N0000000106210201902071111',
		'200GBG00O0000000106880201902071112',
		'200GBG00P0000000104330201902071109',
		'200GBG00P0000000105830201902071111',
		'200GBG00P0000000104400201902071109',
		'200GBG00P0000000103780201902071109',
		'200GBG00P0000000103690201902071109',
		'200GBG00P0000000105980201902071111',
		'200GBG00P0000000105260201902071110'
		
		Wrong price		

		Yes	Louie

		# solution
			# multiple reruns failed
			# for trello: since it requires more time for fix

	Stats:
		Rerun: 21 (25) == 1.19

12 31 21
	/ fix 'fix scenario' version control
	/ fix spiders version control
		/ listings > fi > verkkokauppa
		/ rankings > uk > ebuyer
	/ backup running man tool
	/ backup auto hotkey
	* update laptop

01 01 22
	* job
		/ fixes in daily
		/ send report daily productivity report
		/ send EOW report

	0 (R D-1) Trello NEW CMS Retailer: ebuyer.com

		Issue: Auto copy over / need further investigation

		keyword:
		Gaming Keypad
		Mechanical Keypad
		Playstation Controller

		Screenshot:
		https://prnt.sc/2516g6g
		https://prnt.sc/25170ow 

	0 (R D-1) 1299	2021-12-29	New CMS	K00	all	ebuyer.com	
			Gaming Keypad
			Mechanical Keypad
			Playstation Controller

			Auto copy over for 3 days
			Auto copy over for 1 day
			Auto copy over for 3 days	

			https://prnt.sc/2516g6g
			https://prnt.sc/25170ow
			https://prnt.sc/2517jmv

			renz

	(F D-1) 2246	2022-01-01	New CMS	X00	title	
		www.sears.com.mx	
		X00MXQS0OL000001421470202105190424	

		Special char issue in title	

		https://prnt.sc/25isi19	

		Yes	Jurena

		# solution
			# fix
			# waiting for rebuild then rerun

	Stats:
		Rerun: 2
		Fix: 1[1]

01 05 22
	(R D-1) 2344	2022-01-05	New CMS	K00	image_count	www.power.se	
		K00SEY204K000001555590202108180340
		K00SEY205K000001556190202108180340
		K00SEY20CG000001473700202107060432
		K00SEY20CG000001473720202107060432	
		Missing image_count		
		Yes	Jurena

		# solution
			# rerun by sir ian

	-----
	(R D-1) 1363	2022-01-05	New CMS	K00	all	www.alternate.de	
		Keypad	
		Data Count Mismatch 8 (Fetched Data is Lesser than expected)
		https://prnt.sc/261u6cb		
		ailyn

		# solution
			# rerun

	(F D-1) 1364	2022-01-05	New CMS	K00	product_website	www.netonnet.se
		'2.1 Högtalare',
		'2.1 Speakers',
		'ANC-hörlurar',
		'Bärbar dator',
		'bästa hörlurarna',
		'Ergonomisk stol',
		'Gaming Headset',
		'Gaming Speakers',
		'Headset',
		'Högtalare',
		'hörlurar',
		'Knappsats',
		'Kontorsstol',
		'Mekanisk knappsats',
		'Mekaniskt tangentbord',
		'Mus',
		'PC-Headset',
		'PC-knappsats',
		'PC-mus',
		'PC-tangentbord',
		'Playstation-kontroller',
		'Ps4-styrenhet',
		'PS-kontroller',
		'Speakers',
		'Spel Notebook',
		'Spel tangentbord',
		'Spelbärbar dator',
		'Spelhögtalare',
		'Spelmus',
		'Spelstol',
		'Speltangentbord',
		'Stol',
		'Tangentbord',
		'Trådlös mus',
		'Trådlöst headset',
		'Xbox One X-kontroller',
		'Xbox-kontroller'

		Incorrect Product_website

		https://prnt.sc/261w3fx
		YEs	Rayyan

		# solution
			# fix
			# waiting rebuild
			# rerun

	(R D-1) 1366	2022-01-05	New CMS	200	all	www.dns-shop.ru	
		наушники вкладыши проводные	
			41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g
		Auto Copy Over for 3 days	
		https://prnt.sc/261xd4c		
		Phil

		# solution
			# rerun

	(R D-1) 1367	2022-01-05	New CMS	200	all	www.dns-shop.ru	
		проводные внутриканальные наушники	
		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	
		https://prnt.sc/261z8dp		
		Phil

		# solution
			# rerun

	(R D-2) 1371	2022-01-05	New CMS	200	all	www.re-store.ru	
		'акустика wi-fi', 6
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		'наушники с проводом', 18
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		'портативная колонка wi-fi', 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		'проводные наушники' 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 2 (Fetched Data is Lesser than expected)
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)

		https://prnt.sc/261zpba
		https://prnt.sc/26200k5
		https://prnt.sc/262067y
		https://prnt.sc/2620bvr

		Phil

		# solution
			# multiple reruns failed but accepted by validator

	(R D-1) 1372	2022-01-05	New CMS	K00	all	elgiganten.se	
		PC-MUS
		SPEL NOTEBOOK
		Autocopy over for 1 day 
		https://prnt.sc/261yx1p
		https://prnt.sc/261z1nx
		Mich

		# solution
			# rerun

	(R D-2) 1373	2022-01-05	New CMS	K00	all	elgiganten.se	
		/ 'MEKANISKT TANGENTBORD', 16
		/ 'Speltangentbord', 3
		/ 'Mekanisk knappsats', 11
		'Högtalare', 25
			# redirect but has 9 result in db
		/ 'Kontorsstol', 17
		/ 'PS-kontroller', 6
		/ 'Xbox One X-kontroller', 15
		/ 'ANC-hörlurar' 15
			
		Data Count Mismatch 8 (Fetched Data is Lesser than expected)
		Data Count Mismatch 22 (Fetched Data is Lesser than expected)
		Data Count Mismatch 6 (Fetched Data is Lesser than expected)
		Data Count Mismatch 16 (Fetched Data is Lesser than expected)
		Data Count Mismatch 8(Fetched Data is Lesser than expected)
		Data Count Mismatch19 (Fetched Data is Lesser than expected)
		Data Count Mismatch 10 (Fetched Data is Lesser than expected)
		Data Count Mismatch 10 (Fetched Data is Lesser than expected)

		https://prnt.sc/2621aqw
		https://prnt.sc/2620jt5
		https://prnt.sc/26211u2
		https://prnt.sc/2621xba
		https://prnt.sc/261zm2m
		https://prnt.sc/261zxtg
		https://prnt.sc/261zrqs
		https://prnt.sc/2620854
		
		Mich

		# solution
			# multiple reruns failed
			# its not failed, it is valid in site

	(R D-1) 1390	2022-01-05	New CMS	200	all	www.netonnet.se	
		'alexa högtalare',
		'bärbar högtalare',
		'bluetooth högtalare',
		'bluetooth hörlurar',
		'bluetooth hörlurar in ear',
		'bluetooth hörlurar träning',
		'brusreducerande hörlurar',
		'google högtalare',
		'google home högtalare',
		'högtalare bluetooth',
		'högtalare wifi',
		'hörlurar bluetooth',
		'hörlurar brusreducering',
		'hörlurar in ear',
		'hörlurar löpning',
		'in ear hörlurar',
		'multiroom högtalare',
		'noise cancelling hörlurar',
		'öronsnäcka',
		'portabel högtalare',
		'trådlös högtalare',
		'trådlösa hörlurar',
		'trådlösa hörlurar in ear',
		'trådlösa hörlurar träning',
		'trådlösa in ear hörlurar',
		'vattentät högtalare',
		'wifi högtalare'

		Wrong product title fetch	
		https://prnt.sc/2630dou		
		Christopher	1/5/2021

		# solution
			# waiting for rebuild since it was fixed in issue: 1364
			# rerun

	(R D-1) 1391	2022-01-05	New CMS	K00	all	www.netonnet.se
		Mekaniskt tangentbord
		1 Day Auto Copy over data
		https://prnt.sc/263cocs
		Yes	Rayyan

		# solution 
			# rerun

	Stats:
		Rerun: 9 (10) = 10/9 = 
		Fix: 1[1]

01 06 22
	(R D-1) 2383	2022-01-06	New CMS	200	all	heureka.cz	
		'200CZ9D080000000804600201907080640',
		'200CZ9D080000000550480201904050601',
		'200CZ9D070000000804640201907080646',
		'200CZ9D080000000804610201907080642',
		'200CZ9D070000000804320201907080402',
		'200CZ9D070000000804350201907080416',
		'200CZ9D070000000551040201904050624',
		'200CZ9D080000000552060201904050701',
		'200CZ9D080000000804490201907080629',
		'200CZ9D080000000804570201907080636',
		'200CZ9D080000000804580201907080637'

		Invalid deadlink

		Yes	Mojo

		# Solution
			# not visible
			# rerun interval as per advise of sir Ryan
			# altered sheet by sir Ryan

	(R D-1) 1395	2021-01-06	New CMS	O00	all	www.mediamarkt.be/nl/	
		Droogkast	
		duplicate product_website & product_url	
		https://prnt.sc/266n27l		
		ailyn

		# solution
			# rerun

	(R D-1) 1396	2021-01-06	New CMS	O00	all	www.vandenborre.be/nl	
		Lifestyle TV	
		invalid deadlink	
		https://prnt.sc/266pwdo		
		keen

		# solution
			# rerun

	(R D-1) 1397	2021-01-06	New CMS	O00	all	www.vandenborre.be/nl	
		Koelkast aanbieding	
		uAto Copy Over for 1 Day	
		https://prnt.sc/266rho9		
		keen

		# solution
			# rerun failed
			# 

	(R D-1) 1398	2021-01-06	New CMS	200	all	www.re-store.ru
		акустика wi-fi - 6
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		/ ПРОВОДНЫЕ НАУШНИКИ - 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 2 (Fetched data is lesser than expected)
		Data Count Mismatch 4 (Fetched data is lesser than expected)

		https://prnt.sc/266qfqs
		https://prnt.sc/266r9w7
		Yes
		Rayyan

		# solution
			# rerun

	(R D-1) 1399	2021-01-06	New CMS	200	all	www.boulanger.com	
		enceinte amazon	- 19
		Data Count Mismatch 4 (Fetched Data is Greater than expected)	
		https://prnt.sc/266ll9d		
		Christopher

		# solution
			# rerun

	(R D-2) 1400	2021-01-06	New CMS	200	all	www.mediamarkt.de	
		kopfhörer in ear
			2022-01-06@22:00:00@200@dcb7c03b-0163-462e-b76c-f19e407fe3c0@M3YPUApIEem-gAANOiOHiA
			NEDIS HPBT3052WT, In-ear 

			NEDIS HPBT3260BK, Over-ear Kopfhörer Schwarz
		/ voice lautsprecher	
			2022-01-06@22:00:00@200@c63413b9-7d0f-4909-a4e4-aa051eaa908c@M3YPUApIEem-gAANOiOHiA
			FIGUREIGHT Grey Mcmurray - Stay Up (180g LP)  - (Vinyl)

		duplicate product_website & product_url	


		https://prnt.sc/266ny6o
		https://prnt.sc/266o5ep		
		Christopher

		# solution
			# mutlple reruns failed
			# site behavior

	(R D-1) 1402	2021-01-06	New CMS	200	all	www.elkjop.no	
		mini høyttaler	- 7
		Data Count Mismatch 6 (Fetched Data is Greater than expected)	
		https://prnt.sc/266t076		Christopher

		# https://prnt.sc/266zjud

		# solution 
			# rerun

	Stats:
		Rerun: 8 (9) = 1.13


01 07 22
	(R D-1) 1406	2021-01-07	Old CMS	610	all	www.cervera.se	
		kaffekanne	
		Incorrect Product Website/Product Url fetched	
		https://prnt.sc/26awoon
		Yes	Joemike

		# solution
			# fix unique request url
			# waiting for 3pm rebuild
			# rerun

	(R D-1) 1413	2021-01-07	New CMS	200	all	www.saturn.de	
		kabellose kopfhörer
			986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA	
		duplicate product_website & product_url		
		https://prnt.sc/26axdax		
		Christopher

		# solution
			# rerun

	(R D-1) 1414	2021-01-07	New CMS	200	all	www.elkjop.no	
		hodetelefoner trådløse
			d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw
		in ear hodetelefoner	
			bbf40a90-31dc-48b5-aee1-7ac98887ac46@EGO2juO8EeaOEAANOrFolw
		duplicate product_website & product_url		
		https://prnt.sc/26axj1n		
		Christopher

		# solution
			# rerun

	(R D-1) 1415	2021-01-07	New CMS	200	all	www.elkjop.no	
		mini høyttaler	- 13
			98db7e9b-c280-43d3-9fd4-f78c6adf95e7@EGO2juO8EeaOEAANOrFolw
		Data Count Mismatch 6 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26axiqk		
		Christopher

		# solution
			# rerun

	(R D-1) 1416	2021-01-07	New CMS	200	all	www.elgiganten.se	
		trådlösa hörlurar träning	
			5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw
			2022-01-07@22:00:00@200@5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw
		duplicate product_website & product_url		
		https://prnt.sc/26axl06		
		Christopher

		# solution
			# rerun

	(R D-1) 1417	2021-01-07	New CMS	200	all	www.bol.com	
		enceinte assistant	
			67b1f84a-c8a0-4d22-ade4-3448a6b5015c@UAjszpOqEei-eQANOiOHiA
			2022-01-07@22:00:00@200@67b1f84a-c8a0-4d22-ade4-3448a6b5015c@UAjszpOqEei-eQANOiOHiA
		duplicate product_website & product_url		
		https://prnt.sc/26axmeg		
		Christopher

		# solution
			# rerun

	(R D-1) 1422	2021-01-07	New CMS	Z00	all	www.visions.ca/default.aspx	
		portable bluetooth speaker	
		Auto Copy Over for 1 Day	
		https://prnt.sc/26az7al

		# solution
			# rerun

	(R D-1) 1423	2021-01-07	New CMS	Z00	all	www.costco.com	
		/ amp
			6dc581b7-a9b8-4e7f-a198-270a8593c35e@GzxImt_KEee8oQANOijygQ
		surround sound
			f9d6d588-a738-413f-a8af-1deec79a60db@GzxImt_KEee8oQANOijygQ
			https://prnt.sc/26b0cpu

		Auto Copy Over for 2 Days
		Auto Copy Over for 1 Day	

		https://prnt.sc/26azbqz
		https://prnt.sc/26azb9h		
		keen

		# solution
			# amp: fixed through rerun
			# surround sound: valid dead link or top level category
				# https://prnt.sc/26b0cpu

	-----

	(R D-1) 503	2021-01-17	Old CMS	Q00	all	www.wiggle.co.uk
		Shoes Men
		1 Day Auto Copy over data
		https://prnt.sc/26aysop
		Yes	Rayyan

		# solution
			# rerun

	Stats:
		Rerun: 9

01 08 22
	(R D-1) 1427	2021-01-08	New CMS	200	all	www.dns-shop.ru	
		беспроводные наушники
			fcdf721a-231b-4ce6-a051-35769d9b1ebb@BzxplkXeEeeMawANOiZi-g
		проводные внутриканальные наушники	
			6c37e71c-f038-438f-84a2-2ba31fcd3323@BzxplkXeEeeMawANOiZi-g
		Auto Copy Over for 3 Days	
		https://prnt.sc/26b92ja
		https://prnt.sc/26b93di		
		Phil

		# solution
			# rerun

	(R D-1) 1428	2021-01-08	New CMS	200	all	www.re-store.ru	
		наушники с проводом - 19
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		проводные наушники - 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab
		портативная акустика wi-fi - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 3 (Fetched data is lesser than expected)
		Data Count Mismatch 2 (Fetched data is lesser than expected)
		Data Count Mismatch 1 (Fetched data is lesser than expected)	

		https://prnt.sc/26b944g
		https://prnt.sc/26b96e4
		https://prnt.sc/26b96op
		Phil

		# solution
			# rerun

	(R D-1) 1429	2021-01-08	New CMS	200	all	www.elkjop.no	
		bluetooth høyttaler
			6be84c2b-084b-49f0-b82d-94a96b8a4d55@EGO2juO8EeaOEAANOrFolw
		smarthøyttaler	
			86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw

		duplicate product_website & product_url		
		https://prnt.sc/26b9aj3
		https://prnt.sc/26b9azu		
		Phil

		# solution
			# rerun

	(R D-1) 1430	2021-01-08	New CMS	200	all	www.elgiganten.se	
		bluetooth hörlurar
			bI6h1lKhEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw
		in ear hörlurar
			4be825a7-7cc4-456b-bbaa-7967fe2fa31b@fVQR1tMrEeaOEAANOrFolw
		vattentät högtalare
			60abfa6a-31ac-4dd5-b27c-d2f8e3f60e88@fVQR1tMrEeaOEAANOrFolw

		duplicate product_website & product_url
		
		https://prnt.sc/26b97yk
		https://prnt.sc/26b98rh
		https://prnt.sc/26b99b5
		https://prnt.sc/26b99kh	
		Phil

		# solution
			# rerun

	(R D-1) 1431	2021-01-08	New CMS	200	all	www.bol.com	
		draadloos hoofdtelefoon

		duplicate product_website & product_url		
		https://prnt.sc/26b97dm		
		Phil

		# solution
			# rerun

	(R D-1) 1433	2021-01-08	New CMS	200	all	www.target.com	
		/ portable bluetooth speaker
			https://prnt.sc/26baszl
		/ smart speaker	

		duplicate product_website & product_url		

		https://prnt.sc/26bap20
		https://prnt.sc/26baps7
		Phil

		# solution
			# rerun failed
			# valid duplicate: https://prnt.sc/26baszl

	-----

	(R D-1) 504	2021-01-08	New CMS	200	all	www.re-store.ru	
		speakers voice	
			200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd
		Data Count Mismatch 22 (Fetched Data is lesser than expected)	
		https://prnt.sc/26b93e7	

		Yes	kurt

		# solution
			# rerun

	(F D-1) 505	2021-01-08	New CMS	200	all	www.zalando.de	
		Speakers Bluetooth	
		1 Day Auto Copy over data
		https://prnt.sc/26b9c8u	Yes	Rhunick

		# solution
			# fix with sir ian
			# rebuild, rerun		

	Stats:
		Rerun: 7
		Fix: 1[1]

01 09 22
	(R D-1) 2433	2022-01-09	New CMS	K00	all	alza.cz	
		K00CZ3603F000001526950202108020954	
		spider issue -1 values		
		Yes	Mayeeeee

		# solution
			# rerun

	(R D-1) 2436	2022-01-09	New CMS	100	all	power.se	
		100SEY2010000001665590202111170530	
		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# rerun

	(R D-1) 2437	2022-01-09	New CMS	100	image_count	stadium.se	
		All item_ids	
		No Webshot		
		Yes	Jeremy

		100SEMH010000000822420201908060255
		100SEMH010000000822480201908060300
		100SEMH010000000823110201908060426
		100SEMH010000000823320201908060531
		100SEMH010000000823360201908060534
		100SEMH010000000857490201910100748
		100SEMH010000000882040201911140150
		100SEMH010000001230230202010290758
		100SEMH010000001319610202102040906
		100SEMH010000001321020202102050342
		100SEMH010000001321770202102050343
		100SEMH010000001321960202102050343
		100SEMH010000001321970202102050343
		100SEMH010000001322710202102051007
		100SEMH010000001323540202102051007
		100SEMH010000001440280202106090650

		# solution
			# rerun

	(R D-1) 2439	2022-01-09	New CMS	100	all	utomhusliv.se	
		100SETX010000001649340202111090812
		100SETX010000001649510202111090812	

		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# rerun

	(R D-1) 2440	2022-01-09	New CMS	100	all	utomhusliv.se	
		100SETX010000001646870202111090741	
		Not_found in Stock		
		Yes	Jeremy

		
		# solution
			# rerun

	(R D-2) 2441	2022-01-09	New CMS	Z00	all	walmart.com	all item ids	
		Auto copyover site is up		
		Yes	Turki

		https://www.walmart.com/ip/Sonos-Roam-Smart-speaker-for-portable-use-Wi-Fi-Bluetooth-App-controlled-2-way-shadow-black/866771858
		https://www.walmart.com/ip/Sonos-Roam-Smart-speaker-for-portable-use-Wi-Fi-Bluetooth-App-controlled-2-way-shadow-black/130486269
		https://www.walmart.com/ip/Sonos-Move-Durable-Battery-Powered-Smart-Speaker-with-Additional-Charging-Base-White/781465802
		https://www.walmart.com/ip/Sonos-Move-Durable-Battery-Powered-Smart-Speaker-with-Additional-Charging-Base-Black/680559931
		https://www.walmart.com/ip/Sonos-Sub-Wireless-Subwoofer-Gen-3/176889944
		https://www.walmart.com/ip/Sonos-Sub-Gen-3-The-Wireless-Subwoofer-for-Deep-Bass-Black/266671225
		https://www.walmart.com/ip/Sonos-Beam-White-Smart-Compact-Soundbar/672440069
		https://www.walmart.com/ip/Sonos-Beam-Black-Smart-Compact-Soundbar/636022223
		https://www.walmart.com/ip/Sonos-Arc-Wireless-Sound-Bar-with-Extendable-Wall-Mount-White/855585592
		https://www.walmart.com/ip/Sonos-Arc-Sound-bar-5-0-channel-wireless-Ethernet-Fast-Ethernet-Wi-Fi-NFC-App-controlled-2-way-black/171396558
		https://www.walmart.com/ip/Sonos-One-Gen-2-Two-Room-Wireless-Speaker-Set-with-Voice-Control-Built-In/242395559
		https://www.walmart.com/ip/Sonos-One-Gen-2-Two-Room-Wireless-Speaker-Set-with-Voice-Control-Built-In/757651197?selected=true
		https://www.walmart.com/ip/Sonos-One-SL-Microphone-Free-Smart-Speaker-Black/535028268
		https://www.walmart.com/ip/Sonos-One-SL-Microphone-Free-Smart-Speaker-Black/415344341
		https://www.walmart.com/ip/Sonos-Five-Speaker-wireless-Wi-Fi-App-controlled-2-way-white/257760257
		https://www.walmart.com/ip/Sonos-Five-Speaker-wireless-Ethernet-Fast-Ethernet-Wi-Fi-App-controlled-2-way-matte-black/103539613?selected=true
		https://www.walmart.com/ip/SONOS-AMPG1US1BLK-Amplifier-250-W-RMS-2-Channel-Black/496227416
		https://www.walmart.com/ip/Sonos-PORT1US1BLK-Port-Streaming-Media-Player-Matte-Black/613832608

		https://www.walmart.com/ip/Sonos-Beam-Gen-2-Compact-Smart-Sound-Bar-with-Dolby-Atmos-White/457521756

		# solution
			# multiple reruns failed
			# auto copy over but site and spiders is ok: blocked

	(R D-1) 2445	2022-01-09	New CMS	200	all	fnac.com	
		'200FR21070000001474640202107060614',
		'200FR21080000001246130202011200116'
		invalid deadlink		
		Yes	Louie

		# solution
			# multple reruns failed
			# multiple reruns interval

	(R D-1) 2446	2022-01-09	New CMS	200	all	fnac.com/3P	
		200FRDU080000001381630202104141007	
		invalid deadlink		
		Yes	Louie

		# solution
			# rerun

	(R D-1) 2447	2022-01-09	New CMS	200	webshot_url		
		select * from view_all_productdata where date = '2022-01-09' and webshot_url is NULL	
		No Webshot		
		Yes	Jeremy

		# solution
			# rerun with Kenn

	(R D-1) 2448	2022-01-09	New CMS	100	all	power.no	
		100NOZ2010000001397700202105040251	
		Spider Issue		
		Yes	Keeshia

		# solution
			# rerun

	(R D-1) 2449	2022-01-09	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000000814140201908020341',
		'100DK10010000001222820202010280537',
		'100DK10010000001248230202011250630',
		'100DK10010000001248330202011250630',
		'100DK10010000001248390202011250630',
		'100DK10010000001324630202102090603',
		'100DK10010000001326510202102090603',
		'100DK10010000001397640202105040251',
		'100DK10010000001438730202106080416	'
		invalid -3 delivery		
		Yes	Turki

		# Solution
			# multiple reruns

	(R D-1) 2450	2022-01-09	New CMS	100	all	elgiganten.dk	
		'100DK10010000001324310202102090603',
		'100DK10010000001659210202111162228',
		'100DK10010000001659910202111162228',
		'100DK10010000001659670202111162228',
		'100DK10010000001397940202105040251'

		spider issue -1 values
		Yes	Turki

		# solution
			# multiple reruns

	-----
	(R D-1) 1437	2021-01-09	New CMS	200	all	www.re-store.ru	
		акустика wi-fi 7 
		Data Count Mismatch 2 (Fetched data is lesser than expected)	
		https://prnt.sc/26bkphd		
		Phil

		# solution
			# multiple reruns failed
			# blocked

	(R D-2) 1438	2021-01-09	New CMS	200	all	www.elkjop.no	
		/ bærbar høytaler
		/ trådløs høyttaler wifi
			069deba5-45a9-4199-8edb-302967c084c1@UAjszpOqEei-eQANOiOHiA
		/ wifi høyttaler	

		duplicate product_website & product_url	

		https://prnt.sc/26bky8v
		https://prnt.sc/26bkydm
		https://prnt.sc/26bkztb
		https://prnt.sc/26bl05e		
		Phil

		# solution
			# rerun

	(R D-1) 1440	2021-01-09	New CMS	200	all	www.bol.com	
		spatwaterdichte speaker	

		duplicate product_website & product_url		
		https://prnt.sc/26bkt3n		
		Phil

		# solution
			# rerun

	(R D-1) 1441	2021-01-09	New CMS	200	all	www.mediamarkt.de	
		voice lautsprecher	
		duplicate product_website & product_url		
		https://prnt.sc/26bl0po	

		# solution
			# rerun

	(R D-1) 1442	2021-01-09	New CMS	200	all	www.target.com	
		portable speakers	
		duplicate product_website & product_url		
		https://prnt.sc/26bm7pa		
		Phil

		# solution
			# rerun

	-----

	(R D-1) 509	2021-01-09	New CMS	200	all	www.bol.com	
		Speakers Portable	
		Data Count Mismatch 1 (Fetched Data is lesser than expected)	
		https://prnt.sc/26bkpqf	
		Yes	MICHAEL

		# solution
			# rerun

	(R D-1) 512	2021-01-09	New CMS	100	all	www.boerkopcykler.dk	
		Cykelcomputer med GPS	
		Auto Copy Over for 1 day	
		https://prnt.sc/26bkwvn	
		Yes	kurt

		# solution
			# rerun

	(R D-1) 513	2021-01-09	New CMS	200	all	www.re-store.ru	
		Headphones Wired In Ear	- 8
		Data Count Mismatch 3  (Fetched Data is lesser than expected)	
		https://prnt.sc/26bl3vp	
		Yes	MICHAEL

		# solution
			# rerun

	Stats:
		Rerun: 20 (22) 22/20

01 12 22
	0 (R D-1) Trello
		1454	2022-01-11	New CMS	P00	all	www.citilink.ru 	
			Моторола G
			Моторола Edge
			Андроид телефон	

			Data Count Mismatch 11 (Fetched Data is Lesser than expected)
			Data Count Mismatch 1(Fetched Data is Lesser than expected)
			Data Count Mismatch 7 (Fetched Data is Lesser than expected)	

			https://prnt.sc/26c95uk
			https://prnt.sc/26c96or
			https://prnt.sc/26c97fj
			ailyn

	(R D-1) 1480	2022-01-12	New CMS	K00	all	www.box.co.uk	
		'ANC Headphones',
		'Playstation Controller',
		'PS Controller',
		'Ergonomic Chair',
		'Office Chair',
		'Gaming Chair',
		'Mousemat',
		'2.1 Speakers',
		'Gaming Speakers',
		'Keypad',
		'PC Keypad',
		'Gaming Keypad',
		'Gaming Notebook',
		'Notebook',
		'Gaming Laptop',
		'Laptop',
		'Mechanical Keyboard',
		'Keyboard',
		'PC Keyboard',
		'Gaming Keyboard',
		'Wireless Headset',
		'PC Headset',
		'Gaming Headset',
		'Wireless Mouse',
		'Mouse',
		'PC Mouse',
		'Gaming Mouse'

		Auto Copy Over for 1 Day

		https://prnt.sc/26cmh8n
		https://prnt.sc/26cmhxr
		https://prnt.sc/26cmimb
		https://prnt.sc/26cmjjn

		kurt

		# solution
			# rerun

	(R D-2) 1491	2022-01-12	New CMS	K00	all	elkjop.no	
		GAMING BÆRBAR
			2022-01-12@22:00:00@K00@e9b22926-a5d1-4fbe-ba49-671c6a5f85cf@EGO2juO8EeaOEAANOrFolw
			HP Pavilion 15-ec1816no 15,6 bærbar gaming-PC
				https://prnt.sc/26cn0sg
			HP Pavilion 17-cd1811no 17,3 bærbar gaming PC
				https://prnt.sc/26cn0zh
			HP Pavilion 17-cd1814no 17,3 bærbar gaming-PC
				https://prnt.sc/26cn141

			Lenovo IdeaPad Gaming 3 15ARH05 15,6 bærbar gaming-PC R5/8/512/1650TI
				https://prnt.sc/26cn18n

			# fixed with rerun

		Høyttalere
			558b9bbf-2344-4fd9-919e-7a54bed98168@EGO2juO8EeaOEAANOrFolw
			Bose brilleglass i Tenor-stil (Road Orange)
				https://prnt.sc/26cn1f6
				https://prnt.sc/26cnwcm

		/ Stol
			f96ddb9f-30a6-49f7-b984-06eea8cfcc83@EGO2juO8EeaOEAANOrFolw

		duplicate product_website
		
		https://prnt.sc/26cmhc7
		https://prnt.sc/26cmhto
		https://prnt.sc/26cmg3c		
		keen

		# Solution
			# investigate, rerun

	(R D-2) 1492	2022-01-12	New CMS	K00	all	elkjop.no	
		'PC-HEADSET', 8
		'SPILLMUS', 11 
		'Xbox One X-kontroller', 21 
		'ANC-hodetelefoner' 24

		Data Count Mismatch 17 (Fetched Data is Greater than expected
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 21 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26cmjiq
		https://prnt.sc/26cmk7i
		https://prnt.sc/26cmm48
		https://prnt.sc/26cmmmz		

		keen

		# solution
			# multiple reruns
			# test local goods

	-----

	(R D-1) 2529	2022-01-12	New CMS	200	webshot_url		
		select * from view_all_productdata where date = '2022-01-12' and webshot_url is NULL	
		No webshots		
		Yes	Louie

		# solution
			# rerun

	(R D-1) 2530	2022-01-12	New CMS	K00	image_count	power.no	
		K00NOZ203F000001555140202108180340
		K00NOZ20CG000001467390202107050618	

		Missing  image count	
		https://prnt.sc/26colu3	
		Yes	Jahar

		# solution
			# rerun

	(F D-2) 2531	2022-01-12	New CMS	Z00	rating_reviewers	bestbuy.ca	
		all item_ids	invalid -1 ratings and reviews		
		Yes	Turki

		Z00CAS50O0000001455020202106220604
		Z00CAS50O0000001455410202106220604
		Z00CAS50O0000001455570202106220724
		Z00CAS50O0000001455600202106220724
		Z00CAS50O0000001455630202106220724
		Z00CAS50O0000001455660202106220724
		Z00CAS50O0000001455690202106220724
		Z00CAS50O0000001455720202106220724
		Z00CAS50O0000001455750202106220724
		Z00CAS50O0000001455780202106220724
		Z00CAS50O0000001455810202106220724
		Z00CAS50O0000001455840202106220724
		Z00CAS50O0000001455870202106220724
		Z00CAS50O0000001455990202106220724
		Z00CAS50O0000001456020202106220724
		Z00CAS50O0000001485580202107130218
		Z00CAS50BR020211104071539507797308
		Z00CAS50BR020211104071540810658308

		# solution
			# fix
			# waiting for 6pm rebuild
		
	(R D-1) 2533	2022-01-12	New CMS	X00	webshot_url	walmart.com.mx	
		All item_ids	
		Webshot blocked		
		Yes	Mark

		# solution
			# rerun

	(R D-1) 2535	2022-01-12	New CMS	K00	price_value	mvideo.ru	
		'K00RUP807O000001449520202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001449540202106150452',
		'K00RUP803F000001449440202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP803F000001527340202108020954',
		'K00RUP803F000001449400202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000001310880202101250600',
		'K00RUP80CG000001524900202108020807',
		'K00RUP80CG000001373070202104050819',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000000980410202006250902',
		'K00RUP80CG000001311080202101250600',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000000980430202006250902',
		'K00RUP80CG000000980440202006250902',
		'K00RUP80CG000001311010202101250600',
		'K00RUP80CG000000980380202006250902',
		'K00RUP80IO000001449640202106150452',
		'K00RUP805K000001527540202108020954',
		'K00RUP805K000001527570202108020954',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449670202106150452'

		wrong price fetched		
		Yes	Arfen

		# solution
			# rerun

	(R D-1) 2536	2022-01-12	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP804K000001527710202108020954',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP804K000001527750202108020954',
		'K00RUP803F000001449360202106150452',
		'K00RUP80CG000000980430202006250902',
		'K00RUP80CG000001310930202101250600',
		'K00RUP805K000001527570202108020954'

		incorrect ratings fetched
		Yes	Arfen

		# solution
			# rerun

	Stats:
		Rerun: 9 (11/9) = 1.22
		Fix: 1[2]

01 13 22
	(R D-1) 2537	2022-01-13	New CMS	200		elgiganten.dk	
		200DK100C0000000039290201901180331	
		Price, stocks and DQ not fetched		
		Yes	Mojo

		# rerun

	(R D-1) 2547	2022-01-13	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000557760201904220952',
		'200ITGF070000000556900201904220850',
		'200ITGF070000000557480201904220941',
		'200ITGF080000000557080201904220902',
		'200ITGF080000000801670201906200933'
		Invalid deadlink		
		Yes	Mojo

		# solution
			# multiple reruns failed
			# assisted by sir Ryan

	-----
	(R D-1) 1511	2022-01-13	New CMS	F00	all	bol.com	
		TELEFOONS	
			03a8a6ec-6602-4d27-82a1-693916d7b1e5@UAjszpOqEei-eQANOiOHiA

		3 days auto copyover 	
		https://prnt.sc/26czuoh

		# solution
			# rerun

	(R D-1) 1512	2022-01-13	New CMS	F00	all	bol.com
		Galaxy S21
		S21
		Samsung S21

		Mismatched data count 2  (Fetched Data is lesser than expected
		Mismatched data count 2  (Fetched Data is lesser than expected
		Mismatched data count 2  (Fetched Data is lesser than expected

		https://prnt.sc/26cztpt
		https://prnt.sc/26czt20
		https://prnt.sc/26czsf3

		# solution
			# rerun

	(R D-1) 1516	2022-01-13	New CMS	O00	all	www.bol.com	
		Autodose	

		Data Count Mismatch 1 (Fetched Data is Greater than expected	

		https://prnt.sc/26d035j		
		keen

		# solution
			# rerun

	(R D-1) 1520	2022-01-13	New CMS	O00	all	www.wehkamp.nl	
		was-droogcombinatie	
		Data Count Mismatch 1 (Fetched Data is Greater than expected)
		https://prnt.sc/26d07kf		
		KEEN

		# solution
			# rerun

	(R D-2) 1521	2022-01-13	New CMS	200	all	www.elkjop.no	
		/ google høyttaler
			23b7a17a-a045-4982-ab87-0656aeafd85f@EGO2juO8EeaOEAANOrFolw
		smarthøyttaler
			86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw
		
			JBL Link 300 høyttaler (hvit)
				https://prnt.sc/26d0mi5
			JBL Link 300 høyttaler (sort)
				https://prnt.sc/26d0mro

			JBL Link Music trådløs høyttaler (grå)
				https://prnt.sc/26d0mxv
		/ støydempende hodetelefoner
			fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw

			Sony WH-CH700N trådløse on-ear hodetelefoner (grå)
		/ trådløs høyttaler wifi
			77786a55-9ea9-4477-bdd4-8d085dfa4c5a@EGO2juO8EeaOEAANOrFolw

		duplicate product_website	

		https://prnt.sc/26d0cmy
		https://prnt.sc/26d0de0
		https://prnt.sc/26d0dsf
		https://prnt.sc/26d0fqy
		https://prnt.sc/26d0g0o
		https://prnt.sc/26d0gif
		https://prnt.sc/26d0h6e
		https://prnt.sc/26d0he6
		https://prnt.sc/26d0hmj

		Yes	Yev

	(R D-2) 1522	2022-01-13	New CMS	200	all	www.bol.com	
		/ actief noice cancelling hoofdtelefoon - 13
		woonkamer speaker - 25
			https://prnt.sc/26d0yqf

		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26d0m14
		https://prnt.sc/26d0nfc
		
		Yes	Yev

		# solution
			# multiple reruns
			# manual product check values and value counts

	(R D-1) 1523	2022-01-13	New CMS	200	all	www.bol.com	
		Bluetooth hoofdtelefoon
		duplicate product_website	
		https://prnt.sc/26d0l1c	
		Yes	Yev
			
		# solution
			# rerun
	-----
	(R D-1) 538	2022-01-13	New CMS	O00	all	www.expert.nl	
		Soundbar 2020	
		
		1 Day Auto Copy Over	
		
		https://prnt.sc/26czx90	
		Yes	Joemike

		# solution
			# rerun

	(R D-1) 539	2022-01-13	New CMS	O00	all	www.mediamarkt.nl	
		Dryer
			O00@Q10@1b3d74e7-618b-4ccd-8ef7-9ddf4ca21e12
		TV 2020	
			O00@Q10@84d29899-142b-43e3-81c2-18e8562f502e
		Duplicate Product Website/Product Url	

		https://prnt.sc/26czten
		https://prnt.sc/26czu8k	
		Yes	Joemike

		# solution
			# rerun

	(F D-1) 540	2022-01-13	New CMS	200	product_website	www.netonnet.se	
		'Headphones Bluetooth On Ear',
		'Headphones Noise Cancelling',
		'Headphones Wired In Ear',
		'Speakers Bluetooth',
		'Speakers Multiroom',
		'Headphones Bluetooth In Ear'

		Incorrect scrape of product title	
		https://prnt.sc/26d0eaz
		Yes	Jairus

		# solution
			# fixing
			# rebuild
			# rerun

	Stats:
		Rerun: 11 (13) = 1.18
		Fix: 1[1]

01 14 22
	0 (R D-1) Trello
		Date: January 13, 2022
		Company: MS Price (A00)
		Retailer: microsoft.com/en-ca
		Issue: Incorrect Stock status

		SAMPLE ITEM ID:
		A00CA640KT020211025061827303632298
		A00CA640KT020211025061827429306298
		A00CA640KT020211025061827181260298
		A00CA640KT020211025061827029252298
		A00CA640KT020211025061826781227298
		A00CA640KT020211025061821542564298

	-----
	(R D-1) 2560	2022-01-14	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659250202111162228',
		'100DK10010000001248290202011250630',
		'100DK10010000000819000201908021008',
		'100DK10010000001222830202010280537'
		spider issue -1 values		
		Yes	Turki

		# solution
			# rerun

	(R D-1) 2561	2022-01-14	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001222710202010280537',
		'100DK10010000001222840202010280537',
		'100DK10010000001248330202011250630',
		'100DK10010000001659210202111162228',
		'100DK10010000001659240202111162228'
		invalid -3 delivery		
		Yes	Turki

		# solution
			# rerun

	(R D-1)2563	2022-01-14	New CMS	100	in_stock	seatronic.no	
		all Item_ids	
		ETL_FAILURE		
		Yes	Jing

		# solution
			# rerun

	(R D-1) 2572	2022-01-14	New CMS	200	all	amazon.it/3P	
		'200ITGF070000000557590201904220945',
		'200ITGF080000000556960201904220853',
		'200ITGF080000000801690201906210116',
		'200ITGF070000000557970201904221001'

		invalid deadlink		
		Yes	Mojo

		# solution
			# no issue

	-----

	(R D-1) 1528	2022-01-14	New CMS	Q00 	all	www.sportsshoes.com	
		Gore-Tex jackets women	
		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/26ddx4j		
		ailyn

		# solution
			# multiple reruns

	(R D-2) 1532	2022-01-14	New CMS	200	all	www.elkjop.no	
		'bluetooth ørepropper',
			134fcd48-a112-42c1-a395-caaea7e87e91@EGO2juO8EeaOEAANOrFolw

			JVC Øreplugger EBT5 Bluetooth In-Ear Blå
				https://prnt.sc/26df3ka
			JVC Øreplugger EBT5 Bluetooth In-Ear Rød
				https://prnt.sc/26df3zq
			JVC Øreplugger EBT5 Bluetooth In-Ear Rosa
				https://prnt.sc/26df43e
			JVC Øreplugger EBT5 Bluetooth In-Ear Svart
				https://prnt.sc/26df49w

		'høyttaler bluetooth'
			8751c1fa-12ed-4a08-b269-30c82e74e766@EGO2juO8EeaOEAANOrFolw

			JBL JR POP Bluetooth-høyttaler (blå)

		duplicate prod_url
		https://prnt.sc/26de93j
		https://prnt.sc/26deb33
		Yes	Yev

		# solution
			# rerun
			# product: 'JBL JR POP Bluetooth-høyttaler (blå)' duplicate in sql but not found in site

	(R D-1) 1534	2022-01-14	New CMS	200	all	www.dns-shop.ru	
		наушники накладные беспроводные с 
		bluetooth	

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26dejzg	
		Yes	Yev

		# solution
			# rerun

	(R D-1) 1535	2022-01-14	New CMS	200	all	www.dns-shop.ru	
		проводные наушники	

		Invalid Deadlink	
		https://prnt.sc/26dehm1	
		Yes	Yev

		# solution
			# rerun

	(R D-1) 1536	2022-01-14	New CMS	200	all	www.re-store.ru	
		акустика wi-fi
			b49d9544-7399-4fca-b188-152c384feddc@a4059226-f529-46c6-8a35-c120219fbbab
		НАУШНИКИ С ПРОВОДОМ
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab
		ПОРТАТИВНАЯ АКУСТИКА WI-FI
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 4 (Fetched Data is Lesser than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)

		https://prnt.sc/26deooq 8 
		https://prnt.sc/26denzp 20
		https://prnt.sc/26demqj 4
		Yes	Yev

		# solution
			# multiple reruns

	(R D-1) 1540	2022-01-14	New CMS	Q00 	all	www.webtogs.com	
		Hiking shoes men	
		Auto copyover 1 day 	
		https://prnt.sc/26dfhur	
		yes	Mich

		# solution

	(R D-1) 1541	2022-01-14	New CMS	Q00 	all	www.sport24.dk	
		skibukser dame	
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26devgj		
		renz

		# solution
			# rerun

	(F D-1) 1542	2022-01-14	New CMS	Q00 	all	www.friluftslageret.dk	
		'outdoor bukser dame',
		'outdoor bukser herre',
		'skalbukser herre',
		'vandresko herre',
		'vandtætte sko herre',
		'vindjakke herre'

		Data Count Mismatch 8 (Fetched Data is Lesser than expected)
		Data Count Mismatch 22 (Fetched Data is Greater than expected)
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)
		Data Count Mismatch 17 (Fetched Data is Greater than expected)
		Data Count Mismatch 9 (Fetched Data is Greater than expected)
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26dejw7
		https://prnt.sc/26deb9w
		https://prnt.sc/26dedr7
		https://prnt.sc/26deh0d
		https://prnt.sc/26deidu
		https://prnt.sc/26dej92
		
		renz

		# solution
			# multilple reruns failed
			# fixing
			# to test tomorrow

	(F D-1) 1544	2022-01-14	New CMS	Q00 	all	www.friluftslageret.dk	
		moccasin herre
		regnbukser herre
		Auto copy over 1 day	

		https://prnt.sc/26de4m5
		https://prnt.sc/26dec0m		
		renz
		
		# solution
			# multilple reruns failed
			# fixing
			# to test tomorrow

	(R D-1) 1546	2022-01-14	New CMS	Z00 	all	www.costco.com	
		Speaker	
		Auto copyover 1 day 	
		https://prnt.sc/26dggy4
		yes	Mich

		# solution
			# rerun

	(R D-1) 1547	2022-01-14	New CMS	Z00 	all	www.walmart.com
		home theater system	
		Invalid Deadlink	
		https://prnt.sc/26dgi3j	
		Yes	Kristian Angelo

		# solution
			# rerun

	(R D-1) 1548	2022-01-14	New CMS	Z00 	all	www.abt.com	
		'amp',
		'receiver',
		'soundbars',
		'stereo receiver',
		'wireless speaker'

		1 Day Auto Copy Over	

		https://prnt.sc/26dgmka
		https://prnt.sc/26dgnvn
		https://prnt.sc/26dgone
		https://prnt.sc/26dgpej
		https://prnt.sc/26dgppd	Yes	Kristian Angelo

		# solution
			# rerun

	(R D-1) 1549	2022-01-14	New CMS	200	all	www.bol.com	
		woonkamer speaker	
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26dgrgk	
		Yes	Yev

		# solution
			# rerun

	-----
	(R D-1) 542	2022-01-14	New CMS	Q00	all	www.neye.dk
		Backpack Outdoor
		Incorrect Scraped of Data
		https://prnt.sc/26df234
		YEs	Rayyan

		# solution
			# multiple reruns

	Stats:
		Rerun: 17 (18) = 1.06
		Fix: 2[1]

01 15 22
	(F D-3) Trello
		Retailer: www.friluftslageret.dk
		Issue: Auto copy over / Requires more time to fix

		keyword:
		moccasin herre
		regnbukser herre

		Screenshot:
		https://prnt.sc/26de4m5
		https://prnt.sc/26dec0m

		# solution
			# create api and update website strat checkers and extrators 
			# rerun

		---
		Retailer: www.friluftslageret.dk
		Issue:  data count missmatch / Requires more time to fix
		keyword:
		outdoor bukser dame
		outdoor bukser herre
		skalbukser herre
		vandresko herre
		vandtætte sko herre
		vindjakke herre

		Screenshot:
		https://prnt.sc/26dejw7
		https://prnt.sc/26deb9w
		https://prnt.sc/26dedr7
		https://prnt.sc/26deh0d
		https://prnt.sc/26deidu
		https://prnt.sc/26dej92

		# solution
			# create api and update website strat checkers and extrators
			# rerun

	(R D-1) 2578	2022-01-15	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659240202111162228',
		'100DK10010000000816740201908020830',
		'100DK10010000001222810202010280537'
		spider issue -1 values		
		Yes	Turki

		# solution
			# rerun

	(R D-1) 2579	2022-01-15	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001397640202105040251',
		'100DK10010000001400700202105040816',
		'100DK10010000001659720202111162228',
		'100DK10010000001659910202111162228'
		invalid -3 delivery		
		Yes	Turki

		# solution
			# rerun

	(R D-1) 2581	2022-01-15	New CMS	K00	price_value	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP807O000001449520202106150452',
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001449540202106150452',
		'K00RUP803F000001449400202106150452',
		'K00RUP80CG000001209310202009280432',
		'K00RUP804K000001527750202108020954',
		'K00RUP803F000001527390202108020954',
		'K00RUP80CG000001245150202011170413',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001245130202011170413',
		'K00RUP80CG000001524900202108020807',
		'K00RUP80CG000001524940202108020807',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000001373070202104050819',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001311010202101250600',
		'K00RUP80CG000001310810202101250600',
		'K00RUP80CG000000980380202006250902',
		'K00RUP80CG000001310990202101250600',
		'K00RUP80IO000001449640202106150452',
		'K00RUP805K000001527500202108020954',
		'K00RUP805K000001527540202108020954',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449670202106150452',
		'K00RUP80CG000001310930202101250600',
		'K00RUP80CG000001310950202101250600'
		wrong price fetched		
		Yes	Mayeie

		# solution
			# multiple reruns

	(R D-1) 2582	2022-01-15	New CMS	K00	rating_score mvideo.ru	
		'K00RUP803F000001527310202108020954',
		'K00RUP80CG000000980400202006250902',
		'K00RUP804K000001527200202108020954',
		'K00RUP804K000001527710202108020954',
		'K00RUP804K000001449090202106150452',
		'K00RUP804K000001528820202108020955',
		'K00RUP803F000001527440202108020954',
		'K00RUP803F000001449160202106150452',
		'K00RUP803F000001528840202108020955',
		'K00RUP80CG000001592510202109200556'

		wrong ratings fetched		
		Yes	Mayeie

		# solution
			# rerun

	(R D-1) 2593	2022-01-15	New CMS	100	all	hylte-lantman.com	
		'100SEDH010000001665450202111170530',
		'100SEDH010000001224180202010280943',
		'100SEDH010000000821310201908050828',
		'100SEDH010000001321260202102050343',
		'100SEDH010000001322730202102051007',
		'100SEDH010000001323030202102051007',
		'100SEDH010000001321380202102050343',
		'100SEDH010000000821960201908060213',
		'100SEDH010000001403100202105050324'
		incorrect scraping of title, affecting other essential data such Availabiliy	
		https://prnt.sc/26dvj8b
			Yes	Jeremy

		# Solution
			# rerun
	-----
	(R D-1) 1550	2022-01-15	New CMS	110	all	www.dreamland.be/fr	
		Nintendo Switch Lite	
		Data Count Mismatch 1 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26dtldb	
		Yes	richelle

		# solution
			# multiple reruns failed
			# goods in local
			# wont reflect in sql
			# block

	(R D-1) 1551	2022-01-15	New CMS	110	all	www.dreamland.be/nl	
		Nintendo Switch grijs	
		Data Count Mismatch 16 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26dtnjs	
		Yes	richelle

		# solution
			# multiple reruns failed
			# goods in local
			# wont reflect in sql
			# blocked

			# retry as request

	(R D-1) 1552	2022-01-15	New CMS	110	all	www.fnac.be/nl	
		Nintendo Switch grijs	
		1 Day Auto Copy Over	
		https://prnt.sc/26dtr6o	
		Yes	richelle

		# solution
			# rerun

	(R D-2) 1560	2022-01-15	New CMS	U00	all	www.magazineluiza.com.br
		'Nintendo Switch',
		'Pro Controller',
		'Joy Con',
		'Console Nintendo Switch',
		'Controle Nintendo Switch',
		'Console',
		'Controle sem fio Nintendo Switch',
		'Controle Joy con',
		'Console Switch',
		'Controle Switch',
		'Mini Video Game',
		'Aparelho de Game',
		'Aparelho de Video Game',
		'controle game sem fio'

		Auto Copy Over for 1 day
		Auto Copy Over for 3 days	

		https://prnt.sc/26dtuad
		https://prnt.sc/26dtwot	
		kurt

		# solution
			# multiple reruns failed
			# fixing
			# rebuilding
			# rerun

			# issue with 
				# Mini Video Game
				# Test this:
					/ Nintendo Switch
					/ Mini Video Game
					/ Mini game portatil	
				# rerun

	(F D-1) 1561	2022-01-15	New CMS	U00	all	www.magazineluiza.com.br	
		Mini game portatil	

		Invalid Deadlink	
		https://prnt.sc/26dtvxq		
		kurt

		# solution
			# multiple reruns failed
			# becomes autocopy over
			# fixing
			# rebulding
			# rerun

	(R D-1) 1562	2022-01-15	New CMS	U00	all	www.casasbahia.com.br	
		aparelho de video game	
		Auto Copy Over 1 day	
		https://prnt.sc/26dtxug		
		kurt

		# solution
			# rerun

	(R D-1) 1563	2022-01-15	New CMS	U00	all	www.bemol.com.br	
		aparelho de video game	
		Data Count Mismatch 3 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26duku1		
		kurt

		# solution
			# rerun

	(R D-1) 1566	2022-01-15	New CMS	X00	all	www.sears.com.mx	
		Consola Nintendo Lite	
		Invalid deadlink	
		https://prnt.sc/26dw4fw	
		Yes	richelle

		# solution 
			# rerun

	-----

	(R D-1) 545	2022-01-15	New CMS	200	all	www.saturn.de	
		Speakers Multiroom	
		Auto copy over 1 day	
		https://prnt.sc/26dtz1p	
		Yes	michael

		# solution
			# rerun

	(R D-1) 546	2022-01-15	New CMS	200	all	www.saturn.de	
		Speakers Bluetooth	
		mismatched data	
		https://prnt.sc/26dtzx2	
		Yes	michael

		# solution
			# rerun

	(R D-2) 547	2022-01-15	New CMS	X00	all	www.mercadolibre.com.mx	
		Consolas
			https://prnt.sc/26dvtgc
			https://prnt.sc/26dvtsa

		Software	
			https://prnt.sc/26dvugk
			https://prnt.sc/26dvuxl

		duplicate product_website		
		https://prnt.sc/26dvl9t
		https://prnt.sc/26dvka5	
		Yes	MICHAEL

		# solution
			# rerun
			# investigate

	Q00@Haglofs@12:00NN@10:30AM@Prod19
	100@Garmin@12:00NN@12:35AM@Prod1
	K00@Razer@11:00AM@11:25AM@Prod2
	110@Nintendo Benelux@12:00NN@12:25AM@Prod2
	U00@Nintendo Brazil@12:00NN@12:25AM@Prod2
	X00@Nintendo Mexico@12:00NN@12:25AM@Prod2
	200@Zound@11:00AM@11:50AM@Prod2

	Stats:
		Rerun: 15 (17) = 1.13
		Fix: 1[3] 1[1]

01 16 22
	(R D-2) 2597	2022-01-16	New CMS	P00	all	ozon.ru	
		'P00RUXZ01L000001575880202109060525',
		'P00RU1W01L000001454890202106220209',
		'P00RU1W01L00000145491020210622020'

		Spider Issue (-1)		
		Yes	Mark

		# solution
			# multiple reruns failed
			# https://www.ozon.ru/product/smartfon-motorola-moto-g30-6-128gb-chernyy-305536903/?asb=GIqqTDk9zj%252FPFVilNwR4FzWZBrQ7844XSdBSAZ2Jb78%253D&asb2=91siqwJlH4CMNqntZAbXqrTGUY_8UYT2Erhj5gFGbaY&sh=BqW3WgAAAA
				# where is the description? ask the validator
				# mark this as fixed for now and wait till the validator chats

	(R D-1) 2598	2022-01-16	New CMS	K00	all	dns-shop.ru	K00RUL803F000001527410202108020954	
		Invalid DeadLink		
		Yes	Mayeie

		# solution
			# rerun

	(R D-1) 2599	2022-01-16	New CMS	K00	price_value	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP804K000001527170202108020954',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980400202006250902',
		'K00RUP80CG000001524940202108020807',
		'K00RUP80CG000000980410202006250902',
		'K00RUP80CG000000980450202006250902',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000001524830202108020807',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001245170202011170413',
		'K00RUP80CG000000980440202006250902',
		'K00RUP80CG000001311010202101250600',
		'K00RUP80CG000001310950202101250600',
		'K00RUP80IO000001449640202106150452',
		'K00RUP805K000001527570202108020954',
		'K00RUP80YO000001449690202106150452',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449670202106150452'
		wrong price fetched		
		Yes	Mayeie

		# solution
			# rerun

	(R D-1) 2600	2022-01-16	New CMS	K00	rating_score	mvideo.ru	
		'K00RUP804K000001527200202108020954',
		'K00RUP804K000001449090202106150452',
		'K00RUP803F000001449360202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001527440202108020954',
		'K00RUP80CG000001592510202109200556',
		'K00RUP80CG000001393580202104280059',
		'K00RUP80CG000000980430202006250902',
		'K00RUP80CG000001310930202101250600'
		wrong ratings fetched		
		Yes	Mayeie

		'K00RUP80CG000000980430202006250902',
		'K00RUP80CG000001592510202109200556'

		# Solution
			# multiple reruns

	(R D-1) 2601	2022-01-16	New CMS	K00	all	mvideo.ru	
		'K00RUP803F000001527310202108020954',
		'K00RUP80CG000001245150202011170413',
		'K00RUP80CG000001524900202108020807'
		Spider Issue (-1)		
		Yes	Mayeie

		# solution
			# multiple reruns

	(R D-1) 2602	2022-01-16	New CMS	K00	all	alza.cz	
		K00CZ3605K000001527080202108020954	
		Spider Issue (-1)		
		Yes	Mayeie

		# solution
			# rerun\

	(R D-1) 2603	2022-01-16	New CMS	100	all	utomhusliv.se	
		100SETX010000001651080202111090812	
		Not_found in stock		
		Yes	Jeremy

		# solution
			# rerun

	(R D-1) 2604	2022-01-16	New CMS	100	all	utomhusliv.se	
		'100SETX010000001649410202111090812',
		'100SETX010000001649510202111090812',
		'100SETX010000001646820202111090741',
		'100SETX010000001650450202111090812',
		'100SETX010000001650990202111090812'
		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# rerun

	(R D-1) 2605	2022-01-16	New CMS	100	all	hylte-lantman.com	
		'100SEDH010000000821350201908050833',
		'100SEDH010000001224180202010280943',
		'100SEDH010000001323030202102051007',
		'100SEDH010000001321330202102050343',
		'100SEDH010000001224240202010280943',
		'100SEDH010000001224300202010280943',
		'100SEDH010000001403100202105050324'
		incorrect scraping of title, affecting other essential data such  Availabiliy	
		https://prnt.sc/26e5oyv	
		Yes	Jeremy

		# Solution
			# rerun

	(R D-1) 2606	2022-01-16	New CMS	P00	all	alta.cz	
		'P00CZ3601L000001429830202106010821',
		'P00CZ3601L000001429970202106010821',
		'P00CZ3601L000001429930202106010821',
		'P00CZ3601L000001429840202106010821',
		'P00CZ3601L000001429890202106010821',
		'P00CZ3601L000001477040202107120510'
		Spider Issues		
		Yes	Keeshia

		# solution
			# write jobs? 

 	(R D-1) 2611 2022-01-16	New CMS	200	delivery	elgiganten.dk	
		'200DK100B0000000031520201901180326',
		'200DK100C0000000039290201901180331',
		'200DK100G0000000031950201901180326',
		'200DK100O0000000036230201901180329'
		-3 DQ		
		Yes	Louie

		# solution
			# rerun

	(R D-1) 2612	2022-01-16	New CMS	200	all	amazon 3p	
		'200ESFF080000000557070201904220902',
		'200GBDF070000000557330201904220937'
		Invalid deadlink		
		Yes	Louie

		# solution
			# rerun

	(w) 2615	2022-01-16	New CMS	200	image_count		
		select * from view_all_productdata where date = '2022-01-16' and webshot_url is NULL	
		NO WEBSHOTS		
		Yes	Jeremy

		# solution
			# rerun webshot

	(w) 2616	2022-01-16	New CMS	K00	webshot_url		
		select * from view_all_productdata where date = '2022-01-16' and webshot_url is NULL	
		NO WEBSHOTS		
		Yes	TURKI

		# solution
			# rerun webshot

	(R D-1) 2617	2022-01-16	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000000814140201908020341',
		'100DK10010000001222780202010280537',
		'100DK10010000001248190202011250630',
		'100DK10010000001248260202011250630',
		'100DK10010000001324440202102090603',
		'100DK10010000001326810202102090604',
		'100DK10010000001397940202105040251',
		'100DK10010000001400700202105040816',
		'100DK10010000001659720202111162228',
		'100DK10010000001659910202111162228'
		invalid -3 delivery		
		Yes	TURKI

		# solution
			# rerun

	-----
	(R D-1) 1567	2022-01-16	New CMS	200	all	www.dns-shop.ru	
		портативная колонка с bluetooth	
		invalid deadlink	
		https://prnt.sc/26e578p		
		Phil

		# solution
			# multiple reruns failed
			# multiple rerun

	-----
	(R D-1) 548	2022-01-16	New CMS	100	all	www.seatronic.no	
		kartplottere	
		Invalid Deadlink	
		https://prnt.sc/26e5ko8	Yes	
		kurt

		# solution
			# rerun

	(R D-1) 549	2022-01-16	New CMS	200	all	www.amazon.fr	
		Speakers Portable	
		mismatched data	
		https://prnt.sc/26e5oxh	
		Yes	michael

		# solution
			# rerun

	P00@Motorola@11:00AM@11:25AM@Prod2
	K00@Razer@11:00AM@11:25AM@Prod2
	100@Garmin@12:00NN@12:35AM@Prod1
	200@Zound@11:00AM@11:50AM@Prod2

	Stats:
		Rerun: 16 (17/16) = 1.06
		Webshots: 2

01 19 22
	(R D-1) 2676	2022-01-19	New CMS	K00	title	www.dns-shop.ru	
		K00RUL80CG000000979820202006250707	
		Missing title		
		Yes	Jurena

		# solution
			# no action: time difference

	(R D-1) 2682	2022-01-19	New CMS	K00	all	www.alza.cz	
		K00CZ3604K000001531070202108030723
		K00CZ360CG000001486120202107130335	
		missing data		
		Yes	Charlemagne

		# solution 
			# rerun

	(R D-1) 2702	2022-01-19	New CMS	200	all	amazon 3p retailer	
		'200ITGF070000000801930201906210247',
		'200ITGF070000000557590201904220945',
		'200ITGF080000000557530201904220944',
		'200ITGF080000000557080201904220902',
		'200ITGF070000000557870201904220956',
		'200ITGF080000000801730201906210132',
		'200ITGF080000000801650201906200930',
		'200ITGF070000000557970201904221001',
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-19@22:00:00@200ITGF070000000801930201906210247,2022-01-19@22:00:00@200ITGF070000000557590201904220945,2022-01-19@22:00:00@200ITGF080000000557530201904220944,2022-01-19@22:00:00@200ITGF080000000557080201904220902,2022-01-19@22:00:00@200ITGF070000000557870201904220956,2022-01-19@22:00:00@200ITGF080000000801730201906210132,2022-01-19@22:00:00@200ITGF080000000801650201906200930,2022-01-19@22:00:00@200ITGF070000000557970201904221001,2022-01-19@22:00:00@200ESFF070000000557360201904220937,2022-01-19@23:00:00@200GBDF080000001398770202105040415

			# 200ITGF070000000557590201904220945
			# 200ITGF070000000557870201904220956
			# 200ITGF080000000801730201906210132

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-19@22:00:00@200ITGF070000000557590201904220945,2022-01-19@22:00:00@200ITGF070000000557870201904220956,2022-01-19@22:00:00@200ITGF080000000801730201906210132

		/ '200ESFF070000000557360201904220937',
			# 2022-01-19@22:00:00@200ESFF070000000557360201904220937

		/ '200GBDF080000001398770202105040415'
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-19@23:00:00@200GBDF080000001398770202105040415


		invalid deadlink		
		Yes	Louie

		# solution
			# multiple reruns

	(R D-1) 2703	2022-01-19	New CMS	200	in_stock	amazon.de	
		200DEY0070000001374740202104120408	
		wrong stock status		
		Yes	Louie

		https://www.amazon.de/Marshall-Major-Bluetooth-Faltbar-Kopfh%C3%B6rer-Schwartz/dp/B08KHT2HDT/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Marshall+Major+IV&qid=1617203397&sr=8-1

		2022-01-19@22:00:00@200DEY0070000001374740202104120408

		# solution
			# multiple reruns failed
			# fixed by Kenn: for rebuild
			# 


	(R D-1) 2704	2022-01-19	New CMS	200	rating_reviewers	amazon.it	
		'200ITL4070000000530740201903270425',
		'200ITL4070000000530780201903270510'
		wrong reviews		
		Yes	Louie

		# solution
			# rerun: time difference

	(R D-1) 2705	2022-01-19	New CMS	200	price_value	amazon.es	
		'200ESS4070000000531110201903270621',
		'200ESS4070000000530650201903270416',
		'200ESS4070000000530810201903270524',
		'200ESS4070000000891940201911290851',
		'200ESS4070000000891730201911290821',
		'200ESS4070000000531030201903270603',
		'200ESS4070000000530930201903270552',
		'200ESS4070000000530950201903270555',
		'200ESS4080000000531000201903270559',
		'200ESS4070000000531130201903270624',
		'200ESS4080000000530900201903270540',
		'200ESS4070000000531150201903270631'
		wrong price	
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-19@22:00:00@200ESS4070000000530650201903270416,2022-01-19@22:00:00@200ESS4070000000530810201903270524,2022-01-19@22:00:00@200ESS4080000000530900201903270540,2022-01-19@22:00:00@200ESS4070000000530930201903270552,2022-01-19@22:00:00@200ESS4070000000530950201903270555,2022-01-19@22:00:00@200ESS4080000000531000201903270559,2022-01-19@22:00:00@200ESS4070000000531030201903270603,2022-01-19@22:00:00@200ESS4070000000531110201903270621,2022-01-19@22:00:00@200ESS4070000000531130201903270624,2022-01-19@22:00:00@200ESS4070000000531150201903270631,2022-01-19@22:00:00@200ESS4070000000891730201911290821,2022-01-19@22:00:00@200ESS4070000000891940201911290851
			# multiple reruns failed

			# fixed by Kenn
			# for rebuild
			# rerun

	(R D-1) 2706	2022-01-19	New CMS	200	rating_reviewers	amazon.es	
		200ESS4070000000530730201903270425	
			https://www.amazon.es/Marshall-Major-Auriculares-Bluetooth-Plegables/dp/B07CMFBJM7/
			2022-01-19@22:00:00@200ESS4070000000530730201903270425
		wrong reviews		
		Yes	Louie

		# solution
			# rerun

	(R D-1) 2707	2022-01-19	New CMS	200	rating_reviewers	amazon.co.uk	
		/ '200GBG00C0000000104480201902071109',
		/ '200GBG0070000001397320202105040205',
		/ '200GBG0070000001474580202107060614',
		/ '200GBG0070000001474650202107060614'
			https://www.amazon.co.uk/d/Headphones-Earphones/Beats-Dr-Dre-Headphones-B0518/B00IYA2YRK
			https://www.amazon.co.uk/Marshall-Mode-True-Wireless-Headphones-Black/dp/B08YPBXDBX/ref=sr_1_1?dchild=1&keywords=Marshall+Mode+II&qid=1624875998&sr=8-1
			https://www.amazon.co.uk/Marshall-Minor-True-Wireless-Headphones-Black/dp/B09CHJDZYL/ref=sr_1_1?dchild=1&keywords=Minor+III&qid=1632277769&qsid=261-1445797-1160424&sr=8-1&sres=B09CHJDZYL%2CB002289PTU%2C1445621789%2CB09GLVKGGT%2CB00I2Q99GK%2CB001N5FJC0%2CB09GJLLVKD%2C0739018647%2C3795796628%2CB001N4ZOT4%2CB07J2Q4ZJ5%2CB08ZW46SWV%2CB0046TWL62%2C1974899497%2C0300755864%2CB087TVZWHW
			https://www.amazon.co.uk/Marshall-Motif-ANC-Cancelling-Headphones-Black/dp/B09CHH1Z4K/ref=sr_1_1?dchild=1&keywords=marshall+Motif+A.N.C&qid=1633354732&qsid=260-2451575-2790900&sr=8-1&sres=B09CHH1Z4K%2CB08YPBXDBX%2CB07HMTGJ3T%2CB08B951D35%2CB000BVN582%2CB06XVTKK2G%2CB09CHJDZYL%2CB07J324ZCP%2CB08RNMSVHS%2CB01MRCS4MJ%2CB004U5FAFG
		wrong reviews		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-19@23:00:00@200GBG00C0000000104480201902071109,2022-01-19@23:00:00@200GBG0070000001397320202105040205,2022-01-19@23:00:00@200GBG0070000001474580202107060614,2022-01-19@23:00:00@200GBG0070000001474650202107060614
			# rerun

	-----
	(R D-1) 1622	2022-01-19	New CMS	K00	all	fnac.com	
		Clavier de jeu
		Tapis de souris	
		Auto copy over fr a day	
		https://prnt.sc/26fdr9i
		https://prnt.sc/26fdpwl		
		michael

		# solution
			# rerun

	(R D-1)1623	2022-01-19	New CMS	K00	all	komplett.no
		ANC-hodetelefoner	
		Auto copy over fr a day	
		https://prnt.sc/26fdxoi		
		keen

		# solution
			# rerun

	(R D-1)1626	2022-01-19	New CMS	K00	all	notebooksbilliger.de	
		Gaming Keypad	
		Auto copy over fr a day	
		https://prnt.sc/26fe5rb	
		Yes	mcihael

		# solution
			# rerun

	(R D-1) 1627	2022-01-19	New CMS	K00	all	www.alza.cz	
		Podložky pod myši
		Auto copy over fr a day	
		https://prnt.sc/26fe7te	Yes	kurt

		# solution
			# multiple reruns

	(R D-1)1643	2022-01-19	New CMS	K00	all	citilink.ru	
		PS Контроллер	

		Data Count Mismatch 5 (Fetched Data is Greater than expected)	
		https://prnt.sc/26fe9dq		
		richelle

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-19@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g,2022-01-19@20:00:00@K00@f1507b98-92d9-48c1-8c97-792ef1348e84@8THUOEXdEeeMawANOiZi-g
			# rerun

	0 (R D-1) 1644	2022-01-19	New CMS	K00	all	citilink.ru	
		Контроллер PS4
			784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g
			2022-01-19@20:00:00@K00@784163a8-37a6-44d6-8cd6-e920d5da69bb@8THUOEXdEeeMawANOiZi-g
		Data Count Mismatch 5 (Fetched Data is Lesser than expected)	
		https://prnt.sc/26feb4v		
		richelle

		# solution
			# blocked in spiders
			# trello for now

	(R D-1) 1645	2022-01-19	New CMS	K00	all	dns-shop.ru	
		Динамики	
		Autocopy Over for 1 day and has -3 on Product website	
		https://prnt.sc/26ferkn		
		richelle

		# solution
			# rerun

	(R D-1) 1646	2022-01-19	New CMS	K00	all	dns-shop.ru
		НАУШНИКИ ANC
		Наушники ANC
			f122a9bc-031d-464a-8917-9b5f342cdeeb@BzxplkXeEeeMawANOiZi-g

		Invalid deadlink	
		https://prnt.sc/26fehyrv
		https://prnt.sc/26ff1dt		
		richelle

		# solution
			# multiple reruns

	(R D-3) 1647	2022-01-19	New CMS	K00	all	dns-shop.ru	
		БЕСПРОВОДНАЯ ГАРНИТУРА
			d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g
		ГАРНИТУРА ДЛЯ ПК
			4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g
		ИГРОВАЯ КЛАВИАТУРА
			f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		КЛАВИАТУРА
			5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		КЛАВИАТУРА ПК
			94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		МЕХАНИЧЕСКАЯ КЛАВИАТУРА
			2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		БЕСПРОВОДНАЯ МЫШЬ
			5751b825-32ac-4dd8-95c9-00bf2a9cc045@BzxplkXeEeeMawANOiZi-g
		ИГРОВАЯ МЫШЬ
			058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g
		ИГРОВОЙ НОУТБУК
			6e064f1e-acef-4b07-90a6-5739d6cb2d65@BzxplkXeEeeMawANOiZi-g
		Игровая клавиатура
			f457365a-0a80-4531-8a4c-b1af74da8b81@BzxplkXeEeeMawANOiZi-g
		Клавиатура ПК
			94ff95a9-3523-41d8-9312-1b8ea40ee402@BzxplkXeEeeMawANOiZi-g
		Клавиатура
			5ffb2ad0-d60d-419d-a616-33339be1e31b@BzxplkXeEeeMawANOiZi-g
		Механическая клавиатура
			2ec84d56-ef05-439f-8a5b-dbad56520108@BzxplkXeEeeMawANOiZi-g
		Игровые колонки
			9ac00807-dfb4-41be-82b7-97425f392504@BzxplkXeEeeMawANOiZi-g
		Игровой стул
			a1879757-f209-4cff-b47b-33f66ab50640@BzxplkXeEeeMawANOiZi-g
		Стул
			e0e53423-cf4b-48ea-8236-77b5ef68b695@BzxplkXeEeeMawANOiZi-g
		Контроллер Xbox
			bb7d5abe-c68b-4149-aa36-f8c7a69eb372@BzxplkXeEeeMawANOiZi-g
		Контроллер PS4
			784163a8-37a6-44d6-8cd6-e920d5da69bb@BzxplkXeEeeMawANOiZi-g
		Контроллер PlayStation
			f4b38090-8593-426e-a62f-630e83b51d8c@BzxplkXeEeeMawANOiZi-g
		Игровой коврик для мыши
			0cd67a2d-5384-4e8c-bdec-7a11a48c36e8@BzxplkXeEeeMawANOiZi-g
		Коврик для мыши
			9c92b1ac-b1a4-47a8-9ca8-9f0d5a8bbf8d@BzxplkXeEeeMawANOiZi-g
		наушники
			3084f275-d5b8-4a4c-9e7c-e2a2b5b38d89@BzxplkXeEeeMawANOiZi-g
		лучшие наушники	
			f97d2c2f-31cf-4a69-9a70-740cfe5f4ba4@BzxplkXeEeeMawANOiZi-g

		Autocopy Over fr 3 days

		Autocopy Over fr 1 day
		Autocopy Over fr 1 day
		Autocopy over fr 3 days
		Autocopy over fr 3 days
		Autocopy over fr 1 day
		Autocopy over fr 1 day
		Autocopy Over fr 2 days
		Autocopy Over fr 1 day
		Autocopy Over fr 1 day
		Autocopy Over fr 3 days
		Autocopy Over fr 3 days
		Autocopy Over fr 1 day
		Autocopy Over fr 3 days
		Autocopy Over fr 3 days
		Autocopy Over fr 1 day
		Autocopy Over fr 1 day
		Autocopy Over fr 3 days
		Autocopy Over fr 2 days
		Autocopy Over fr 2 days
		Autocopy Over fr 1 day
		Autocopy Over fr 2 days
		Autocopy Over fr 1 day	

		https://prnt.sc/26feew2
		https://prnt.sc/26fef5i
		https://prnt.sc/26fegds
		https://prnt.sc/26fei6j
		https://prnt.sc/26feinl
		https://prnt.sc/26feky6
		https://prnt.sc/26fem7kc
		https://prnt.sc/26fen7n
		https://prnt.sc/26fenvt
		https://prnt.sc/26feoab
		https://prnt.sc/26feoi9
		https://prnt.sc/26feozk
		https://prnt.sc/26fepat
		https://prnt.sc/26few3h
		https://prnt.sc/26fey4w
		https://prnt.sc/26fez41
		https://prnt.sc/26fezf9
		https://prnt.sc/26fezo7
		https://prnt.sc/26ff03e
		https://prnt.sc/26ff0g4
		https://prnt.sc/26ff0pr
		https://prnt.sc/26ff0yx
		https://prnt.sc/26ff1sc

		yes	richelle

		# solution
			# rerun

			# rerun this again
				# Гарнитура для ПК
					4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g
				# Беспроводная гарнитура
					d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g
				# Игровая мышь	
					058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g

				# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-19@20:00:00@K00@4f64e49e-a893-45e7-8496-3014f12574a5@BzxplkXeEeeMawANOiZi-g,2022-01-19@20:00:00@K00@d4f3a770-8590-41e9-82f1-28fc8a51fd2d@BzxplkXeEeeMawANOiZi-g,2022-01-19@20:00:00@K00@058373d8-5da5-4d72-8015-0f27efa98941@BzxplkXeEeeMawANOiZi-g

	-----
	(R D-1) 570	2022-01-19	New CMS	200	all	www.amazon.fr	
		Headphones Noise Cancelling	
		Auto copy over fr 1 day	
		https://prnt.sc/26fdmul	Yes	Rhunick

		# solution
			# rerun

	(R D-1) 571	2022-01-19	New CMS	200	all	www.elgiganten.se	
		Headphones Noise Cancelling 12
		Speakers Multiroom	5

		Data Count Mismatch (Fetched Data is lesser than expected)	

		https://prnt.sc/26fdx2g	
		Yes	Rhunick

		# solution
			# multiple reruns failed
			# rerun: time difference
	
	(R D-1) 572	2022-01-19	New CMS	200	all	www.re-store.ru	
		/ headphones wired in ear 8
			200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f
		/ speakers voice	3
			200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd


		Data Count Mismatch 2 (Fetched Data is lesser than expected)	

		https://prnt.sc/26fdvh1
		https://prnt.sc/26fdvrl	
		Yes	kurt

		# solution
			# multiple reruns failed
			# blocked
			# rerun

	Stats:
		Rerun: 20 (22/20) = 1.1
	
01 20 22
	(R D-1) 2739	2022-01-20	New CMS	200	all	re-store.ru	
		200RULQ070000000906770202001290622	
			https://www.re-store.ru/catalog/1001906/
		Invalid dEadlink		
		Yes	Mojo

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-20@21:00:00@200RULQ070000000906770202001290622
			# rerun

	(R D-1) 2743	2022-01-20	New CMS	K00	all	power.dk	
		K00DKX20CG000001487220202107130336
		K00DKX20CG000001582810202109130551
		K00DKX20CG000001487640202107130336
		K00DKX205K000001554360202108180340
		spider issue -1 values		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@K00DKX20CG000001487220202107130336,2022-01-20@22:00:00@K00DKX20CG000001582810202109130551,2022-01-20@22:00:00@K00DKX20CG000001487640202107130336,2022-01-20@22:00:00@K00DKX205K000001554360202108180340

			# multiple reruns failed, requires checking in local

	(R D-1) 2744	2022-01-20	New CMS	K00	all	fnac.com	
		K00FR2103F000001306610202101210448	
		invalid deadlink		
		Yes	Arfen

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-20@22:00:00@K00FR2103F000001306610202101210448
			# rerun

	(w) 2745	2022-01-20	New CMS	K00	webshot_url		
		select * from view_all_productdata where date = '2021-1-20' and webshot_url is NULL	no webshot 		
		Yes	Arfen

	-----
	(R D-1) 1652	2022-01-20	New CMS	O00	all	www.coolblue.nl	
		wasmachine aanbieding
			1ee54418-6634-4a69-82e2-707fa845c84d@dyBnppOqEei-eQANOiOHiA
		NEO QLED
			70ef8b5c-7f58-4c4d-ac71-81cd13ddbfd4@dyBnppOqEei-eQANOiOHiA
		Wifi soundbar
			f625fd7d-9490-4204-b1be-dc5091acbe71@dyBnppOqEei-eQANOiOHiA

		duplicate product_website	
		https://prnt.sc/26fyzp3
		https://prnt.sc/26fyxoi
		https://prnt.sc/26fz10e		

		ailyn

		# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@O00@1ee54418-6634-4a69-82e2-707fa845c84d@dyBnppOqEei-eQANOiOHiA,2022-01-20@22:00:00@O00@70ef8b5c-7f58-4c4d-ac71-81cd13ddbfd4@dyBnppOqEei-eQANOiOHiA,2022-01-20@22:00:00@O00@f625fd7d-9490-4204-b1be-dc5091acbe71@dyBnppOqEei-eQANOiOHiA

		# solution
			# rerun

	(R D-1) 1653	2022-01-20	New CMS	O00	all	www.bol.com	
		Autodose	
			51df8bf3-e3d8-4ef8-b648-dc435d586116@UAjszpOqEei-eQANOiOHiA
		Auto Copy Over fr 1 Day	
		https://prnt.sc/26fz9hr	
		keen

		# solution
			# rerun

	(R D-1) 1654	2022-01-20	New CMS	O00	all	www.bol.com	
		was-droogcombinatie	
			# b78aa926-43ea-4c10-bdc0-943201617b2c@UAjszpOqEei-eQANOiOHiA
		duplicate product_website	
		https://prnt.sc/26fz8qv		
		keen

		# solution
			# rerun

	(R D-1) 1658	2022-01-20	New CMS	200	all	www.dns-shop.ru
		/ наушники вкладыши проводные
			41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g
			python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-20@21:00:00@200@41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g
		портативная колонк
			6889e93b-60e8-4b27-a39e-690c8afbe53d@BzxplkXeEeeMawANOiZi-g

		Data Count Mismatch 7 (Fetched Data is Lesser than expected)

		https://prnt.sc/26g04pk
		Yes	Rayyan

		# solution 
			# портативная колонк
				# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@21:00:00@200@6889e93b-60e8-4b27-a39e-690c8afbe53d@BzxplkXeEeeMawANOiZi-g
				# rerun

	(R D-1) 1659	2022-01-20	New CMS	200	all	www.mediamarkt.de	
		bluetooth kopfhörer anc
			f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA
		kopfhörer anc
			f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@M3YPUApIEem-gAANOiOHiA
		lautsprecher bluetooth
			e5ca8a90-3052-4c6a-8d17-be2f285b3aa2@M3YPUApIEem-gAANOiOHiA	

		duplicate product_website & product_url		
		https://prnt.sc/26fzun7		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@f57a5238-de1f-48fe-9781-b8908d0f6342@M3YPUApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@f03d4bd3-7ab6-40a1-811e-2543d3aaa7c4@M3YPUApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@e5ca8a90-3052-4c6a-8d17-be2f285b3aa2@M3YPUApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@e5ca8a90-3052-4c6a-8d17-be2f285b3aa2@M3YPUApIEem-gAANOiOHiA
			# rerun

	(R D-2) 1660	2022-01-20	New CMS	200	all	www.saturn.de	
		/ amazon lautsprecher
			a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA
		/ bluetooth kopfhörer
			9f66bde9-a88f-40a3-9970-1c4305890a7c@UbbyaApIEem-gAANOiOHiA
		/ kabellose kopfhörer
			986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA
		/ kopfhörer bluetooth
			bbc80f13-6ef7-4837-925a-128b2119fcdc@UbbyaApIEem-gAANOiOHiA
		/ lautsprecher	
			32528cf1-d6f4-4ea8-b15c-017d43e90b1d@UbbyaApIEem-gAANOiOHiA

		duplicate product_website & product_url		
		https://prnt.sc/26g022s		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@9f66bde9-a88f-40a3-9970-1c4305890a7c@UbbyaApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@bbc80f13-6ef7-4837-925a-128b2119fcdc@UbbyaApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@32528cf1-d6f4-4ea8-b15c-017d43e90b1d@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@986d3596-754d-4124-8368-d782c997b2d8@UbbyaApIEem-gAANOiOHiA,2022-01-20@22:00:00@200@32528cf1-d6f4-4ea8-b15c-017d43e90b1d@UbbyaApIEem-gAANOiOHiA

			# rerun

	(R D-2) 1661	2022-01-20	New CMS	200	all	www.elkjop.no	
		lyddempende hodetelefoner
			15c89f57-8b35-4f19-8afd-87618ffe7c3c@EGO2juO8EeaOEAANOrFolw
		smarthøyttaler
			86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw
		støydempende headset
			403ca6ce-4b62-48ea-ab28-272f8b1b6002@EGO2juO8EeaOEAANOrFolw
		trådløse hodetelefoner
			cTj0UE4NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw
		trådløse øreplugger
			2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw

		duplicate product_website & product_url		
		https://prnt.sc/26g0cm3		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@15c89f57-8b35-4f19-8afd-87618ffe7c3c@EGO2juO8EeaOEAANOrFolw,2022-01-20@22:00:00@200@86341c70-7e73-4fe7-9fc8-bd307b95cdc4@EGO2juO8EeaOEAANOrFolw,2022-01-20@22:00:00@200@403ca6ce-4b62-48ea-ab28-272f8b1b6002@EGO2juO8EeaOEAANOrFolw,2022-01-20@22:00:00@200@cTj0UE4NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw,2022-01-20@22:00:00@200@2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@cTj0UE4NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw,2022-01-20@22:00:00@200@2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-20@22:00:00@200@2e4706b1-98b4-4a03-98a7-bc424e4ef5d5@EGO2juO8EeaOEAANOrFolw
			# rerun

	(R D-1) 1662	2022-01-20	New CMS	200	all	www.elgiganten.se	
		bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw
		in ear hörlurar
			4be825a7-7cc4-456b-bbaa-7967fe2fa31b@fVQR1tMrEeaOEAANOrFolw
		portabel högtalare
			6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		trådlösa hörlurar in ear	
			47580afb-3c40-4e7e-bdba-a84ee624f879@fVQR1tMrEeaOEAANOrFolw

		duplicate product_website & product_url		
		https://prnt.sc/26g0j36		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw,2022-01-20@22:00:00@200@4be825a7-7cc4-456b-bbaa-7967fe2fa31b@fVQR1tMrEeaOEAANOrFolw,2022-01-20@22:00:00@200@6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw,2022-01-20@22:00:00@200@47580afb-3c40-4e7e-bdba-a84ee624f879@fVQR1tMrEeaOEAANOrFolw
			# rerun

	(R D-1) 1663	2022-01-20	New CMS	200	all	www.bol.com	
		bedraad hoofdtelefoon
			69f1c9e4-a58a-4df6-98ca-b94327a408db@UAjszpOqEei-eQANOiOHiA

		duplicate product_website & product_url		
		https://prnt.sc/26g0zwx		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@69f1c9e4-a58a-4df6-98ca-b94327a408db@UAjszpOqEei-eQANOiOHiA
			# rerun

	(R D-1) 1664	2022-01-20	New CMS	200	all	www.coolblue.nl	
		Home speaker	
		duplicate product_website & product_url		
		https://prnt.sc/26g11ak		
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-20@22:00:00@200@ef8bb1f1-95c0-407f-8d90-a122d708b0ae@dyBnppOqEei-eQANOiOHiA
			# rerun
	-----

	Stats:
		Rerun: 13 (15/13) = 1.15
		Webshots: 1

01 21 22
	(R D-1) 2747	2022-01-21	New CMS	100	all	elgiganten.dk	
		'100DK10010000001659250202111162228',
		'100DK10010000001659240202111162228',
		'100DK10010000001659210202111162228',
		'100DK10010000001660410202111162228',
		'100DK10010000001659820202111162228',
		'100DK10010000001222710202010280537',
		'100DK10010000001659670202111162228'

		spider issue -1 values		
		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@100DK10010000001659250202111162228,2022-01-21@22:00:00@100DK10010000001659240202111162228,2022-01-21@22:00:00@100DK10010000001659210202111162228,2022-01-21@22:00:00@100DK10010000001660410202111162228,2022-01-21@22:00:00@100DK10010000001659820202111162228,2022-01-21@22:00:00@100DK10010000001222710202010280537,2022-01-21@22:00:00@100DK10010000001659670202111162228

			# rerun

	(R D-1)2748	2022-01-21	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001397490202105040251',
		'100DK10010000001413660202105130512',
		'100DK10010000001659190202111162228'
		-3 delivery
		Yes	Turki

		# solution	
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@100DK10010000001397490202105040251,2022-01-21@22:00:00@100DK10010000001413660202105130512,2022-01-21@22:00:00@100DK10010000001659190202111162228

			# rerun

	(R D-1) 2750	2022-01-21	New CMS	Q00	image_count	bergfreunde.de	
		'Q00DEDW0IJ000001551390202108170817',
			https://www.bergfreunde.de/hagloefs-tight-medium-20-daypack/
		'Q00DEDW0IJ000001577320202109080311',
			https://www.bergfreunde.de/hagloefs-lava-30-reisetasche/
		'Q00DEDW0IJ000001577330202109080311'
			https://www.bergfreunde.de/hagloefs-lava-50-reisetasche/

		Incorrect image_count		
		Yes	Neil

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@Q00DEDW0IJ000001551390202108170817,2022-01-21@22:00:00@Q00DEDW0IJ000001577320202109080311,2022-01-21@22:00:00@Q00DEDW0IJ000001577330202109080311

			# 

	-----

	(R D-1) 1670	2022-01-21	New CMS	Q00	all	www.xxl.no	
		Vindjakke menn	
			dd769e97-6066-4199-8d4c-eff549c59049@pJKIWgWrEei-cgANOiOHiA

		Mismatched data count 2 (Fetched Data is Greater than expected)	
		https://prnt.sc/26gm7l3
		yes	Mich

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@Q00@dd769e97-6066-4199-8d4c-eff549c59049@pJKIWgWrEei-cgANOiOHiA
			# rerun

	(R D-1) 1672	2022-01-21	New CMS	Q00	all	www.johnlewis.com	
		Shell jackets men	

		Auto copy over fr 1 day	
		https://prnt.sc/26gmjx9
		Yes	renz

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-21@23:00:00@Q00@f459d4bd-f767-4ef3-af8d-85b850b86b5e@GWJcVrlSEeem0wANOiOHiA

			# rerun

	(F D-3) 1673	2022-01-21	New CMS	Q00	all	www.sport24.dk	
		'gore-tex sko herre',
		'Hikingsko herre',
		'letvægtsjakke dame',
		'letvægtsjakke herre',
		'moccasin herre',
		'outdoor bukser dame',
		'outdoor bukser herre',
		'regnbukser dame',
		'regnbukser herre',
		'regnjakker dame',
		'regnjakker herre',
		'skalbukser dame',
		'skalbukser herre',
		'skaljakke dame',
		'skaljakke herre',
		'skibukser dame',
		'skibukser herre',
		'skijakke dame',
		'skijakke herre',
		'vandrebuks dame',
		'vandrebuks herre',
		'vandresko herre',
		'vandtætte sko herre',
		'vindjakke dame',
		'vindjakke herre'

		Wrong Url-1 prod_url/prod_website	

		https://prnt.sc/26gmh4u	Yes	renz

		# solution
			'007ec0c6-f75c-44c1-aa42-348e74de49b2@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'1ade8966-3387-4bd3-86e3-ca9bcc98fa3a@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'1e538439-ab05-4470-8801-b58a7acba7a7@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'314b4a95-381e-4832-8257-601502bc8357@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'3a2e1ff9-d024-47e2-b24b-7b790a28c889@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'41b4dda1-97a1-45f4-9fd0-f06218f847de@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'46482125-8a22-4f39-8f1d-46458907e5d8@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'4b77cfb6-b617-4dba-b08c-ab7a0b5a9b15@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'536b0451-fd40-4f31-9491-7bfb94645f8e@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'5fb8d8da-a688-4aaa-a7c0-79800ed15824@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'61a3a1b7-b591-4341-8422-2a198ca1d566@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'61fc667f-110c-4310-95e5-73fef7b37e98@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'6386e20b-57bd-4c2a-9d31-c1ee63d6bd1d@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'68dcf888-5143-48f6-b7ff-6e8fd64a1527@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'6e716d04-f248-446e-ace2-e353e12d7f2f@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'92b5946e-ee0b-42d6-8727-b563f5d69984@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'932eef87-c295-4617-b590-1e224c83b4a4@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'93f5e519-1dfc-4cdb-8f68-bab1b744cc0b@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'99b3b8d8-0072-4de1-b3e3-f1831ac2fdcc@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'a8d42e87-7d26-4384-b693-d90429c8fb94@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'bc7bd64e-e2e4-4332-9015-7b57b40ef9a1@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'be88aabd-33be-473a-9526-43df471caf09@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'd2b1e8a1-e4e0-4fa0-aae7-28a2aa498ed9@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'dbdfe948-bee3-4e83-b5fe-c6e79fa1a591@c06efc90-9919-4e56-8c05-2be6eff1bd16',
			'f7e1029a-c6d4-4e1a-a933-e3c58f89dfa2@c06efc90-9919-4e56-8c05-2be6eff1bd16'

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@Q00@007ec0c6-f75c-44c1-aa42-348e74de49b2@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@1ade8966-3387-4bd3-86e3-ca9bcc98fa3a@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@1e538439-ab05-4470-8801-b58a7acba7a7@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@314b4a95-381e-4832-8257-601502bc8357@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@3a2e1ff9-d024-47e2-b24b-7b790a28c889@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@41b4dda1-97a1-45f4-9fd0-f06218f847de@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@46482125-8a22-4f39-8f1d-46458907e5d8@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@4b77cfb6-b617-4dba-b08c-ab7a0b5a9b15@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@536b0451-fd40-4f31-9491-7bfb94645f8e@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@5fb8d8da-a688-4aaa-a7c0-79800ed15824@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@61a3a1b7-b591-4341-8422-2a198ca1d566@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@61fc667f-110c-4310-95e5-73fef7b37e98@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@6386e20b-57bd-4c2a-9d31-c1ee63d6bd1d@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@68dcf888-5143-48f6-b7ff-6e8fd64a1527@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@6e716d04-f248-446e-ace2-e353e12d7f2f@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@92b5946e-ee0b-42d6-8727-b563f5d69984@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@932eef87-c295-4617-b590-1e224c83b4a4@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@93f5e519-1dfc-4cdb-8f68-bab1b744cc0b@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@99b3b8d8-0072-4de1-b3e3-f1831ac2fdcc@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@a8d42e87-7d26-4384-b693-d90429c8fb94@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@bc7bd64e-e2e4-4332-9015-7b57b40ef9a1@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@be88aabd-33be-473a-9526-43df471caf09@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@d2b1e8a1-e4e0-4fa0-aae7-28a2aa498ed9@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@dbdfe948-bee3-4e83-b5fe-c6e79fa1a591@c06efc90-9919-4e56-8c05-2be6eff1bd16,2022-01-21@22:00:00@Q00@f7e1029a-c6d4-4e1a-a933-e3c58f89dfa2@c06efc90-9919-4e56-8c05-2be6eff1bd16

			# multiple reruns failed
			# fixing
				# fixed but not tested on woking keywords
			# for rebuild
			# for rerun

			# this failed
				'skibukser dame',
				'skijakke dame',
				'skijakke herre',
				'skibukser herre'

	(R D-2) 1674	2022-01-21	New CMS	Q00	all	www.friluftslageret.dk	
		'Hikingsko herre',
		'letvægtsjakke dame',
		'regnbukser dame',
		'regnjakker dame',
		'skaljakke dame',
		'skijakke dame',
		'vandrebuks dame',
		'vandrebuks herre',
		'vindjakke dame'

		Invalid Deadlink	

		https://prnt.sc/26gm6cl
		https://prnt.sc/26gm5lq
		https://prnt.sc/26gm4ow
		https://prnt.sc/26gm6wv
		https://prnt.sc/26gm9oe
		https://prnt.sc/26gmai3
		https://prnt.sc/26gmb5f
		https://prnt.sc/26gmbia
		https://prnt.sc/26gmc4o	

		Yes	renz

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-21@22:00:00@Q00@007ec0c6-f75c-44c1-aa42-348e74de49b2@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@5fb8d8da-a688-4aaa-a7c0-79800ed15824@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@61a3a1b7-b591-4341-8422-2a198ca1d566@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@6386e20b-57bd-4c2a-9d31-c1ee63d6bd1d@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@932eef87-c295-4617-b590-1e224c83b4a4@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@93f5e519-1dfc-4cdb-8f68-bab1b744cc0b@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@99b3b8d8-0072-4de1-b3e3-f1831ac2fdcc@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@a8d42e87-7d26-4384-b693-d90429c8fb94@796c817b-3b04-44b2-ac03-ef13a00079f8,2022-01-21@22:00:00@Q00@be88aabd-33be-473a-9526-43df471caf09@796c817b-3b04-44b2-ac03-ef13a00079f8

			'007ec0c6-f75c-44c1-aa42-348e74de49b2@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'5fb8d8da-a688-4aaa-a7c0-79800ed15824@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'61a3a1b7-b591-4341-8422-2a198ca1d566@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'6386e20b-57bd-4c2a-9d31-c1ee63d6bd1d@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'932eef87-c295-4617-b590-1e224c83b4a4@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'93f5e519-1dfc-4cdb-8f68-bab1b744cc0b@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'99b3b8d8-0072-4de1-b3e3-f1831ac2fdcc@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'a8d42e87-7d26-4384-b693-d90429c8fb94@796c817b-3b04-44b2-ac03-ef13a00079f8',
			'be88aabd-33be-473a-9526-43df471caf09@796c817b-3b04-44b2-ac03-ef13a00079f8'

			# multiple reruns failed
			# checking local: add keyword that utilizes api
			# for rebuild 5pm
				# rerun

	(R D-1) 1683	2022-01-21	New CMS	200	all	www.target.com	
		SMART SPEAKER
			30f9dc2a-2a90-4381-8bc4-37deb248b8f4@0l0XDL-yEei-eQANOiOHiA
		WIRELESS BLUETOOTH EARBUDS
			58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA

		duplicate product_website & product_url		
		https://prnt.sc/26gpb4n
		Christopher

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-21@04:00:00@200@30f9dc2a-2a90-4381-8bc4-37deb248b8f4@0l0XDL-yEei-eQANOiOHiA, 2022-01-21@04:00:00@200@58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-21@04:00:00@200@58117609-86fd-409a-97fb-fe757a6b0a47@0l0XDL-yEei-eQANOiOHiA

			# rerun

	-----
	(F D-1) 577	2022-01-21	New CMS	Q00	all	www.lannasport.se
		Herrbyxor
			Q00@KU0@6f3da90d-2d4b-43dc-9e6b-e82faa4ee1ad
			https://www.lannasport.se/sv/articles/3231/byxor?tag207=204
		Herrskor
			Q00@KU0@0fedafaa-6cfb-4543-bca1-d35f9575661b
			https://www.lannasport.se/sv/articles/3172/skor?tag207=204

		Data Count Mismatch 1(Fetched Data is Lesser than expected 

		https://prnt.sc/26glsie
		Yes	Rayyan

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@Q00@KU0@6f3da90d-2d4b-43dc-9e6b-e82faa4ee1ad, 2022-01-21@22:00:00@Q00@KU0@0fedafaa-6cfb-4543-bca1-d35f9575661b

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-21@22:00:00@Q00@KU0@0fedafaa-6cfb-4543-bca1-d35f9575661b
			# checking local: didnt proceed to next page
			# fixed, added new link indicators 
			# for rebuild
			# rerun 3:15 pm
			# for rerun
			# rerun

	(F D-1) 578	2022-01-21	New CMS	200	all	www.fnac.com	
		Headphones Wired In Ear
			https://www.fnac.com/Ecouteur-intra-auriculaire-Sport/Ecouteur-par-usage/nsh450542/w-4#bl=SONCasque,-%C3%A9couteurARBO
		Invalid Deadlink		
		Yes	Jairus

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-21@22:00:00@200@210@ce58e593-1df6-4099-8c2d-ecb5f0806f11

			# multiple reruns failed
			# checking local: dl start requester issue
			# for rebuild
			# for rerun
			# rerun

	(R D-1) 580	2022-01-21	New CMS	Z00	all	www.amazon.com.mx
		'components',
		'home theater - soundbars',
		'portable',
		'speaker - wireless audio'

		Data Count Mismatch 1(Fetched Data is Lesser than expected 
		https://prnt.sc/26gr9aa
		Yes	Yev

		# solution
			# assisted by sir jave
				# rerun after rebuild by sir jave

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-21@05:00:00@Z00@VC0@fc7a05eb-0d89-4a9a-b1cc-97c68498357c,2022-01-21@05:00:00@Z00@VC0@6de9a7c4-e4ca-4300-b490-2ce85f28178b,2022-01-21@05:00:00@Z00@VC0@b116bf86-f993-49b6-938b-d79e1fca3a39,2022-01-21@05:00:00@Z00@VC0@0e98a6c4-990e-48af-afd5-9544995e042d

			# rerun

	Q00@Haglofs@10:30AM@Prod1
	200@Zound@11:00AM@11:50AM@Prod2

	Stats:
		Rerun: 8 (9/8) = 1.13
		Fix: 2[1] 1[3]

01 22 22
	(F D-2) Trello
		/ push 1673

	-----

	(R D-1) 2765	2022-01-22	New CMS	100	all	elgiganten.dk	
		'100DK10010000001248260202011250630',
		'100DK10010000001438730202106080416',
		'100DK10010000001659250202111162228',
		'100DK10010000001659190202111162228',
		'100DK10010000001400740202105040816',
		'100DK10010000001659820202111162228',
		'100DK10010000001222710202010280537',
		'100DK10010000001326370202102090603',
		'100DK10010000001397790202105040251'

		invalid deadlinks	
		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@100DK10010000001248260202011250630,2022-01-22@22:00:00@100DK10010000001438730202106080416,2022-01-22@22:00:00@100DK10010000001659250202111162228,2022-01-22@22:00:00@100DK10010000001659190202111162228,2022-01-22@22:00:00@100DK10010000001400740202105040816,2022-01-22@22:00:00@100DK10010000001659820202111162228,2022-01-22@22:00:00@100DK10010000001222710202010280537,2022-01-22@22:00:00@100DK10010000001326370202102090603,2022-01-22@22:00:00@100DK10010000001397790202105040251

			# rerun

	(R D-1) 2766	2022-01-22	New CMS	110	specifications	amazon.nl	
		110NLWQ0RV020211109092304742931313	
		Missing Specifications		
		Yes	Switzell

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@110NLWQ0RV020211109092304742931313
			# rerun

	(R D-1) 2767	2022-01-22	New CMS	110	specifications	broze.be	
		110BEA11RV020211206101659610100340
			# https://broze.be/web/BrozeWSite.nsf/ArticleByRef/SWT2520347?OpenDocument
		110BEA11RV020211206101659770379340
			# https://broze.be/web/BrozeWSite.nsf/ArticleByRef/SWT2520747?OpenDocument	

		Missing Specifications		
		Yes	Switzell

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@110BEA11RV020211206101659610100340, 2022-01-22@22:00:00@110BEA11RV020211206101659770379340

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-22@22:00:00@110BEA11RV020211206101659610100340
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@110BEA11RV020211206101659770379340

			# multiple reruns failed

			# checking site: https://prnt.sc/26h9wda
			# valid no attribute


	(R D-1) 2769	2022-01-22	New CMS	100	all	hylte-lantman.com
		'100SEDH010000000821270201908050821',
		'100SEDH010000001224180202010280943',
		'100SEDH010000001224190202010280943',
		'100SEDH010000000821310201908050828',
		'100SEDH010000001323030202102051007',
		'100SEDH010000001320500202102040906'

		incorrect scraping of title, affecting other essential data such as Availabiliy	

		https://prnt.sc/26h6pf9	
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@100SEDH010000000821270201908050821,2022-01-22@22:00:00@100SEDH010000001224180202010280943,2022-01-22@22:00:00@100SEDH010000001224190202010280943,2022-01-22@22:00:00@100SEDH010000000821310201908050828,2022-01-22@22:00:00@100SEDH010000001323030202102051007,2022-01-22@22:00:00@100SEDH010000001320500202102040906

			# rerun

	(R D-1) 2770	2022-01-22	New CMS	100	all	power.se	
		100SEY2010000001228220202010290705	
		Invalid DeadLink		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@100SEY2010000001228220202010290705

			# rerun

	(R D-1) 2771	2022-01-22	New CMS	K00	price_value	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP80AD000001449550202106150452',
		'K00RUP80AD000001449540202106150452',
		'K00RUP80AD000001527640202108020954',
		'K00RUP803F000001449400202106150452',
		'K00RUP803F000001449160202106150452',
		'K00RUP803F000001527340202108020954',
		'K00RUP803F000001527440202108020954',
		'K00RUP803F000001449380202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001245150202011170413',
		'K00RUP80CG000001373070202104050819',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80YO000001449660202106150452',
		'K00RUP80YO000001449690202106150452',
		'K00RUP805K000001527540202108020954',
		'K00RUP80IO000001449640202106150452',
		'K00RUP80CG000001310990202101250600',
		'K00RUP80CG000001310930202101250600',
		'K00RUP80CG000000980440202006250902'
		wrong price fetched		
		Yes	Mayewwe

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@21:00:00@K00RUP805O000001449050202106150452,2022-01-22@21:00:00@K00RUP80AD000001449550202106150452,2022-01-22@21:00:00@K00RUP80AD000001449540202106150452,2022-01-22@21:00:00@K00RUP80AD000001527640202108020954,2022-01-22@21:00:00@K00RUP803F000001449400202106150452,2022-01-22@21:00:00@K00RUP803F000001449160202106150452,2022-01-22@21:00:00@K00RUP803F000001527340202108020954,2022-01-22@21:00:00@K00RUP803F000001527440202108020954,2022-01-22@21:00:00@K00RUP803F000001449380202106150452,2022-01-22@21:00:00@K00RUP80DO000001449630202106150452,2022-01-22@21:00:00@K00RUP80CG000000980370202006250902,2022-01-22@21:00:00@K00RUP80CG000001245150202011170413,2022-01-22@21:00:00@K00RUP80CG000001373070202104050819,2022-01-22@21:00:00@K00RUP80CG000001310970202101250600,2022-01-22@21:00:00@K00RUP80YO000001449660202106150452,2022-01-22@21:00:00@K00RUP80YO000001449690202106150452,2022-01-22@21:00:00@K00RUP805K000001527540202108020954,2022-01-22@21:00:00@K00RUP80IO000001449640202106150452,2022-01-22@21:00:00@K00RUP80CG000001310990202101250600,2022-01-22@21:00:00@K00RUP80CG000001310930202101250600,2022-01-22@21:00:00@K00RUP80CG000000980440202006250902

			# https://www.mvideo.ru/products/igrovaya-mysh-razer-basilisk-ultimate-rz01-03170100-r3g1-50134223

			# multiple reruns

	(R D-1) 2772	2022-01-22	New CMS	K00	rating_score	mvideo.ru
		'K00RUP804K000001527250202108020954',
		'K00RUP803F000001449360202106150452',
		'K00RUP803F000001449420202106150452',
		'K00RUP803F000001527310202108020954',
		'K00RUP80CG000001311070202101250600',
		'K00RUP80CG000000980430202006250902'
		wrong ratings fetched		
		Yes	Mayewwe

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@21:00:00@K00RUP804K000001527250202108020954,2022-01-22@21:00:00@K00RUP803F000001449360202106150452,2022-01-22@21:00:00@K00RUP803F000001449420202106150452,2022-01-22@21:00:00@K00RUP803F000001527310202108020954,2022-01-22@21:00:00@K00RUP80CG000001311070202101250600,2022-01-22@21:00:00@K00RUP80CG000000980430202006250902

			# rerun

	(R D-1) 2773	2022-01-22	New CMS	K00	all	mvideo.ru	
		'K00RUP804K000001527710202108020954',
		'K00RUP803F000001528840202108020955',
		'K00RUP807Q000001528850202108020955',
		'K00RUP805K000001527500202108020954'
		spider issue -1 values		
		Yes	Mayewwe

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@21:00:00@K00RUP804K000001527710202108020954,2022-01-22@21:00:00@K00RUP803F000001528840202108020955,2022-01-22@21:00:00@K00RUP807Q000001528850202108020955,2022-01-22@21:00:00@K00RUP805K000001527500202108020954

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@21:00:00@K00RUP805K000001527500202108020954

			# multiple reruns


	(R D-1) 2784	2022-01-22	New CMS	200	all	amazon 3p retailers	
		/ 200ITGF070000000557370201904220937
		/ 200ITGF080000000557080201904220902
		/ 200ESFF080000000557070201904220902	
		lnvalid deadlink		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@200ITGF070000000557370201904220937,2022-01-22@22:00:00@200ITGF080000000557080201904220902

			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-22@22:00:00@200ESFF080000000557070201904220902

			# multiple reruns
	
	0 (R D-1) 2788	2022-01-22	New CMS	K00	all	jarir.com	
		all item_ids	
		etl failure in price and not found (-1) for all other fields and title contains special characters		
		Yes	Turki

		# solution
			# multiple reruns failed

			# deadlink bottom
				# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-22@21:00:00@K00SABA07U020211116100231002517320,2022-01-22@None@K00SABA07U020211116100231145396320,2022-01-22@None@K00SABA07U020211116100231268187320,2022-01-22@None@K00SABA07U020211116100231401226320,2022-01-22@None@K00SABA07U020211116100231526298320,2022-01-22@None@K00SABA07U020211116100231652625320,2022-01-22@None@K00SABA07U020211116100231795048320,2022-01-22@None@K00SABA07U020211116100231937037320,2022-01-22@None@K00SABA07U020211116100232084400320,2022-01-22@None@K00SABA07U020211116100232215675320,2022-01-22@None@K00SABA07U020211116100232347713320,2022-01-22@None@K00SABA07U020211116100232476901320,2022-01-22@None@K00SABA07U020211116100232606676320,2022-01-22@None@K00SABA07U020211116100232737661320,2022-01-22@None@K00SABA07U020211116100232863082320,2022-01-22@None@K00SABA07U020211116100233017023320,2022-01-22@None@K00SABA07U020211116100233156582320,2022-01-22@None@K00SABA07U020211116100233308040320,2022-01-22@None@K00SABA07U020211116100233433484320,2022-01-22@None@K00SABA07U020211116100233588517320

			# top complete product_website
				# for trello: too lot of item_ids to resolve and requires plenty of time to fix

	(F D-1) 2789	2022-01-22	New CMS	X00	specifications	mercadolibre.com.mx	
		X00MXEV0OL000001439350202106080517	
		valid no specs in pdp, value should be {}		
		Yes	Switzell

		# solution
			# https://articulo.mercadolibre.com.mx/MLM-700780841-super-smash-bros-nintendo-switch-nuevo-sellado-_JM#position=24&search_layout=grid&type=item&tracking_id=c0c63df7-120a-4489-849d-b3cd08270363

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@05:00:00@X00MXEV0OL000001439350202106080517

			# check site, no specificaiton, please verify
			# need to be checked in local

			# fix unhandled tag
			# rebuild
			# rerun

	(R D-1) 2794	2022-01-22	New CMS	200	delivery	elgiganten.dk	
		'200DK100G0000000031950201901180326',
		'200DK100D0000000031960201901180326',
		'200DK100H0000000041600201901180334',
		'200DK10070000001397300202105040205'
		wrong DQ		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@200DK100G0000000031950201901180326,2022-01-22@22:00:00@200DK100D0000000031960201901180326,2022-01-22@22:00:00@200DK100H0000000041600201901180334,2022-01-22@22:00:00@200DK10070000001397300202105040205

			# rerun

	(R D-1) 2795	2022-01-22	New CMS	200	all	elgiganten.dk	
		'200DK100H0000000041600201901180334',
		'200DK100G0000000031950201901180326',
		'200DK10070000001397300202105040205',
		'200DK100Q0000000032320201901180326'
		spider issues		
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-22@22:00:00@200DK100Q0000000032320201901180326
			# rerun

	(R D-1)2796	2022-01-22	New CMS	F00	all	fnac.be/nl	
		/ F00BE9B05R020211207020255242346341
		F00BE9B040000001642080202111040958	
		Invalid Deadlink
		Yes	Keeshia

		# solution
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-22@22:00:00@F00BE9B05R020211207020255242346341,2022-01-22@22:00:00@F00BE9B040000001642080202111040958
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-22@22:00:00@F00BE9B05R020211207020255242346341

			# multiple reruns

	-----
	(R D-1) 1687	2022-01-22	New CMS	M00	all	www.musti.no	
		Hud	
		Auto copy over 1 day	
		https://prnt.sc/26h6sa7		
		MICHAEL

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-22@22:00:00@M00@011a94de-7d62-4767-b89d-36257038cd2e@e91b2a1d-d514-49bb-afce-9bc2b6a1e743
			# rerun
			# 

	0 (R D-1) 1699	2022-01-22	New CMS	U00	all	www.magazineluiza.com.br	
		joy con	

		Data Count Mismatch 1 (Fetched Data is Greater than expected)	
		https://prnt.sc/26h7irx		
		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-22@02:00:00@U00@86c5be64-e098-4678-beda-fd4d8895e764@G3u21LZqEeidIgANOiZi-g

			# multiple reruns failed
			# checking local: weird
				# need further time to fix
				# new data source found

		'Nintendo Switch',
		'Pro Controller',
		'Joy Con',
		'Console Nintendo Switch',
		'Controle Nintendo Switch',
		'Console',
		'Controle sem fio Nintendo Switch',
		'Controle Joy con',
		'Console Switch',
		'Controle Switch',
		'Mini Video Game',
		'Aparelho de Game',
		'Aparelho de Video Game',
		'controle game sem fio'

		'Mini game portatil'

		# tested goods all
			# push for tomorrow

	(R D-1) 1700	2022-01-22	New CMS	U00	all	www.magazineluiza.com.br	
		mini video game	
		Auto Copy Over fr 3 days	
		https://prnt.sc/26h7kne		
		kurt

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-22@02:00:00@U00@94b59958-8189-4a66-a160-a1e724e3ad52@G3u21LZqEeidIgANOiZi-g

			# multiple reruns failed
			# need to check in local:
				# needs further time to fix
				# as observed new data source found
				# recommend: remove the api keyword in dl strategy

			# tested goods all
				# push for tomorrow

	(R D-1) 1703	2022-01-22	New CMS	200	all	www.target.com	
		amazon speaker	
		duplicate product_website & product_url		
		https://prnt.sc/26h9cee		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-22@04:00:00@200@b369e967-64f2-44f7-afaa-f9cbfe9f215b@0l0XDL-yEei-eQANOiOHiA
			# rerun


	(R D-1) 1705	2022-01-22	New CMS	X00	all	www.sears.com.mx	
		Consola Nintendo Switch Lite	
		Invalid deadlink	
		https://prnt.sc/26ha9ow		
		MICHAEL

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-22@05:00:00@X00@4bc89052-7d33-4a6b-bda4-23c1ae037d9d@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# multiple reruns failed
			# multiple reruns


	(R D-1) 1706	2022-01-22	New CMS	X00	all	www.sears.com.mx	
		Nintendo Joy Con	
		Auto copy over fr 1 day	
		https://prnt.sc/26haac4		
		MICHAEL

		# solution
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-22@05:00:00@X00@c3b588ab-2bbc-4c43-831d-1f84455db1d9@a2e04412-f8ee-4aa8-b5b0-69bd076338f7
			# multiple reruns failed
			# multiple reruns

	-----
	0 (R D-1) 581	2022-01-22	New CMS	200	all	www.amazon.co.uk	
		Headphones Noise Cancelling	
		Invalid Deadlink	
		https://prnt.sc/26h6prd	Yes	Rhunick

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-22@23:00:00@200@G00@5514919e-e6cd-482d-b23a-c25f5cdb8f33
			# rerun

	0 (R D-1) 582	2022-01-22	New CMS	200	all	www.amazon.de	
		Headphones Noise Cancelling
			# https://www.amazon.de/s/ref=lp_570278_nr_n_5?fst=as%3Aoff&rh=n%3A562066%2Cn%3A%21569604%2Cn%3A17303695031%2Cn%3A570278%2Cn%3A361589011&bbn=570278&ie=UTF8&qid=1572010051&rnid=570278
		Headphones Bluetooth In Ear	
			# https://www.amazon.de/s/ref=lp_570278_nr_n_3?fst=as%3Aoff&rh=n%3A562066%2Cn%3A%21569604%2Cn%3A17303695031%2Cn%3A570278%2Cn%3A617060011&bbn=570278&ie=UTF8&qid=1572010051&rnid=570278

		3 Days Auto Copy Over	
		https://prnt.sc/26h6rtl	Yes	Rhunick
	
		# solution
			# 200@Y00@101d7ab2-5b1d-4a15-a707-e8a4e34132c8
			# 200@Y00@ed3689e9-1520-49a9-bf58-2856c3252f8e

			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-21@05:00:00@Z00@VC0@fc7a05eb-0d89-4a9a-b1cc-97c68498357c,2022-01-21@05:00:00@Z00@VC0@6de9a7c4-e4ca-4300-b490-2ce85f28178b,2022-01-21@05:00:00@Z00@VC0@b116bf86-f993-49b6-938b-d79e1fca3a39,2022-01-21@05:00:00@Z00@VC0@0e98a6c4-990e-48af-afd5-9544995e042d

			# multiple reruns failed
			# checking local: page redirect

			# for Trello
				# https://trello.com/c/E6e2dP8G/3124-zound-listings-invalid-deadlink-in-wwwamazonde

	(R D-1) 583	2022-01-22	New CMS	200	all	www.re-store.ru	
		headphones wired in ear	
		Data Count Mismatch 1(Fetched Data is Lesser than expected	
		https://prnt.sc/26h6zso	Yes	kurt

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-22@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f
			# multiple reruns

	Q00@Haglofs@10:30AM@Prod1
	X00@Nintendo Mexico@12:00NN@12:25AM@Prod2
	U00@Nintendo Brazil@12:00NN@12:25AM@Prod2

	Stats:
		Rerun: 22
		Fix: 2[1.5]

01 23 22
	(F D-3) Trello1
		/ 1699
			Nintendo Brazil

	(F D-3) Trello2
		push this frm yesterdays issues; 
			/ 1699
			/ 1700
				Nintendo Brazil

	fix this:
		582
			# javascript rendered? 

	-----
	(R D-1) 2797	2022-01-23	New CMS	100	all	hylte-lantman.com	
		'100SEDH010000001665490202111170530',
		'100SEDH010000001224180202010280943',
		'100SEDH010000000821330201908050830',
		'100SEDH010000001320430202102040906',
		'100SEDH010000001320500202102040906'

		incorrect scraping of title, affecting other essential data such as Availabiliy		
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@100SEDH010000001665490202111170530,2022-01-23@22:00:00@100SEDH010000001224180202010280943,2022-01-23@22:00:00@100SEDH010000000821330201908050830,2022-01-23@22:00:00@100SEDH010000001320430202102040906,2022-01-23@22:00:00@100SEDH010000001320500202102040906

			# rerun

	(R D-1) 2802	2022-01-23	New CMS	K00	rating_reviewers alternate.de		
		'K00DETF03F000001538600202108110154',
		'K00DETF0CG000001486800202107130336',
		'K00DETF0CG000001486940202107130336'
		Yes	Mayewwe

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@K00DETF03F000001538600202108110154,2022-01-23@22:00:00@K00DETF0CG000001486800202107130336,2022-01-23@22:00:00@K00DETF0CG000001486940202107130336

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@K00DETF0CG000001486800202107130336,2022-01-23@22:00:00@K00DETF0CG000001486940202107130336

			# multiple reruns

	(R D-1) 2803	2022-01-23	New CMS	P00	all	krefel.be/fr	
		P00BESU01L000001420470202105180758	
		Spider Issue		
		Yes	Keesha

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@P00BESU01L00000142047020210518075

			# rerun

	(R D-1) 2804	2022-01-23	New CMS	P00	all	krefel.be/nl	
		P00BEIS01L000001396030202105030459	

		Spider Issue		
		Yes	Keesha

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@P00BEIS01L000001396030202105030459

			# rerun

	(R D-1) 2805	2022-01-23	New CMS	100	all	all	power.se
		100SEY2010000001666230202111170531
		100SEY2010000001402210202105040817
		
		Invalid DeadLink		
		Yes	Jeremyy

		# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@100SEY2010000001666230202111170531, 2022-01-23@22:00:00@100SEY2010000001402210202105040817

		# solution
			# multiple reruns

	0 (R D-2) 2807	2022-01-23	New CMS	F00	all	proximus.be/nl/	All item_ids	
		No Data
		Yes	Glenn

		# solution
			# cant find in sql
			# cant find in rearch
			# cant find in couch

			# image to text from validator
				# # OfOximusBeJt F008EJX040000001635780202111040406 Created 20 days ago I Updated 20 days ago 
				# Proximusbein8 F008EJX040000001635830202111040406 Created 2 months ago I Updated 2 months ago I 
				# proximus F000EJX040000001635890202111040406 Created 2 months ago I Updated 2 months ago A 
				# proximus hew FOOBEJX040000001636080202111040406 Created 2 months ago I Updated 2 months ago A 
				# Draumus boinu F00BEJX040000001636130202111040406 Created 2 months ago I Updated 2 months ago 
				# Proximusbeire F008EJX040000001636190202111040406 Created 2 months ago I Updated 2 months ago A 
				# proximus ben' F000EJX040000001636200702111040406 Created 2 months ago I Updated 2 months ago 
				# proxlmus berg' F00BEJX040000001636250202111040406 Created 2 months ago I Updated 2 months ago 
				# groximustein8 F00BEJX040000001636340202111040406 Created 2 months ago I Updated 2 months ago 
				# proximus belri: FOOBEIXO5R020220110050216106438010 Created 2 months ago I Updated 2 months ago 4 

				F008EJX040000001635780202111040406
				F008EJX040000001635830202111040406
				F000EJX040000001635890202111040406
				FOOBEJX040000001636080202111040406
				F00BEJX040000001636130202111040406
				F008EJX040000001636190202111040406
				F000EJX040000001636200702111040406
				F00BEJX040000001636250202111040406
				F00BEJX040000001636340202111040406
				FOOBEIXO5R020220110050216106438010

			# F00BEJX040000001637130202111040406
			# F00BEJX040000001637200202111040406

			# python rerun_cmp.py -s CRAWL_FFAILED
			
			# https://trello.com/c/yGMlqSQz/3136-samsung-mobile-comparisons-no-data-in-proximusbe-nl
				# solved by sir Ryan Banilad
					# unhandled et error


	(R D-1) 2809	2022-01-23	New CMS	100	delivery	elgiganten.dk	
		'100DK10010000001222710202010280537',
		'100DK10010000001222810202010280537',
		'100DK10010000001266350202012230608',
		'100DK10010000001659210202111162228',
		'100DK10010000001659300202111162228',
		'100DK10010000001659370202111162228',
		'100DK10010000001659820202111162228'

		-3 delivery		
		Yes	Turki

		# soluton
			# cant load sql
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@100DK10010000001222710202010280537,2022-01-23@22:00:00@100DK10010000001222810202010280537,2022-01-23@22:00:00@100DK10010000001266350202012230608,2022-01-23@22:00:00@100DK10010000001659210202111162228,2022-01-23@22:00:00@100DK10010000001659300202111162228,2022-01-23@22:00:00@100DK10010000001659370202111162228,2022-01-23@22:00:00@100DK10010000001659820202111162228


			# 100DK10010000001222710202010280537
			# 100DK10010000001659820202111162228
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@100DK10010000001222710202010280537,2022-01-23@22:00:00@100DK10010000001659820202111162228

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@100DK10010000001659820202111162228

			# multiple reruns

	(R D-2) 2812	2022-01-23	New CMS	F00	all	vodafone.nl	All item_IDs	
		auto copy over (site is up)		
		Yes	Glenn

		# solution
			# multiple reruns
				# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-23@22:00:00@F00NLZG0JA000000858660201910110308,2022-01-23@22:00:00@F00NLZG0JA000000858830201910110315,2022-01-23@22:00:00@F00NLZG0JA000000858980201910110318,2022-01-23@22:00:00@F00NLZG0JA000000859090201910110322,2022-01-23@22:00:00@F00NLZG0JA000000859270201910110326,2022-01-23@22:00:00@F00NLZG0JA000000859430201910110332,2022-01-23@22:00:00@F00NLZG0JA000000859760201910110343,2022-01-23@22:00:00@F00NLZG0JA000000859970201910110355,2022-01-23@22:00:00@F00NLZG0JA000000860080201910110358,2022-01-23@22:00:00@F00NLZG0JA000000860200201910110401,2022-01-23@22:00:00@F00NLZG0JA000000860360201910110404,2022-01-23@22:00:00@F00NLZG0JA000000860420201910110408,2022-01-23@22:00:00@F00NLZG0JA000000860540201910110412,2022-01-23@22:00:00@F00NLZG0JA000000860660201910110415,2022-01-23@22:00:00@F00NLZG0JA000000860760201910110417,2022-01-23@22:00:00@F00NLZG040000001414100202105170844,2022-01-23@22:00:00@F00NLZG040000001414110202105170844,2022-01-23@22:00:00@F00NLZG0EL000001414120202105170844,2022-01-23@22:00:00@F00NLZG040000001414210202105170844,2022-01-23@22:00:00@F00NLZG040000001414230202105170844,2022-01-23@22:00:00@F00NLZG040000001414250202105170844,2022-01-23@22:00:00@F00NLZG040000001414270202105170844,2022-01-23@22:00:00@F00NLZG040000001414300202105170844,2022-01-23@22:00:00@F00NLZG040000001414330202105170844,2022-01-23@22:00:00@F00NLZG040000001414350202105170844,2022-01-23@22:00:00@F00NLZG040000001414370202105170844,2022-01-23@22:00:00@F00NLZG040000001414390202105170844,2022-01-23@22:00:00@F00NLZG040000001414420202105170844,2022-01-23@22:00:00@F00NLZG040000001414450202105170844,2022-01-23@22:00:00@F00NLZG040000001414480202105170845,2022-01-23@22:00:00@F00NLZG040000001414510202105170845,2022-01-23@22:00:00@F00NLZG040000001414540202105170845,2022-01-23@22:00:00@F00NLZG040000001414570202105170845,2022-01-23@22:00:00@F00NLZG040000001414600202105170845,2022-01-23@22:00:00@F00NLZG040000001414630202105170845,2022-01-23@22:00:00@F00NLZG040000001414660202105170845,2022-01-23@22:00:00@F00NLZG040000001414690202105170845,2022-01-23@22:00:00@F00NLZG040000001414720202105170845,2022-01-23@22:00:00@F00NLZG040000001414750202105170845,2022-01-23@22:00:00@F00NLZG040000001414780202105170845,2022-01-23@22:00:00@F00NLZG040000001414810202105170845,2022-01-23@22:00:00@F00NLZG040000001414840202105170845,2022-01-23@22:00:00@F00NLZG040000001414870202105170845,2022-01-23@22:00:00@F00NLZG040000001414900202105170845,2022-01-23@22:00:00@F00NLZG040000001414930202105170845,2022-01-23@22:00:00@F00NLZG040000001414960202105170845,2022-01-23@22:00:00@F00NLZG040000001414990202105170845,2022-01-23@22:00:00@F00NLZG040000001415020202105170845,2022-01-23@22:00:00@F00NLZG040000001415080202105170845,2022-01-23@22:00:00@F00NLZG040000001415110202105170845,2022-01-23@22:00:00@F00NLZG040000001415140202105170845,2022-01-23@22:00:00@F00NLZG040000001415180202105170845,2022-01-23@22:00:00@F00NLZG040000001415210202105170845,2022-01-23@22:00:00@F00NLZG040000001415240202105170845,2022-01-23@22:00:00@F00NLZG040000001415280202105170845,2022-01-23@22:00:00@F00NLZG040000001415300202105170845,2022-01-23@22:00:00@F00NLZG040000001415400202105170845,2022-01-23@22:00:00@F00NLZG040000001415430202105170845,2022-01-23@22:00:00@F00NLZG0WJ000001415620202105170845,2022-01-23@22:00:00@F00NLZG0WJ000001415790202105170845,2022-01-23@22:00:00@F00NLZG0WJ000001415820202105170845,2022-01-23@22:00:00@F00NLZG0WJ000001415840202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415870202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415890202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415920202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415950202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415980202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416010202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416030202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416060202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416090202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416120202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416150202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416170202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416200202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416230202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416260202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416290202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416310202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416340202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416370202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416400202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416430202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416450202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416480202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416510202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416540202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416570202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416590202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416620202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416650202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416680202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416710202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416740202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416770202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416800202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416830202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416860202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416890202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416920202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416950202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416980202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417010202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417040202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417070202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417100202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417130202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417160202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417190202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417220202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417250202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417280202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417310202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417340202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417370202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417400202105170845,2022-01-23@22:00:00@F00NLZG0EL000001417640202105170845,2022-01-23@22:00:00@F00NLZG0JA000001423410202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423490202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423580202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423670202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423760202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423850202105250316,2022-01-23@22:00:00@F00NLZG0WJ000001529970202108030256,2022-01-23@22:00:00@F00NLZG0WJ000001530050202108030256,2022-01-23@22:00:00@F00NLZG0WJ000001530140202108030256,2022-01-23@22:00:00@F00NLZG0WJ000001530220202108030256,2022-01-23@22:00:00@F00NLZG0EL000001530280202108030256,2022-01-23@22:00:00@F00NLZG040000001549710202108170144,2022-01-23@22:00:00@F00NLZG040000001549800202108170144,2022-01-23@22:00:00@F00NLZG040000001549960202108170144,2022-01-23@22:00:00@F00NLZG040000001550050202108170144,2022-01-23@22:00:00@F00NLZG040000001550120202108170144,2022-01-23@22:00:00@F00NLZG040000001550210202108170144,2022-01-23@22:00:00@F00NLZG040000001550300202108170144,2022-01-23@22:00:00@F00NLZG040000001550390202108170144,2022-01-23@22:00:00@F00NLZG040000001550480202108170144,2022-01-23@22:00:00@F00NLZG040000001550630202108170144,2022-01-23@22:00:00@F00NLZG0WJ000001582600202109130523,2022-01-23@22:00:00@F00NLZG040000001582690202109130523,2022-01-23@22:00:00@F00NLZG040000001582700202109130523,2022-01-23@22:00:00@F00NLZG040000001582710202109130523,2022-01-23@22:00:00@F00NLZG0JA000001598090202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598180202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598270202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598360202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598450202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598540202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598630202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598720202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598810202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598900202109280410,2022-01-23@22:00:00@F00NLZG0JA000001598990202109280410,2022-01-23@22:00:00@F00NLZG0JA000001599080202109280411,2022-01-23@22:00:00@F00NLZG0JA000001599170202109280411,2022-01-23@22:00:00@F00NLZG0JA000001599260202109280411,2022-01-23@22:00:00@F00NLZG0JA000001599350202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599440202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599530202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599620202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599710202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599800202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599890202109280434,2022-01-23@22:00:00@F00NLZG0JA000001599980202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600070202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600160202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600250202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600340202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600430202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600520202109280434,2022-01-23@22:00:00@F00NLZG0JA000001600610202109280517,2022-01-23@22:00:00@F00NLZG0JA000001600700202109280517,2022-01-23@22:00:00@F00NLZG0JA000001600790202109280517,2022-01-23@22:00:00@F00NLZG0JA000001600880202109280517,2022-01-23@22:00:00@F00NLZG0JA000001600970202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601060202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601150202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601240202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601330202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601420202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601510202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601600202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601690202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601780202109280518,2022-01-23@22:00:00@F00NLZG0JA000001601870202109280559,2022-01-23@22:00:00@F00NLZG0JA000001601960202109280559,2022-01-23@22:00:00@F00NLZG0JA000001602050202109280559,2022-01-23@22:00:00@F00NLZG0JA000001602140202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602230202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602320202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602410202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602500202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602590202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602680202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602770202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602860202109280600,2022-01-23@22:00:00@F00NLZG0JA000001602950202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603040202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603130202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603220202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603310202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603400202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603490202109280600,2022-01-23@22:00:00@F00NLZG0JA000001603580202109280600,2022-01-23@22:00:00@F00NLZG0WJ000001621370202110180628,2022-01-23@22:00:00@F00NLZG0EL000001642110202111040958,2022-01-23@22:00:00@F00NLZG0EL000001642530202111040959,2022-01-23@22:00:00@F00NLZG0ME020211207021624871773341,2022-01-23@22:00:00@F00NLZG05R020220103055429338708003,2022-01-23@22:00:00@F00NLZG05R020220103055429500054003,2022-01-23@22:00:00@F00NLZG0UD020220103055432334565003,2022-01-23@22:00:00@F00NLZG05R020220117024908255116017

				# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-23@22:00:00@F00NLZG0JA000000859090201910110322,2022-01-23@22:00:00@F00NLZG0JA000000860200201910110401,2022-01-23@22:00:00@F00NLZG0JA000000860360201910110404,2022-01-23@22:00:00@F00NLZG0JA000000860420201910110408,2022-01-23@22:00:00@F00NLZG0JA000000860660201910110415,2022-01-23@22:00:00@F00NLZG0JA000000860760201910110417,2022-01-23@22:00:00@F00NLZG0EL000001414120202105170844,2022-01-23@22:00:00@F00NLZG040000001414230202105170844,2022-01-23@22:00:00@F00NLZG040000001414250202105170844,2022-01-23@22:00:00@F00NLZG040000001414270202105170844,2022-01-23@22:00:00@F00NLZG040000001414570202105170845,2022-01-23@22:00:00@F00NLZG040000001414600202105170845,2022-01-23@22:00:00@F00NLZG040000001414480202105170845,2022-01-23@22:00:00@F00NLZG040000001414690202105170845,2022-01-23@22:00:00@F00NLZG040000001414750202105170845,2022-01-23@22:00:00@F00NLZG040000001414780202105170845,2022-01-23@22:00:00@F00NLZG040000001415210202105170845,2022-01-23@22:00:00@F00NLZG040000001415430202105170845,2022-01-23@22:00:00@F00NLZG0JA000001415950202105170845,2022-01-23@22:00:00@F00NLZG040000001415280202105170845,2022-01-23@22:00:00@F00NLZG040000001415300202105170845,2022-01-23@22:00:00@F00NLZG0JA000001416890202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417010202105170845,2022-01-23@22:00:00@F00NLZG0JA000001417250202105170845,2022-01-23@22:00:00@F00NLZG0JA000001423410202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423490202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423580202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423670202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423760202105250316,2022-01-23@22:00:00@F00NLZG0JA000001423850202105250316,2022-01-23@22:00:00@F00NLZG0JA000001417370202105170845

				# valid deadlinks https://prnt.sc/26hxffh

				# multiple reruns


	(R D-1) 2813	2022-01-23	New CMS	200	all	dns-shop.ru	
		200RUL80C0000000907240202001290728	
		invalid deadlink		
		Yes	Louie

		# solution
			# 2022-01-23@21:00:00@2022-01-23@21:00:00@200RUL80C0000000907240202001290728
			# 2022-01-23@21:00:00@200RUL80C0000000907240202001290728
			# python rerun_cmp.py -s CRAWL_FAILED -l 2022-01-23@21:00:00@200RUL80C0000000907240202001290728

			# rerun

	(R D-1) 2814	2022-01-23	New CMS	200	all	amazon.it/3p	
		200ITGF070000000557760201904220952
		200ITGF070000000801930201906210247
		200ITGF080000000557080201904220902	

		invalid deadlink	
		Yes	Louie

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200ITGF070000000557760201904220952,2022-01-23@22:00:00@200ITGF070000000801930201906210247,2022-01-23@22:00:00@200ITGF080000000557080201904220902

			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200ITGF080000000557080201904220902

			# multiple reruns

	-----
	(R D-2) 1714	2022-01-23	New CMS	200	all	www.elgiganten.se	
		/ bluetooth hörlurar träning
			06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw
		/ hörlurar bluetooth
			9c1af0b2-63e7-4c5a-894c-1ec88db4d3f2@fVQR1tMrEeaOEAANOrFolw
		/ noise cancelling hörlurar
			RtItRU4NEei-cwANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		/ portabel högtalare
			6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw
		/ trådlösa hörlurar träning
			5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw
		/ trådlösa in ear hörlurar
			a968ea17-66bb-4886-82c8-19e23da694a3@fVQR1tMrEeaOEAANOrFolw

		duplicate product_website & product_url

		https://prnt.sc/26hrhii
		https://prnt.sc/26hrhuw
		https://prnt.sc/26hriba
		https://prnt.sc/26hrimm
		https://prnt.sc/26hrjqn
		https://prnt.sc/26hrkgz
		https://prnt.sc/26hrkwq
		https://prnt.sc/26hrlfv
		https://prnt.sc/26hrlui
		https://prnt.sc/26hrm6d
		https://prnt.sc/26hrnm1
		https://prnt.sc/26hrnzd
		https://prnt.sc/26hro8s
		https://prnt.sc/26hrof1
		https://prnt.sc/26hropo
		https://prnt.sc/26hrp7r
		https://prnt.sc/26hrqg1
		https://prnt.sc/26hrr4u		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200@06eaeb75-c998-4087-8591-d9cd3a516494@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@9c1af0b2-63e7-4c5a-894c-1ec88db4d3f2@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@RtItRU4NEei-cwANOiOHiA@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@a968ea17-66bb-4886-82c8-19e23da694a3@fVQR1tMrEeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200@9c1af0b2-63e7-4c5a-894c-1ec88db4d3f2@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@RtItRU4NEei-cwANOiOHiA@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@6f1Uq1KcEeeFswANOiOHiA@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw,2022-01-23@22:00:00@200@a968ea17-66bb-4886-82c8-19e23da694a3@fVQR1tMrEeaOEAANOrFolw


			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200@5dbe8349-122f-47c7-83a4-c5c732d5f966@fVQR1tMrEeaOEAANOrFolw, 2022-01-23@22:00:00@200@a968ea17-66bb-4886-82c8-19e23da694a3@fVQR1tMrEeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200@a968ea17-66bb-4886-82c8-19e23da694a3@fVQR1tMrEeaOEAANOrFolw

			# multiple reruns

	(R D-1) 1715	2022-01-23	New CMS	200	all	www.dns-shop.ru
		наушники вкладыши проводные	
			41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g
		duplicate product_website & product_url		

		https://prnt.sc/26hrdx1
		https://prnt.sc/26hrew4
		https://prnt.sc/26hrfmg		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-23@21:00:00@200@41a067c5-1e3c-4253-83d8-1e1c74233154@BzxplkXeEeeMawANOiZi-g
			# rerun

	-----
	(R D-1) 591	2022-01-22	New CMS	100	all	www.kjell.com/se	
		Smartwatch	
		1 Day Auto Copy Over	
		https://prnt.sc/26hrii3	
		Yes	Rhunick

		# solution
			# no result in sql
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-23@22:00:00@100@8H0@8ef41c86-0f04-49e8-b5fa-888038f0e01a
			# rerun

	(R D-1) 592	2022-01-23	New CMS	200	all	www.re-store.ru	
		Headphones Wired In Ear - 8
			200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f
		Speakers Voice	- 3
			200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd
		miss match data	

		https://prnt.sc/26hsdtv
		https://prnt.sc/26hsere	

		Yes	michael

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-23@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f,2022-01-23@21:00:00@200@LQ0@2e718594-bb46-43c0-8bb0-498862ece8cd

			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-23@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# multiple reruns

	(R D-1) 593	2022-01-23	New CMS	200	all	www.saturn.de	
		Headphones Bluetooth On Ear
			200@910@c6f0dedb-bbbe-49e9-82c3-28d7eee6d8d8
		Speakers Multiroom
			200@910@e4da3210-56d1-4f71-90f5-9b9674adb2b4

		miss match data	

		https://prnt.sc/26hsjih
		https://prnt.sc/26hsk3z	
		Yes	michael

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-23@22:00:00@200@910@c6f0dedb-bbbe-49e9-82c3-28d7eee6d8d8,2022-01-23@22:00:00@200@910@e4da3210-56d1-4f71-90f5-9b9674adb2b4
			# rerun

	Stats:
		Rerun: 15 (18/15) = 1.2 
		Fix: 2[3]

01 28 22
	(w) Trello
		https://trello.com/c/xvSMNTCL/3155-eva-solo-comparisons-blocked-webshot-in-magasindk
			# webshot url
				https://detailwebshots.blob.core.windows.net/610-ws/2022-01-28/610DKHX00P000001617630202110110431.png
				610SEZW01P000001496480202107200327

			# url
				https://www.magasin.dk/stegepande-20-cm.-keramisk/pj3nxyo5jey.html?dwvar_pj3nxyo5jey_color=s%C3%B8lv%2Fsort
				610DKHX00P000001605590202109280746

			# solved by rerun

	(w) Trello
		https://trello.com/c/jMMsdRUy/3157-eva-solo-comparisons-blocked-webshot-in-cerverase
			# webshot url
				https://detailwebshots.blob.core.windows.net/610-ws/2022-01-28/610SEZW01P000001496480202107200327_ACO.png

			# url
				https://www.cervera.se/produkt/eva-solo-tradgard-fagelshelter
				610SEZW01P000001496480202107200327

				# // {"type": "click-using-evaluate", "element": "div.NostoOverlayClosePermanently.NostoCloseButton", "delayBeforeAction": 2000, "delayAfterAction": 2000},

				# https://prnt.sc/26kp4pr
				# status code 404

	Stats:
		Webshots: 2 

01 29 22
	(D) Development
		https://www.grainger.com/
			# comparison
				# https://www.grainger.com/product/RUBBERMAID-COMMERCIAL-PRODUCTS-Stick-Sweeper-5M865

				# price
				# currency
				# availability
				# rating
					# reviews
					# rating
				# title
				# brand
				# description
				# image url
				# video url

			# listings
				# 

		https://www.ferguson.com/

	(R D-2) 1822	2022-01-29	New CMS	200	all	www.re-store.ru	
		/ проводные наушники - 17
			bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная колонка wi-fi - 3
			8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab
		/ портативная акустика wi-fi - 3
			75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab
		/ наушники с проводом - 20
			3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

		Data Count Mismatch 5 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 2 (Fetched Data is Less than expected)
		Data Count Mismatch 6 (Fetched Data is Less than expected)	

		https://prnt.sc/26l3b9x
		https://prnt.sc/26l3cnp
		https://prnt.sc/26l3e1x
		https://prnt.sc/26l3f0i		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab,2022-01-29@21:00:00@200@8e970e2e-b775-46bd-b172-00c845d5252c@a4059226-f529-46c6-8a35-c120219fbbab,2022-01-29@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab,2022-01-29@21:00:00@200@3b929f29-cf3f-43cf-8d3d-cbc14e2b06ea@a4059226-f529-46c6-8a35-c120219fbbab

			# bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab
			# 75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@21:00:00@200@bfed8a03-9385-4010-9dec-9360fe56bc5b@a4059226-f529-46c6-8a35-c120219fbbab,2022-01-29@21:00:00@200@75f657e3-a194-4009-9e38-cf6aaf4dc3b8@a4059226-f529-46c6-8a35-c120219fbbab

			# multiple reruns

	(R D-1) 1823	2022-01-29	New CMS	200	all	www.saturn.de	
		/ amazon lautsprecher
			a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA
		/ bluetooth kopfhörer
			9f66bde9-a88f-40a3-9970-1c4305890a7c@UbbyaApIEem-gAANOiOHiA
		/ smart lautsprecher
			ad52f085-94b1-4478-a4fd-01ad3bd21b06@UbbyaApIEem-gAANOiOHiA

		duplicate product_website & product_url	

		https://prnt.sc/26l3sih
		https://prnt.sc/26l3svp
		https://prnt.sc/26l3tro
		https://prnt.sc/26l3v7g
		https://prnt.sc/26l3vho		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@200@a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA,2022-01-29@22:00:00@200@9f66bde9-a88f-40a3-9970-1c4305890a7c@UbbyaApIEem-gAANOiOHiA,2022-01-29@22:00:00@200@ad52f085-94b1-4478-a4fd-01ad3bd21b06@UbbyaApIEem-gAANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHEDd -l 2022-01-29@22:00:00@200@a662c575-2ea4-4348-9feb-036c4e8c1f03@UbbyaApIEem-gAANOiOHiA

			# multiple reruns

	(R D-2) 1824	2022-01-29	New CMS	200	all	www.elkjop.no	
		/ hodetelefoner trådløse
			d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw

			North Touch Two helt trådløse hodetelefoner (sort)
				https://prnt.sc/26l52xl
			Skullcandy Dime helt trådløse hodetelefoner (true black)
				https://prnt.sc/26l53e9

		/ støydempende hodetelefoner
			fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw

			Sony WH-CH700N trådløse on-ear hodetelefoner (blå)
			Sony WH-CH700N trådløse on-ear hodetelefoner (grå)
			Sony WH-CH700N trådløse on-ear hodetelefoner (sort)

		/ trådløse hodetelefoner	
			cTj0UE4NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw



		duplicate product_website & product_url	

		https://prnt.sc/26l3na5
		https://prnt.sc/26l3obm
		https://prnt.sc/26l3orq
		https://prnt.sc/26l3p7h
		https://prnt.sc/26l3pys
		https://prnt.sc/26l3qba

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@200@d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw,2022-01-29@22:00:00@200@fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw,2022-01-29@22:00:00@200@cTj0UE4NEei-cwANOiOHiA@EGO2juO8EeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@200@d49da1cb-0478-4248-9a74-fec80478472e@EGO2juO8EeaOEAANOrFolw,2022-01-29@22:00:00@200@fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw

			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-29@22:00:00@200@fec500e1-b9bb-4046-9fee-f8f3190f74c3@EGO2juO8EeaOEAANOrFolw

			# multiple reruns

	(R D-1) 1825	2022-01-29	New CMS	200	all	www.bol.com	
		/ Bluetooth hoofdtelefoon
			07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA
		/ draagbaar speaker
			1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA

			N-GEAR DISCO BLOCK 410 - Draagbare karaoke set met 2 microfoons - Roze

		duplicate product_website & product_url

		https://prnt.sc/26l3hku
		https://prnt.sc/26l3igr	

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@200@07c08c68-cab2-4853-bb7b-2e3d79bfd32a@UAjszpOqEei-eQANOiOHiA,2022-01-29@22:00:00@200@1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA

			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@200@1d2b7c08-f03b-476a-9f5b-57c1c57ea334@UAjszpOqEei-eQANOiOHiA

			# multiple reruns

	(R D-1) 1826	2022-01-29	New CMS	M00	all	www.zoo.se	
		ragdoll - 6
			70547464-8e48-4215-9f2b-b02c8ff77269@99ed35e1-25ec-4802-af31-7441d3edc758
		Chihuahua - 13
			0388b43a-dd3f-49ec-a1d0-2c0fede17add@99ed35e1-25ec-4802-af31-7441d3edc758

		Data Count Mismatch 2 (Fetched Data is Greater than expected)
		Data Count Mismatch 2 (Fetched Data is Lesser than expected)	

		https://prnt.sc/26l3sxp
		https://prnt.sc/26l3u5f

		michael

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@M00@70547464-8e48-4215-9f2b-b02c8ff77269@99ed35e1-25ec-4802-af31-7441d3edc758,2022-01-29@22:00:00@M00@0388b43a-dd3f-49ec-a1d0-2c0fede17add@99ed35e1-25ec-4802-af31-7441d3edc758

			# check site: equal count with sql

			# rerun, investigate in site

	(R D-1) 628	2022-01-29	New CMS	110	all	www.amazon.nl	
		Software
		Autocopy Over fr 1 day	
		https://prnt.sc/26l4k45	
		Yes	richelle

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-29@22:00:00@110@WQ0@49842f83-ec00-4c12-9944-649cb0cacf70
			# rerun

	(R D-1) 2954	2022-01-29	New CMS	100	delivery	elgiganten.dk	
		100DK10010000001248230202011250630
		100DK10010000001397490202105040251
		100DK10010000001397940202105040251
		100DK10010000001659670202111162228

		-3 delivery
		Yes	Turki

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-29@22:00:00@100DK10010000001248230202011250630,2022-01-29@22:00:00@100DK10010000001397490202105040251,2022-01-29@22:00:00@100DK10010000001397940202105040251,2022-01-29@22:00:00@100DK10010000001659670202111162228

			# rerun

	(R D-1) 2955	2022-01-29	New CMS	K00	price_value	mvideo.ru	
		'K00RUP805O000001449050202106150452',
		'K00RUP807O000001449520202106150452',
		'K00RUP807O000001449510202106150452',
		'K00RUP80AD000001449540202106150452',
		'K00RUP804K000001527130202108020954',
		'K00RUP804K000001449560202106150452',
		'K00RUP803F000001449440202106150452',
		'K00RUP803F000001449400202106150452',
		'K00RUP80DO000001449630202106150452',
		'K00RUP80CG000000980370202006250902',
		'K00RUP80CG000001524940202108020807',
		'K00RUP80CG000001311080202101250600',
		'K00RUP80CG000001310970202101250600',
		'K00RUP80CG000001311060202101250600',
		'K00RUP80CG000001310880202101250600',
		'K00RUP80CG000001245160202011170413',
		'K00RUP80CG000001245170202011170413',
		'K00RUP80CG000000980440202006250902',
		'K00RUP80CG000001409840202105110503',
		'K00RUP80CG000001310950202101250600',
		'K00RUP80CG000001310930202101250600',
		'K00RUP80CG000001310840202101250600',
		'K00RUP80CG000001373010202104050819',
		'K00RUP80CG000000980380202006250902',
		'K00RUP80CG000001310990202101250600',
		'K00RUP805K000001527540202108020954',
		'K00RUP805K000001527570202108020954',
		'K00RUP80YO000001449690202106150452',
		'K00RUP80YO000001449660202106150452'

		Wrong price fetched		
		Yes	Mayeee

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-29@21:00:00@K00RUP805O000001449050202106150452,2022-01-29@21:00:00@K00RUP807O000001449520202106150452,2022-01-29@21:00:00@K00RUP807O000001449510202106150452,2022-01-29@21:00:00@K00RUP80AD000001449540202106150452,2022-01-29@21:00:00@K00RUP804K000001527130202108020954,2022-01-29@21:00:00@K00RUP804K000001449560202106150452,2022-01-29@21:00:00@K00RUP803F000001449440202106150452,2022-01-29@21:00:00@K00RUP803F000001449400202106150452,2022-01-29@21:00:00@K00RUP80DO000001449630202106150452,2022-01-29@21:00:00@K00RUP80CG000000980370202006250902,2022-01-29@21:00:00@K00RUP80CG000001524940202108020807,2022-01-29@21:00:00@K00RUP80CG000001311080202101250600,2022-01-29@21:00:00@K00RUP80CG000001310970202101250600,2022-01-29@21:00:00@K00RUP80CG000001311060202101250600,2022-01-29@21:00:00@K00RUP80CG000001310880202101250600,2022-01-29@21:00:00@K00RUP80CG000001245160202011170413,2022-01-29@21:00:00@K00RUP80CG000001245170202011170413,2022-01-29@21:00:00@K00RUP80CG000000980440202006250902,2022-01-29@21:00:00@K00RUP80CG000001409840202105110503,2022-01-29@21:00:00@K00RUP80CG000001310950202101250600,2022-01-29@21:00:00@K00RUP80CG000001310930202101250600,2022-01-29@21:00:00@K00RUP80CG000001310840202101250600,2022-01-29@21:00:00@K00RUP80CG000001373010202104050819,2022-01-29@21:00:00@K00RUP80CG000000980380202006250902,2022-01-29@21:00:00@K00RUP80CG000001310990202101250600,2022-01-29@21:00:00@K00RUP805K000001527540202108020954,2022-01-29@21:00:00@K00RUP805K000001527570202108020954,2022-01-29@21:00:00@K00RUP80YO000001449690202106150452,2022-01-29@21:00:00@K00RUP80YO000001449660202106150452

			# rerun

	Stats:
		Rerun: 8 (10/8) = 1.25
		Development: 1

01 30 22
	(D) Development
		* img
		* shipping

	(R D-1) 2979	2022-01-30	New CMS	100	all	hylte-lantman.com	
		'100SEDH010000001665450202111170530',
		'100SEDH010000001224190202010280943',
		'100SEDH010000000821330201908050830',
		'100SEDH010000001321330202102050343',
		'100SEDH010000001224300202010280943',
		'100SEDH010000001224310202010280943',
		'100SEDH010000001320430202102040906'

		incorrect scraping of title, affecting other essential data such as Availabiliy	
		https://prnt.sc/26lp1dr	
		Yes	Jeremy

		# solution
			# python rerun_cmp.py -s CRAWL_FINISHED -l 2022-01-30@22:00:00@100SEDH010000001665450202111170530,2022-01-30@22:00:00@100SEDH010000001224190202010280943,2022-01-30@22:00:00@100SEDH010000000821330201908050830,2022-01-30@22:00:00@100SEDH010000001321330202102050343,2022-01-30@22:00:00@100SEDH010000001224300202010280943,2022-01-30@22:00:00@100SEDH010000001224310202010280943,2022-01-30@22:00:00@100SEDH010000001320430202102040906

			# rerun

	-----
	(R D-1) 1850	2022-01-30	New CMS	200	all	www.bol.com
		draadloos hoofdtelefoon
			78603b7c-e7f7-45ba-9acc-d5c397ffa869@UAjszpOqEei-eQANOiOHiA
		hoofdtelefoon	
			94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA

		duplicate product_website & product_url

		https://prnt.sc/26loa35
		https://prnt.sc/26loanf		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-30@22:00:00@200@78603b7c-e7f7-45ba-9acc-d5c397ffa869@UAjszpOqEei-eQANOiOHiA,2022-01-30@22:00:00@200@94e8fb1f-2701-46ac-b332-778924b5ea1f@UAjszpOqEei-eQANOiOHiA

			# rerun
	
	(R D-1) 1851	2022-01-30	New CMS	200	all	www.coolblue.nl	
		audio speaker	
			a82e95db-fce5-486a-96a5-4e92017f1903@dyBnppOqEei-eQANOiOHiA

		duplicate product_website & product_url	
		https://prnt.sc/26lobxm		
		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-30@22:00:00@200@a82e95db-fce5-486a-96a5-4e92017f1903@dyBnppOqEei-eQANOiOHiA
			# rerun

	(R D-1) 1852	2022-01-30	New CMS	200	all	www.dns-shop.ru
		беспроводная акустика bluetooth
			d3eb3d84-83b9-4bdc-8374-f1e7480daae1@BzxplkXeEeeMawANOiZi-g

		duplicate product_website & product_url	

		https://prnt.sc/26looaj
		https://prnt.sc/26looq6
		https://prnt.sc/26lop05		

		Phil

		# solution
			# python rerun_rnk.py -s CRAWL_FINISHED -l 2022-01-30@21:00:00@200@d3eb3d84-83b9-4bdc-8374-f1e7480daae1@BzxplkXeEeeMawANOiZi-g

			# rerun

	(F D-3) 1853	2022-01-30	New CMS	010	all	www.currys.co.uk	
		docking stations
			051ff4da-b6e0-4ae8-be67-23e40f4927ae@IO4KJA6jEeeOEgANOrFolw

		privacy screen	
			f7e7e52d-dea9-4f9b-9dfb-779222f09861@IO4KJA6jEeeOEgANOrFolw

		3 Days Auto Copy Over	
		https://prnt.sc/26lqxt5		
		Rhunick

		# solution	
			# python rerun_rnk.py -s CRAWL_FAILED -l 2022-01-30@23:00:00@010@051ff4da-b6e0-4ae8-be67-23e40f4927ae@IO4KJA6jEeeOEgANOrFolw,2022-01-30@23:00:00@010@f7e7e52d-dea9-4f9b-9dfb-779222f09861@IO4KJA6jEeeOEgANOrFolw

			# multiple reruns failed
			# check local: 
				# pagination failed
					# new api required
					# but no url found in new api data

					# cloudscraper failed: 
					# try with unblocker

					# test with one 
					# rebulid
	-----

	(R D-1) 633	2022-01-30	New CMS	100	all	www.dustinhome.se	
		Fordonsnavigation
			100@L10@07d4ac0c-443e-499d-80cc-db5de067b0bc

		Invalid Deadlink	
		https://prnt.sc/26lopkt	

		Yes	Rhunick

		# solutio
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-30@22:00:00@100@L10@07d4ac0c-443e-499d-80cc-db5de067b0bc

			# rerun

	(R D-1) 634	2022-01-30	New CMS	100	all	www.dustinhome.se	
		Pulsklockor & aktivitetsarmband
			100@L10@3d506ccf-0031-426c-ae33-7a5b825f6047
		Smartklockor
			100@L10@0641b2a3-3369-4d4d-963e-1adcc6f0f9c0
		Utomhus GPS:er
			100@L10@08c43282-dd24-4d26-89d1-114b65c616d1

		1 Day Auto Copy Over	
		https://prnt.sc/26loo3r	

		Yes	Rhunick

		# solution
			# python rerun_lst.py -s CRAWL_FAILED -l 2022-01-30@22:00:00@100@L10@3d506ccf-0031-426c-ae33-7a5b825f6047,2022-01-30@22:00:00@100@L10@0641b2a3-3369-4d4d-963e-1adcc6f0f9c0,2022-01-30@22:00:00@100@L10@08c43282-dd24-4d26-89d1-114b65c616d1

			# rerun

	(R D-1) 635	2022-01-30	New CMS	200	all	www.re-store.ru	
		Headphones Wired In Ear	- 8

		miss match	

		https://prnt.sc/26lp04c	
		Yes	michael

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-30@21:00:00@200@LQ0@d40b78a3-9af7-4a1c-af5a-47341363873f

			# multiple reruns

	(R D-1) 636	2022-01-30	New CMS	200	all	www.amazon.com	
		Speakers Bluetooth	
		miss match data	

		https://prnt.sc/26lqymq

		Yes	michael

		# solution
			# python rerun_lst.py -s CRAWL_FINISHED -l 2022-01-30@04:00:00@200@W00@609ecbd2-4c48-4940-9b5d-f0368a66462d

			# rerun

	010@Inriver@01:30PM@01:45AM@Prod2

	Stats:
		Rerun: 8
		Fix: 1[3]
		Development: 1
		
01 31 22
	0 1853 2022-01-30	New CMS	010	all	www.currys.co.uk	
		docking stations
			051ff4da-b6e0-4ae8-be67-23e40f4927ae@IO4KJA6jEeeOEgANOrFolw

		privacy screen	
			f7e7e52d-dea9-4f9b-9dfb-779222f09861@IO4KJA6jEeeOEgANOrFolw

		3 Days Auto Copy Over	
		https://prnt.sc/26lqxt5		
		Rhunick

		# solution
			# rerun

		alexa speaker
		amazon speaker
		bluetooth earbuds
		in ear earphones
		multi room speakers
		multiroom speakers
		noise cancelling earbuds
		noise cancelling earphones
		portable bluetooth speakers
		wifi speakers
		wireless bluetooth speakers
		wireless in ear headphones

--------------------------------------------------------
# Learnings
	# always check couch for ppt
		# if ppt, rerun atleast 6 times with interval
		# if not, ask teams for trello recommendation

	# causes why visible = 0
		# -1 availability, -1 ratings, -1 price
		# write jobs

	# causes why visible = 0 in 3p (3rd parties)
		# 0 visibility if 3p has no 3p items

	# (N'russian')
		# national language character set

	# trello retest
		# copy override leading to not work in rerun

	# COMMENTS
		# comment in sheet if maximum reruns but no spider issues
			# "Please request for copy over for these item ids as of the moment: 
			# 200CZ9D080000000552060201904050701
			# 200CZ9D080000000804580201907080637

		# auto copy over but site is up and if valid deadlink
			# Remaining ACO item ids are valid deadlink; Request to set to deadlink for these item ids

		# 3p items; good in local but not fetched during rerun; blocked
			# Request copy over for now. Data is now ETL but 3P items are not fetched due to being blocked. Issue under observation