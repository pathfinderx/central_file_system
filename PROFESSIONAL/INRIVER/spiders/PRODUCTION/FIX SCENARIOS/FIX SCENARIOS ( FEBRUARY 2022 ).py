02 01 23
	Development assitance
		* smythstoys.com/ie/en-ie

	Webshots
		(W) Request 1:
			EMEA region

		REquest 2:
			NA region

	Daily Issues:
		(R D-1) 14178	2023-02-01	OCR-K00	image_count	www.otto.de	
			K00DE0Z03O000001433020202106020354
			K00DE0Z03O000001433060202106020354
			K00DE0Z0DO000001433930202106020354
			K00DE9603F000001306140202101210429
			Missing image_count		Yes	Jurena

			-- 02 01 23 otto.de missing image
			SELECT * FROM view_all_productdata where date='2023-02-01' AND item_id in ('K00DE0Z03O000001433020202106020354', 'K00DE0Z03O000001433060202106020354', 'K00DE0Z0DO000001433930202106020354', 'K00DE9603F000001306140202101210429') ORDER BY source

02 02 23
	Webshots
		(W) EMEA region

		(W) NA region

	MSPrice
		(F D-1) Request 1:
			Hello! 
			Request to rerun gamestop.com.DB: A00
			Reason: Incorrect price
			Item ID: A00USM50RB000001247430202011230449

			SELECT * FROM view_all_productdata where date='2023-02-02' AND item_id in ('A00USM50RB000001247430202011230449') ORDER BY source

			{
			    "response": {
			        "task_uuid": "91243ae8-8944-4c77-b13d-4ead779e489c"
			    },
			    "version": "1.0"
			}

			# after rebuild
			{
			    "response": {
			        "task_uuid": "05fd7789-cb26-4b66-9c8f-8fe4b32145d7"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "64253252-619e-471d-887f-01f4b92bd457"
			    },
			    "version": "1.0"	
			}

			{
			    "response": {
			        "task_uuid": "58cb13b7-56fb-42a4-96e7-088381a4a9f7"
			    },
			    "version": "1.0"
			}

	SQL update:
		(SP D-1) Request 1:
			Request to Set Deadlink Listings Data in Samsung WG
			DB: O00
			Table: view_all_listingsdata
			Date: 02/02/2023
			Retailer: www.krefel.be/nl
			Category(ies):
			TV 2020
			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63

			# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

02 03 23
	Webshots
		(W) EMEA region

		(W) NA region

	Daily Issues:
		(F D-2) (R D-2) 6067	2022-02-03	New CMS	OCR-610	all	www.cervera.se	
			vannkoker	
			Auto copy over for 3 days	
			https://prnt.sc/DigzzzXF83fx	
			Yes	renz

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2023-02-03' 
				AND website = 'www.cervera.se' 
				AND keyword in (N'vannkoker')
				GROUP BY keyword, ritem_id, country, SOURCE

			{
			    "response": {
			        "task_uuid": "9e22e643-5396-4b6c-b16d-b6f0368e0b9f"
			    },
			    "version": "1.0"
			}

			# 12 products only
			{
			    "response": {
			        "task_uuid": "fc5550c1-42bc-44e6-83a4-0dec3e8e578f"
			    },
			    "version": "1.0"
			}

	MSPrice:
		(F D-2) (R D-1) Request 1:
			Hello Good Afternoon
			Request to rerun gamestop.comDB: A00
			Reason: Incorrect price
			Item ID:
			A00USM50RB000001247500202011230449
			A00USM50GC000000260590201903040940
			A00USM50KT020211108025941245342312
			A00USM50RB000001247490202011230449
			A00USM50PR000001606130202109280913
			A00USM50RB000001247460202011230449
			A00USM50CG000001342890202103100448
			A00USM50CG000001342900202103100448

			/ -1.0000	USD	A00USM50GC000000260590201903040940 
				https://www.gamestop.com/video-games/xbox-one/accessories/cables-adapters/products/xbox-one-wireless-pc-adapter/118038.html?gclid=CjwKCAiAkan9BRAqEiwAP9X6UV_JuVrvfMvr_ZcFhyJHYe8CZMUsrVEXrI1YMVq9-gV7ZH71SFzgpBoCIh4QAvD_BwE&gclsrc=aw.ds&bvstate=pg:2/ct:r

			/ -1.0000	USD	A00USM50RB000001247460202011230449 
				https://www.gamestop.com/video-games/xbox-series-x/accessories/products/xbox-series-x-play-and-charge-kit/11108370.html

			/ 59.9900	USD	A00USM50RB000001247490202011230449 
				https://www.gamestop.com/gaming-accessories/controllers/products/microsoft-xbox-series-x-wireless-controller-shock-blue/224738.html

			/ -1.0000	USD	A00USM50RB000001247500202011230449 
			https://www.gamestop.com/video-games/xbox-series-x/accessories/products/microsoft-xbox-series-x-black-wireless-controller-with-wireless-adapter-for-windows-10/11109978.html

			/ -1.0000	USD	A00USM50CG000001342890202103100448 
				https://www.gamestop.com/video-games/pc-gaming/accessories/mice-and-mouse-pads/products/naga-x-wired-mmo-gaming-mouse/11113764.html

			/ 79.9700	USD	A00USM50CG000001342900202103100448 
				https://www.gamestop.com/video-games/pc-gaming/accessories/mice-and-mouse-pads/products/viper-8khz-wired-gaming-mouse/11113763.html?condition=New

			/ -1.0000	USD	A00USM50PR000001606130202109280913 
				https://www.gamestop.com/gaming-accessories/controllers/products/backbone-one-ios-gaming-controller-for-iphones/11153984.html?condition=New

			/ -1.0000	USD	A00USM50KT020211108025941245342312 
				https://www.gamestop.com/gaming-accessories/gaming-headsets/xbox-series-x%7Cs/products/microsoft-wired-stereo-headset-for-xbox-series-x-20th-anniversary/307422.html

			SELECT * FROM view_all_productdata where date='2023-02-03' AND item_id in ('A00USM50RB000001247500202011230449', 'A00USM50GC000000260590201903040940', 'A00USM50KT020211108025941245342312', 'A00USM50RB000001247490202011230449', 'A00USM50PR000001606130202109280913', 'A00USM50RB000001247460202011230449', 'A00USM50CG000001342890202103100448', 'A00USM50CG000001342900202103100448') ORDER BY source

			{
			    "response": {
			        "task_uuid": "f08a6786-931f-445f-aea8-df97ad7fd547"
			    },
			    "version": "1.0"
			}

	Development
		* listings pagination
			/ micromania.fr
			* mediamarkt.lu
			officedepot.com
			officedepot.com.mx
			officeworks.com.au
			online.acer.com.au
			paris.cl

02 04 23
	MSPrice: 
		(W) Request 1:
			EMEA region webshots

		(R D-2) Request 2:
			Request to rerun data in currys.co.uk DB: A00
			Retailer: currys.co.uk
			Reason: No Data
			Item ID: Please see attached file

		(F D-1) (R D-2) Request 3:
			Goodmorning.
			Request to rerun otto.de DB: A00
			Reason: ETL Failure
			Item ID: 

			A00DE0Z0KT020221103195538532876307
			A00DE0Z0KT020221103195538968762307
			A00DE0Z0KT020221103195539429577307
			A00DE0Z0KT020221103195539885910307
			A00DE0Z0KT020221103195540386520307
			A00DE0Z0KT020221103195540824807307
			A00DE0Z0KT020221103195547113130307
			A00DE0Z0KT020221103195541294072307
			A00DE0Z0KT020221103195547388420307
			A00DE0Z0KT020221108143322951438312
			A00DE0Z0KT020211115034839054083319
			A00DE0Z0KT020220302122953882068061
			A00DE960RB000000950500202005220114

			SELECT * FROM view_all_productdata where date='2023-02-04' AND item_id in ('A00DE0Z0KT020221103195538532876307', 'A00DE0Z0KT020221103195538968762307', 'A00DE0Z0KT020221103195539429577307', 'A00DE0Z0KT020221103195539885910307', 'A00DE0Z0KT020221103195540386520307', 'A00DE0Z0KT020221103195540824807307', 'A00DE0Z0KT020221103195547113130307', 'A00DE0Z0KT020221103195541294072307', 'A00DE0Z0KT020221103195547388420307', 'A00DE0Z0KT020221108143322951438312', 'A00DE0Z0KT020211115034839054083319', 'A00DE0Z0KT020220302122953882068061', 'A00DE960RB000000950500202005220114') ORDER BY source

			{
			    "response": {
			        "task_uuid": "d7f1095b-e213-4ba0-899e-f070a3b6a25a"
			    },
			    "version": "1.0"
			}

		(W) Request 4:
			NA region webshots

	Daily Issues:
		(R D-2) 14187	2023-02-04	OCR-K00	price_value	euro.com.pl	
			K00PLD80CG000000980830202006250948 
			K00PLD80CG000000980850202006250948
			K00PLD80CG000001209500202009280448
			K00PLD80CG000001244570202011170356
			K00PLD80CG000001244590202011170356
			K00PLD80CG000001244600202011170356
			K00PLD80CG000001298390202101180406
			K00PLD803F000001308480202101210525
			K00PLD806O000001447400202106150451
			K00PLD80AD000001447560202106150451
			K00PLD80CG000001592020202109200556
			K00PLD80CG000001592140202109200556
			K00PLD807U020211115034606278362319
			K00PLD807U020220627045519079362178
			K00PLD807U020220627045521138512178
			K00PLD807U020221228014515361839362
			K00PLD807U020221228014515673586362
			Incorrect Price fetch invalid -1		
			Yes	Ash

			SELECT * FROM view_all_productdata where date='2023-02-04' AND item_id in ('K00PLD80CG000000980830202006250948', 'K00PLD80CG000000980850202006250948', 'K00PLD80CG000001209500202009280448', 'K00PLD80CG000001244570202011170356', 'K00PLD80CG000001244590202011170356', 'K00PLD80CG000001244600202011170356', 'K00PLD80CG000001298390202101180406', 'K00PLD803F000001308480202101210525', 'K00PLD806O000001447400202106150451', 'K00PLD80AD000001447560202106150451', 'K00PLD80CG000001592020202109200556', 'K00PLD80CG000001592140202109200556', 'K00PLD807U020211115034606278362319', 'K00PLD807U020220627045519079362178', 'K00PLD807U020220627045521138512178', 'K00PLD807U020221228014515361839362', 'K00PLD807U020221228014515673586362') ORDER BY price_value 

			{
			    "response": {
			        "task_uuid": "e3ce0fbe-10dd-49e7-896b-5d7bf2f8285e"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "4a16ee35-1f74-4257-af8e-2f34f93523c1"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "b7bfb529-7903-49fa-a43a-f506a4d1f28a"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "cee59149-7b3f-4d0b-9b61-7d7d3e395c59"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "4d8294fa-0b7b-4b09-8fde-ddcd5ccf4b3b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1a232ab0-daaf-40de-8024-6676e2be36c5"
			    },
			    "version": "1.0"
			}

						{
			    "response": {
			        "task_uuid": "818d414d-685f-4d2a-b6be-d9e8a47f2df3"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "e5297b10-cd06-4f6c-b491-2f546085b221"
			    },
			    "version": "1.0"
			}

		---
		(F D-1) (R D-2) 6069	2022-02-04	
			New CMS	OCR-U00	all	www.submarino.com.br	
			aparelho de video game	
			Duplicate Data	
			https://prnt.sc/k5xFMjHzjiHK	
			Yes	kurt

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.submarino.com.br' and 
				var2.keyword in (N'aparelho de video game') and 
				var2.[date] = '2023-02-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "dab6212b-b2bf-4afc-a361-a6f03784afc3"
			    },
			    "version": "1.0"
			}

		(F D-1) (R D-2) 6071	2022-02-04	New CMS	OCR-X00	all	www.coppel.com	
			Switch Nintendo	
			Duplicate Data	
			https://prnt.sc/hTFm_Ab3IlcJ	
			Yes	Kristian

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.coppel.com' and 
				var2.keyword in (N'Switch Nintendo') and 
				var2.[date] = '2023-02-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "00dd3bf4-1b03-4b37-86e8-1acb9c9cea1b"
			    },
			    "version": "1.0"
			}

		---

	SQL Update:
		(SP D-1) Request 1:
			Request to Set Deadlink Listings Data in Nintendo Brazil
			DB:U00 Table: view_all_listingsdata Date: 02/04/2023

			Retailer:www.bemol.com.br Category(ies):
			console Listings UUID(s):
			53fd2a65-67bc-497b-a3dc-59e807b40d13 

			Screenshot(s):https://prnt.sc/ieO22iA6UdMG 
			Reason: Fetching Data even if its Deadlink 

			# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

	Trello:
		(F D-1) (R D-2) Request 1:
			[OCR-Nintendo Brazil] Rankings : Duplicate Data in www.submarino.com.br
			Retailer:

			www.submarino.com.br

			Keyword:

			Controle Nintendo Switch

			Issue:

			Duplicate Data

			Screenshot:

			https://prnt.sc/TjMpcKVs_7FM

			s per BE said:

			Inconsistent result after 2 fixes and multiple reruns. Requires observation for next run

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.submarino.com.br' and 
				var2.keyword in (N'Controle Nintendo Switch') and 
				var2.[date] = '2023-02-04' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# no rerun executed 

	Development
		* listings pagination
			/ mediamarkt.lu
			/ officedepot.com
			officedepot.com.mx
			officeworks.com.au
			online.acer.com.au
			paris.cl

02 05 23
	MSPrice:
		(W) Request 1:
			webshots EMEA region

		(F D-1) (R D-2) Request 2:
			Hello, good morning ! Request to rerun otto.de. 
			Thank youu.DB: A00
			Reason: etl_failure in stocks status

			SELECT * FROM view_all_productdata where date='2023-02-05' AND item_id in ('A00DE960GC000000134230201903040450', 'A00DE960GC000000171100201903040525', 'A00DE960GC000000296670201903040958', 'A00DE960RB000000872260201911060926', 'A00DE960RB000000873470201911061131', 'A00DE960RB000000873540201911061147', 'A00DE960RB000000950330202005220114', 'A00DE960RB000000950410202005220114', 'A00DE960RB000000950500202005220114', 'A00DE960RB000001238140202011060535', 'A00DE0Z0RB000001412200202105120254', 'A00DE0Z0KT020211012064142315724285', 'A00DE0Z0KT020211115034837838146319', 'A00DE0Z0KT020211115034838393916319', 'A00DE0Z0KT020211115034839054083319', 'A00DE0Z0KT020220302122951708126061', 'A00DE0Z0KT020220302122952051646061', 'A00DE0Z0KT020220302122952751838061', 'A00DE0Z0KT020220302122953882068061', 'A00DE0Z0KT020220302122955132191061', 'A00DE0Z0KT020220302122955733635061', 'A00DE0Z0KT020220302122956245920061', 'A00DE0Z0KT020220717222327517006198', 'A00DE0Z0KT020220717222327993585198', 'A00DE0Z0KT020220717222328914547198', 'A00DE0Z0KT020220717222329828742198', 'A00DE0Z0KT020220717222330613438198', 'A00DE0Z0KT020220807132952474751219', 'A00DE0Z0KT020220807132953835324219', 'A00DE0Z0KT020221103195537674859307', 'A00DE0Z0KT020221103195538128345307', 'A00DE0Z0KT020221103195538532876307', 'A00DE0Z0KT020221103195538968762307', 'A00DE0Z0KT020221103195539429577307', 'A00DE0Z0KT020221103195539885910307', 'A00DE0Z0KT020221103195540386520307', 'A00DE0Z0KT020221103195540824807307', 'A00DE0Z0KT020221103195541294072307', 'A00DE0Z0KT020221103195547113130307', 'A00DE0Z0KT020221103195547388420307', 'A00DE0Z0KT020221108143318081085312', 'A00DE0Z0KT020221108143318516449312', 'A00DE0Z0KT020221108143318907289312', 'A00DE0Z0KT020221108143319293394312', 'A00DE0Z0KT020221108143320500338312', 'A00DE0Z0KT020221108143320998082312', 'A00DE0Z0KT020221108143321400539312', 'A00DE0Z0KT020221108143321704597312', 'A00DE0Z0KT020221108143322104163312', 'A00DE0Z0KT020221108143322556646312', 'A00DE0Z0KT020221108143322951438312', 'A00DE0Z0KT020221118082540094590322', 'A00DE0Z0KT020221122202031726668326') ORDER BY source

			{
			    "response": {
			        "task_uuid": "89f476f2-6059-4fc2-accb-2e53794de956"
			    },
			    "version": "1.0"
			}

		(W) Request 3:
			webshots NA region

		(R D-2) Request 4:
			Hello, good afternoon!
			Request to rerun walmart.com. Thank youu.
			DB: A00
			Reason: invalid deadlink

			A00USO50GC000000204770201903040719
			A00US140RB000000868250201911050244
			A00US140RB000000868370201911050310
			A00US140RB000000868420201911050323
			A00US140RB000000954460202005220842
			A00US140RB000001198670202008251002
			A00US140RB000001198980202008251002
			A00US140RB000001247040202011230358
			A00US140RB000001247050202011230358
			A00US14040000001345970202103100649
			A00US140CI020220301072445675718060
			A00US1401R020220301072452436681060
			A00US1400G020220301072506485486060
			A00US140KT020220713155905186897194
			A00US140EU020220830103049543811242
			A00US1403T020220830103059613394242
			A00US1404U020220830103100144925242
			A00US1404U020220830103101760347242
			A00US140DJ020220830103111281553242
			A00US1408T020220830103117222893242
			A00US1403T020220830103117567749242
			A00US1404U020220830103118335031242
			A00US140KT020221119104309632035323
			A00US140KT020221119104310487479323
			A00US140KT020221119104312118600323
			A00US140KT020221121104610489764325
			A00US140KT020230123221108660632023

			SELECT * FROM view_all_productdata where date='2023-02-05' AND item_id in ('A00USO50GC000000204770201903040719', 'A00US140RB000000868250201911050244', 'A00US140RB000000868370201911050310', 'A00US140RB000000868420201911050323', 'A00US140RB000000954460202005220842', 'A00US140RB000001198670202008251002', 'A00US140RB000001198980202008251002', 'A00US140RB000001247040202011230358', 'A00US140RB000001247050202011230358', 'A00US14040000001345970202103100649', 'A00US140CI020220301072445675718060', 'A00US1401R020220301072452436681060', 'A00US1400G020220301072506485486060', 'A00US140KT020220713155905186897194', 'A00US140EU020220830103049543811242', 'A00US1403T020220830103059613394242', 'A00US1404U020220830103100144925242', 'A00US1404U020220830103101760347242', 'A00US140DJ020220830103111281553242', 'A00US1408T020220830103117222893242', 'A00US1403T020220830103117567749242', 'A00US1404U020220830103118335031242', 'A00US140KT020221119104309632035323', 'A00US140KT020221119104310487479323', 'A00US140KT020221119104312118600323', 'A00US140KT020221121104610489764325', 'A00US140KT020230123221108660632023') ORDER BY source

			{
			    "response": {
			        "task_uuid": "01f10ff0-4d9a-48af-999c-acfd8e984ccc"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "ac1c7531-a5b5-4af8-8996-37909499ed43"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "4ec54b8d-fcdf-449e-b8fd-3e483090a94d"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "3efb0289-30bc-457d-82e5-8fe7fa719a49"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "c79c81c1-d15d-4016-8894-2ae4a39fc466"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "c79c81c1-d15d-4016-8894-2ae4a39fc466"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "b88d0eaf-c797-4671-844c-cefee9f68cf9"
			    },
			    "version": "1.0"
			}

	Trello:
		(F D-2) (R D-2) Ticket 1:
			# https://trello.com/c/KSLgGV04/4657-ms-price-comparisons-wrong-price-fetch-in-fnaces
			# [MS PRICE] Comparisons: Wrong price fetch in Fnac.es
			Date: February 04, 2023
			Company: A00
			Retailer: Fnac.es
			Issue: Wrong price fetch in Fnac.es

			Sample Item IDs:
			A00ES0B0RB000001052870202008060259
			A00ES0B0RB000001052710202008060234
			A00ES0B0RB000000992210202007210542

			Sample website:
			https://detailwebshots.blob.core.windows.net/a00-ws/2023-02-04/A00ES0B0RB000001052870202008060259.png
			https://www.fnac.es/Microsoft-Surface-Pro-7-i5-8GB-256GB-Plata-Tablet-Tablet/a6972728?oref=8ce08073-3fc2-0eb7-a863-19845454f913
			https://www.fnac.es/Teclado-Microsoft-QJW-00012-para-Surface-Pro-X-Accesorios-informatica-Funda-tablet/a7023985#omnsearchpos=1

			SELECT * FROM view_all_productdata where date='2023-02-05' AND website = 'www.fnac.es' AND item_id in ('A00ES0B0RB000001052870202008060259', 'A00ES0B0RB000001052710202008060234', 'A00ES0B0RB000000992210202007210542') ORDER BY source

		(F D-2) (R D-2) Ticket 2:
			# https://trello.com/c/3L7YNp2o/4661-garmin-comparisons-incorrect-reviews-scraped-in-milrabfi
			# [GARMIN] Comparisons: Incorrect reviews scraped in milrab.fi
			Date: February 5, 2023
			Company: GARMIN (100)
			Country: Finland
			Retailer: milrab.fi
			Issue: incorrect reviews scraped
			Affected item_ids:

			100FIII0ZQ020220124093143494202024
			100FIII0ZQ020220124093143005327024
			100FIII0ZQ020220124093142545357024
			100FIII0ZQ020220124093139650019024
			100FIII0ZQ020220124093139213001024
			100FIII0ZQ020220124093138657386024
			100FIII0ZQ020220905084939379507248
			100FIII0ZQ020220905084939379507248
			100FIII0ZQ020220905084939250505248
			100FIII0ZQ020220214062220009052045
			100FIII0ZQ020220214062217728363045
			100FIII0ZQ020220214062217020562045
			100FIII0ZQ020220905084936338451248
			100FIII0ZQ020220214062203060112045
			100FIII0ZQ020220905084936022758248
			100FIII0ZQ020220103074424620375003
			100FIII0ZQ020220103074424002461003
			100FIII0ZQ020220103074423557358003
			100FIII010000001646490202111090741
			100FIII010000001648640202111090811
			100FIII010000001648620202111090811
			100FIII010000001648610202111090811
			100FIII010000001646480202111090741
			100FIII010000001648590202111090811

			-- 02 05 23 milrab.fi incorrect reviews scraped
			SELECT * FROM view_all_productdata where date='2023-02-05' AND item_id in ('100FIII0ZQ020220124093143494202024', '100FIII0ZQ020220124093143005327024', '100FIII0ZQ020220124093142545357024', '100FIII0ZQ020220124093139650019024', '100FIII0ZQ020220124093139213001024', '100FIII0ZQ020220124093138657386024', '100FIII0ZQ020220905084939379507248', '100FIII0ZQ020220905084939379507248', '100FIII0ZQ020220905084939250505248', '100FIII0ZQ020220214062220009052045', '100FIII0ZQ020220214062217728363045', '100FIII0ZQ020220214062217020562045', '100FIII0ZQ020220905084936338451248', '100FIII0ZQ020220214062203060112045', '100FIII0ZQ020220905084936022758248', '100FIII0ZQ020220103074424620375003', '100FIII0ZQ020220103074424002461003', '100FIII0ZQ020220103074423557358003', '100FIII010000001646490202111090741', '100FIII010000001648640202111090811', '100FIII010000001648620202111090811', '100FIII010000001648610202111090811', '100FIII010000001646480202111090741', '100FIII010000001648590202111090811') ORDER BY source

		(F D-1) (R D-2) Ticket 3:
			# https://trello.com/c/S4KD1Oqn/4662-garmin-comparisons-incorrect-reviews-scraped-in-milrabno
			# [GARMIN] Comparisons: Incorrect reviews scraped in milrab.no
			Date: February 5, 2023
			Company: GARMIN (100)
			Country: Norway
			Retailer: milrab.no
			Issue: incorrect reviews scraped
			Affected item_ids:

			100NOJI0ZQ020220124094302114352024
			100NOJI0ZQ020220124094308149264024
			100NOJI0ZQ020220124094308149264024
			100NOJI0ZQ020220124094307795735024
			100NOJI0ZQ020220124094307487656024
			100NOJI0ZQ020220124094305403841024
			100NOJI0ZQ020220124094305109966024
			100NOJI0ZQ020220124094304749345024
			100NOJI0ZQ020220830052000290975242
			100NOJI0ZQ020220830051959888476242
			100NOJI010000001662330202111170057
			100NOJI010000001439530202106080557
			100NOJI0ZQ020220214062224920443045
			100NOJI010000001226500202010290433
			100NOJI0ZQ020220830052010947800242
			100NOJI0ZQ020220103074427261710003
			100NOJI0ZQ020220103074427079666003
			100NOJI0ZQ020220103074426887553003
			100NOJI010000001662850202111170057
			100NOJI010000001662820202111170057
			100NOJI010000001662950202111170057
			100NOJI010000001662920202111170057
			100NOJI010000001662890202111170057
			100NOJI010000001662870202111170057
			100NOJI0ZQ020220830051956251744242
			100NOJI010000000880730201911130949
			100NOJI010000001226510202010290433
			100NOJI010000001662950202111170057
			100NOJI0ZQ020220830051956251744242
			100NOJI0ZQ020220830052002910149242
			100NOJI0ZQ020220830052005771796242
			100NOJI0ZQ020220830052011396172242

	Daily Issues:
		(Trello) 1787	2023-02-05	New CMS	OCR-R10	all	www.lowes.com	
			caulk
			exterior caulk

			Data Count Mismatched 1 (Fetched data is lesser than expected)
			Data Count Mismatched 1 (Fetched data is greater than expected)

			https://prnt.sc/gQp9JuPowaTg
			https://prnt.sc/psbOun3cgO0s
			Yes	Michael

			# incorrect rankings run
			{
			    "response": {
			        "task_uuid": "47c52a09-e13e-4457-bc7b-bcb02ba8314f"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "29d2bba5-8456-4be3-8830-db77cfb25a6b"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "0e26433c-31be-4ce7-a0d3-92c14bfdfe14"
			    },
			    "version": "1.0"
			}

			https://www.lowes.com/pl/Exterior--Caulk-Caulking-Paint/4294729414?refinement=4294837992

02 08 23 
	SQL update:	
		(SP D-1) Request 1:
			Request for copy over in RAZER DB: K00
			Table: view_all_productdata
			Source Date: 2023-01-11
			Target Date: 2023-02-08 Reason: incorrect description
			# files on downloads

			# product > copy_over ? prod_copy_over_by_date+item_id.sql

	MSPrice
		(W) Request 1:
			EMEA region webshots

		(W) Request 2:
			NA region webshots

	Daily Issues:
		(F D-1) (R D-2) 14194	2023-02-08	OCR - K00	description	alternate.nl	
			All Item_id	Missing descriptions	
			https://prnt.sc/cSn9T0kuv6rZ	
			Yes	Neil

			SELECT * FROM view_all_productdata WHERE date='2023-02-08' AND website = 'www.alternate.nl' AND description = '-1'

			{
			    "response": {
			        "task_uuid": "97632dc8-f1e3-4fcc-a867-d08ac56f9132"
			    },
			    "version": "1.0"
			}

		(R D-1) 14195	2023-02-08	OCR - K00	image_count	euro.com.pl	
			K00PLD807U020211115034607722283319	
			missing image_count		
			Yes	Switzell

			SELECT * FROM view_all_productdata where date='2023-02-08' AND item_id in ('K00PLD807U020211115034607722283319') ORDER BY source

			{
			    "response": {
			        "task_uuid": "c93f8e4e-4a15-4fd7-a903-7146978220e7"
			    },
			    "version": "1.0"
			}

	Development
		/ listings pagination
			/ officedepot.com.mx
				# https://www.officedepot.com.mx/officedepot/en/Categor%C3%ADa/c/ROOT?q=%3Arelevance%3Acategory%3A05-067-701-0&page=0
				/ create, backup and push
				/ add to unblocker sheet

			/ officeworks.com.au
				# https://www.officeworks.com.au/shop/officeworks/c/technology/audio-speakers/wireless-bluetooth-speakers
				/ create and backup

			/ online.acer.com.au
				# https://online.acer.com.au/gaming
				/ create and backup
			/ paris.cl
				# https://www.paris.cl/electro/television/smart-tv/
				/ create and backup

02 09 23
	MSPrice:
		(W) Request 1:
			EMEA region webshots

		(F D-1) (R D-3) Request 2:
			Good Afternoon!
			Request to rerun invalid deadlink.
			DB: A00
			Retailer: Walmart.com
			Issue: Invalid deadlink
			Item ID: pls see attached file

			-- 02 09 23 walmart.ca invalid deadlink
			SELECT * FROM view_all_productdata where date='2023-02-09' AND item_id in ('A00USO50GC000000260610201903040940', 'A00US140RB000000866580201911040348', 'A00US140RB000000867580201911040807', 'A00US140RB000000867900201911040950', 'A00US140RB000000868250201911050244', 'A00US140RB000000886060201911201050', 'A00US14040000000983980202007020426', 'A00US140RB000001198590202008251002', 'A00US140RB000001198650202008251002', 'A00US140RB000001198670202008251002', 'A00US140RB000001198740202008251002', 'A00US140RB000001198880202008251002', 'A00US140RB000001199010202008251002', 'A00US140RB000001199030202008251002', 'A00US14040000001212660202010021141', 'A00US140RB000001239680202011060655', 'A00US140RB000001239840202011060655', 'A00US140RB000001247000202011230358', 'A00US140RB000001247070202011230358', 'A00US14040000001345980202103100649', 'A00US140KT020211108025937028026312', 'A00US140KT020211108025937276910312', 'A00US140KT020211108025939898370312', 'A00US140KT020211108025940413346312', 'A00US140KT020211108025941154397312', 'A00US140CI020220301072446122584060', 'A00US140B9220220301072447511531060', 'A00US1401R020220301072452095252060', 'A00US1401R020220301072452436681060', 'A00US1401R020220301072452807325060', 'A00US1401R020220301072453763899060', 'A00US1401R020220301072455747823060', 'A00US140QG020220301072457591600060', 'A00US1400G020220301072506975681060', 'A00US140KT020220713155907892563194', 'A00US140KT020220807202449464317219', 'A00US140KT020220807202451595273219', 'A00US140DJ020220830103048209053242', 'A00US140EU020220830103048505105242', 'A00US140EU020220830103048927223242', 'A00US1403T020220830103050651770242', 'A00US1403T020220830103051390890242', 'A00US140XR020220830103052076447242', 'A00US140UT020220830103054929440242', 'A00US1404U020220830103100774231242', 'A00US1404U020220830103101475534242', 'A00US1403T020220830103104777897242', 'A00US140ST020220830103105805644242', 'A00US140XR020220830103107496813242', 'A00US140UT020220830103108343670242', 'A00US1408T020220830103108731611242', 'A00US1408T020220830103108916947242', 'A00US1408T020220830103110182935242', 'A00US1408T020220830103110558372242', 'A00US140DJ020220830103110795050242', 'A00US140DJ020220830103111036894242', 'A00US1401U020220830103111805528242', 'A00US1401R020220830103115895290242', 'A00US140EU020220830103116073190242', 'A00US140KT020221119104310025677323', 'A00US140KT020221119104310487479323', 'A00US140KT020221119104316218776323', 'A00US140KT020230123221106631556023', 'A00US140KT020230123221107555533023', 'A00US140KT020230123221107976427023', 'A00US140KT020230123221110708412023') ORDER BY source

			{
			    "response": {
			        "task_uuid": "44cc106c-90f7-445b-907e-dd1faf1bd806"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "0b4271b4-7914-4622-81c2-fa9e63c2c35c"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "1bb1edd7-e812-41cb-874c-7cd5638c8507"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "d920d667-bb4d-41e7-96ea-044d0cf41ceb"
			    },
			    "version": "1.0"
			}

			# after rebuild
			{
			    "response": {
			        "task_uuid": "a736a423-f822-46fa-8111-39c6716a7ac6"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "654369a7-a0be-4416-aea6-5b2325a96591"
			    },
			    "version": "1.0"
			}

	Development
		/ listings pagination
			/ officeworks.com.au
				# https://www.officeworks.com.au/shop/officeworks/c/technology/audio-speakers/wireless-bluetooth-speakers
				/ push

			/ online.acer.com.au
				# https://online.acer.com.au/gaming
				/ push

			/ paris.cl
				# https://www.paris.cl/electro/television/smart-tv/
				/ add this to copilot as recursive function for searching hidden paginations
				/ push

			/ add the sir jave solution
				/ third party json autofix

		/ rankings code review on while loop:
			/ microsoft.com/en-us
				/ infinite loop resolved

			/ noelleming.co.nz
				/ infinite loop resolved

	SQL update:
		(SP D-1) Request 1:
			Request to Set Deadlink Rankings Data in Samsung CE
			DB: O00
			Table: view_all_rankingsdata
			Date: 2/9/2023
			Retailer: www.vandenborre.be/nl
			Keywords:
			Dolby Atmos soundbars
			Wifi soundbar
			grote tv
			mega scherm
			Stofzuiger zonder zak

			Screenshots: https://prnt.sc/yuX0AHa7GC2N
			https://prnt.sc/fbMyKqlM9MkZ
			https://prnt.sc/SLmRQkvRIF-Q
			https://prnt.sc/p1XyDKKl-h3S
			https://prnt.sc/49gLkc-3ZbjQ
			Reason: Fetching data even if its deadlink

			# exec:
				rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 2:
			Request to Set Deadlink Listings Data in Samsung WG
			DB: O00
			Table: view_all_listingsdata
			Date: 02/09/2023
			Retailer: www.krefel.be/nl
			Category(ies):
			TV 2020
			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63
			Screenshot(s):https://prnt.sc/fZatk90gQ3s7
			Reason: Fetching Data even if its Deadlink (Top Level Category)

			# exec
				# listings ? set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

		(SP D-1)Reqeust 3:
			Request to Set Deadlink Rankings Data in Samsung CE
			DB: O00
			Table: view_all_rankingsdata
			Date: 2/9/2023
			Retailer: www.krefel.be/nl Keyword(s):
			Droger aanbieding
			Droogkast aanbieding
			Koelkast aanbieding
			was-droogcombinatie aanbieding
			wasmachine aanbieding 

			Screenshot(s):
				https://prnt.sc/qAYTASEG3Vsz
				https://prnt.sc/L12vsPHFrydm
				https://prnt.sc/N5wSe_qDejz8
				https://prnt.sc/52CaxdjjZ4tv
				https://prnt.sc/IAzP8KrAB3s0 

			Reason: Fetching data even if its deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

02 10 23
	Trello:
		Ticket 1:
			Date : 02-05-23

			Retailer
			www.lowes.com

			Cause: Proxy Issue

			Category
			exterior caulk

			Issue description
			Data Count Mismatched 1 (Fetched data is greater than expected)

			Screenshot
			https://prnt.sc/psbOun3cgO0s

		(SP D-2) (R D-1) Ticket 2:
			Date: February 02, 2023
			Company: A00
			Retailer: microsoft.com/en-ca
			Issue: Incorrect data (Configured items)

			Sample Item IDs:
			A00CA640KT020221118082546560131322
			A00CA640KT020221118082547906937322
			A00CA640KT020221118082548095599322
			A00CA640KT020221118082557956100322
			A00CA640KT020221118082546793548322
			A00CA640KT020221118082557828027322
			A00CA640KT020221118082547026230322
			A00CA640KT020221118082557441232322
			A00CA640KT020221118082557574041322
			A00CA640KT020211025061823492328298
			A00CA640KT020211025061824470621298
			A00CA640KT020221118082549205444322
			A00CA640KT020221118082558332714322
			A00CA640KT020221118082558462714322
			A00CA640KT020221118082557295524322
			A00CA640KT020221118082557704489322
			A00CA640KT020221118082548454472322
			A00CA640KT020221118082548643994322
			A00CA640KT020221118082547263289322
			A00CA640KT020221118082548273014322
			A00CA640KT020221118082548826784322
			A00CA640KT020221118082558081168322
			A00CA640KT020221118082549016827322
			A00CA640KT020221118082558207326322
			A00CA640KT020221118082547495410322
			A00CA640KT020230130040710521076030
			A00CA640KT020230130040708853741030
			A00CA640KT020230130040708426221030
			A00CA640KT020230130040708720956030
			A00CA640KT020230130040711920694030
			A00CA640KT020230130040712052171030
			A00CA640KT020230130040709265521030
			A00CA640KT020230130040708995548030
			A00CA640KT020230130040711371646030
			A00CA640KT020230130040711495667030
			A00CA640KT020230130040711639679030
			A00CA640KT020230130040711777274030
			A00CA640KT020230130040709132004030
			A00CA640KT020230130040708581212030
			A00CA640KT020221118082545810303322
			A00CA640KT020221118082547722734322
			A00CA640KT020221118093943107788322
			A00CA640KT020221118093943552589322

			Affected item_ids:
			A00CA640KT020211025061819516706298
			A00CA640KT020211025061824661436298
			A00CA640RB000001411860202105120254
			A00CA640KT020220713155907469060194-LTE
			A00CA640KT020220713155905707922194
			A00CA640KT020230130040711639679030

	MSPrice:
		(W) Request 1:
			EMEA webshots

		(R D-3) Request 2:
			Hello, good morning! 
			Request to rerun microsoft.com/nl-nl. Thank you.
			DB: A00
			Reason: Incorrect data (configured items).
			Item IDs:

			A00NLP40RB000001406540202105050652
			A00NLP40KT020221118082601657206322
			A00NLP40KT020211025061818880735298
			A00NLP40RB000001406440202105050652
			A00NLP40KT020221118082601358365322

			SELECT * FROM view_all_productdata where date='2023-02-10' AND item_id in ('A00NLP40RB000001406540202105050652', 'A00NLP40KT020221118082601657206322', 'A00NLP40KT020211025061818880735298', 'A00NLP40RB000001406440202105050652', 'A00NLP40KT020221118082601358365322') ORDER BY source

			{
			    "response": {
			        "task_uuid": "211f5842-da1c-4af0-803e-a893a575ddba"
			    },
			    "version": "1.0"
			}

			# after rebuild
			{
			    "response": {
			        "task_uuid": "c32f0d66-fc7d-4a0c-8ab9-e333eab3ab71"
			    },
			    "version": "1.0"
			}

			# after variant change
			{
			    "response": {
			        "task_uuid": "ad65b78f-2fb3-4858-8ac1-b71c6ff16e55"
			    },
			    "version": "1.0"
			}

			# after 2nd variant change
			{
			    "response": {
			        "task_uuid": "227ff135-125a-4746-99c3-3cfa46c2587f"
			    },
			    "version": "1.0"
			}

		(W) Request 3:
			NA Webshots

	SQL update:
		(SP D-1) Request 1:
			Request for copy over in ZOUND 
			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2023-02-09
			Target Date: 2023-02-10
			ItemID: 200DKF3080000001398830202105040415

			# exec:
				# product > copy_over > 3p_copy_over_by_date+item_id.sql
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql

02 11 23
	Trello:
		(R D-2) Ticket 1:
			# https://trello.com/c/rH45wUPd/4664-ocr-daplistings-data-count-mismatched-in-wwwlowescom
			# [OCR-DAP]LISTINGS: Data Count Mismatched in www.lowes.com
			Date : 02-05-23

			Retailer
			www.lowes.com

			Cause: Proxy Issue

			Category
			exterior caulk

			Issue description
			Data Count Mismatched 1 (Fetched data is greater than expected)

			Screenshot
			https://prnt.sc/psbOun3cgO0s

			# exec:
				# send ss. Moved trello

	MSPrice:
		(R D-1) Request 1:
			Request to rerun NO DATA in currys.co.uk

			A00GBI00RB000001235520202011060456
			A00GBI00KT020211025061812038925298
			A00GBI00KT020211025061811547198298
			A00GBI00KT020211025061810992660298
			A00GBI00KT020221122202030157285326
			A00GBI00KT020221122202031234175326
			A00GBI00KT020221122202030940114326
			A00GBI00KT020221108143310175976312
			A00GBI00KT020221108143309934197312
			A00GBI00KT020221108143309688542312
			A00GBI00KT020221108143309259120312
			A00GBI00KT020221108143308851072312
			A00GBI00KT020221108143308594675312
			A00GBI00KT020221108143310644812312
			A00GBI00KT020221108143308292089312
			A00GBI00KT020220717222326201774198
			A00GBI00KT020220717222333826834198
			A00GBI00KT020220717222333656385198
			A00GBI00KT020220717222333478036198
			A00GBI00KT020220717222326921169198
			A00GBI00KT020220717222326528291198
			A00GBI00KT020220717222325787760198
			A00GBI00KT020220717222333307941198
			A00GBI00KT020221103195537221300307
			A00GBI00KT020221103195542662339307
			A00GBI00KT020221103195542478615307
			A00GBI00KT020221018190427099743291
			A00GBI00KT020221018190426732214291
			A00GBI00KT020221018190426395150291
			A00GBI00KT020221018190426064754291
			A00GBI00KT020221103195537002846307
			A00GBI00KT020221018190425678937291

			# exec:
				# wait load in queue

		(W) Request 2:
			webshots

		(SP D-2) Request 3:
			Hello, good morning! 
			Pwede ko pa check ani maam/sir, more than 30 mins na po sya running sa OCR.
			Task 2023-02-11T01:59:32.228Z cb7eb011-f21a-48e1-a543-4a4c929d7c56
			Retailer: amazon.es/direct
			Item ID: A00ESLR0RB000000992930202007230521

			SELECT * FROM view_all_productdata where date='2023-02-11' AND item_id in ('A00ESLR0RB000000992930202007230521') ORDER BY source

		Request 4:
			NA region webshot

	SQl update:
		(SP D-1) Request 1:
			Good Morning,
			Request to Set Deadlink Rankings Data in Zound Rankings
			DB: 200
			Table: view_all_rankingsdata
			Date: 2/11/2023
			Retailer: www.currys.co.uk
			Keyword(s): multiroom speakers
			Screenshot(s): https://prnt.sc/xb9Z8VStQTmy
			Reason: Fetching data even if its deadlink(pdp)

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 2:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2023-02-11
			Field for update:
			is_new
			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_is_new.sql

	Development
		listings:
			/ officedepot.com
				/ fixed availability

			/ officeworks.com.au
				/ image

			/ online.acer.com.au
				/ availability

		listings et
			/ mediamarkt.lu
				/ availability
				/ push

			/ micromania.fr
				/ availability
				/ push

			/ officedepot.com
				/ price
				/ availability
				/ ratings
				/ reviews

			/ officedepot.com.mx
				/ availability

			/ officeworks.com.au
				/ price
				/ availability
				/ ratings
				/ reviews

			/ online.acer.com.au
				/ availabilty

			/ paris.cl
				/ availabilty
				/ price

02 12 23
	Special
		/ reminders to microsoft.com/en-ca for variants update on channel
			/ update trello

		/ send cms report

		Trello:
			(SP D-1) Ticket 1:
				# https://trello.com/c/DoTYOQ9L/4655-ms-price-comparisons-incorrect-data-configured-items-in-microsoftcom-en-ca
				# [MS PRICE] Comparisons: Incorrect data (configured items) in microsoft.com/en-ca
				Date: February 02, 2023
				Company: A00
				Retailer: microsoft.com/en-ca
				Issue: Incorrect data (Configured items)
				A00CA640RB000001411860202105120254
				A00CA640KT020220713155907469060194
				A00CA640KT020211025061819516706298
				A00CA640KT020211025061824661436298
				A00CA640KT020230130040711639679030

	Development:
		/ officedepot.com.mx
			/ push

		/ officeworks.com.au
			/ push

		/ online.acer.com.au
			/ push

		/ paris.cl
			/ push

		/ paris.cl
			/ improve next url loop

	MSPrice:
		(W) Request 1:
			EMEA webshots

	SQL update:
		(SP D-1) Request 1:
			Request for copy over in ZOUND DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2023-02-11
			Target Date: 2023-02-12
			ItemID:200DKF3080000001398830202105040415

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# PRODUCT > copy_over > prod_copy_over_by_date+item_id

02 13 23
	Calamari Edit
		/ 02 09 23
			/ break mistake
		/ 02 08 23
			/ incomplete project
		/ 02 05 23 
		/ 02 04 23
		/ 02 03 23
		/ 02 02 23
		/ 02 01 23

02 15 23
	MSPrice
		(R D-2) Request 1:
			Request to rerun NO DATA in microsoft.com/es-es

			SELECT * FROM view_all_productdata where date='2023-02-15' AND item_id in ('A00ESQ40RB000001237480202011060520', 'A00ESQ40RB000001237490202011060520', 'A00ESQ40RB000000994780202008040310', 'A00ESQ40RB000000994720202008040310', 'A00ESQ40RB000000994730202008040310', 'A00ESQ40RB000000994590202008040310', 'A00ESQ40RB000000994600202008040310', 'A00ESQ40RB000000994610202008040310', 'A00ESQ40RB000000994620202008040310', 'A00ESQ40RB000000994630202008040310', 'A00ESQ40RB000000994800202008040310', 'A00ESQ40RB000000994810202008040310', 'A00ESQ40RB000000994820202008040310', 'A00ESQ40RB000000994830202008040310', 'A00ESQ40RB000000994840202008040310', 'A00ESQ40RB000000994850202008040310', 'A00ESQ40RB000000994860202008040310', 'A00ESQ40RB000000994870202008040310', 'A00ESQ40RB000000994880202008040310', 'A00ESQ40RB000000994650202008040310', 'A00ESQ40RB000000994660202008040310', 'A00ESQ40KT020221118082559868875322', 'A00ESQ40KT020221118082600003084322', 'A00ESQ40KT020221118082600182385322', 'A00ESQ40KT020221118082600306795322', 'A00ESQ40KT020221118082600460956322', 'A00ESQ40KT020221118082600583534322', 'A00ESQ40KT020221118082600714783322', 'A00ESQ40KT020221118082600837319322', 'A00ESQ40KT020221118082600962352322', 'A00ESQ40KT020221118082601097202322', 'A00ESQ40KT020221118082601223598322') ORDER BY source

			{
			    "response": {
			        "task_uuid": "54b8e06c-8aed-4a9c-9fad-ddcea7aaabe1"
			    },
			    "version": "1.0"
			}

		(W) Request 2:
			EMEA region webshots

		(W) Request 3:
			Request to rerun Incorrect webshots 
			DB: A00
			Reason: Incorrect webshots
			retailer: fnac.es
			Item_Id: pls see attached file

			# issues
				13 14 15 16
				21 23 26 30
				31 34 36 37 38 
				41 42

				2 8 9 12 14 

				12

		(W) Request 4:
			NA region webshots
	
	Trello:
		Ticket 1:
			# https://trello.com/c/EqpPzSe6/4681-zurn-compliance-missing-specifications-in-wwwamazoncom
			# [ZURN] Compliance - Missing specifications in www.amazon.com
			Issue: Please fix spyder to fetch the specs found in pdp

			date: 2023-02-14

			retailer: www.amazon.com

			items_ids:

			H10USW00Q8220220125025646271192025-Sample item
			H10USW00Q8220220125025655144787025
			H10USW00Q8220220125025653433895025
			H10USW00Q8220220125025655312187025
			H10USW00Q8220220406015050557785096
			H10USW00Q8220220125025646435679025
			H10USW00Q8220220125025651993183025
			H10USW00Q8220220125025649711058025
			H10USW00Q8220220406015038034188096
			H10USW00Q8220220125025655635668025
			H10USW00Q8220220406015045870494096
			H10USW00Q8220220406015048165371096
			H10USW00Q8220220125025654791706025
			H10USW00Q8220220406015046035111096
			H10USW00Q8220220125025654979751025
			H10USW00Q8220220406015048959707096
			H10USW00Q8220220406015042937415096
			H10USW00Q8220220125025651077839025
			H10USW00Q8220220125025659927666025
			H10USW00Q8220220310035723166225069
			H10USW00Q8220220125025655481346025
			H10USW00Q8220220406015046162367096
			H10USW00Q8220220406015049354365096
			H10USW00Q8220220125025651822313025
			H10USW00Q8220220125025652442836025
			H10USW00Q8220220406015048022944096
			H10USW00Q8220220125025706570075025
			H10USW00Q8220220406015050827843096
			H10USW00Q8220220406015045587944096
			H10USW00Q8220220406015050156077096
			H10USW00Q8220220406015048693170096
			H10USW00Q8220220406015045711732096
			H10USW00C9220220316085423927976075
			H10USW00Q8220220406015050016061096
			H10USW00Q8220220406015048564068096
			H10USW00Q8220220125025654629087025

		Ticket 2:
			# https://trello.com/c/Q27J2Fqd/4682-zurn-compliance-incomplete-specifications-in-wwwamazoncom
			# [ZURN] Compliance - Incomplete specifications in www.amazon.com
			Issue: Please fix the spyder to fetch the entire specs found in pdp

			date: 2023-02-14

			retailer: www.amazon.com

			item_ids:

			H10USW00C9220220406015034248505096-sample item
			H10USW00C9220220406015034837969096
			H10USW00C9220220406015036218041096
			H10USW00C9220220406015037223667096
			H10USW00C9220220406015039305272096
			H10USW00LA220220926034953588660269
			H10USW00NA220220926034958454137269
			H10USW00Q8220220125025644219264025
			H10USW00Q8220220125025644566725025
			H10USW00Q8220220125025645100532025
			H10USW00Q8220220125025646777228025
			H10USW00Q8220220125025646940720025
			H10USW00Q8220220125025647096197025
			H10USW00Q8220220125025647520502025
			H10USW00Q8220220125025647698280025
			H10USW00Q8220220125025647854690025
			H10USW00Q8220220125025648006918025
			H10USW00Q8220220125025648166510025
			H10USW00Q8220220125025648348139025
			H10USW00Q8220220125025648523334025
			H10USW00Q8220220125025648714038025
			H10USW00Q8220220125025648883744025
			H10USW00Q8220220125025649053876025
			H10USW00Q8220220125025649224607025
			H10USW00Q8220220125025649385275025
			H10USW00Q8220220125025649546654025
			H10USW00Q8220220125025649895134025
			H10USW00Q8220220125025650081970025
			H10USW00Q8220220125025650574954025
			H10USW00Q8220220125025650740512025
			H10USW00Q8220220125025650901679025
			H10USW00Q8220220125025651265239025
			H10USW00Q8220220125025651661749025
			H10USW00Q8220220125025652123364025
			H10USW00Q8220220125025652285781025
			H10USW00Q8220220125025652762098025
			H10USW00Q8220220125025653587960025
			H10USW00Q8220220125025653753216025
			H10USW00Q8220220125025653911659025
			H10USW00Q8220220125025654111414025
			H10USW00Q8220220125025654270996025
			H10USW00Q8220220125025654456136025
			H10USW00Q8220220125025655980350025
			H10USW00Q8220220125025656174254025
			H10USW00Q8220220125025656337654025
			H10USW00Q8220220125025656500662025
			H10USW00Q8220220125025656882306025
			H10USW00Q8220220125025657074322025
			H10USW00Q8220220125025657259048025
			H10USW00Q8220220125025657425421025
			H10USW00Q8220220125025657589920025
			H10USW00Q8220220125025657758205025
			H10USW00Q8220220125025658301077025
			H10USW00Q8220220125025659423358025
			H10USW00Q8220220125025700895983025
			H10USW00Q8220220125025703002212025
			H10USW00Q8220220125025704030176025
			H10USW00Q8220220125025704249188025
			H10USW00Q8220220125025704411400025
			H10USW00Q8220220125025704913216025
			H10USW00Q8220220125025705231728025
			H10USW00Q8220220125025705394305025
			H10USW00Q8220220125025705560245025
			H10USW00Q8220220125025705727829025
			H10USW00Q8220220125025706241716025
			H10USW00Q8220220125025706405561025
			H10USW00Q8220220125025706763832025
			H10USW00Q8220220125025706928205025
			H10USW00Q8220220125025707260107025
			H10USW00Q8220220125025707417321025
			H10USW00Q8220220125025707576315025
			H10USW00Q8220220125025707734105025
			H10USW00Q8220220125025708221831025
			H10USW00Q8220220125025708356875025
			H10USW00Q8220220125025708540185025
			H10USW00Q8220220125025708706147025
			H10USW00Q8220220125025708914702025
			H10USW00Q8220220125025709075592025
			H10USW00Q8220220125025709236506025
			H10USW00Q8220220125025711922341025
			H10USW00Q8220220131092057209392031
			H10USW00Q8220220131092057910045031
			H10USW00Q8220220131092058403820031
			H10USW00Q8220220310035722975890069
			H10USW00Q8220220310035723552759069
			H10USW00Q8220220310035723746783069
			H10USW00Q8220220310035723928130069
			H10USW00Q8220220316085428085696075
			H10USW00Q8220220316085428285309075
			H10USW00Q8220220316085429317234075
			H10USW00Q8220220316085431479502075
			H10USW00Q8220220316085432782541075
			H10USW00Q8220220316085433114497075
			H10USW00Q8220220406015033906588096
			H10USW00Q8220220406015034576724096
			H10USW00Q8220220406015034971332096
			H10USW00Q8220220406015035111300096
			H10USW00Q8220220406015035378882096
			H10USW00Q8220220406015035517712096
			H10USW00Q8220220406015035937546096
			H10USW00Q8220220406015036351185096
			H10USW00Q8220220406015036499315096
			H10USW00Q8220220406015036634226096
			H10USW00Q8220220406015037365390096
			H10USW00Q8220220406015037504714096
			H10USW00Q8220220406015037639596096
			H10USW00Q8220220406015037890540096
			H10USW00Q8220220406015038182410096
			H10USW00Q8220220406015038311690096
			H10USW00Q8220220406015038446020096
			H10USW00Q8220220406015038578515096
			H10USW00Q8220220406015038711514096
			H10USW00Q8220220406015039441432096
			H10USW00Q8220220406015039574614096
			H10USW00Q8220220406015039712939096
			H10USW00Q8220220406015039847528096
			H10USW00Q8220220406015040751660096
			H10USW00Q8220220406015040903911096
			H10USW00Q8220220406015041032967096
			H10USW00Q8220220406015041324901096
			H10USW00Q8220220406015041458832096
			H10USW00Q8220220406015041734266096
			H10USW00Q8220220406015042007731096
			H10USW00Q8220220406015042373977096
			H10USW00Q8220220406015042576997096
			H10USW00Q8220220406015042760167096
			H10USW00Q8220220406015043079339096
			H10USW00Q8220220406015043799659096
			H10USW00Q8220220406015044104331096
			H10USW00Q8220220406015044945411096
			H10USW00Q8220220406015046292180096
			H10USW00Q8220220406015046419161096
			H10USW00Q8220220406015046781830096
			H10USW00Q8220220406015046936095096
			H10USW00Q8220220406015047090312096
			H10USW00Q8220220406015047268423096
			H10USW00Q8220220406015047426195096
			H10USW00Q8220220406015047590108096
			H10USW00Q8220220406015047760236096
			H10USW00Q8220220406015047888006096
			H10USW00Q8220220406015048294156096
			H10USW00Q8220220406015048829234096
			H10USW00Q8220220406015049095304096
			H10USW00Q8220220406015049223696096
			H10USW00Q8220220406015049884030096
			H10USW00Q8220220406015050288572096
			H10USW00Q8220220406015050418938096
			H10USW00Q8220220406015050962272096
			H10USW00Q8220220406015051485672096
			H10USW00Q8220220406015052803962096
			H10USW00Q8220220406015053113107096
			H10USW00Q8220220406015053251103096
			H10USW00Q8220220406015053389743096
			H10USW00Q8220220406015053527705096
			H10USW00Q8220220406015054201759096
			H10USW00Q8220220406015054338750096
			H10USW00Q8220220406015054471296096
			H10USW00Q8220220406015054622007096
			H10USW00Q8220220406015054900719096
			H10USW00Q8220220406015055035348096
			H10USW00Q8220220406015055174333096
			H10USW00Q8220220406015055326075096
			H10USW00Q8220220406015055470902096
			H10USW00Q8220220406015055608234096
			H10USW00Q8220220406015055743180096
			H10USW00Q8220220406015055880612096
			H10USW00Q8220220406015056162130096
			H10USW00Q8220220406015056288955096
			H10USW00Q8220220406015056442444096
			H10USW00Q8220220406015056585154096
			H10USW00Q8220220606035804924853157
			H10USW00Q8220220606035805058520157
			H10USW00ZE220220926034944602171269

	Daily:
		(R D-1) 6083	2022-02-15	New CMS	K00	all	ldlc.com	
			Clavier PC	No data	
			https://prnt.sc/58mo0yNlxN8k	
			Yes	Michael

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2023-02-15' 
				AND website = 'www.ldlc.com' 
				AND keyword in (N'Clavier PC')
				GROUP BY keyword, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "6e56e202-828a-4e9a-87b4-0c48612a87be"
			    },
			    "version": "1.0"
			}

		(R D-2) 6084	2022-02-15	New CMS	K00	all	www.bol.com	
			/ Draadloze headsets
			/ Gaming toetsenborden
			/ koptelefoon
			/ Laptop
			/ Notebook
			/ Playstation-controller
			/ Toetsenbord
			
			No data	
			https://prnt.sc/n7MW7PTCaCwh
			https://prnt.sc/XjbE6Woqj1GS
			Yes	Michael

			SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2023-02-15' 
				AND website = 'www.ldlc.com' 
				AND keyword in (N'Draadloze headsets',
				'Gaming toetsenborden',
				'koptelefoon',
				'Laptop',
				'Notebook',
				'Playstation-controller',
				'Toetsenbord')
				GROUP BY keyword, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "b2cbd6a6-7940-4dc7-86b9-33ab8b408b3d"
			    },
			    "version": "1.0"
			}

			# correct payload
			{
			    "response": {
			        "task_uuid": "bde19a98-d592-4b36-b962-a4c2f71c6367"
			    },
			    "version": "1.0"
			}

		---

		(R D-2) 1789	2023-02-15	New CMS	K00	all	www.pcdiga.com	
			Keyboards	
			No Data	
			https://prnt.sc/20QW8CB6_aON	
			Yes	Jairus
			c2155696-5f33-4ef5-841e-5f6da529ce55

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2023-02-15' 
				AND website = 'www.pcdiga.com' 
				AND category in (N'Keyboards')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source				

			{
			    "response": {
			        "task_uuid": "973c70f8-f233-4afb-83ad-92778ba592d4"
			    },
			    "version": "1.0"
			}

		(R D-2) 1790	2023-02-15	New CMS	K00	all	www.pcgarage.ro	
			Mice	
			No Data	
			https://prnt.sc/iuweLs1Pgf0r	
			Yes	Jairus
			c09d7541-277c-4c7f-883a-4589f4427888

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2023-02-14' 
				AND website = 'www.pcgarage.ro' 
				AND category in (N'Mice')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source		

			{
			    "response": {
			        "task_uuid": "4473da8a-4bad-43e6-9ff6-3f0c9b71d828"
			    },
			    "version": "1.0"
			}
	
02 26 23
	Trello:
		fr_rebuild (F D-1) (R D-2) Ticket 1:
			# https://trello.com/c/D0vmkXNO/4689-razer-compliance-incomplete-description-in-wwwalzacz
			# [RAZER] Compliance - Incomplete description in www.alza.cz
			Issue: Please fix spyder to fetch the description found in pdp

			date: 2023-02-15
			retailer: www.amazon.com
			items_ids: K00CZ3603F000001527020202108020954

			Note: As of 2023-02-15, data has been copy over		

		fr_rebuild (F D-1) (R D-2) Ticket 2:
			# https://trello.com/c/ptyRjBZx/4685-razer-compliance-missing-imagecount-in-wwweurocompl
			# [RAZER] Compliance - Missing image_count in www.euro.com.pl
			Issue: Auto copy over and missing image_count, please fix spyder to fetch images found in pdp

			date: 2023-02-15
			retailer: www.euro.com.pl
			items_ids:
			K00PLD80CG000001244560202011170356
			K00PLD80CG000001298320202101180406

			Note: As of 2023-02-15, data has been copy over

	MSPrice:
		(W) Request 1:
			EMEA region webshots

		(W) Request 2:
			Good morning!

			Request to rerun Blocked webshots
			DB: A00
			Reason: Blocked webshots

			retailer: fnac.com (FR)
			Item_Id: pls see attached filehttps://prnt.sc/wU0SSbAQIGlQ

			# blocked after 3 multiple re-webshot attempts

		(W) Request 3:
			Good morning!

			Request to rerun Blocked webshots
			DB: A00
			Reason: Blocked webshots

			retailer: fnac.es (ES)
			Item_Id: pls see attached filehttps://prnt.sc/a9bf5V7ZrCAT

			# blocked after 3 multiple re-webshot attempts

		(W) Request 4:
			NA region

	SQL update:
		(SP D-1) Request 1:
			Request to Set Deadlink Rankings Data in Samsung CE
			DB: O00
			Table: view_all_rankingsdata
			Date: 2/16/2023
			Retailer: www.vandenborre.be/nl
			Keywords:

			Dolby Atmos soundbars
			Wifi soundbar
			grote tv
			mega scherm
			Stofzuiger zonder zak

			Screenshots:https://prnt.sc/jzAqxr4IX008https://prnt.sc/XlbXAdTRusT7https://prnt.sc/HTFIBguvQ2F2https://prnt.sc/NDrQp_rbhdG0https://prnt.sc/LCkUpoXIpp8M

			# exec: 
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 2:
			Request to Set Deadlink Listings Data in Samsung WG
			DB: O00
			Table: view_all_listingsdata
			Date: 02/16/2023
			Retailer: www.krefel.be/nl
			Category(ies):
			TV 2020
			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63
			Screenshot(s):https://prnt.sc/oK5E7JaJVq2L

			# exec:
				# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

		(SP D-1) Request 3:
			Request to Set Deadlink Rankings Data in Samsung CE
			DB: O00
			Table: view_all_rankingsdata
			Date: 2/16/2023
			Retailer: www.krefel.be/nl
			Keyword(s):
			Droger aanbieding
			Droogkast aanbieding
			Koelkast aanbieding
			was-droogcombinatie aanbieding
			wasmachine aanbieding

			Screenshot(s):https://prnt.sc/U2nyutIkkWVehttps://prnt.sc/mAC3xvm7zwS-https://prnt.sc/M8NHuAahl6E5https://prnt.sc/QTFXbFyL6qELhttps://prnt.sc/kfyHQhMTRg0u

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 4:
			Request to Set Deadlink Rankings Data in Samsung Mobile
			DB: F00
			Table: view_all_rankingsdata
			Date: 2/16/2023
			Retailer: www.coolblue.nl
			Keyword(s):smartphone
			Screenshot(s):https://prnt.sc/lFJ1Bdy6PThf
			Reason: Fetching Data even if its Deadlink (Top Level Category)

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

# ----- """""" Arresting Wire -----	

# REBUILD
	# target directory to rebuild
		'cd /spiders/<target directory>'
		'git fetch && git pull' or in GUI

		# no need to rebuild if latest commit is yesterday and no new commit today for specific spider
			# comparison
				'. deploy-production.sh 202302100942-update-title-for-microsoft-com-en-gb'

				# '. deploy-production.sh 202302051014-update-availability-for-otto-de'

				# '. deploy-production.sh 202211201055-add-hard-timeout-for-elgiganten-se'

			# rankings
				# '. deploy-production.sh 202302091108-update-pagination-for-noelleeming-co-nz'

				# '. deploy-production.sh 202208141504-add-build-pipelines'

				# '. deploy-production.sh 202210131544-add-unblocker-for-pcgarage-ro'

			# listings
				# '. deploy-production.sh 202302081351-implement-strategy-for-vatanbilgisayar-com'

				# '. deploy-production.sh 202302041505-implement-strat-for-store-acer-com-fr-fr'

				# '. deploy-production.sh 202212081455-update-product-query-selector-for-hellotv-nl'

			# . deploy-production.sh <yyyy-mm-hhmm>-<commit name>
			# commit name should be all lower case and replace spaces with dashes
			# this is the rebuild command
			# it should be always up to date

			# webhshots (webshot-nodejs)
				# . deploy-production.sh 202208211612-sanborns-com-mx-update-custom-actions

				# . deploy-production-override.sh 202208211646-sanborns-com-mx-update-custom-actions

			# ps
				# . deploy-production2.sh 202210131500-update-checker-for-gigantti-fi

	# check logs
		'df -h'
			# watch on /dev/sdal
			# watch on use %
				# it shouldnt reach 99%, mostly reaches 1 month to 99%
				# if it reaches greater than 94%, it needs to be deleted, 
					* '<remove image command>'
						# only intermediates do this command

	# config (go to cluster if r)
			'az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2'
			# tunong and gusto tanawun
			# maoy tanawun nga metric
		# OTHER COMMANDS
			# az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
			# az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
			az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
			az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction

			# az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
			az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction

			# az aks get-credentials --resource-group crawlers --name AKSListingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSListingsETProduction

			az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSListingsLProduction

			az aks get-credentials --resource-group crawlers --name AKSWebshotProduction2

			az aks get-credentials --resource-group crawlers --name AKSPhysicalStoreProduction2

	# check pods
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=webshot-nodejs'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=ws-ov-nodejs'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=physstores'

	# remove image
		# ask permission first to sir Rav or seniors
		# cd to spider (only to comparison, rankings and listings, no need et and special spiders (puppeteer))
			# Remove images - change grep "app" 
			'docker images | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "listing" | awk '{print $1 ":" $2}' | xargs docker rmi'	

		# new delete image by sir Rav. Updated: https://prnt.sc/b26xl8wSDzfF
			Comparisons: 'docker images -a | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			Rankings: 'docker images -a | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			Listings: 'docker images -a | grep "listings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			

		# for https://prnt.sc/w172FMRfZiMx 
			kubectl get pods | grep ContainerStatusUnknown | awk '{print $1}' | xargs 
			kubectl delete podkubectl get pods | grep Evicted | awk '{print $1}' | xargs
			kubectl delete podkubectl get pods | grep OOMKilled | awk '{print $1}' | xargs kubectl delete pod