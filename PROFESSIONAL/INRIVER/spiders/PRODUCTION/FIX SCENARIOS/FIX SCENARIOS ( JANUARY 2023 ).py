01 02 23
	timesheet
		08:00 - 17:00

		eval
		08:00 - 09:00

		garmin
		09:00 - 13:48

		zound 
		13:48 - 1700

	(F D-1) (R D-2) 6018	2023-01-02	New CMS	OCR - 100	all	www.fjellsport.no	
		AVSTANDSMÅLER
		GPS KLOKKE
		SMARTKLOKKE
		smart watch

		1 Day Auto Copy Over	
		https://prnt.sc/tjMcZX_DNtM2
		https://prnt.sc/rRYnLGmoMwcV
		https://prnt.sc/xCkc5J0C6pKA

		Yes	reymark

		SELECT keyword, ritem_id, country, source, COUNT(1) FROM view_all_rankingsdata var  WHERE date = '2023-01-02' 
				AND website = 'www.fjellsport.no' 
				AND keyword in (N'AVSTANDSMÅLER',
					'GPS KLOKKE',
					'SMARTKLOKKE')
				GROUP BY keyword, ritem_id, country, source

		{
		    "response": {
		        "task_uuid": "055eb669-45ac-41ec-834e-d7741b962fe3"
		    },
		    "version": "1.0"
		}

		# smart watch

		{
		    "response": {
		        "task_uuid": "45f27b2d-a853-47e9-a332-fd3545c56036"
		    },
		    "version": "1.0"
		}

	SQL update:
		(SP D-5) Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2023-01-02

			Field for update: in_stock,is_new and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_is_new.sql
				# 3p (SDA) > 3p_update_condition_and_is_new.sql

01 04 22
	timesheet
		07:56 - 17:11

		coke
		07:56 - 13:15

		eval
		13:15 - 13:30

		coke
		13:30 - 17:11

	CCEP runs
		* activate workers
		* no validation

01 05 23
	timesheet
		07:57 - 17:00

		coke 
		07:57 - 13:15

		eval
		13:15 - 13:30

		zound
		13:30 - 14:00

		coke
		14:00 - 17:00

	SQL update:
		(SP D-1) Request 1:
			Request to Set Deadlink Rankings Data in Samsung WG
			DB: O00
			Table: view_all_rankingsdata
			Date:01/5/2023
			Retailer:

			www.vandenborre.be/nl

			Keywords:

			Dolby Atmos soundbars
			Wifi soundbar
			grote tv
			Stofzuiger zonder zak

			Screenshot(s):
			https://prnt.sc/OfkX_PBIZt8W
			https://prnt.sc/3goBQ7Kt7LRw
			https://prnt.sc/bo3DresGPbNY
			https://prnt.sc/mCiUEnumTN6u

			Reason: Fetching data even if its Deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 2:
			Request to Set Deadlink Rankings Data in Zound

			DB: 200

			Date: 01/05/2023
			Retailer: www.elkjop.no
			Keyword: lyddempende hodetelefoner
			Screenshot: https://prnt.sc/jDciZmoIk3_n

			Reason: Fetching data even if its Deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

		(SP D-1) Request 3:
			Request to Set Deadlink Listings Data in Samsung WG

			DB: O00

			Table: view_all_listingsdata

			Date: 01/05/2022

			Retailer: www.krefel.be/nl

			Category(ies):
			TV 2020

			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63

			Screenshot(s):
			https://prnt.sc/OlrpY53rPIls

			Reason: Fetching Data even if its Deadlink (Top Level Category)

			# exec:
				# listings > list_set_to_dead_link_by_date+listings_uuid+postal.sql

		(SP D-1) +Request 4:
			Request to Set Deadlink Rankings Data in Samsung WG

			DB: O00

			Table: view_all_rankingsdata

			Date:01/5/2023

			Retailer:

			www.krefel.be/nl

			Keyword(s):

			Energiezuinige koelkast

			Screenshot(s):
			https://prnt.sc/VLsGe5aCs7TB

			Reason: Fetching data even if its Deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

	(SP D-15) CCEP validations and runs

01 06 23
	timesheet
		07:58 - 18:16

		eval
		07:58 - 08:15

		msprice
		08:15 - 10:24

		msprice
		13:26 - 14:38

		zound 
		14:38 - 18:16

	Webshot
		(w) Request 1:
			EMEA region

		(w) Request 2:
			NA region

	SQL update:
		Request 1:
			Request for update in ZOUND SDA

			DB: 200
			Table: view_3P_data
			Date: 2023-01-06

			Field for update: retailer_name, is_new and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_condition_and_is_new.sql
				# 3p (SDA) > 3p_update_is_new.sql
				
01 07 23
	timesheet
		edit time to 17:00
		07:51 - 23:33

		eval
		07:51 - 08:00

		zound
		08:00 - 13:49

		msprice
		13:49 - 14:39

		zound
		14:39 -	17:00

	SQL update:
		(SP D-2) Request 1:
			Request for copy over in ZOUND

			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2023-01-06
			Target Date: 2023-01-07
			ItemID:

			200FRMS070000001382730202104150735
			200FRMS0NC000001382760202104150735
			200FRMS070000001382780202104150735
			200FRMS070000001382840202104150735
			200FRMS070000001220960202010261317
			200FRMS070000001398360202105040415
			200FRMS080000001382870202104150735
			200FRMS080000001382910202104150735

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-2) Request 2:
			DB: 200
			Table: view_all_productdata and view_3P_data
			Source Date: 2023-01-06
			Target Date: 2023-01-07
			ItemID:

			200FRNS070000001220590202010261317

			# exec:
				# 3p (SDA) > 3p_copy_over_by_date+item_id.sql
				# product > copy_over > prod_copy_over_by_date+item_id.sql

		(SP D-2) Request 3:
			Request for update in ZOUND SDA


			DB: 200
			Table: view_3P_data
			Date: 2023-01-07

			Field for update:  is_new and condition

			Reason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_condition_and_is_new.sql
				# 3p (SDA) > 3p_update_is_new.sql

		(SP D-1) Request 4:
			Request to Set Deadlink Rankings Data in Zound

			DB: 200

			Date: 01/07/2023
			Retailer: www.elkjop.no
			Keyword: lyddempende hodetelefoner
			Screenshot: https://prnt.sc/9kcmXyC7I5aj

			Reason: Fetching data even if its Deadlink

			# exec:
				# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

	Webshots:
		Request 1:
			NA region

01 08 23
	timesheet
		07:53 - 17:19

		eval
		07:53 - 09:00

		manufacturing demo
		09:00 - 13:43 

		ms price
		13:43 - 14:57

		zound
		14:57 - 17:19


	Daily Issues:
		(R D-2) 14083	2023-01-08	210	all	amazon.de	all item_ids	
			NO DATA		
			Yes	Crestine

			SELECT count(1) FROM view_all_productdata vap WHERE date = '2023-01-08'

			payload:
			{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "210",
			            "run_mode": "normal_run",
			            "schedule_time": "22:00:00",
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.amazon.de"
			            ],
			            "data_points": []
			        }
			    }
			}

			# wrong scheduled date
			{
			    "response": {
			        "task_uuid": "15dfbcc1-5281-4d60-a958-6075feca77c9"
			    },
			    "version": "1.0"
			}

			# correct shceduled date
			{
			    "response": {
			        "task_uuid": "527911e8-a668-48e1-9b97-a0f73d6ea157"
			    },
			    "version": "1.0"
			}

		(R D-2) 14084	2023-01-08	310	all	
			jaktia.no
			outdoorexperten.se
			currys.co.uk
			all item_ids	NO DATA		
			Yes	Crestine

			payload_builder{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "310",
			            "run_mode": "normal_run",
			            "schedule_time": "23:00:00",
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.currys.co.uk"
			            ],
			            "data_points": []
			        }
			    }
			}

			# jaktia.no
			{
			    "response": {
			        "task_uuid": "43fe8e57-58dc-4d5f-b2bc-194c657401cd"
			    },
			    "version": "1.0"
			}

			# outdoorexperten.se
			{
			    "response": {
			        "task_uuid": "716e612d-3cf5-49d3-8450-b76c18a6eeec"
			    },
			    "version": "1.0"
			}

			# currys.co.uk
			{
			    "response": {
			        "task_uuid": "22168dcc-9a67-405e-9f71-c7842f688dd0"
			    },
			    "version": "1.0"
			}

		(R D-2) 14085	2023-01-08	D10	all	coolblue.nl	all item_ids	
			NO DATA		
			Yes	Crestine

			# payload
			{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "D10",
			            "run_mode": "normal_run",
			            "schedule_time": "22:00:00",
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.coolblue.nl"
			            ],
			            "data_points": []
			        }
			    }
			}

			# exec
			{
			    "response": {
			        "task_uuid": "da70d3f9-a620-4cad-a417-4d008f64bf97"
			    },
			    "version": "1.0"
			}

		(R D-2) 14086	2023-01-08	E10	all	elgiganten.se	all item_ids	
			NO DATA		
			Yes	Crestine

			# payload 
			{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "E10",
			            "run_mode": "normal_run",
			            "schedule_time": "22:00:00", 
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.elgiganten.se"
			            ],
			            "data_points": []
			        }
			    }
			}

			# elgiganten.se
			{
			    "response": {
			        "task_uuid": "93ce3947-f8c7-4306-97c6-1a58d0e3c058"
			    },
			    "version": "1.0"
			}

		(R D-2) 14087	2023-01-08	F10	all	
			bcc.nl
			mediamarkt.be/nl

			all item_ids	

			NO DATA		
			Yes	Crestine

			# payload
			{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "F10",
			            "run_mode": "normal_run",
			            "schedule_time": "22:00:00", 
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.mediamarkt.be/nl/" 
			            ],
			            "data_points": []
			        }
			    }
			}

			# bcc.nl
			{
			    "response": {
			        "task_uuid": "d826b0cc-b580-46b1-b7d0-3de4b1d2d00d"
			    },
			    "version": "1.0"
			}	

			# mediamarkt.be/nl
			{
			    "response": {
			        "task_uuid": "6941b161-4ca3-4a61-9718-92c92bb1f94d"
			    },
			    "version": "1.0"
			}

		(R D-2) 14088	2023-01-08	G10	all	zalando.de	all item_ids	
			NO DATA		
			Yes	Crestine

			# payload
			{
			    "version": "1.0",
			    "request": {
			        "task_name": "Normal Run 2023-01-08",
			        "user_uuid": "1Q2W3E4R5T6Y7U8I9O0P",
			        "data": {
			            "company": "G10",
			            "run_mode": "normal_run",
			            "schedule_time": "22:00:00",
			            "schedule_date": "2023-01-08",
			            "retailers": [
			                "www.zalando.de" 
			            ],
			            "data_points": []
			        }
			    }
			}

			{
			    "response": {
			        "task_uuid": "c4808521-a943-4873-b25c-6d5f1226ef5f"
			    },
			    "version": "1.0"
			}

		SQL update:
			(SP D-1) Request 1:
				Request to Set Deadlink Rankings Data in Zound

				DB: 200

				Date: 01/07/2023
				Retailer: www.elkjop.no
				Keyword: lyddempende hodetelefoner
				Screenshot: https://prnt.sc/ZF0mwm-PLQH9

				Reason: Fetching data even if its Deadlink

				# exec:
					# rankings > set_to_dead_link > rank_set_to_dead_link_by_date+retailer+keyword.sql

			(SP D-1) Request 2:
				Request for update in ZOUND SDA

				DB: 200
				Table: view_3P_data
				Date: 2023-01-08

				Field for update: condition

				Reason:, -2 valuesw

				# exec:
					# 3p (SDA) > 3p_update_condition.sql

		Webshots:
			(W) Request 1:
				NA region

01 11 23
	timesheet
		17:59 - 19:00

		coke 07:59 - 13:15
		coke 13:45 - 19:00
		
	(SP D-20) CCEP
		/ spider runs

01 12 23
	timesheet
		ccep
		08:00 - 13:30

		eval
		13:30 - 13:45	

		ccep
		13:45 - 22:40

	(SP D-40) CCEP
		/ duplicate deletion
		/ extracting incorrect counts

01 13 23
	timesheet
		ccep
		06:38 - 13:15

		eval
		13:15: 13:30

		ccep
		13:30 - 19:36

	(SP D-30) CCEP
		/ duplicate deletion
		/ extracting incorrect counts

01 14 23
	timesheet
		08:00 - 18:00

		coke
			08:00 - 13:15

		eval
			13:15 - 13:30

		coke 
			13:30 - 18:00

	(SP D-20) CCEP

01 18 23
	timesheet
		ccep
		07:10 - 13:15

		eval
		13:15 - 13:30

		eval
		13:45 - 17:00

	(SP D-10) CCEP runs

01 19 23
	timesheet
		06:31 - 20:25

		ccep:
		06:31 - 13:30

		eval
		13:30 - 13:45

		coke 
		13:45 - 17:00

		ms eeo:
		17:00 - 20:25

	(SPA D-15) CCEP
		/ runs and validation

	(Dev) listings development

01 20 23
	timesheet
		07:50 - 17:54

		ms eeo 
		07:50 - 13:15

		eval
		13:15 - 13:30

		ms eeo
		13:30 - 17:37

		miele
		17:37 - 17:54

	(Dev) listings development

	SQL update:
		(SP D-) Request 1:
			Request for Removal in MIELE

			DB: P10
			Table: productdata
			Date: 2023-01-20
			Retailer:

			www.currys.co.uk

			Reason: These are test date and should not be published in production. Compliance data have no separate publishing flag yet.

			# exec:
				# product > delete > prod_delete_all_by_date+retailer.sql

01 21 23
	timesheet
		ms eeo
			07:53 - 17:00

	(Dev) listings development

01 22 23
	time sheet
		08:00 - 17:00

		ms eeo
			08:00 - 08:39

		ms price
			08:39 - 10:16

		dap 
			10:16 - 15:00

		garmin
			15:00 - 17:00


	MSPrice
		(W) Request 1:
			emea region webshots

	(DEV) listings development

	Daily Issues:
		2023-01-22	New CMS	OCR-R10	all	www.lowes.com	
			Window Caulk	
			1 Day Auto Copy Over	
			https://prnt.sc/GQGOVLBcpYqI	
			Yes	Kristian

			{
			    "response": {
			        "task_uuid": "9402968c-8864-48e2-9866-5827f80b6fda"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "f02a48d7-1908-4068-9dd3-b0cbc699cd9f"
			    },
			    "version": "1.0"
			}

			# 2nd fix
			{
			    "response": {
			        "task_uuid": "b62e86d0-d37c-4cc0-bbd9-304f4c0c98d1"
			    },
			    "version": "1.0"
			}

		2023-01-22	New CMS	OCR-R10	all	www.lowes.com	
			Silicone Caulking	
			Duplicate Data	
			https://prnt.sc/m1H8-x4GF3Yj
			https://prnt.sc/ZWz6WAi9f7r0
			Yes	Kristian

			{
			    "response": {
			        "task_uuid": "4da98098-bfb7-4e2e-9aca-e00a6ac2166c"
			    },
			    "version": "1.0"
			}

			# 2nd fix
			{
			    "response": {
			        "task_uuid": "6b8a9773-00b4-4533-93ac-0b538488c1a7"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "7c5f00fd-81ce-4a60-a7da-c9e3a19c1bcb"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "ce4faf29-f1d4-4ced-8797-8d6d74e6849f"
			    },
			    "version": "1.0"
			}

		---
		(F D-1) (R D-2) 1778	2023-01-22	New CMS	OCR-100	all	www.sport1.no	
			Elektronikk	
			Invalid Deadlink	
			https://prnt.sc/80WkwpzyiczO	
			Yes	Rhunick

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2023-01-22' 
				AND website = 'www.sport1.no' 
				AND category in (N'Elektronikk')
				GROUP BY category, litem_id, country, category_url, listings_uuid, source

			{
			    "response": {
			        "task_uuid": "a17c1aa1-3b0f-4b46-a610-4806be6b9ca3"
			    },
			    "version": "1.0"
			}

01 25 23
	Time sheet:
		07:24 - 19:12

		ms eeo
		07:24 - 09:00

		ccep runs
		09:00 - 12:00

		ms eeo
		12:00 - 13:15

		eval
		13:15 - 13:30

		ccep runs
		13:30 - 17:00

		ms eeo
		17:00 - 19:12

	(SP D-10) CCEP
		/ runs

	(DEV) Rankings development
		/ mediamarkt.lu
		/ mediamarkt.pt
		/ officedepot.com.mx
		/ online.acer.com.au
		/ paris.cl

01 26 23
	timesheet
		07:51 - 20:13

		coke
		07:51 - 13:30

		Eval
		13:30 - 14:45

		coke
		14:45 - 18:00

		ms eeo
		18:00 - 20:13

	CCEP Coke

	(DEV) Rankings development
		/ noelleeming.co.nz
		/ microsoft.com/en-us
		/ officeworks.com.au

01 27 23
	Time sheet

		ms eeo
		07:54 - 13:15

		eval
		13:15 - 13:30

		MS Price
		13:30 - 14:22

		miele
		13:22 - 15:00

		ms eeo
		15:00 - 18:15

	* resolve sir Jave issue

	SQL update:
		(SP D-1) Request 1:
			Request for Removal in MIELEDB: P10
			Table: productdata
			Date: 2023-01-27
			Retailer:www.amazon.co.uk
			Reason: These are test date and should not be published in production. Compliance data have no separate publishing flag yet.

		# exec:
			# product > delete > prod_delete_all_by_date+retailer.sql

01 28 23
	timesheet
		07:50 - 17:05

		evaluate
		07:50 - 08:48

		ms price
		08:48 - 10:07

		nintendo brazil
		10:07 - 17:05

	MSPrice
		Request 1:
			EMEA region webshots

	Daily Request:
		2023-01-28	New CMS	OCR-U00	all	www.submarino.com.br	
			Controle Nintendo Switch	
			Duplicate Data	
			https://prnt.sc/TjMpcKVs_7FM	
			Yes	kurt

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.submarino.com.br' and 
				var2.keyword in (N'Controle Nintendo Switch') and 
				var2.[date] = '2023-01-28' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			# exec:
				{
				    "response": {
				        "task_uuid": "d118880f-56a5-42c2-8268-037ce84ba1f0"
				    },
				    "version": "1.0"
				}

				{
				    "response": {
				        "task_uuid": "a6b04fc4-d241-46f8-ac88-800054aeeb35"
				    },
				    "version": "1.0"
				}

			# after rebuild
				{
				    "response": {
				        "task_uuid": "175cd9b9-dce6-414e-998e-6f836603167b"
				    },
				    "version": "1.0"
				}

				{
				    "response": {
				        "task_uuid": "363b3962-b5a2-4f94-bad8-d1e0fd06dfd4"
				    },
				    "version": "1.0"
				}

			# after 2nd rebuild 	

01 29 23
	Timesheet
		07:52 - 17:07

		EEO
		07:52 - 08:35

		MS price
		08:35 - 10:14

		razer
		10:14 - 11:00

		dap
		11:00 - 12:30

		garmin
		12:30 - 14:08

		ms price 
		14:08 - 14:45

		garmin
		14:45 - 17:07
	
	Webshots
		(W) Request 1:
			EMEA region

	Daily Issues:

		(R D-1) 14168	2023-01-29	OCR-K00	price_value	euro.com.pl	
			K00PLD80CG000000980830202006250948 
			K00PLD80CG000000980840202006250948
			K00PLD80CG000001298300202101180406
			K00PLD80CG000001373110202104050819
			K00PLD80MD000001447680202106150451
			K00PLD80DO000001448000202106150451
			K00PLD80KO000001448030202106150451
			K00PLD805K000001448090202106150451
			K00PLD804K000001450090202106150623
			K00PLD80AD000001528160202108020954
			K00PLD80AD000001528260202108020955
			K00PLD804K000001528390202108020955
			K00PLD803F000001528460202108020955
			K00PLD805K000001528550202108020955
			K00PLD80CG000001592050202109200556
			K00PLD807U020211115034605887172319
			K00PLD807U020211115034607406756319
			K00PLD807U020211115034607722283319
			K00PLD807U020220214041205357669045
			K00PLD807U020220214041205764883045
			K00PLD807U020220214041205918486045
			K00PLD807U020220214041208493515045
			K00PLD807U020220425061225458347115
			K00PLD807U020220620051758004743171
			K00PLD807U020220620051803777495171
			K00PLD807U020220620055714358369171
			K00PLD807U020220620055714597096171
			K00PLD807U020220627045519586031178
			K00PLD807U020220627045520367725178
			K00PLD807U020220627045520605804178
			K00PLD807U020220627045521659028178
			K00PLD807U020220627045521951246178
			K00PLD807U020220627045522747928178
			K00PLD807U020220627045523020275178
			K00PLD807U020220627045523553044178
			K00PLD807U020220627045524386675178
			K00PLD807U020220627045524678905178
			K00PLD807U020221228014515361839362
			K00PLD807U020221228014515966446362
			K00PLD807U020230113034227954269013

			Wron gprice/ invalid -1 price 		
			Yes	Ash

			-- 01 29 23 Razer Wron gprice/ invalid -1 price 		
			SELECT * FROM view_all_productdata where date='2023-01-29' AND item_id in ('K00PLD80CG000000980830202006250948', 'K00PLD80CG000000980840202006250948', 'K00PLD80CG000001298300202101180406', 'K00PLD80CG000001373110202104050819', 'K00PLD80MD000001447680202106150451', 'K00PLD80DO000001448000202106150451', 'K00PLD80KO000001448030202106150451', 'K00PLD805K000001448090202106150451', 'K00PLD804K000001450090202106150623', 'K00PLD80AD000001528160202108020954', 'K00PLD80AD000001528260202108020955', 'K00PLD804K000001528390202108020955', 'K00PLD803F000001528460202108020955', 'K00PLD805K000001528550202108020955', 'K00PLD80CG000001592050202109200556', 'K00PLD807U020211115034605887172319', 'K00PLD807U020211115034607406756319', 'K00PLD807U020211115034607722283319', 'K00PLD807U020220214041205357669045', 'K00PLD807U020220214041205764883045', 'K00PLD807U020220214041205918486045', 'K00PLD807U020220214041208493515045', 'K00PLD807U020220425061225458347115', 'K00PLD807U020220620051758004743171', 'K00PLD807U020220620051803777495171', 'K00PLD807U020220620055714358369171', 'K00PLD807U020220620055714597096171', 'K00PLD807U020220627045519586031178', 'K00PLD807U020220627045520367725178', 'K00PLD807U020220627045520605804178', 'K00PLD807U020220627045521659028178', 'K00PLD807U020220627045521951246178', 'K00PLD807U020220627045522747928178', 'K00PLD807U020220627045523020275178', 'K00PLD807U020220627045523553044178', 'K00PLD807U020220627045524386675178', 'K00PLD807U020220627045524678905178', 'K00PLD807U020221228014515361839362', 'K00PLD807U020221228014515966446362', 'K00PLD807U020230113034227954269013') ORDER BY price_value


			{
			    "response": {
			        "task_uuid": "338bcaf7-8bb4-4172-bac2-53196f8750f9"
			    },
			    "version": "1.0"
			}

		--- 

		(F D-1) (R D-2) 6054	2023-01-29	New CMS	OCR-R10	all	www.lowes.com	
			paint caulk	
			1 Day Auto Copy Over	
			https://prnt.sc/KxvptHacb27n	
			Yes	Kristian

			SELECT date,website, keyword, product_website,product_url, ritem_id, country, source, COUNT(1) from view_all_rankingsdata var2 
				where var2.website = 'www.lowes.com' and 
				var2.keyword in (N'paint caulk') and 
				var2.[date] = '2023-01-29' 
				GROUP BY date,website, keyword, product_website,product_url, ritem_id, country, source

			{
			    "response": {
			        "task_uuid": "3242a601-bb83-44dd-846a-3e3679091448"
			    },
			    "version": "1.0"
			}

		---

		(R D-2) 1783	2023-01-29	New CMS	OCR-100	all	www.netonnet.no	
			GPS	
			duplicate data	
			https://prnt.sc/oj8WwARCxuwu	
			Yes	Rhunick

			SELECT category, litem_id, country, category_url, listings_uuid, source, COUNT(1) FROM view_all_listings_data var  WHERE date = '2023-01-29' 
				AND website = 'www.netonnet.no' 
				AND category in (N'GPS')
				GROUP BY category, litem_id, country, category_url, listings_uuid, SOURCE
				
			SELECT date, website, category, product_website, category_url, litem_id, country, source, COUNT(1) from view_all_listings_data var2 
							where var2.website = 'www.netonnet.no' and 
							var2.category in (N'GPS') and 
							var2.[date] = '2023-01-29' 
							GROUP BY date, website, category, product_website, category_url, litem_id, country, source
							
							
			SELECT * FROM view_all_listings_data WHERE date = '2023-01-29' AND website = 'www.netonnet.no' AND category in ('GPS') ORDER BY rank

			https://www.netonnet.no/art/hjem-fritid/bil-og-gps/gps-til-bil

			# remarks:
				Valid duplicate ang page 2:
				https://prnt.sc/M1hXpTTFcSfR

				Page 2:
				https://prnt.sc/N9-tVQk9kX-k

	SQL update:
		(SP D-1) Request 1:
			Request for update in ZOUND SDA
			DB: 200
			Table: view_3P_data
			Date: 2023-01-29Field for update: is_newReason:, -2 values

			# exec:
				# 3p (SDA) > 3p_update_is_new.sql

	Trello tickets:
		Trello 1:
			www.submarino.com.br
			Keyword: Controle Nintendo Switch
			Issue: Duplicate Data

			Screenshot:

			Screenshot

			as per BE said:

			Inconsistent result after 2 fixes and multiple reruns. Requires observation for next run

02 01 23

	Development assitance
		* smythstoys.com/ie/en-ie

	Webshots
		(W) Request 1:
			EMEA region

		Request 2:
			NA region

	Daily Issues:
		(R D-1) 14178	2023-02-01	OCR-K00	image_count	www.otto.de	
			K00DE0Z03O000001433020202106020354
			K00DE0Z03O000001433060202106020354
			K00DE0Z0DO000001433930202106020354
			K00DE9603F000001306140202101210429
			Missing image_count		Yes	Jurena

			-- 02 01 23 otto.de missing image
			SELECT * FROM view_all_productdata where date='2023-02-01' AND item_id in ('K00DE0Z03O000001433020202106020354', 'K00DE0Z03O000001433060202106020354', 'K00DE0Z0DO000001433930202106020354', 'K00DE9603F000001306140202101210429') ORDER BY source

02 02 23
	Webshots
		(W) EMEA region

		(W) NA region

	MSPrice
		(F D-1) Request 1:
			Hello! 
			Request to rerun gamestop.com.DB: A00
			Reason: Incorrect price
			Item ID: A00USM50RB000001247430202011230449

			SELECT * FROM view_all_productdata where date='2023-02-02' AND item_id in ('A00USM50RB000001247430202011230449') ORDER BY source

			{
			    "response": {
			        "task_uuid": "91243ae8-8944-4c77-b13d-4ead779e489c"
			    },
			    "version": "1.0"
			}

			# after rebuild
			{
			    "response": {
			        "task_uuid": "05fd7789-cb26-4b66-9c8f-8fe4b32145d7"
			    },
			    "version": "1.0"
			}

			{
			    "response": {
			        "task_uuid": "64253252-619e-471d-887f-01f4b92bd457"
			    },
			    "version": "1.0"	
			}

			{
			    "response": {
			        "task_uuid": "58cb13b7-56fb-42a4-96e7-088381a4a9f7"
			    },
			    "version": "1.0"
			}

	SQL update:
		(SP D-1) Request 1:
			Request to Set Deadlink Listings Data in Samsung WG
			DB: O00
			Table: view_all_listingsdata
			Date: 02/02/2023
			Retailer: www.krefel.be/nl
			Category(ies):
			TV 2020
			Listings UUID(s):
			e342c65e-1fae-4961-a983-5d663b82cd63

			# listings > set_to_dead_link > list_set_to_dead_link_by_date+listings_uuid+postal.sql

# ----- """""" Arresting Wire -----

# REBUILD
	# target directory to rebuild
		'cd /spiders/<target directory>'
		'git fetch && git pull' or in GUI

		# no need to rebuild if latest commit is yesterday and no new commit today for specific spider
			# comparison
				# '. deploy-production.sh 202301291513-update-specs-for-wayfair-com'

				# '. deploy-production.sh 202301291537-Update-availability-for-zalando-de'

				# '. deploy-production.sh 202211201055-add-hard-timeout-for-elgiganten-se'

			# rankings
				# '. deploy-production.sh 202301291752-update-dlstrat-for-lowes-com'

				# '. deploy-production.sh 202208141504-add-build-pipelines'

				# '. deploy-production.sh 202210131544-add-unblocker-for-pcgarage-ro'

			# listings
				# '. deploy-production.sh 202301291759-update-dl-and-ws-strategy-for-mediamarkt-ch-de'

				# '. deploy-production.sh 202301271521-Implement-strategy-for-alkosto-com'

				# '. deploy-production.sh 202212081455-update-product-query-selector-for-hellotv-nl'

			# . deploy-production.sh <yyyy-mm-hhmm>-<commit name>
			# commit name should be all lower case and replace spaces with dashes
			# this is the rebuild command
			# it should be always up to date

			# webhshots (webshot-nodejs)
				# . deploy-production.sh 202208211612-sanborns-com-mx-update-custom-actions

				# . deploy-production-override.sh 202208211646-sanborns-com-mx-update-custom-actions

			# ps
				# . deploy-production2.sh 202210131500-update-checker-for-gigantti-fi

	# check logs
		'df -h'
			# watch on /dev/sdal
			# watch on use %
				# it shouldnt reach 99%, mostly reaches 1 month to 99%
				# if it reaches greater than 94%, it needs to be deleted, 
					* '<remove image command>'
						# only intermediates do this command

	# config (go to cluster if r)
			'az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2'
			# tunong and gusto tanawun
			# maoy tanawun nga metric
		# OTHER COMMANDS
			# az aks get-credentials --resource-group crawlers --name AKSComparisonProduction
			# az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction
			az aks get-credentials --resource-group crawlers --name AKSComparisonProduction2
			az aks get-credentials --resource-group crawlers --name AKSComparisonETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSComparisonLProduction

			# az aks get-credentials --resource-group crawlers --name AKSRankingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction
			az aks get-credentials --resource-group crawlers --name AKSRankingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSRankingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSRankingsLProduction

			# az aks get-credentials --resource-group crawlers --name AKSListingsProduction
			# az aks get-credentials --resource-group crawlers --name AKSListingsETProduction

			az aks get-credentials --resource-group crawlers --name AKSListingsProduction2
			az aks get-credentials --resource-group crawlers --name AKSListingsETProduction2
			# az aks get-credentials --resource-group crawlers --name AKSListingsLProduction

			az aks get-credentials --resource-group crawlers --name AKSWebshotProduction2

			az aks get-credentials --resource-group crawlers --name AKSPhysicalStoreProduction2

	# check pods
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=comparison-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=rankings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-et'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=listings-ppt'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=webshot-nodejs'
			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=ws-ov-nodejs'

			'kubectl get pods -o wide --sort-by="{.spec.nodeName}" -l app=physstores'

	# remove image
		# ask permission first to sir Rav or seniors
		# cd to spider (only to comparison, rankings and listings, no need et and special spiders (puppeteer))
			# Remove images - change grep "app" 
			'docker images | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			'docker images | grep "listing" | awk '{print $1 ":" $2}' | xargs docker rmi'	

		# new delete image by sir Rav. Updated: https://prnt.sc/b26xl8wSDzfF
			Comparisons: 'docker images -a | grep "comparison" | awk '{print $1 ":" $2}' | xargs docker rmi'
			Rankings: 'docker images -a | grep "rankings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			Listings: 'docker images -a | grep "listings" | awk '{print $1 ":" $2}' | xargs docker rmi'
			