WEBSHOT TOOL AND BACKDOOR KT.

https://inriver-my.sharepoint.com/:v:/p/johndarie_silaba/EW4oSs9XvSlIlmBj_Qw0HesBdPrZpR9F1dcwrNxe_TnANQ

Webshot Plugin - Extension

PL:
- javascript

Repo:
- git@ssh.dev.azure.com:v3/inriver/Evaluate/be-webshot-plugin (Chrome extension)


After cloning repo:
- create Releases folder in the root directory
- archiver script. save as archiver.sh the shell script below

```
#!/usr/bin/env bash
databasename=$(cat manifest.json | sed 's/.*"version": "\(.*\)".*/\1/;t;d')

if test -z "$databasename"
then
echo "No version. Default version to 1.0.0."
databasename=1.0.0
else
echo "Version $databasename detected."
fi

echo "Archiving finished."

git archive -o Releases/detail-webshot-plugin-$databasename.zip HEAD
```

Update frequency:
- code changes: any new client that needs webshot

How to add new clients:
- get client name from relevant sources
- create new git branch (e.g. git checkout -b feature/add-client-)
- edit main.html and add the clients in the dropdown element
- edit manifest.json and increment the version number (the last number)
- commit changes in git
- run archiver.sh
- take the .zip file from Releases folder and upload to shared folder
- push branch to repo
- create pull request and add Jestoni Yap as reviewer


Webshot Plugin - API

Repo:
- git@ssh.dev.azure.com:v3/inriver/Evaluate/be-webshot-plugin-api (API)

PL:
- NodeJS

Update frequency:
- config update: dependent on be-webshot-plugin
- code changes: rarely

How to update config:
- login to 172.100.30.6 as detail user
- change directory to webshot-plugin-server-nodejs-2
- update config.json and add the new client name and code
- restart the pm2 processes server-prod2 and mapper-prod2
- pm2 stop server-prod2
- pm2 stop mapper-prod2)
- pm2 start server.js -i 3 --exp-backoff-restart-delay=100 --name server-prod
- pm2 start mapper.js -i 3 --exp-backoff-restart-delay=100 --name mapper-prod

In case of code changes:
- create new git branch (e.g. git checkout -b /)
- do changes
- commit changes in git
- push branch to repo
- create pull request and add Jestoni Yap as reviewer
- wait for approval and merge to main
- login to 172.100.30.6 as detail user
- change directory to webshot-plugin-server-nodejs-2
- git pull changes
- restart the pm2 processes server-prod2 and mapper-prod2
- pm2 stop server-prod2
- pm2 stop mapper-prod2)
- pm2 start server.js -i 3 --exp-backoff-restart-delay=100 --name server-prod
- pm2 start mapper.js -i 3 --exp-backoff-restart-delay=100 --name mapper-prod


Backdoor API

Repo:
- git@ssh.dev.azure.com:v3/inriver/Evaluate/be-validation-api (API)

PL:
- python

Update frequency: rarely. almost never

In case of code changes:
- create new git branch (e.g. git checkout -b /)
- do changes
- commit changes in git
- push branch to repo
- create pull request and add Jestoni Yap as reviewer
- wait for approval and merge to main
- login to 172.100.30.6 as detail user
- change directory to backdoor-api-production-2
- git pull changes