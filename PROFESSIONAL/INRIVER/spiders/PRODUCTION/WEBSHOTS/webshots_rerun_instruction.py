	1. activate this
		# D:\THREADSTONE\[3] CONFIGURATION\runner_tool_test
		# python app.py

	2. no webshots query
		# SELECT item_id, country, product_website, source FROM view_all_productdata where date='2022-12-08' AND item_id in ('A00USX00RB000001197560202008250920') and webshot_url is null and source = 'etl' ORDER BY webshot_url 

	3. paste item_ids to input_item_ids.txt
		# from input_item_ids.txt from 'D:\THREADSTONE\[3] CONFIGURATION\runner_tool_test\docs'

	4. execute script builder command
		# rerun_webshots -c A00 -s etl -d 2022-12-08

	5. copy from rerun_cmd_generated.txt 
		# from input_item_ids.txt from 'D:\THREADSTONE\[3] CONFIGURATION\runner_tool_test\docs'

	6. activate and execute in vm
		# 172.100.20.5 - Prod2
		# cd /opt/job-manager
		# source /opt/venv/bin/activate

		# then paste this (5. copy from rerun_cmd_generated.txt)

	7. webshots monitoring at:
		# D:\THREADSTONE\PROFESSIONAL\INRIVER\spiders\PRODUCTION\WEBSHOTS\excel files

# 