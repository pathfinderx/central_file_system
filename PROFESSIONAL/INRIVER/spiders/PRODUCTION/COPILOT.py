@LOOP
    # for compa 
        # except for image, means it is list
        for key, values in res.items():
            print(key, ' : ', res.get(key).get('value'))

    # for rankings
        for x in final_list:
            for y in x:
                print(y.get('title').get('value'))

        # title with brand
        for x in final_list:
            for y in x:
                print(y.get('title').get('value'), ' -> ', y.get('brand').get('value'))


    # for listings
        for x in res.get('products'):
            for y in x:
                print(y.get('title').get('value'))

        for x in res.get('products'):
            for y in x:
                print(y.get('rating').get('score').get('value'))

    # accessing complicated dictionary key
        # convert it to list so you can transform the complicated keys into an integers
            json_list = list(_json.get('props').get('apolloState').get('ROOT_QUERY').values())

            # example:
                dict = {'a;slkdjfowiejf;alksdjf':'1', 'asdijfoapiwejf;laksdjf....':'2'}
                json_list = list(dict)

                # json_list = [0:'1', 1:'2']

    # safety index deletion or prevention of shift index issue 
        # ref: C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\website\de\zalando_de.py

        products = soup.select('div.DT5BTM.w8MdNG article')
        if products:
            # check and remove invalid products (as per request from validator)
            index_of_invalid_products = []
            for i, v in enumerate(products):
                url_to_test = products[i].select_one('a._LM.JT3_zV.CKDt_l.CKDt_l.LyRfpJ').get('href')
                if url_to_test:
                    hasValidUrl = re.search(r'(zalando|html)', url_to_test) # temporary indicator for valid products (url that contains 'zalando' or 'html')
                    if not hasValidUrl:
                        index_of_invalid_products.append(i)
            if index_of_invalid_products:
                products = [values for index, values in enumerate(products) if index not in index_of_invalid_products] # deletion alternative to prevent inaccuracy of deletion due to shifting of index
            return str(products), products

    @specifications
        # removal of unwanted text as child node or child element or child tag, decompose alternative (use find('html_tag').extract() method)
        # ref: C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\website\br\shopb_com_br.py (at get_specification())
        # site ref: https://stackoverflow.com/questions/40760441/exclude-unwanted-tag-on-beautifulsoup-python
            tag = soup.select_one('div.abas-custom')
            if tag:
                specs_tag_parent = tag.select_one('ul')
                specs_tag_nodes = specs_tag_parent.select('li')

                if specs_tag_nodes:
                    source = str(tag)
                    for x in specs_tag_nodes:
                        if x.find('strong'):
                            specs_key = x.find('strong').get_text().replace(':','').replace('\xa0','')
                            x.find('strong').extract()
                            specs_value = x.get_text().replace('\xa0','')
                            value.update({specs_key:specs_value})

@CHECKERS
    # data type checker
        if isinstance (data, dict)

        if type(data) == dict

    # check soup from requester
        str(soup).find('keyword')

    # check if datatype has value
        if bool(product_id_container)

    # check if dict has a specific key
        if 'ProductList' in str(json_list[index])

    @ isinstance
        # from bs4 import BeautifulSoup, Tag, NavigableString
            # D:\THREADSTONE\2021 - 2022\[1]INRIVER\spiders\[1]comparison-spiders\ph\lazada_ph

            from bs4 import BeautifulSoup, Tag, NavigableString
            
            class checkInstance ():
                if isinstance (data, Tag):
                    pass

    # check if items in list is equal to a string
        # ref: https://www.adamsmith.haus/python/answers/how-to-check-if-a-string-contains-an-element-from-a-list-in-python
        # ref: C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\website\se\zoo_se.py

        self.brand_list = ["Acana","Brit Care","Canagan","Eukanuba","Granngården","Hill's","Hills","Nutrima","Orijen","Pure Natural","Purina","Royal Canin"]
        any(x in brand_text for x in self.brand_list)

@SELECTORS
    # works most of the times using itemprop style selector
    # ref: C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\website\br\mercadolivre_com_br.py
        tag = soup.select_one('input[name="itemId"]')

    # find all these tags:
        # ref: C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\website\es\mediamarkt_es.py (at get_description())
        tag.find_all(['p', 'h3', 'strong'])

    # or in one selector
        # ref: C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\website\ro\emag_ro.py (on get_ratings)
        tag_score_option = soup.select_one('a.rating-text.gtm_rp101318' or 'a[href="#reviews-section"]')

@STRINGS
    # C:\repositories\FREEZED (FORMER)\extract-transform-comparison\transformers\us\fastenal_com.py

    def transform_description (self, value):
        stripper = re.compile('<.*?>')
        stripped = re.sub(stripper, '', value)
        value = html.unescape(stripped).strip()
        return value


    # unicode escapes
    # C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\website\de\alternate_de.py
    def decodeUTF8 (self, data):
        encoded = str(data).     ('utf_8') # encode to bytes
        decoded = encoded.decode('unicode_escape')
        stripped = decoded.replace('\\','')
        strippedSoup = BeautifulSoup(stripped, 'lxml')
        return strippedSoup

    # mostly used
        tag.get_text().strip()

    # alternative if there are unusual characters and html tags left
    ' '.join(descriptionTag.stripped_strings)

    # remove trailing spaces or remove multiple succeding spaces
        # ref: C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\website\es\amazon_es.py
        if special_description:
                special_text = ''.join([x.get_text().strip() for x in special_description])
                stripped_text = re.sub('[\s]{2}', "", description)
                description += ' ' + stripped_text if stripped_text else None

    # alternative for trailing spaces when re.sub() doesnt work well (or failed to deal with two trailing spaces)
        ' '.join(value.split())

    # spacing issues when using get_text, especially with child elements that also have texts inside
        # ref: C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\website\de\alternate_de.py
            title_tag.get_text(" ", strip=True)

@REGEX
    # find script using regex
        soup.find('script', text = re.compile ('props'))

    # regex for sku (sequece: A123B)
        # C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\download\us\grainger_com.py

        if not sku:
            raw_sku = self.global_url.split('-')[-1]
            sku_test = re.match("^(?!.*(:[a-zA-Z]{2,3}|[0-9]{2,3}))[a-zA-Z0-9]{3,5}", raw_sku) # check if not valid sku (valid sku is: combination of C'letter and numbers of 5 occurences)
            if not sku_test: 
                sku_match = re.findall('(?:[0-9]{1,3}|[A-Z]{2,3}|[0-9]{1,3}){3,5}', raw_sku) # reassign new sku
                sku = sku_match[0] if sku_match and len(sku_match) >= 0 else None # ternary operation

        url = f'https://www.grainger.com/product/info?productArray={sku}&n=true'

    # for obfuscations:
        # sample 1: C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\website\sk\telekom_sk.py (line 384)
            _rgx = re.search(r'window\.\__INITIAL\_STATE\__.=.({.*})', scrp_tag.get_text(strip=True))

        # sample 2: (C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\download\de\bergzeit_de.py) (line 113)
            match1 = re.search(r'(#trboModule_13014_94284_61_container.trboModuleContainer .trbo-content .trbo-canvas .trbo-stage .trbo-item .trbo-badge)([\s\S]+)([}]])',raw).group(2)
            match2 = re.search(r'(wrapParts = me.attr\()([\s\S]+)',match1).group(2)
            match3 = re.search(r'(numberOfProducts)([\s\S]+)', match2).group(2)
            match4 = re.search(r'(trboModule_13014_94284_61_instance.setupProductSlider\(\[)([\s\S]+)',match3).group(2)


    # best method:
        # sample: C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\website\ru\iport_ru.py
            parent_tag = soup.select_one('#catalog-listing')
                if parent_tag:
                    product_tags = parent_tag.find_all('div',{'class':re.compile(r'CatalogGridstyles__CatalogGrid__Item.*')})

@THIRPARTY (3P)
    # if good in local (compa and et) but wont reflect in server after several reruns,
        # add increase timeout to 10
        # C:\repositories\FREEZED (FORMER)\comparison-spiders\strategies\download\fi\hintaopas_fi.py
        # WARNING! THIS IS UNVALIDATED YET because it fails during rerun on (03 03 22 (ticket 3823))

        def download_api_data(self, url, headers):
        text = None
        response = requests.get(url, headers=headers, timeout=randint(10,60)) # this
        print('Request 1 -- '+str(response.status_code))
        if response.status_code not in [200, 201]:
            response = self.requester.get(url, headers=headers, timeout=randint(10,60)) # this
            print('Request 2 -- '+str(response.status_code))
        if response.status_code not in [200, 201]:
            response = self.scraper.get(url, headers=headers,proxies=self.requester.session.proxies)
            print('Request 3 -- '+str(response.status_code))
        if response.status_code in [200, 201]:
            text = response.text

        return text

@DOWNLOAD STRATEGIES
    # cookies  
        # dynamic cookies (using cloudscraper)
            # C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\us\grainger_com.py

            # Effective on invalid deadlink issue specially when using unblocker of cloudscraper
                # ref: C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\br\submarino_com_br.py
                # has data on local using cloudscraper but invalid deadlink on main server
                # applied dynamic cookies (cloudscraper cookies) with normal requester
                # worked smoothly on main server

        class DynamicCookies ():
            # import the cloudscraper
            import cloudscraper
               
            # initialize both requester and scraper
            def __init__ (self, requester):
                self.requester = requester
                self.scraper = cloudscraper.create_scraper()
                self.scraper.proxies = requester.proxies # obsolete?

            def download(self, url, postal_code = None, timeout=10, headers=None, cookies=None, data=None):
                # ..... statements .....

                cookies = self.get_cookies(url)
                try:
                    res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

                    # ..... statements .....
                except Exception as e:
                    raise DownloadFailureException ('Download failed - Unhandled exception')

            def get_cookies (self, url):
                res = self.scraper.get(url, proxies = self.requester.session.proxies)
                cookies = res.cookies.get_dict()
                if cookies:
                    cookies['pv'] = 'list'
                return cookies

    # kwargs optional dynamic cookies or cloudscraper requester.get
        try:
            product_list = self.request_data_using_scraper(urls[page], timeout, headers, cookies, state='cookie')
            if product_list:
                raw_data_list.append(product_list)
            else:
                product_list = self.request_data_using_scraper(urls[page], timeout, headers, cookies, state='scraper')
                if product_list:
                    raw_data_list.append(product_list)
        except Exception as e:
            print(e)

        def request_data_using_scraper (self, url, timeout, headers, cookies, **kwargs):
            try:
                res = None
                if kwargs['state'] == 'cookie':
                    cookies = self.get_cookies(url)
                    res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                elif kwargs['state'] == 'scraper':
                    res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200,201]:
                    product_list = self.get_raw_data(res.text)
                    return product_list

                if not self.next_page(res.text):
                    None

            except Exception as e:
                print(e)

    # unblocker
        from request.unblocker import UnblockerSessionRequests
        self.unblocker = UnblockerSessionRequests('us')

    # dynamic unblocker cookies
        cookies = self.unblocker.session.cookies.get_dict()
        res = self.scraper.get(url, timeout=10, headers=headers,cookies=cookies)

    # redirect?
        for history in res.history:
            if history.status_code in [302, 301]:
                raw_data = None
                break

@URL modification
    # getting base url
        f'https://{self.__class__.WEBSITE}'

    # parsing
        import urllib, json

        payload = {
            ...
        }

        # parse the url
        parsed_url = urllib.parse.urlencode(payload)

        # dump the url through requester (only for post request)
        res = requester.post(..., data=json.dumps(payload))

    # payload parsing
        from urllib.parse import urlencode, quote

        payload = {
            'appMode': 'b2c',
            'user': 'anonymous',
            'operationName': 'getStoresByLocationWithProduct',
            'variables': '{"articleNumber":'+ json.dumps(articleNumber) +',"latitude":null,"longitude":null,"myStoreId":"3070"}',
            'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"ee66aa5be34447d95dfc73bea5f8de9ad2a0754c674b95f0a12544b1a36eeea2"}}'
        }

        queries = urlencode(payload, quote_via=quote)
        url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(queries)

    # playing with params if json.dumps() and urlencode() doesnt work
        # ref: C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\download\es\mediamarkt_es.py
        # ref(more url modifications here): C:\repositories\FREEZED (FORMER)\ranking-spiders\strategies\download\es\mediamarkt_es.py 
            params = {}
            params['operationName']='GetRecoProductsById'
            params["variables"] = {
                "ids":product_ids,
                "withMarketingInfos":'false'
            }
            params["extensions"] = {
                "persistedQuery":{
                    "version":1,
                    "sha256Hash":"ba1251c7568dcaaa8829135e77f9a490820e5cb020a2336df176894ec4cddcae"
                },
                "pwa":{
                    "salesLine":"Media",
                    "country":"ES",
                    "language":"es"
                }
            }

            headers = self.get_headers()
            headers['x-operation'] = 'GetRecoProductsByIds'

            url = 'https://www.mediamarkt.es/api/v1/graphql?operationName={}&variables={}&extensions={}'.format(params['operationName'],params['variables'],params['extensions'])
            url = url.replace(' ','').replace("\'", '"').replace('"false"','false')

        # OPTIONS
            quote()
            json.dumps()
            json.dumps(variables, ensure_ascii=False)


@PPT
    # selector for button with multiple attributes or data
        let selector3 = `.tileproductplacement button[data-pid="${variantId}"][data-inventoryid="${inventoryId}"]`
        # ref: C:\repositories\FREEZED (FORMER)\puppeteer-comparison-spiders\strategies\us\microsoft-com-en-us.js
            # line 233

@ PYTHON CONSTANTS
    # used only for 1 time call or 1 time variable assignment
    # There is no build-in module for python constants nor primitive call for constants. Do this instead:
        if not self.url: # if theres no value, put value. The next time it is called it is not null again
            self.url = url

        # or use private method

@ IMBA MOVES
    # ref(dlstrat): C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\br\shopb_com_br.py
    # ref(wsstrat): C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\website\br\shopb_com_br.py
   
------------------------------
@VSCODE ESSENTIAL SHORTCUTS
    # pallette: search for specific file
        'ctrl + p'

    # copy active file path
        'shift + alt + c'

    # go to specific line
        'ctrl + g'
        # then select the line number

    # adding breakpoint
        'click line + F9'

@Sublime 
    #  collapse first level
        'ctrl + k + (1)'

    # collapse second level
        'ctrl + k + (2)'

@ OTHERS
    # initialize the logger?
        # ref: C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\website\sk\telekom_sk.py
            # line 17
        self.logger = logging.getLogger(__name__)