-- Run this on running_man db
-- GET RETAILER WITH ALL DEAD LINK JOBS --
declare @date varchar(100);
declare @company_tbl as table(company varchar(10));
set @date = '2022-10-07'
insert into @company_tbl(company)
values ('C00'),('R00'),('B10'),('C10'),('E10'),('D10'),('I10')

select
	retailer,
    case
        when data_id like 'RANK@%' then 'rnk'
        when data_id like 'LIST@%' then 'lst'
        else 'cmp'
    end as metric,
    company,
	--sum(iif(process_name like 'write_jobs%', 1, 0)) as write_jobs,
	sum(iif((process_name like '%spider' or process_name like '%spider_rerun') and log_type <> 'error', 1, 0)) as spider_successes,
	sum(iif((process_name like '%spider' or process_name like '%spider_rerun') and log_type = 'error', 1, 0)) as spider_failures,
	--sum(iif(process_name like '%transform%', 1,	0)) as transforms,
	--sum(iif(process_name like '%load%', 1, 0)) as loads,
	sum(iif(process_name like '%dead_link' and log_type <> 'error', 1, 0)) as dead_link_successes,
	sum(iif(process_name like '%dead_link' and log_type = 'error', 1, 0)) as dead_link_failures,
	sum(iif(process_name like '%copy_over' and log_type <> 'error', 1, 0)) as copy_over_successes,
	sum(iif(process_name like '%copy_over' and log_type = 'error', 1, 0)) as copy_over_failure
from rm_internal_log
where v_schedule_date = @date and
	company not in (select company from @company_tbl)
group by
    case
        when data_id like 'RANK@%' then 'rnk'
        when data_id like 'LIST@%' then 'lst'
        else 'cmp'
    end,
    company,
    retailer
having
    --sum(iif(process_name like 'write_jobs%', 1, 0)) > 0 and
    sum(iif((process_name like '%spider' or process_name like '%spider_rerun') and log_type <> 'error', 1, 0)) = 0 and
	sum(iif((process_name like '%spider' or process_name like '%spider_rerun') and log_type = 'error', 1, 0)) > 1 and
	sum(iif(process_name like '%transform%', 1,	0)) = 0 and
	sum(iif(process_name like '%load%', 1, 0)) = 0
order by
    metric, retailer, company