
from urllib.parse import urlencode, quote
import json
import re

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class MilrabSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.is_api = False

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 
                'accept-encoding': 'gzip, deflate', 
                'accept-language': 'en-US,en;q=0.9', 
                'cache-control': 'max-age=0', 
                'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"', 
                'sec-ch-ua-mobile': '?0', 
                'sec-ch-ua-platform': '"Windows"', 
                'sec-fetch-dest': 'empty', 
                'sec-fetch-mode': 'navigate', 
                'sec-fetch-site': 'same-origin', 
                'upgrade-insecure-requests': '1', 
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code
            
            result = {}
            if status_code in [200, 201]:
                is_redirect = False

                if not is_redirect:
                    if 'content="og:product"' in res.text \
                        or '__iso__Item __iso__Item-wrapper' in res.text: # PDP Checker
                        result['res'] = res.text
                        result['url'] = url
                        
                        return json.dumps(result, ensure_ascii=False)
                    else:
                        headers = {
                            'accept': 'application/json',
                            'content-type': 'application/json',
                            'referer': url,
                            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
                        }
                        data = self.extra_download(url+'?json=true', timeout=timeout, headers=headers, cookies=cookies)
                        try:
                            if data:
                                result = json.loads(data)
                                if 'components' in result:
                                    result['url'] = url + '?json=true'
                                    self.is_api = True
                                    return json.dumps(result)
                        except:
                            pass

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def extract_api_video(self, pid, title, brand_name, ean, origin_url):
        # original_callback = '_vdlyc00e6161f4'
        try:
            callback = '_vdly5649cdd13d'
            dict = {
                'productId':'I-'+pid,
                'productTitle': title,
                'brandName':brand_name,
                'ean': ean,
                'SKU': pid,
                'oos':'0',
                'maxItems':'15',
                'ytwv':'', 
                '_b':'Chrome',
                '_bv':'94.0.4606.81',
                'p':'1',
                '_w':'1351',
                '_h':'313',
                '_pl':'sv',
                # 'ownVideos':'%7B%22ytVideoId%22%3A%22t275-6Uyc_o%22%7D%5D',
                'tsltd':'0',
                'callback': callback,
                'hn':'www.milrab.se',
                'href': origin_url,
                'sId':'s%3AwI8hbRg5Pi5DTCFkSvvvcYlJV08aiyA0.9O6DpiPfs%2B0gOuJ87D0xVNB4ZeE6JaN6s2V6yRpE%2FG8',    
            }

            headers = {'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36'}

            queries = urlencode(dict, quote_via=quote)
            api_url = 'https://dapi.videoly.co/1/videos/0/792/?{}'.format(queries)
        
            dapi_data = self.requester.get(api_url, headers=headers).text
            match = re.search(r'^.+?\((.+)\)\;', dapi_data )

            _json = json.loads(match.group(1))
                
            _json.update({'url': api_url})

            return _json
        except:
            pass

        return {}

    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text

            return None
            # raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            return None
            # raise DownloadFailureException('Download failed - Unhandled Exception')
