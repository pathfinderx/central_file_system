import json
import logging
from urllib.parse import quote
import html
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class MilrabSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.milrab.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = None
        _json = {}
        if self.downloader.is_api:
            _json_data = json.loads(raw_data)
            source_url = _json_data['url']
            api_data = _json_data['components']
        else:
            soup = BeautifulSoup(json.loads(raw_data)['res'], "lxml")
            source_url = json.loads(raw_data)['url']
            _json = self.get_json(soup)
            _json.update({ "url" : source_url})
            

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, api_data)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup, api_data)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, api_data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup, api_data)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup, api_data)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup, api_data) 

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup, api_data)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup, api_data)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup, _json)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, api_data)

        return result

    def get_json(self, soup):
        tag = soup.find('script', {'type':'application/ld+json'})

        if tag:
            data = tag.get_text().strip()
            data = json.loads(data, strict=False)

        return data

    def get_price(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                tag = soup.find('script', {'type':'application/ld+json'})

                if tag:
                    price = self.get_json(soup)

                    if price and 'offers' in price.keys() and 'price' in price['offers']:
                        source, value = str(tag), str(price['offers']['price'])
                    elif 'offers' in price:
                        value = str(price['offers'].get('lowPrice', None))
                        source = str(price)
                    else:
                        tag = soup.select_one('.__iso__Item-priceBlock .__iso__Item-price')
                        if tag:
                            value = tag.get_text().strip()
                            source = str(tag)
            elif soup is None:
                tag = api_data['content']['value']['gtmProduct']
                if 'price' in tag:
                    source = json.dumps(tag)
                    value = tag['price']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                tag = soup.find('script', {'type':'application/ld+json'})

                if tag:
                    currency = self.get_json(soup)

                    if currency and 'offers' in currency.keys() and 'priceCurrency' in currency['offers']:
                        source, value = str(tag), currency['offers']['priceCurrency']
            else:
                tag = api_data['content']['value']['gtmProduct']
                if 'currencyCode' in tag:
                    source = json.dumps(tag)
                    value = tag['currencyCode']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                tag = soup.select_one('.__iso__Item-addToBackInStock') \
                    or soup.select_one('.__iso__Item-addToBasket')
                if tag:
                    value = tag.get_text().strip()
                    source = str(tag)

                if value == 'NOT_FOUND':

                    tag = soup.find('script', {'type':'application/ld+json'})

                    if tag:
                        availability = self.get_json(soup)
                        oosButForDelivery = soup.select_one('span.b-show-stock__out-of-stock-with-eta')
                        if oosButForDelivery:
                            source, value = str(oosButForDelivery), oosButForDelivery['class'][0] 
                        else:    
                            if availability and 'offers' in availability.keys() and 'availability' in availability['offers']:
                                source, value = str(tag), availability['offers']['availability']
            else:
                skus = api_data['content']['value']['skus']
                if len(skus) > 0 and 'isAvailableInStock' in skus[0]:
                    source, value = json.dumps(skus[0]), str(skus[0]['isAvailableInStock'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup, api_data):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                code = self.get_code(soup)
                _json = self.get_ratings_from_api(code)
                if _json:
                    if 'your_review_count' in _json:
                        reviews_value = str(_json['your_review_count'])
                        reviews_source = str(_json)
                    if 'your_avg_score' in _json:
                        score_value = str(_json['your_avg_score'])
                        reviews_source = str(_json)
            else:
                code = api_data['content']['value']['productCode']
                ratings_api = self.get_ratings_from_api(code)
                if ratings_api:
                    if 'your_review_count' in ratings_api:
                        reviews_value = str(ratings_api['your_review_count'])
                        reviews_source = str(ratings_api)
                    if 'your_avg_score' in ratings_api:
                        score_value = str(ratings_api['your_avg_score'])
                        reviews_source = str(ratings_api)
            # tag = soup.find('script', {'type':'application/ld+json'})

            # if tag:
            #     review = self.get_json(soup)
            #     rating = self.get_json(soup)

            #     if review and 'aggregateRating' in review.keys() and 'reviewCount' in review['aggregateRating']:
            #         reviews_source, reviews_value = str(tag), str(review['aggregateRating']['reviewCount'])

            #     if rating and 'aggregateRating' in rating.keys() and 'ratingValue' in rating['aggregateRating']:
            #         score_source, score_value = str(tag), str(rating['aggregateRating']['ratingValue'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                _json = self.get_json(soup)
                if _json and 'name' in _json:
                    value = html.unescape(_json['name'])
                    source = str(_json)
                else:
                    tag = soup.select_one('h1.partname') or soup.select_one('.__iso__Item.__iso__Item-title')
                    if tag:
                        source = str(tag)
                        # FORCE REPLACE DUE TO MISMATCH VALUES IN ULTIMA #
                        if "\r" in " ".join(tag.stripped_strings):
                            value =  " ".join(tag.stripped_strings).replace("\r"," ").replace("\n","").replace("\t", "")
                        else:
                            value =  " ".join(tag.stripped_strings)
            else:
                source = json.dumps(api_data)
                value = api_data['content']['value']['altText'].replace(' -', '')


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                _json = None
                tag = soup.find('script',{'type':'application/ld+json'})
                if tag:
                    _json = json.loads(tag.get_text())

                if _json:
                    source = str(tag)
                    if "brand" in _json.keys():
                        value = _json['brand']['name']
            else:
                if 'brand' in api_data['content']['value']:
                    source, value = json.dumps(api_data), api_data['content']['value']['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value        

    def get_description(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                tag = soup.select_one('#partDescriptionTab') \
                    or soup.select_one('.__iso__Item-tabPane.__iso__Item-active .__iso__Item-dynamicFieldWrapper')


                if tag:
                    source = str(tag)
                    value = " ".join(tag.stripped_strings)
            else:
                source = json.dumps(api_data)
                if 'seo' in api_data and 'value' in api_data['seo']:
                    description = api_data['seo']['value'].get('metaDescription', None)
                    if description:
                        value = str(description)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        return source, value

    def _get_image_urls(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if soup:
                tags = soup.select('.__iso__Item-mainBlock .swiper-slide img')
                if tags:
                    for img in tags:
                        if img.has_attr('src'):
                            value.append('https://www.milrab.se' + img['src'])
                    source = str(tags)
            else:
                tags = api_data['content']['value']['fullViewportResponsiveImages']
                if tags:
                    for img in tags:
                        if 'src' in img:
                            value.append('https://www.milrab.se' + img['src'])
                    source = json.dumps(tags)
            # tag = soup.select('.b-product-image-slide')
            # if tag:
            #     source = str(tag)
            #     i = 0
            #     for i in range(len(tag)):
            #         img = tag[i].get('data-src')
            #         value.append(img)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self,soup, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            # if 1 json fail, automatic NOT_FOUND
            # the site has sku, but product Id is used as mpn for creating the url.
            if _json and 'mpn' in _json:
                productId = _json['mpn'].strip() if 'gtin13' in _json else _json['sku']
                productTitle = _json['name'].strip().replace("-"," ")
                brand =  _json['brand']['name'].strip()
                gtin13_or_ean =  _json['gtin13'].strip() if 'gtin13' in _json else ''
                href = _json['url']
                if productId and productTitle and brand and gtin13_or_ean and href:
                    _json = self.downloader.extract_api_video(productId, productTitle, brand, gtin13_or_ean, href )

                    if 'items' in _json:
                        video_ids = [i["videoId"] for i in _json["items"]]
                        value = [f"https://youtu.be/{i}" for i in video_ids]
                        source = _json['url']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value


        return source, value

    def _get_delivery(self, soup, api_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                tag = soup.select_one('.b-show-stock__value')

                if tag:
                    source = str(tag)
                    if tag.has_attr('title'):
                        value = tag['title']
                    else:
                        value =  ' '.join(tag.stripped_strings)
            else:
                if 'stockStatus' in api_data['content']['value']:
                    source = json.dumps(api_data)
                    value = api_data['content']['value']['stockStatus']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_code(self, soup):
        tag = soup.select_one('.__iso__Item-mainSection')
        if tag and tag.has_attr('data-product-code'):
            return tag['data-product-code']


    def get_ratings_from_api(self, code):
        api_url = f'https://js.testfreaks.com/onpage/milrab.se/reviews.json?url_key={code}'
        headers = {'accept': '*/*', 'accept-encoding': 'gzip, deflate', 'accept-language': 'en-US,en;q=0.9', 'referer': 'https://www.milrab.fi/', 'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"', 'sec-ch-ua-mobile': '?0', 'sec-ch-ua-platform': '"Windows"', 'sec-fetch-dest': 'script', 'sec-fetch-mode': 'no-cors', 'sec-fetch-site': 'cross-site', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'}
        try:
            raw = self.downloader.extra_download(api_url,headers=headers)
            if raw:
                _json = json.loads(raw)
                if _json:
                    return _json
        except:
            pass
        return {}


  


