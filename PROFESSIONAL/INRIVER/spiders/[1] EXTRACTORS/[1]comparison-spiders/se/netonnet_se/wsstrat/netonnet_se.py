import json
import time
import logging
import re, requests

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class NetonnetSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.netonnet.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #### WILL BE HANDLED NOW IN ET #####
            # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            # https://trello.com/c/ooAeQ8MZ/2094-razer-comparisons-wrong-price-fetched-in-netonnetse
            tag = soup.select_one("div.price-big")
            if not tag:
                value = Defaults.GENERIC_NOT_FOUND.value
            # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            else:
                tag = soup.select_one('span[itemprop="offers"] > span[itemprop="price"]')

                if tag:
                    source = str(tag)
                    content = tag.get_text()
                    if content:
                        value = content

                if not tag:
                    script_data = self._get_scrp_tag(soup)

                    if script_data and 'offers' in script_data and 'price' in script_data['offers']:
                        source = str(script_data)
                        value = script_data['offers']['price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span[itemprop="offers"] > meta[itemprop="priceCurrency"]')

            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('span[itemprop="offers"] > span[itemprop="availability"]')

            if tag:
                source = str(tag)
                availability = tag.get_text().strip()

                if availability:
                    value = availability

            if not tag:
                script_data = self._get_scrp_tag(soup)

                if script_data and 'offers' in script_data and 'availability' in script_data['offers']:
                    source = str(script_data)
                    value = script_data['offers']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one("div.rating")
            # Reviews
            if tag:
                reviews_source = str(tag)
                span = tag.find("span")

                if span:
                    reviews_value = re.search(r'\d+', span.get_text().strip()).group()
                
            # Ratings
            tag = soup.select_one("div.rating > a > div > div.ratingOverlay")
            
            if tag and tag.has_attr('style'):
                score_source = str(tag)
                rating = re.search(r'\d+', tag.get("style")).group()
                
                if rating:
                    rating = (float(rating) / 100.0) * 5.0
                    score_value = str(round(rating, 1))

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title = None
            src = []
            tag = soup.select_one('div.subTitle h1')
            sub_tag = soup.select_one('div.product-leftInfo h2')
            if sub_tag:
                src.append(str(sub_tag))
                title = sub_tag.get_text().strip()
            if tag:
                src.append(str(tag))
                title += " - " + tag.get_text().strip() 
            
            if title:
                source = " + ".join(src)
                value = title

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="brand")
            if tag:
                source = str(tag)
                value = ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc_1 = soup.select_one('#collapseFive')
            desc_2 = soup.find("div", {"class":"product-usps"})

            if desc_1 and desc_2:
                source = str(desc_1) + " " + str(desc_2)
                desc_1 = ' '.join(desc_1.stripped_strings)
                desc_2 = ' '.join(desc_2.stripped_strings)
                final_description = desc_1 +' '+desc_2
                value = final_description

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            item_id = None
            tag = soup.find("script", text=re.compile('.*dataLayer.=.window.dataLayer.*'))

            if tag:
                rgx_pid = re.search(r"productId:.?\'(\w+)'", tag.text)
                #specs is in api
                if rgx_pid:
                    item_id = rgx_pid.group(1)
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip().replace(';',''))
                    if json and 'sku' in _json.keys():
                        item_id = _json['sku']

            if item_id:
                source, value = self.downloader.download_specifications(item_id)
            
            _value = {}
            for k, v in value.items():
                if isinstance(v, list):
                    v = ', '.join(v)

                _value[k] = v

            value = _value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            base = 'https://www.netonnet.se'
            tag = soup.find('div',{'class':'owl-carousel slide'})

            if tag:
                tag = tag.select('div.item')
                source = str(tag)
                for img in tag:
                    if img:
                        src = img.select_one('picture img').get('data-src')
                        value.append(base + src)
            else:
                tags = soup.select('#productImageCarousel picture img.productImage')
                if tags:
                    source = str(tags)
                    for tag in tags:
                        if tag.has_attr('data-src'):
                            value.append(base + tag.get('data-src'))
                        elif tag.has_attr('src'):
                            value.append(base + tag.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
        try:
            pid = None
            title = None
            sku = None
            brand = None
            pid_tag = soup.select_one('input[name="ProductId"]') 
            if pid_tag:
                pid = pid_tag['value']

            title_tag = soup.select_one('input[name="ProductName"]')
            if title_tag:
                title = title_tag['value']
            
            brand_tag = soup.select_one('input[name="ProductBrand"]')
            if brand_tag:
                brand = brand_tag['value']

            sku_tag = soup.select_one('span[itemprop="mpn"]')
            if sku_tag:
                sku = sku_tag.text.strip()
            elif not sku_tag:
                sku_tag = soup.select_one('[name="ProductId"]')
                if sku_tag:
                    sku = sku_tag['value']
            
            if sku and title and brand:
                temp, availability = self.get_availability(soup) #temporary checker --- as observed, videos from api will not appear when item is discontinued
                unavailableTag = soup.find('div', class_="alert alert-danger historical-product") # This item is no longer for sale tag

                if availability and availability.lower() != 'discontinued':
                    source, value = self.downloader.extract_api_video(pid, title,brand,sku)
                elif availability and availability.lower() != 'instock' and not unavailableTag: # if discontinued but have videolinks displayed in pdp
                    video_api = self.downloader.extract_api_video(pid, title,brand,sku)
                    if video_api:
                        source, value = video_api

            tag = soup.select("#collapseFive > div > div iframe") #moved here from top --- this function fetches video from description
            if tag:
                source += str(tag)
                for video in tag:
                    if video and video.has_attr('src'):
                        video = video['src']
                        value.append(video) if '//' not in video else value.append(video.replace("//", "")) 
 
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".deliveryInfoText")

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}

        for nth in tags:
            result["%s" % (nth.get_text().strip())] = nth.get_text().strip()

        return result

    @staticmethod
    def _get_scrp_tag(soup):
        tag = soup.find('script', text=re.compile('"@context":"https://schema.org/","@type":"Product"'))
        _json = None

        if tag:
            _rgx = re.search(r'(\{"@context.*\});',tag.get_text(strip=True))
            if _rgx:
                _json = json.loads(_rgx.group(1))

        return _json
