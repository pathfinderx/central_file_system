import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class HjertmansSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.hjertmans.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews) - No Ratings
        # result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
        #     result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
        #     result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications - No Specs
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

                if _json and "Offers" in _json.keys() and "Price" in _json["Offers"]:
                    source, value = str(tag), str(_json['Offers']["Price"])

            else:
                tag = soup.select_one('span.campaign-price') or soup.select_one('.price')

                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text())

                if _json and "Offers" in _json.keys() and "PriceCurrency" in _json["Offers"]:
                    source, value = str(tag), _json['Offers']["PriceCurrency"]
            else:
                value = "SEK"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("span", {"itemprop": "availability"})

            if tag and tag.has_attr("class"):
                source = str(tag)
                availability = " ".join(tag.stripped_strings)

                if availability:
                    value = availability

            else:
                tag = soup.select_one('[itemprop="availability"]')

                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('h1', {'itemprop': 'name'})

            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text())

                if _json and "Brand" in _json.keys():
                    source, value = str(tag), _json['Brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            src = []
            tag = soup.find('div', {'itemprop': 'description'})

            if tag:
                desc_tags = tag.select('p')
                src.append(str(tag))
                for key in desc_tags:
                    specskey = key.select_one('strong')
                    if specskey:
                        if specskey.get_text().strip() == 'Specifikationer':
                            break
                    else:
                        desc.append(key.get_text().strip())

                # src.append(str(tag))
                # description = tag.get_text().strip()
                # if len(description):
                #     desc.append(description)
            else:
                tag = soup.select_one('.usp')
                if tag:
                    src.append(str(tag))
                    description = tag.get_text().strip()
                    if len(description):
                        desc.append(description)
            
            if len(desc):
                source, value = " + ".join(src), "".join(desc)
                
            if value == 'NOT_FOUND':
                tag = soup.select_one('div[id="LongDescription"]')
                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings).strip()
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select(".article-image") or soup.select("#MainImage .image span img")

            if tag:
                source = str(tag)
                for img in tag:
                    if img.has_attr('href') or img.has_attr('src'):
                        image_url = img.get('href') or img.get('src')
                        if "hjertmans.se" not in image_url:
                            image_url = "https://www.hjertmans.se" + image_url
                        value.append(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.find(lambda tag:tag.name=="strong" and "Specifikationer" in tag.text)
            if tag:
                specs = tag.parent.get_text(strip=True).replace("Specifikationer", "").replace("\xa0", "")

                if specs:
                    source, value = str(tag), self._build_spec(specs)

        except Exception as e:
            self.logger(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('div', {'itemprop': 'description'})

            if tag:
                source = str(tag)
                iframes = tag.select("div.description iframe")
                if iframes:
                    for iframe in iframes:
                        video = iframe.get("src")
                        if video:
                            value.append(video)
                        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one("span.ships-within")
            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    
    @staticmethod
    def _build_spec(tag):
        result = {}
        val = []
        key_val = ["Specifikationer"]
        val.append(tag)

        result = dict(zip(key_val, val))
      
        return result  

