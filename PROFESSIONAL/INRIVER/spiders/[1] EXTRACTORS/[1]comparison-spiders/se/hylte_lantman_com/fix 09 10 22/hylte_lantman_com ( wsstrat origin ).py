import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class HylteLantmanComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.hylte-lantman.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews) - No Ratings
        # result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
        #     result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
        #     result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)
        
        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications - No Specs
        # result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery and shipping
        result["delivery"]["source"], result["delivery"]["value"], \
            result["shipping"]["source"], result["shipping"]["value"] = self._get_delivery_and_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(lambda tag: tag.name == "script" and "priceCurrency" in tag.text)
            content = None

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text())
                if _json and 'price' in _json['offers'].keys():
                    content = str(_json['offers']["price"])
                elif _json and 'lowPrice' in _json['offers'].keys():
                    content = str(_json['offers']["lowPrice"])

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(lambda tag: tag.name == "script" and "priceCurrency" in tag.text)

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text())
                if _json and 'priceCurrency' in _json['offers'].keys():
                    content = _json['offers']["priceCurrency"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(lambda tag: tag.name == "script" and "priceCurrency" in tag.text)

            if tag:
                content = 'http://schema.org/OutOfStock'
                source = str(tag)
                _json = json.loads(tag.get_text())
                if _json and 'availability' in _json['offers'].keys():
                    content = _json['offers']["availability"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',text=re.compile('"offers":'))

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text())
                if _json and 'name' in _json.keys() and _json["name"]:
                    content = _json["name"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', {'type': 'application/ld+json'})

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text())
                if 'brand' in _json.keys():
                    content = _json["brand"]['name']

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text=re.compile(r'sub_description*') )

            if tag:
                source = str(tag)
                _rgx = re.search(r'sub_description\":\"(.*)\",\"uom\"', tag.get_text())
                if _rgx:
                    #desc_soup = BeautifulSoup(_rgx.group(1).encode('ASCII', 'ignore').decode('unicode_escape'), 'lxml')
                    desc_soup = BeautifulSoup(_rgx.group(1).encode('latin1','ignore').decode('unicode_escape'),'lxml')
                    content = " ".join(desc_soup.stripped_strings)

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('button.css-r347vo') or soup.select('button.css-dsfppb')
            if tags:
                source = str(tags)
                for tag in tags:
                    img = tag.find('img')
                    if img and img.has_attr('src'):
                        value.append(img['src'].replace('90&w=90&h=90','900&w=900&h=900'))

            else:
                tag = soup.find('script', {'type': 'application/ld+json'})

                if tag:
                    source = str(tag)
                    _json = json.loads(tag.get_text())
                    if 'image' in _json.keys():
                        content = _json["image"]

                    if content:
                        if isinstance(content,list):
                            value = content
                        else:                            
                            value.append(content)
            
            # Extras / Top Up Images https://prnt.sc/1rgj36c
            _id = None
            product_id = None
            _tag = soup.find('script', text=re.compile(r'window.__INITIAL_STATE__'))
            if _tag:
                _rgx = re.search(r'"page":(\{.*\}),"type":"product"', _tag.get_text())
                if _rgx:
                    _rgx2 = re.search(r'"part_no":"([\d]+)","default_name', _rgx.group(1))
                    if _rgx2:
                        _id = _rgx2.group(1)
            if _id:
                main_api = f'https://www.hylte-lantman.com/api/v1/frontend/product/no/{_id}?application_id=1'
                raw_data = self.downloader.extra_download(main_api)
                if raw_data:
                    _json = json.loads(raw_data)
                    product_id = _json['data']['id']
            
            if product_id:
                api_url = f'https://www.hylte-lantman.com/api/v1/frontend/product/{product_id}/promotions/products/unique?application_id=1&as_variants=true'
                raw_data = self.downloader.extra_download(api_url)
                if raw_data:
                    _json = json.loads(raw_data)
                    for data in _json['data']:
                        if not data['price']:
                            value.append(data['format']['image'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile(r'sub_description*') )

            if tag:
                source = str(tag)
                _rgx = re.search(r'sub_description\":\"(.*)\",\"uom\"', tag.get_text())
                if _rgx:
                    vid_soup = BeautifulSoup(_rgx.group(1).encode('ASCII', 'ignore').decode('unicode_escape'), 'lxml')
                    iframes = vid_soup.select("iframe")
                    if iframes:
                        for iframe in iframes:
                            video = iframe.get("src")
                            if video:
                                value.append(video)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery_and_shipping(self, soup):
        del_source = Defaults.GENERIC_NOT_FOUND.value
        del_value = Defaults.GENERIC_NOT_FOUND.value
        ship_source = Defaults.GENERIC_NOT_FOUND.value
        ship_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            if 'JetshopData'.lower() in str(soup).lower():
                tag = soup.find("script", text=re.compile('.*JetshopData.*'))
                if tag:
                    _rgx = re.search(r'var.JetshopData\=({.*});', tag.get_text())
                    if _rgx:
                        _json = json.loads(_rgx.group(1))
                        if _json:
                            del_source = str(tag)
                            ship_source = str(tag)
                            if "ProductInfo" in _json.keys():
                                # Shipping
                                shipping = self.get_shipping_class(_json["ProductInfo"]["FreightClassID"])
                                if shipping:
                                    ship_source += ' + ' + shipping
                                    ship_value = BeautifulSoup(shipping, "lxml").find("body").get_text()
                                
                                # Delivery
                                delivery = self.get_delivery_class(_json["ProductInfo"]["FreightClassID"])
                                if delivery:
                                    ship_source += ' + ' + delivery
                                    del_value = BeautifulSoup(delivery, "lxml").find("body").get_text()
                              

        except Exception as e:
            self.logger.exception(e)
            del_value = FailureMessages.DELIVERY_EXTRACTION.value
            ship_value = FailureMessages.SHIPPING_EXTRACTION.value

        return del_source, del_value, ship_source, ship_value

    def get_delivery_class(self, key):
        if key == 1:
            return """<p>Beställningen skickas inom 1-3 dagar.</p>
                    <p>Väljer du leverans till ombud är normalt sändningen framme nästa vardag och hämtas ut på närmsta Postnordombud.</p>
                    <p>Väljer du hemleverans levereras beställningen dagtid hem till din dörr med tidsbegränsade leveransfönster om 2-3 timmar som du själv väljer.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 2:
            return """<p>Beställningen skickas inom 1-3 dagar.</p>
                    <p>Därefter är normalt sändningen framme nästa vardag och hämtas ut på närmsta Postnordombud.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 3:
            return """<p>Beställningen skickas inom 1-3 dagar.</p>
                    <p>Därefter levereras beställningen dagtid hem till din dörr med tidsbegränsade leveransfönster om 2-3 timmar som du själv väljer.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 4:
            return """<p>Beställningen skickas inom 1-3 dagar.</p>
                    <p>Väljer du leverans till ombud är normalt sändningen framme nästa vardag och hämtas ut på närmsta Postnordombud.</p>
                    <p>Väljer du hemleverans levereras beställningen dagtid hem till din dörr med tidsbegränsade leveransfönster om 2-3 timmar som du själv väljer.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 5:
            return """<p>Leveranstiden är normalt 3-5 arbetsdagar från att beställningen skickats.</p>
                    <p>Beställningen skickas inom 1-3 dagar. Därefter levererar Postnord beställningen till angiven adress så nära de kan komma med sin lastbil. Inbärning eller bärhjälp ingår inte. Postnord aviserar dig via SMS för överenskommelse om leveransdag och tidsintervall. Chauffören ringer upp dig före leverans.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 6:
            return """<p>Beställningen skickas inom 1-3 dagar.</p>
                    <p>Väljer du leverans till ombud är normalt sändningen framme nästa vardag och hämtas ut på närmsta Postnordombud.</p>
                    <p>Väljer du hemleverans levereras beställningen dagtid hem till din dörr med tidsbegränsade leveransfönster om 2-3 timmar som du själv väljer.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 7:
            return """<p>Leveranstiden är normalt 3-5 arbetsdagar från att beställningen skickats.</p>
                    <p>Beställningen skickas inom 1-3 dagar. Därefter levererar Postnord beställningen till angiven adress så nära de kan komma med sin lastbil. Inbärning eller bärhjälp ingår inte. Postnord aviserar dig via SMS för överenskommelse om leveransdag och tidsintervall. Chauffören ringer upp dig före leverans.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 28:
            return """<p>Leveranstiden är normalt 3-5 arbetsdagar från att beställningen skickats.</p>
                    <p>Beställningen skickas inom 1-3 dagar. Därefter levererar Postnord beställningen till angiven adress så nära de kan komma med sin lastbil. Inbärning eller bärhjälp ingår inte. Postnord aviserar dig via SMS för överenskommelse om leveransdag och tidsintervall. Chauffören ringer upp dig före leverans.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 29:
            return """<p>Leveranstiden är normalt 3-5 arbetsdagar från att beställningen skickats.</p>
                    <p>Beställningen skickas inom 1-3 dagar. Därefter levererar Postnord beställningen till angiven adress så nära de kan komma med sin lastbil. Inbärning eller bärhjälp ingår inte. Postnord aviserar dig via SMS för överenskommelse om leveransdag och tidsintervall. Chauffören ringer upp dig före leverans.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        elif key == 30:
            return """<p>Leveranstiden är normalt 3-5 arbetsdagar från att beställningen skickats.</p>
                    <p>Beställningen skickas inom 1-3 dagar. Därefter levererar Postnord beställningen till angiven adress så nära de kan komma med sin lastbil. Inbärning eller bärhjälp ingår inte. Postnord aviserar dig via SMS för överenskommelse om leveransdag och tidsintervall. Chauffören ringer upp dig före leverans.</p>
                    <p>Tracking av beställningen skickas per mail.</p>"""
        return None

    def get_shipping_class(self, key):
        if key==1:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Denna produkt har standardfrakt.</p>
                <!--<p>Fraktkostnaden är <b>49 kr</b> för hämta hos ombud.</p>-->
                <hr>"""
        elif key == 2:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Skickas fraktfritt till närmsta ombud!</p>
                    <hr>"""
        elif key == 3:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Skickas fraktfritt hem till dörren!</p>
                    <hr>"""
        elif key == 4:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Skickas fraktfritt!</p>
                    <p>(Välj själv i kassan om ordern skall skickas till närmsta ombud eller hem till dörren)</p>
                    <hr>"""
        elif key == 5:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Skickas fraktfritt hem till dörren!</p>
                    <hr>"""
        elif key == 6:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Köp så många du vill till en leveransadress, Du betalar endast en frakt!</p>
                    <p>Fraktkostnaden är 249 kr. (Välj själv i kassan om ordern skall skickas till närmsta ombud eller hem till dörren)</p>
                    <hr>"""
        elif key == 7:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Köp så många du vill till en leveransadress, Du betalar endast en frakt!</p>
                    <p>Fraktkostnaden är <b>295 kr</b> för hemleverans.</p>
                    <hr>"""
        elif key == 28:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Köp så många du vill till en leveransadress, Du betalar endast en frakt!</p>
                    <p>Fraktkostnaden är <b>495 kr</b> för hemleverans.</p>
                    <hr>"""
        elif key == 29:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Köp så många du vill till en leveransadress, Du betalar endast en frakt!</p>
                    <p>Fraktkostnaden är <b>695 kr</b> för hemleverans.</p>
                    <hr>"""
        elif key == 30:
            return """<p class="color-green shipping-product-desc font-bold"><i class="fa fa-fw fa-truck"></i> Köp så många du vill till en leveransadress, Du betalar endast en frakt!</p>
                    <p>Fraktkostnaden är <b>995 kr</b> för hemleverans.</p>
                    <hr>"""
        return None
