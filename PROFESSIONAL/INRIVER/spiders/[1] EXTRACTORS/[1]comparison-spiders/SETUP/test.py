from pprint import pprint
from copy import deepcopy
import urllib3
import json
from dotenv import load_dotenv
from request.crawlera import CrawleraSessionRequests
from request.luminati import LuminatiSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
if __name__ == '__main__':
    # country = 'fi'
    # website = 'www.karkkainen.com'
    # url = " https://www.karkkainen.com/verkkokauppa/garmin-forerunner-45-gps-urheilukello-juoksukello?searchTerm=forerunner%2045"
    # company_code = '200'

    # country = 'us'
    # website = 'www.target.com'
    # url = 'https://www.target.com/p/xbox-series-x-wireless-controller---carbon-black/-/A-80744784'
    # company_code = '100'

    country = 'mx'
    website = 'www.sanborns.com.mx'
    url = 'https://www.sanborns.com.mx/producto/186361/nsw-mario-kart-8-deluxe/'
    company_code = '200'

    # country = 'br'
    # website = 'www.miranda.com.br'
    # url = 'https://www.miranda.com.br/produto/controle-switch-joy-con-vermelho-e-azul-neon-para-nintendo/4957580'
    # company_code = 'U00'

    requester = LuminatiSessionRequests(country)  # Init requester
    factory = StrategyFactory()

    country = 'uk' if country == 'gb' else country
    dl_strategy = factory.get_download_strategy(website, country)(requester)  # Init download strategy for retailer with
    ws_strategy = factory.get_website_strategy(website, country, company_code)(dl_strategy)  # Init website strategy
    
    # Get Proxy IP info
    pprint(dl_strategy.requester.proxy_info)
    
    # Get raw data
    raw_data = dl_strategy.download(url)

    # Get extracted data
    res = ws_strategy.execute(raw_data)
    deepcopy(res)
    
    # pprint(res)
    with open('../extract-transform-comparison/et_test.json', 'w', encoding='utf-8') as f:
        json.dump(res, f, ensure_ascii=False, indent=2)