import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class NoonComSaudiEnWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.noon.com/saudi-en/'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('div.priceNow')

            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            else:
                script_tag = soup.find('script', text=re.compile(r'"@context": "https://schema.org/"'))
                if script_tag:
                    _json = json.loads(script_tag.get_text())
                    if _json and 'offers' in _json and _json['offers'] and 'price' in _json['offers'][0]:
                        source, value = str(script_tag), _json['offers'][0]['price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = "SAR"

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # prioritize this as sometimes json conflicts with the pdp
            tag = soup.find('div', text=re.compile('product is not available', re.IGNORECASE)) or soup.find('div', text=re.compile('available soon', re.IGNORECASE))
            if tag:
                # additional check to avoid false positive
                seller_exist = self.check_if_seller_exist(soup)
                if not seller_exist:
                    source, value = str(tag), "outofstock"

            # for instock availability get on the json.
            if value == 'NOT_FOUND':
                tag = soup.select_one('.sc-1xw7r3i-0.grpnyI')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()
                else:
                    tags = soup.find_all('script', type='application/ld+json')
                    tag = next((i for i in tags if i.string and re.search(r'"@type":\s*"Product"', i.string)), None)

                    if tag:
                        data = json.loads(tag.string)
                        if data['offers']:
                            source, value = str(tag), data['offers'][0]['availability']  
                        
            if value == 'NOT_FOUND':
                tag = soup.select_one('.sc-1xw7r3i-0.MXkWJ') or soup.select_one('.sc-ad141e45-0') or soup.select_one('.sc-8bb36963-0') or \
                        soup.select_one('.text.addToCart')
                if tag:
                    source, value = str(tag), tag.get_text(strip=True)

            pdp_tag = soup.select_one('.sc-1xw7r3i-0.MXkWJ') or soup.select_one('.sc-1xw7r3i-0.fbfKGs') or \
                soup.select_one('[class="sc-ad141e45-0 UEOPl"]')
            if pdp_tag and pdp_tag.get_text().strip() == 'Sorry! This product is not available.':
                source, value = str(tag), "outofstock"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', id='__NEXT_DATA__') or soup.select_one('script#__NEXT_DATA__')
            if tag:
                # rating_data = re.search(r'"product_rating".+"value":(.+).+"count":(\d+)', tag.string)
                rating_data = re.search(r'("summary".+"averageRating":(.+),"breakdown".+"ratingCount":(\d+))', tag.string)
                if rating_data:
                    score_source, score_value = str(rating_data.group(1)), rating_data.group(2)
                    reviews_source, reviews_value = str(rating_data.group(1)), rating_data.group(3)
                else:
                    txt = tag.get_text().strip()
                    _json = json.loads(txt)
                    if _json and 'props' in _json and 'pageProps' in _json['props'] and 'props' in  _json['props']['pageProps']:
                        props = _json['props']['pageProps']['props']
                        if 'catalog' in props and 'product' in props['catalog']:
                            product = props['catalog']['product']
                            if product and 'reviews' in product and 'summary' in product['reviews']:
                                reviews_sum = product['reviews']['summary']
                                if 'ratingCount' in reviews_sum:
                                    score_value = str(reviews_sum['averageRating'])
                                    reviews_value = str(reviews_sum['ratingCount'])
                                    score_source = reviews_source = str(_json)


            if reviews_value == 'NOT_FOUND' and score_value == 'NOT_FOUND':
                score_tag = tag = soup.find('span',{'class':'sc-8wv4rk-1 cfUwDc'})
                reviewers_tag = soup.find('div',{'class':'sc-1gzgd1-0 evFpbC'})
                if score_tag and reviewers_tag:
                    score_source = str(score_tag)
                    reviews_source = str(reviewers_tag)
                    score_value = score_tag.text
                    reviews_value = reviewers_tag.text

                else:
                
                    sku = ''
                    tag = soup.find('script',text = re.compile('"@type": "Product"'))
                    if tag:
                        _json = json.loads(tag.text.strip())
                        if _json and 'sku' in _json.keys():
                            sku = _json['sku']
                        if sku:
                            payload = {
                                'lang': None,
                                'page': 1,
                                'provideBreakdown': True,
                                'ratings': [1, 2, 3, 4, 5],
                                'sku': sku
                            }
                            api_url = 'https://www.noon.com/_svc/reviews/fetch/v1/product-reviews/list'

                            response = self.downloader.extra_download(api_url,data=json.dumps(payload))
                            if response:
                                _json_response = json.loads(response)
                                if _json_response and 'summary' in _json_response.keys():
                                    # Ratings
                                    if 'rating' in _json_response['summary'] and _json_response['summary']['rating']:
                                        score_source,score_value = api_url,str(_json_response['summary']['rating'])

                                    # Reviews
                                    # if 'commentCount' in _json_response['summary'] and _json_response['summary']['commentCount']:
                                    #     reviews_source,reviews_value = api_url,str(_json_response['summary']['commentCount'])
                                    # This is rating count not review count
                                    # else:
                                    if 'count' in _json_response['summary'] and _json_response['summary']['count']:
                                        reviews_source,reviews_value = api_url,str(_json_response['summary']['count'])

               
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:       
            tag = soup.select_one('h1.sc-1vbk2g0-12.xaGRV') or soup.select_one('.sc-ebb3cc52-12.kVfnLm')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            else: 
                tag = soup.select_one('h1.sc-1vbk2g0-8.cfCaBu')
                if tag:
                    source, value = str(tag), tag.get_text(strip=True)

                if not tag:
                    tag = soup.select_one('[property="og:title"]')

                    if tag and tag.has_attr('content'):
                        source, value = str(tag), tag.get('content').replace("online in Riyadh, Jeddah and all KSA","")
            if value == 'NOT_FOUND':
                title_tag = soup.find('h1', class_='sc-c44e3e2d-12 kODUYj')
                if title_tag:
                    source = str(title_tag)
                    value = title_tag.text.strip()
                
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.sc-1vbk2g0-11.kGxKzu')
            if tag:
                source, value = str(tag), tag.text
            else:
                tag = soup.select_one('div.sc-1vbk2g0-7')
                if tag:
                    source, value = str(tag), tag.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try: 
            tag = soup.select_one('div.sc-6d68e1fa-4.kYHMbw')
                
            if tag:
                if tag.get_text(strip=True).split(" ")[0][:10] in 'Highlights':
                    value = ' '.join(tag.stripped_strings) #'Highlights ' + tag.get_text().strip()[10:]
                else:
                    value = ' '.join(tag.stripped_strings) #tag.get_text(strip=True)
                source = str(tag)

            tag = soup.select_one("script#__NEXT_DATA__")
            data = None
            if tag and value == Defaults.GENERIC_NOT_FOUND.value:
                source = str(tag)
                data = json.loads(tag.string)

                product_highlights = ''
                product_overview = ''
                if 'feature_bullets' in data['props']['pageProps']['catalog']['product']:
                    product_highlights = data['props']['pageProps']['catalog']['product']['feature_bullets']

                if 'long_description' in data['props']['pageProps']['catalog']['product']:
                    product_overview = data['props']['pageProps']['catalog']['product']['long_description']

                    value = ' '.join(product_highlights) + " " + product_overview

            if value == None:
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value
         
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select_one("script#__NEXT_DATA__")
            if tag:
                source = str(tag)
                data = json.loads(tag.string)
                specifications = None
                if 'cataglog' in data['props']['pageProps'] and 'specifications' in data['props']['pageProps']['catalog']['product']:
                    specifications = data['props']['pageProps']['catalog']['product']['specifications']
                elif 'props' in data['props']['pageProps'] and 'specifications' in data['props']['pageProps']['props']['catalog']['product']:
                    specifications = data['props']['pageProps']['props']['catalog']['product']['specifications']
                    
                if specifications:
                    for spec in specifications:
                        value[spec['name']] = spec['value']
                
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.find_all('script', type='application/ld+json')
            tag = next((i for i in tags if i.string and re.search(r'"@type":\s*"Product"', i.string)), None)

            if tag:
                data = json.loads(tag.string)
                value = data['image']
                source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.estimator_first p.sc-1au63nj-2.yvQft') or soup.select_one('.sc-1xw7r3i-0.grpnyI')
            if tag and tag.get_text(strip=True):
                source, value = str(tag), tag.get_text(strip=True)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        # website doesn't support delivery
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value

    def check_if_seller_exist(self, soup):
        tag = soup.select_one('div img[alt="seller"]') or soup.find('img', {"alt": "seller"})
        if tag:
            return True
        
        _json = self.get_json(soup)
        if _json:
            offers = _json.get('offers', None)
            if offers:
                seller = offers[0].get('seller', None)
                if seller:
                    seller_name = seller.get('name', None)
                    if seller_name:
                        return True

        return False

    def get_json(self, soup):
        tags = soup.find_all('script', type='application/ld+json')
        tag = next((i for i in tags if i.string and re.search(r'"@type":\s*"Product"', i.string)), None)

        if tag:
            try:
                data = json.loads(tag.string)
                if data:
                    return data
            except:
                pass
        return {}