import re, json
import math
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class JarirComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.jarir.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[class="price price--pdp"] [class="ar-number"]') or soup.select_one('.price .ar-number') \
                or soup.select_one('.fixed-product .price .ar-number')
            if tag:
                source, value = str(tag), tag.get_text().strip()
            else:
                meta_tag = soup.select_one('meta[itemprop="price"]')
                if meta_tag and meta_tag.has_attr('content'):
                    value = meta_tag['content']
                    source = str(meta_tag)
            
            # tag = soup.select_one('.price-box .price')

            # if tag:
            #     tag_currency = tag.select_one('span')
            #     if tag:
            #         tag_currency.decompose()
            #         source, value = str(tag), tag.get_text().strip()
            

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', { 'itemprop' : 'priceCurrency'})

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag['content']
            else:
                value = "SAR"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            api_sku = soup.select_one('[itemprop="productID"]')
            if api_sku:
                api_sku = api_sku.get_text()
                # api_url = f'https://www.jarir.com/api/stock/getavailability?skuData={api_sku.get_text(strip=True)}|1|0&storeCode=sa_ar'
                api_url = 'https://www.jarir.com/api/stock/getavailability?skuData={}|1|0&storeCode=sa_ar'.format(api_sku)
                api_res = self.downloader.extra_download(api_url)
                if api_res:
                    _json = json.loads(api_res)
                    if _json and 'data' in _json and 'result' in _json['data'] and _json['data']['result'][0] and 'stock_availablity' in _json['data']['result'][0]:
                        stk = _json['data']['result'][0]['stock_availablity']
                        if stk == 'AVAILABLE':
                            value = 'instock'
                        else:
                            value = 'outofstock'
                        source = str(api_url)

            # tag = soup.find(itemprop="availability")

            # if tag and tag.has_attr('href'):
            #     source, value = str(tag), tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            api_sku = soup.select_one('[itemprop="productID"]')
        
            if api_sku:
                # api_url = 'https://d1le22hyhj2ui8.cloudfront.net/onpage/jarir.com-ar/reviews.json?key={}'.format(api_sku.text.strip())
                # api_res = self.downloader.extra_download(api_url)
                # if api_res:
                #     api_res_json= api_res.replace('testFreaks.cb(','').replace(')','')
                #     _json = json.loads(api_res_json)
                #     #review
                #     if _json and 'pro_review_count' in _json:
                #         reviews_source, reviews_value = str(api_url), str(_json['pro_review_count'])
                        
                #     #score
                #     if _json and 'score' in _json:
                #         score_source, score_value = str(api_url), str(math.ceil((_json['score']/2)*10)/10)
                                     
                # if score_value == Defaults.GENERIC_NOT_FOUND.value or reviews_value == Defaults.GENERIC_NOT_FOUND.value:
                
                # should scrape CUSTOMER REVIEWS as per validator request
                api_url = 'https://www.jarir.com/api/review/getreview?sku={}&review_type=&type=all&storeCode=sa_ar'.format(api_sku.get_text().strip())
                api_res = self.downloader.extra_download(api_url)
                if api_res:
                    _json = json.loads(api_res)
                    #Ratings
                    if 'data' in _json and _json['data'][0] and _json['data'][0][0] and 'average' in _json['data'][0][0]:
                        score_source, score_value = str(api_url), str(_json['data'][0][0]['average'])
                        
                    #Reviews
                    if 'data' in _json and _json['data'][1]:
                        reviews_source, reviews_value = str(api_url), str(len(_json['data'][1]))
                    
                    if reviews_value == 'NOT_FOUND':
                        score_source = Defaults.GENERIC_NOT_FOUND.value
                        score_value = Defaults.GENERIC_NOT_FOUND.value
                        reviews_source = Defaults.GENERIC_NOT_FOUND.value
                        reviews_value = Defaults.GENERIC_NOT_FOUND.value
                    
            
            # # Reviews
            # reviews_tag = soup.find("meta", attrs={"itemprop" : "ratingCount"})

            # if reviews_tag and reviews_tag.has_attr('content'):
            #     reviews_source, reviews_value = str(reviews_tag), str(reviews_tag['content'])

            # # Ratings
            # score_tag = soup.find("meta", attrs={"itemprop" : "ratingValue"})

            # if score_tag and score_tag.has_attr('content'):
            #     score_source, score_value = str(score_tag), str(float(score_tag['content']))
        
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[class="product-title__title"] [itemprop="name"]') or soup.find('h1', {'class': 'product-title__title'})

            if tag:
                source, value = str(tag), tag.get_text().strip().replace('\u200e', '')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            description = []
            src = []
            tag = soup.find('script', type="text/javascript", text= re.compile('mpn'))
            script_list = soup.select('script', type='text/javascript')
            if tag or script_list:
                if script_list: 
                    for x in script_list:
                        if (re.search(r'mpn', x.get_text())):
                            tag = x # iterate and assign to tag variable if 'mpn' matches with any script_list's string content

                mpn_compile = [
                    'mpn:.\"(.*)\"', # former regex compilation for mpn
                    'mpn:\"([\S]+)\",productcode'
                ]

                product_id_compile = [
                    'productId:.\"(.*)\"', # former regex compilation for product_id
                    'childSku:\"([0-9]+)\"' # product_id not found, searching childsku as alternative
                ]

                brand_compile = [
                    'brand:.\"(.*)\"', # former regex compilation for brand
                    'brand:\"([\S]+)\",'
                ]

                flixprod_compile = [
                    "window.flixJsCallbacks.pid ='(.*)';", # former regex compilation for brand
                    'flixinpage_([0-9]+)'
                ]

                # get mpn
                mpn = re.search(mpn_compile[1], tag.get_text()).group(1) if re.search(mpn_compile[1], tag.get_text()) else None
                prod_id = re.search(r'childSku:\"([0-9]+)\"', tag.get_text()).group(1) if re.search(r'childSku:\"([0-9]+)\"', tag.get_text()) else None
                brand = re.search(brand_compile[1], tag.get_text()).group(1) if re.search(brand_compile[1], tag.get_text()) else None
                distributor = '12399' # temporary static value for distributor because the api_url for distributor extraction was not found or status code 404
                flix_prod = None
                lang = 'ar'
                form_data = {
                    'mpn': mpn,
                    'id': prod_id,
                    'brand': brand
                }
                api_url = 'https://www.jarir.com/catalog/product_Thirdpartycontent/'
                headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
                }
                raw_data = self.downloader.extra_download(api_url, headers=headers, data=form_data)
                if raw_data:
                    distributor = re.search(r"distributor.=.\'(.*)\'", raw_data).group(1) if re.search(r"distributor.=.\'(.*)\'", raw_data) else None
                
                api_url_2 = f'https://media.flixcar.com/delivery/js/inpage/{distributor}/{lang}/mpn/{mpn}?&={distributor}&={lang}&mpn={mpn}&brand={brand.lower()}&fl=ae&ssl=1&ext=.js'
                raw_data_2 = self.downloader.extra_download(api_url_2)
                if raw_data_2:
                    flix_prod = re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2).group(1) if re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2) else None
                
                if not flix_prod:
                    lang = 'ar'
                    api_url_2 = f'https://media.flixcar.com/delivery/js/inpage/{distributor}/{lang}/mpn/{mpn}?&={distributor}&={lang}&mpn={mpn}&brand={brand.lower()}&fl=ae&ssl=1&ext=.js'
                    raw_data_2 = self.downloader.extra_download(api_url_2)
                    if raw_data_2:
                        flix_prod = re.search(flixprod_compile[1], raw_data_2).group(1) if re.search(flixprod_compile[1], raw_data_2) else '0'

                api_url_3 = f'https://media.flixcar.com/delivery/inpage/show/{distributor}/{lang}/{flix_prod}/json?c=jsonpcar{distributor}{lang}{flix_prod}&complimentary=0&type=.html'
                response = self.downloader.extra_download(api_url_3)
                if response:
                    rgx, _json, soup, tag, description_container = None, None, None, None, list()
                    try:
                        rgx = re.search(r'\(({[\s\S]+)\)', response)
                        _json = json.loads(rgx.group(1)) if rgx else None
                        soup = BeautifulSoup(_json.get('html'), 'lxml') if (_json and 'html' in _json) else None
                        tag = soup.select('.flix-std-content' or '.flix-std-title.flix-d-h3' or '.flix-std-desc' or '.flix-std-p.flix-d-p')
                        src.append(api_url_3)
                        
                        if tag:
                            for x in tag:
                                description.append(' '.join(x.stripped_strings)) 
                        elif _json and 'html' in _json:
                            desc_soup = BeautifulSoup(_json['html'], 'lxml')
                            for tag_script in desc_soup.select('script'):
                                tag_script.extract()
                                
                            for tag_stype in desc_soup.select('style'):
                                tag_stype.extract()
                            description.append(' '.join(desc_soup.stripped_strings))

                    except Exception as e:
                        print('Error at: wsstrat -> get_description() -> api_url_3 -> with message: ', e) 
                
                api_url_4 = f'https://ws.cnetcontent.com/ff7d2c30/script/2e65976528?cpn={prod_id}&mf={brand}&pn={mpn}&lang=en&market=SA&host=www.jarir.com&nld=1'
                response = self.downloader.extra_download(api_url_4)
                if response:
                    rgx = re.search(r'"html": "(.*)" }\r\n]',response,re.MULTILINE|re.DOTALL)
                    if rgx:
                        desc_soup = BeautifulSoup(rgx.group(1).replace('\\"','"').replace('\\n',''),'lxml')
                        if desc_soup.select_one('.ccs-cc-video-duration'):
                            desc_soup.select_one('.ccs-cc-video-duration').extract()
                        src.append(api_url_4)
                        description.append(' '.join(desc_soup.stripped_strings))

            if src or description:
                source = " + ".join(src)
                value = " + ".join(description)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('[data-card="specification"] table tr') or soup.select('#card-specifications .table.table--info.table--bordered-bottom tr')
            
            if len(tags):
                source, value = str(tags), self._get_initial_specs(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            tags = soup.find('script', text= re.compile('src:"https://ak-asset.jarir.com/'))
            
            if tags:
                imgs_json = tags.get_text().strip().split('productGallery')[1]
                if imgs_json:
                    imgs = re.findall(r'(src:"https://ak-asset.jarir.com/akeneo-prod/asset/[a-zA-Z0-9/_.jpg]+)', imgs_json)
                    if imgs:
                        for row in imgs:
                            if '.jpg' in row:
                                value.append("https://www.jarir.com/cdn-cgi/image/fit=contain,width=auto,height=auto,format=auto,quality=100,metadata=none/"+str(row[5:]))   
                        source = str(tags) 
            # tags = soup.select("#gallery img") or \
            #     soup.select('.gallery__slide-item img')

            # if tags:
            #    source, value = str(tags), self._get_images(tags)
               
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
      
        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}

        for tag in tags:
            th_tag = tag.select_one("th")
            td_tag = tag.select_one("td")
            result["%s" % th_tag.get_text().strip()] = td_tag.get_text().strip()

        return result
    
    @staticmethod
    def _get_images(tags):
        result = []
        for img in tags:
            if img and img.has_attr('data-src'):
                img = img['data-src'].split("https")[-1]
                result.append('https' + img)
            else:
                break
        return result

