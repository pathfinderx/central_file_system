from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import cloudscraper

class JarirComDownloadStrategy(DownloadStrategy):

    def __init__(self, requester):
        self.scraper = cloudscraper.create_scraper()
        self.requester = requester


    def download(self, url, timeout=randint(10, 60), headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        
        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                response = self.scraper.get(url, timeout=timeout)
                # response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = response.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in response.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break
                
                #Comment because causing auto copy over in all item_ids https://prnt.sc/YUqcPYWsBKWX
                # if '404 غير موجود' in response.content.decode('utf-8') or '404' in response.content.decode('utf-8'): # PDP CHECKER
                #     raise DownloadFailureException('Download Failed - 404 not found') 

                if not is_redirect:
                    if 'عذرا ولكن لايمكن العثور على الصفحة التي تبحث عنها.' in response.content.decode('utf-8'): # PDP CHECKER
                        raise DownloadFailureException('Download Failed - Auto rerun is possible')
                    else:
                        return response.content.decode('utf-8')               

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        

        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                response = self.scraper.get(url, timeout=timeout)
            else:
                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = response.status_code

            if status_code in [200, 201]:
                return response.text

            # exception bypass (tolerable)
            # raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except Exception as e:
            print('Error at: wsstrat -> extra_download() -> with message: ', e)
            return None