import logging
import re
import json
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class JarirComSaEnWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.jarir.com/sa-en'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('div.price')

            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'SAR'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('link[itemprop="availability"]')

            if tag:
                source, value = str(tag), tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _id_tag = soup.select_one('[itemprop="productID"]')
            if _id_tag:
                _id = _id_tag.text.strip()
                api_url = f'https://d1le22hyhj2ui8.cloudfront.net/onpage/jarir.com/reviews.json?key={_id}&callback=testFreaks.cb'
                response = self.downloader.extra_download(api_url)
                if response:
                    _json = json.loads(response.split('testFreaks.cb(')[1].split(')')[0])
                    if _json:
                        # Ratings
                        if 'score' in _json.keys():
                            score_source = api_url
                            score_max_value = "10"
                            score_value = str(_json['score'])
                        
                        # Reviews
                        if 'pro_review_count' in _json.keys():
                            reviews_source = api_url
                            reviews_value = str(_json['pro_review_count'])

            if reviews_value == Defaults.GENERIC_NOT_FOUND.value and score_max_value == Defaults.GENERIC_NOT_FOUND.value:
                tag_score = soup.select_one('meta[itemprop="ratingValue"]')
                tag_review = tag = soup.select_one('meta[itemprop="ratingCount"]')

                if tag_score and tag_review:
                    if tag_score.has_attr('content') and tag.has_attr('content'):
                        reviews_source, score_source = str(tag_review), str(tag_score)
                        reviews_value, score_value = tag_review.get('content'), tag_score.get('content')

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1.product-title__title')

            if tag:
                source, value = str(tag), tag.get_text().strip()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Brand not explicitly stated in pdp
        
        # try:
        #     tag = soup.select_one('[itemprop="brand"]')

        #     if tag and tag.has_attr('content'):
        #         source, value = str(tag), tag.get('content')

        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        # Description not explicitly stated in pdp.

        try:
            # Getting this description format is too hard.
            # format_1_tag = soup.select_one('div[id="third-party-content-wrapper"]')
            description = []
            src = []
            tag = soup.find('script', type="text/javascript", text= re.compile('mpn'))
            script_list = soup.select('script', type='text/javascript')
            if tag or script_list:
                if script_list: 
                    for x in script_list:
                        if (re.search(r'mpn', x.get_text())):
                            tag = x # iterate and assign to tag variable if 'mpn' matches with any script_list's string content


                # str(tag)[57565:57565+500]
                # str(tag)[1138:1138+500]
                # tag.get_text()[63053:63053+100]

                # matches = re.compile(r'12399').finditer(tag.get_text())
                # for x in matches:
                    # print(x)

                # productid search
                    # productid -> this.product sku? (https://prnt.sc/QhnBCvfOAbDH)
                        # cpn -> this.product.sku -> parent.sku or child.sku -> product_id

                # distributor search
                    # api_url = 'https://www.jarir.com/catalog/product_Thirdpartycontent/' -> status code 404 or not found
                    # 12399 as static and found on other pages
                
                mpn_compile = [
                    'mpn:.\"(.*)\"', # former regex compilation for mpn
                    'mpn:\"([\S]+)\",productcode'
                ]

                product_id_compile = [
                    'productId:.\"(.*)\"', # former regex compilation for product_id
                    'childSku:\"([0-9]+)\"' # product_id not found, searching childsku as alternative
                ]

                brand_compile = [
                    'brand:.\"(.*)\"', # former regex compilation for brand
                    'brand:\"([\S]+)\",'
                ]

                flixprod_compile = [
                    "window.flixJsCallbacks.pid ='(.*)';", # former regex compilation for brand
                    'flixinpage_([0-9]+)'
                ]

                # get mpn
                mpn = re.search(mpn_compile[1], tag.get_text()).group(1) if re.search(mpn_compile[1], tag.get_text()) else None
                prod_id = re.search(r'childSku:\"([0-9]+)\"', tag.get_text()).group(1) if re.search(r'childSku:\"([0-9]+)\"', tag.get_text()) else None
                brand = re.search(brand_compile[1], tag.get_text()).group(1) if re.search(brand_compile[1], tag.get_text()) else None
                distributor = '12399' # temporary static value for distributor because the api_url for distributor extraction was not found or status code 404
                flix_prod = None
                lang = 'd2'
                form_data = {
                    'mpn': mpn,
                    'id': prod_id,
                    'brand': brand
                }
                api_url = 'https://www.jarir.com/catalog/product_Thirdpartycontent/'
                headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
                }
                raw_data = self.downloader.extra_download(api_url, headers=headers, data=form_data)
                if raw_data:
                    distributor = re.search(r"distributor.=.\'(.*)\'", raw_data).group(1) if re.search(r"distributor.=.\'(.*)\'", raw_data) else None
                
                api_url_2 = f'https://media.flixcar.com/delivery/js/inpage/{distributor}/{lang}/mpn/{mpn}?&={distributor}&={lang}&mpn={mpn}&brand={brand.lower()}&fl=ae&ssl=1&ext=.js'
                raw_data_2 = self.downloader.extra_download(api_url_2)
                if raw_data_2:
                    flix_prod = re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2).group(1) if re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2) else None
                
                if not flix_prod:
                    lang = 'd2'
                    api_url_2 = f'https://media.flixcar.com/delivery/js/inpage/{distributor}/{lang}/mpn/{mpn}?&={distributor}&={lang}&mpn={mpn}&brand={brand.lower()}&fl=ae&ssl=1&ext=.js'
                    raw_data_2 = self.downloader.extra_download(api_url_2)
                    if raw_data_2:
                        flix_prod = re.search(flixprod_compile[1], raw_data_2).group(1) if re.search(flixprod_compile[1], raw_data_2) else '0'

                api_url_3 = f'https://media.flixcar.com/delivery/inpage/show/{distributor}/{lang}/{flix_prod}/json?c=jsonpcar{distributor}{lang}{flix_prod}&complimentary=0&type=.html'
                raw_data_3 = self.downloader.extra_download(api_url_3)
                if raw_data_3 and distributor and lang and flix_prod:
                    _rgx = re.search(r"jsonpcar" + distributor + lang + flix_prod + r"\((.*)\)", raw_data_3)
                    if _rgx:
                        _json = json.loads(_rgx.group(1))
                        desc_soup = BeautifulSoup(_json['html'], 'lxml')
                        src.append(api_url_3)
                        for _script in desc_soup.select('script'):
                            _script.decompose()
                        for _style in desc_soup.select('style'):
                            _style.decompose()
                        description.append(' '.join(desc_soup.stripped_strings))
                
                api_url_4 = f'https://ws.cnetcontent.com/ff7d2c30/script/2e65976528?cpn={prod_id}&mf={brand}&pn={mpn}&lang=en&market=SA&host=www.jarir.com&nld=1'
                response = self.downloader.extra_download(api_url_4)
                if response:
                    rgx = re.search(r'"html": "(.*)" }\r\n]',response,re.MULTILINE|re.DOTALL)
                    if rgx:
                        desc_soup = BeautifulSoup(rgx.group(1).replace('\\"','"').replace('\\n',''),'lxml')
                        if desc_soup.select_one('.ccs-cc-video-duration'):
                            desc_soup.select_one('.ccs-cc-video-duration').extract()
                        src.append(api_url_4)
                        description.append(' '.join(desc_soup.stripped_strings))

                api_url_5 = f'https://media.flixcar.com/delivery/inpage/show/{distributor}/{lang}/{flix_prod}/json?c=jsonpcar{distributor}{lang}{flix_prod}&complimentary=0&type=.html'
                response = self.downloader.extra_download(api_url_5)
                if response:
                    rgx, _json, soup, tag, description_container = None, None, None, None, list()
                    try:
                        rgx = re.search(r'\(({[\s\S]+)\)', response)
                        _json = json.loads(rgx.group(1)) if rgx else None
                        soup = BeautifulSoup(_json.get('html'), 'lxml') if (_json and 'html' in _json) else None
                        tag = soup.select('.flix-std-content' or '.flix-std-title.flix-d-h3' or '.flix-std-desc' or '.flix-std-p.flix-d-p')
                        src.append(api_url_5)
                        
                        for x in tag:
                            description.append(' '.join(x.stripped_strings))

                        # [' '.join(x.stripped_strings) for x in tag if x.attrs.get('class') for y in x.attrs.get('class') if re.search(r'flix-std', y)]
                        # [x for x in tag if x.attrs.get('class') and True in [y for y in x.attrs.get('class') if re.search(r'flix-std']]
                        # descriptions = [' '.join(x.stripped_strings) for x in tag if x.attrs.get('class') and True in [True for y in x.attrs.get('class') if re.search(r'flix-std', y)]]
                        # [description_container.append(x) for x in descriptions if x not in description_container]

                    except Exception as e:
                        print('Error at: wsstrat -> get_description() -> api_url_5 -> with message: ', e) 

            if src or description:
                source = " + ".join(src)
                value = " + ".join(description)
            # format_2_tag = soup.select_one('div.page__container iframe')
            # if format_2_tag:
            #     url = format_2_tag['src']
            #     raw_data = self.downloader.download(url)
            #     source = url
            #     value = BeautifulSoup(raw_data, 'lxml').get_text().strip()
                
            # else:
            #     format_3_tags = soup.select('div.page__row')
            #     if len(format_3_tags):
            #         for format_3_tag in format_3_tags:
            #             if(format_3_tag.select_one('div.divider--small')):
            #                 page_container = format_3_tag.select_one('div.page__container')
            #                 source = str(page_container)
            #                 value = page_container.get_text().strip()
            #                 break

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('div[id=card-specifications] tr.table__row')
            
            if len(tags):
                source = str(tags)

                for tag in tags:
                    key = tag.select_one('th').get_text().strip()
                    val = tag.select_one('td').get_text().strip()
                    value[key] = val
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('div.gallery__slide img.image.lazyload')

            if len(tags):
                source = str(tags)
                for tag in tags:
                    try:
                        if tag['data-src']:
                            value.append(tag['data-src'])
                    except:
                        pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value
            
        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        tag = soup.find('script', type="text/javascript", text= re.compile('mpn'))
        if tag:
            # get mpn
            mpn = re.search(r'mpn:.\"(.*)\"', tag.get_text()).group(1) if re.search(r'mpn:.\"(.*)\"', tag.get_text()) else None
            prod_id = re.search(r'productId:.\"(.*)\"', tag.get_text()).group(1) if re.search(r'productId:.\"(.*)\"', tag.get_text()) else None
            brand = re.search(r'brand:.\"(.*)\"', tag.get_text()).group(1) if re.search(r'brand:.\"(.*)\"', tag.get_text()) else None
            if mpn and prod_id and brand:
                api_url = f'https://ws.cnetcontent.com/ff7d2c30/script/2e65976528?cpn={prod_id}&mf={brand}&pn={mpn}&lang=en&market=SA&host=www.jarir.com&nld=1'
                response = self.downloader.extra_download(api_url)
                if response:
                    rgx = re.search(r'data-video-url=\\"(https:[\w./]+)\\"',response)
                    if rgx:
                        source = api_url
                        value.append(rgx.group(1))

                distributor = None
                flix_prod = None
                lang = 'd2'
                form_data = {
                    'mpn': mpn,
                    'id': prod_id,
                    'brand': brand
                }
                api_url = 'https://www.jarir.com/catalog/product_Thirdpartycontent/'
                headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
                }
                raw_data = self.downloader.extra_download(api_url, headers=headers, data=form_data)
                if raw_data:
                    distributor = re.search(r"distributor.=.\'(.*)\'", raw_data).group(1) if re.search(r"distributor.=.\'(.*)\'", raw_data) else None

                api_url_2 = f'https://media.flixcar.com/delivery/js/inpage/{distributor}/{lang}/mpn/{mpn}?&={distributor}&={lang}&mpn={mpn}&brand={brand.lower()}&fl=ae&ssl=1&ext=.js'
                raw_data_2 = self.downloader.extra_download(api_url_2)
                if raw_data_2:
                    flix_prod = re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2).group(1) if re.search(r"window.flixJsCallbacks.pid ='(.*)';", raw_data_2) else None
                
                if distributor and flix_prod:
                    api_url_3 = f'https://media.flixcar.com/delivery/inpage/show/{distributor}/{lang}/{flix_prod}/json?c=jsonpcar{distributor}{lang}{flix_prod}&complimentary=0&type=.html'
                    raw_data_3 = self.downloader.extra_download(api_url_3)
                    if raw_data_3 and distributor and lang and flix_prod:
                        _rgx = re.search(r"jsonpcar" + distributor + lang + flix_prod + r"\((.*)\)", raw_data_3)
                        if _rgx:
                            _json = json.loads(_rgx.group(1))
                            desc_soup = BeautifulSoup(_json['html'], 'lxml')
                            vid_tag = desc_soup.select_one('.flix-jw')
                            if vid_tag and vid_tag.has_attr('value'):
                                _json_playlist = json.loads(vid_tag['value'])
                                if _json_playlist and 'playlist' in _json_playlist and _json_playlist['playlist']:
                                    source = api_url_3
                                    for item in _json_playlist['playlist']:
                                        if 'Video' in item['title']:
                                            value.append('https:'+item['file'])

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Needs to select location before delivery estimation
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value
