import re, json
import logging
import requests
from urllib.parse import urlencode, quote
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template
from strategies import StrategyFactory


class MicrosoftComEnSgWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.microsoft.com/en-sg'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.zero_checker = None

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title - no brand indicated for this retailer
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result
    
    #GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="price") or \
                soup.select_one('.h3.font-weight-normal')

            other_tag = soup.select_one('.total-price .price-value')

            if tag and tag.has_attr('content') and not other_tag:
                source, value = str(tag), tag.get('content')
            else:
                if tag:
                    if tag.select_one('.font-weight-semibold'):
                        source = str(tag)
                        value = tag.select_one('.font-weight-semibold').get_text().strip()
                    else:
                        source, value = str(tag), ' '.join(tag.stripped_strings)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    #GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("meta", { "itemprop":"priceCurrency" })
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag.get('content')
            else:
                # forced default currency
                value = "SGD"
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    #GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            aid = None
            tag = soup.select_one('button.c-button.f-primary.cli_defaultFocus')

            if tag and tag.has_attr('data-m'):
                _json = json.loads(tag['data-m'])
                if _json:
                    pid = _json['pid']
                    aid = _json['tags']['availabilityId'] if 'availabilityId' in _json['tags'] else None
                    sku_tag = soup.find('meta', {'name':'ms.ms_sales_part_number'})
                    sku = _json['tags']['sku'] if 'sku' in _json['tags'] else sku_tag['content']
                    script_tag = soup.find('script', text=re.compile('ReactDOM.hydrate'))

                    if script_tag:
                        script = script_tag.get_text(strip=True)

                    source, stock = self.get_stocks(self, script, pid, sku, aid)
                    if stock:
                        if stock['inStock'] == 'True':
                            value = stock['inStock']
                        else:
                            for availability in stock['availabilities']:
                                for _key in availability['availableLots'].keys():
                                    if sku and availability['catalogSkuId'].lower() == sku:  
                                        value = "outofstock"
                                    else:
                                        rgx = re.search(r'ReactDOM.hydrate\(.*\(.*OneRF_DynamicModules\.ButtonPanel.*buttonsPageBar_AddToCartButton.*document', script)
                                        if rgx:
                                            data = rgx.group()
                                            if data:
                                                data = json.loads(re.search(r'(\{.*\})', data).group(1))
                                                add_to_cart = True
                                                out_of_stock = False
                                                for action in data['Actions']:
                                                    if sku == action['SkuId']:
                                                        if 'Add to cart' == action['AriaLabel']:
                                                            add_to_cart = action['VisibilityConditions'][0]['Value']
                                                        if 'Out of stock' == action['AriaLabel']:
                                                            out_of_stock = action['VisibilityConditions'][0]['Value']
                                                if add_to_cart and not out_of_stock:
                                                    value = "instock"
                                                elif not add_to_cart and out_of_stock:
                                                    value = "outofstock"

                                            else:
                                                value = "instock"

                    elif soup.select_one('button.cli_defaultFocus'):
                        tag = soup.select_one('button.cli_defaultFocus')
                        if tag and tag.has_attr('aria-label'):
                            tag_stock = tag.get('aria-label')
                            if tag_stock in ['Configure now', 'Add to cart']:
                                source, value = str(tag), 'instock'
                            elif tag_stock == 'Out of stock':
                                source, value = str(tag), 'outofstock'
                    else:
                        value = "outofstock" 
                else:
                    value = "instock"

            else:
                tag = soup.select_one('.buy-box-buy-buttons.mb-3 a')
                if tag:
                    source = str(tag)
                    for k, v in tag.attrs.items():
                        if k.lower() == 'aria-disabled':
                            value = '{}={}'.format(k, v)
                            break
                else:
                    tag = soup.select_one('.buy-box-buy-buttons button.btn') 
                    if tag:
                       value = tag.get_text().strip()
                       source = str(tag)
                    else:
                        _json =  self.get_product_json(soup)
                        if _json and 'offers' in _json and 'offers' in _json['offers']:
                            offers = _json['offers']
                            if offers['offers']:
                                value = _json['offers']['offers'][0]['availability']
                                source = str(_json)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    #GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 10 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    #GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("h1", {"id":"page-title"}) or soup.find("h1", {"class":"c-heading-4"})
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            else:
                tag = soup.find('h1',{'id':'DynamicHeading_productTitle'})
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()
                else:
                    tag = soup.select_one("script[type='application/ld+json']")
                    if tag:
                        _json = json.loads(tag.get_text())
                        if _json:
                            source = str(tag)
                            value = _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    #GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    #GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            regex = re.compile('.*c-pivot.*')
            tag = soup.find("section", {"class" : regex})
            if tag:
                tags = tag.findAll("section", { "role":"presentation" })
                if len(tags):
                    source = str(tag)
                    desc = ''
                    for item in tags:
                        desc = desc + ' ' + ' '.join(item.stripped_strings)
                    value = desc
                else:
                    tag = soup.select_one('section#pivot-OverviewTab')
                    if tag:
                        source = str(tag)
                        value = " ".join(tag.stripped_strings) 
                    else:
                        source, value = self._get_desc(soup)          
            else:
                source, value = self._get_desc(soup)

            if value in ['', ' ', None, Defaults.GENERIC_NOT_FOUND.value]:
                tag = soup.select_one('.responsivegrid')
                if tag:
                    [i.decompose() for i in tag.select('script')]
                    [i.decompose() for i in tag.select('style')]
                    source, value = str(tag),  ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    #GET SPECIFICATIONS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            regex = re.compile('.*cli_specifications.*')
            tags = soup.findAll("div", { "class" : regex })
            if len(tags):
                source = str(tags)
                for tag in tags.select("tr"):
                    key = tag.select_one("th")
                    val = tag.select_one("td")
                    value["%s" % key.get_text().strip()] = val.get_text().strip()
            else:
                parent_tag = soup.select_one('section#pivot-TechSpecsTab')
                if parent_tag:
                    tag = [tag for tag in parent_tag.select('div.c-table.f-divided') if 'x-hidden' not in tag.get('class')]

                    if tag: 
                        for li in tag:
                            specs_tag = li.select('tr')
                            source = str(parent_tag)
                            for item in specs_tag:
                                key_val = item.select_one('th')
                                v_val = item.select_one('td')
                                value[key_val.get_text().strip()] = v_val.get_text().strip()

                # tags = soup.select_one("section#pivot-TechSpecsTab")
                # if len(tags):
                #     source = str(tags)
                #     for item in tag.select("ul > li"):
                #         div = item.select("div")
                #         if len(div) >= 2:
                #             value["%s" % div[0].get_text().strip()] = div[1].find("div").get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    #GET IMAGE URLS
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            sku_id = None
            image_list = []
            tag = soup.select_one('#metadataButton button')
            if tag and tag.has_attr('data-m'):
                _json = json.loads(tag.get('data-m'))
                sku_id = _json['tags']['sku']

            tag = soup.find("script", text = re.compile(r".*React\.createElement\(OneRF\_DynamicModules\.DynamicImageGallery*")) or soup.find("script", text = re.compile(r".*React\.createElement\(OneRF\_DynamicModules\.DynamicImage*"))
            if tag:
                _rgx = re.search(r'OneRF\_DynamicModules\.DynamicImageGallery\,(.*)\).*document', tag.get_text()) or re.search(r'OneRF\_DynamicModules\.DynamicImage\,(.*)\).*document', tag.get_text())
                if _rgx: 
                    _json = json.loads(_rgx.group(1))
                    if _json:
                        if 'DisplayData' in _json.keys():
                            source = str(tag)
                            if sku_id:
                                if sku_id.upper() in _json['DisplayData'].keys():
                                    image_list = _json['DisplayData'][sku_id.upper()]['ImagesData']
                                elif len(_json['DisplayData']) == 1 :
                                    image_list = _json['DisplayData']['']['ImagesData']

                                elif 'ImagesData' in _json['DisplayData']['']:
                                    image_list = _json['DisplayData']['']['ImagesData']
                            elif 'Key' in _json['DisplayData']['']:
                                image_list2 = _json['DisplayData']['']['Key']
                                value.append(image_list2)

                            else:
                                image_list = _json['DisplayData']['']['ImagesData']

                            for image in image_list:
                                if 'srcset' in image['PictureTagData'][0]['Attributes'].keys():
                                    value.append(image['PictureTagData'][0]['Attributes']['srcset'])
            
            else:
                tags = soup.select('ul.c-group li > picture img.c-image') or soup.select('.d-flex.my-0.mx-auto img')
                if len(tags):
                    source = str(tags)
                    for img in tags:
                        if img and img.has_attr('src'):
                            value.append(img['src'])
                elif not tags:
                    tag = soup.select_one('div.fc-img-carousel-container img')
                    if tag and tag.has_attr('src'):
                        source = str(tag)
                        value.append(tag['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        return source, value
    
    #GET SHIPPING
    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.ShippingMessage')
            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value
    
    @staticmethod
    def get_stocks(self, script, productId, skuId, availabilityId):
        try:
            payload = [
                {
                    "condition":"",
                    "productId":productId,
                    "skuId":skuId,
                    "inventorySkuId":'',
                    "availabilityId":availabilityId,
                    "distributorId":"9000000013"
                }
            ]
            
            
            rgx = re.search(r'ReactDOM.hydrate\(.*\(.*OneRF_DynamicModules\.SfaPageBar.*document',script)
            if rgx:
                data = rgx.group()
                data = data.split('SfaPageBar, ')[1]
                data = data.split('), document')[0]
                data = json.loads(data)

            else:
                rgx = re.search(r'ReactDOM.hydrate\(.*\(.*OneRF_DynamicModules\.ButtonPanel.*document',script)
                if rgx:
                    data = rgx.group()
                    data = json.loads(re.search(r'(\{.*\})', data).group(1))

            webConditions = data['ModuleMetadata']['WebConditions'] if 'ModuleMetadata' in data else data['WebConditions']
            for i, key in enumerate(webConditions.keys(), 1):
                if skuId in webConditions[key]['Params']:
                    payload[0]['inventorySkuId'] = webConditions[key]['Params'][2]
                    payload[0]['condition'] = 'IsOutOfStock%s' % i
                    if availabilityId:
                        payload[0]['availabilityId'] = availabilityId
                        break
                    else:
                        payload[0]['availabilityId'] = webConditions[key]['Params'][3]
                        break
            
            action = None
            if 'Actions' in data:
                for i in data['Actions']:
                    if i['SkuId'] == payload[0]['skuId']:
                        action = i['SkuId']

            headers= {'Content-Type':'application/json; charset=UTF-8'}
            endpoint = 'https://inv.mp.microsoft.com/v2.0/inventory/SG?MS-CorrelationId=5567ccd5-9f7e-452d-9026-06b73284ac87&MS-RequestId=5567ccd5-9f7e-452d-9026-06b73284ac87&mode=continueOnError'
            res = requests.post(endpoint, data=json.dumps(payload), headers=headers)

            if res:
                _json = json.loads(res.text)
                return endpoint, _json

            elif not action:
                return str(endpoint), None
                
            else: 
                stock = {'inStock':'false'}
                return str(endpoint), stock
    
        except Exception as e: 
            print(e)
            stock = {'inStock': 'false'}
            return str(endpoint), stock
    
    @staticmethod
    def _get_desc(soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        tag = soup.find('div',{'class':'RegionPivot'})
        if tag:
            reg = tag.attrs.get('data-allregions')
            if reg:
                rp = reg.replace('#','').split(',')
                src = []
                temp = []
                for item in rp:
                    if not 'primaryR9' in item:
                        tags = soup.find('div',{'id': item})
                        if tags:
                            src.append(str(tags))
                            temp.append(tags.get_text().strip())
                        source, value = ' '.join(src),' '.join(temp)
        return source,value

    def get_product_json(self, soup):
        tag = soup.select_one('script[type="application/ld+json"]') or soup.find('script', {"type":"application/ld+json"})
        try:
            if tag:
                _json = json.loads(tag.get_text())
                return _json
        except:
            pass
        return None