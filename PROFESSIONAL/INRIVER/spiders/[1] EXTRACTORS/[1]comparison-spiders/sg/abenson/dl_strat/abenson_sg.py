from urllib.parse import urlparse
import json
import time
import re

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class AbensonSgDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None 

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not headers:
                headers = {
                   "Accept": "*/*",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "en-US, en; q=0.5",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"            
                    }
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                if not is_redirect:
                    return res.text
                else:
                    raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')