import json
import logging
import re
from bs4 import BeautifulSoup, Tag, NavigableString
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class AbensonSgWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.abenson.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)


        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".special-price")
            if tag:
                price = tag.get_text()
                source = str(tag)
                value = tag.get_text()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="priceCurrency"]')
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag['content']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="availability"]')
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag['content']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5" 
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
       
        try:
            tag = soup.find('div', class_='title-wrapper')
            tag2 = soup.select_one('[itemprop="ratingValue"]')
            if tag and tag2: # has_attr('itemprop')?
                score = tag.find('span', class_="average-review big-bold")
                score_source = str(tag)
                score_value = str(score.string)
                reviews_source = str(tag2)
                reviews_value = tag2.get_text()
                
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1', class_=".page-title")
            if tag:
                title = tag.get_text()
                source = str(tag)
                value = title
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="brand"]')
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag['content']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', class_="PDP_overview2")
            if tag:
                val = ""
                for p in tag.find_all('p', class_="MsoNormal"):
                    val += str(p.string) # use get_text()
                value = val
                source = str(tag)
         
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
        
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('table', id="product-attribute-specs-table")
            if tag:
                dict = {}
                keys = []
                values = []

                for tags in tag.find('tbody'):
                    if (type(tags) == Tag):
                        for specs in tags.find_all(attrs={"data-th":"Features"}):
                            for info in specs.find_all('p'):
                                keys.append(info.strong.get_text())
                                info.strong.extract()
                                values.append(info.get_text())

                for x in range(0,len(keys)):
                    dict.update({keys[x]:values[x]})
                    
                source = str(tag)
                value = dict
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('script', text = re.compile('mage/gallery/gallery'))
            if tag:
                json_val = json.loads(tag.text.strip())
                image_url = [x['full'] for x in json_val['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data']]
                source = str(tag)
                value = list(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value
         

    