import requests, json, re
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ShopbComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def get_ratings(self, soup):
        try:
            headers = {
                'Accept': '*/*',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.54 Safari/537.36'
            }
                
            # extract data for reviews api request params requirements (https://prnt.sc/Je3jS9ZSdvE7)
            try:
                script_tag_store_key, tag_store_key = soup.select('script[type="text/javascript"]'), str()
                if script_tag_store_key:
                    for x in script_tag_store_key:
                        if re.search(r'service\.yourviews\.com\.br', x.get_text()):
                            matcher =  re.search(r'(yvs.src[\s\S]+\")(;)', x.get_text()).group(0)
                            tag_store_key = matcher if matcher else None
                    if tag_store_key:
                        tag_store_key = tag_store_key.split('/')[-2]

                script_tag_product_store_id, product_store_id = soup.select('script[type="text/javascript"][charset="utf-8"]'), str()
                if script_tag_product_store_id:
                    for x in script_tag_product_store_id:
                        if re.search(r'removePaymentItems',x.get_text()):
                            matcher = re.search(r'(\[)(.)+?(;)',x.get_text()).group(0)
                            product_store_id = matcher if matcher else None
                    if product_store_id:
                        product_store_id = re.search(r'[0-9]+', product_store_id.split(',')[0]).group(0)

                url = f''
                if tag_store_key and product_store_id:
                    url = f'https://service2.yourviews.com.br/review/GetReview?storeKey={tag_store_key}&productStoreId={product_store_id}&extendedField=&yv__rpl=?'
            except Exception as e:
                print('Error in dlstrat -> get_ratings() -> falure to extract ratings params request requiremets, with message, ', e)
 
            res = self.requester.get(url, timeout=10, headers=headers, cookies=None)
            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
