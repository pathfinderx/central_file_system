import json
import requests
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ShopbComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
