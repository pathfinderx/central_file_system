import logging, json, re

from bs4 import BeautifulSoup, NavigableString

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ShopbComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shopb.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.preco-produto.destaque-avista')
            if tag:
                price_tag = tag.select_one('strong.preco-promocional.cor-principal')
                if price_tag and price_tag.has_attr('data-sell-price'):
                    source, value = str(value), price_tag.get('data-sell-price')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            value = 'BRL'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.cor-secundaria.disponibilidade-produto')
            if tag:
                availability_tag = tag.select_one('b')
                source, value = str(tag), availability_tag.get_text().strip()
            else:
                unavailable_tag = soup.select_one('div.avise-btn > input')
                if unavailable_tag and unavailable_tag.has_attr('value'):
                    source, value = str(unavailable_tag), unavailable_tag.get('value').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass # ratings and reviews: javascript rendered

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.info-principal-produto > h1.nome-produto.titulo.cor-secundaria')
            if tag:
                value = tag.get_text().strip()
                source = str(value)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.codigo-produto')
            if tag:
                brand_tag = tag.select_one('a[itemprop="url"]')
                if brand_tag:
                    source, value = str(tag), brand_tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.abas-custom')
            if tag:
                merged_descriptions, descriptions_list, description_tag = '', [], tag.select_one('div.texto-descr-produto')
                if description_tag:
                    # only for DESCRIÇÃO
                    descriptions_list.append(description_tag.get_text().replace('\n', ' ').strip())

                    # includes: REQUISITOS, ITENS INCLUSOS, GARANTIA (ref: https://prnt.sc/EcAII8yVuu6G)
                    # exludes: ESPECIFICAÇÕES and TAGS 
                    for i,v in enumerate(description_tag.next_siblings):
                        if (not isinstance(v,NavigableString) and 
                            not v.has_attr('class')):
                            if (not re.search(r'ESPECIFICAÇÕES', v.get_text().strip()) and 
                                not re.search(r'TAGS', v.get_text().strip()) and 
                                not v.find('strong')):
                                descriptions_list.append(v.get_text().replace('\n', ' ').strip())

                    # merge
                    if descriptions_list:
                        for index,value in enumerate(descriptions_list):
                            if (index == len(descriptions_list) -1): # if last index, no '+' (next description indicator)
                                merged_descriptions += ''.join(value) 
                            else:
                                merged_descriptions += ''.join(value) + ' + '

                    if merged_descriptions:
                        source, value = str(tag), merged_descriptions.replace(r'xa0', ' ')
                        

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select_one('div.abas-custom')
            if tag:
                specs_tag_parent = tag.select_one('ul')
                specs_tag_nodes = specs_tag_parent.select('li')

                if specs_tag_nodes:
                    source = str(tag)
                    for x in specs_tag_nodes:
                        if x.find('strong'):
                            specs_key = x.find('strong').get_text().replace(':','').replace('\xa0','')
                            x.find('strong').extract()
                            specs_value = x.get_text().replace('\xa0','')
                            value.update({specs_key:specs_value})

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select_one('div.produto-thumbs.thumbs-horizontal').select('img')
            if tag:
                for x in tag:
                    if x.has_attr('data-mediumimg'):
                        value.append(x.get('data-mediumimg'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        # not available
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # not available
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # not available
        return source, value