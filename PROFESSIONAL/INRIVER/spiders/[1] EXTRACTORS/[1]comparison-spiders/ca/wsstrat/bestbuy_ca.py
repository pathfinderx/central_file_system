import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BestbuyCaWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.bestbuy.ca'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self._json = self.get_json(soup)
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand - No brand stated
        # result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)   

        # Extract delivery - no delivery stated
        # result["delivery"]["source"], result["delivery"]["value"]= self._get_delivery(soup)

        return result

    def get_json(self, soup):
        _json = {}
        try:
            tag = soup.find("script", text=re.compile('.*window.__INITIAL_STATE__.*'))
            
            if tag:
                get_script_data = re.search('({.*})', tag.get_text())
                if get_script_data:
                    get_script_data = get_script_data.group(1)
                    _json = json.loads(get_script_data, strict=False)

        except Exception as e:
            print(e)
        
        return str(tag), _json


    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'price'})

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag["content"]
            
            if not tag:
                source, _json = self._json
                if _json and 'product' in _json and 'product' in _json['product'] and 'priceWithoutEhf' in _json['product']['product']:
                    value = str(_json['product']['product']['priceWithoutEhf'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'priceCurrency'})

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag["content"]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            disabled_add_to_cart = soup.select_one('.addToCartButtonContainer_2ZF35 .addToCartButton.regular_cDhX6.disabled_XY3i_')
            if disabled_add_to_cart:
                source, value = str(disabled_add_to_cart), disabled_add_to_cart['class'][-1]
            else:
                tag = soup.find("link", {"itemprop": "availability"})

                if tag and tag.has_attr('href'):
                    enabled_add_to_cart = soup.select_one('.addToCartButtonContainer_2ZF35 .addToCartButton.regular_cDhX6')
                    if enabled_add_to_cart:
                        source, value = str(enabled_add_to_cart), enabled_add_to_cart['class'][-1]
                    else:
                        source, value = str(tag), tag["href"]

                if not tag:
                    source, _json = self._json
                    if _json and 'product' in _json and 'availability' in _json['product'] and 'shipping' in _json['product']['availability'] and 'status' in _json['product']['availability']['shipping']:
                        value = str(_json['product']['availability']['shipping']['status'])


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            review_tag = soup.find("span", {"itemprop":"ratingCount"})
            score_tag = soup.find("meta", {"itemprop":"ratingValue"})

            if review_tag:
                reviews_source, reviews_value = str(review_tag), review_tag.get_text().strip()
  
            if score_tag and score_tag.has_attr('content'):
                score_source, score_value = str(score_tag), score_tag["content"]

            if not review_tag or (not review_tag and not score_tag):
                source, _json = self._json
                if _json and 'product' in _json and 'product' in _json['product'] and 'customerRatingCount' in _json['product']['product']:
                    reviews_source, reviews_value = source, str(_json['product']['product']['customerRatingCount'])
                
                if _json and 'product' in _json and 'product' in _json['product'] and 'customerRating' in _json['product']['product']:
                    score_source, score_value = source, str(_json['product']['product']['customerRating'])
                

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value  = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("h1", {"itemprop": "name"})

            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            
            if not tag:
                source, _json = self._json

                if _json and 'product' in _json and 'product' in _json['product'] and 'name' in _json['product']['product']:
                    value = _json['product']['product']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # def get_brand(self, soup):
    #     source = Defaults.GENERIC_NOT_FOUND.value
    #     value = Defaults.GENERIC_NOT_FOUND.value

    #     try:
    #         _json = None
    #         tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"})
    #         if tag:
    #             _json = json.loads(tag.get_text())

    #         if _json:
    #             source = str(tag)
    #             if "brand" in _json.keys():
    #                 value = _json['brand']['name']

    #     except Exception as e:
    #         self.logger.exception(e)
    #         value = FailureMessages.BRAND_EXTRACTION.value

    #     return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:    
            tag, _json = self._json
            description = []
            src = []
            tag = soup.find('div', {'itemprop': 'description'})
            if tag:
                description.append(" ".join(tag.stripped_strings))
                src.append(str(tag))
            
            parent_tag = soup.select_one('#MoreInformation')
            if parent_tag:
                tags = parent_tag.select('li')
                if tags:
                    src.append(str(parent_tag))
                    for tag in tags:
                        description.append(tag.get_text().strip())
            
            if _json and 'product' in _json:
                if 'product' in _json['product']:
                    if 'longDescription' in _json['product']['product'] and _json['product']['product']['longDescription']:
                        _soup = BeautifulSoup(_json['product']['product']['longDescription'], 'lxml')
                        desc = ' '.join(_soup.stripped_strings)
                        if desc:
                            description.append(desc)
            
            tag = soup.find('div', {'class': re.compile(r'productDescription_')})
            if tag:
                src.append(str(tag))
                description.append(' '.join(tag.stripped_strings))

            if description:
                source = " + ".join(src)
                value = " + ".join(description)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:

            tag, _json = self._json
            if _json and 'product' in _json.keys():
                if _json and 'specs' in _json['product']['product'].keys():
                    specs = _json['product']['product']['specs']
                    source = str(tag)
                    for base_spec in specs:
                        for spec in specs[base_spec]:
                            value[spec['name']] = spec['value']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag, _json = self._json
            if _json and 'product' in _json.keys():
                if 'additionalImages' in _json['product']['product'].keys():
                    images = _json['product']['product']['additionalImages']
                    source = str(tag)
                    for image in images:
                        value.append(image)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag, _json = self._json
            if _json and 'product' in _json.keys():
                if 'productVideos' in _json['product']['product'].keys():
                    videos = _json['product']['product']['productVideos']
                    source = str(tag)
                    for video in videos:
                        video_url = 'https://youtu.be/' + video['id'] 
                        value.append(video_url)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value


    
    
