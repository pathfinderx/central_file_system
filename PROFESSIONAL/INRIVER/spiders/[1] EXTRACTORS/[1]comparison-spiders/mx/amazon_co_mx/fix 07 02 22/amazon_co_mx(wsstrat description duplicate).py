import json
import logging
import re
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template, get_result_buybox_template


class AmazonComMxWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.amazon.com.mx'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        buybox_template = get_result_buybox_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)  
        
        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)        

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)  

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)               

        # Extract Buybox
        result["buybox"] = self._get_buybox(soup, buybox_template)

        return result 

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = self.check_tag(    
                soup.select_one("div#apex_desktop .priceToPay span.a-offscreen"),  
                soup.select_one('.priceToPay span.a-offscreen'),
                soup.select_one('span.a-price.a-text-price.a-size-medium span.a-offscreen'),         
                soup.select_one('#priceblock_ourprice'),
                soup.select_one('#price_inside_buybox'),
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('#mbc-price-1'),
                soup.select_one('.a-price.a-text-price.a-size-medium span'),
                soup.select_one('#tp_price_block_total_price_ww span'),
                attrs = ['data-asin-price']
            )

            if tag:
                source, value = tag

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('#attach-base-product-currency-symbol'),
                attrs=['data-asin-currency-code', 'value']
                )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#availability')

            if tag:
                tag = self._sanitize(tag)
                source, value = str(tag), " ".join(tag.stripped_strings)
            
            #Temporary fix:check if price is not found stocks should be OOS 
            #issue: https://trello.com/c/zjKpCDpT/3088-nintendo-mx-comparisons-wrong-stocks-status-in-amazoncommx
            if not tag:
                tag = self.get_price(soup)
                if tag[1] == 'NOT_FOUND':
                    value = 'outofstock'
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = self.check_tag(
                soup.select_one('[data-hook="rating-out-of-text"]'),
                soup.select_one('#acrPopover'),
                attrs = ['title']
            )
            tag_review = self.check_tag(
                soup.select_one('#acrCustomerReviewText'),
                )

            if tag_score and tag_review:
                reviews_source, reviews_value  = tag_review
                score_source, score_value = tag_score
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#productTitle')

            if tag:
                source, value = str(tag), tag.get_text().strip()                            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('th', text=re.compile(r'Marca', re.IGNORECASE)) # Brand in Specs
            if tag:
                parent_tag = tag.find_parent('tr')
                if parent_tag:
                    brand_tag = parent_tag.select_one('td')
                    if brand_tag:
                        source = str(parent_tag)
                        value = brand_tag.get_text().strip()
            elif soup.find('span', text=re.compile(r'.*Marca.*', re.IGNORECASE)): # Brand in top Desc
                tag = soup.find('span', text=re.compile(r'.*Marca.*', re.IGNORECASE))
                if tag:
                    parent_tag = tag.find_parent('tr')
                    if parent_tag:
                        brand_tag = parent_tag.select('td')
                        if len(brand_tag) == 2:
                            source = str(parent_tag)
                            value = brand_tag[-1].get_text().strip()
            else:
                tag = self.check_tag(
                    soup.select_one('#bylineInfo'),
                    soup.select_one('#mbc'),
                    attrs=['data-brand']
                )

                if tag:
                    source, value = tag
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _src = []
            description = []
            # Product Description Top
            tags = soup.select('div#feature-bullets ul li')
            if len(tags):
                _src.append(str(tags))
                for li in tags:
                    if not li.has_attr('id'):
                        description.append(' '.join(li.stripped_strings))
            
            # Product Description Bottom
            tag = soup.find('div',{'id':'productDescription'})
            if tag:
                _src.append(str(tag))
                description.append(' '.join(tag.stripped_strings))
            else:
                tags = soup.select('td.apm-top')
                if len(tags):
                    _src.append(str(tags))
                    for tag in tags:
                        description.append(' '.join(tag.stripped_strings))

            # From Manufacturer
            tag = soup.select_one('#dpx-aplus-product-description_feature_div') or soup.select_one('#aplus_feature_div')
            if tag:
                _src.append(str(tag))
                tag = self._sanitize(tag)
                if tag.select('.a-expander-prompt'):
                    for item in tag.select('.a-expander-prompt'):
                        item.extract()
                description.append(' '.join(tag.stripped_strings)           )

            tags = soup.select('div.apm-sidemodule-textright')
            if tags:
                _src.append(str(tags))
                for tag in tags:
                    description.append(' '.join(tag.stripped_strings))

            tag = soup.find('div',{'class':'apm-sidemodule-textleft'})
            if tag:
                _src.append(str(tag))
                description.append(' '.join(tag.stripped_strings))

            extra_desc_tag = soup.select_one('[id="productDescription"]')
            if extra_desc_tag:
                # value = value + " " + extra_desc_tag.get_text().strip()
                _src.append(str(tag))
                description.append(' '.join(extra_desc_tag.stripped_strings))

            if description:
                # has_string to check if array has string or value
                has_string = False
                for _string in description:
                    if '' != _string:
                        has_string = True

                # removing duplicate values per index
                temp_container = list() 
                [temp_container.append(x) for x in description if x not in temp_container]
                        
                # merge all values from temp_container with '+' sign as separator
                if has_string:
                    source = " + ".join(_src)
                    value = " + ".join(temp_container)

                # remove any specified trailing characters
                if value:
                    value = value.strip(' + ').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            # tag_key = self.check_tag(
            #     soup.select('#prodDetails tr td.label'),
            #     soup.select('.wrapper.CAlocale tr td.label'),
            #     soup.select('#productDetails_detailBullets_sections1 tr th'),
            #     soup.select('.tsRow span:nth-of-type(1)'),
            #     soup.select('#product-details-grid_feature_div tr td.label'),
            #     )
            # tag_val = self.check_tag(
            #     soup.select('#prodDetails tr td.value'),
            #     soup.select('.wrapper.CAlocale tr td.value'),
            #     soup.select('#productDetails_detailBullets_sections1 tr td'),
            #     soup.select('.tsRow span:nth-of-type(2)'),
            #     soup.select('#product-details-grid_feature_div tr td.value'),
            # )

            # if tag_key and tag_val:
            #     source, value = self._build_spec(tag_key, tag_val)

            tag = (
                soup.select('#productDetails_techSpec_section_1 tr'),
                soup.select('#productDetails_detailBullets_sections1 tr'),
                soup.select('#detailBullets_feature_div li'),
            )

            if tag:
                source = str(tag)
                for _data in tag:
                    for val in _data:
                        val = self._sanitize(val)
                        key_val = val.select_one('th')
                        v_val = val.select_one('td')
                        if key_val and v_val:
                            value[key_val.get_text(strip=True)] = v_val.get_text(strip=True)
                        else:
                            spec = val.get_text(strip=True)
                            if ':' in spec:
                                key = spec.split(':')[0].replace('\n\u200f\n','')
                                val = spec.split(':')[1].replace('\n\u200e','')
                                if key and val:
                                    value[key] = val
                                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            script_tag = soup.find("script",text=re.compile("'colorImages': {.*}"),attrs={"type":"text/javascript"})
            if script_tag:
                source = str(script_tag)
                script_regex = re.search("'colorImages': {.*}", script_tag.text.strip())
                script_json = json.loads(script_regex.group(0).replace("'colorImages': ","").replace("'initial'",'"initial"'))
                images = script_json["initial"]
            
                for image in images:
                    img = image["hiRes"]
                    if not img:
                        img = image["large"]
                    value.append(img)
            else:
                script_tag = soup.find("script", text=re.compile(".*P.AboveTheFold.*"), attrs={"type":"text/javascript"})
                
                if script_tag:
                    source = str(script_tag)
                    var_data = re.search(r"data = ({[\s\S]*});", script_tag.get_text().strip())
                    if var_data:
                        json_data = json.loads(var_data.group(1).replace("\'", "\""))
                        for img_list in json_data["colorImages"]["initial"]:
                            if img_list["hiRes"] and img_list["hiRes"] not in value:
                                value.append(img_list["hiRes"])

                            if img_list["large"] and img_list["large"] not in value:
                                value.append(img_list["large"])

                            if img_list["thumb"] and img_list["thumb"] not in value:
                                value.append(img_list["thumb"])                         

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            if tag:
                text = tag.get_text().strip().split('jQuery.parseJSON')[1]
                rgx = re.search(r'(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
                source = str(tag)
                
                if rgx:
                    _json = json.loads(rgx.group(1).replace('\\', ''))    #json.loads(rgx.group(1).replace('l\\', ''))  
                    if 'videos' in _json:
                        value = [i['url'] for i in _json['videos']]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value  

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one("#delivery-message"),
                soup.select_one("#ddmDeliveryMessage"),
                soup.select_one('#contextualIngressPtLabel_deliveryShortLine')
                )
            if tag:
                source, value = tag

            # As per validator "DQ should be the stock indicator"
            availability_tag = soup.select_one('#availability')
            if availability_tag:
                value = value + " " + availability_tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
             soup.select_one("#olp-upd-new-freeshipping .a-color-base"),
             soup.select_one('#shippingMessageInsideBuyBox_feature_div'),
             soup.select_one(".a-size-base.a-color-secondary")
             )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def _get_buybox(self, soup, template):
        buybox_result = deepcopy(template)

        try:
            buybox_tag = soup.select_one("#buybox") or soup.select_one(".a-box-group")

            if not buybox_tag:
                temp_tag = soup.select_one(".a-box")
                if 'a-alert' not in temp_tag.get('class'):
                    buybox_tag = temp_tag
            
            if buybox_tag:
                buybox_result['source'] = str(buybox_tag)

                # Extract text
                buybox_result['text'] = self.__get_sanitized_tag(buybox_tag)
                # Extract price and currency
                buybox_result["price"]["value"], buybox_result["price"]["currency"] = self.__get_buybox_price_and_currency(buybox_tag)
                # Extract delivery
                buybox_result["delivery"] = self.__get_buybox_delivery(soup, buybox_tag)
                # Extract shipping
                buybox_result["shipping"] = self.__get_buybox_shipping(buybox_tag)
                # Extract availability
                buybox_result["availability"] = self.__get_buybox_availability(soup, buybox_tag)
                # Extract retailer
                buybox_result["retailer"] = self.__get_buybox_retailer(buybox_tag)
                # Extract used_price
                buybox_result["used_price"] = self.__get_buybox_used_price(soup, buybox_tag)
                # Extract new_price
                buybox_result["new_price"] = self.__get_buybox_new_price(soup, buybox_tag)
                # Extract cartable
                buybox_result["cartable"]['source'], buybox_result["cartable"]['value'] = self.__get_buybox_cartable(soup, buybox_tag)
                # Extract buyable
                buybox_result["buyable"]['source'], buybox_result["buyable"]['value'] = self.__get_buybox_buyable(soup, buybox_tag)
                # Extract add_on
                buybox_result["add_on"] = self.__get_buybox_add_on(buybox_tag)            
                
        except Exception as e:
            self.logger.exception(e)
            buybox_result['text'] = FailureMessages.BUYBOX_EXTRACTION.value

        return buybox_result

    def __get_buybox_price_and_currency(self, buybox_tag):
        price = Defaults.GENERIC_NOT_FOUND.value
        currency = 'MXN' # Defaulted

        try:
            tag = buybox_tag.select_one("#price_inside_buybox") or\
                        buybox_tag.select_one("#newBuyBoxPrice") or\
                        buybox_tag.select_one("#unqualifiedBuyBox .a-color-price")

            if not tag:
                tag = buybox_tag.select_one(".a-color-price")
                if tag:
                    if "a-size-medium" not in tag.get("class") or \
                    tag.parent.get("id") in ["availability", "outOfStock"]:
                        tag = None

            if tag:
                # sample : $2,499.00
                price = tag.get_text().strip()
                currency = 'MXN' # Defaulted

            if not bool(re.search('\d', price)):
                tag = buybox_tag.select_one('.a-price.a-text-price.a-size-medium span') or \
                    buybox_tag.select_one('.a-offscreen')

                if tag:
                    price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            price = FailureMessages.BUYBOX_EXTRACTION.value
            currency = FailureMessages.BUYBOX_EXTRACTION.value

        return price, currency
    
    def __get_buybox_delivery(self, soup, buybox_tag):
        delivery = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#ddmDeliveryMessage") or soup.select_one("#delivery-message")
            if tag:
                delivery = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            delivery = FailureMessages.BUYBOX_EXTRACTION.value

        return delivery

    def __get_buybox_shipping(self, buybox_tag):
        shipping = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#price-shipping-message")
            if tag:
                shipping= tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            shipping = FailureMessages.BUYBOX_EXTRACTION.value

        return shipping
    
    def __get_buybox_availability(self, soup, buybox_tag):
        availability = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#availability") or buybox_tag.select_one("#outOfStock .a-text-bold") or soup.select_one("#availability")
                
            if tag:
                availability = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            availability = FailureMessages.BUYBOX_EXTRACTION.value

        return availability
    
    def __get_buybox_retailer(self, buybox_tag):
        retailer = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#merchant-info")
            if tag:
                if tag.select_one('#sellerProfileTriggerId'):
                    retailer = tag.select_one('#sellerProfileTriggerId').get_text().strip()
                else:
                    retailer = re.sub(r'[^\w ]+', '', tag.get_text()
                                                .split("por ")[-1].split(".")[0]).strip()
            elif buybox_tag.select_one('#sellerProfileTriggerId'):
                retailer = buybox_tag.select_one('#sellerProfileTriggerId').get_text().strip()
            else:
                retailer = "Amazon" # Defaulted in Ultima
            
        except Exception as e:
            self.logger.exception(e)
            retailer = FailureMessages.BUYBOX_EXTRACTION.value

        return retailer

    def __get_buybox_used_price(self, soup, buybox_tag):
        used_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #usedPrice")
            if tag:
                used_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            used_price = FailureMessages.BUYBOX_EXTRACTION.value

        return used_price
    
    def __get_buybox_new_price(self, soup, buybox_tag):
        new_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #newBuyBoxPrice")
            if tag:
                new_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            new_price = FailureMessages.BUYBOX_EXTRACTION.value

        return new_price

    def __get_buybox_cartable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        cartable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box .a-icon-cart")
            if tag:
                source = str(tag)
                cartable = str(True)
            else:
                cartable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            cartable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, cartable

    def __get_buybox_buyable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        buyable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-icon-buynow")
            if tag:
                source = str(tag)
                buyable = str(True)
            else:
                buyable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            buyable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, buyable

    def __get_buybox_add_on(self, buybox_tag):
        add_on = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = buybox_tag.select(".abbListItem")
            if tags:
                add_on = ", ".join([self.__get_sanitized_tag(tag) for tag in tags])
            
        except Exception as e:
            self.logger.exception(e)
            add_on = FailureMessages.BUYBOX_EXTRACTION.value

        return add_on        

    @staticmethod
    def check_tag(*tags, attrs=[]):
        for tag in tags:
            if not isinstance(tag, list):
                if tag and attrs:
                    for attr in attrs:
                        if tag.has_attr(attr):
                            if len(tag.get(attr)) > 0:
                                return str(tag), tag.get(attr)
                        else:
                            if len(tag.get_text().strip()) > 0:
                                return str(tag), tag.get_text().strip()
                elif tag:
                    return str(tag), tag.get_text().strip()
            else:
                if tag:
                    return tag
        return None

    def _build_spec(self, key_tag, val_tag):
        key_tag = [self._sanitize(tag) for tag in key_tag]
        val_tag = [self._sanitize(tag) for tag in val_tag]
        value = {" ".join(k.stripped_strings):" ".join(v.stripped_strings) for k,v in zip(key_tag, val_tag)}
        return str(key_tag+val_tag), value 
    
    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag    
    
    @staticmethod
    def __get_sanitized_tag(tag):
        tags_to_extract = ['script', 'style']

        for i in tags_to_extract:
            for j in tag.select(i):
                j.extract()

        return ' '.join(tag.stripped_strings)