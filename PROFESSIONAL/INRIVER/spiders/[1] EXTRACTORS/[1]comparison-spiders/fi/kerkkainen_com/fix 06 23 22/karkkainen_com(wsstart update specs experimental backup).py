import json
import logging
import re
from urllib.parse import urlencode
from random import randint
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class KarkkainenComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.karkkainen.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls - No Videos
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org.*'), attrs= {"type":"application/ld+json"})

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text().strip(), strict=False)
                if "offers" in _json.keys():
                    if "price" in _json["offers"].keys():
                        content = _json["offers"]["price"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org.*'), attrs= {"type":"application/ld+json"})

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text().strip(), strict=False)
                if "offers" in _json.keys():
                    if "priceCurrency" in _json["offers"].keys():
                        content = _json["offers"]["priceCurrency"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            tag = soup.find('div', {'id':re.compile(r'availability-entitledItem_*')})
            if tag and tag.has_attr('data-json') and tag['data-json']:
                _json = json.loads(tag.get('data-json'))
                source = str(tag)
                if 'inventoryStatus' in _json:
                    value = _json['inventoryStatus']
                elif len(_json) >= 1 and _json[0]:
                    if 'inventoryStatus' in _json[0]:
                        value = _json[0]['inventoryStatus']
                    for keys in _json:
                        if keys['productId'] == '476261501': #color variant issue temporary fix
                            value = keys['inventoryStatus']
            else:
                tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org.*'), attrs= {"type":"application/ld+json"})

                if tag:
                    source = str(tag)
                    _json = json.loads(tag.get_text().strip(), strict=False)
                    if "offers" in _json.keys():
                        if "availability" in _json["offers"].keys():
                            content = _json["offers"]["availability"]

                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org.*'), attrs= {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text().strip(), strict=False)
                if "aggregateRating" in _json.keys():
                    # Reviews
                    if "reviewCount" in _json["aggregateRating"].keys():
                        reviews_source = str(tag)
                        reviews_value = _json["aggregateRating"]["reviewCount"]

                    # Ratings
                    if "ratingValue" in _json["aggregateRating"].keys():
                        score_source = str(tag)
                        score_value = _json["aggregateRating"]["ratingValue"]

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org.*'), attrs= {"type":"application/ld+json"})

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text().strip(), strict=False)
                if "name" in _json.keys():
                    content = _json["name"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop':'brand'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pre_desc = ''
            short_specs = ''
            source_list = []
            value_list = []

            tag = soup.select_one('.short-description')

            if tag:
                source_list.append(str(tag))
                value_list.append(' '.join(tag.stripped_strings))

            desc_tag = soup.select_one(".long-description")

            if desc_tag:
                source_list.append(str(desc_tag))
                # Extract ul tags
                for spec in desc_tag.select('li'):
                    short_specs += ("\n>" + spec.get_text().strip())
                    spec.decompose()
                
                value_list.append(' '.join(desc_tag.stripped_strings))
                value_list.append(short_specs)
                
            if source_list and value_list:
                source, value = ' '.join(source_list), ' '.join(value_list)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict
        temp_value = {}
        try:
            
            specs = []
            tag = soup.select_one('.descriptive-attributes')

            if tag:
                specs.append(tag)
                source = str(tag) 
            
            tag = soup.select_one('.ean-part-number')

            if tag:
                specs.append(tag)
                if source == 'NOT_FOUND':
                    source = str(tag)
                else: 
                    source = source + ' + ' + str(tag)

            if specs:
                temp_value = self._get_initial_specs(specs)
                value = temp_value

            product_data = soup.find('div', {'id':re.compile(r'availability-entitledItem_[\s\S]')})
            if product_data and product_data.has_attr('data-json') and product_data['data-json']:
                _json, inventoryStatus = json.loads(product_data.get('data-json')), None
                inventoryStatus = [v.get('inventoryStatus') for k,v in enumerate(_json)] if _json else None

            # if not value:
            if (not temp_value['EAN:'] or not temp_value) and 'Available' in inventoryStatus:
                payload=dict(
                    storeId= None,
                    langId= '-11',
                    catalogId= None,
                    catalogEntryId= None,
                    productId= None,
                    requesttype='ajax'
                )

                # tag = soup.select_one('input[name="storeId"]')
                # payload['storeId'] = tag.get('value') if tag else ''

                # tag = soup.select_one('input[name="langId"]')
                # payload['langId'] = tag.get('value') if tag else ''

                # tag = soup.select_one('input[name="catalogId"]')
                # payload['catalogId'] = tag.get('value') if tag else ''

                # scrapping the warranty or Takuu field
                ######################################
                tag = soup.select_one('ul.descriptive-attributes')
                if tag:
                    warranty_tag = tag.find_previous()
                    if warranty_tag:
                        match = re.search(r'\"Takuu\"\: \"(.*?)\"',warranty_tag.getText())
                        if match:
                            value['Takuu:'] = match.group(1)
                        else:
                            span_tags = tag.find_all('span')
                            if span_tags:
                                value['Takuu:'] = span_tags[1].getText().strip()
                value.update(temp_value)
                #################################

                tag = soup.find('script', text=re.compile(r'var productID'))
                if tag:
                    script_tag = tag.find_next()
                    if script_tag:
                        match = re.search(r'InventoryStatusJS\(({.*?})',script_tag.getText())
                        if match:
                            ids = re.search(r'.*?(\d+).*?(\d+).*?(.\d+)', match.group(1))
                            if ids:
                                payload['storeId'] = ids.group(1)
                                payload['catalogId'] = ids.group(2)
                                payload['langId'] = ids.group(3)

                tag = soup.find('div', {'id':re.compile(r'availability-entitledItem_*')})
                if tag and tag.has_attr('data-json') and tag['data-json']:
                    _json = json.loads(tag.get('data-json'))
                    if 'productId' in _json[0]:
                        for item in _json:
                            # removed the check for 'Available' to also get specs from unavailable products
                            if item['inventoryStatus']: # item['inventoryStatus'] == 'Available':
                                payload['catalogEntryId'] = item['productId']
                                break
                # previous code shows productId and catalogEntryId is probably the same. but code is not replaced.
                # as possibly having a different IDs on some products.

                tag = soup.find('script', text=re.compile(r'var productID'))
                if tag:
                    _rgx = re.search(r'var productID = "(.*)";', tag.get_text())
                    if _rgx:
                        payload['productId'] = _rgx.group(1)
                        if not payload['catalogEntryId']:
                            payload['catalogEntryId'] =  _rgx.group(1)

                api_url = 'https://www.karkkainen.com/GetCatalogEntryDetailsByIDView'
                headers = {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
                    'X-Requested-With': 'XMLHttpRequest'
                }
                try:
                    _rawdata = self.downloader.extra_download(api_url, headers=headers, data=payload)
                    if _rawdata:
                        _json = json.loads(_rawdata)
                        if 'catalogEntry' in _json and 'catalogEntryIdentifier' in _json['catalogEntry'] and 'externalIdentifier' in _json['catalogEntry']['catalogEntryIdentifier']:
                            if 'mfPartNumber' in _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']:
                                if not value['EAN:']:
                                    value['EAN:'] = _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']['mfPartNumber']
                            if 'partNumber' in _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']: 
                                if not value['Tuotenumero:']: 
                                    value['Tuotenumero:'] = _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']['partNumber']
                            if 'spnu' in _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']:
                                if not value['Toimittajan tuotenumero:']:       
                                    value['Toimittajan tuotenumero:'] = _json['catalogEntry']['catalogEntryIdentifier']['externalIdentifier']['spnu']
                        source = api_url
                except:
                    value = {}
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        image_url = None

        try:
            tag = soup.select(".image-viewer-items-list a")

            if not tag:
                tag = soup.select('.image_container a')

            if tag:
                source = str(tag)
                for link in tag:
                    if link.has_attr('href') and 'https://img' in link.get('href'):
                        image_url = link.get('href')
                        value.append(image_url)

                    if link.has_attr('data-image') and image_url == None:
                        image_url = link.get('data-image')
                        value.append(image_url)
                    
                
                    if link.img:
                        if link.img.has_attr('src') and image_url == None:
                            image_url = link.img.get('src')
                            value.append(image_url)
            add_img_tag = soup.find('div', text=re.compile('"ItemZoomImage" : "'))
            if add_img_tag:
                _data = json.loads(add_img_tag.get_text().replace('\t','').replace('\n','').replace("\'\'",'""'))
                if _data and 'ItemZoomImage' in _data[0] and _data[0]['ItemZoomImage']:
                    add_img = _data[0]['ItemZoomImage']
                    if not add_img in value:
                        value.append(add_img)
            
            if len(value) == 1 and value[0] == '':
                value = []
                source = Defaults.GENERIC_NOT_FOUND.value


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            params = {
                "maxItems": "15",
                "ytwv": "",
                "_b": "Chrome",
                "_bv": "89.0.4389.90",
                "p": "1",
                "_w": "1903",
                "_h": "947",
                "_pl": "fi",
                "_cl": "fi",
                "hn": "www.karkkainen.com",
                "sId": "s:bi6Uw9RYp9iG3oP7NIMB80KB6EoPpZ5j.Tm/0MRvpZwW5wd/47hcldZ6p/rwQZ5bSBPT5No55hho"
            }

            tag = soup.select_one('[name="productBreadCrumb"]')

            if tag and tag.has_attr('value'):
                params['categoryTree'] = ' / '.join(tag['value'].split(' / ')[:-1])

            tag = soup.select_one('.svea_calculator')

            if tag and tag.has_attr('data-price'):
                params['productPrice'] = tag['data-price']

            tag = soup.select_one('[name="productName"]')

            if tag and tag.has_attr('value'):
                params['productTitle'] = tag['value']

            tag = soup.find('span', {'id': re.compile('item_EAN_.*')})

            if tag:
                params['ean'] = tag.get_text(strip=True)

            tag = soup.select_one('[data-partnumber]')

            if tag:
                params['SKU'] = tag['data-partnumber']

            tag = soup.select_one('[name="keywords"]')

            if tag and tag.has_attr('content'):
                params['brandName'] = tag['content'].strip()

            tag = soup.select_one('[itemprop="availability"]')

            if tag and tag.has_attr('content'):
                
                params['oos'] = '0' if tag['content'] == 'in_stock' else '1'

            tag = soup.select_one('[rel="canonical"]')

            if tag and tag.has_attr('href'):
                params['href'] = tag['href']
            
            url = 'https://dapi.videoly.co/1/videos/0/235/?{}'.format(urlencode(params))

            res = self.downloader.download_api(url)

            if res:
                _json = json.loads(res, strict=False)
                source = url

                if 'items' in _json:
                    for item in _json['items']:
                        if 'videoId' in item:
                            v_url = 'https://www.youtube.com/embed/{}'.format(item['videoId'])
                            value.append(v_url)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value        

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # do not comment this -- this is needed
            # find other indicators if delivery must be -1
            availability = self.get_availability(soup)[1]
            tag = soup.find('script', text=re.compile('populateAndShowEstDeliveryTime')) or soup.find('script', text=re.compile('new InventoryStatusJS'))
            if tag:
                if 'populateAndShowEstDeliveryTime' in tag.text.strip():
                    source = str(tag)
                    text = tag.text.split('populateAndShowEstDeliveryTime')[-1]
                    text = text.split("'")[1]
                    if text:
                        value = text
                else:
                    store_id = None
                    catalog_id = None
                    item_id = None
                    store_rgx = re.search(r'storeId: \'([0-9]+)\'',tag.text.strip())
                    if store_rgx:
                        store_id = store_rgx.group(1)
                    catalog_rgx = re.search(r'catalogId: \'([0-9]+)\'',tag.text.strip())
                    if catalog_rgx:
                        catalog_id = catalog_rgx.group(1)
                    item_id_tag = soup.find('div', {'id':re.compile(r'availability-entitledItem_*')})
                    if item_id_tag and item_id_tag.has_attr('data-json') and item_id_tag['data-json']:
                        _json = json.loads(item_id_tag.get('data-json'))
                        for item in _json:
                            if 'productId' in item and item['inventoryStatus'] != 'Unavailable' and 'availabilityDateTime' not in item:
                                item_id = item['productId']
                                break

                    payload = {
                       'storeId': store_id,
                        'catalogId': catalog_id,
                        'langId': '-11',
                        'itemId': item_id,
                        'physicals': False,
                        'requesttype': 'ajax' 
                    }
                    api_url = 'https://www.karkkainen.com/GetInventoryStatusByIDViewV2'
                    res = self.downloader.extra_download(api_url,data=payload,timeout=randint(10,30))
                    if res:
                        _json = json.loads(res)
                        if _json and 'onlineInventory' in _json and 'deliveryTime' in _json['onlineInventory']:
                            source,value = api_url,_json['onlineInventory']['deliveryTime']
                        elif len(_json) >= 2 and _json[0]:
                            if 'x_customField3' in _json[0]:
                                deltime = _json[0]['x_customField3']
                                month = deltime.split('-')[1]
                                day = deltime.split('-')[2].split('T')[0]
                                year = deltime.split('-')[0]
                                if month and day and year:
                                    source = api_url
                                    value = 'Lähetetään arviolta ' + day + '.' + month + '.' + year
                            for keys in _json:
                                if keys['productId'] == '476261501': #color variant issue temporary fix
                                    availability = keys['inventoryStatus']
                        # if _json and 'onlineInventory' in _json.keys() and 'deliveryTime' in _json['onlineInventory']:
                        #     source,value = api_url,_json['onlineInventory']['deliveryTime']

            if availability in ['https://schema.org/OutOfStock','Unavailable']:
                value = Defaults.GENERIC_NOT_FOUND.value
                source = Defaults.GENERIC_NOT_FOUND.value
                    
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value


    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            rows = nth.select("li")

            for row in rows:
                try:
                    tag = row.select('span')
                    if tag and len(tag) >= 2:
                        key = tag[0]
                        data = tag[1]
                    
                    result[key.get_text().strip()] = data.get_text().strip()

                except:
                    continue

        return result
