import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class TeliaFiWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.telia.fi'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        #Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        return result

    #get price
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.mini-card__price')

            if tag:
                source = str(tag)
                content = tag.get_text()

                if content:
                    value = content

            else:
                tag = soup.select_one('#__NEXT_DATA__')
                if tag:
                    variant_ids = []
                    sku = None
                    _id = None
                    sku_tag = soup.select_one('.a2aue6-3.smallText--md')
                    if sku_tag:
                        sku = sku_tag.text.strip()
                    else:
                        link_tag =  soup.select_one('link[rel="canonical"]')  
                        if link_tag and link_tag.has_attr('href'):
                            href = str(link_tag['href'])
                            sku = href.split('/')[6]

                    _json = json.loads(tag.text.strip())
                    if _json and 'props' in _json.keys() and 'pageProps' in _json['props'] and 'product' in _json['props']['pageProps']\
                        and 'variants' in _json['props']['pageProps']['product'] and _json['props']['pageProps']['product']['variants']:
                        for item in _json['props']['pageProps']['product']['variants']:
                            if 'id' in item:
                                variant_ids.append(item['id'])
                            if 'urlSku' in item:    
                                if item['urlSku'].lower() == sku.lower():
                                    _id = item['id']
                            elif 'sku' in item:
                                if item['sku'].lower() == sku.lower():
                                    _id = item['id']
                    

                    if variant_ids and _id:
                        headers = {
                        "accept": "*/*",
                        "accept-encoding": "gzip, deflate",
                        "accept-language": "en-US,en;q=0.9",
                        "content-type": "application/json",
                        "origin": "https://www.telia.fi",
                        "referer": "https://www.telia.fi",
                        "sec-fetch-dest": "empty",
                        "sec-fetch-mode": "cors",
                        "sec-fetch-site": "cross-site",
                        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
                        }
                        payload = {
                            'variantIds': variant_ids
                        }
                        api_url = 'https://api.prod.b2c-new-online.com/api/v1/variant/pricing'
                        res = self.downloader.download(url = api_url, headers = headers, data = json.dumps(payload))
                        if res:
                            res_json = json.loads(res)
                            for item in res_json:
                                if item['id'] == _id:
                                    discountInfo = item['discountInfo']
                                    if discountInfo:
                                        source,value = api_url,str(item['price']['discountedPrice']['withVat'])
                                    else:
                                        source,value = api_url,str(item['price']['withVat'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    #get currency
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    #get availability
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # from what was observed, the site proritizes to display the variant thats
            # in stock, sequentially. so we need to get the first variant thats in stock
            # if none found, then get the first out of stock variant 
            tag = soup.select_one('div.availability div.availability__stock.availability__stock--yes')
            if tag:
                source, value = str(tag), tag['class'][1] # get the class instead
            else:
                tag = soup.select_one('div.availability div.availability__stock.availability__stock--no')
                if tag:
                    source, value = str(tag), tag['class'][1] # get the class instead

                else:
                    color_tag = soup.select_one('[data-e2e-selector="selected-variant1"]')
                    if color_tag:
                        color = color_tag.text.strip()
                        tag = soup.select_one(f'[data-e2e-color-option="{color}"]')
                        if tag and tag.has_attr('data-e2e-is-available'):
                            source,value = str(tag),tag['data-e2e-is-available']
                        else:
                            tag = soup.select_one('#__NEXT_DATA__')
                            if tag:
                                variant_ids = []

                                _id = None
                                _json = json.loads(tag.text.strip())
                                if _json and 'props' in _json.keys() and 'pageProps' in _json['props'] and 'product' in _json['props']['pageProps']\
                                    and 'variants' in _json['props']['pageProps']['product'] and _json['props']['pageProps']['product']['variants']:
                                    for item in _json['props']['pageProps']['product']['variants']:
                                        if 'id' in item:
                                            variant_ids.append(item['id'])
                                        # if item['urlSku'] == sku:
                                        #     _id = item['id']
                                    if 'id' in _json['props']['pageProps']['product']:
                                        _id = _json['props']['pageProps']['product']['id']
                                

                                if variant_ids:
                                    headers = {
                                    "accept": "*/*",
                                    "accept-encoding": "gzip, deflate",
                                    "accept-language": "en-US,en;q=0.9",
                                    "content-type": "application/json",
                                    "origin": "https://www.telia.fi",
                                    "referer": "https://www.telia.fi",
                                    "sec-fetch-dest": "empty",
                                    "sec-fetch-mode": "cors",
                                    "sec-fetch-site": "cross-site",
                                    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
                                    }
                                    payload = {
                                        'variantIds': variant_ids
                                    }
                                    api_url = 'https://api.prod.b2c-new-online.com/api/v1/variant/availability'
                                    res = self.downloader.download(url = api_url, headers = headers, data = json.dumps(payload))
                                    if res:
                                        res_json = json.loads(res)
                                        for item in res_json:
                                            if item['id'] == _id:
                                                source,value = api_url,str(item['state'])
            # if value == Defaults.GENERIC_NOT_FOUND.value:
            #     value = "outofstock"

               
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    #get ratings
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url = None
            brand = None
            title = None
            path = None
            url_tag = soup.find('link',{'rel':'canonical'})
            if url_tag and url_tag.has_attr('href'):
                url = url_tag.get('href')
                path = url_tag.get('href').split('.fi')[1]

            brand_tag = soup.find('div',{'class':'appliance__brand'}) or soup.select_one('.sc-7fdae21a-1.gSIVwp.ingress--lg') or soup.select_one('span.ingress--lg')
            if brand_tag:
                brand = brand_tag.get_text().strip()

            title_tag = soup.find('meta',{'name':'title'})
            if title_tag and title_tag.has_attr('content'):
                title = title_tag.get('content')
            else:
                title_tag = soup.select_one('span[class="headline--lg"]') or soup.select_one('span.heading--lg')
                if title_tag and brand:
                    title = brand+' '+title_tag.text.strip()
            
            if url and brand and title and path:
                req_url = 'https://api.witview.app/discovery/query'
                headers = {
                    "accept": "*/*",
                    "accept-encoding": "gzip, deflate",
                    "accept-language": "en-US,en;q=0.9",
                    "content-length": "431",
                    "content-type": "application/json",
                    "origin": "https://kauppa.telia.fi",
                    "referer": f"{url}",
                    "sec-fetch-dest": "empty",
                    "sec-fetch-mode": "cors",
                    "sec-fetch-site": "cross-site",
                    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
                }
                payload = {
                    "app":{
                        "av": "867a0a4",
                        "ci": "7c0359a1",
                        "doNotTrack": False,
                        "guid": "f0317e88-9c1d-4738-8982-fe5c3a45e0a8",
                        "locale": "fi",
                        "sv": "3",
                        "testGroups": [],
                        "uid": "eyJtaW1lVHlwZXNDb3VudCI6NCwicGx1Z2luc0NvdW50IjozLCJ3aWR0aCI6MTM2NiwiaGVpZ2h0Ijo2NTcsInBpeGVsRGVwdGgiOjI0fQ=="
                    },
                    "data":{
                            "brand": f"{brand}",
                            "locale": "fi",
                            "name": f"{title}",
                            "url":{
                                "path":f"{path}",
                        }
                    }
                }
                try:
                    res = self.downloader.download(url = req_url, headers = headers, data = json.dumps(payload))
                    if res:
                        json_data = json.loads(str(res))

                        # if json_data and 'item' in json_data.keys() and 'reviews' in json_data['item'] and 'totals' in json_data['item']['reviews'] and 'stars' in json_data['item']['reviews']['totals']:
                        if json_data and 'data' in json_data.keys() and 'ratingData' in json_data['data'] and 'total' in json_data['data']['ratingData'] and 'totalRating' in json_data['data']['ratingData']['total'] and 'totalCount' in json_data['data']['ratingData']['total']:
                            #score
                            score_value = str(json_data['data']['ratingData']['total']['totalRating'])
                            score_source = str(req_url)
                            #reviewers
                            reviews_value = str(json_data['data']['ratingData']['total']['totalCount'])
                            reviews_source = str(req_url)
                except:
                    pass
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    #get title
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta',{'name':'title'})
            if tag:
                source = str(tag)
                value = tag.get('content')
            else:
                tag = soup.select_one('.a2aue6-2.headline--lg') or soup.select_one('h1.a2aue6-0 span.headline--lg') or soup.select_one('span[class="headline--lg"]') or soup.select_one('h1.sc-6248ff78-0.eoprSI span.heading--lg')
                if tag:
                    source,value = str(tag), tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    #get brand
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta',{'name':'title'})
            if tag:
                source = str(tag)
                value = tag.get('content').split(' ')[0]
            else:
                tag = soup.select_one('.a2aue6-1.ingress--lg') or soup.select_one('.sc-7fdae21a-1.gSIVwp.ingress--lg') or soup.select_one('h1.sc-6248ff78-0.eoprSI span.sc-6248ff78-1.dDiiVx.ingress--lg')
                if tag:
                    source,value = str(tag),tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    #get description
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source_list = []
            value_list = []

            tag = soup.select_one('.appliance__description') or soup.select_one('.sc-18cu7t4-1.fldnvN') or soup.select_one('[class="sc-f4f24100-2 cYnSHq"]')\
                or soup.select_one('.sc-f8212851-2.kjuoDW') or soup.select_one('.sc-14af53e4-2.bclOBw') or soup.select_one('.sc-53b149ff-4') \
                or soup.select_one('.sc-6173584-1.bDfJUb') or soup.select_one('.sc-7b1f78e5-2.cGzYpU')
            if tag:
                source_list.append(str(tag))
                value_list.append(' '.join(tag.stripped_strings))            

            # else:
            tag_2 = soup.select_one('[data-hide="accordion-0"] .accordion__content') or soup.select_one('.accordion__content-technical-details') #or\
                #soup.find('meta',{'name':'description'}) 
            

            if tag_2:
            
                source_list.append(str(tag_2))
                if tag_2.has_attr('content'):
                    value_list.append(tag_2['content'])
                else:
                    value_list.append(' '.join(tag_2.stripped_strings))

            if source_list and value_list:
                source, value = ' '.join(source_list), ' '.join(value_list)    

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    #get specs
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('div',{'class':'feature-class-table'})
            tag_2 = soup.select('[data-hide="accordion-1"] li') or soup.select('[data-hide="accordion-2"] li')
            if tag:
                source = str(tag)
                value = self._get_initial_specs(tag.select('div'))
            elif tag_2:
                source = str(tag)
                value = self.__build_specs(tag_2)
            else:
                tag = soup.select('.sc-1xq76qh-1.tCeTG p') or soup.select('.sc-8aa8ed07-1.fnIFpg p')
                if len(tag):
                    source = str(tag)
                    for spec in tag:
                        sp = spec.get_text(strip=True)
                        if ':' in sp:
                            key = sp.split(':')[0]
                            val = sp.split(':')[1]
                            if key and val:
                                value[key.strip()]=val.strip()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    #get image url
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select('.image-gallery__thumbs-container.jsHide.jsGalleryThumbs')
            if len(tag):
                image = tag[0].select('img')
                source = str(image)
                for img in image:
                    if img.get('src'):
                        src = 'https://kauppa.telia.fi' + img.get('src')
                        value.append(src)
            else:
                tag = soup.select('.sc-1gfuoha-0.bjPpCy a') or soup.select('.sc-319f2a90-0.jnjStO a') or soup.select('a[aria-label="Tuotekuva"]')
                if len(tag):
                    source = str(tag)
                    base = 'https:'
                    for img in tag:
                        if img.has_attr('href'):
                            value.append(base + img.get('href'))
                if not tag: 
                    temp_tag = soup.select_one('div.image-gallery')
                    if temp_tag:
                        tag = temp_tag.select('img.jsGalleryImage')
                        if tag:
                            source = str(tag)
                            for img in tag:
                                if img.get('data-src'):
                                    src = 'https://kauppa.telia.fi/' + img.get('data-src')
                                    value.append(src)
            if not value:
                tag = soup.select_one('div.sc-9137dd1b-0.EoocC')
                if tag:
                    imgs = tag.select('img')
                    for img in imgs:
                        if img.has_attr('src'):
                            value.append(f"https:{img['src']}")
                    source = str(imgs)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #get video url
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag_video_id = soup.select_one('#videoly-product-id')
            tag_title = soup.select_one('#videoly-product-title')
            tag_brand = soup.select_one('.appliance__brand')
            tag_href = soup.select_one('[rel="canonical"]')
            

            if tag_video_id and tag_title and tag_brand and tag_href:
                params = {
                    'href': tag_href['href'] if tag_href.has_attr('href') else '',
                    'productId': ' '.join(tag_video_id.stripped_strings),
                    'productTitle': ' '.join(tag_title.stripped_strings),
                    'brandName': ' '.join(tag_brand.stripped_strings),
                    'hn': 'kauppa.telia.fi'
                    
                }
                api_url='https://dapi.videoly.co/1/videos/0/247/?{param}'.format(
                    param = '&'.join(['{}={}'.format(k,v) for k,v in params.items()])
                    )
                
                data = self.downloader.download(api_url)
                if data:
                    _json = json.loads(data)
                    if 'items' in _json:
                        source = str(api_url)
                        value = ['https://www.youtube.com/embed/{vid_id}'.format(
                            vid_id = i['videoId']) for i in _json['items']]


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}
        head_key = ''
        key = None
        value = None
        for tag in tags:
            if 'feature-group-tablerow' in tag.get('class'):
                head_key = ' '.join(tag.stripped_strings)
                result[f'{head_key}'] = {}
            if 'attrib' in tag.get('class'):
                key = ' '.join(tag.stripped_strings)
            if 'attrib-value' in tag.get('class'):
                value = ' '.join(tag.stripped_strings)
            
                result[f'{head_key}'][f'{key}'] = f'{value}' 
            
        return result


    def __build_specs(self, tags):
        specs = {}
        if tags:
            for tag in tags:
                if ':' in tag.text:
                    k, v = ' '.join(tag.stripped_strings).split(':', 1)
                    specs[k.strip()] = v.strip()

        return specs
                

