import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class KomplettNoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.komplett.no'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications - No Specs
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta',{'itemprop':'price'})
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag.get('content')

            else:
                source, _json = self.get_json(soup)
                if _json and 'offers' in _json and 'price' in _json['offers'] and _json['offers']['price'] != '0':
                    value = str(_json['offers']['price'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, _json = self.get_json(soup)

            if 'offers' in _json:
                if 'priceCurrency' in _json['offers']:
                    value = _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, _json = self.get_json(soup)

            if 'offers' in _json:
                if 'availability' in _json['offers']:
                    value = _json['offers']['availability']
                    
            if value and 'outofstock' in value.lower():
                stock_status = soup.select_one('.stockstatus-stock-details')
                if stock_status:
                    source, value = str(stock_status),stock_status.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, _json = self.get_json(soup)

            if 'aggregateRating' in _json:
                if 'reviewCount' in _json['aggregateRating']:
                    reviews_value = _json['aggregateRating']['reviewCount']
                    reviews_source = source
                if 'ratingValue' in _json['aggregateRating']:
                    score_value = _json['aggregateRating']['ratingValue']
                    score_source = source                    

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("h1", {"itemprop":"name"})

            if tag:
                source, value = str(tag), tag.get_text()
            else:
                source, _json = self.get_json(soup)

                if 'name' in _json:
                    value = _json['name']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, _json = self.get_json(soup)

            if 'brand' in _json:
                if 'name' in _json['brand']:
                    value = _json['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            all_src = []
            description = ''

            tag = soup.find('div',{'class':'product-responsive-info col-xs-12 col-sm-10 col-md-8 col-lg-6 col-sm-offset-1 col-md-offset-2 col-lg-offset-3 product-sections-left-content'})
            if tag:
                all_src.append(str(tag))
                description = description + ' ' + ' '.join(tag.stripped_strings)

            if description:
                source = " + ".join(all_src)
                value = description

            if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value]:
                tag = soup.select_one('.product-section-content')

                if tag:
                    source, description = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('.swiper-wrapper button img')
            url_pref = 'https://%s' % self.__class__.WEBSITE
            value = [url_pref+i.get('src') for i in tags if i.has_attr('src')] if tags else []
            source = str(tags) if tags else source

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
    
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tags = soup.select('.product-section-content iframe')
            source = str(tags)
            for tag in tags:
                if tag.has_attr('src'):
                    value.append(tag.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.stockstatus-stock-details')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value   

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag_key = soup.select('.responsive-table.fixed-layout th')
            tag_value = soup.select('.responsive-table.fixed-layout td')

            if tag_key and tag_value:
                source, value = self._build_spec(tag_key, tag_value)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value


    def get_json(self, soup):
        tag = soup.select_one('[type="application/ld+json"]')
        if tag:
            return str(tag), json.loads(tag.get_text().strip())

    @staticmethod
    def _build_spec(key_tag, val_tag):
        value = {k.get_text().strip():v.get_text().strip() for k,v in zip(key_tag, val_tag)}
        return str(key_tag+val_tag), value            