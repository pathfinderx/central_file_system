import json
import logging
import re

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class FjellsportNoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.fjellsport.no'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.nosto_product > span.price') or soup.select_one('.a2.an.ar')

            if tag:
                source = str(tag)
                content = tag.get_text()
            
                if content:
                    value = content
            else:
                tag = soup.find('script',text=re.compile('Offer","availability"'))
                if tag:
                    _data = json.loads(tag.get_text().strip())
                    if _data:
                        source = str(tag)
                        value = str(_data[0]['offers'][0]['price'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            tag = soup.select_one('.nosto_product > span.price_currency_code')

            if tag:           
                source = str(tag)
                content = tag.get_text().strip()

                if content:
                    value = content
                else:
                    value = 'NOK'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.find('script',text=re.compile('Offer","availability"'))
            if tag:
                _data = json.loads(tag.get_text().strip())
                if _data:
                    source = str(tag)
                    value = _data[0]['offers'][0]['availability']
                    for _dta in _data:
                        if 'offers' in _dta and _dta['offers'][0]['availability'] == 'https://schema.org/LimitedAvailability':
                            source = str(tag)
                            value = _dta['offers'][0]['availability']

            else:

                tag = soup.select_one('.nosto_product > span.availability')
                if tag:
                    source = str(tag)
                    content = tag.get_text().strip()

                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
        try:
            _data = None
            tag = soup.find('script',text=re.compile('Offer","availability"'))
            if tag:
                _data = json.loads(tag.get_text().strip())

            reviews_tag = soup.select_one('.nosto_product > span.review_count')
            
            if reviews_tag:
                reviews_source = str(reviews_tag)
                content = reviews_tag.get_text().strip()
                if content:
                    reviews_value = content
            elif _data and 'aggregateRating' in _data[0]:
                reviews_source = str(tag)
                reviews_value = _data[0]['aggregateRating']['reviewCount']

            scores_tag = soup.select_one('.nosto_product > span.rating_value')
            
            if scores_tag:
                score_source = str(scores_tag)
                content = scores_tag.get_text().strip()
                if content:
                    score_value = content
            elif _data and 'aggregateRating' in _data[0]:
                score_source = str(tag)
                score_value = _data[0]['aggregateRating']['ratingValue']

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value
        # Ratings is not displayed in retailers website

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.nosto_product > span.name') or soup.select_one('h1.bf.bh.i6') or soup.select_one('h1.bf.bh.i5') 
            if tag:
                source = str(tag)
                content = tag.get_text().strip()
            
                if content:
                    value = content
            else:
                tag = soup.find('script',text=re.compile('Offer","availability"'))
                if tag:
                    _data = json.loads(tag.get_text().strip())
                    if _data:
                        source = str(tag)
                        value = str(_data[0]['name'])
           
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.brand')

            if tag:
                source = str(tag)
                value = tag.get_text().strip()
            else:
                tag = soup.find('script',text=re.compile('Offer","availability"'))
                if tag:
                    _data = json.loads(tag.get_text().strip())
                    if _data:
                        source,value = str(tag),_data[0]['manufacturer']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        desc = ''
        desc_list = []
        try:
            # bullet_desc = soup.select_one('ul.i5.e4.dv.f7.mb')
            # if bullet_desc:
            #     desc_list.append(' '.join(bullet_desc.stripped_strings))

            # bullet description
            bullet_desc = soup.select('[class="mb h8"]')
            if (bullet_desc):
                for x in bullet_desc:
                    desc_list.append(' '.join(x.stripped_strings))
                
            tag = soup.select_one('span.description')
            if tag:
                source = str(tag)
                tag = tag.get_text().strip() 
                tag = BeautifulSoup(tag, 'lxml')
                desc =  tag.get_text().strip().replace('\n', ' ')
                content = desc
                if content:
                    desc_list.append(content)
            else:
                tag = soup.find('script',text=re.compile('Offer","availability"'))
                if tag:
                    _data = json.loads(tag.get_text().strip())
                    if _data:
                        source = str(tag)
                        desc_soup = BeautifulSoup(_data[0]['description'],'lxml')
                        desc_list.append(' '.join(desc_soup.stripped_strings))

            if desc_list:
                value = ' '.join(desc_list)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('table.data-table tr')
            
            if tags:
                source = str(tags)
                content = self._get_initial_specs(tags)

                if content:
                    value = content
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            tag = soup.select('div.photozoom-product-images a')

            if tag:
                source = str(tag)
                value = [img['href'] for img in tag]
            else:
                tag = soup.find('script',text=re.compile('Offer","availability"'))
                if tag:
                    _data = json.loads(tag.get_text().strip())
                    if _data:
                        source= str(tag)
                        for item in _data[0]['image']:
                            if 'preset=medium' in item:
                                value.append(item)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        try:
            tag = self._extract_video_urls(soup)
            if tag:
                source = str(tag)
                res = self.downloader.download(tag)
                if res is not None:
                    videos = re.findall('\W*videoId[^:]*:\D(.\w*.\w*)"', res)
                    value = ['https://www.youtube.com/embed/'+vid for vid in videos]
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}
        for i in tags:
            key = i.select_one('th.label')
            value = i.select_one('td.data')
            result[key.get_text() if key else ''] = value.get_text() if value else ''
        return result

    @staticmethod
    def _extract_video_urls(soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
        title = soup.select_one('.nosto_product > span.name')
        title = title.get_text().replace(" ", "%20")
        brand = soup.select_one('span.brand')
        brand = brand.get_text().strip() if brand else ''
        sku = soup.select_one('span.sku')
        sku = sku.get_text().strip() if sku else ''
        pid = soup.select_one('a.photozoom-product-images__image')
        pid = pid['data-productids'] if pid else ''
        pVariation = soup.find('input', {'name':'product'})
        pVariation = pVariation['value'] if pVariation else ''
        url = 'https://dapi.videoly.co/1/videos/0/387/?brandName=%s&SKU=%s&productId=%s-id%s'\
                '&productTitle=%s&productVariation=%s&oos=0&maxItems=15&ytwv=undefined&_b=Chrome'\
                '&_bv=76.0&p=1&_w=1903&_h=979&_pl=nb&_cl=no&callback=_vdly13ed0d2032&hn=www.fjellsport.no' \
                % (brand, sku, brand, pid, title, pVariation)
        vid_url = url.replace('+', '%2B')
        
        return vid_url