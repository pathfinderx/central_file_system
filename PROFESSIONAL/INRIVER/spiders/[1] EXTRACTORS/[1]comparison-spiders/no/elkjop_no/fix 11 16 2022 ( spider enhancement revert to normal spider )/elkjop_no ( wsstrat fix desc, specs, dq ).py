import json
import logging
import re
from urllib.parse import urlencode
from copy import deepcopy
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from urllib.parse import urlencode, quote

class ElkjopNoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.elkjop.no'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        _data = {}
        _data = self._get_api_data(soup) 

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, _data)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup, _data)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, _data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup, _data)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, result)

        return result

    def get_price(self, soup, _data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #### WILL BE HANDLED NOW IN ET #####
            # # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            # tag = soup.select_one(".product-price-text")
            # if tag and "ikke tilgjengelig" in tag.get_text().lower():
            #     value = '0.0'
            # # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            # else:   
            if not _data == {}:
                source = str(_data['data']['product'])
                if _data['data']['product']['availableForCollectAtStoreCount'] > 0: # price is not visible when stocks are Zero
                    if _data['data']['product']['currentPricing']['price'] != None:
                        value = str(_data['data']['product']['currentPricing']['price']['value'])
                    else:
                        if 'data' in _data and 'product' and _data['data'] and 'offers' in _data['data']['product']:
                            offers = _data['data']['product']['offers']
                            if offers:
                                if 'offerPrice' in offers[0] and 'valueExVat' in offers[0]['offerPrice']:
                                    value = str(_data['data']['product']['offers'][0]['offerPrice']['valueExVat'])
                elif value == 'NOT_FOUND':
                    buy_box = soup.select_one('.buy-box__top.ng-star-inserted') # price box in PDP
                    if buy_box and _data['data']['product'] and 'activePricing' in _data['data']['product'] and 'value' in _data['data']['product']['activePricing']:
                        value = str(_data['data']['product']['activePricing']['value'])
                else:
                    if _data and 'data' in _data and 'product' in _data['data']:
                        product = _data['data']['product']
                        if 'availableForCollectAtStoreCount' in product and 'buyableOnline' in product \
                            and 'buyableInternet' in product and 'buyableCollectAtStore' in product \
                            and 'hasMarketplaceOffers' in product:
                                if product['availableForCollectAtStoreCount'] > 0 or product['buyableOnline'] \
                                    or product['buyableInternet'] or product['buyableCollectAtStore'] or \
                                        product['hasMarketplaceOffers']:

                                    if 'data' in _data and 'product' and _data['data'] and 'activePricing' in _data['data']['product']:
                                        value = str(_data['data']['product']['activePricing']['value'])
                if value == 'None':
                    if _data['data']['product'] and 'offers' in _data['data']['product'] and _data['data']['product']['offers'] and 'prices' in _data['data']['product']['offers'][0] and 'activePrice' in _data['data']['product']['offers'][0]['prices']:
                        value = str(_data['data']['product']['offers'][0]['prices']['activePrice'])
                    else:
                        value = Defaults.GENERIC_NOT_FOUND.value
            else:
                first_tag_content = None
                tag = soup.find('meta', {'itemprop': 'price'})

                if tag and tag.has_attr('content'):
                    source = str(tag)
                    content = tag["content"]

                    if content:
                        value = content
                        first_tag_content = content
                        
                        
                        
                ### FOR HANDLING HIDDEN PRICES ###
                if not soup.select_one('.product-price-container span'): 
                    value = Defaults.GENERIC_NOT_FOUND.value
                ### FOR HANDLING HIDDEN PRICES ###

                #Temporary fix here for different price tag
                if first_tag_content and first_tag_content == '0':
                    parent_tag = tag.find_parent('div',{'class':'product-price'})
                    if parent_tag:
                        value = parent_tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup, _data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if not _data == {}:
                source = str(_data['data']['product'])
                if _data['data']['product']['currentPricing']['price'] != None:
                    value = str(_data['data']['product']['currentPricing']['price']['currency'])
            else:
                tag = soup.find('meta', {'itemprop': 'priceCurrency'})

                if tag and tag.has_attr('content'):
                    source = str(tag)
                    content = tag["content"]

                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup, _data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("link", {"itemprop": "availability"})

            if tag and tag.has_attr("href"):
                source = str(tag)
                availability = tag.get("href")

                if availability:
                    value = availability
                
            else:
                tag = soup.select_one('.store-status') or soup.select_one('.stock-info')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()
                    
                if value == 'https://schema.org/InStoreOnly': # For instoreonly availability issue
                    tag = soup.select_one('span.el-button-text span.product-price-text')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()

            if value == 'NOT_FOUND':
                if _data['data']['product']:
                    source = str(_data)
                    if _data['data']['product'] != None:
                        value = str(_data['data']['product']['buyableOnline'])
                else:
                    _json = None
                    _tag = soup.find('script', text= re.compile(r'"@type": "Product"'), type="application/ld+json")
                    if _tag:
                        _json = json.loads(_tag.get_text().strip())
                        if 'offers' in _json.keys():
                            source = str(_json['offers'])
                            value = _json['offers']['availability']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews
            tag = soup.find("meta", {"itemprop":"reviewCount"})

            if tag and tag.has_attr('content'):
                reviews_source = str(tag)
                content = tag["content"]

                if content:
                    reviews_value = content

            # Ratings
            tag = soup.find("meta", {"itemprop":"ratingValue"})

            if tag and tag.has_attr('content'):
                score_source = str(tag)
                content = tag["content"]

                if content:
                    score_value = content
            
            if score_value == 'NOT_FOUND' and reviews_value == 'NOT_FOUND':
                _json = None
                _tag = soup.find('script', text= re.compile(r'"@type": "Product"'), type="application/ld+json")
                if _tag:
                    _json = json.loads(_tag.get_text(strip=True))
                    if 'aggregateRating' in _json.keys():
                        #Rating
                        score_value = _json['aggregateRating']['ratingValue']
                        #Review
                        reviews_value = _json['aggregateRating']['reviewCount']
                    score_source = reviews_source = str(_json['aggregateRating'])
                    
            if score_value == 'NOT_FOUND' and reviews_value == 'NOT_FOUND':
                _data = self._get_api_desc(soup) #Rating/Reviews Available here
                if not _data == {}:
                    if _data['data']['product'] != None:
                        #Rating
                        score_source = str(_data)
                        score_value = str(_data['data']['product']['averageRating'])
                        #Review
                        reviews_source = str(_data)
                        reviews_value = str(_data['data']['product']['numberOfReviews'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            _tag = soup.find('script', text= re.compile(r'"@type": "Product"'), type="application/ld+json")
            if _tag:
                _json = json.loads(_tag.get_text().strip())
                
            tag = soup.find('meta', {'itemprop': 'name'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag["content"]
            
            if _json is not None:
                source = str(_tag)
                value = _json['name']
            else:
                _data = self._get_api_desc(soup) #Title Available here
                if not _data == {}:
                    if _data['data']['product'] != None:
                        source = str(_data)
                        value = _data['data']['product']['title']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
            # parent_tag = soup.find('span', {'itemprop': 'brand'})

            # if parent_tag:
            #     tag = parent_tag.find('meta', {'itemprop':'name'})
            #     if tag and tag.has_attr('content'):
            #         source = str(tag)
            #         value = tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pre_desc = ''
            short_desc = soup.select_one("p.short-description")

            if short_desc:
                source = str(short_desc)
                short_specs = soup.select("ul.specs li")

                if short_specs:
                    pre_desc = short_desc.get_text().strip()

                    for spec in short_specs:
                        pre_desc += ("\n>" + spec.get_text().strip())

                    pre_desc += "\n\n"

            sku = None

            tag = soup.find("meta", {"itemprop":"sku"})

            if tag and tag.has_attr('content'):
                sku = tag.get("content")

            if tag:
                desc_url = "https://www.elkjop.no/INTERSHOP/web/WFS/store-elkjop-Site/no_NO/-/NOK/" \
                            "CC_AjaxProductTab-Get?ProductSKU=" + sku \
                            + "&TemplateName=CC_ProductMoreInformationTab&PageletUUID="
                source += ' + ' + desc_url
                desc_req = self.downloader.extra_download(desc_url)

                if desc_req:
                    desc_soup = BeautifulSoup(desc_req, 'lxml')
                    description = " ".join(desc_soup.stripped_strings)

                if not description:
                    tag = soup.find("meta", {"property": "og:description"})

                    if tag and tag.has_attr('content'):
                        source += ' + ' + str(tag)
                        description = tag.get("content")

                if pre_desc and description:
                    value = pre_desc + description.split('var ccs_cc_args')[0] #Remove the html tags that being scrape

                elif pre_desc:
                    value = pre_desc

                elif description:
                    value = description

            tag = soup.select_one('.article-page.main-content.col.any-1-1')

            if tag:
                source += ' + ' + str(tag)
                [i.decompose() for i in tag.select('script')]
                [i.decompose() for i in tag.select('style')]
                value += ' ' + ' '.join(tag.stripped_strings)
            
            else:
                try: 
                    temp_desc = []
                    _data = self._get_api_desc(soup)
                    if not _data == {}:
                        source = str(_data['data']['product'])
                        if _data['data']['product'] != None:
                            _data = _data['data']['product']
                            if 'shortDescription' in _data.keys():
                                temp_desc.append(_data['shortDescription'])
                            if 'longDescription' in _data.keys():
                                longDesc = _data['longDescription']
                                desc_tag = BeautifulSoup(longDesc,'lxml')
                                temp_desc.append(' '.join(desc_tag.stripped_strings))
                                #Removing Html Tags
                                # if '<br><br><strong>' and '</strong><br>' in longDesc:
                                #     longDesc = longDesc.replace('<br><br><strong>',' ').replace('</strong><br>',' ')
                                #     if longDesc:
                                #         temp_desc.append(longDesc)
                                # elif '<br /><br /><strong>' and '</strong>' and '<br />' in longDesc:
                                #     longDesc = longDesc.replace('<br /><br /><strong>','').replace('</strong>','').replace('<br />-',' -')
                                #     if longDesc:
                                #         temp_desc.append(longDesc)
                                # elif '</strong>' and '<strong>' and '<br /><br /><strong>' and '</strong><br />' and '<br />' in longDesc:
                                #     longDesc = longDesc.replace('</strong>','').replace('<strong>','').replace('<br /><br /><strong>','').replace('<br />','')
                                #     if longDesc:
                                #         temp_desc.append(longDesc)
                                # else:
                                #     temp_desc.append(longDesc)
                        if temp_desc:
                            value = ' '.join(temp_desc)

                except Exception as e:
                    print('Error at wsstrat -> get_description() -> api description extraction -> with message: ', e)

            long_description_tag = soup.select_one('.product-features__long-description')
            if (long_description_tag and 
                value in [Defaults.GENERIC_NOT_FOUND.value]):
                value += ' ' + ' '.join(long_description_tag.stripped_strings)

                if value:
                    source += ' ' + str(long_description_tag) if source else str(long_description_tag)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup, _data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            pass
            # sku = None

            # tag = soup.find("meta", {"itemprop":"sku"})

            # if tag and tag.has_attr('content'):
            #     sku = tag.get("content")

            # if tag:
            #     spec_url = "https://www.elkjop.no/INTERSHOP/web/WFS/store-elkjop-Site/no_NO/-/NOK/" \
            #                 "CC_AjaxProductTab-Get?ProductSKU=" + sku \
            #                 + "&TemplateName=CC_ProductSpecificationTab&PageletUUID="
            #     source = spec_url
            #     spec_req = self.downloader.extra_download(spec_url)

            #     if spec_req:
            #         spec_soup = BeautifulSoup(spec_req, 'lxml')
            #         tag = spec_soup.select("table")
            #         if tag:                   
            #             value = self._get_initial_specs(tag)
            # else:
            #     _data = self._get_api_spec(soup)
            #     if not _data == {}:
            #         source = str(_data['data']['product']['specification'])
            #         if _data['data']['product'] != None:
            #             _data = _data['data']['product']['specification']['attributeGroups']
            #             for data in _data:
            #                 if 'attributes' in data.keys():
            #                     specification = data['attributes']
            #                     for sp in specification:
            #                         if 'name' and 'value' in sp:
            #                             key = sp['name']
            #                             val = sp['value']
            #                             if key and val:
            #                                 value[key] = val

            # soup.select_one('.pdp__specs.product-specification > .product-attributes')

            # for x in soup.select_one('.pdp__specs.product-specification').select('table > tr'):
            #     for y in x.select('td'):
            #         print(y.attrs.get('class'))


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select("div.mediaAsset div img")

            if tag:
                source = str(tag)
                for img in tag:
                    if img.has_attr('src'):
                        image_url = img.get('src')
                        if "elkjop.no" not in image_url:
                            image_url = "https://www.elkjop.no" + image_url
                        value.append(image_url.replace('prod_all4one', 'fullsize'))
                    if img.has_attr('data-lazy'):
                        image_url = img.get('data-lazy')
                        if "elkjop.no" not in image_url:
                            image_url = "https://www.elkjop.no" + image_url
                        value.append(image_url.replace('prod_all4one', 'fullsize'))
            else:
                tag = soup.select_one('div.product-title-wrap p.sku')
                if tag:
                    productId = tag.get('data-product-sku')
                    imgHost = "http://tubby.scene7.com/is/image/"
                    req_url = "http://tubby.scene7.com/is/image//tubby/" + productId + "_set?req=imageset"
                    set_img = self.downloader.extra_download(req_url).text.strip().replace(",",";").split(";")
                    set_img = list(set(set_img))
                    source = req_url

                    for img in set_img:
                        if 'too many connections' in img.lower():
                            continue

                        if len(img) > 0 and "advanced" not in img and "video" not in img:
                            value.append(imgHost + img.replace('prod_all4one', 'fullsize'))
                if not tag:
                    img_carousel = soup.find('div', class_='swiper-wrapper')
                    if img_carousel:
                        source = str(img_carousel)
                        img_containers = img_carousel.find_all('div', class_ ='swiper-slide ng-star-inserted')
                        for img in img_containers:
                            img = img.img['src']
                            value.append(img)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            _sku = None
            _title = None

            tag = soup.find("meta", {"itemprop":"sku"})

            if tag and tag.has_attr('content'):
                _sku = tag.get("content")

            tag = soup.find('meta', {'itemprop': 'name'})

            if tag and tag.has_attr('content'):
                _title = tag["content"]
            

            if _sku and _title:
                url = "https://dapi.videoly.co/1/videos/0/411/?SKU=%s" \
                        "&productTitle=%s" \
                        "&callback=_vdly7c9e3f86d7&hn=www.elkjop.no" \
                        % (_sku, _title)

                source = url
                res = self.downloader.extra_download(url)

                if res:
                    videos = re.findall(r'\W*videoId[^:]*:\D(.\w*.\w*)"', res)

                    for video in videos:
                        video = "https://www.youtube.com/embed/%s" % video
                        value.append(video)

            if not value:
                tags = soup.select('.article-text.article-free-html source[type="video/mp4"]')

                if tags:
                    source = str(source)
                    value = ['https://www.elkjop.no{}'.format(i['src']) for i in tags if i.has_attr('src')]    

            if not value:
                tags = soup.find_all('video',{'class':'el-videoplayer'})
                if tags:
                    source = str(tag)
                    value = ['https://www.elkjop.no{}'.format(i['src']) for i in tags if i.has_attr('src')] 
            if not value:
                ean = soup.select_one('meta[itemprop="gtin8"]')
                if ean and ean.has_attr('content'):
                    _ean = ean['content']
                href = soup.select_one('link[itemprop="url"]')
                if href and href.has_attr('href'):
                    _href = href['href']
                brand = soup.select_one('span[itemprop="brand"] meta')
                if brand and brand.has_attr('content'):
                    _brand = brand['content']
                    
                    if _ean and _href and _brand and _title and _sku:
                        response = self.downloader.api_video(_ean,_href,_brand,_title,_sku)
                        if response:
                            videos = re.findall(r'\W*videoId[^:]*:\D(.\w*.\w*)"', response)
                            for video in videos:
                                video = "https://www.youtube.com/embed/%s" % video
                                value.append(video)
            if not value:
                vid = soup.select('.video-player')
                for video in vid:
                    if video.has_attr('src'):
                        value.append(video['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup, _result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #Fixes as per https://www.elkjop.no/product/mobil-og-gps/gps-til-bil-og-motorsykkel/21759/garmin-drive-52-mt-s-gps
            tag = soup.select_one('.el-button-text .product-price-text')
            if tag:
                if 'klikk & hent' not in ' '.join(tag.stripped_strings).lower():
                    
                    tag = soup.select_one('.stock-info .items-in-stock.align-left') or \
                        soup.select_one('#stock-info')
                        
                    if tag:
                        value = ' '.join(tag.stripped_strings)
                        source = str(tag)

                    if not value:
                        tag = soup.select_one('.store-status.align-left')

                        if tag:
                            value = ' '.join(tag.stripped_strings)
                            source = str(tag)
            else:
                if _result['availability']['value'].lower() in ['klikk & hent','false','soldout']:
                    if _result['price']['value'] == 'NOT_FOUND':
                        value = 'Beklager, dette produktet finnes ikke lengre i vårt sortiment.'
                    else:
                        value = 'Dette produktet er ikke tilgjengelig på nettlager'

            # Delivery Extraction from 3 apis
            script_tag = soup.select('script[type="text/javascript"]') 
            articleNumber = None

            if (script_tag): 
                for x in script_tag:  # extract article number
                    rgx = re.search(r'productId\":\"([\w\W]+?)\"', x.get_text())
                    if (rgx):
                        articleNumber = rgx.group(1) # add breaker

            sha_product_delivery_indications = '65d7c26b01f3064bef4615c1c52d4898f9b441e67ed63f88e539bd34ae2e6127'
            sha_product_dynamic_details_url = '57c045574d87df751afcdf5be14012fa2605d00df934008aacb5c14d8cd4a3e8'
            sha_localizations = '0f11947341c38907926d0125871b3770f132d9d8c902237ef50ee1db5c7ad51e'

            # search in network: 'getProductDeliveryIndications' (https://prnt.sc/C0rnWHYzEmXi)
            product_delivery_indications_request_url = f'https://www.elkjop.no/cxorchestrator/no/api?getProductDeliveryIndications&appMode=b2c&user=anonymous& operationName=getProductDeliveryIndications&variables=%7B%22destination%22%3A%7B%22postalCode%22%3A%221473%22%2C%22countryCode%22%3A%22NO%22%2C%22storeId%22%3A%221017%22%7D%2C%22articleNumber%22%3A%22{articleNumber}%22%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22{sha_product_delivery_indications}%22%7D%7D'
                          
            # search in network: 'getProductWithDynamicDetails' (https://prnt.sc/YK4tL2FyU9vG)
            product_dynamic_details_url = f'https://www.elkjop.no/cxorchestrator/no/api?getProductWithDynamicDetails&appMode=b2c&user=anonymous&operationName=getProductWithDynamicDetails&variables=%7B%22articleNumber%22%3A%22{articleNumber}%22%2C%22withCustomerSpecificPrices%22%3Afalse%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22{sha_product_dynamic_details_url}%22%7D%7D'

            # search in network 'localization' (https://prnt.sc/WDClDf4-NyTE) (https://prnt.sc/XcTbQnm85Afv) (https://prnt.sc/gJ91CwLvSl6S)
            res_localizations = f'https://www.elkjop.no/cxorchestrator/no/api?localizations&appMode=b2c&user=anonymous&operationName=localizations&variables=%7B%22lang%22%3A%22no%22%2C%22appMode%22%3A%22b2c%22%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22{sha_localizations}%22%7D%7D'

            res_delivery_indicators = self.downloader.download_from_api(product_delivery_indications_request_url)
            res_dynamic_details = self.downloader.download_from_api(product_dynamic_details_url)
            res_localizations = self.downloader.download_from_api(res_localizations)

            if (res_delivery_indicators and res_dynamic_details and res_localizations):
                _json_dynamic_details = json.loads(res_dynamic_details)
                _json_localizations = json.loads(res_localizations)
                
                if (_json_dynamic_details and
                    'data' in _json_dynamic_details and
                    'product' in _json_dynamic_details['data'] and
                    'itemAvailability' in _json_dynamic_details['data']['product']): 
                    
                    availability_state = _json_dynamic_details['data']['product']['itemAvailability']

                    if (availability_state in ['InStock']): # for 'InStock' availabilility State
                        _json_delivery_indicators = json.loads(res_delivery_indicators)

                        time_range = _json_delivery_indicators.get('data').get('product').get('deliveryIndication').get('deliveryTimeRange')
                        delivery_format_source = _json_localizations.get('data').get('localizations').get('json').get('product.detail.buyBox.availability.deliveryDaysRange')

                        if (time_range and delivery_format_source):
                            rgx = re.sub(r'{[\s\S]+}', time_range, delivery_format_source)
                            delivery = rgx if rgx else Defaults.GENERIC_NOT_FOUND.value

                    elif (availability_state in ['SoldOut']):  # for 'InStock' availabilility State
                        delivery_format_source = _json_localizations.get('data').get('localizations').get('json').get('product.general.unavailableOnline')
                        
                        delivery = delivery_format_source if delivery_format_source else Defaults.GENERIC_NOT_FOUND.value

                    elif (availability_state in ['Discontinued']): # for 'Discontinued' availabilility State
                        delivery_format_source = _json_localizations.get('data').get('localizations').get('json').get('product.detail.buybox.status.discontinued')
                        
                        delivery = delivery_format_source if delivery_format_source else Defaults.GENERIC_NOT_FOUND.value

                    # you can find another indicators here: _json_dynamic_details['data']['product']['itemAvailability'] if not written above

            ######################################################################################################
            # KEY SEQUENCE for javascript rendered DQ (https://cdn-eu.dynamicyield.com/api/9877182/api_dynamic.js)
            # 'getProductWithDynamicDetails' > json['data']['product']['itemAvailability']
                # 3 states:
                    # if value is 'InStock' -> DQ: Levering fra 2 dag(er)
                        # get api from 'getProductDeliveryIndications' > json['data']['product']['deliveryIndication']['deliveryTimeRange'] as timerange
                        # get api from 'localization' > json['data']['localizations']['json']['product.detail.buyBox.availability.deliveryDaysRange'] > and substitute daysRange with timerange 

                    # if 'SoldOut' -> DQ: Dette produktet er ikke tilgjengelig på nettlager
                        # get api from 'localization' > json['data']['localizations']['json']['product.general.unavailableOnline']

                    # 'Discontinued' -> DQ: Beklager, dette produktet finnes ikke lengre i vårt sortiment.
                        # get api from 'localization' > json['data']['localizations']['json']['product.detail.buybox.status.discontinued']

                        # same state: Dette produktet er ikke tilgjengelig for privatkunder. Gå til bedriftsiden vår for å kjøpe som bedriftskunde.

                    # 'InStoreOnly -> DQ: Ikke tilgjengelig
                        # get api from 'localization' > json['data']['localizations']['json']['product.detail.buybox.availability.notAvailable']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            rows = nth.select("tr")

            for row in rows:
                try:
                    tag = row.select('td')
                    if tag and len(tag) >= 2:
                        key = tag[0]
                        data = tag[1]
                    
                    result[key.get_text().strip()] = data.get_text().strip()

                except:
                    continue

        return result
    
    def _get_api_data(self, soup):
        _id = None
        _data = {}
        tag = soup.select_one('div[data-bv-show="questions"]')

        if tag:
            _id = tag.get('data-bv-product-id')
            if not _id:
                tag = soup.select_one('[property="og:url"]')
                if tag:
                    _id = tag['content'].split('/')[-1]

        
        params = {
            'appMode': 'b2c',
            'user': 'anonymous',
            'operationName': 'getProductWithDynamicDetails'
        }
        #71e301b8e2a96d97efb141d297ab0880ae67cebb9e3c0f6d4184977075f64c06 - old
        if _id:
            variables = {"articleNumber":_id,"withCustomerSpecificPrices":False}
            extensions = {"persistedQuery":{"version":1,"sha256Hash":"628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705"}}
            params['variables'] = json.dumps(variables, ensure_ascii=False)
            params['extensions'] = json.dumps(extensions, ensure_ascii=False)

            # req_url = 'https://www.elkjop.no/cxorchestrator/no/api?appMode=b2c&user=anonymous&operationName=getProductWithDynamicDetails&variables=%7B%22articleNumber%22%3A%2287248%22%2C%22withCustomerSpecificPrices%22%3Afalse%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2271e301b8e2a96d97efb141d297ab0880ae67cebb9e3c0f6d4184977075f64c06%22%7D%7D'
            req_url = 'https://www.elkjop.no/cxorchestrator/no/api?{}'.format(urlencode(params))
            
            res = self.downloader.download_from_api(req_url)
            
            if res:
                _data = json.loads(str(res))

        return _data
    
    def _get_api_spec(self, soup):
        _id = None
        _data = {}
        tag = soup.select_one('div[data-bv-show="questions"]')

        if tag:
            _id = tag.get('data-bv-product-id')
            if not _id:
                tag = soup.select_one('[property="og:url"]')
                if tag:
                    _id = tag['content'].split('/')[-1]
        
        params = {
            'appMode': 'b2c',
            'user': 'anonymous',
            'operationName': 'getProductSpecification'
        }
        if _id:
            variables = {"articleNumber":_id}
            extensions = {"persistedQuery":{"version":1,"sha256Hash":"efac648c4c9304aa989f449218f6e6bc18c94fbc0d015001c4cc808079c0e92b"}}
            params['variables'] = json.dumps(variables, ensure_ascii=False)
            params['extensions'] = json.dumps(extensions, ensure_ascii=False)

            req_url = 'https://www.elkjop.no/cxorchestrator/no/api?{}'.format(urlencode(params))
            
            res = self.downloader.download_from_api(req_url)
            
            if res:
                _data = json.loads(str(res))

        return _data
    
    def _get_api_desc(self, soup):
        _id = None
        _data = {}
        tag = soup.select_one('div[data-bv-show="questions"]')

        if tag:
            _id = tag.get('data-bv-product-id')
            if not _id:
                tag = soup.select_one('[property="og:url"]')
                if tag:
                    _id = tag['content'].split('/')[-1]
            
            
        params = {
            'appMode': 'b2c',
            'user': 'anonymous',
            'operationName': 'getProductWithDetails'
        }
        if _id:
            variables = {"articleNumber":_id,"isB2B":False}
            extensions = {"persistedQuery":{"version":1,"sha256Hash":"172093ad41d48dee3ca99455fd9eccc9e8e9a65c6025a54d4379b0ec818e6407"}}
            params['variables'] = json.dumps(variables, ensure_ascii=False)
            params['extensions'] = json.dumps(extensions, ensure_ascii=False)

            req_url = 'https://www.elkjop.no/cxorchestrator/no/api?{}'.format(urlencode(params))
                
            res = self.downloader.download_from_api(req_url)
                
            if res:
                _data = json.loads(str(res))

        return _data

