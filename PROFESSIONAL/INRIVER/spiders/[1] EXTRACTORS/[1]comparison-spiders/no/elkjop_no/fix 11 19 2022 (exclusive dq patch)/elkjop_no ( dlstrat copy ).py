from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from urllib.parse import urlencode, quote
import json, re
from random import randint


class ElkjopNoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.url = ''

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
                        'authority': 'www.elkjop.no'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [307]: # temporary fix, allow 302 redirect
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    if 'meta property="og:type" content="product"' in res.text or 'data-ta="productname-text">' in res.text or '"data":{"product"' in res.text: # PDP Checker
                        _headers = dict(res.headers)
                        if 'Server-Timing' in _headers:
                            _rgx = re.search(r';desc=(.*)', _headers['Server-Timing'])
                            if _rgx:
                                self.x_instana_t = _rgx.group(1)
                        return res.text
                    else:
                        raise DownloadFailureException()

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text
            return None
            # raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            return None
            # raise DownloadFailureException('Download failed - Unhandled Exception')

    def api_video(self,_ean,_href,_brand,_title,_sku):
        payload = {
            'brandName':_brand,
            'SKU':_sku,
            'productPrice':'',
            'productId':_sku,
            'productTitle':_title,
            'categoryTree':'Lyd & Hi-Fi - Hodetelefoner - Marshall Major III Voice trådløse on-ear hodetelefoner (sort) -', #Static for now. Still finding way to dynamic this
            'ean':_ean,
            'oos':0,
            'maxItems':15,
            'ytwv':'',
            '_b':'Chrome',
            '_bv': '97.0.4692.99',
            'p':1,
            '_w':1903,
            '_h':937,
            '_pl':'',
            '_cl':'no',
            'tsltd':0,
            'hn':'www.elkjop.no',
            'href':_href,
            'sId':'s:Cgs151pAh9J4CHlCw67s09rn7qtgw364.x5t/vhOVYgI1GD2QxFEUUpLEtUt1SvQqBdpHz1nfXlI',
            'callback':'_vdly5a2f5e655c'
        }
        headers = {
            'accept':'*/*',
            'accept-encoding':'gzip, deflate',
            'accept-language':'en-US,en;q=0.9',
            'referer':'https://www.elkjop.no/',
            'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'
        }
        queries = urlencode(payload, quote_via=quote)
        api_url = 'https://dapi.videoly.co/1/videos/0/411/?{}'.format(queries)
        try:
            res = self.requester.get(api_url, headers=headers)
            if res.ok:
                raw_data = res.text

        except Exception as e:
            print(f'Failed downloading product videos: {e}')
            raw_data = None

        return raw_data
    
    def download_from_api(self, url, referer=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):

            headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'authorization': '',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'referer': self.url,
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'x-app-checkout': 'false',
            'x-app-mode': 'b2c',
            'x-authorization': '',
            'x-instana-t': self.x_instana_t,
            'x-is-anonymous': 'true',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                            ' Chrome/79.0.3945.130 Safari/537.36'
            }

        if referer:
            headers['referer'] = referer

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text
            else:
                return None

        except:
            return None