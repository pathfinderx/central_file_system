from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class HellotvNlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.primProd = []
        self.ratings = []
        self.prod_url = None
        self.primProd_url = None
        self.ratings_url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'content-length': '229',
                'content-type': 'application/json',
                'origin': 'https://www.hellotv.nl',
                'pragma': 'no-cache',
                'referer': url,
                'store': 'hello_tv_nl',
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None
            
        try:
            _id = self.get_id(url, headers)
            self.primProd = self.get_primcore_product(_id, headers)
            self.ratings = self.get_ratings(_id, headers)

            payload = {
                "operationName":"Product",
                "variables":{"id":int(_id)},
                "query":"query Product($id: Int!) {\n  product(id: $id) {\n    ...ProductDetailsFragment\n    __typename\n  }\n}\n\nfragment ProductDetailsFragment on ProductInterface {\n  ...ProductCardFragment\n  sku\n  shortDescription {\n    html\n    __typename\n  }\n  description {\n    html\n    __typename\n  }\n  image {\n    label\n    url\n    __typename\n  }\n  mediaGallery {\n    disabled\n    label\n    position\n    url\n    __typename\n  }\n  stockStatus\n  onlyXLeftInStock\n  manufacturer\n  canonicalUrl\n  __typename\n}\n\nfragment ProductCardFragment on ProductInterface {\n  id\n  sku\n  name\n  ean\n  enabled\n  metaDescription\n  canonicalUrl\n  urlKey\n  urlRewrites {\n    url\n    parameters {\n      name\n      value\n      __typename\n    }\n    __typename\n  }\n  onlyXLeftInStock\n  pimcoreProductData {\n    id\n    visibilityplayseat\n    __typename\n  }\n  smallImage {\n    url\n    label\n    disabled\n    __typename\n  }\n  priceRange {\n    minimumPrice {\n      discount {\n        amountOff\n        percentOff\n        __typename\n      }\n      finalPrice {\n        currency\n        value\n        __typename\n      }\n      regularPrice {\n        currency\n        value\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  pimcoreId\n  stockStatus\n  __typename\n}\n"
            }

            api_url = f'https://www.hellotv.nl/graphql?product/id={_id}'
            res = self.requester.post(api_url, timeout=10, headers=headers, cookies=None, data=json.dumps(payload))

            status_code = res.status_code

            if status_code in [200, 201]:
                self.prod_url = api_url
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')


    def get_id(self, _url, headers):
        _id = None
        _url_ext = _url.replace('https://www.hellotv.nl','')

        try:
            payload = {
                "operationName":"ResolveUrl",
                "variables":{"url":_url_ext},
                "query":"query ResolveUrl($url: String!) {\n  urlResolver(url: $url) {\n    id\n    type\n    relativeUrl\n    redirectCode\n    __typename\n  }\n}\n"
            }
            url = f'https://www.hellotv.nl/graphql?urlResolver/url={_url_ext}'
            res = self.requester.post(url, timeout=10, headers=headers, cookies=None, data=json.dumps(payload))
            _json = json.loads(res.text)
            _id = str(_json['data']['urlResolver']['id'])
            
        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled Exception')
        return _id

    def get_primcore_product(self, _id, headers):
        data = []
        try:
            api_url = f'https://www.hellotv.nl/graphql?product/id={_id}'
            self.primProd_url = api_url
            payload = {
                "operationName":"ProductPimcoreData",
                "variables":{"id":int(_id)},
                "query": "query ProductPimcoreData($id: Int!) {\n  product(id: $id) {\n    id\n    pimcoreProductData {\n      ...PimcoreProductCardFragment\n      ean\n      isBuitenkansje\n      productnaam\n      merk\n      pros\n      cons\n      alternatieveProductenTweakwiseTemplateId\n      main2 {\n        ...MinimalPimcoreImage\n        __typename\n      }\n      productfotos {\n        ... on PimcoreHotspotimage {\n          image {\n            ...MinimalPimcoreImage\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      detailImages {\n        ... on PimcoreHotspotimage {\n          image {\n            ...MinimalPimcoreImage\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      videoThumbnails {\n        ... on PimcoreHotspotimage {\n          image {\n            ...MinimalPimcoreImage\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      variant\n      screenSize\n      soundbarKleur\n      receiverKleur\n      muurbeugelKleur\n      televisieKleur\n      cdspelerKleur\n      draadlozespeakerKleur\n      hifispeakerKleur\n      homecinemaKleur\n      netwerkspelerKleur\n      stereosetKleur\n      versterkerKleur\n      voedingKleur\n      psProductFotos {\n        ... on PimcoreHotspotimage {\n          image {\n            ...MinimalPimcoreImage\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      compatibilityTitle\n      psPrismicSlicesCompatibility {\n        ... on PimcoreObjectPrismicSlice {\n          id\n          prismicType\n          prismicUid\n          __typename\n        }\n        __typename\n      }\n      psPrismicSliceProductFeatures {\n        ... on PimcoreObjectPrismicSlice {\n          id\n          prismicType\n          prismicUid\n          __typename\n        }\n        __typename\n      }\n      psPrismicSliceProductInfoList {\n        ... on PimcoreObjectPrismicSlice {\n          id\n          prismicType\n          prismicUid\n          __typename\n        }\n        __typename\n      }\n      playseatTagline\n      pSVariant\n      pSVariantNaam\n      pSVariantWaarde\n      pSSpecialeEditie\n      playseatTagline\n      psLangeOmschrijving\n      psCrosssellTemplateId\n      pSViewSwitcherTitle\n      viewSwitcher {\n        ... on PimcoreFieldcollectionPlayseatProductViewSwitcher {\n          name\n          mainImage {\n            ...MinimalPimcoreImage\n            __typename\n          }\n          vsDetailImages {\n            ... on PimcoreHotspotimage {\n              image {\n                ...MinimalPimcoreImage\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      psCombinations {\n        ... on PimcoreObjectCombiDeal {\n          id\n          discountPercentage\n          order\n          combiProducts {\n            ... on PimcoreProduct {\n              id\n              productnaam\n              ean\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      psKeyFeaturesList {\n        ... on PimcoreObjectProductKeyFeatures {\n          id\n          keyFeaturesList\n          __typename\n        }\n        __typename\n      }\n      promotions {\n        ... on PimcoreObjectPromotion {\n          id\n          title\n          link\n          label\n          text\n          isKeepMeInformed\n          image {\n            fullpath\n            __typename\n          }\n          priority\n          promotionProductButtons {\n            ... on PimcoreObjectProductButton {\n              id\n              ...PimcoreObjectProductButtonFragment\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      primaryPromotion {\n        ... on PimcoreObjectPromotion {\n          id\n          title\n          link\n          label\n          link\n          text\n          image {\n            fullpath\n            __typename\n          }\n          priority\n          promotionProductButtons {\n            ... on PimcoreObjectProductButton {\n              id\n              ...PimcoreObjectProductButtonFragment\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      specialist {\n        ... on PimcoreObjectSpecialist {\n          id\n          name\n          location\n          image {\n            fullpath\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      relatedCategories {\n        ... on PimcoreObjectRelatedCategorySet {\n          relatedCategories {\n            ... on PimcoreObjectRelatedCategories {\n              id\n              title\n              image {\n                fullpath\n                __typename\n              }\n              viewAllTitle\n              viewAllLink\n              categories {\n                ... on PimcoreCategory {\n                  id\n                  name\n                  urlKey\n                  urlPath\n                  image {\n                    fullpath\n                    __typename\n                  }\n                  __typename\n                }\n                __typename\n              }\n              facets {\n                ... on PimcoreFieldcollectionRelatedCategoryFacet {\n                  attributeKey\n                  inputs {\n                    argument\n                    input\n                    __typename\n                  }\n                  __typename\n                }\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        ... on PimcoreObjectRelatedCategories {\n          id\n          viewAllTitle\n          viewAllLink\n          categories {\n            ... on PimcoreCategory {\n              id\n              name\n              urlKey\n              urlPath\n              image {\n                fullpath\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      prismicSlices {\n        ... on PimcoreObjectProductPrismicSlices {\n          element {\n            ... on PimcoreObjectPrismicSlice {\n              id\n              as\n              prismicType\n              prismicUid\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      productButtons {\n        ... on PimcoreObjectProductButton {\n          id\n          ...PimcoreObjectProductButtonFragment\n          __typename\n        }\n        __typename\n      }\n      ...PimcoreSpecificationsFragment\n      siblings {\n        ... on PimcoreProduct {\n          ...PimcoreProductSiblingFragment\n          __typename\n        }\n        __typename\n      }\n      pSProductNaam\n      psPageType\n      webmVideoUrl\n      mp4VideoUrl\n      psPromotions {\n        ... on PimcoreObjectPromotion {\n          label\n          title\n          text\n          link\n          image {\n            fullpath\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      psPrimaryPromotion {\n        ... on PimcoreObjectPromotion {\n          label\n          title\n          text\n          link\n          image {\n            fullpath\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      upsellproduct {\n        ... on PimcoreProduct {\n          ean\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment MinimalPimcoreImage on PimcoreAsset {\n  id\n  fullpath\n  filename\n  metadata {\n    name\n    data\n    __typename\n  }\n  __typename\n}\n\nfragment PimcoreProductCardFragment on PimcoreProduct {\n  id\n  ean\n  productnaam\n  kenmerken\n  isBuitenkansje\n  enabled\n  variant\n  variantValue\n  screenSize\n  quantityIncrementsAmount\n  urlKey\n  robotsActive\n  robotValues\n  maxSnippetValue\n  maxImagePreviewValue\n  maxVideoPreviewValue\n  unavailableAfterDate\n  siblings {\n    ... on PimcoreProduct {\n      id\n      ean\n      isBuitenkansje\n      enabled\n      variant\n      variantValue\n      urlKey\n      price\n      screenSize\n      main2 {\n        ... on PimcoreAsset {\n          fullpath\n          filename\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  tweakwiseCrossSellTemplateId\n  voorraadAmsterdam\n  voorraadArnhem\n  voorraadBreda\n  voorraadDoetinchem\n  voorraadEindhoven\n  voorraadGroningen\n  voorraadNaarden\n  voorraadNijmegen\n  voorraadUtrecht\n  voorraadZoeterwoude\n  voorraadDenBosch\n  voorraadRotterdam\n  voorraadDuiven\n  psKorteomschrijving\n  psMediumomschrijving\n  playseatTagline\n  psUspList {\n    ... on PimcoreObjectProductUspsLists {\n      id\n      uspList\n      __typename\n    }\n    __typename\n  }\n  psMainProductImage {\n    ... on PimcoreAsset {\n      fullpath\n      filename\n      metadata {\n        name\n        data\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  psMainProductHoverImage {\n    ... on PimcoreAsset {\n      fullpath\n      filename\n      __typename\n    }\n    __typename\n  }\n  main2 {\n    ... on PimcoreAsset {\n      fullpath\n      filename\n      __typename\n    }\n    __typename\n  }\n  hoverImage {\n    ... on PimcoreAsset {\n      fullpath\n      filename\n      __typename\n    }\n    __typename\n  }\n  productvideo1 {\n    type\n    data {\n      ... on PimcoreVideoDataDescriptor {\n        id\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  productvideo2 {\n    type\n    data {\n      ... on PimcoreVideoDataDescriptor {\n        id\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  productvideo3 {\n    type\n    data {\n      ... on PimcoreVideoDataDescriptor {\n        id\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  productvideo4 {\n    type\n    data {\n      ... on PimcoreVideoDataDescriptor {\n        id\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  primaryPromotion {\n    ... on PimcoreObjectPromotion {\n      id\n      label\n      __typename\n    }\n    __typename\n  }\n  promotions {\n    ... on PimcoreObjectPromotion {\n      title\n      label\n      link\n      linkBehaviour\n      id\n      text\n      priority\n      image {\n        ... on PimcoreAsset {\n          fullpath\n          __typename\n        }\n        __typename\n      }\n      promotionProductButtons {\n        ... on PimcoreObjectProductButton {\n          id\n          ...PimcoreObjectProductButtonFragment\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  combinations {\n    ... on PimcoreObjectCombiDeal {\n      id\n      discountPercentage\n      order\n      combiProducts {\n        ... on PimcoreProduct {\n          id\n          productnaam\n          ean\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  deliveryTimetemplate {\n    ... on PimcoreObjectDeliverytimes {\n      deliveryText\n      deliveryTextShort\n      deliveryTextWeekend\n      deliveryTextWeekendShort\n      color\n      __typename\n    }\n    __typename\n  }\n  combiDealSets {\n    ... on PimcoreObjectCombiDealSet {\n      id\n      title\n      discount\n      discountLabel\n      combiDeals {\n        ... on PimcoreObjectCombiDealNew {\n          id\n          title\n          image {\n            ... on PimcoreAsset {\n              fullpath\n              filename\n              __typename\n            }\n            __typename\n          }\n          products {\n            ... on PimcoreProduct {\n              id\n              productnaam\n              ean\n              enabled\n              variant\n              urlKey\n              price\n              specialprice\n              quantityIncrementsAmount\n              main2 {\n                ... on PimcoreAsset {\n                  fullpath\n                  filename\n                  __typename\n                }\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment PimcoreSpecificationsFragment on PimcoreProduct {\n  specifications {\n    __typename\n    field\n    fieldType\n    fieldTitle\n    fieldTooltip\n    sectionName\n    sectionTitle\n    objectBrick\n    value {\n      ... on PimcoreSpecificationsObjectBrickAttributeString {\n        value\n        displayValue\n        __typename\n      }\n      ... on PimcoreSpecificationsObjectBrickAttributeStrings {\n        values: value\n        displayValue\n        displayValues\n        __typename\n      }\n      ... on PimcoreSpecificationsObjectBrickAttributeFloat {\n        amount: value\n        displayValue\n        __typename\n      }\n      ... on PimcoreSpecificationsObjectBrickAttributeBoolean {\n        isEnabled: value\n        displayValue\n        __typename\n      }\n      ... on PimcoreSpecificationsObjectBrickAttributeInt {\n        displayValue\n        __typename\n      }\n      __typename\n    }\n  }\n  __typename\n}\n\nfragment PimcoreObjectProductButtonFragment on PimcoreObjectProductButton {\n  id\n  title\n  hideAddToCart\n  usps\n  showTooltip\n  tooltip\n  tooltipMobile\n  enabled1\n  enabled2\n  text1\n  text2\n  variant1\n  variant2\n  icon1\n  icon2\n  link1\n  link2\n  openIn1\n  openIn2\n  standardButtonColor1\n  standardButtonColor2\n  standardBorderColor1\n  standardBorderColor2\n  standardTextColor1\n  standardTextColor2\n  activeButtonColor1\n  activeButtonColor2\n  activeBorderColor1\n  activeBorderColor2\n  activeTextColor1\n  activeTextColor2\n  hoverButtonColor1\n  hoverButtonColor2\n  hoverBorderColor1\n  hoverBorderColor2\n  hoverTextColor1\n  hoverTextColor2\n  focusButtonColor1\n  focusButtonColor2\n  focusBorderColor1\n  focusBorderColor2\n  focusTextColor1\n  focusTextColor2\n  useHexStyling\n  __typename\n}\n\nfragment PimcoreProductSiblingFragment on PimcoreProduct {\n  id\n  productnaam\n  ean\n  isBuitenkansje\n  variant\n  screenSize\n  soundbarKleur\n  receiverKleur\n  muurbeugelKleur\n  televisieKleur\n  draadlozespeakerKleur\n  cdspelerKleur\n  hifispeakerKleur\n  homecinemaKleur\n  netwerkspelerKleur\n  stereosetKleur\n  versterkerKleur\n  voedingKleur\n  pSVariant\n  pSVariantNaam\n  pSVariantWaarde\n  pSSpecialeEditie\n  urlKey\n  main2 {\n    ...MinimalPimcoreImage\n    __typename\n  }\n  productfotos {\n    ... on PimcoreHotspotimage {\n      image {\n        ...MinimalPimcoreImage\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  detailImages {\n    ... on PimcoreHotspotimage {\n      image {\n        ...MinimalPimcoreImage\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  ...PimcoreSpecificationsFragment\n  __typename\n}\n"
            }
            res = self.requester.post(api_url, timeout=10, headers=headers, cookies=None, data=json.dumps(payload))
            _json = json.loads(res.text)
            data = _json
            
        except Exception as e:
            pass
        
        return data

    def get_ratings(self, _id, headers):
        data = []
        api_url = f'https://www.hellotv.nl/graphql?advreview/productId={_id}'
        self.ratings_url = api_url
        payload = {
            "operationName":"ReviewStats",
            "variables":{"productId":int(_id)},
            "query":"query ReviewStats($productId: Int!) {\n  advreview(productId: $productId, page: 0) {\n    totalRecords\n    ratingSummaryValue\n    __typename\n  }\n}\n"
        }
        
        try:
            res = self.requester.post(api_url, timeout=10, headers=headers, cookies=None, data=json.dumps(payload))
            _json = json.loads(res.text)
            data = _json
            
        except Exception as e:
            pass

        return data