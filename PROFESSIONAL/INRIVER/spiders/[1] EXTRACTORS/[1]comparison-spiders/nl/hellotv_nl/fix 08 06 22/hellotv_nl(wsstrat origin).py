import json
import logging
import re
import datetime, time
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_variety_template, get_variety_template


class HellotvNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.hellotv.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_variety_template()
        prodData = json.loads(raw_data)
        primProd = self.downloader.primProd
        ratings = self.downloader.ratings
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(prodData)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(prodData)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(prodData)

        # Extract ratings (score and reviews) 
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(ratings)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(prodData)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(primProd)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(prodData, primProd)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(primProd)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(primProd)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(primProd)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(primProd)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(result)

        return result

    def get_price(self, prodData):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if prodData['data']['product']['priceRange']['minimumPrice']['finalPrice']['value']:
                source, value = self.downloader.prod_url, str(prodData['data']['product']['priceRange']['minimumPrice']['finalPrice']['value'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, prodData):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR' #Default

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, prodData):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if prodData['data']['product']['stockStatus']:
                source, value = self.downloader.prod_url, prodData['data']['product']['stockStatus']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value
    
    def get_rating(self, ratings):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 10 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if ratings['data']['advreview']['ratingSummaryValue']:
                score_source, score_value = self.downloader.ratings_url, str(ratings['data']['advreview']['ratingSummaryValue'])
            if ratings['data']['advreview']['totalRecords']:
                reviews_source, reviews_value = self.downloader.ratings_url, str(ratings['data']['advreview']['totalRecords'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, prodData):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if prodData['data']['product']['name']:
                source, value = self.downloader.prod_url, prodData['data']['product']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if primProd['data']['product']['pimcoreProductData']['merk']:
                source, value = self.downloader.primProd_url, primProd['data']['product']['pimcoreProductData']['merk']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, prodData, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            temp_desc = []
            temp_src = []
            if primProd and 'data' in primProd.keys() and 'product' in primProd['data'] and 'pimcoreProductData' in primProd['data']['product'] and \
                'pros' in primProd['data']['product']['pimcoreProductData'] and 'cons' in primProd['data']['product']['pimcoreProductData']:
                temp_desc.append(primProd['data']['product']['pimcoreProductData']['pros'].replace('\n',''))
                temp_desc.append(primProd['data']['product']['pimcoreProductData']['cons'].replace('\n',''))
                temp_src.append(self.downloader.primProd_url)

            if prodData['data']['product']['description']['html']:
                _desc = prodData['data']['product']['description']['html']
                #Removing html tags
                if _desc:
                    temp_desc.append(_desc.replace('<br />\n',''))
                    temp_src.append(self.downloader.prod_url)
            
            if temp_desc and temp_src:
                value = '. '.join(temp_desc)
                source = ' + '.join(temp_src)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            if primProd['data']['product']['pimcoreProductData']['specifications']:
                source = self.downloader.primProd_url
                for _spec in primProd['data']['product']['pimcoreProductData']['specifications']:
                    key_val = _spec['fieldTitle']
                    v_val = _spec['value']['displayValue']
                    value[key_val] = v_val if v_val is not None else ''
                

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if primProd['data']['product']['pimcoreProductData']['main2']['fullpath']:
                value.append('https://pimcore.hellotv.nl/' + primProd['data']['product']['pimcoreProductData']['main2']['fullpath'])
            if primProd['data']['product']['pimcoreProductData']['productfotos']:
                for _img in primProd['data']['product']['pimcoreProductData']['productfotos']:
                    value.append('https://pimcore.hellotv.nl/' + _img['image']['fullpath'])

            if value:
                source = self.downloader.primProd_url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            for idx in range(1, 4):
                _vid = primProd['data']['product']['pimcoreProductData']['productvideo' + str(idx)]
                if _vid:
                    value.append('https://www.youtube.com/watch?v=' + _vid['data']['id'])

            if value:
                source = self.downloader.primProd_url
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, primProd):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if primProd['data']['product']['pimcoreProductData']['deliveryTimetemplate']['deliveryText']:
                source, value = self.downloader.primProd_url, primProd['data']['product']['pimcoreProductData']['deliveryTimetemplate']['deliveryText']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
    
    def _get_shipping(self, result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value
    
    