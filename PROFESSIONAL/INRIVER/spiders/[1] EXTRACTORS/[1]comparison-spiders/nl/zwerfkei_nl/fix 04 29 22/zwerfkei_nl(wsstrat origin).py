import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

import math


class ZwerfkeiNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.zwerfkei.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if soup:
                discount_price_container = soup.select_one('.discountprice')
                price_container = soup.select_one('.prijs ')
                if discount_price_container:
                    price_value = discount_price_container.text
                    if price_value:
                        value = self.clean_string(price_value)
                        source = str(discount_price_container)

                elif price_container:
                    price_value = price_container.text
                    if price_value:
                        value = self.clean_string(price_value)
                        source = str(price_container)

                else:
                    tag = soup.select_one('div.article_price') or soup.select_one('[itemprop="highPrice"]')
                    if tag and tag.has_attr('content'):
                        source, value = str(tag), tag.get('content')
                    else:
                        source, value = str(tag), tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        value = 'EUR'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("meta",{"property":"og:availability"})
            if tag:
                source, value = str(tag), tag.get('content')
            else:
                if soup:
                    stock_status_container = soup.select_one('span.stock_status')
                    availability_container = soup.select('.hint_stock')
                    if availability_container and stock_status_container:
                        product_status = stock_status_container.get('data-status')
                        if len(availability_container) > 0 and product_status:
                            for hint_item in availability_container:
                                if product_status in str(hint_item):
                                    value = str(hint_item.text).strip()
                                    source = str(hint_item)
                                    

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "10"  # For this site, max rating is 10 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                rating_container = soup.find('meta', {'itemprop':'ratingValue'})
                if rating_container:
                    rating_value = rating_container.get('content')
                    if rating_value:
                        score_value = str(rating_value)
                        score_source = str(rating_container)

                reviews_container = soup.find('meta', {'itemprop':'ratingCount'})
                if reviews_container:
                    reviews_containter_value = reviews_container.get('content')
                    if reviews_containter_value:
                        reviews_value = str(reviews_containter_value)
                        reviews_source = str(reviews_container)
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                title_container = soup.find('span',{'class':'name'})
                if title_container:
                    title_value = title_container.text
                    if title_value:
                        value = self.clean_string(title_value)
                        source = str(title_container)
                else:
                    title_tag = soup.select_one('[property="og:title"]')

                    if title_tag and title_tag.has_attr('content'):
                        source, value = str(title_tag), title_tag.get('content')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                brand_container = soup.find('meta', {'itemprop':'brand'})
                if brand_container:
                    brand_value = brand_container.get('content')
                    if brand_value:
                        value = self.clean_string(brand_value)
                        source = str(brand_container)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                descrption_container = soup.find('meta', {'itemprop':'description'})
                if descrption_container:
                    description_value = descrption_container.get('content')
                    if description_value:
                        value = self.clean_string(description_value)
                        source = str(descrption_container)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            if soup:
                specification_container = soup.select('.article_description .list-group .list-group-item .row')
                if specification_container:
                    if len(specification_container) > 0:
                        for specification_item in specification_container:
                            specification_entry = specification_item.select('div')
                            if specification_entry:
                                if len(specification_entry) > 1:
                                    spec_label = specification_entry[0].text
                                    spec_value = specification_entry[1].text

                                    if spec_label and spec_value:
                                        value[self.clean_string(spec_label)] =self.clean_string(spec_value)
                                        source = str(specification_container)
                else:
                    spec_tag = soup.select('.d-none.d-md-block .bs.list-group .bs.list-group-item')
                    if spec_tag:
                        source = ''.join(str(ii) for ii in spec_tag)
                        for _spec in spec_tag:
                            key_val = _spec.select_one('.bs.col-5')
                            v_val = _spec.select_one('.bs.col')
                            value[key_val.get_text(strip=True)] = v_val.get_text(strip=True)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if soup:
                image_list = soup.select('.images .thumblist li img')
                if image_list:
                    if len(image_list) > 0:
                        for image_item in image_list:
                            image_url = image_item.get('src')
                            if image_url:
                                value.append(image_url.replace('/9-', '/10-'))
                else:
                    img_tag = soup.select('.list-inline-item > a > img')
                    if img_tag:
                        source = ''.join(str(ii) for ii in img_tag)
                        value = [_img['src'].replace('/9-','/3-') for _img in img_tag if 'nopicture.jpg' not in _img['src']] #Change image size

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                delivery_container = soup.select('.fa-ul li a')
                if delivery_container:
                    if len(delivery_container) > 0:
                        for delivery_entry in delivery_container:
                            delivery_value = delivery_entry.text
                            if delivery_value:
                                if 'Vandaag' in delivery_value:
                                    value = self.clean_string(delivery_value)
                                    source = str(delivery_entry)
                                    break
                else:
                    delivery_container = soup.select_one('.row.mb-1 a')
                    if delivery_container:
                        source, value = str(delivery_container), ' '.join(delivery_container.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                shipping_container = soup.select('.fa-ul li')
                if shipping_container:
                    if len(shipping_container) > 0:
                        for shipping_entry in shipping_container:
                            shipping_value = shipping_entry.text
                            if shipping_value:
                                if 'Gratis' in shipping_value:
                                    value = self.clean_string(shipping_value)
                                    source = str(shipping_entry)
                                    break
                                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def clean_string(self, text):
        final_text = None
        if text:
            final_text = str(text).replace('\n', '')
            final_text = final_text.replace('\t', '')
            final_text = final_text.replace('\r', '')
            final_text = final_text.replace('\xa0', ' ')
            final_text = final_text.replace('&#39;', "'")
            final_text = final_text.replace('&nbsp;', ' ')
            final_text = final_text.strip()
        return final_text