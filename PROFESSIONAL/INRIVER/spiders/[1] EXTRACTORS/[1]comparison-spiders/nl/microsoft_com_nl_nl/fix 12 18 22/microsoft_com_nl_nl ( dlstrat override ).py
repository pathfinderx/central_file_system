from strategies.download.uk.microsoft_com_en_gb import MicrosoftComEnGbDownloadStrategy

from strategies.download.base import DownloadStrategy, DownloadWithVariantStrategy
from strategies.download.exceptions import DownloadFailureException
from urllib.parse import urlencode
from bs4 import BeautifulSoup

import json
import re

class MicrosoftComNlNlDownloadStrategy(MicrosoftComEnGbDownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None, variant=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        
        if variant:
            if 'uid' in variant:
                del variant['uid']
            
        if not isinstance(headers, dict):
            headers = {
                "Accept": "text/html, application/xhtml+xml, image/jxr, */*",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US, en; q=0.5",
                "Host": "www.microsoft.com",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063"
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if 'www.xbox.com' in url:
                headers['Host'] = 'www.xbox.com'
                
            if not data:
                response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = response.status_code
            
            if status_code in [200, 201]:
                is_redirect = False

                for item in response.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    
                    if self._is_dead_link(response.text):
                        raise DownloadFailureException('This url is valid dead link')
                        
                    else:
                        response = self._get_variant_raw_text(response, headers, variant=variant)
                        return response.text + f'<div class="main_url" content="{url}"></div>'

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    # Get raw data using variant (if variant has content)
    def _get_variant_raw_text(self, res, headers, variant=None):

        if not variant:
            print("[*] No variant detected. Will not proceed downloading url with variant")
            return res
            
        try:

            ui_type = self._get_ui_type(res.text)

            print("[*] Downloading url with variant via {}. . .".format(ui_type))
            
            # UI checker
            if ui_type == 'v1 tile selector':
                return self._get_data_via_v1_tile(res, headers, variant=variant)
            
            # No variant selector/picker and no variant picker types of UI are almost identical
            # Check on the same condition.
            if ui_type in ['color only selector', 'no variant selector']:
                return self._get_data_via_color_picker(res, headers, variant=variant)
            
        except Exception as e:
            print('[*] Unable to download url content with variant. Exception triggered: {}'.format(e))    
                 
        return res

    # Get raw data via variant specification picker
    def _get_data_via_v1_tile(self, res, headers, variant=None):
        soup = BeautifulSoup(res.text, 'lxml')
        color_hex = None
        url = None
        
        try:
            if 'color' in variant.keys():
                tag = soup.select_one('link[rel="canonical"]') or \
                            soup.select_one('[property="og:url"]')
                                
                if tag:
                    if tag.has_attr('href'):
                        url = tag.get('href')
                    else:
                        url = tag.get('content')
                    
                    tags = soup.select('button[data-select-button-swatch]')
                    
                    for tag in tags:
                        color = None
                        for k, v in tag.attrs.items():
                            if isinstance(v, str):
                                if variant['color'].lower() in v.lower():
                                    color = v.lower()
                        if color:
                            break

                    # If no color selector, just return the original response
                    if not tag:
                        print('[*] No spec color picker. Will not overwrite response')
                        return res


                if tag and tag.has_attr('data-value'):
                    color_hex = {
                        "selectedColor": tag.get('data-value')
                    }                            

                if url and color_hex:
                    url = '{}?{}'.format(url, urlencode(color_hex))
                    res = self.requester.get(url, headers=headers)

                    if res.ok: 
                        return res

        except Exception as e:
            print('[*] Unable to download url content with variant. Cause: {}'.format(e)) 

        return res
    
    # Get raw data via variant color picker      
    def _get_data_via_color_picker(self, res, headers, variant=None):
        
        soup = BeautifulSoup(res.text, 'lxml')
        color_sku = None
        url = None
        try:
            url_tag = soup.select_one('link[rel="canonical"]')
            if url_tag and url_tag.has_attr('href'):
                url = url_tag.get('href')

            if 'color' in variant.keys():
                
                tags = soup.select('[name="sku-selector"] option') or \
                        soup.find('script', text=re.compile('window\.__BuyBox__='))

                if tags:
                    color_sku = self._get_sku_id(tags, variant['color'])

                if url and color_sku:
                    url = '{}/{}'.format(url, color_sku)
                    res = self.requester.get(url, headers=headers)
                    print(res.url)
                    if res.ok:
                        return res
                 
        except Exception as e:
            print('[*] Unable to download url content with variant. Cause: {}'.format(e)) 
        
        return res        
    
    # UI identifier
    def _get_ui_type(self, res):
        if isinstance(res, str):
            soup = BeautifulSoup(res, 'lxml')
        else:
            soup = res
        
        # No variant selector/picker
        # Sample URL: https://www.microsoft.com/en-gb/d/surface-pro-5th-gen/8nkt9wttrbjk/hn2j?activetab=pivot:overviewtab
        tag = soup.select_one('.cli_videoPlayerFadeContent')
        
        if tag: return 'no variant selector'
        
        # Color ONLY selector/picker
        # https://www.microsoft.com/en-gb/d/surface-go-type-cover-qwerty/8VSZF6WTWF29/DXSM?activetab=pivot:overviewtab
        tag = soup.select_one('[data-automation-test-id="buy-box-edition-picker-sku-selector"]') or \
                soup.select_one('.buy-box.material-md-card')
        
        if tag: return 'color only selector'
        
        # All variants/tile type selector/picker v1
        # Sample URL: https://www.microsoft.com/en-gb/store/configure/Surface-Laptop-Go-2/8PGLPV76MJHN
        tag = soup.select_one('.filterselection')
        
        if tag: return 'v1 tile selector'
        # All variants/tile type selector/picker v2
        # Sample URL: https://www.microsoft.com/en-gb/store/configure/configure-your-surface-laptop-4/946627fb12t1?selectedColor=000000
        tag = soup.select_one('.fc-img-carousel-container')
        
        if tag: return 'v2 tile selector'
        
        

    # PDP Checker
    def _is_dead_link(self, res):
        
        soup = BeautifulSoup(res, 'lxml')
        
        tag = soup.select_one('#rootContainer_ConfiguratorV2 [data-reactroot] p')
        if tag:
            text = ' '.join(tag.stripped_strings).lower()
            if 'this product currently has no configurations available.' in text:
                return True\
                
        if 'metadataButton' in res or \
            'DynamicHeading_productTitle' in res or \
                'page-title' in res or 'c-heading-4' in res:
            return False

    def _get_data_from_api(self, url, sku_id):
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "en-US,en;q=0.9",
            "Connection": "keep-alive",
            "Content-Length": "562",
            "Content-Type": "application/json",
            "Host": "inv.mp.microsoft.com",
            "Origin": "https://www.microsoft.com",
            "Referer": "https://www.microsoft.com/",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36"
        }        
        
        payload = [{"skuId":sku_id,"distributorId":"9000000013"}]
        
        res = self.requester.post(url, data=json.dumps(payload), headers=headers)
        print(url)
        if res.ok:
            return res.json()
        
        return None
    
    def _get_sku_id(self, tag, variant_color):
        
        if isinstance(tag, list):
            tags = tag
            for tag in tags:
                color = ' '.join(tag.stripped_strings).lower()
                if variant_color.lower() in color:
                    return tag.get('value')
            

        else:
            data = json.loads(tag.text.split('__=')[-1])
            
            for i in data['selectorInfo'][0]['selectionItems']:
                if variant_color.lower() in i['label'].lower():
                    return i['skuId']
        
        return None