from strategies.website.uk.microsoft_com_en_gb import MicrosoftComEnGbWebsiteStrategy

class MicrosoftComNlNlWebsiteStrategy(MicrosoftComEnGbWebsiteStrategy):
    WEBSITE = 'www.microsoft.com/nl-nl'

    def __init__(self, downloader):
        super().__init__(downloader)