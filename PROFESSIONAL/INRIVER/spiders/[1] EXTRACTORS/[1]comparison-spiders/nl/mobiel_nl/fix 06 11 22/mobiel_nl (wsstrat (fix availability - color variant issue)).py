import re, json
import logging
from urllib.parse import urlencode

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template
from strategies.download.exceptions import DownloadFailureException

from bs4 import BeautifulSoup


class MobielNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mobiel.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        data = soup.find('script', type='application/ld+json')
        self.model_data = {}
        

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup,data)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup,data)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup,data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup,data)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup,data)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup,data)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        # result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, result)

        return result

    # GET PRICE
    def get_price(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            json_data = json.loads(data.get_text())
            if json_data and 'offers' in json_data and 'price' in json_data['offers']:
                tag = json_data['offers']['price']
                source, value = str(data), str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            json_data = json.loads(data.get_text())
            if json_data and 'offers' in json_data and 'priceCurrency' in json_data['offers']:
                tag = json_data['offers']['priceCurrency']
                source, value = str(data), str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            if not self.storage_color_variant_checker(soup,data):
                value = 'outofstockduetovariants'
            else:
                if self.model_data:
                    value = self.model_data['offers']['availability']

                # Commented temporarily ---> remove when useless
                else:
                    # Color Variant
                    tag = soup.find('script', text=re.compile(r'window.__REACT_PROPS__ .*"config"')) or soup.find('script', text=re.compile(r'window.__STATIC_PROPS__ .*"config"'))
                    # soup.find('script', text=re.compile(r'window.__REACT_PROPS__ ='))
                    active_variant_availability, availability = soup.select_one('div.ChooseColor__Row-sc-15379f3-2.ceHCdk.active') or soup.find('div',{'class':re.compile('ChooseColor__Row-sc-.* active')}), str()
                    selected_color = None
                    url_color = None
                    if active_variant_availability:
                        selected_variant = active_variant_availability.find('div',{'class':re.compile('ChooseColor__CheckboxWrapper.*')})
                        if selected_variant:
                            selected_color = selected_variant.text.strip()
                            rgx = re.search(r'los-toestel/(.*)',self.downloader.orig_url)
                            if rgx:
                                url_color = rgx.group(1)
                                if '?' in url_color:
                                    url_color = url_color.split('?')[0]
                        if (url_color and
                            selected_color and
                            selected_color.lower() == url_color.split('-')[-1]):

                            source, value = str(active_variant_availability), 'instock'

                        elif url_color and selected_color and (selected_color.lower() != url_color.replace('-',' ')):
                            value = 'outofstockduetovariants'
                        else:  
                            for x in active_variant_availability.get('class'):
                                if ('active' in x):
                                    availability = active_variant_availability.select_one('div.StockInfo__StockInfoContainer-sc-cxnc3j-0') or \
                                        active_variant_availability.select_one('div.StockInfo__StockInfoContainer-sc-cxnc3j-0.dUirFl.backordered.ChooseColor__ColorStockInfo-sc-15379f3-12.ljEEmU')
                            if availability:
                                source, value = str(active_variant_availability), availability.get_text().strip()
                
                    elif tag:
                        source = str(tag)
                        get_script_data = re.search('({.*})', tag.get_text())
                        if get_script_data:
                            _json = json.loads(get_script_data.group(1))
                            if _json and 'state' in _json.keys() and 'config' in _json['state']:
                                color_variant = _json['state']['config']['path']
                                color_variant = color_variant.split('/')[-1]
                            #Color available in PDP
                            if _json and 'state' in _json.keys() and 'singlePhone' in _json['state'] and 'phoneVersions' in _json['state']['singlePhone']:
                                for color in _json['state']['singlePhone']['phoneVersions']:
                                    color_available = color['versionNameSlug']
                                    #Get Stock
                                    if color_variant == color_available:
                                        value = color['stockStatus']
                            elif _json and 'state' in _json.keys() and 'productPage' in _json['state'] and 'product' in _json['state']['productPage']\
                                and 'productVersions' in _json['state']['productPage']['product'] and _json['state']['productPage']['product']['productVersions']:
                                for item in _json['state']['productPage']['product']['productVersions']:
                                    if 'slug' in item.keys() and item['slug'] == color_variant:
                                        value = item['stock']['status']
                                        break

                    #IF Color Variant is not Available in PDP
                    if value == 'NOT_FOUND':
                        #value = 'outofstock'
                        #Add another Checker
                        tag = soup.select_one('.ChooseColor__ProductColor-sc-1esakua-5.hewTmz.active')
                        if tag:
                            # checker = tag.get_text(strip=True)
                            # if color_variant is not checker.lower():
                            #     value = 'outofstock'
                            # else:
                            tag = soup.select_one('.old-phone__ribbon') or soup.select_one('.stock-info__label')
                            if tag:
                                source, value = str(tag), tag.get_text(strip=True)
                            if not tag:
                                tag = soup.select_one('.Tooltip__Underline-sc-1ypkd24-0.hpfrCI')
                                if tag:
                                    source, value = str(tag), tag.get_text(strip=True)

                    if value == 'NOT_FOUND':
                        # If "add to cart" button are not found, stock is OOS
                        tag = soup.select_one('.Button__Base-sc-123cew0-0.bNVClG.PhoneVersions__OrderButton-sc-1x4v2bc-6') or \
                            soup.select_one('.PhoneVersions__Form-hnkspj-5.csUuoa') # cart tag
                        if tag:
                            source, value = str(tag), "instock"
                        else:
                            # value = "outofstock"
                            tag = soup.select_one('.stock-info') or soup.select_one('.StockInfo__StockInfoContainer-jtv0ru-0') or soup.find('div',{'class':re.compile('StockInfo__StockInfoContainer-.*')})
                            if tag:
                                source, value = str(tag),tag.text.strip()
                                
                    cart_tag = soup.select_one('.PhoneVersions__Form-hnkspj-5.csUuoa')
                    if value == 'NOT_FOUND' and not cart_tag:
                        value = "outofstock"
                    
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup,data):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "10"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            json_data = json.loads(data.get_text())
            if json_data and 'aggregateRating' in json_data and 'ratingValue' in json_data['aggregateRating'] and 'reviewCount' in json_data['aggregateRating']:
                score_source = str(data)
                score_value = str(json_data['aggregateRating']['ratingValue'])
                reviews_source = str(data)
                reviews_value = str(json_data['aggregateRating']['reviewCount'])
            if json_data and 'aggregateRating' in json_data and 'bestRating' in json_data['aggregateRating']:
                score_max_value = json_data['aggregateRating']['bestRating']
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            json_data = json.loads(data.get_text())
            if json_data and 'name' in json_data:
                tag = json_data['name']
                source, value = str(data), str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            json_data = json.loads(data.get_text())
            if json_data and 'brand' in json_data and 'name' in json_data['brand']:
                tag = json_data['brand']['name']
                source, value = str(data), str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.phone-information.phone-information') or soup.select_one('#description-section')
            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings) #tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict
        try:
            tag = soup.select_one('div#specificaties-content') or soup.find('div',{'class': re.compile(r'PhoneSpecs__SectionContainer-.*')})
            if tag:
                keys = tag.select('dt.specification__label.specification__label--slide-out') or soup.find_all('dt',{'class': re.compile(r'PhoneSpecs__TableLabel.*')})
                vals = tag.select('dd.specification__value.specification__value--slide-out') or soup.find_all('dt',{'class': re.compile(r'PhoneSpecs__TableValue.*')})
                for spec in range(len(keys)):
                    key = keys[spec].get_text().strip()
                    val = vals[spec].get_text().strip()
                    if key and val:
                        value[key] = val
                        
                source = str(tag)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            tag = soup.select_one('div.dt-carousel.js-desktop-carousel') or soup.find('div',{'class': re.compile(r'EmblaCarousel__SlidesContainer-.*')})
            if tag:
                imgs = tag.select('img')
                for row in imgs:
                    if row.has_attr('src'):
                        value.append(row['src'])
                
                source = str(tag) 
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
        pass
      
        return source, value

    def _get_delivery(self, soup, result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            # check first if the text container exist, then get json
            tag = soup.select_one('div[class^=Delivery__Base-]') or soup.select_one('div.PhoneVersions__Info-sc-hnkspj-3.jRclBt')
            if tag:
                _json = self.get_delivery_json(soup)
                if tag.get_text():
                    source, value = str(tag), tag.get_text().strip()

                elif _json and 'general_shipment_option' in _json:
                    val = _json['general_shipment_option']
                    # html_rgx = re.compile(r'<.*?>|(&nbsp;)') # Remove html tags from text
                    # value = html_rgx.sub(' ', val)   
                    delivery_soup = BeautifulSoup(val,'lxml')
                    if delivery_soup.select_one('.tooltip--center'):
                        delivery_soup.select_one('.tooltip--center').extract()
                    source = str(_json)
                    value = ' '.join(delivery_soup.stripped_strings)

            if value == 'NOT_FOUND':
                tag = soup.find('div',{'class':'Delivery__Base-sc-9fuxkx-0 hrgpXy PhoneVersions__PhoneDelivery-hnkspj-4 canAhR'}) or soup.select_one('.PhoneVersions__Info-hnkspj-3.jMaGQF span') or soup.select_one('div[class="Delivery__Base-sc-9fuxkx-0 iJsOrT PhoneVersions__PhoneDelivery-hnkspj-4 fbaOgC"] > span')
                if tag:
                    source = str(tag)
                    value = tag.get_text(strip=True)
                else:
                    tag = soup.find('form', {'class': re.compile('PhoneVersions__Form.+')})
                    params = {}

                    if tag and tag.has_attr('action'):
                        params['product_id'] = tag['action'].split('=')[-1]
                        url = 'https://www.mobiel.nl/smartphone/data/shipment_option?{}'.format(urlencode(params))
                        res = self.downloader.download(url)

                        if res:
                            _json = json.loads(res, strict=False)

                            if _json['shipmentOption']:
                                source = url
                                value = ' '.join(
                                    BeautifulSoup(
                                        _json['shipmentOption']['shortDescription'],
                                        'lxml'
                                    ).stripped_strings
                                )
                tag = soup.select_one('.Delivery__Base-sc-9fuxkx-0')
                if tag:
                    source, value = str(tag), tag.get_text().strip()

            # As per validator, delivery should be -1 for Out of stock
            if result['availability']['value'].lower() in ['Dit product is niet meer leverbaar.','dit product is niet meer leverbaar.','verwacht','Niet meer leverbaar','niet meer leverbaar','no_longer_available','in bestelling','backordered','outofstockduetovariants',
            'https://schema.org/backorder']:
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value
                
        except DownloadFailureException:
            # if api download failure just return not found.
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_delivery_json(self, soup):
        tag = soup.find('script', text=re.compile('"cart_icon_data"', re.IGNORECASE))
        if tag:
            txt = tag.get_text().strip()
            matches = re.search(r'\((.*?)\)', txt)
            if matches:
                _json = matches.group(1)
                _json = json.loads(_json)
                return _json
        return None

    def storage_color_variant_checker(self,soup,data):
        result = True
        storage = None
        url_color = None
        _json = json.loads(data.text)
        url_used = self.downloader.orig_url
        if self.downloader.orig_url == self.downloader.res_url:
            print('Redirected URL')

        # Get Storage variant from URL
        if 'opslagcapaciteit' in url_used:
            storage = url_used.split('opslagcapaciteit=')[1]
            if '&' in storage:
                storage = storage.split('&')[0]
        else:
            storage_rgx = re.search(r'smartphone/.*-([0-9]+gb)/los-toestel',url_used)
            if storage_rgx:
                storage = storage_rgx.group(1)


        # Get Color in URL
        rgx = re.search(r'los-toestel/(.*)',url_used)
        if rgx:
            url_color = rgx.group(1)
            if '?' in url_color:
                url_color = url_color.split('?')[0].replace('-',' ')

        if storage and url_color:
            # variant = storage+' '+url_color
            if 'model' in _json.keys():
                model_detail = [x for x in _json['model'] if url_color in x['name'].lower() and storage in x['name'].lower()]
                if not model_detail:
                    tag = soup.find('div',{'class':re.compile('ProductHeader__StorageAndColor-sc-*')})
                    if tag:
                        variant_from_tag = tag.text.strip().lower()
                        if variant_from_tag and url_color in variant_from_tag and storage in variant_from_tag:
                            result = True
                        else:
                            result = False
                    else:
                        result = False
                else:
                    self.model_data = model_detail[0]
            
        return result
