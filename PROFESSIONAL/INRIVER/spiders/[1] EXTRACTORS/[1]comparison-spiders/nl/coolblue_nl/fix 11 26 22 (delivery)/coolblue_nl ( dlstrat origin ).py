from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class CoolblueNlDownloadStrategy(DownloadStrategy):

    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=randint(10, 60), headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        
        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = response.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in response.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    if self.is_valid(response.text): #PDP CHECKER
                        return response.text
                    else:
                        raise DownloadFailureException('Download failed - Missing elements/tags')

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    

    def is_valid(self, text):
        if '"@type": "Offer"' in text or 'js-product-name':
            return True
        else:
            return False