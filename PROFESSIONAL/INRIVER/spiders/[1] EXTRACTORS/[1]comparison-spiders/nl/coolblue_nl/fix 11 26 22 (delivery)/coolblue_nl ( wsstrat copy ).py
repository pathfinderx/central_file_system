import json
import logging
import re
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class CoolblueNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.coolblue.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.zero_checker = None

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls - No Videos Found
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery 
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, result)

        # Extract shipping 
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
          
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
        
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "offers" in _json.keys() and "price" in _json["offers"].keys():
                    price_source, price = str(tag), _json["offers"]['price']
            #Price Checker: Fetching price even if No price found in pdp
            price_checker = soup.select_one('.product-page')
            if price_checker and price_checker.has_attr('data-product-id'):
                checker = price_checker.get('data-product-id')
            
            if checker == '873412':
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value
            else:
                value = price
                source = price_source

            # PDP Price checker
            PDP_price_tag = soup.select_one('[class="sales-price js-sales-price"] .sales-price__current')
            if not PDP_price_tag:
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value
                if value == 'NOT_FOUND':
                    other_price_tag = soup.select_one('.data-table__cell strong.sales-price__current')
                    if other_price_tag:
                        source, value = str(other_price_tag), other_price_tag.get_text(strip=True)
                    else:
                        other_price_tag = soup.select_one('strong.sales-price__current')
                        if other_price_tag:
                            source, value = str(other_price_tag), other_price_tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "offers" in _json.keys() and "priceCurrency" in _json["offers"].keys():
                    source, value = str(tag), _json["offers"]['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "offers" in _json.keys() and "availability" in _json["offers"].keys():
                    source, value = str(tag), _json["offers"]['availability']

            if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value]:
                tag = soup.select_one('[itemprop="availability"]')

                if tag and tag.has_attr('content'):
                    source, value = str(tag), tag['content']

            #Some item_id in Prod2 fetch In stock even if its Out of Stock
            stock_checker = soup.select_one('.color--unavailable')
            if stock_checker:
                stock = stock_checker.get_text(strip=True)
            if value == 'http://schema.org/PreSale' and stock == 'Binnenkort leverbaar':
                value = "https://schema.org/outofstock"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "10"  # For this site, max rating is 10 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"}) or soup.find("script", text=re.compile('.*reviewRating.*'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "aggregateRating" in _json.keys():
                    # Reviews
                    if "reviewCount" in _json["aggregateRating"].keys():
                        reviews_source, reviews_value = str(tag), _json["aggregateRating"]["reviewCount"]

                    # Ratings
                    if "ratingValue" in _json["aggregateRating"].keys():
                        score_source, score_value = str(tag), _json["aggregateRating"]["ratingValue"]
                        
                else:
                    tag = soup.select_one('.review-rating__reviews.overflow--ellipsis[href="#product-reviews"]') or \
                        soup.select_one('.review-rating__reviews.overflow--ellipsis [href="#product-reviews"]')
                    if tag:
                        temp = tag.get_text().split("(")
                        if temp:
                            score_source, score_value = str(tag), re.sub('[^0-9,/]', '', temp[0])
                            reviews_source, reviews_value = str(tag), re.sub('[^0-9]', '', temp[1])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "name" in _json.keys():
                    source, value = str(tag), _json["name"]
                if _json and 'itemReviewed' in _json.keys() and 'name' in _json['itemReviewed']:
                    source, value = str(tag), _json['itemReviewed']["name"]

            if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value] :
                tag = soup.select_one('.js-product-name')
                if tag:
                    source, value = str(tag), tag.get_text()
                else:
                    tag = soup.select_one('.product-card__title')
                    if tag:
                        source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
           
            if tag:
                _json = json.loads(tag.get_text().replace('\n', '').replace('\r', '').replace('\t', ''))

                if _json and "brand" in _json.keys() and "name" in _json["brand"].keys():
                    source, value = str(tag), _json["brand"]['name']

            if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value]:
                source = value = Defaults.GENERIC_NOT_FOUND.value # Forced to -1

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src = []
            val = []
            tag_header = soup.find('h2', {'class':'mb--0 hide@sm-down'})
            if tag_header:
                src.append(str(tag_header))
                val.append(' '.join(tag_header.stripped_strings))
            
            pros_cons = soup.find('div', {'class': 'col--12 col--9@sm'})

            if pros_cons:
                # section1_tag = pros_cons.select_one('div.section--1')
                # if section1_tag:
                # temporary text remover
                if pros_cons.select_one('.call-to-action__content') or pros_cons.select_one('.additional-text'):
                    pros_cons.select_one('.call-to-action__content').extract()
                    pros_cons.select_one('.additional-text').extract()
                src.append(str(pros_cons)) #section1_tag))
                val.append(' '.join(pros_cons.stripped_strings)) #section1_tag.stripped_strings))

            _json = None
            tag = soup.find("script", text=re.compile('.*"@type": "Product".*'), attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text().replace('\n', ' ').replace('\r\r', ' ').replace('\r', '').replace('\t', ''))

                if _json and "description" in _json.keys():
                    src.append(str(tag))
                    val.append('Omschrijving '+_json["description"])

            tag = soup.select_one('.js-product-in-the-box-container')
            if tag:
                src.append(str(tag))
                val.append(' '.join(tag.stripped_strings))

            #Comment for now. Feel free to uncomment
            # if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value]:
            #     tag = soup.select_one('#product-information')

            #     if tag:
            #         src.append(str(tag))
            #         val.append(tag.get_text(strip=True))

            if src and val:
                source = ' '.join(src)
                value = ' '.join(val)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.select('.js-specifications-content .product-specs .product-specs__list-item')

            if tag:
                source, value = str(tag), self._get_initial_specs(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select_one('.product-media-gallery.js-media-gallery')

            if tag and tag.has_attr('data-component') and 'productMediaGalleryZoomOverlay' in tag.get('data-component'):
                source = str(tag)
                _json = json.loads(tag.get('data-component'))
                for images in _json:
                    if images['name'] == "productMediaGalleryZoomOverlay":
                        if 'images' in images['options'].keys():
                            for img_url in images['options']['images']:
                                if 'image' in img_url['type']:
                                    value.append(img_url['url'])
            elif tag:
                imglist = tag.select('img.product-media-gallery__item-image.js-product-media-gallery__item-image.js-product-media-gallery__lazy-load')
                if imglist:     
                    source = str(imglist)
                    for images in imglist:
                        if images and images.has_attr('src'):
                            if 'data:image/gif' in images.get('src'):
                                if images.has_attr('data-src'):
                                    if images.get('data-src') != '':
                                        value.append(images.get('data-src'))
                            else:
                                value.append(images.get('src'))
            else:
                tag = soup.find('img',{'class':'image-with-text-overlay__image'})
                if tag and tag.has_attr('src'):
                    source = str(tag)
                    value.append(tag.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []

        try:
            tags = soup.select('.js-product-media-gallery__item.js-product-page-video-link')
            if len(tags):
                source = str(tags)
                for vid in tags:
                    value.append(vid['data-video'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
    
    def _get_delivery(self, soup, result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            tags = soup.select_one('div.section--3')
            tag = tags.select_one('span.icon-with-text__text')
            if tag:
                source, value = str(tag), tag.get_text().strip()
            else:
                tag = soup.select_one('div.section--4.js-expandable-list')
                if tag:
                    value = Defaults.GENERIC_NOT_FOUND.value
                    # if Vergelijkbare is needed for DQ, 
                    # comment value 
                    # remove comment below

                    #tag = soup.select_one('div.section--6 > h3')
                    #if tag and 'Vergelijkbare' in tag:
                    #     source, value = str(tag), tag.get_text().strip()
                else:
                    tag = soup.select_one('.color--highlight .icon-with-text span:nth-of-type(2)') or \
                        soup.select_one('strong.color--unavailable') or \
                        soup.select_one('strong.color--available')
                    if tag:
                        source, value = str(tag), tag.get_text().strip()
      
            #tag = soup.select_one('.js-delivery-information-usp')
            #if tag:
            #    source, value = str(tag), " ".join(tag.stripped_strings)

            # AS PER VALIDATOR, DQ SHOULD BE -1 IF PRICE IS NOT FOUND IN PDP
            if result['price']['value'] == 'NOT_FOUND':
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value
                
            #check if product has "never available again" in image DQ should be -1
            check_tag = soup.select_one('[class="image-with-text-overlay__overlay"] h2')
            if check_tag:
                if check_tag.get_text().strip() == 'Nooit meer leverbaar':
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
    
    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.js-delivery-information-usp')
            if tag:
                source, value = str(tag), " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value
    
    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
    
        for tag in tags:
            try:

                # key
                key_tag = tag.select_one('.product-specs__item-title')
                if key_tag:
                    key = key_tag.get_text().strip()
                
                # value 
                data_tag = tag.select_one('.product-specs__item-spec')
                if data_tag:
                    data = data_tag.get_text().strip()
                
                result[key] = data

            except:
                continue

        return result
