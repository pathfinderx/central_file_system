import json
import logging
import re
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class VandenborreBeNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.vandenborre.be/nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)


        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'offers' in _json.keys() and 'priceSpecification' in _json['offers'] and 'price' in _json['offers']['priceSpecification']:
                    source, value = str(tag), _json['offers']['priceSpecification']['price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value


    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'offers' in _json.keys() and 'priceCurrency' in _json['offers']:
                    source, value = str(tag), _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'offers' in _json.keys() and 'availability' in _json['offers']:
                    source, value = str(tag), _json['offers']['availability']
                

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5" 
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'aggregateRating' in _json.keys():
                    # Reviews
                    if "reviewCount" in _json["aggregateRating"].keys():
                        reviews_source = str(tag)
                        reviews_value = str(_json["aggregateRating"]["reviewCount"])

                    # Ratings
                    if "ratingValue" in _json["aggregateRating"].keys():
                        score_source = str(tag)
                        score_value = str(_json["aggregateRating"]["ratingValue"])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'name' in _json:
                    source, value = str(tag), _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'brand' in _json.keys():
                    source, value = str(tag), _json['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',type = 'application/ld+json', text = re.compile('priceCurrency'))
            if tag:
                _json = json.loads(re.sub(r'[\r\n]','',tag.text.strip()))
                if _json and 'description' in _json.keys():
                    source, value = str(tag), _json['description']
          
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
        
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('table.specs-table')
            if len(tags)>0:
                source = []
                for tag in tags:
                    spec = tag.select('tr')
                    if len(spec):
                        source.append(str(tag))
                        for sp in spec:
                            key = sp.select_one('td.has-lexi') or sp.select_one('td')
                            val = sp.select_one('td.specs-value')
                            value[key.text.strip()] = val.text.strip()
            
            source = ' '.join(source)
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select_one('div.image-gallery-thumbs')
            if tag:
                img = tag.select('button[data-gtm-label="main product miniature"]')
                if len(img):
                    source = str(tag)
                    for image in img:
                        if image.has_attr('href'):
                            value.append('https:'+image.get('href'))

        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value
         

    
