from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import urllib, json
from request.unblocker import UnblockerSessionRequests

class AlternateDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        # self.requester = requester
        self.requester = UnblockerSessionRequests('de')
        self.normal_requester = requester
        self.url = None
    
    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not self.url: # extract once to prevent any changes during next call unless self.url was nullified
            self.url = url

        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=None, cookies=cookies)
            else:
                res = self.requester.post(url, headers=headers, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def download_product_features_description(self, url=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'content-type': 'text/html; charset=utf-8',
                'cache-control': 'no-cache',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'script',
                'sec-fetch-mode': 'no-cors',
                'sec-fetch-site': 'cross-site',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            # Temporary fix. Features description not found in lxml data; recourse to static api instead as temporary fix
            if url in ['https://www.alternate.de/Razer/Blade-14-(RZ09-0427NGA3-R3G1)-Gaming-Notebook/html/product/1819261']:    
                apiUrl = 'https://media.flixcar.com/delivery/inpage/show/499/de/2156338_2138768_1943043/json?c=jsonpcar499de215633821387681943043&complimentary=0&type=.html'
                res = self.normal_requester.get(apiUrl, timeout=timeout, headers=headers, cookies=cookies)
                if res.status_code in [200,201]:
                    return res.text    

        except Exception as e:
            print(e)
        

    def download_ratings(self, product, url=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(product, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "Host": "www.alternate.de",
                "accept": "*/*",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9",
                "cache-control": "max-age=0",
                "Content-type": "application/x-www-form-urlencoded;charset=UTF-8",
                'Faces-Request': 'partial/ajax',
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
            }

        if not isinstance(cookies, dict):
            cookies = None

        url = "https://www.alternate.de/mobile/details.xhtml"

        data = {
            'lazyForm': 'lazyForm',
            'p': product,
            'lazyComponent': 'lazyRatings',
            'javax.faces.ViewState': 'stateless',
            'javax.faces.source': 'lazyButton',
            'javax.faces.partial.event': 'click',
            'javax.faces.partial.execute': 'lazyButton lazyButton',
            'javax.faces.behavior.event': 'action',
            'javax.faces.partial.ajax': 'true',
        }

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            return None    #added this to cause no error in ratings

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def download_shipping(self, product, url=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(product, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "Host": "www.alternate.de",
                "accept": "*/*",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9",
                "cache-control": "max-age=0",
                "Content-type": "application/x-www-form-urlencoded;charset=UTF-8",
                'Faces-Request': 'partial/ajax',
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
            }

        if not isinstance(cookies, dict):
            cookies = None

        url = "https://www.alternate.de/mobile/details.xhtml"

        data = {
            'lazyForm': 'lazyForm',
            'p': product,
            'lazyComponent': 'lazyShippingCosts',
            'javax.faces.ViewState': 'stateless',
            'javax.faces.source': 'lazyButton',
            'javax.faces.partial.event': 'click',
            'javax.faces.partial.execute': 'lazyButton lazyButton',
            'javax.faces.behavior.event': 'action',
            'javax.faces.partial.ajax': 'true',
        }

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text

            return None

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')