import logging
import re
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class AlternateDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.alternate.de'
    API = 'https://www.alternate.de/mobile/details.xhtml'
    PRODUCT = None

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        product = soup.find('meta', {'itemprop': 'sku'})
        self.PRODUCT = product['content']

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.price span') or soup.find('div', {'class': 'price'})
            if tag:
                source, value = str(tag), tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'id': 'product-top-right'})
            if tag:
                availability_tag = tag.select_one('div.col-12.p-3.text-center')
                if availability_tag:
                    source, value = str(availability_tag), availability_tag.text.replace("\n", "").strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_data = self.downloader.download_ratings(self.PRODUCT)
            if raw_data:
                data = BeautifulSoup(raw_data, "lxml")
                tag = data.find('div', {'class': 'col-12 text-center'})
                if tag:
                    reviews_source = str(self.API)
                    reviews_value = str(tag).split('</div>')[1].strip() 
                    rating_tag = tag.find('div', {'id': 'ratings-card-average-headline'})
                    if rating_tag:
                        score_source, score_value = str(self.API), tag.text.replace("\n", "").strip()
                        
            if 'NOT_FOUND' in score_value:
                split_url = str(self.downloader.url).split('product')
                _url = split_url[0] + 'productRatings' + split_url[1]
                _rating = self.downloader.download(_url)

                if _rating:
                    _data = BeautifulSoup(_rating, "lxml")
                    tag = _data.select_one('[itemprop="aggregateRating"] [itemprop="ratingValue"]')

                    if tag and tag.has_attr('content'):
                        score_source, score_value = str(tag), tag.get('content')

                    if 'NOT_FOUND' in reviews_value:
                        tag = _data.select_one('[itemprop="aggregateRating"] [itemprop="ratingCount"]')

                        if tag and tag.has_attr('content'):
                            reviews_source, reviews_value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'class': 'product-name'})
            if tag:
                title_tag = tag.find('span', {'itemprop': 'name'})
                if title_tag:
                    source, value = str(title_tag), ' '. join(tag.get_text().split())

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'class': 'product-name'})
            if tag:
                title_tag = tag.find('span', {'itemprop': 'brand'})
                if title_tag:
                    source, value = str(title_tag), title_tag.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'id': 'product-details-container'})
            if tag:
                description_tag = tag.find('div', {'id': 'product-description'})
                if description_tag:
                    source, value = str(description_tag), description_tag.text.replace("\n", "")

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'id': 'product-details-container'})
            if tag:
                specification_tag = tag.find('div', {'id': 'product-details'})
                if specification_tag:
                    specifications = specification_tag.select('tr')
                    value = {}
                    for spec in specifications:
                        values = spec.select('td')
                        if values:
                            value[values[0].text] = values[1].text


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value
        
        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('div', {'id': 'slick-images-big'})
            if tag:
                images = tag.select('img')
                source = str(tag)
                for image in images:
                    if image.has_attr('data-lazy'):
                        value.append(self.WEBSITE + image['data-lazy'])
                    else:
                        value.append('https://'+self.WEBSITE + image['data-src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('div', {'id': 'slick-videos-big'})
            if tag:
                videos = tag.select('video')
                source = str(tag)
                for video in videos:
                    value.append(video['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            parent_tag = soup.find('div', {'id': 'product-top-right'})
            if parent_tag:
                tag = parent_tag.select_one('div.col-12.p-3.text-center')
                if tag:
                    [i.decompose() for i in tag.select('span.ml-1')]
                    source, value = str(tag), " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_data = self.downloader.download_shipping(self.PRODUCT)
            if raw_data:
                data = BeautifulSoup(raw_data, "lxml")
                tag = data.find('span', {'class': ''})    
                if tag:
                    source, value = str(self.API), tag.text.replace("\n", "").strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value
