import logging
import re
import json, html

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class AlternateDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.alternate.de'
    API = 'https://www.alternate.de/mobile/details.xhtml'
    PRODUCT = None

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.baseUrl = None
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        self.baseUrl = self.downloader.url

        product = soup.find('meta', {'itemprop': 'sku'})
        self.PRODUCT = product['content']

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.price span') or soup.find('div', {'class': 'price'}) or soup.select_one('span.price')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'id': 'product-top-right'})
            if tag:
                availability_tag = tag.select_one('div.col-12.p-3.text-center') or tag.select_one('.font-weight-bold.text-center.mt-3')
                if availability_tag:
                    source, value = str(availability_tag), availability_tag.text.replace("\n", "").strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_data = self.downloader.download_ratings(self.PRODUCT)
            if raw_data:
                data = BeautifulSoup(raw_data, "lxml")
                tag = data.find('div', {'class': 'col-12 text-center'})
                if tag:
                    reviews_source = str(self.API)
                    reviews_value = str(tag).split('</div>')[1].strip() 
                    rating_tag = tag.find('div', {'id': 'ratings-card-average-headline'})
                    if rating_tag:
                        score_source, score_value = str(self.API), tag.text.replace("\n", "").strip()
                        
            if 'NOT_FOUND' in score_value:
                split_url = str(self.downloader.url).split('product')
                _url = split_url[0] + 'productRatings' + split_url[1]
                _rating = self.downloader.download(_url)

                if _rating:
                    _data = BeautifulSoup(_rating, "lxml")
                    tag = _data.select_one('[itemprop="aggregateRating"] [itemprop="ratingValue"]')

                    if tag and tag.has_attr('content'):
                        score_source, score_value = str(tag), tag.get('content')

                    if 'NOT_FOUND' in reviews_value:
                        tag = _data.select_one('[itemprop="aggregateRating"] [itemprop="ratingCount"]')

                        if tag and tag.has_attr('content'):
                            reviews_source, reviews_value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'class': 'product-name'})
            if tag:
                title_tag = tag.find('span', {'itemprop': 'name'})
                if title_tag:
                    source, value = str(title_tag), ' '. join(tag.get_text().split())

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'class': 'product-name'})
            if tag:
                title_tag = tag.find('span', {'itemprop': 'brand'})
                if title_tag:
                    source, value = str(title_tag), title_tag.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            src = []

            tag = soup.find('div', {'id': 'product-details-container'})
            if tag:
                description_tag = tag.find('div', {'id': 'product-description'})
                if description_tag:
                    description = description_tag.select_one('span[itemprop="description"]')
                    desc.append(description.get_text().strip())
                    src.append(str(description_tag))
                    # source, value = str(description_tag), description.get_text().strip()
            

            raw = self.get_rich_description_api(soup)
            if raw:
                id = None
                matches = re.search(r'jsonpcar499de(\d+)', raw)
                if matches:
                    id = matches.group(1)
                    wrapper_text = f'jsonpcar499de{id}('
                    text = raw.strip(wrapper_text).strip(')')
                    try:
                        _json = json.loads(text)
                        raw_html = _json.get('html', None)
                        if raw_html:
                            rich_soup = BeautifulSoup(raw_html, 'lxml')
                            tag = self._sanitize(rich_soup)

                            # REMOVE THIS TAGS
                            x_tag = tag.select_one('#flix_selector_3d')
                            if x_tag:
                                x_tag.decompose()
                            x_tag = tag.select_one('#flix-privacy-policy-text')
                            if x_tag:
                                x_tag.decompose()
                            x_tags = tag.select('[class^=fl1xcarousel-control]') 
                            if x_tags:
                                for x in x_tags:
                                    x.decompose()
                                
                            txt = tag.get_text().strip()
                            desc.append(txt)
                            src.append(str(raw))                      
                    except:
                        # do nothing if error occurs, check other source, debug the api if the description is missing
                        pass

            raw_data =  self.downloader.download_product_features_description(self.baseUrl)
            if raw_data:
                strippedStrings = raw_data.replace('\\r', '').replace('\\t', '').replace('\\n', '')
                decodedLxml = self.decodeUTF8(strippedStrings)
                if decodedLxml:
                    features_description_tag = decodedLxml.select('div.flix-std-desc')
                    if features_description_tag:
                        source, descriptions = str(features_description_tag), []
                        for description in features_description_tag:
                            descriptions.append(''.join(description.get_text()))
                        # value = value + ' '.join((descriptions))
                        desc.append(' '.join(descriptions))
                        src.append(str(features_description_tag))

            sku = None
            gtin = self.get_gtin(soup)
            mpn = self.get_mpn(soup)
            brand = self.get_brand(soup)[1].replace('\n','')
            sku_tag = soup.select_one('[itemprop="sku"]')
            if sku_tag:
                sku = sku_tag['content']

            if sku and brand and mpn and gtin:  
                api_url = 'https://ws.cnetcontent.com/cb732d62/script/1a3539ed4b?cpn=%s&mf=%s&pn=%s&upcean=%s&lang=de&market=DE&host=www.alternate.de&nld=1' % (sku,brand,mpn,gtin)
                raw_data = self.downloader.extra_download(api_url)
                if raw_data:
                    _rgx = re.search(r'"html": "(.*)" }\r\n]',raw_data,re.MULTILINE|re.DOTALL)
                    if _rgx:
                        src.append(api_url)
                        _desc = _rgx.group(1).encode('utf-8').decode('unicode-escape')
                        _desc = _desc.encode('latin1').decode('utf-8')
                        raw_soup = BeautifulSoup(_desc, 'lxml')
                        if raw_soup.find('span',{'class': re.compile(r'.*video-duration.*')}):
                            raw_soup.find('span',{'class': re.compile(r'.*video-duration.*')}).extract()
                        description = " ".join(raw_soup.stripped_strings).replace('\\n', '')
                        desc.append(description)

            if desc:
                value = ' + '.join(desc)
                source = ' + '.join(src)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div', {'id': 'product-details-container'})
            if tag:
                specification_tag = tag.find('div', {'id': 'product-details'})
                if specification_tag:
                    specifications = specification_tag.select('tr')
                    value = {}
                    for spec in specifications:
                        values = spec.select('td')
                        if values:
                            value[values[0].text] = values[1].text


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value
        
        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('div', {'id': 'slick-images-big'}) or soup.find('div', {'id': 'swiper-images-big'})
            if tag:
                images = tag.select('img')
                source = str(tag)
                for image in images:
                    if image.has_attr('data-lazy'):
                        value.append(self.WEBSITE + image['data-lazy'])
                    else:
                        value.append('https://'+self.WEBSITE + image['data-src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('div', {'id': 'slick-videos-big'})
            if tag:
                videos = tag.select('video')
                source = str(tag)
                for video in videos:
                    value.append(video['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            parent_tag = soup.find('div', {'id': 'product-top-right'})
            if parent_tag:
                tag = parent_tag.select_one('div.col-12.p-3.text-center')
                if tag:
                    [i.decompose() for i in tag.select('span.ml-1')]
                    source, value = str(tag), " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_data = self.downloader.download_shipping(self.PRODUCT)
            if raw_data:
                data = BeautifulSoup(raw_data, "lxml")
                tag = data.find('span', {'class': ''})    
                if tag:
                    source, value = str(self.API), tag.text.replace("\n", "").strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def decodeUTF8 (self, data):
        encoded = str(data).encode('utf_8') # encode to bytes
        decoded = encoded.decode('unicode_escape')
        stripped = decoded.replace('\\','')
        strippedSoup = BeautifulSoup(stripped, 'lxml')
        return strippedSoup

    def get_rich_description_api(self, soup):

        gtin = self.get_gtin(soup)
        mpn = self.get_mpn(soup)
        brand = self.get_brand(soup)[1].replace('\n','')

        flix_media_api = f'https://media.flixcar.com/delivery/js/inpage/499/de/mpn/{mpn}/ean/{gtin}?&=499&=de&mpn={mpn}&ean={gtin}&brand={brand}&ssl=1&ext=.js'

        try:
            res = self.downloader.download(flix_media_api )

            product_id = self.get_product_id(res)

            url = f'https://media.flixcar.com/delivery/inpage/show/499/de/{product_id}/json?c=jsonpcar499de{product_id}&complimentary=0&type=.html'

            headers = {'accept': '*/*', 'accept-encoding': 'gzip, deflate, br', 'accept-language': 'en-US,en;q=0.9', 'referer': 'https://www.alternate.de/', 'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"', 'sec-ch-ua-mobile': '?0', 'sec-ch-ua-platform': '"Windows"', 'sec-fetch-dest': 'script', 'sec-fetch-mode': 'no-cors', 'sec-fetch-site': 'cross-site', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'}

            result = self.downloader.download(url, headers=headers)

            if result:
                return result

        except:
            pass

        return None

    def get_gtin(self, soup):

        tag = soup.select_one('meta[itemprop="gtin8"]')
        if tag and tag.has_attr('content'):
            return tag['content']

        tag = soup.select_one('#product-details table tr:contains(EAN) td.c4')
        if tag:
            return tag.get_text().strip()

    def get_mpn(self, soup):
        tag = soup.select_one('#product-details table tr:contains(Hersteller-Nr) td.c4')
        if tag:
            return tag.get_text().strip()

    def get_product_id(self, text):
        matches = re.search(r'#flixinpage_(\d+)"', text) \
                or re.search(r"product:'(\d+)'", text) \
                or re.search(r"flixJsCallbacks.pid='(\d+)'", text)

        if matches:
            return matches.group(1)

    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag
         