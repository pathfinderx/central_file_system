import json
import logging
import re
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class OttoDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.otto.de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        _json_data = json.loads(raw_data) 
        result = get_result_base_template()
        soup = BeautifulSoup(_json_data['result_text'], "lxml")
        url = _json_data['url']

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, url)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, url)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup,url)   

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        return result

    #GET JSON
    def get_json(self, soup):
        try:
            tag = soup.find('script', text=re.compile(r'.*window\.__PRELOADED_STATE__'))
            _json = {}
            if tag:
                rgx = re.search(r'\{.*\}', tag.get_text())
                _json = json.loads(rgx.group()) if rgx else {}

            return str(tag), _json
                
        except Exception as e:
            print(e)
            

    # GET PRICE
    def get_price(self, soup, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            res = None
            headers = {
                "accept": "*/*",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                "referer": url,
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36",
                "x-requested-with": "XMLHttpRequest"
            }
            #
            try:
                variation_id = url.split('variationId=')[-1]
                if 'variation' in variation_id:
                    variation_id = variation_id.split('=')[-1]
                product_id = url.split('#')[-2].split('/')[-2].split('-')[-1]
                api = 'https://www.otto.de/p/views/detailviewInlineJSON/{}?variationId={}'.format(product_id, variation_id)
                res = self.downloader.download(api, headers=headers)
            except:
                #No product variation provided
                pass # Exception triggers when request returns 404 https://prnt.sc/10ht257
                # return source, value

            tag = soup.select_one('[itemprop="price"]')
            tag_json = soup.select_one('#productDataJson')
            _json = {} 

            try:
                if res:
                    _json = json.loads(res, strict=False)
                    if 'result_text' in _json:
                        _json = json.loads(_json['result_text'])
                        
                        source = str(url)
                elif tag_json:
                    _json = json.loads(tag_json.text, strict=False)
                    source = str(tag_json)

                if _json:
                    if 'variations' in _json:
                        if variation_id not in _json['variations']:
                            for i in _json['variations'].keys():
                                variation_id = i
                                break
                        
                        if 'displayPrice' in _json['variations'][variation_id]:
                            if 'techPriceAmount' in _json['variations'][variation_id]['displayPrice']:
                                value = str(_json['variations'][variation_id]['displayPrice']['techPriceAmount'])
                                    
                elif tag and tag.has_attr('href'):
                    source, value = str(tag), tag.get('href')
                
            except:
                if tag and tag.has_attr('href'):
                    source, value = str(tag), tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="priceCurrency"]')

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            res = None
            headers = {
                "accept": "*/*",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                "referer": url,
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36",
                "x-requested-with": "XMLHttpRequest"
            }
            #
            try:
                variation_id = url.split('variationId=')[-1]
                if 'variation' in variation_id:
                    variation_id = variation_id.split('=')[-1]
                product_id = url.split('#')[-2].split('/')[-2].split('-')[-1]
                api = 'https://www.otto.de/p/views/detailviewInlineJSON/{}?variationId={}'.format(product_id, variation_id)
                res = self.downloader.download(api, headers=headers)
            except:
                #No product variation provided
                pass # Exception triggers when request returns 404 https://prnt.sc/10ht257
                # return source, value

            tag = soup.select_one('[itemprop="price"]')
            tag_json = soup.select_one('#productDataJson')
            _json = {} 

            try:
                if res:
                    _json = json.loads(res, strict=False)
                    if 'result_text' in _json:
                        _json = json.loads(_json['result_text'])
                        
                        source = str(url)
                elif tag_json:
                    _json = json.loads(tag_json.text, strict=False)
                    source = str(tag_json)

                if _json:
                    if 'variations' in _json:
                        if variation_id not in _json['variations']:
                            for i in _json['variations'].keys():
                                variation_id = i
                                break
                        
                        if 'availability' in _json['variations'][variation_id]:
                            if 'status' in _json['variations'][variation_id]['availability']:
                                value = str(_json['variations'][variation_id]['availability']['status'])
                                    
                elif tag and tag.has_attr('href'):
                    source, value = str(tag), tag.get('href')
                
            except:
                if tag and tag.has_attr('href'):
                    source, value = str(tag), tag.get('href')
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = soup.select_one('[itemprop="ratingValue"]')
            tag_review = soup.select_one('[itemprop="reviewCount"]')

            if tag_score and tag_review:
                if tag_score.has_attr('content') and tag_score.has_attr('content'):
                    reviews_source, score_source = str(tag_review),str(tag_score)
                    reviews_value, score_value = tag_review.get('content'),\
                                                tag_score.get('content')
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1[itemprop="name"]')
            if tag:
                source = str(tag)
                value = tag.get_text().strip()
            else:
                tag = soup.select_one('[property="og:title"]')

                if tag and tag.has_attr('content'):
                    # source, value = str(tag), tag.get('content')
                    source = str(tag)
                    title = tag.get('content').split(' online kaufen | OTTO')[0]
                    value = title
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            tag = soup.select_one('[itemprop="brand"]')

            if tag:
                source, value = str(tag), tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            src = []
            # tag = soup.select_one('.prd_unorderedList') or \
            #     soup.select_one('div#description div')
            # if tag:
            #     src.append(str(tag))
            #     txt = ' '.join(tag.stripped_strings)
            #     if not txt == '':
            #         desc.append(txt)
                    

            tag = soup.find('div',{'class':'prd_detailShortInfo__wrapper'})
            if tag:
                src.append(str(tag))
                txt = ' '.join(tag.stripped_strings)
                if not txt == '':
                    desc.append(txt)
                
            tag = soup.find('ul',{'class':'prd_sellingPoints__list pl_copy100'})
            if tag:
                src.append(str(tag))
                txt = ' '.join(tag.stripped_strings)
                if not txt == '':
                    desc.append(txt)
            
            # tag = soup.select_one('#description')
            headDescription, bodyDescription = soup.select('h1[data-qa="rc_teaser-headline"]'), soup.select('div[data-qa="rc_image-teaser-container"]')
            if headDescription or bodyDescription:
                # src.append(str(tag))
                headText, bodyText = ' '.join([string.get_text() for string in headDescription]).replace('\n',''), ' '.join([string.get_text() for string in bodyDescription]).replace('\n','')
                if headText or bodyText:
                    text = headText + ' ' + bodyText
                    desc.append(text) if text else None
                # desc.appned()

                # src.append(str(tag))
                # [i.decompose() for i in tag.select('style')]
                # [i.decompose() for i in tag.select('script')]
                # txt = ' '.join(tag.stripped_strings)
                # if txt:
                #     desc.append(txt)
            
            # this is specs not part of description
            # tag = soup.select_one('.js_prd_moreBox__content')
            # if tag:
            #     src.append(str(tag))
            #     [i.decompose() for i in tag.select('style')]
            #     [i.decompose() for i in tag.select('script')]
            #     txt = ' '.join(tag.stripped_strings)
            #     if not txt == '':
            #         desc.append(txt)
            
            # tags = soup.select('.rc_teaser__text')
            # if tags:
            #     for tag in tags:
            #         txt = ' '.join(tag.stripped_strings)
            #         if not txt == '':
            #             desc.append(txt)
                        
            if desc:
                source = " + ".join(src)
                value = " + ".join(desc)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('[data-scrollto=".js_prd_details"] tr')

            if tags:
                source = str(tags)
                for i in tags:
                    k = i.select_one('td.left')
                    v = k.find_next_sibling().get_text().strip()

                    value[k.get_text().strip()] = v


                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    # GET IMAGES
    def _get_image_urls(self, soup, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            #Not Working
            # tags = soup.select('.js_prd_swiper-slide')
            # source = str(tags) if tags else source
            # value = [_.get('data-image-url') for _ in tags if _.has_attr('data-image-url')]
            tags = soup.select('#js_prd_verticalImageControl img')
            if tags:
                source = str(tags)
                for img in tags:
                    if img.has_attr('src') and not img.get('class'):
                        image_url = img.get('src') + '&w=2500&h=1406'
                        value.append(image_url)

            if not value:
                tags = soup.find('script',attrs={'id':'productDataJson'}, type = 'application/json')

                pgsrc_varid = soup.find('div',{'class':'js_exactag'}).attrs.get('data-variationid')
                
                if '?variationId' in url or '#variationId' in url:
                    varid = re.split(r'\?|#',url)[-1].split('=')[1]
                else:
                    varid = pgsrc_varid

                if tags and varid:
                    _json = json.loads(tags.get_text())
                    if  _json and 'variations' in _json.keys():
                        if varid not in _json['variations']:    #if variationId in url not found in variations
                            varid = pgsrc_varid

                        if 'images' in _json['variations'][varid]:
                            source = str(tags)
                            for img in _json['variations'][varid]['images']:
                                value.append('https://i.otto.de/i/otto/{}'.format(img['id']))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
       
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tags = soup.select('[itemtype="http://schema.org/VideoObject"] meta[itemprop="contentURL"]')

            if tags:
                source = str(tags)
                for vid in tags:
                    if vid.has_attr('content'):
                        video_url = vid.get('content')
                        value.append(video_url)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            sku_tag = soup.select_one('[class="exactag js_exactag"]')
            if sku_tag and sku_tag.has_attr('data-variationid') or sku_tag.has_attr('data-variationId'):
                sku = sku_tag.get('data-variationid') or sku_tag.get('data-variationId')
            tag_json = soup.select_one('#productDataJson')
            if tag_json:
                _json = json.loads(tag_json.text, strict=False)
                if _json:
                    if 'variations' in _json.keys() and sku in _json['variations'] and 'availability' in _json['variations'][sku] and 'displayName' in _json['variations'][sku]['availability']:
                        value = _json['variations'][sku]['availability']['displayName']
                        source = _json['variations'][sku]['availability']
                        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value




