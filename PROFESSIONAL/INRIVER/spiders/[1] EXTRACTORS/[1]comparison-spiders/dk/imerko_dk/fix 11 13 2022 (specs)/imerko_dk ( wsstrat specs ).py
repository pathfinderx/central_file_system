import re
import json
import logging

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ImercoDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.imerco.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.__state = {}
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        self.__state["raw_data"] = raw_data
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Get script data for page
        # self.__state['product_data'] = self.get_script_source(soup)
        self.__state['product_data'] = self.get_next_data(soup)
      

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result
    
    def get_script_source(self, soup):
        data = {}
        scripts = soup.select('script')
        for script in scripts:
            if script.string:
                text_data = re.search(r'{.+Price.+}', script.string)
                if text_data:
                    text_data = text_data[0]
                    text_data = text_data.replace('{\\"', '{"')
                    text_data = text_data.replace('\\":', '":')
                    text_data = text_data.replace(':\\"', ':"')
                    text_data = text_data.replace(',\\"', ',"')
                    text_data = text_data.replace('\\",', '",')
                    text_data = text_data.replace('\\"}', '"}')
                    text_data = text_data.replace('\\"]', '"]')
                    text_data = text_data.replace('[\\"', '["')
                    data['value'] = json.loads(text_data)
                    data['source'] = script
                    break
        return data
    
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)

                if (product and 
                'c_activepromo' in product and
                'promotiontype' in product['c_activepromo']):
                
                    if ('c_effectivePrice' in product and 
                        product['c_activepromo']['promotiontype'] in ['Julepris']): # Julepris = Christmas Price

                        value = str(product['c_effectivePrice'])
                        source = str(_json)

                    else:
                        if ('price' in product):
                            value = str(product['price'])
                            source = str(_json)

                elif 'pricePerUnit' in product:
                    value = str(product['pricePerUnit'])
                    source = str(_json)

            # if self.__state["product_data"]["value"]["Price"]["PriceText"] == "Medlemspris":
            #     value = str(self.__state["product_data"]["value"]["Price"]["BeforeValue"])
            # else:
            #     value = str(self.__state["product_data"]["value"]["Price"]["PriceValue"])
                
            # source = str(self.__state["product_data"]["source"])
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = "DKK"

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # value = self.__state['product_data']['value']['SchemaOrgData']['offers']['availability']
            # source = str(self.__state['product_data']['source'])
            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)

                if 'inventory' in product:
                    stock = product['inventory']
                    if 'orderable' in stock:
                        value = str(stock['orderable'])
                        source = str(_json)

                if value == 'NOT_FOUND' and 'c_in_stock' in product:
                    value = str(product['c_in_stock'])
                    source = str(_json)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        rating_map = {
            "empty": 0,
            "ten": 10,
            "twenty": 20,
            "thirty": 30,
            "fourty": 40,
            "fifty": 50,
            "sixty": 60,
            "seventy": 70,
            "eighty": 80,
            "ninety": 90,
            "full": 100,
        }

        try:
            # current new PDP has no observe implementation of ratings
            pass
            # tag_score = soup.select_one('yotpo-stars')
            # tag_review = soup.select_one('span.product__rating__amount')

            # if tag_score and tag_review:
            #     if tag_score.has_attr('star-count'):
            #         reviews_source, score_source = str(tag_review), str(tag_score)
            #         reviews_value, score_value = str(tag_review.text[1:-1]), str(tag_score.get('star-count'))
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:

            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)
                if 'name' in product:
                    name = product['name']
                    if 'c_synopsis' in product:
                        name = '%s %s' % (name, product['c_synopsis'])
                    value = str(name)
                    source = str(_json)
            # # https://detailsupport.atlassian.net/browse/DCS-1512
            # tag = soup.select_one('[name="name"]')

            # if tag and tag.has_attr('content'):
            #     source, value = str(tag), str(tag['content'])
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)
                if 'brand' in product:
                    value = str(product['brand'])
                    source = str(_json)

            # value = self.__state['product_data']['value']['Brand']
            # source = str(self.__state['product_data']['source'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)
                if 'shortDescription' in product:
                    desc.append(product['shortDescription'])

                if 'c_ProductUsp' in product:
                    desc.append(product['c_ProductUsp'])

                if 'longDescription' in product:
                    txt = product['longDescription']
                    new_soup = BeautifulSoup(txt, 'lxml')
                    txt = ' '.join(new_soup.stripped_strings)
                    desc.append(txt)

                if desc:
                    value = ' + '.join(desc)
                    source = str(_json)
            # desc_soup = soup.select_one('product-description')
            # if desc_soup:
            #     value = " ".join(desc_soup.stripped_strings)
            #     source = str(desc_soup)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            _json = self.__state['product_data']

            if (_json and 
                'page' in _json and 
                'data' in _json['page']):

                product = _json['page']['data'].get('product', None)

                if 'c_specifications' in product:
                    _spec_json = json.loads(product['c_specifications'])

                    if _spec_json:
                        value = _spec_json
                        source = str(_json)

                else: # if 'c_specifications' not found and dependent to 'translations > pdp' structure
                    pdp_structure = _json.get('translations').get('pdp')

                    if (pdp_structure and product):
                        pdp_specs = list() 

                        for key,val in pdp_structure.items(): # extract pdp equivalent names
                            if (str(key).find('spec.') > -1):
                                pdp_specs.append({key:val})

                        if (pdp_specs):
                            for key, val in product.items():
                                if (key in [next(iter(pdp_key.keys())).split('spec.')[-1] for pdp_key in pdp_specs]):
                                    for iteration in pdp_specs:
                                        if( f'spec.{key}' == next(iter(iteration.keys())) ):
                                            value.update({next(iter(iteration.values())):val})
                            
                        if value:
                            source = str(pdp_structure)

            # specs = soup.select('product-specifications')
            # if len(specs) == 2:
            #     specs = specs[1]

            #     for item in json.loads(specs['specifications']):
            #         value[item['Name']] = item["Values"][0]["Value"]

            #     source = str(specs)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []
        
        try:
            PLACEHOLDER_IMG = '3990_00e61fd8-f3bb-45e5-8405-9d01655a7c9b'

            tags = soup.select('div button.css-1oalzhq div[style="position:relative"] div img')
            if tags:
                for img in tags:
                    src = img['src'].split('65/')[-1]
                    if PLACEHOLDER_IMG in src:
                        continue
                    value.append(src)

                if value: 
                    source = str(tags)
            # this could does not provide accurate photos
            # _json = self.__state['product_data']
            # if _json and 'page' in _json and 'data' in _json['page']:
            #     product = _json['page']['data'].get('product', None)
            #     if 'imageGroups' in product:
            #         if product['imageGroups']:
            #             images = product['imageGroups'][0]['images']
            #             if images:
            #                 for img in images:
            #                     value.append(img['link'])
            #                 source = str(_json)
            

            # for image in self.__state['product_data']['value']['Images']:
            #     value.append(image['Url'])
            # source = str(self.__state['product_data']['source'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value
            
        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  
        
        try:
            pass
            # no observe videos on new PDP
            # for video in self.__state['product_data']['value']['Videos']:
            #     value.append(video['VideoUrl'])
            # source = str(self.__state['product_data']['source'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value
            
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self.__state['product_data']
            if _json and 'page' in _json and 'data' in _json['page']:
                product = _json['page']['data'].get('product', None)

                delivery_days = None
                cost = None
                translations = _json.get('translations', None)
                if translations:
                    basket = translations.get('basket', None)
                    if basket and 'deliveryDays' in basket:
                        delivery_days = basket['deliveryDays']

                if 'c_minimumShippingCost' in product:
                    cost = product['c_minimumShippingCost']

                if cost and delivery_days:
                    fee_label = 'Fri fragt'
                    if int(cost) > 0:
                        fee_label = f'Fragt fra {cost}'

                    value = f'{fee_label} — {delivery_days}'


            # value = self.__state['product_data']['value']['StockLabel'] or self.__state['product_data']['value']['DeliveryTime']
            # source = str(self.__state['product_data']['source'])

            # id = self.__state['product_data']['value']['ProductUrl'].split('?id=')[-1]
            # if id == '100386430':
            #     stock =  self.__state['product_data']['value']['SchemaOrgData']['offers']['availability']
            #     if stock == 'http://schema.org/OutOfStock':
            #         value = 'Ikke på lager online'
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value        
        
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        
        try:
            pass
            # value = self.__state['product_data']['value']['DisplayShippingPriceLabel']
            # source = str(self.__state['product_data']['source'])
                
        except Exception as e:
            self.logger.exception(e)

        # website doesn't support shipping
        return source, value

    def get_next_data(self, soup):
        tag = soup.select_one('#__NEXT_DATA__')
        if tag:
            try:
                txt = tag.get_text().strip()
                _json = json.loads(txt)
                if _json and 'props' in _json:
                    return _json['props'].get('pageProps', {})

            except:
                pass
        return {}
