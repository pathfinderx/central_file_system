import re

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ImercoDkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            # headers = {
            #     "user-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "\
            #         "Chrome/90.0.4430.212 Safari/537.36",
            #     "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,"\
            #         "image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            #     "accept-encoding":"gzip, deflate, br",
            #     "accept-language":"en-US,en;q=0.9"

            # }
            headers = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 'accept-encoding': 'gzip, deflate', 'accept-language': 'en-US,en;q=0.9', 'cache-control': 'max-age=0', 'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"', 'sec-ch-ua-mobile': '?0', 'sec-ch-ua-platform': '"Windows"', 'sec-fetch-dest': 'document', 'sec-fetch-mode': 'navigate', 'sec-fetch-site': 'same-origin', 'sec-fetch-user': '?1', 'upgrade-insecure-requests': '1', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                # for item in res.history:
                #     if item.status_code in [302, 307]:
                #         is_redirect = True
                #         status_code = item.status_code
                #         break

                if not is_redirect and 'name="mobile-web-app-capable" content="yes"' in res.text \
                    and self.is_pdp(res.text):
                    return res.text
            
            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def is_pdp(self, raw):
        matches = re.search(r'"@type":\s?"Product"', raw)
        if matches:
            return True
        return False