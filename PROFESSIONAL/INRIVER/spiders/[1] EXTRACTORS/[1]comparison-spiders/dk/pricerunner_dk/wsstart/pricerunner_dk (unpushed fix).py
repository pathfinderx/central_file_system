import json
import logging
import re, copy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template, get_result_3p_template


class PricerunnerDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.pricerunner.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        self.api_3p_items = self.get_3p_items_in_api(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls - no video indicated
        # result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery and shipping
        result["delivery"]["source"], result["delivery"]["value"], \
            result["shipping"]["source"], result["shipping"]["value"] = self._get_delivery_and_shipping(soup)

        result['third_parties'] = self.get_third_parties(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #tag = soup.find('meta', {'itemprop': 'price'})
            tag = soup.find('meta', {'itemprop': 'lowPrice'})
            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content
            else:
                tag = soup.find('meta', {'itemprop': 'price'})
                if tag and tag.has_attr('content'):
                    source = str(tag)
                    content = tag["content"]
                    if content:
                        value = content
                else:
                    content = None
                    tag = soup.select_one('script#initial_payload')
                    if tag:
                        _json = json.loads(tag.get_text().strip())

                        if "__INITIAL_STATE__" in _json.keys():
                            id_tag = soup.select_one('#product-body')
                            if id_tag and id_tag.has_attr('data-productid'):
                                _id = id_tag.get('data-productid')
                                if _id in _json['__INITIAL_STATE__']['productList'].keys():
                                    if 'merchantOffers' in _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers']) >= 1:
                                        if 'price' in _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers'][0].keys():
                                            source = str(tag)
                                            value = _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers'][0]['price']['amount']
                                    elif 'merchantOffers' in _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers']) >= 1:
                                        if 'price' in _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers'][0].keys():
                                            source = str(tag)
                                            value = _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers'][0]['price']['amount']
                                elif 'pl' in _json['__INITIAL_STATE__']['productList'].keys():
                                    if 'merchantOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers']) >= 1:
                                        if 'price' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers'][0].keys():
                                            source = str(tag)
                                            value = _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers'][0]['price']['amount']
                                    elif 'merchantOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers']) >= 1:
                                        if 'price' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers'][0].keys():
                                            source = str(tag)
                                            value = _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers'][0]['price']['amount']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'priceCurrency'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('script#initial_payload')
            content = None
            if tag:
                _json = json.loads(tag.get_text().strip())

                if "__INITIAL_STATE__" in _json.keys():
                    if 'productReducers' in _json['__INITIAL_STATE__'].keys():
                        if 'offers' in _json['__INITIAL_STATE__']['productReducers']['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers']) >= 1:
                            if 'stock' in _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0].keys():
                                source = str(tag)
                                content = _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0]['stock']
                        elif 'offers' in _json['__INITIAL_STATE__']['productReducers']['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers']) >= 1:
                            if 'stock' in _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0].keys():
                                source = str(tag)
                                content = _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0]['stock']
                    elif 'productList' in _json['__INITIAL_STATE__'].keys():
                        id_tag = soup.select_one('#product-body')
                        if id_tag and id_tag.has_attr('data-productid'):
                            _id = id_tag.get('data-productid')

                            if _id in _json['__INITIAL_STATE__']['productList'].keys():
                                if 'merchantOffers' in _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers']) >= 1:
                                    if 'stockStatus' in _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers'][0]['offers'][0].keys():
                                        source = str(tag)
                                        content = _json['__INITIAL_STATE__']['productList'][_id]['nationalOffers']['merchantOffers'][0]['offers'][0]['stockStatus']
                                elif 'merchantOffers' in _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers']) >= 1:
                                    if 'stockStatus' in _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers'][0]['offers'][0].keys():
                                        source = str(tag)
                                        content = _json['__INITIAL_STATE__']['productList'][_id]['internationalOffers']['merchantOffers'][0]['offers'][0]['stockStatus']
                            elif 'pl' in _json['__INITIAL_STATE__']['productList'].keys() and _id in _json['__INITIAL_STATE__']['productList']['pl']:
                                if 'nationalOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id] and 'internationalOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id]:
                                    if 'merchantOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers']) >= 1:
                                        if 'stockStatus' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers'][0]['offers'][0].keys():
                                            source = str(tag)
                                            content = _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOffers']['merchantOffers'][0]['offers'][0]['stockStatus']
                                    elif 'merchantOffers' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers']) >= 1:
                                        if 'stockStatus' in _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers'][0]['offers'][0].keys():
                                            source = str(tag)
                                            content = _json['__INITIAL_STATE__']['productList']['pl'][_id]['internationalOffers']['merchantOffers'][0]['offers'][0]['stockStatus']
                                else:
                                    if 'nationalOfferCount' in _json['__INITIAL_STATE__']['productList']['pl'][_id] and _json['__INITIAL_STATE__']['productList']['pl'][_id]['nationalOfferCount']:
                                        source = str(tag)
                                        content = _json['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][5]['state']['data']['filteredOfferList']['merchantOffers'][0]['offers'][0]['stockStatus']
                            else:
                                offer = {}
                                offer_list = {}
                                for item in _json['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries']:
                                    if 'productlist-initial' in item['queryKey']:
                                        offer = item
                                    if 'productlist-offers' in item['queryKey']:
                                        offer_list = item
                                    
                                if 'nationalOfferCount' in offer['state']['data'] and offer['state']['data']['nationalOfferCount']:
                                    source = str(tag)
                                    content = offer_list['state']['data']['filteredOfferList']['merchantOffers'][0]['offers'][0]['stockStatus']
                                else:
                                    if 'internationalOfferCount' in offer['state']['data'] and offer['state']['data']['internationalOfferCount']:
                                        source = str(tag)
                                        if offer_list['state']['data']['filteredOfferList']['merchantOffers']:
                                            content = offer_list['state']['data']['filteredOfferList']['merchantOffers'][0]['offers'][0]['stockStatus']
                                        else:
                                            if self.api_3p_items:
                                                content = self.api_3p_items['filteredOfferList']['merchantOffers'][0]['offers'][0]['stockStatus']
                                    else:
                                        content = 'outofstock'
                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews
            tag = soup.find("meta", {"itemprop":"ratingCount"})

            if tag and tag.has_attr('content'):
                reviews_source = str(tag)
                content = tag["content"]

                if content:
                    reviews_value = content

            # Ratings
            tag = soup.find("meta", {"itemprop":"ratingValue"})

            if tag and tag.has_attr('content'):
                score_source = str(tag)
                content = tag["content"]

                if content:
                    score_value = content

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('h1', {'itemprop': 'name'})

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            content = None
            tag = soup.select_one('script#initial_payload')
            if tag:
                _json = json.loads(tag.get_text().strip())

                if "__INITIAL_STATE__" in _json.keys():
                    if 'productReducers' in _json['__INITIAL_STATE__'].keys():
                        if 'categoryBrandFilter' in _json['__INITIAL_STATE__']['productReducers'].keys():
                            if 'brandName' in _json['__INITIAL_STATE__']['productReducers']['categoryBrandFilter'].keys():
                                source = str(tag)
                                content = _json['__INITIAL_STATE__']['productReducers']['categoryBrandFilter']['brandName']
                    elif 'productList' in _json['__INITIAL_STATE__'].keys():
                        id_tag = soup.select_one('#product-body')
                        if id_tag and id_tag.has_attr('data-productid'):
                            _id = id_tag.get('data-productid')

                            if _id in _json['__INITIAL_STATE__']['productList'].keys():
                                if 'brand' in _json['__INITIAL_STATE__']['productList'][_id].keys():
                                    source = str(tag)
                                    content = _json['__INITIAL_STATE__']['productList'][_id]['brand']['name']
                            elif 'pl' in _json['__INITIAL_STATE__']['productList'].keys() and _id in _json['__INITIAL_STATE__']['productList']['pl']:
                                if 'brand' in _json['__INITIAL_STATE__']['productList']['pl'][_id].keys():
                                    source = str(tag)
                                    content = _json['__INITIAL_STATE__']['productList']['pl'][_id]['brand']['name']
                            else:
                                if 'brand' in _json['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][4]['state']['data']:
                                    source = str(tag)
                                    content = _json['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][4]['state']['data']['brand']['name']
                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src = []
            desc = []
            tag = soup.find(itemprop="description")

            if tag and tag.has_attr('content'):
                src.append(str(tag))
                desc.append(tag.get('content'))
            else:
                src.append(str(tag))
                desc.append(tag.get_text().strip())
            
            tag = soup.select_one('#produktinformation')
            if tag and tag.parent: 
                desc_tag = tag.parent.select_one('div p')
                if desc_tag:
                    src.append(str(tag))
                    desc.append(desc_tag.get_text().strip())
            
            if desc:
                source = " + ".join(src)
                value = " + ".join(desc)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('div._1L2Rrze0Pm')
            if tags:
                source =str(tags)
                value = self._get_initial_specs(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('button img._185trAgM9i')

            if tags:
                source = str(tags)
                for img in tags:
                    if img.has_attr('src'):
                        image_url = img.get('src')
                        value.append(image_url.replace('60x60', '640x640'))

            if not value:
                tags = soup.select('div.Co4lKnnkJY div._1c4rs8_fVQ')
                if tags:
                    source = str(tags)
                    for tag in tags:
                        img = tag.find('img')
                        if img and img.has_attr('src'):
                            value.append(img.get('src').replace('40x40','640x640'))

            if not value:
                tag = soup.select_one('script[id="initial_payload"]')
                if tag:
                    rgx = re.search('\"images\":(.{.*}.),"ribbon', tag.get_text())
                    if rgx:
                        images = json.loads(rgx.group(1))
                        for img in images:
                            if "path" in img:
                                img = img['path']
                                if img.startswith('http'):
                                    value.append(img)
                                else:
                                    value.append('https://{}{}'.format(self.__class__.WEBSITE, img))
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
    
    def _get_delivery_and_shipping(self, soup):
        delivery_source = Defaults.GENERIC_NOT_FOUND.value
        delivery_value = Defaults.GENERIC_NOT_FOUND.value
        shipping_source = Defaults.GENERIC_NOT_FOUND.value
        shipping_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('script#initial_payload')
            if tag:
                _json = json.loads(tag.get_text().strip())

                if "__INITIAL_STATE__" in _json.keys():
                    if 'productReducers' in _json['__INITIAL_STATE__'].keys():
                        if 'offers' in _json['__INITIAL_STATE__']['productReducers']['nationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers']) >= 1:
                            if 'delivery' in _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0].keys():
                                delivery_source = str(tag)
                                delivery_value = _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0]['delivery']
                            
                            if 'shipping' in _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0].keys():
                                shipping_source = str(tag)
                                shipping_value = _json['__INITIAL_STATE__']['productReducers']['nationalOffers']['offers'][0]['shipping']
                            
                        elif 'offers' in _json['__INITIAL_STATE__']['productReducers']['internationalOffers'].keys() and len(_json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers']) >= 1:
                            if 'delivery' in _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0].keys():
                                delivery_source = str(tag)
                                delivery_value = _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0]['delivery']
                            
                            if 'shipping' in _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0].keys():
                                shipping_source = str(tag)
                                shipping_value = _json['__INITIAL_STATE__']['productReducers']['internationalOffers']['offers'][0]['shipping']

        except Exception as e:
            self.logger.exception(e)
            delivery_value = FailureMessages.DELIVERY_EXTRACTION.value
            shipping_value = FailureMessages.DELIVERY_EXTRACTION.value

        return delivery_source, delivery_value, shipping_source, shipping_value

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            tag = nth.select_one('div p')

            if tag:
                key = tag.get_text().strip()

                tag = nth.select_one('.rzuL-61SnU.yfeimw92ho._3aGENKRWel._3_beFIN3FX')

                if tag:
                    data = tag.get_text().strip()
            
                result[key] = data

        return result

    def __get_script(self, soup):
        script_tag = soup.select_one('script#initial_payload')
        return script_tag

    def __get_3p_merchants(self, soup, payload):
        _id = None
        items = []
        id_tag = soup.select_one('#product-body')
        if id_tag and id_tag.has_attr('data-productid'):
            _id = id_tag.get('data-productid')

        if payload and '__INITIAL_STATE__' in payload and \
            'productList' in payload['__INITIAL_STATE__'] and \
            'currentProductId' in payload['__INITIAL_STATE__']['productList'].keys():

            product_id = payload['__INITIAL_STATE__']['productList']['currentProductId']
            
            offer_count = payload['__INITIAL_STATE__']['productList'][product_id]['nationalOffers']['offerCount']

            if offer_count > 0:
                items = payload['__INITIAL_STATE__']['productList'][product_id]['nationalOffers']['merchants']
            else:
                offer_count = payload['__INITIAL_STATE__']['productList'][product_id]['internationalOffers']['offerCount']
                
                if offer_count > 0:
                    items = payload['__INITIAL_STATE__']['productList'][product_id]['internationalOffers']['merchants']
                else:
                    items = []
        elif payload and '__INITIAL_STATE__' in payload and \
            'productList' in payload['__INITIAL_STATE__'] and \
            'pl' in payload['__INITIAL_STATE__']['productList'].keys():
            if _id:
                product_id = _id
                if product_id in payload['__INITIAL_STATE__']['productList']['pl']:
                    if 'nationalOffers' in payload['__INITIAL_STATE__']['productList']['pl'][_id] and 'internationalOffers' in payload['__INITIAL_STATE__']['productList']['pl'][_id]:
                        offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOffers']['offerCount']

                        if offer_count > 0:
                            items = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOffers']['merchants']
                        else:
                            offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['internationalOffers']['offerCount']
                            
                            if offer_count > 0:
                                items = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['internationalOffers']['merchants']
                            else:
                                items = []
                    else:
                        offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOfferCount']
                        if offer_count > 0:
                            items = payload['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][5]['state']['data']['filteredOfferList']['merchants']
                        else:
                            items = []
                else:
                    offer = {}
                    offer_list = {}
                    for item in payload['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries']:
                        if 'productlist-initial' in item['queryKey']:
                            offer = item
                        if 'productlist-offers' in item['queryKey']:
                            offer_list = item
                    if offer:
                        offer_count = offer['state']['data']['nationalOfferCount']
                        if offer_count > 0:
                            items = offer_list['state']['data']['filteredOfferList']['merchants']
                        else:
                            offer_count = offer['state']['data']['internationalOfferCount']
                            if offer_count > 0:
                                items = offer_list['state']['data']['filteredOfferList']['merchants']
                                if not items:
                                    if self.api_3p_items:
                                        items = self.api_3p_items['filteredOfferList']['merchants']
                            else:
                                items = []
                    else:
                        items = []
        return items

    def get_3p_items(self, soup, payload):
        _id = None
        items = []
        id_tag = soup.select_one('#product-body')
        if id_tag and id_tag.has_attr('data-productid'):
            _id = id_tag.get('data-productid')

        if payload and '__INITIAL_STATE__' in payload and \
            'productList' in payload['__INITIAL_STATE__'] and \
            'currentProductId' in payload['__INITIAL_STATE__']['productList'].keys():
        
            product_id = payload['__INITIAL_STATE__']['productList']['currentProductId']
            offer_count = payload['__INITIAL_STATE__']['productList'][product_id]['nationalOffers']['offerCount']

            if offer_count > 0:
                items = payload['__INITIAL_STATE__']['productList'][product_id]['nationalOffers']['merchantOffers']
            else:
                offer_count = payload['__INITIAL_STATE__']['productList'][product_id]['internationalOffers']['offerCount']
                
                if offer_count > 0:
                    items = payload['__INITIAL_STATE__']['productList'][product_id]['internationalOffers']['merchantOffers']
                else:
                    items = []
        elif payload and '__INITIAL_STATE__' in payload and \
            'productList' in payload['__INITIAL_STATE__'] and \
            'pl' in payload['__INITIAL_STATE__']['productList'].keys():
            
            if _id:
                product_id = _id
                if product_id in payload['__INITIAL_STATE__']['productList']['pl']:
                    if 'nationalOffers' in payload['__INITIAL_STATE__']['productList']['pl'][_id] and 'internationalOffers' in payload['__INITIAL_STATE__']['productList']['pl'][_id]:
                        offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOffers']['offerCount']

                        if offer_count > 0:
                            items = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOffers']['merchantOffers']
                        else:
                            offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['internationalOffers']['offerCount']
                            
                            if offer_count > 0:
                                items = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['internationalOffers']['merchantOffers']
                            else:
                                items = []
                    else:
                        offer_count = payload['__INITIAL_STATE__']['productList']['pl'][product_id]['nationalOfferCount']
                        if offer_count > 0:
                            items = payload['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][5]['state']['data']['filteredOfferList']['merchantOffers']
                        else:
                            items = []
                            
                else:
                    offer = {}
                    offer_list = {}
                    for item in payload['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries']:
                        if 'productlist-initial' in item['queryKey']:
                            offer = item
                        if 'productlist-offers' in item['queryKey']:
                            offer_list = item
                    if offer:
                        offer_count = offer['state']['data']['nationalOfferCount']
                        if offer_count > 0:
                            items = offer_list['state']['data']['filteredOfferList']['merchantOffers']
                        else:
                            offer_count = offer['state']['data']['internationalOfferCount']
                            if offer_count > 0:
                                items = offer_list['state']['data']['filteredOfferList']['merchantOffers']
                                if not items:
                                    if self.api_3p_items:
                                        items = self.api_3p_items['filteredOfferList']['merchantOffers']
                            else:
                                items = []
                    else:
                        items = []
                   
                    

        return items

    def get_third_parties(self, soup):
        template = get_result_3p_template()

        # get initial payload json
        script_tag = self.__get_script(soup)
        
        # create a sharable instance variable for the json
        self.__initial_payload = json.loads(script_tag.contents[0])

        # create a sharable instance variable of merchants
        self.__merchants = self.__get_3p_merchants(soup, self.__initial_payload)

        items = self.get_3p_items(soup, self.__initial_payload)
        results = []
        rank_3p = 1

        for item in items:
            result = copy.deepcopy(template)
            result['rank'] = str(rank_3p)
            result['price']['source'], result['price']['value'] = self.get_3p_price(item)
            result['currency']['source'], result['currency']['value'] = self.get_3p_currency(item)
            result['retailer_name']['source'], result['retailer_name']['value'], result['is_direct_seller'] = self.get_3p_retailer(item)
            result['retailer_url']['source'], result['retailer_url']['value'] = self.get_3p_retailer_url(item)
            result['condition']['source'], result['condition']['value'] = self.get_3p_condition(item)
            result['description']['source'], result['description']['value'] = self.get_3p_description(item)
            result['logo']['source'], result['logo']['value'] = self.get_3p_logo(item)
            result['delivery']['source'], result['delivery']['value'] = self.get_3p_delivery(item)
            result['shipping']['source'], result['shipping']['value'] = self.get_3p_shipping(item)
            result['availability']['source'], result['availability']['value'] = self.get_3p_availability(item)
            result['url']['source'], result['url']['value'] = self.get_3p_url(item)
            result['title']['source'], result['title']['value'] = self.get_3p_title(item)

            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_3p_rating(item)

            rank_3p += 1
            
            results.append(result)

        return results

    def get_3p_price(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:

            if 'price' in item:
                source = str(item)
                value = str(item['price']['amount'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_currency(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'price' in item:
                source = str(item)
                value = item['price']['currency']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_title(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'offers' in item:
                source = str(item)
                value = item['offers'][0]['name']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_retailer(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        is_direct_seller = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'merchantId' in item:
                merchant = self.__merchants[item['merchantId']]
                source = str(merchant)
                value = merchant['name']
            
            direct_sellers = ['pricerunner', 'pricerunner.dk', 'www.pricerunner.dk']

            if value in direct_sellers:
                is_direct_seller = str(True)
            else:
                is_direct_seller = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value, is_direct_seller
    
    def get_3p_retailer_url(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'merchantId' in item:
                merchant = self.__merchants[item['merchantId']]
                source = str(merchant)
                retailer_url = merchant['url']
                
                if self.__class__.WEBSITE not in retailer_url:
                    retailer_url = 'https://%s%s' % (self.__class__.WEBSITE, retailer_url)
                
                value = retailer_url
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_logo(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'merchantId' in item:
                merchant = self.__merchants[item['merchantId']]

                if merchant['logo'] is not None:
                    source = str(merchant)
                    value = 'https://www.pricerunner.dk/images/180x60/logos/' + merchant['logo']['name']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_url(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            if 'offers' in item:
                source = str(item)
                value = 'https://www.pricerunner.dk' + item['offers'][0]['url']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_description(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_availability(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'offers' in item:
                source = str(item)
                value = item['offers'][0]['stockStatus']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_condition(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_delivery(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'delivery' in item and item['delivery'] is not None:
                source = str(item)

                if not item['delivery']['minDays'] == item['delivery']['maxDays']:
                    value = 'Leveringstid {} - {} dage'.format(item['delivery']['minDays'], item['delivery']['maxDays'])
                else:
                    value = 'Leveringstid {} dage'.format(item['delivery']['maxDays'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_shipping(self, item):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'shipping' in item and item['shipping'] is not None:
                source = str(item)
                value = str(item['shipping']['amount'])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return source, value 

    def get_3p_rating(self, item):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if 'merchantId' in item:
                merchant = self.__merchants[item['merchantId']]

                if 'rating' in merchant and merchant['rating'] is not None:
                    score_source = str(merchant)
                    score_value = str(merchant['rating']['average'])

                    reviews_source = str(merchant)
                    reviews_value = str(merchant['rating']['count'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.THIRD_PARTY_EXTRACTION.value
            reviews_value = FailureMessages.THIRD_PARTY_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    def get_3p_items_in_api(self,soup):
        _json_api = None
        tag = soup.select_one('script#initial_payload')
        if tag:
            try:
                _json = json.loads(tag.get_text().strip())
                if "__INITIAL_STATE__" in _json.keys():
                    _data = _json['__INITIAL_PROPS__']['__DEHYDRATED_QUERY_STATE__']['queries'][-2]['state']
                    if _data:
                        product_id = _data['data']['product']['id']
                        category_id = _data['data']['category']['id']
                        api_url = f'https://www.pricerunner.dk/public/productlistings/v3/pl/{category_id}-{product_id}/dk/filter?includeInternationalMerchants=true'
                        res = self.downloader.download(api_url)
                        if res:
                            _json_api = json.loads(res)

            except:
                pass

        return _json_api