import requests, re, json, urllib.parse
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ElgigantenDkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.x_instana_t = 'bd9d922244607835' # defaulted

    def download(self, url, timeout=randint(10, 60), headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        self.global_url = url

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [307]: # temporary fix, allow 302 redirect
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    if 'meta property="og:type" content="product"' in res.text or 'data-ta="productname-text">' in res.text or '"data":{"product"' in res.text: # PDP Checker
                        _headers = dict(res.headers)
                        if 'Server-Timing' in _headers:
                            _rgx = re.search(r';desc=(.*)', _headers['Server-Timing'])
                            if _rgx:
                                self.x_instana_t = _rgx.group(1)
                        return res.text
                    else:
                        raise DownloadFailureException()

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def download_from_api(self, url, referer=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'application/json, text/plain, */*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'authorization': '',
                'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
                'sec-ch-ua-mobile': '?0',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'x-app-checkout': 'false',
                'x-app-mode': 'b2c',
                'x-authorization':'',
                'x-instana-t': self.x_instana_t
            }

        if referer:
            headers['referer'] = referer

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.text
            else:
                return None

        except:
            return None

    def get_api_data (self, timeout = 10, headers = None, cookies = None):
        assert isinstance(self.global_url, str)
        assert isinstance(timeout, int)
        
        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36',
            'x-app-checkout': 'false',
            'x-app-mode': 'b2c',
            'x-instana-t': 'f2c9a95a31ca724d',
            'x-is-anonymous': 'true',
        }

        # replace this with recursive + regex to select 6digits numeric values
        product_id = self.global_url.split('/')[-2]

        # appMode: b2c
        # user: anonymous
        # operationName: getProductWithDynamicDetails
        # variables: {"articleNumber":"286781","withCustomerSpecificPrices":false}
        # extensions: {"persistedQuery":{"version":1,"sha256Hash":"628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705"}}

        # variables = {'articleNumber':f'{product_id}' ,'withCustomerSpecificPrices':'false'}
        # data = str(variables).replace("'", '"')

        # payload = {
        #     'appMode': 'b2c',
        #     'user': 'anonymous',
        #     'operationName': 'getProductWithDynamicDetails',
        #     'variables': data,
        #     'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705"}}',
        # }

        # url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urllib.parse.urlencode(payload)).replace('+','')
        # url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(json.dumps(payload))

        url = f'https://www.elgiganten.dk/cxorchestrator/dk/api?appMode=b2c&user=anonymous&operationName=getProductWithDetails&variables=%7B%22articleNumber%22%3A%22{product_id}%22%2C%22isB2B%22%3Afalse%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22172093ad41d48dee3ca99455fd9eccc9e8e9a65c6025a54d4379b0ec818e6407%22%7D%7D'

        # original
            # https://www.elgiganten.dk/cxorchestrator/dk/api?appMode=b2c&user=anonymous&operationName=getProductWithDynamicDetails&variables=%7B%22articleNumber%22%3A%22286781%22%2C%22withCustomerSpecificPrices%22%3Afalse%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705%22%7D%7D

        #    'https://www.elgiganten.dk/cxorchestrator/dk/api?appMode=b2c&user=anonymous&operationName=getProductWithDynamicDetails&variables=%7B%27articleNumber%27%3A+%27286781%27%2C+%27withCustomerSpecificPrices%27%3A+%27false%27%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705%22%7D%7D'

        # 'https://www.elgiganten.dk/cxorchestrator/dk/api?appMode=b2c&user=anonymous&operationName=getProductWithDynamicDetails&variables=%7B%22articleNumber%22%3A%22286781%22%2C%22withCustomerSpecificPrices%22%3A%22false%22%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22628a8cf7305b5e1b507a1906ecd0b11a564f45b34b09d71a6eec869e9485b705%22%7D%7D'

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                return res.text
            else:
                return None

        except:
            return None