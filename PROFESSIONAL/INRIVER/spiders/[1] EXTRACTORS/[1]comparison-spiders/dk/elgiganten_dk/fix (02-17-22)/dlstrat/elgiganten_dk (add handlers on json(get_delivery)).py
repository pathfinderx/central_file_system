import json
import logging
import re

from urllib.parse import urlencode
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ElgigantenDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.elgiganten.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        #product specefic for url: https://www.elgiganten.dk/erhverv/product/outlet/garmin-drive-52-mt-s-gps-bil/131167?context=b2c
        _data = {}
        tag = soup.select_one('div[data-bv-show="questions"]')
        if tag:
            _id = tag.get('data-bv-product-id')
            params = {'operationName': 'getProductWithDynamicDetails'}
            variables = {"articleNumber":_id,"withCustomerSpecificPrices":False}
            extensions = {"persistedQuery":{"version":1,"sha256Hash":"71e301b8e2a96d97efb141d297ab0880ae67cebb9e3c0f6d4184977075f64c06"}} #Old 54af93c12bebcd8730246c5df9e4ec747f46d28783544d0d645e924aa7cec934
            params['variables'] = json.dumps(variables, ensure_ascii=False)
            params['extensions'] = json.dumps(extensions, ensure_ascii=False)

            # req_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?operationName=getProductWithDynamicDetails&variables=%7B%22articleNumber%22%3A%22131167%22%2C%22withCustomerSpecificPrices%22%3Afalse%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2254af93c12bebcd8730246c5df9e4ec747f46d28783544d0d645e924aa7cec934%22%7D%7D'
            req_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urlencode(params))
            res = self.downloader.download_from_api(req_url)
            if res:
                _data = json.loads(str(res))
        else:
            _json = None
            _tag = soup.find('script', text= re.compile(r'"@type": "Product"'), type="application/ld+json")
            if _tag:
                _json = json.loads(_tag.get_text().strip())
        #end

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup,_data)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup,_data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, _data)

        return result

    def get_price(self, soup,_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #### WILL BE HANDLED NOW IN ET #####
            # # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            # tag = soup.select_one(".product-price-text")
            # if tag and "ikke tilgængelig" in tag.get_text().lower():
            #     value = '0.0'
            # # HANDLING FOR OUTOFSTOCK PRODUCTS WITH PRICES NOT VISIBLE IN PDP #
            # else:   
            if not _data == {}:
                source = str(_data)
                if _data['data']['product']['currentPricing']['price'] != None:
                    value = str(_data['data']['product']['currentPricing']['price']['value'])
                else:
                    value = str(_data['data']['product']['offers'][0]['offerPrice']['valueExVat'])
            else:
                tag = soup.find('meta', {'itemprop': 'price'})

                if tag and tag.has_attr('content'):
                    source = str(tag)
                    content = tag["content"]

                    if content:
                        value = content
                
                ### FOR HANDLING HIDDEN PRICES ###
                if not soup.select_one('.product-price-container span'): 
                    value = Defaults.GENERIC_NOT_FOUND.value
                ### FOR HANDLING HIDDEN PRICES ###

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'priceCurrency'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content
            else:
                value = 'DKK' # Defaulted
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup,_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if not _data == {}:
                source = str(_data)
                value = str(_data['data']['product']['buyableOnline'])
            else:
                tag = soup.find("link", {"itemprop": "availability"})

                if tag and tag.has_attr("href"):
                    source = str(tag)
                    availability = tag.get("href")

                    if availability:
                        value = availability
                else:
                    tag = soup.select_one('.store-status')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()
                
                if value == 'https://schema.org/InStoreOnly': # For instoreonly availability issue
                    tag = soup.select_one('span.el-button-text span.product-price-text')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            _tag = soup.find('script', text= re.compile(r'"@type": "Product"'), type="application/ld+json")
            if _tag:
                _json = json.loads(_tag.get_text().strip())

            # Reviews
            tag = soup.find("meta", {"itemprop":"reviewCount"})

            if tag and tag.has_attr('content'):
                reviews_source = str(tag)
                content = tag["content"]

                if content:
                    reviews_value = content
            else:
                reviews_source = str(_tag)
                reviews_value = str(_json['aggregateRating']['reviewCount'])

            # Ratings
            tag = soup.find("meta", {"itemprop":"ratingValue"})

            if tag and tag.has_attr('content'):
                score_source = str(tag)
                content = tag["content"]

                if content:
                    score_value = content
            else:
                score_source = str(_tag)
                score_value = str(_json['aggregateRating']['ratingValue'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'name'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag["content"]
            else:
                tag = soup.find('h1',{'class':'product-title'})
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            parent_tag = soup.find('span', {'itemprop': 'brand'})

            if parent_tag:
                tag = parent_tag.find('meta', {'itemprop':'name'})
                if tag and tag.has_attr('content'):
                    source = str(tag)
                    value = tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #### PYPPETEER IMPLEMENTATION ####
            # tag = soup.select_one('.section.product-inspiration')
            
            # if tag:
            #     for _script in tag.select('script'):
            #         _script.decompose()
            #     for _style in tag.select('style'):
            #         _style.decompose()
                    
            #     source, value = str(tag), ' '.join(tag.stripped_strings)
            #### PYPPETEER IMPLEMENTATION ####

            description = []
            src = []
            pre_desc = ''
            short_desc = soup.select_one("p.short-description")

            if short_desc:
                src.append(str(short_desc))
                short_specs = soup.select("ul.specs li")

                if short_specs:
                    pre_desc = short_desc.get_text().strip()

                    for spec in short_specs:
                        pre_desc += ("\n>" + spec.get_text().strip())

                    description.append(pre_desc)

            sku = None

            tag = soup.find("meta", {"itemprop":"sku"})

            if tag and tag.has_attr('content'):
                sku = tag.get("content")

            if tag:
                desc_url = "https://www.elgiganten.dk/INTERSHOP/web/WFS/store-elgigantenDK-Site/da_DK/-/DKK" \
                            "//CC_AjaxProductTab-Get?ProductSKU=" + sku \
                            + "&TemplateName=CC_ProductMoreInformationTab"
                desc_req = self.downloader.extra_download(desc_url)

                if desc_req:
                    src.append(desc_url)
                    desc_soup = BeautifulSoup(desc_req, 'lxml')
                    description.append(" ".join(desc_soup.stripped_strings))


            tag = soup.find("meta", {"property": "og:description"})

            if tag and tag.has_attr('content'):
                src.append(str(tag))
                description.append(tag.get("content"))
            
            # NEW UI
            tag = soup.select_one('.product-features__long-description')
            if tag:
                src.append(str(tag))
                description.append(" ".join(tag.stripped_strings))
            if description:
                source = " + ".join(src)
                value = " + ".join(description)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            sku = None

            tag = soup.find("meta", {"itemprop":"sku"})

            if tag and tag.has_attr('content'):
                sku = tag.get("content")

            if tag:
                spec_url = "https://www.elgiganten.dk/INTERSHOP/web/WFS/store-elgigantenDK-Site/da_DK/-/DKK" \
                            "//CC_AjaxProductTab-Get?ProductSKU=" + sku \
                            + "&TemplateName=CC_ProductSpecificationTab"
                source = spec_url
                spec_req = self.downloader.extra_download(spec_url)

                if spec_req:
                    spec_soup = BeautifulSoup(spec_req, 'lxml')
                    tag = spec_soup.select("table")
                    if tag:                   
                        value = self._get_initial_specs(tag)
            
            # NEW UI
            tag = soup.select_one('div[data-bv-show="questions"]')
            if tag:
                _id = tag.get('data-bv-product-id')
                params = {
                    'operationName': 'getProductSpecification'
                }
                variables = {"articleNumber":_id}
                extensions = {"persistedQuery":{"version":1,"sha256Hash":"efac648c4c9304aa989f449218f6e6bc18c94fbc0d015001c4cc808079c0e92b"}}
                params['variables'] = json.dumps(variables, ensure_ascii=False)
                params['extensions'] = json.dumps(extensions, ensure_ascii=False)

                req_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urlencode(params))
                res = self.downloader.download_from_api(req_url)
                if res:
                    _data = json.loads(str(res))
                    for attributes in _data['data']['product']['specification']['attributeGroups']:
                        for attribute in attributes['attributes']:
                            key = attribute['name']
                            val = attribute['value']
                            value[key] = val

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select("div.mediaAsset div img")

            if tag:
                source = str(tag)
                for img in tag:
                    if img.has_attr('src'):
                        image_url = img.get('src')
                        if "elgiganten.dk" not in image_url:
                            image_url = "https://www.elgiganten.dk" + image_url
                        value.append(image_url.replace('prod_all4one', 'fullsize'))
                    if img.has_attr('data-lazy'):
                        image_url = img.get('data-lazy')
                        if "elgiganten.dk" not in image_url:
                            image_url = "https://www.elgiganten.dk" + image_url
                        value.append(image_url.replace('prod_all4one', 'fullsize'))
            else:
                tag = soup.select_one('div.product-title-wrap p.sku')
                if tag:
                    productId = tag.get('data-product-sku')
                    imgHost = "http://tubby.scene7.com/is/image/"
                    req_url = "http://tubby.scene7.com/is/image//tubby/" + productId + "_set?req=imageset"
                    set_img = self.downloader.extra_download(req_url).text.strip().replace(",",";").split(";")
                    set_img = list(set(set_img))
                    source = req_url
                    for img in set_img:
                        if 'too many connections' in img.lower():
                            continue

                        if len(img) > 0 and "advanced" not in img and "video" not in img:
                            value.append(imgHost + img.replace('prod_all4one', 'fullsize'))

            # NEW UI
            tags = soup.select('img.ng-star-inserted.loading.ng-star-inserted')
            source = str(tags)
            for tag in tags:
                if tag.has_attr('src'):
                    value.append(tag.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            sku = None
            pid = None
            title = None
            price = None

            tag = soup.find("meta", {"itemprop":"sku"})

            if tag and tag.has_attr('content'):
                sku = tag.get("content")

            tag = soup.find("meta", {"itemprop":"productID"})

            if tag and tag.has_attr('content'):
                pid = tag.get("content")

            tag = soup.find('meta', {'itemprop': 'name'})

            if tag and tag.has_attr('content'):
                title = tag["content"]
            
            tag = soup.find("meta", {"itemprop":"price"})

            if tag and tag.has_attr('content'):
                price = tag["content"]
            
            tag = soup.find("meta", {"itemprop":"category"})

            if tag and tag.has_attr('content'):
                category = "%s - %s -" % (tag["content"].replace('>', '-'), title)

            if sku and pid and title and price and category:
                url = "https://dapi.videoly.co/1/videos/0/410/?SKU=%s&" \
                        "productPrice=%s&" \
                        "productId=%s&" \
                        "productTitle=%s&" \
                        "categoryTree=%s&" \
                        "oos=0&maxItems=15&ytwv=undefined&_b=Chrome&_bv=78.0&" \
                        "p=1&_w=1903&_h=937&_pl=&_cl=sv&callback=_vdly425c4b28de&" \
                        "hn=www.elgiganten.dk&sId=s:Hq9yzTYzXYqlsNDbhU0w829QobQFjLyr.IzDUIRPqGoyXsRDmT8tyFdCSK0SDu7mJsonRAD5im54" \
                        % (sku, price, pid, title, category)

                source = url
                res = self.downloader.extra_download(url)

                if res:
                    videos = re.findall(r'\W*videoId[^:]*:\D(.\w*.\w*)"', res)

                    for video in videos:
                        video = "https://www.youtube.com/embed/%s" % video
                        value.append(video)
            if not value:
                tags = soup.select('.article-text.article-free-html source')

                if tags:
                    source = str(source)
                    value = ['https://www.elgiganten.dk{}'.format(i['src']) for i in tags if i.has_attr('src')]

            if not value:
                tag = soup.select('[aria-label="video"] > video > source')
                if tag:
                    source = str(tag)
                    value = [_vid['src'] for _vid in tag if _vid.has_attr('src')]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup, _data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.stock-info .items-in-stock.align-left') or \
                soup.select_one('#stock-info')
                
            if tag:
                value = ' '.join(tag.stripped_strings)
                source = str(tag)

            if not value:
                tag = soup.select_one('.store-status.align-left')

                if tag:
                    value = ' '.join(tag.stripped_strings)
                    source = str(tag)
            if 'Produktet er på lager i' in value:
                value = Defaults.GENERIC_NOT_FOUND.value

            if value == Defaults.GENERIC_NOT_FOUND.value:
                
                tag = soup.select_one('[rel="canonical"]')
                if tag and tag.has_attr('href'):
                    referer = tag['href']
                else:
                    tag = soup.select_one('[type="application/ld+json"]')
                    if tag:
                        referer = json.loads(tag.text)['@id']

                params = {
                    'operationName':'getSettings',
                    'varables': json.dumps({}),
                    'extensions': json.dumps({
                        "persistedQuery":{
                            "version":1,
                            "sha256Hash":"71e301b8e2a96d97efb141d297ab0880ae67cebb9e3c0f6d4184977075f64c06" #updated hash
                            #0bf18b4e68055139a89cdbb9a9680708243fc9060e2fcfb69f3f4ad98866b01d - old
                            #29c56100ffc6e29a3277ee13137e719568a4c66751c0f465a08217028564e17e - old
                            #71e301b8e2a96d97efb141d297ab0880ae67cebb9e3c0f6d4184977075f64c06 - new
                        }
                    }
                    )
                }

                settings_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urlencode(params))

                res = self.downloader.download_from_api(settings_url, referer=referer)

                if res:
                    _json = json.loads(res)
                    if _json:
                        params = {'operationName':'getProductDeliveryIndications'}
                        params['variables'] = json.dumps(
                            {
                                "destination": {
                                    "postalCode":_json['data']['settings']['fallbackPostalCode'],
                                    "countryCode": "DK",
                                    "storeId": _json['data']['settings']['fallbackStoreId']
                                },
                                "articleNumber": referer.split('/')[-1]
                            }                    
                        )
                        params['extensions'] = json.dumps(
                            {
                                "persistedQuery": {
                                    "version": 1,
                                    "sha256Hash": "728947b1e186d0f6ff06ad2159e07c636e1ceee8f85b60b7c89d2f7b336c80ea"
                                }
                            }
                        )

                        delivery_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urlencode(params))

                        res = self.downloader.download_from_api(delivery_url, referer=referer)

                        if res:
                            _json = json.loads(res)
                            if _json:
                                for delivery_type in _json['data']['product']['deliveryIndication']['deliveryIndications']:
                                    if delivery_type['deliveryType'] == 'Postal':
                                        value = delivery_type['fastDeliveryDate']
                                        break

                                if not _json['data']['product']['deliveryIndication']['deliveryIndications']:
                                    value = str(_json['data']['product']['deliveryIndication']['deliveryIndications'])

                                    if value == '[]':
                                        if _data['data']['product']['family']:
                                            value = json.dumps(_data['data']['product']['family'])

                                        if _data['data']['product']['buyableInternet'] and _data['data']['product']['buyableInternet']:
                                            value = Defaults.GENERIC_NOT_FOUND.value
                                        
                                        else:
                                            params['operationName'] = 'getProductBStocks'
                                            params['variables'] = json.dumps(
                                                {
                                                    'articleNumber': referer.split('/')[-1]
                                                }
                                            )
                                            params['extensions'] = json.dumps(
                                                {
                                                    "persistedQuery": {
                                                        "version": 1,
                                                        "sha256Hash": "72b5abb37b72cdd3756a79f9049bef908791b227770e22aa7c8ed382ed6e4e81"
                                                    }
                                                }
                                            )
                                            stock_url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(urlencode(params))
                                            res = self.downloader.download_from_api(stock_url, referer=referer)

                                            stock_json = json.loads(res)

                                            if stock_json['data']['product']['siblings']:
                                                value = json.dumps(stock_json['data']['product']['siblings'])
                                            source = str(delivery_url)
                else:
                    value = Defaults.Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value
        finally:
            pass

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            rows = nth.select("tr")

            for row in rows:
                try:
                    tag = row.select('td')
                    if tag and len(tag) >= 2:
                        key = tag[0]
                        data = tag[1]
                    
                    result[key.get_text().strip()] = data.get_text().strip()

                except:
                    continue

        return result
