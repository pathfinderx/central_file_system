import json
import logging
import re, requests

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class KomplettDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.komplett.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)
        
        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        # Extract shipping
        # result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            _json = self._get_json(soup)
            if _json['offers']['price']:
                source, value = str(_json), _json['offers']['price']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'DKK'
        try:
            _json = self._get_json(soup)
            if _json['offers']['priceCurrency']:
                source, value = str(_json), _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self._get_json(soup)
            if _json['offers']['availability']:
                source, value = str(_json), _json['offers']['availability']
                #stockstatus-stock-details

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _json = self._get_json(soup)
            if _json['name']:
                source, value = str(_json), _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _json = self._get_json(soup)
            if _json['brand']['name']:
                source, value = str(_json), _json['brand']['name']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value
    
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _json = self._get_json(soup)
            if "aggregateRating" in _json:
                
                score_value = _json['aggregateRating']['ratingValue']
                reviews_value = _json['aggregateRating']['reviewCount']
                score_source = str(_json)
                reviews_source = str(_json)
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value 

        try:
            tags = soup.select_one('div.product-responsive-info.col-xs-12.col-sm-10.col-md-8.col-lg-6.col-sm-offset-1.col-md-offset-2.col-lg-offset-3.product-sections-left-content')
            # rows = tags.select('div.row')
            # if rows:
            #     for row in rows:
            #         val = ''.join(row.get_text().strip())
            #   source = str(rows)
            if tags:
                val = " ".join(tags.stripped_strings)
                if val:
                    source = str(tags)
                    value = val
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict
        
        try:
            tags = soup.select_one('div.col-xs-12.col-md-10.col-md-offset-1.product-sections-left-content')
            tables = tags.select('table')
            for table in tables:
                rows = table.select('tr')
                for row in rows:
                    key = row.select_one('th').get_text().strip()
                    val = row.select_one('td').get_text().strip()
                    if key and val:
                        value[key] = val

            # tables = soup.find_all('table')
            # for table in tables:
            #     if table.select('tr'):
            #         for tr in table.select('tr'):
            #             if tr.select_one('th') and tr.select_one('td'):
            #                 source = str(tables)
            #                 value.update({tr.select_one('th').get_text().strip():tr.select_one('td').get_text().strip()})

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('img.item-content')
            if tags:
                for tag in tags:
                    if "https" in tag['src']:
                        img = tag['src'].replace("img/p/100", "img/p/1200")
                    else:
                        img = "{}{}".format("https://www.komplett.dk/", tag['src']).replace("img/p/100", "img/p/1200") 
                    value.append(img)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        try:
            tag = soup.select('.embed-responsive-item')
            for video in tag:
                if video and video.has_attr('src'):
                    value.append(video['src'])
            source = str(tag)
    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value
    
    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.stockstatus-stock-details')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value


        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def _get_json(self, soup):
        tag = soup.find('script', {'type':'application/ld+json'})
        _json = json.loads(tag.get_text().strip())

        return _json