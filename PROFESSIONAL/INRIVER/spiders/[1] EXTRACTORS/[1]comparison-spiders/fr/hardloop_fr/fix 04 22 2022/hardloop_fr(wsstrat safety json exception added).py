import json
import logging
import re

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class HardloopFrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.hardloop.fr'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.data = {}

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self.data = self.get_json_from_tag(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result


    def get_json_from_tag(self, soup: BeautifulSoup = None):
        data = {}
        if soup:
            tag = soup.select_one('script#__NEXT_DATA__')
            if tag:
                data = json.loads(tag.text, strict=False)
        
        return data
        
    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('span', {'class': re.compile('productInfos_normalPrice_')})
            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            product_id = None
            tag = soup.find('select', {'class': re.compile('productInfos_sizeField_')})
            if tag:
                option_tags = tag.select('option')
                
                for tag in option_tags:
                    if tag.has_attr('selected'):
                        product_id = tag['value']
                        break
                        
            if product_id:
                fallback = self.data['props']['pageProps']['fallback']
                for key in fallback.keys():
                    if 'color-references' in key:
                        if 'color_references' in fallback[key].keys():
                            for color in fallback[key]['color_references']:
                                if 'variations' in color:
                                    for variation in color['variations']:
                                        if 'id_product_attribute' in variation.keys() and 'quantity' in variation.keys():
                                            if product_id == str(variation['id_product_attribute']):
                                                value = str(variation['quantity'])
                                                source = str(variation)
                                                break
            #PDP Add to Cart button Checker to check if valid OOS
            if value == '0':
                btn_checker = soup.select_one('.productInfos_isAvailable__ZL1O3')
                if btn_checker and btn_checker.get_text(strip=True) == 'Ajouter au panier':
                    value = '1' #override value from OOS to IS #meaning Add to Cart button is present in PDP
                    source = str(btn_checker)
            else:
                if value == Defaults.GENERIC_NOT_FOUND.value:
                    btn_tag = soup.select_one('.productInfos_addCart__3CDcq')
                    if btn_tag:
                        source, value = str(btn_tag), btn_tag.get_text() 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value


        try:
            tag = soup.find('h1', {'class':re.compile('productInfos_productName')})
            if tag:
                tag = tag.select_one('span')

                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('a.productInfos_manufacturer__1pM9L')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            temp_desc = []
            temp_src = []
            additional_tag = soup.select_one('.productShow_productTitles__3g9ZP').next
            if additional_tag and additional_tag != 'Spécificités techniques':
                temp_desc.append(additional_tag.get_text(strip=True))
                temp_src.append(str(additional_tag))
                
            tag = soup.select_one('.productDescription_descContent__2wCgM')
            if tag:
                temp_desc.append(' '.join(tag.stripped_strings))
                temp_src.append(str(tag))
            
            if temp_src and temp_desc:
                source = ' '.join(temp_src)
                value = ' '.join(temp_desc)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            try:
                json_string, _json = soup.select_one('script#__NEXT_DATA__').next_element, None
                _json = json.loads(json_string)
            except Exception as e:
                print('Error: wsstrat -> get_specifications() -> failed to load json')
            
            if (_json and 
                'props' in _json and
                'pageProps' in _json['props'] and
                'fallback' in _json['props']['pageProps']):
                
                source, json_base_target = str(_json), _json['props']['pageProps']['fallback']
                for k,v in json_base_target.items():
                    if (re.search(r'(technical-specifications)+', k)):
                        for x,y in json_base_target[k].items():
                            if ('technical_specifications' in x):
                                value.update({specs.get('name'):specs.get('value') for specs in json_base_target[k][x]})
                                        
            else:
                product_id = None
                tag = soup.find('select', {'class': re.compile('productInfos_sizeField_')})
                if tag:
                    option_tags = tag.select('option')
                    
                    for tag in option_tags:
                        if tag.has_attr('selected'):
                            product_id = tag['value']
                            break
                            
                if product_id:
                    fallback = self.data['props']['pageProps']['fallback']
                    for key in fallback.keys():
                        if 'technical-specifications' in key:
                            if 'technical_specifications' in fallback[key].keys():
                                for spec in fallback[key]['technical_specifications']:
                                    value[spec['name']] = spec['value']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            product_id = None
            tag = soup.find('select', {'class': re.compile('productInfos_sizeField_')})
            if tag:
                option_tags = tag.select('option')
                
                for tag in option_tags:
                    if tag.has_attr('selected'):
                        product_id = tag['value']
                        break
                        
            if product_id:
                fallback = self.data['props']['pageProps']['fallback']
                for key in fallback.keys():
                    if 'color-references' in key:
                        if 'color_references' in fallback[key].keys():
                            for color in fallback[key]['color_references']:
                                if 'variations' in color:
                                    for variation in color['variations']:
                                        if 'id_product_attribute' in variation.keys() and 'images' in variation.keys():
                                            if product_id == str(variation['id_product_attribute']):
                                                for img in variation['images']:
                                                    if 'large_default' in img.keys():
                                                        value.append(img['large_default'])
                                                

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value



    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.productInfos_delivery__w8kay')
            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
