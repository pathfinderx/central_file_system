import json
import logging
import re

from bs4 import BeautifulSoup, Tag

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class FnacComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.fnac.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self._api_url = None

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        _soup = self.download_from_tab(soup)
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, _soup)

        # Extract price listed
        result["price_listed"]["source"], result["price_listed"]["value"] = self.get_price_listed(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, _soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, _soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result
    
    def download_from_tab(self, soup):
        try:
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                _id = tag.get('data-tracking-prid')
                api_url = f'https://www.fnac.com/Nav/API/FnacOfferTab/{_id}/1?page=1'
                self._api_url = api_url
                _rawdata = self.downloader.do_get_download(api_url)

                if _rawdata:
                   return BeautifulSoup(_rawdata, "lxml")
                else:
                    return None
        except:
            return None

    def get_price(self, soup, _soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Requesting to get prices from left tab
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                if _soup:                    
                    if soup.select_one('.js-fnacTabContent.isActive'):
                        tag = _soup.select_one('.f-priceBox .f-priceBox-price.f-priceBox-price--reco') or soup.select_one('.f-priceBox .f-priceBox-price.f-priceBox-price--reco')
                        if tag:
                            source = str(self._api_url)
                            value = "".join(tag.stripped_strings)
                    else:
                        tag = soup.select_one('.f-faPriceBox__priceLine span')
                        if tag:
                            source = str(tag)
                            value = "".join(tag.stripped_strings)
                else:
                    if soup.select_one('li[data-track-click="MpFnacOffer"]'):
                        tag = soup.select_one('li[data-track-click="MpFnacOffer"] span')
                        if tag and tag.get_text().strip() == '':
                            return source, value
                    # For OOS products
                    if soup.select_one('.f-priceBox'): # FOR HANDLING HIDDEN PRICES IN PDP
                        tag = soup.find("script", text=re.compile('.*"@type":"Product".*'), attrs= {"type":"application/ld+json"})
                        if tag:
                            _json = json.loads(tag.get_text())

                            if _json and "offers" in _json.keys() and 'price' in _json['offers']:
                                source, value = str(tag), str(_json['offers']['price'])

            else:
                # For OOS products
                if soup.select_one('.f-faPriceBox__priceLine span'): # FOR HANDLING HIDDEN PRICES IN PDP #Please change only this line if issue for oos with price and oos without price
                    tag = soup.find("script", text=re.compile('.*"@type":"Product".*'), attrs= {"type":"application/ld+json"})
                    if tag:
                        _json = json.loads(tag.get_text())
                        if _json and 'offers' in _json.keys() and 'availability' in _json['offers']:
                            if _json and "offers" in _json.keys() and 'price' in _json['offers']:
                                source, value = str(tag), str(_json['offers']['price'])


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_price_listed(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.f-priceBox-price.f-priceBox-price--old')
            
            if tag:
                source, value = str(tag), tag.get_text()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_LISTED_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'offers' in _json.keys() and 'priceCurrency' in _json['offers']:
                    source, value = str(tag), _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup, _soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src = None
            stock = None
            
            # Requesting to get prices from left tab
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                if _soup:
                    if soup.select_one('li.f-productOffers-tab.js-fnacOffersTab'):
                        tag = _soup.select_one('.f-buyBox-availability')
                        if tag:
                            src = str(self._api_url)
                            stock = " ".join(tag.stripped_strings)
                    else:
                        tag = soup.select_one('.f-buyBox-availability')
                        if tag:
                            src = str(tag)
                            stock = " ".join(tag.stripped_strings)
            if not stock:
                tag = soup.find("script", text=re.compile("tc_vars*"))
                if tag:
                    tc_vars = re.search("({.*})", tag.get_text().strip())
                    if tc_vars:
                        tc_vars = json.loads(tc_vars.group(1))
                        if '103' in tc_vars['product_availability'] or '115' in tc_vars['product_availability']: 
                            src = str(tag)
                            stock = str(tc_vars['product_availability'])
                        elif '' in tc_vars['product_availability'] and '0' in str(tc_vars['product_pricemode']):
                            src = str(tag)
                            stock = str(tc_vars['product_pricemode'])
                        else:
                            tag = soup.find('span',{'class':'f-buyBox-availabilityStatus'})
                            if tag:
                                src = str(tag)
                                stock = tag.get_text().strip()
                            else:
                                tag = soup.find('script',{'type':'application/ld+json'})
                                if tag:
                                    data = json.loads(tag.get_text().strip())
                                    if data:
                                        src = str(tag)
                                        stock = str(data['offers']['availability'])

            if stock:
                source = src
                value = stock

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                reviews_source = str(tag)
                score_source = str(tag)
                if "aggregateRating" in _json.keys():
                    # Reviews
                    if "ratingCount" in _json["aggregateRating"].keys():
                        reviews_value = str(_json["aggregateRating"]["ratingCount"])

                    # Ratings
                    if "ratingValue" in _json["aggregateRating"].keys():
                        score_value = str(_json["aggregateRating"]["ratingValue"])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'name' in _json:
                    # _brand = self.get_brand(soup)
                    # source, value = str(tag), _brand[1] + ' ' + _json['name']
                    # DO NOT UNCOMMENT --- check ET if brand is scraped but not prepended in title
                    source, value = str(tag), _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or \
                soup.find('script', text=re.compile('.*"@type":"Product".*'))

            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'brand' in _json.keys() and 'name' in _json['brand']:
                    source, value = str(tag), _json['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc_list = []
            source_list = []
            _json = None
            
            tag = soup.select_one('div.js-Expander div.f-productDesc__raw')
            if tag and tag.select('li'):
                desc_li = tag.select('li')
                for item in desc_li:
                    source_list.append(str(item))
                    desc_list.append(item.get_text())
            
            else:
                tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})

                if tag:
                    _json = json.loads(tag.get_text())

                    if _json and 'description' in _json:
                        _desc = ' '.join(re.sub('<[^>]+>', '', _json['description']).split())
                        if _desc:
                            source_list.append(str(tag))
                            desc_list.append(_desc) 

                if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value] and not desc_list:

                    tag = soup.select_one('.productStrate__raw.summaryStrate__raw')

                    if tag:
                        source_list.append(str(tag))
                        desc_list.append(' '.join(tag.stripped_strings))

            try:
                tag = soup.select_one('.productStrate.brandWordStrate.js-expand script')
                url = None
                ean = None
                _id = None
                dist = None
                if tag:
                    if tag.has_attr('src'):
                        # url = tag['src']
                        if tag.has_attr('data-flix-distributor'):
                            dist = tag['data-flix-distributor']
                        if tag.has_attr('data-flix-ean'):
                            ean = tag['data-flix-ean']
                    else:
                        # _rgx = re.search(r'script.src = "(.*)";', tag.get_text())
                        # url = _rgx.group(1) if _rgx else None
                        dist_rgx = re.search(r'script.setAttribute\("data-flix-distributor", "([0-9]+)"\);',tag.text.strip())
                        id_rgx = re.search(r'script.setAttribute\("data-flix-ean", "([0-9]+)"\);',tag.text.strip())
                        
                        if id_rgx and dist_rgx:
                            ean = id_rgx.group(1)
                            dist = dist_rgx.group(1)
                if not tag:
                    ean_tag = soup.select_one('script[type="application/ld+json"]')
                    if ean_tag:
                        _json = json.loads(ean_tag.get_text())
                        ean = _json['gtin13']
                    dist_tag = soup.select_one('script[type="text/plain"]')
                    if dist_tag:
                        dist_rgx = re.search(r'script.setAttribute\("data-flix-distributor", "([0-9]+)"\);',dist_tag.text.strip())
                        if dist_rgx:
                            dist = dist_rgx.group(1)

                if ean and dist:    
                    url = 'https://media.flixcar.com/delivery/js/inpage/{}/fr/ean/{}?&={}&=fr&ean={}&fl=fr&ssl=1&ext=.js'.format(dist, ean, dist, ean)  
                    # if url:    
                    res = self.downloader.do_get_download(url)

                    if res:
                        id_rgx = re.search("window.flixJsCallbacks.pid ='(.*)'", res) or re.search(r",product:'(\d+)'", res) or re.search(r"flixinpage_(\d+)'", res)
                        dist_rgx = re.search("distributor: '(.*)'", res) or re.search(r"dist='(\d+)'", res) or re.search(r"distributor:'(\d+)'",res)

                        if id_rgx and dist_rgx:
                            _id = id_rgx.group(1)
                            dist = dist_rgx.group(1)  

                if _id and dist:
                    api_url = 'https://media.flixcar.com/delivery/inpage/show/{}/fr/{}/json?&complimentary=0&type=.html'.format(dist, _id)     
                    _res = self.downloader.do_get_download(api_url)         

                    _json_data = json.loads(_res[1:][:-1])     
                    soup = BeautifulSoup(_json_data['html'], 'lxml')  
                    [i.decompose() for i in soup.select('style')]
                    [i.decompose() for i in soup.select('script')]     
                    source_list.append(api_url) 
                    desc_list.append(' '.join(soup.stripped_strings))      
                
                inpageContainer = soup.select_one('div.inpage_selector_info')
                if inpageContainer:
                    descriptionTag = inpageContainer.select_one('div.flix-std-content') or inpageContainer.select_one('div.flix-std-pad')
                    if descriptionTag:
                        description = ' '.join(descriptionTag.stripped_strings)
                        source, value = str(inpageContainer), description

                    #NOT INCLUDED ON DESCRIPTION       
                    #if value == 'NOT_FOUND':
                        #desc_tag = soup.select_one('[class="f-productProperties__item f-productProperties__item--long"]')
                        #if desc_tag:
                        #    source, value = str(desc_tag), desc_tag.get_text().strip()

            except Exception as e:
                print(e)
                pass

            # commented this temporarily
            # tag = soup.select_one('.productStrate.includedAccessoriesStrate')

            # if tag:
            #     source_list.append(str(tag))
            #     desc_list.append(' '.join(tag.stripped_strings))    

            if not desc_list:
                tag = soup.select_one('div.js-Expander div.f-productDesc__raw')     
                if tag:
                    source_list.append(str(tag))
                    desc_list.append(' '.join(tag.stripped_strings)) 

            desc = []
            if source_list and desc_list:
                for list in desc_list:
                    list = list.replace('&lt;p','')
                    desc.append(list)
                source, value = ' '.join(source_list), ' '.join(desc)

            # else:
            #     tag = soup.find("meta", { "name":"description" })
            #     if tag and tag.has_attr('content'):
            #         source = str(tag)
            #         value = tag["content"]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('.characteristicsStrate__lists .characteristicsStrate__list')
            if tags:
                source = str(tags)
                value = self._get_initial_specs(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('.f-productVisuals-thumbnailsWrapper .f-productVisuals-thumbnail[data-type="image"]') or soup.select('div.f-productVisuals__thumbnail') \
                or soup.select('.f-productVisuals__thumbnail') or soup.select('button.f-productMedias__thumbnailsItem')

            if tags:
                source = str(tags)
                for img in tags:
                    if img.has_attr('data-src-zoom'):
                        image_url = img.get('data-src-zoom')
                        if image_url:
                            value.append(image_url)
                    elif img.has_attr('data-src'):
                        image_url = img.get('data-src')
                        if image_url:
                            value.append(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tags = soup.select('.f-productVisuals .VideoPlayerPlaylist-list .VideoPlayerPlaylist-item') or soup.select('.f-productVisuals__video .videoPlayer__playlist .videoPlayer__playlist-item') \
                or soup.select('.f-productMedias__view .videoPlayer__playlist .videoPlayer__playlist-item')

            alternative_video_tag = soup.select_one('.videoPlayer__media')

            if tags:
                source = str(tags)
                for vid in tags:
                    if vid.has_attr('data-video-0-url'):
                        if vid['data-video-0-url']:
                            video_url = vid.get('data-video-0-url')
                            value.append(video_url)
                            break
                    elif vid.has_attr('data-video-url'):
                        if vid['data-video-url']:
                            video_url = vid.get('data-video-url')
                            value.append(video_url)
                            break

            elif (alternative_video_tag):
                value = [x.get('src') for x in alternative_video_tag.contents if isinstance(x, Tag) and x.has_attr('src')]
                if value:
                    source = str(alternative_video_tag)

            else:
                tags = soup.select('.videoPlayer__playlist > div')
                if len(tags) > 0:
                    for vid in tags:
                        if vid.has_attr('data-video-0-url'):
                            if vid['data-video-0-url']:
                                source = str(tags)
                                value.append(vid['data-video-0-url'])
                                break

            if not value or len(value) <= 0:
                url = 'https://media.flixcar.com/delivery/js/hotspot/75/fr/ean/'

                ean_tag = soup.select_one('.f-productPage.clearfix.js-articleView') or soup.select_one('.f-productPage.js-articleView')

                if ean_tag and ean_tag.has_attr('data-ean13'):
                    ean = ean_tag.get('data-ean13')
                    url += ean
                    res = self.downloader.do_get_download(url)

                    if res:
                        rgx = re.search(r"value='(\{.*\})'", res)
                        _json = json.loads(rgx.group(1).replace("\\", "")) if rgx else {}

                        if 'playlist' in _json:
                            source = url
                            value = ['https:'+i['file'] for i in _json['playlist'] if 'file' in i]

            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup, _soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src = None
            stock = None
            
            # Get from left tab
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                if _soup:
                    if soup.select_one('li.f-productOffers-tab.js-fnacOffersTab'):
                        tag = _soup.select_one('.f-buyBox-availability')
                        if tag:
                            src = str(self._api_url)
                            stock = " ".join(tag.stripped_strings)
                    else:
                        tag = soup.select_one('.f-buyBox-availability')
                        if tag:
                            src = str(tag)
                            stock = " ".join(tag.stripped_strings)
            if not stock:
                tag = soup.find('span',{'class':'f-buyBox-availabilityStatus'})
                if tag:
                    src = str(tag)
                    stock = tag.get_text().strip()
                else:
                    tag = soup.find('script',{'type':'application/ld+json'})
                    if tag:
                        data = json.loads(tag.get_text().strip())
                        if data:
                            src = str(tag)
                            if 'offers' in data:
                                stock = str(data['offers']['availability'])

            if stock:
                source = src
                value = stock

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one("p.f-buyBox-shipping")
            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value       

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            rows = nth.select(".characteristicsStrate__item")

            for row in rows:
                try:
                    tag = row.select_one('.characteristicsStrate__term')
                    if tag:
                        key = " ".join(tag.stripped_strings)

                    tag = row.select_one('.characteristicsStrate__definition')
                    if tag:
                        data = " ".join(tag.stripped_strings)
                    
                    result[key] = data

                except:
                    continue

        return result