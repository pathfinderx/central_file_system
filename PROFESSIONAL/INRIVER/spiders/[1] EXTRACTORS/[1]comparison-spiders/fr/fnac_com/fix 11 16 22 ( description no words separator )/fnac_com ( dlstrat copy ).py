import cloudscraper
import requests, random
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests


class FnacComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('fr')
        session = requests.Session()
        self.scraper = cloudscraper.create_scraper(sess=session)
        self.root_url = None
        self.cookies = None

    def download(self, url, timeout=randint(60, 120), headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'pragma': 'no-cache',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
            }
            self.root_url = url
            res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
            print(url, res.status_code)

            if res.status_code not in [200, 201]:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                print('Re-trigger requests using Luminati', res.status_code)

            if res.status_code not in [200, 201]:
                res = self.scraper.get(url)
                print('Re-trigger requests using Cloudscraper', res.status_code)

            # if res.status_code not in [200, 201]:
            #     print('entering requests library. . . ')
            #     rotating_agents = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36']
            #     headers = {
            #         'accept-encoding': 'gzip, deflate',
            #         'accept-language': 'en-US,en;q=0.9',
            #         'cache-control': 'no-cache',
            #         'user-agent': random.choice(rotating_agents)
            #     }
            #     res = requests.get(url, headers=headers, timeout=10)

            # print(headers)
            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                # for item in res.history:
                #     if item.status_code in [302, 307]:
                #         is_redirect = True
                #         status_code = item.status_code
                #         break

                if not is_redirect:
                    if '"@type":"Product"' in res.text or 'aggregateRating' in res.text:
                        self.cookies = res.cookies.get_dict()
                        return res.text
                    else:
                        raise DownloadFailureException('Download failed - may need auto rerun')

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def do_get_download(self, url, data=None):
        try:
            # headers = {
            #     'accept': '*/*',
            #     'accept-encoding': 'gzip, deflate',
            #     'accept-language': 'en-US,en;q=0.9',
            #     'referer': 'https://www.fnac.com/',
            #     'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
            #                 '(KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'
            # }
            headers = {
                        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding': 'gzip, deflate',
                        'accept-language': 'en-US,en;q=0.9',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache',
                        'sec-fetch-dest': 'document',
                        'sec-fetch-mode': 'navigate',
                        'sec-fetch-site': 'none',
                        'sec-fetch-user': '?1',
                        'upgrade-insecure-requests': '1',
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
                    }
            if not data:
                res = self.unblocker.get(url, timeout=10000, headers=None)
                print(url, res.status_code)

                if res.status_code not in [200, 201]:
                    res = self.requester.get(url, timeout=10000, headers=headers,cookies=self.cookies)
                    print('Re-trigger requests using Luminati', res.status_code)

                if res.status_code not in [200, 201]:
                    headers = {
                        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding': 'gzip, deflate',
                        'accept-language': 'en-US,en;q=0.9',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache',
                        'sec-fetch-dest': 'document',
                        'sec-fetch-mode': 'navigate',
                        'sec-fetch-site': 'none',
                        'sec-fetch-user': '?1',
                        'upgrade-insecure-requests': '1',
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
                    }
                    cookies = self.unblocker.session.cookies.get_dict()
                    res = self.scraper.get(url, timeout=10, headers=headers,cookies=cookies)
                    print('Re-trigger requests using Cloudscraper', res.status_code)    

                if res.status_code in [200, 201]:
                    return res.text
            else:
                # headers = {
                #     'accept': 'text/html, */*; q=0.01',
                #     'accept-encoding': 'gzip, deflate, br',
                #     'accept-language': 'en-US,en;q=0.9',
                #     'content-type': 'application/json',
                #     'origin': 'https://www.fnac.com',
                #     'referer': self.root_url,
                #     'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
                #     'sec-ch-ua-mobile': '?0',
                #     'sec-fetch-dest': 'empty',
                #     'sec-fetch-mode': 'cors',
                #     'sec-fetch-site': 'same-origin',
                #     'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
                #     'x-requested-with': 'XMLHttpRequest'
                # }
                headers = {
                        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding': 'gzip, deflate',
                        'accept-language': 'en-US,en;q=0.9',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache',
                        'sec-fetch-dest': 'document',
                        'sec-fetch-mode': 'navigate',
                        'sec-fetch-site': 'none',
                        'sec-fetch-user': '?1',
                        'upgrade-insecure-requests': '1',
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36',
                        'x-requested-with': 'XMLHttpRequest'
                    }
                res = self.unblocker.post(url, timeout=10000, headers=None, data=data)
                print(url, res.status_code)

                if res.status_code not in [200, 201]:
                    res = self.requester.post(url, timeout=10000, headers=headers, data=data,cookies=self.cookies)
                    print('Re-trigger requests using Luminati', res.status_code)

                if res.status_code not in [200, 201]:
                    res = self.requester.post(url, timeout=10000, headers=headers)
                    print('Re-trigger requests using Luminati', res.status_code)              

                if res.status_code not in [200, 201]:
                    headers = {
                        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding': 'gzip, deflate',
                        'accept-language': 'en-US,en;q=0.9',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache',
                        'sec-fetch-dest': 'document',
                        'sec-fetch-mode': 'navigate',
                        'sec-fetch-site': 'none',
                        'sec-fetch-user': '?1',
                        'upgrade-insecure-requests': '1',
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
                    }
                    cookies = self.unblocker.session.cookies.get_dict()
                    res = self.scraper.post(url, timeout=10, headers=headers,data=data,cookies=cookies)
                    print('Re-trigger requests using Cloudscraper', res.status_code)    

                if res.status_code in [200, 201]:
                    return res.text

            return None

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')