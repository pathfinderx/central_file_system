# import requests, cloudscraper
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests

class DartyComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.unblocker = UnblockerSessionRequests('fr')
        self.requester = requester
        # sess = requests.Session()
        # self.scraper = cloudscraper.create_scraper(sess)
        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
        }

    def download(self, url, timeout=60, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None
        try:
           
            res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
            print('default requests..')
            
            # if res.status_code not in [200, 201]:
            #     print('cloudscraper requests..')
            #     res = self.scraper.get(url)

            # if res.status_code not in [200, 201]:
            #     headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36"}
            #     print('normal requests..')
            #     res = requests.get(url, headers=headers, timeout=10)
            
            status_code = res.status_code

            if status_code in [200, 201]:
                if 'itemtype="https://schema.org/Product"' in res.text or 'id="product_tabs"' in res.text or 'div id="main_products_list" class="main_products_list"' not in res.text:  #'property="og:type" content="product"' in res.text  <-- try to not use this pdp checker 
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))
        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')