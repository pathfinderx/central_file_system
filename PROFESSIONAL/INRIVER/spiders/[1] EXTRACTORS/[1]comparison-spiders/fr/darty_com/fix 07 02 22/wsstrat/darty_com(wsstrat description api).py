import json
import logging
import re
import requests
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class DartyComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.darty.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract price listed
        result["price_listed"]["source"], result["price_listed"]["value"] = self.get_price_listed(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result
   
    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("meta", { "itemprop":"price" })

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag['content']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET PRICE LISTED
    def get_price_listed(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.find("meta", { "itemprop":"price" })

            if tag:
                source, value = str(tag), tag['content']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value 

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="priceCurrency")

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag['content']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
      
        try:
            tags = soup.find(itemprop="availability")

            if tags and tags.has_attr('href'):
                source, value = str(tags), tags['href']
            
            else:
                # For OOS products
                tag = soup.select_one('.product_overlay_unavailable')
                if tag:
                    source = str(tag)
                    value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews
            reviews_tag = soup.find(itemprop="reviewCount")

            if reviews_tag and reviews_tag.has_attr('content'):
                reviews_source, reviews_value = str(reviews_tag), reviews_tag['content']

            # Ratings
            score_tag = soup.find(itemprop="ratingValue")

            if score_tag and score_tag.has_attr('content'):
                score_source, score_value = str(score_tag), score_tag['content']

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
     
        try:
            tag = soup.select_one('.product_head h1')
            
            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)
       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop='brand')
            
            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')
           
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('div.columns')
            
            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)
            else:
                tag = soup.select_one("div#product_description")
                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

            additional_description = self.get_desc_api(soup) # returns tuple (source and value)
            if (additional_description and additional_description[0] and additional_description[1]):
                source = str(additional_description[0])
                value += ' + ' +  ' '.join([''.join(x.stripped_strings) for x in additional_description[1] if x.get_text(strip=True) != ''])
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict
        
        try:
           tags = soup.select_one("#product_caracteristics") or soup.select_one(".product_bloc_content.bloc.ombre > table > tbody")
           if tags:
               source, value = str(tags), self.extract_specs(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        regex = re.compile('.*darty_product_pic_more_pics.*')
        
        try:
            tag = soup.find("ul", {"class" : regex})
            if tag:
                source = str(tag)
                tag = tag.select("img")
                if tag:
                    for items in tag:
                        if items:
                            temp = items["src"]
                            if '_e' in temp:
                                temp = temp.replace("_e", "_s")
                                value.append(temp)
                            else:
                                if "static" in items["src"]:
                                    pass
                                else:
                                    value.append(items["src"])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
        try:
            tags = soup.select("li", {"class":"darty_product_picture_video_trigger"})
            if tags:
                source = str(tags)
                value = self.extract_videos(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
           tag = soup.select_one(".product-delivery-info")
           if tag:
               source = str(tag)
               value = ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    # get json
    @staticmethod
    def get_json(tags):
        src = 'NOT FOUND'
        app_data = None

        if tags:
            src = str(tags)
            app_data = json.loads(tags.get_text())
            
        return src, app_data
    
    @staticmethod
    def extract_videos(tags):
        video = []
        for i in tags:
            try:
                video.append("https://www.youtube.com/embed/%s" % i["data-youtube-id"])
            except:
                pass
            
        return video

    # extract specs
    @staticmethod
    def extract_specs(tags):
        specs = {}
        trs = tags.select("tr")
        specs = {}
        for tr in trs:
            try:
                key = tr.select_one("th").get_text().strip()
                value = tr.select_one("td").get_text().strip()
                specs[key] = value
            except:
                keylist = []
                vallist = []
                for trs in tags:
                    ths = trs.select("th")
                    tds = trs.select("td")
                    keylist.append(str(ths[0].get_text()))
                    vallist.append(str(tds[0].get_text()))
                specs = dict(zip(keylist,vallist))
        return specs


    def get_desc_api(self, soup):
        ##### SOURCES #####
        # get distributor: 
            # https://www.darty.com/static/wUsi/wro/desktop_product.pack.js ()
            # or use static distributor (2754) as commonly identified on other pages

        # get ean (0017817692311) for inpage_data (947142)
            # https://media.flixcar.com/delivery/js/inpage/2754/fr/ean/0017817692311?&=2754&=fr&ean=0017817692311&brand=Bose&ssl=1&ext=.js

        # description data: 
            #  https://media.flixcar.com/delivery/inpage/show/2754/fr/947142/json?c=jsonpcar2754fr947142&complimentary=0&type=.html

        try:
            payload = {
                'distributor' : '2754', # static: commonly found on other item_ids
                'inpage_data' : None,
            }
            
            ean_tag = soup.select_one('meta[itemprop="gtin13"]')
            brand_tag = soup.select_one('div[itemprop="brand"] > meta')

            if (brand_tag and ean_tag):
                if (ean_tag.has_attr('content') and
                    brand_tag.has_attr('content')):
        
                    payload['ean'] = ean_tag.get('content')
                    payload['brand'] = brand_tag.get('content')

                if ('distributor' in payload and
                    'brand' in payload and
                    'ean' in payload):

                    # https://media.flixcar.com/delivery/js/inpage/2754/fr/ean/0017817692311?&=2754&=fr&ean=0017817692311&brand=Bose&ssl=1&ext=.js
                    inpage_data_url = f'https://media.flixcar.com/delivery/js/inpage/{payload["distributor"]}/fr/ean/{payload["ean"]}?&=2754&=fr&ean={payload["ean"]}&brand={payload["brand"]}&ssl=1&ext=.js'
                    inpage_data_res = self.downloader.get_description(inpage_data_url)

                    flixinpage_rgx, description_url, payload["inpage_data"] = None, None, 0 
                    flixinpage_rgx = re.search(r'flixinpage_([0-9]+)', inpage_data_res) if inpage_data_res else None

                    payload['inpage_data'] = flixinpage_rgx.group(1) if flixinpage_rgx else None

                    description_url = f'https://media.flixcar.com/delivery/inpage/show/{payload["distributor"]}/fr/{payload["inpage_data"]}/json?c=jsonpcar{payload["distributor"]}fr{payload["inpage_data"]}&complimentary=0&type=.html' if payload['inpage_data'] != None else None 

                    if description_url:
                        _json, description_data_rgx, description_data_res, description_data_lxml, tag = None, None, None, None, None
                        description_data_res = self.downloader.get_description(description_url)
                        description_data_rgx = re.search(r'\(({\"css[\s\S]+})', description_data_res).group(1) if description_data_res else None

                        _json = json.loads(description_data_rgx) if description_data_rgx else None
                        if (_json and _json.get('html')):
                            description_data_lxml = BeautifulSoup(_json.get('html'), 'lxml')
                            tag = description_data_lxml.select('div.inpage_selector_feature') if description_data_lxml else None

                            return (description_data_lxml, tag) if (description_data_lxml and tag) else (None, None)

        except Exception as e:
            print('Error in: get_desc_api() -> with message: ', e)
            return (None, None)
           