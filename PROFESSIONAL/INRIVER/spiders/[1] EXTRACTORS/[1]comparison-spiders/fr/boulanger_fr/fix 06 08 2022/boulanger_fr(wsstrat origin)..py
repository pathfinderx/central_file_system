import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BoulangerComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.boulanger.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery - No Delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'price'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and 'offers' in _json.keys() and 'price' in _json['offers']:
                        source,value = str(tag),str(_json['offers']['price'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta', {'itemprop': 'priceCurrency'})

            if tag and tag.has_attr('content'):
                source = str(tag)
                content = tag["content"]

                if content:
                    value = content
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and 'offers' in _json.keys() and 'priceCurrency' in _json['offers']:
                        source,value = str(tag),str(_json['offers']['priceCurrency'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:       
            cartcheck = soup.select_one('div.col-13.offset-2.product-sidebar__btn') or soup.select_one('a.button.buttonOrange.x-addToCart')
            if cartcheck:
                tag = soup.find('link', {'itemprop': 'availability'})
                product_available_tag = soup.select_one('.pb-infos.pb-right.product-unavailable .infoAchat > p') or soup.select_one('.infoAchat p') #\
                    #or soup.select_one('div.product-delivery__container.product-delivery__container--error p')
                if tag and tag.has_attr('href'):
                    if product_available_tag and "Livraison" in product_available_tag.get_text(strip=True):
                        source, value = str(product_available_tag), ' '.join(product_available_tag.parent.stripped_strings) #product_available_tag.get_text(strip=True) # used parent tag to fetch availability
                    else:
                        source, value = str(tag), tag["href"]
                else:
                    tag = soup.find('script',text = re.compile('"@type":"Product"'))
                    if tag:
                        _json = json.loads(tag.text.strip())
                        if _json and 'offers' in _json.keys() and 'availability' in _json['offers']:
                            source,value = str(tag),str(_json['offers']['availability'])
                    if value == 'NOT_FOUND' or value == "": 
                        tags = soup.select('svg.decepticon.decepticon-availability.decepticon-available')
                        check = 1 
                        if tags:            
                            for tag in tags:
                                if not tag['aria-hidden']:
                                    check_tag = tag.select_one('use')
                                    if check_tag:
                                        if "#mono-check" in check_tag['xlink:href']:
                                            check = check + 1
                                else:
                                    tags = soup.select_one('div.pickupInfo.purchaseBlockTitle')
                                    if tags:
                                        if "Retrait" in tags.get_text().strip():
                                            source = str(tags)
                                            check = check - 1
                        else:   
                            tags = soup.select_one('div.pickupInfo.purchaseBlockTitle')
                            if tags:
                                if "Retrait" in tags.get_text().strip():
                                    source = str(tags)
                                    check = check - 1
                                    
                            if check > 1:
                                source = str(tags)
                                value = "in-stock"
                            else:
                                source = str(tags)
                            value = "out-of-stock"
            elif not cartcheck:
                tags = soup.select_one('div.product-delivery')
                if tags and tags.has_attr('data-product-no-buyable'):
                    if tags['data-product-no-buyable'] == 'true':
                        value = "out-of-stock"
                        source = str(tags)
                    
            #tag = soup.find('link', {'itemprop': 'availability'})
            #product_available_tag = soup.select_one('.pb-infos.pb-right.product-unavailable .infoAchat > p') or soup.select_one('.infoAchat p')

            #if tag and tag.has_attr('href'):
            #    if product_available_tag and "Livraison" in product_available_tag.get_text(strip=True):
            #        source, value = str(product_available_tag), ' '.join(product_available_tag.parent.stripped_strings) #product_available_tag.get_text(strip=True) # used parent tag to fetch availability
            #    else:
            #        source, value = str(tag), tag["href"]
            #else:
            #    tag = soup.find('script',text = re.compile('"@type":"Product"'))
            #    if tag:
            #        _json = json.loads(tag.text.strip())
            #        if _json and 'offers' in _json.keys() and 'availability' in _json['offers']:
            #            source,value = str(tag),str(_json['offers']['availability'])
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "50"  # For this site, max rating is 50 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            parent_tag = soup.select_one('.ratingandRef')
            if parent_tag:
                # Reviews
                tag = parent_tag.select_one('.link')

                if tag:
                    reviews_source = str(tag)
                    content = tag.get_text().strip()

                    if content:
                        reviews_value = content

                # Ratings
                tag = parent_tag.select_one('.rating')

                if tag:
                    score_source = str(tag)
                    content = tag.get('class')[-1]

                    if content:
                        score_value = content
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and 'aggregateRating' in _json.keys():
                        score_max_value = '5'
                        # Reviews
                        if 'reviewCount' in _json['aggregateRating']:
                            reviews_source,reviews_value = str(tag),str(_json['aggregateRating']['reviewCount'])

                        #Ratings
                        if 'ratingValue' in _json['aggregateRating']:
                            score_source,score_value = str(tag),str(_json['aggregateRating']['ratingValue'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('h1', {'itemprop': 'name'}) or soup.select_one('.product-title__main')

            if tag:
                source = str(tag)
                value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('span', {'itemprop': 'brand'}) or soup.select_one('a.product-title__link')

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc_list = (
                    soup.select_one('[itemprop="description"]') or soup.find('p',{'class':'short-description'}),
                    #soup.find("div",{"class":"best-points bottom"}),
                    #soup.find("div", {"class":"features-table"}),
                    #soup.find("section",{"class":"product-features"}), #commented this temporarily -- removed in ticket 5741 daily runs
                    soup.select_one('.product-description__content')
                    )

            data = []
            
            if len(desc_list):
                for tag in desc_list:
                    if tag:
                        if source == 'NOT_FOUND':
                            source = str(tag)
                            data.append(' '.join(tag.stripped_strings))
                        else:
                            source += str(tag)
                            data.append(' '.join(tag.stripped_strings))

            desc = ' '.join(data)

            if desc:
                value = desc

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.select_one('.features-table .characteristic')

            if tag:
                source = str(tag)
                value = self._get_initial_specs(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            gtin = None
            tag = soup.find("span", {"itemprop":"gtin13"})
            if tag:
                gtin = tag.get_text().strip()
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and 'gtin13' in _json.keys():
                        gtin = _json['gtin13']
            if gtin:
                # gtin should be 13 digits, if not append 0 at the front, 
                # this is a site bug where json string erase the leading 0
                if len(gtin) < 13:
                    number_of_0s = 13 - len(gtin)
                    zeros = ''.join(['0' for n in range(number_of_0s)])
                    gtin = f'{zeros}{gtin}'
                    
                api_url = "https://boulanger.scene7.com/is/image/Boulanger/%s_mixed?req=set,json,UTF-8&labelkey=label&id=99046343" % gtin
                headers = {'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8', 'accept-encoding': 'gzip, deflate', 'accept-language': 'en-US,en;q=0.9', 'cache-control': 'no-cache', 'pragma': 'no-cache', 'referer': 'https://www.boulanger.com/','sec-ch-ua-mobile': '?0', 'sec-ch-ua-platform': '"Linux"', 'sec-fetch-dest': 'image', 'sec-fetch-mode': 'no-cors', 'sec-fetch-site': 'cross-site', "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"}
                raw_data = self.downloader.extra_download(api_url, headers=headers)
                if "Error while processing Fvctx image" not in raw_data:
                    _rgx = re.search(r'jsonResponse\((.*\}\})\,.*\)\;', raw_data)
                    if _rgx:
                        _json = json.loads(_rgx.group(1))
                        source = api_url
                        images = _json['set']['item']
                        if type(images) == list: # for many images
                            for image in images:
                                # not include video thumbnail
                                if 'type' in image and image['type'] == 'video':
                                    continue
                                image_param = image['s']['n']
                                image_url = "https://boulanger.scene7.com/is/image/%s" % image_param
                                value.append(image_url)
                        elif type(images) == dict: # for 1 image
                            image_param = images['s']['n']
                            image_url = "https://boulanger.scene7.com/is/image/%s" % image_param
                            value.append(image_url)
                else:
                    api_url = f'https://boulanger.scene7.com/is/image//Boulanger/{gtin}_multi?req=set,json,UTF-8&id=197088501'
                    # if _json and 'image' in _json:
                    #     value.append(_json['image']) 
                    #     source = str(tag)
                    headers = {'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8', 'accept-encoding': 'gzip, deflate', 'accept-language': 'en-US,en;q=0.9', 'cache-control': 'no-cache', 'pragma': 'no-cache', 'referer': 'https://www.boulanger.com/','sec-ch-ua-mobile': '?0', 'sec-ch-ua-platform': '"Linux"', 'sec-fetch-dest': 'image', 'sec-fetch-mode': 'no-cors', 'sec-fetch-site': 'cross-site', "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"}
                    raw_data = self.downloader.extra_download(api_url, headers=headers)
                    if "Error while processing Fvctx image" not in raw_data:
                        _rgx = re.search(r'jsonResponse\((.*\}\})\,.*\)\;', raw_data)
                        if _rgx:
                            _json = json.loads(_rgx.group(1))
                            source = api_url
                            images = _json['set']['item']
                            if type(images) == list: # for many images
                                for image in images:
                                    # not include video thumbnail
                                    if 'type' in image and image['type'] == 'video':
                                        continue
                                    image_param = image['s']['n']
                                    image_url = "https://boulanger.scene7.com/is/image/%s" % image_param
                                    value.append(image_url)
                            elif type(images) == dict: # for 1 image
                                image_param = images['s']['n']
                                image_url = "https://boulanger.scene7.com/is/image/%s" % image_param
                                value.append(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            gtin = None
            tag = soup.find("span", {"itemprop":"gtin13"})
            if tag:
                gtin = tag.get_text().strip()
            else:
                tag = soup.find('script',text = re.compile('"@type":"Product"'))
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and 'gtin13' in _json.keys():
                        gtin = _json['gtin13']
            if gtin:
                api_url = "https://boulanger.scene7.com/is/image/Boulanger/%s_mixed?req=set,json,UTF-8&labelkey=label" % gtin
                raw_data = self.downloader.extra_download(api_url)
                if "Error while processing Fvctx image" not in raw_data:
                    _rgx = re.search(r'jsonResponse\((.*\}\})\,.*\)\;', raw_data)
                    if _rgx:
                        _json = json.loads(_rgx.group(1))
                        source = api_url
                        items = _json['set']['item']
                        if type(items) == list:
                            for item in items:
                                if 'type' in item and item['type'] == 'video':
                                    value.append('https://boulanger.scene7.com/e2/{}'.format(item['s']['n']))
            if len(value) == 0:
                tags = soup.find_all("video", {"class":"s7Video"})
                if tags:
                    source = str(tags)
                    for tag in tags:
                        if tag.has_attr('src'):
                            value.append(tag.get('src'))

                else:
                    tag = soup.select('[itemprop="gtin13"]')

                    if tag:
                        source = str(tag)
                        for i in tag:
                            value.append('https://boulanger.scene7.com/e2/Boulanger/{}_v_0'.format(i.get_text().strip()))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value
    
    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".delivery-purchase")

            if tag:
                source = str(tag)
                value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        rows = tags.select("td.characteristics-table")

        for row in rows:
            try:
                tag = row.select_one('.bold-title')
                if tag:
                    key = tag.get_text().replace(':', '').strip()
                    tag.decompose()
                    data = row.get_text().strip()
                
                result[key] = data

            except:
                continue

        return result

    def _check_tag(self, *tags):
        data = []
        for tag in tags:
            if tag:
                data.append(' '.join(tag.stripped_strings))

        return ' '.join(data)