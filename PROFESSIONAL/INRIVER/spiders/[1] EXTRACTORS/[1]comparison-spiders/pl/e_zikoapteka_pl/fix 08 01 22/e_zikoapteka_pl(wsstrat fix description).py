import logging
import re
import json, bs4

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class EZikoaptekaPlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.e-zikoapteka.pl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.find('span',{'class':'price-value'})

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="priceCurrency"]')

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="availability"]')
            if tag and tag.has_attr('href'):
                source, value = str(tag), tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = '5'
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        #Ratings and Reviews not supported

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('h1',{'itemprop':'name'})
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.dictionary-word-name')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag, desc = soup.select_one('div.tab.tab-product-long-desc'), list()
            if tag:
                # source, value = str(tag), ' '.join(tag.stripped_strings)
                for x in tag.contents:
                    if (x.name not in ['script', 'style']):
                        if (type(x) == bs4.element.Tag and
                            not x.attrs.get('style') in ['display: none;', 'display: inline;']):
                            # print('Tag -> ', f'({x.name}) attrs: ({x.attrs}) ', x.get_text())
                            desc.append(' '.join(x.stripped_strings))

                        elif (type(x) == bs4.element.NavigableString and 
                            'attrs' in x and
                            not x.attrs.get('style') in ['display: none;', 'display: inline;']):
                            # print('Navigable String -> ', x)
                            desc.append(x)

                if desc:
                    source = str(tag)
                    value = ' '.join([x for x in desc if x not in ['', '\n']])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select('.similar_panel.row.clear div.grid-3')
            if tag:
                source = str(tag)
                for spec in tag:
                    key = spec.select_one('.name')
                    val = spec.select_one('.active')
                    if key and val:
                        value[key.get_text(strip=True)] = val.get_text(strip=True)
                    else:
                        val = spec.select('a')
                        temp_val = []
                        if len(val):
                            for _val in val:
                                temp_val.append(_val.get_text(strip=True))
                        if temp_val:
                            value[key.get_text(strip=True)] = ' '.join(temp_val)                        

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('#variant_field_image a')
            if len(tags):
                source = str(tags)
                base_url = 'https://www.zikodermo.pl' #Can be change depending on the setup
                for img in tags:
                    if base_url not in img.get('href'):
                        value.append(base_url + img['href'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # website doesn't support delivery
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value
