import json
import logging
import re
from datetime import datetime
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class AoComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.ao.com'


    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)        

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)        

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        return result

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        price = None
 
        try:
            tag = soup.select_one('[itemprop="price"]')
            
            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        currency = None
        try:
            tag = soup.select_one('[itemProp="priceCurrency"]')
            
            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    #GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="availability"]')

            if tag and tag.has_attr('href'):
                source, value = str(tag), tag.get('href')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value 

        return source, value                   

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Extract score
            tag_score = soup.select_one('span[itemProp="ratingValue"]')
            tag_reviews = soup.select_one('[itemProp="reviewCount"]')

            if tag_score:
                score_source, score_value = str(tag_score),\
                                tag_score.get_text().strip()

            if tag_reviews:
                reviews_source, reviews_value = str(tag_reviews),\
                                    tag_reviews.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        title = None

        try:
            tag = soup.select_one('.title')

            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value


    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="brand"]')

            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value      
    
    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        description = []
        src = []
        try:

            tag = soup.select_one('[itemprop="description"]')
            if tag:
                description.append(' '.join(tag.stripped_strings))
                src.append(str(tag))
            #Additional Description
            tag2 = soup.select_one('#richContent')
            if tag2:
                description.append(' '.join(tag2.stripped_strings))
                src.append(str(tag2))
            
            if description and src:
                value = ' '.join(description)
                source = ' '.join(src)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            tag = soup.select_one('#mediaData')

            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text().strip())

                if 'images' in _json:
                    value = ['https:'+i['large'] for i in _json['images']]
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value



    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            key_tag = soup.select('#productSpecification .second')
            val_tag = soup.select('#productSpecification .third')

            if key_tag and val_tag:
                source, value = self._build_spec(key_tag, val_tag)
            else:
                tag = soup.select('[data-accordion="specification-features"]')
                if tag:
                    source = str(tag)
                    for specs in tag:
                        spec = specs.select('[data-tag-intersection="full product specification"] div')
                        for sp in spec:
                            if sp:
                                key_tag = sp.select_one('div.text-body-sm')
                                val_tag = sp.select_one('span.ml-auto')

                                if key_tag and val_tag:
                                    if 'More info' in key_tag.get_text(strip=True):
                                        value[key_tag.get_text(strip=True).split('More info')[0]] = val_tag.get_text(strip=True)
                                    else:
                                        value[key_tag.get_text(strip=True)] = val_tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value            


    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.deliveryDateDesktop')

            if tag:
                source, value = str(tag), tag.get_text().strip()
            else:
                tag = soup.select_one('span[class="hidden sm:inline-block md:hidden"]')
                if tag:
                    dq = tag.get_text(strip=True)
                    if '△' in dq:
                        value = dq.split('△')[0]
                    elif '&#9651;' in dq:
                        value = dq.split('&#9651;')[0]
                    else:
                        value = dq

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value       

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        try:
            tag = soup.find("script", text=re.compile(r'.*https\:\/\/schema.org/.*'), attrs= {"type":"application/ld+json"})
            if tag:
                source = str(tag)
                _json = json.loads(tag.get_text(strip=True))
                for data in _json:
                    if '@type' in data and 'VideoObject' in data['@type']:
                        value.append(data['embedUrl'])
            #description video
            #Inprogress
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value
        return source, value     


    @staticmethod
    def _build_spec(key_tag, val_tag):
        value = {k.get_text().strip():v.get_text().strip() for k,v in zip(key_tag, val_tag)}
        return str(key_tag+val_tag), value 