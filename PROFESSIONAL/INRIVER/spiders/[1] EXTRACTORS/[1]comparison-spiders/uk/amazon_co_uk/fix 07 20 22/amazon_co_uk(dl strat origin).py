from urllib.parse import urlparse
import json
import time
import re

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests

class AmazonCoUkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('uk')
        self.headers = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/'
                                    'apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'accept-encoding': 'gzip, deflate, br',
                        'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                        'cache-control': 'no-cache',
                        'pragma': 'no-cache',
                        'sec-fetch-mode': 'navigate',
                        'sec-fetch-site': 'none',
                        'sec-fetch-user': '?1',
                        'upgrade-insecure-requests': '1',
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                                        '(KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
                        }
    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None
        try:
            if not data:
                res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
            else:
                res = self.unblocker.post(url, timeout=timeout, headers=None, cookies=cookies, data=data)

            if 'offer-listing' in url:
                if 'id="olpProduct' not in res.text:
                    self.obtain_cookies(url)
                    res = self.requester.get(url, timeout=timeout, headers=self.headers)                    
                    
            else:
                if 'id="productTitle"' not in res.text: # Cookie generation
                    self.obtain_cookies(url)
                    res = self.requester.get(url, timeout=timeout, headers=self.headers)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break
            
                if 'offer-listing' in url:
                    if 'id="olpProduct' not in res.text:
                        is_redirect = True
                else:
                    if 'id="productTitle"' not in res.text: # PDP checker
                        is_redirect = True


                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))
        except Exception as e:
            print(e)

    def obtain_cookies(self, url):
        try:
            self.requester.session.headers.update(self.headers)

            timestamp = str(time.time()).replace('.','')[:13]
            url = urlparse('/'.join(url.split('/')[:6]))
            session_token_url = None
            csm_hit = None
            

            r = self.requester.get('%s://%s' % (url.scheme, url.hostname)) # Will obtain i18n-prefs, session-id, session-id-time cookies

            rgx = r"ue_id = '(.*)',.*ajaxParams = .*\{'uri':'(.*)'\},"
            rgx = re.search(rgx, r.text, re.DOTALL|re.MULTILINE)
            if rgx:
                csm_hit = re.search('(.*)\',', rgx.group(1)).group(1)

            self.requester.session.cookies.update(
                {'csm-hit':'tb:s-%s|%s&t:%s&adb:adblk_yes' % (csm_hit, timestamp, timestamp)}
                )
            ubid_url = '%s://%s%s' % (url.scheme, url.hostname, '/gp/overlay/display.html')


            data = {
                "origSessionId": self.requester.session.cookies['session-id'],
                "subPageType": "null",
                "pageType": "Detail",
                "ASIN": url.path.split('/')[-1],
                "path": url.path,
                "isAUI": "1",
                "referrer": "",
                "queryString": ""
            }

            self.requester.session.headers =  {
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                'content-length': '147',
                'content-type': 'application/x-www-form-urlencoded',
                'origin': '%s://%s' % (url.scheme, url.hostname),
                'referer': url.geturl(),
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                            '(KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',      
                'x-requested-with': 'XMLHttpRequest'
                }
                
            self.requester.post(ubid_url, data=json.dumps(data)) #Will obtain ubid-acbca, x-wl-uid cookies

            self.requester.session.headers = {
                'accept': 'text/html,*/*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                'cache-control': 'no-cache',
                'pragma': 'no-cache',
                'referer': url.geturl(),
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                            '(KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest'
                }
            session_token_url = '%s://%s/gp/product/ajax-handlers/reftag.html/ref=psd_bb_i_%s' % (url[0], url[1], url.path.split('/')[-1]) 

            self.requester.get(session_token_url) #Will Obtain session-token cookie
        except Exception as e:
            print('Unable to obtain cookies: %s' % e )
            pass
            
        return None