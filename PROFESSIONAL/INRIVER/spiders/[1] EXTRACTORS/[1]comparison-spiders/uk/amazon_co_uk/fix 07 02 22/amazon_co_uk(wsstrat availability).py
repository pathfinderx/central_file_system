import json
import logging
import re
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template, get_result_buybox_template


class AmazonCoUkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.amazon.co.uk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        buybox_template = get_result_buybox_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self.temp_raw_data = deepcopy(raw_data)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)  
        
        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)        

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)  

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)     

        # Extract Buybox
        result["buybox"] = self._get_buybox(soup, buybox_template)

        return result            

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            #check stock tag if OOS before fetching price
            tag = soup.select_one('#availability span')

            if tag:
                check_oos = tag.get_text().split(".")[0].strip()
                if check_oos == "Currently unavailable" or check_oos == 'P' or check_oos == '':
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value
                
                else:
                    price_tag = self.check_tag(
                        soup.select_one('[class="a-size-base a-color-price offer-price a-text-normal"]'),
                        soup.select_one('span.a-offscreen'),
                        soup.select_one('span.a-price.a-text-price.a-size-medium span.a-offscreen'),
                        soup.select_one('#priceblock_dealprice'),
                        soup.select_one('#priceblock_ourprice'),
                        soup.select_one('#price_inside_buybox') or soup.select_one('#newBuyBoxPrice'),
                        soup.select_one('#mbc-price-1'), # switched with the one above this
                        soup.select_one('#cerberus-data-metrics'),
                        # soup.select_one('span[data-action="show-all-offers-display"] span.a-color-price'),
                        # soup.select_one('.a-size-large.a-color-price.olpOfferPrice.a-text-bold'),
                        attrs = ['data-asin-price']
                    )

                    if price_tag:
                        source, value = price_tag
                        
                    #check if this pdp has 'buy used' in price force to -1. this rule is applicable to none MS Price(A00) amazon retailers only 01/02/2022
                    check_used = soup.select_one('[id="usedBuySection"]')
                    if check_used:
                        source = Defaults.GENERIC_NOT_FOUND.value
                        value = Defaults.GENERIC_NOT_FOUND.value
                        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('#attach-base-product-currency-symbol'),
                attrs=['data-asin-currency-code', 'value']
                )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[id="add-to-cart-button-ubb"]')
            if tag and tag.has_attr('value'):
                source, value = str(tag), tag['value']
            else:
                tag = soup.select_one('#availability')

                if tag:
                    for _script in tag.select('script'):
                        _script.decompose()
                    for _style in tag.select('style'):
                        _style.decompose()
                    source, value = str(tag), tag.get_text().strip()
            if not value:
                tag = soup.select_one('span[id="submit.add-to-cart-announce"]')
                if tag:
                    source, value = str(tag), tag.get_text(strip=True)
        
            # checker for undisabled button but oos (https://prnt.sc/yjbPqwsfZhvW)
            if value:
                availability_tag = soup.select_one('div#availability')
                stock_indicator = availability_tag.select_one('span.a-size-medium.a-color-state')
                
                if (availability_tag and stock_indicator and
                    re.search(r'Temporarily out of stock', availability_tag.get_text(strip=True))):
                    
                    source, value = str(availability_tag), stock_indicator.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = self.check_tag(
                soup.select_one('[data-hook="rating-out-of-text"]'),
                soup.select_one('#acrPopover'),
                soup.select_one('#olpProductDetails i.a-icon.a-icon-star span'),
                attrs = ['title']
            )
            tag_review = self.check_tag(
                soup.select_one('#acrCustomerReviewText'),
                soup.select_one('#olpProductDetails span.a-size-small a')
                )

            if tag_score and tag_review:
                reviews_source, reviews_value  = tag_review
                score_source, score_value = tag_score
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#productTitle') or \
                soup.select_one('#olpProductDetails .a-size-large.a-spacing-none')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)                            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('th', text=re.compile(r'Brand')) # Brand in Specs
            if tag:
                parent_tag = tag.find_parent('tr')
                if parent_tag:
                    brand_tag = parent_tag.select_one('td')
                    if brand_tag:
                        source = str(parent_tag)
                        value = brand_tag.get_text().strip()
            elif soup.find('span', text=re.compile(r'.*Brand.*')): # Brand in top Desc
                tag = soup.find('span', text=re.compile(r'.*Brand.*'))
                if tag:
                    parent_tag = tag.find_parent('tr')
                    if parent_tag:
                        brand_tag = parent_tag.select('td')
                        if len(brand_tag) == 2:
                            source = str(parent_tag)
                            value = brand_tag[-1].get_text().strip()
            else:
                tag = self.check_tag(
                    soup.select_one('#mbc'),
                    soup.select_one('#bylineInfo'),
                    attrs=['data-brand']
                )

                if tag:
                    source, value = tag
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _src = []
            description = ''
            tags = soup.select('div#feature-bullets ul li')
            if len(tags):
                _src.append(str(tags))
                for li in tags:
                    if not li.has_attr('id'):
                        description = description + ' ' + ' '.join(li.stripped_strings)

            tag = soup.find('div',{'id':'productDescription'})
            if tag:
                _src.append(str(tag))
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()
                description = description + ' ' + ' '.join(tag.stripped_strings)

            # tag = soup.find('div',{'class':'apm-centerthirdcol apm-wrap'})
            # if tag:
            #     _src.append(str(tag))
            #     description = description + ' ' + ' '.join(tag.stripped_strings)

            tags = soup.select('td.apm-top')
            if len(tags):
                _src.append(str(tags))
                for tag in tags:
                    description = description + ' ' + ' '.join(tag.stripped_strings)

            tags = soup.select('div.apm-sidemodule-textright')
            if tags:
                _src.append(str(tags))
                for tag in tags:
                    description = description + ' ' + ' '.join(tag.stripped_strings)

            tag = soup.find('div',{'class':'apm-sidemodule-textleft'})
            if tag:
                _src.append(str(tag))
                description = description + ' ' + ' '.join(tag.stripped_strings)
            
            soup = BeautifulSoup(self.temp_raw_data,'lxml')
            # From Manufacturer
            tag = soup.select_one('#dpx-aplus-product-description_feature_div') or soup.select_one('#aplus_feature_div')
            if tag:
                _src.append(str(tag))
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()
                description = description + ' ' + ' '.join(tag.stripped_strings)

            if description:
                source = " + ".join(_src)
                value = description

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag_key = self.check_tag(
                soup.select('#prodDetails tr td.label'),
                soup.select('.wrapper.CAlocale tr td.label'),
                soup.select('#productDetails_detailBullets_sections1 tr th'),
                soup.select('.tsRow span:nth-of-type(1)'),
                soup.select('#product-details-grid_feature_div tr td.label'),
                soup.select('#product-specification-table tr th'),
                soup.select('#productOverview_feature_div table td.a-span3') #specs from desc
                )
            tag_val = self.check_tag(
                soup.select('#prodDetails tr td.value'),
                soup.select('.wrapper.CAlocale tr td.value'),
                soup.select('#productDetails_detailBullets_sections1 tr td'),
                soup.select('.tsRow span:nth-of-type(2)'),
                soup.select('#product-details-grid_feature_div tr td.value'),
                soup.select('#product-specification-table tr td'),
                soup.select('#productOverview_feature_div table td.a-span9') #specs from desc
            )

            if tag_key and tag_val:
                source, value = self._build_spec(tag_key, tag_val)
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            # tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            # if tag:
            #     text = tag.get_text().strip().split('jQuery.parseJSON')[1]
            #     rgx = re.search('(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
            #     source = str(tag)
                
            #     if rgx:
            #         _json = json.loads(rgx.group(1).replace('l\\', ''))
            #         img_list = _json['landingAsinColor']
            #         if img_list in _json['colorImages']:
            #             value =[img['hiRes'] for img in _json['colorImages'][img_list] if 'hiRes' in img]
            #         else:
            #             tag = soup.find('script', text=re.compile('.*colorImages*'))
            #             if tag:
            #                 _json = tag.get_text().strip()
            #                 _json = _json.split("'colorImages':")[1]\
            #                         .split("'colorToAsin'")[0].strip()[:-1]\
            #                         .replace("'",'"')
            #                 _json = json.loads(_json)
            #                 source = str(tag)
            #                 if 'initial' in _json:
            #                     value = [i['hiRes'] for i in _json['initial']]
            
            tag = soup.find("script", text = re.compile(r"register\(\"ImageBlockATF\""), attrs = {"type":"text/javascript"})
            if tag:
                img_js_text = " ".join(tag.get_text().strip().replace("'", "\"").split())
                if img_js_text:
                    img_js_data = re.search(r"var data = ({.*});", img_js_text)
                    if img_js_data:
                        source = str(tag)
                        img = re.findall(r'\"large\".*?\"(.*?\.jpg)\",', img_js_data.group(1))
                        # for _img in img:
                        #     if _img not in value:
                        #         value.append(_img)
                        value = re.findall(r'\"large\".*?\"(.*?\.jpg)\",', img_js_data.group(1))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            if tag:
                text = tag.get_text().strip().split('jQuery.parseJSON')[1]
                rgx = re.search(r'(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
                source = str(tag)
                
                if rgx:
                    _json = json.loads(rgx.group(1).replace('l\\', ''))    
                    if 'videos' in _json:
                        value = [i['url'] for i in _json['videos']]
            
            tag = soup.select_one('.video-container')
            if tag:
                rgx = re.search(r'airyAssetConfig\["mediaSourceInfo"\].*=.*(\[.*\]);',tag.text.strip(), re.MULTILINE|re.DOTALL)
                if rgx:
                    items = re.findall(r'("[\s\S]+")',rgx.group(1))
                    if items:
                        for item in items:
                            value.append(item.replace('"',''))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value  

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one("#delivery-message"),
                soup.select_one("#ddmDeliveryMessage")
                )
            if tag:
                source, value = tag

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
             soup.select_one("#olp-upd-new-freeshipping .a-color-base"),
             soup.select_one('#shippingMessageInsideBuyBox_feature_div'),
             soup.select_one(".a-size-base.a-color-secondary")
             )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def _get_buybox(self, soup, template):
        buybox_result = deepcopy(template)

        try:
            buybox_tag = soup.select_one("#buybox") or soup.select_one(".a-box-group")

            if not buybox_tag:
                temp_tag = soup.select_one(".a-box")
                if 'a-alert' not in temp_tag.get('class'):
                    buybox_tag = temp_tag
            
            if buybox_tag:
                buybox_result['source'] = str(buybox_tag)

                # Extract text
                buybox_result['text'] = self.__get_sanitized_tag(buybox_tag)
                # Extract price and currency
                buybox_result["price"]["value"], buybox_result["price"]["currency"] = self.__get_buybox_price_and_currency(buybox_tag)
                # Extract delivery
                buybox_result["delivery"] = self.__get_buybox_delivery(buybox_tag)
                # Extract shipping
                buybox_result["shipping"] = self.__get_buybox_shipping(buybox_tag)
                # Extract availability
                buybox_result["availability"] = self.__get_buybox_availability(soup, buybox_tag)
                # Extract retailer
                buybox_result["retailer"] = self.__get_buybox_retailer(buybox_tag)
                # Extract used_price
                buybox_result["used_price"] = self.__get_buybox_used_price(soup, buybox_tag)
                # Extract new_price
                buybox_result["new_price"] = self.__get_buybox_new_price(soup, buybox_tag)
                # Extract cartable
                buybox_result["cartable"]['source'], buybox_result["cartable"]['value'] = self.__get_buybox_cartable(soup, buybox_tag)
                # Extract buyable
                buybox_result["buyable"]['source'], buybox_result["buyable"]['value'] = self.__get_buybox_buyable(soup, buybox_tag)
                # Extract add_on
                buybox_result["add_on"] = self.__get_buybox_add_on(buybox_tag)            
                
        except Exception as e:
            self.logger.exception(e)
            buybox_result['text'] = FailureMessages.BUYBOX_EXTRACTION.value

        return buybox_result

    def __get_buybox_price_and_currency(self, buybox_tag):
        price = Defaults.GENERIC_NOT_FOUND.value
        currency = "EUR" # defaulted

        try:
            tag = buybox_tag.select_one("#price_inside_buybox") or\
                        buybox_tag.select_one("#newBuyBoxPrice") or\
                        buybox_tag.select_one("#unqualifiedBuyBox .a-color-price")

            if not tag:
                tag = buybox_tag.select_one(".a-color-price")
                if tag:
                    if "a-size-medium" not in tag.get("class") or \
                    tag.parent.get("id") in ["availability", "outOfStock"]:
                        tag = None

            if tag:
                # sample : 80,90 €
                price = tag.get_text().strip()
                currency = "EUR" # defaulted
            
            if not bool(re.search('\d', price)):
                tag = buybox_tag.select_one('.a-price.a-text-price.a-size-medium span') or \
                    buybox_tag.select_one('.a-offscreen')

                if tag:
                    price = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            price = FailureMessages.BUYBOX_EXTRACTION.value
            currency = FailureMessages.BUYBOX_EXTRACTION.value

        return price, currency
    
    def __get_buybox_delivery(self, buybox_tag):
        delivery = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#ddmDeliveryMessage")
            if tag:
                delivery = " ".join(tag.stripped_strings)
            
        except Exception as e:
            self.logger.exception(e)
            delivery = FailureMessages.BUYBOX_EXTRACTION.value

        return delivery

    def __get_buybox_shipping(self, buybox_tag):
        shipping = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#price-shipping-message")
            if tag:
                shipping= tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            shipping = FailureMessages.BUYBOX_EXTRACTION.value

        return shipping
    
    def __get_buybox_availability(self, soup, buybox_tag):
        availability = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#availability") or buybox_tag.select_one("#outOfStock .a-text-bold") or soup.select_one("#availability")
                
            if tag:
                availability = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            availability = FailureMessages.BUYBOX_EXTRACTION.value

        return availability
    
    def __get_buybox_retailer(self, buybox_tag):
        retailer = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#merchant-info")
            if tag:
                if tag.select_one('#sellerProfileTriggerId'):
                    retailer = tag.select_one('#sellerProfileTriggerId').get_text().strip()
                else:
                    retailer = re.sub(r'[^\w ]+', '', tag.get_text()
                                                .split("by ")[-1].split(".")[0].split('(')[0]).strip() # 2M (1-3 Days Delivery)
            elif buybox_tag.select_one('#sellerProfileTriggerId'):
                retailer = buybox_tag.select_one('#sellerProfileTriggerId').get_text().strip()
            else:
                retailer = "Amazon" # Defaulted in Ultima
            
        except Exception as e:
            self.logger.exception(e)
            retailer = FailureMessages.BUYBOX_EXTRACTION.value

        return retailer

    def __get_buybox_used_price(self, soup, buybox_tag):
        used_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #usedPrice")
            if tag:
                used_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            used_price = FailureMessages.BUYBOX_EXTRACTION.value

        return used_price
    
    def __get_buybox_new_price(self, soup, buybox_tag):
        new_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #newBuyBoxPrice")
            if tag:
                new_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            new_price = FailureMessages.BUYBOX_EXTRACTION.value

        return new_price

    def __get_buybox_cartable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        cartable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box .a-icon-cart")
            if tag:
                source = str(tag)
                cartable = str(True)
            else:
                cartable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            cartable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, cartable

    def __get_buybox_buyable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        buyable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-icon-buynow")
            if tag:
                source = str(tag)
                buyable = str(True)
            else:
                buyable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            buyable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, buyable

    def __get_buybox_add_on(self, buybox_tag):
        add_on = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = buybox_tag.select(".abbListItem")
            if tags:
                add_on = ", ".join([self.__get_sanitized_tag(tag) for tag in tags])
            
        except Exception as e:
            self.logger.exception(e)
            add_on = FailureMessages.BUYBOX_EXTRACTION.value

        return add_on            

    @staticmethod
    def _build_spec(key_tag, val_tag):
        # Sanitize
        for _key in key_tag:
            for i in ['script', 'style']:
                for j in _key.select(i):
                    j.extract()

        for _key in val_tag:
            for i in ['script', 'style']:
                for j in _key.select(i):
                    j.extract()

        value = {k.get_text().strip():re.sub('.zg_hrsr.*', '', v.get_text().replace('\n','').replace('\xa0','').strip()) for k, v in zip(key_tag, val_tag)}
        return str(key_tag+val_tag), value

    @staticmethod
    def check_tag(*tags, attrs=[]):
        for tag in tags:
            if not isinstance(tag, list):
                if tag and attrs:
                    for attr in attrs:
                        if tag.has_attr(attr):
                            if len(tag.get(attr)) > 0:
                                return str(tag), tag.get(attr)
                        else:
                            if len(tag.get_text().strip()) > 0:
                                return str(tag), " ".join(tag.stripped_strings)
                elif tag:
                    return str(tag), " ".join(tag.stripped_strings)
            else:
                if tag:
                    return tag
        return None
    
    @staticmethod
    def __get_sanitized_tag(tag):
        tags_to_extract = ['script', 'style']

        for i in tags_to_extract:
            for j in tag.select(i):
                j.extract()

        return ' '.join(tag.stripped_strings)