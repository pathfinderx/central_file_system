import re, json
import logging

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

import js2py

class JohnlewisComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.johnlewis.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.product_data = None

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self.product_data = self.get_api_product_data(soup)
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability - same value with delivery. Displays stock sometimes in delivery element
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, _availability=result["availability"]["value"]) # pass availability as argument for delivery exctraction

        return result

    def get_json(self, soup): # for price
        try:
            tag = soup.select_one('[type="application/ld+json"]')
            source = ''
            _json = {}
            
            if tag:
                _json = json.loads(tag.get_text().strip())
                source = str(tag)
                
            return _json, source
        except:
            _json = json.loads(tag.get_text().strip().replace('&quot;','"'))
            source = str(tag)
                
            return _json, source

    def get_product_data_json(self, soup): # alternative source for price
        tag = soup.select_one('script#__PRODUCT_DATA__')
        try:
            if tag: 
                _json = json.loads(tag.get_text())
                return _json
        except:
            pass
        return None

    def get_product_next_data(self, soup): # for video
        try:
            tag = soup.select_one('script[type="application/json"]#__NEXT_DATA__')
            if tag:
                _json = json.loads(tag.get_text())
                return _json

        except:
            pass
        return None

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)
            # If price is not present in PDP do not scrape.
            if soup.select_one('.prices-container .price') or soup.select_one('[data-test="product-price"]'):  # For Hidden Prices for OOS Products
                if 'offers' in _json and 'price' in _json['offers']:
                    value = str(_json['offers']['price'])
                elif 'offers' in _json and 'lowPrice' in _json['offers']:
                    value = str(_json['offers']['lowPrice'])
            else:
                if 'offers' in _json and 'price' in _json['offers'] and soup.select_one('span.ProductPrice__item--1p_iL'):
                    value = str(_json['offers']['price'])

            if value == 'NOT_FOUND':
                tag = soup.select('.ProductPrice_ProductPrice__item__Gh-Gm')
                if tag and len(tag) > 1:
                    tag = tag[-1]
                    source = str(tag)
                    value = tag.get_text().strip()
                else:
                    _json = self.get_product_data_json(soup)
                    if _json:
                        value = _json['price']
                        source = str(_json)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)

            if 'offers' in _json and 'priceCurrency' in _json['offers']:
                value = _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value
    
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # prioritze new source
            tag = soup.select_one('button[data-testid="basket:add"]') or soup.select_one('section[data-cy="email-me"] h2')
            if tag:
                value = tag.get_text().strip()
                source = str(tag)

            if value == 'NOT_FOUND':
                _json, source = self.get_json(soup)

                if 'offers' in _json and 'availability' in _json['offers']:
                    value = _json['offers']['availability']

                if value == Defaults.GENERIC_NOT_FOUND.value:
                    url, payload, headers = self._get_prerequisites(soup)
                    res = self.downloader.download(url, data=payload, headers=headers)
                    _json = json.loads(res)
                    if _json['stocks'][0]['stockQuantity']:
                        source, value = url, str(_json['stocks'][0]['stockQuantity'])
                    else:
                        source, value = url, str(_json['stocks'][0]['availabilityStatus'])


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)
            # score_source = reviews_source = source

            if 'aggregateRating' in _json:
                if 'reviewCount' in _json['aggregateRating'] and 'ratingValue' in _json['aggregateRating']:
                    reviews_value = str(_json['aggregateRating']['reviewCount'] )
                    score_value = str(_json['aggregateRating']['ratingValue']  )
                    score_source = reviews_source = source
            else:
                if 'productId' in _json:
                    _json_ratings_str = self.downloader.get_ratings(_json['productId'])
                    _json_ratings = json.loads(_json_ratings_str)
                    if _json_ratings and 'url' in _json_ratings and 'score' in _json_ratings and 'review' in _json_ratings:
                        reviews_value =_json_ratings['review']
                        score_value = _json_ratings['score']
                        score_source = reviews_source = _json_ratings['url']             
                        
            if score_value and reviews_value == Defaults.GENERIC_NOT_FOUND.value:
                tag = soup.find('script',{'id':'__NEXT_DATA__'},type = 'application/json')
                if tag:
                    _json = json.loads(tag.text.strip())
                    if 'props' in _json.keys() and 'pageProps' in _json['props'] and 'product' in _json['props']['pageProps'] \
                        and 'reviewSummary' in _json['props']['pageProps']['product']:
                        _data = _json['props']['pageProps']['product']['reviewSummary']
                        
                        if 'averageRating' in _data.keys() and 'numberOfReviews' in _data.keys():
                            #Rating
                            score_value = str(_data['averageRating'])
                            score_source = str(_data)
                            #Review
                            reviews_value = str(_data['numberOfReviews'])
                            reviews_source = str(_data)
                        
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)
            if 'color' in _json:
                if 'name' in _json:
                    name = _json['name']
                    if not _json['color']:
                        value = _json['name']
                    else:
                        if name and _json['color'] in name:
                            value = _json['name']
                        else:
                            value = name + ', ' + _json['color']
            else:
                if 'name' in _json:
                    name = _json['name']
                    if name == 'John Lewis & Partners':
                        tag = soup.select_one('.Title_titleOld__1s82A') or soup.select_one('[data-testid="product\:title"]')
                        if tag:
                            source, value = str(tag), tag.get_text(strip=True)
                    else:
                        value = name


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)

            if 'brand' in _json and 'name' in _json['brand']:
                value = _json['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value        

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json, source = self.get_json(soup)

            if 'description' in _json:
                value = _json['description']

                # create a soup instance so that we can remove the tags
                description_soup = BeautifulSoup(value, 'lxml')
                description = ''

                for stripped in description_soup.stripped_strings:
                    description += str(stripped) + ' '

                value = description
            else:
                tag = soup.select_one('[data-testid="description:content"]')
                if tag:
                    _text = re.sub(r'[\n\t]*', "", ' '.join(tag.stripped_strings)) # remove (\n,\t() in text
                    source, value = str(tag), _text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.select("div.ProductSpecification_productSpecificationListItem__2Za7A") \
                or soup.select_one("dl.ProductSpecification_productSpecificationList__3M1Qn")

            if tag:
                source = str(tag)
                if isinstance(tag, list):
                    specification_titles = [spec.select_one("span.ProductSpecification_productSpecificationListLabelName__3zwM2").getText() for spec in tag]
                    specification_values = [spec.select_one("dt.ProductSpecification_productSpecificationListValue__12cdU").getText() for spec in tag]
                elif tag.name == 'dl':
                    specification_titles = [title.getText() for title in tag.select('dd.ProductSpecification_productSpecificationListLabel__5-gkO')]
                    specification_values = [title.getText() for title in tag.select('dt.ProductSpecification_productSpecificationListValue__12cdU')]

                for idx in range(0, len(specification_titles)): 
                    value[specification_titles[idx]] = specification_values[idx]
            else:
                parent_tag = soup.select_one('.ProductSpecificationList_productSpecificationListContainer__epw8X')
                if parent_tag:
                    _keys = parent_tag.select('[data-cy="product-spec-attr"]')
                    _vals = parent_tag.select('[data-cy="product-spec-attr-value"]')
                    if len(_keys) and len(_vals):
                        source = str(parent_tag)
                        for idx in range(0, len(_keys)):
                            value[str(_keys[idx].get_text())] = str(_vals[idx].get_text())

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('.product-images .product-images__item img') 
            
            if tags:
                source, value = str(tags), ['https:'+i.get('data-large-image') for i in tags if i.has_attr('data-large-image')]
            if not tags:
                tags = soup.select('.ImageMagnifier_zoomable-image-container__db7jH > div > img')
                if tags:
                    source = ''.join(str(ii) for ii in tags)
                    for _img in tags:
                        if _img.has_attr('src'):
                            value.append(_img.get('src'))
            if not tags:
                tags = soup.select('li.product-media__item img')
                if tags:
                    source, value = str(tags), ['https:'+i.get('data-large-image') for i in tags if i.has_attr('data-large-image')]
                    _json, temp_source = self.get_json(soup)
                    if _json and 'image' in _json:
                        value.append(_json['image'])
            if not tags:
                tags = soup.select('.Layout_image__1LfSG img[class^="StandardGallery_galleryImage"]')
                if tags:
                    source = str(tags)
                    for img in tags:
                        if img.has_attr('src') and 'prod_vid_thmb' not in img['src']:
                            base = 'https:'
                            value.append(base + img.get('src'))
                else:
                    tags = soup.select('.ImageMagnifier_image-wrapper__GhoSr img')
                    if  tags:
                        source = str(tags)
                        for tag in tags:
                            if tag and tag.has_attr('src'):
                                value.append(tag['src'])
            if not tags:
                tags = soup.select('[data-testid="thumbnail-image"] img')
                if  tags:
                    source = str(tags)
                    for tag in tags:
                        if tag and tag.has_attr('src'):
                            image = tag['src']
                            if 'https:' not in image:
                                base = 'https:'
                                value.append(base + image)
                            else:
                                value.append(image)

            #Comment for now . Causing Incorrect image count
            #     else:
            #         _json, source = self.get_json(soup)
            #         if _json and 'image' in _json:
            #             value.append(_json['image'])

            # if len(value) == 1:
            #     tags = soup.select('.media')

            #     if tags:
            #         source = str(tags)
            #         value = ['https:{}'.format(i['src']) for i in tags if i.has_attr('src')]
                    
            if not value:
                tags = soup.select('main .slick-slide .zoom img')
                if tags:
                    for img in tags:
                        value.append(f'https:{img["src"]}')
                        
            tags = soup.select('.slick-list .slick-slide img')
            
            if tags and len(value) != len(tags):
                values = []
                
                for tag in tags:
                    if tag.has_attr('src'):
                        if tag.get('src').startswith('https'):
                            img  = tag.get('src')
                        else:
                            img = 'https:{}'.format(tag.get('src'))
                            
                        values.append(img)
                        
                # Remove None values
                value = [i for i in values if i is not None]


            if value == [] and source == Defaults.GENERIC_NOT_FOUND.value:
                img_container = soup.find('div', {'class': 'Thumbnails_thumbnailTrack__3l0SD'}) 
                if img_container:
                    imgs = img_container.find_all('button')
                    source = str(img_container)
                    if imgs:
                        for img in imgs:
                            url = img.img['src'].split('?')[0] if ['Thumbnails_video__3QRxc','Thumbnails_video_3QRxc'] not in img['class'] and 'vid' not in img.img['src'] else ''
                            value.append(f'https:{url}') 
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            
        # check if there are videos in the pdp
            pdp_video = False
            pdp_images = soup.select('.carousel_c-carousel__pagination__35F8J li')
            if pdp_images:
                for li in pdp_images:
                    data_test_id = li.select_one('button').get('data-testid')
                    if data_test_id == 'thumbnail-video':
                        pdp_video = True
                        break
            else:
                pdp_images_button = soup.select_one('.Thumbnails_thumbnailTrack__3l0SD button.Thumbnails_button__3ZBm2.Thumbnails_thumbnailImage__1hMyG.Thumbnails_active__1Gio1') \
                                    or soup.select_one('.VideoButton_videoButton__9_bsA')
                if pdp_images_button:
                    pdp_video = True
                    
            
            if not pdp_video:
                 return source, value 
                        
            # prioritize new source
            _json = self.get_product_next_data(soup)
                # if 'props' in _json and 'pageProps' in _json['props'] and 'videos' in _json['props']['pageProps']:
                #     videos = _json['props']['pageProps']['videos']
                #     for vid in videos:
                #         value.append(vid['videoUrl'])
                #     source = str(_json)
                # this commented code refactored below.

            if 'props' in _json and 'pageProps' in _json['props']:
                page_props = _json['props']['pageProps']
                if  'videos' in page_props:
                    videos = page_props['videos']
                    for vid in videos:
                        if 'https:' in vid['videoUrl']:
                            value.append(vid['videoUrl'])
                        else:
                            value.append('https:' + vid['videoUrl'])
                    source = str(_json)
                elif 'product' in page_props and 'media' in page_props['product']:
                    media = page_props['product']['media']
                    if media and 'videos' in media:
                        videos = media['videos']
                        for video in videos:
                            for source in video['sources']:
                                if source['key'].lower() == 'stream':
                                    url = source['url']
                                    value.append(f'https:{url}')
                        source = str(_json)
                        
            if not value:
                tags = soup.select('video.product-video source')

                if tags:
                    source = str(tags)
                    value = ['https:{}'.format(i['src']) for i in tags if i.has_attr('src')]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value        

    def _get_delivery(self, soup, _availability=None):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = soup.select_one(".a--progress-container")
            if tags:
                tags1 = tags.select_one('p.a--progress-copy.a--progress-copy--bold')
                if tags1:
                    source = str(tags)
                    value = tags1.get_text().strip()
            
            if not tags:
                tag = soup.select_one(".enriched-delivery-table")
                if tag:
                    source, value = str(tag), tag.get_text().strip()

            if (soup and 
                _availability and
                value in [Defaults.GENERIC_NOT_FOUND.value]):

                availability_in_product_data = None
                availability_indicators = ['Add to your basket'] # add more availability indicators from _availability args from result["availability"]["value"] template

                try: 
                    # extract availability from self.product_data as optional availability indicator in case if argument: _availability is null
                    if (self.product_data and
                        'variants' in self.product_data and
                        isinstance(self.product_data['variants'], list) and
                        len(self.product_data['variants']) > 0 and
                        'availability' in self.product_data['variants'][0] and 
                        'availableToOrder' in self.product_data['variants'][0]['availability']):

                        availability_in_product_data = self.product_data['variants'][0]['availability']['availableToOrder']

                    js_url = f'https://www.johnlewis.com/electricals-ui/_next/static/chunks/pages/pdp-6a37bb6ff7edd956.js' # request url for javascript code (contains delivery indicator)
                    res = self.downloader.download(js_url)

                    if (res):
                        rgx = re.search(r'(t\.availability\.availableToOrder\?[\s\S]+?),', res) # find the ternary operation from javascript code that resembles the delivery
                        
                        isAvailable = True if (_availability in availability_indicators or availability_in_product_data) else False
                        indicator_script = f'availability=true\navailability' if isAvailable else f'availability=false\navailability'
                        ternary_operation_script = rgx.group(1).replace('t.availability.availableToOrder', indicator_script)

                        js_py = js2py.eval_js(ternary_operation_script) # execute js ternary operation

                        if (js_py):
                            source = str(rgx)
                            value = js_py

                except Exception as e:
                    print('Error at wsstrat: at _get_delivery() -> with message: ', e)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
    
    def _build_spec(self, key_tag, val_tag):
        value = {k.contents[0].strip():v.get_text().strip() for k,v in zip(key_tag, val_tag)}
        return value, str(key_tag+val_tag)

    
    def _get_prerequisites(self, soup):
        api_url = 'https://www.johnlewis.com/electricals-ui/api/stock/v2'
        headers = {
            "accept": "*/*",
            "accept-encoding": "gzip, deflate",
            "accept-language": "en-US,en;q=0.9",
            "cache-control": "no-cache",
            "content-type": "application/json",
            "origin": "https://www.johnlewis.com",
            "pragma": "no-cache",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "\
                "(KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
        }
        payload = {
            'skus':[]
        }
        try:
            tag = soup.select_one('[property="og:image"]')

            if tag and tag.has_attr('content'):
                content = tag['content'].split('?')[0].split('/')[-1]
                if 'alt' in content:
                    content = content.split('alt')[0]
                payload['skus'].append(content)
                payload = json.dumps(payload)

            tag = soup.select_one('link[rel="canonical"]')

            if tag and tag.has_attr('href'):
                headers['referer'] = tag['href']
                
        except Exception as e:
            print('Failed to get stock prerequisites')

        return api_url, payload, headers

    def get_api_product_data (self, soup): # for delivery, price, ratings, etc.
        try:
            script_tag, _json = soup.select_one('script[type="application/ld+json"]'), None
            _json = json.loads(script_tag.get_text(strip=True)) if script_tag else None

            if (_json and
                'sku' in _json and
                'productId' in _json):

                request_url = f'https://api.johnlewis.com/catalogue/graphql?key=AIzaSyDtVsqXz2-LpOo5-RFYiAa2InbnSyfNZAM&query=query%20getByProductIds($productIds:%20String!,%20$skuIds:%20String!)%7B%20activeAndRetiredProducts(%20where:%20%7B%20pimProductIds:%20%5B%20$productIds%20%5D%20%7D%20)%20%7B%20activeProducts%20%7B%20__typename%20...%20on%20SimpleProduct%20%7B%20productTypeHierarchy%20%7D%20...%20on%20CompositeBundle%20%7B%20productTypeHierarchy%20%7D%20title%20unitOfMeasure%20variantPriceRange%20%7B%20for%20display%20%7B%20min%20max%20%7D%20value%20%7B%20min%20max%20%7D%20reductionHistory%20%7B%20chronology%20display%20%7B%20min%20max%20%7D%20%7D%20%7D%20aliases%20%7B%20pimProductId%20%7D%20browsePageHierarchy%20%7B%20name%20%7D%20variants%20(where:%20%7B%20skuIds:%20%5B$skuIds%5D%20%7D)%7B%20...%20on%20CompositeBundleVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20sellingRestrictions%20%7B%20highFatSugarSalt%20%7D%20%7D%20...%20on%20StaticBundleVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20sellingRestrictions%20%7B%20highFatSugarSalt%20%7D%20%7D%20...%20on%20StockVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20sellingRestrictions%20%7B%20highFatSugarSalt%20%7D%20differentiators%20%7B%20colour%20size%20%7D%20%7D%20id%20title%20images%20%7B%20primary%20%7D%20price%20%7B%20for%20value%20display%20reductionHistory%20%7B%20chronology%20value%20%7D%20%7D%20availability%20%7B%20availableToOrder%20%7D%20parentProduct%20%7B%20variants%20%7B%20availability%20%7B%20availableToOrder%20%7D%20%7D%20%7D%20%7D%20reviewSummary%20%7B%20averageRating,%20numberOfReviews%20%7D%20%7D%20retiredProducts%20%7B%20__typename%20title%20aliases%20%7B%20pimProductId%20%7D%20browsePageHierarchy%20%7B%20name%20%7D%20variants%20(where:%20%7B%20skuIds:%20%5B$skuIds%5D%20%7D)%7B%20...%20on%20RetiredCompositeBundleVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20%7D%20...%20on%20RetiredStaticBundleVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20%7D%20...%20on%20RetiredStockVariant%20%7B%20aliases%20%7B%20skuId%20%7D%20differentiators%20%7B%20colour%20size%20%7D%20%7D%20id%20title%20images%20%7B%20primary%20%7D%20%7D%20%7D%20%7D%20%7D&variables=%7B%22productIds%22:%22{_json.get("productId")}%22,%22skuIds%22:%22{_json.get("sku")}%22%7D'

                res = self.downloader.download(request_url)

                if (res and isinstance(res, str)):
                    product_data_json = json.loads(res)

                    if (product_data_json and 
                        'data' in product_data_json and 
                        'activeAndRetiredProducts' in product_data_json['data'] and
                        'activeProducts' in product_data_json['data']['activeAndRetiredProducts'] and
                        isinstance(product_data_json['data']['activeAndRetiredProducts']['activeProducts'], list) and
                        len(product_data_json['data']['activeAndRetiredProducts']['activeProducts']) > 0 and
                        'variants' in product_data_json['data']['activeAndRetiredProducts']['activeProducts'][0]):

                        return product_data_json['data']['activeAndRetiredProducts']['activeProducts'][0]
                
        except Exception as e:
            print('Error at wsstrat: get_api_product_data() -> with message: ', e)

        