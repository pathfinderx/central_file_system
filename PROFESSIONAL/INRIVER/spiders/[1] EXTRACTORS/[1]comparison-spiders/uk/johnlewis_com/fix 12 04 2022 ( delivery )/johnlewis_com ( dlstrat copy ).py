import json

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class JohnlewisComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
                'sec-ch-ua-mobile': '?0',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
        
    def get_ratings(self, prod_id):

        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'origin': 'https://www.johnlewis.com',
            'referer': 'https://www.johnlewis.com/',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36'
        }

        try:
            url = 'https://api.johnlewis.com/ratings-reviews/v1/reviews?productId={}&pageNumber=1&pageSize=5&sort=newest&api_key=AIzaSyBhAj6yFnAO-9ZmmntqfnBzHsj4RvIqOew'.format(prod_id)
            res = self.requester.get(url, headers=headers)
            
            _json = json.loads(res.text)
            
            ratings = {}
            
            if _json and 'overallRatings' in _json and 'value' in _json['overallRatings'] and 'count' in _json['overallRatings']:
                ratings['url'] = url
                ratings['score'] = str(_json['overallRatings']['value'])
                ratings['review'] = str(_json['overallRatings']['count'])
                return json.dumps(ratings)
            
            return None
    
        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
