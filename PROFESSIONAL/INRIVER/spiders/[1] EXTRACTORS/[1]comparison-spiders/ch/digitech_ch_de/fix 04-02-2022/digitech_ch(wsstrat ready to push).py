import json
import logging
import re

from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class DigitecChDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.digitec.ch/de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pdp_tag = soup.select_one('[class="productPricing_price__1HTkJ"]') or soup.select_one('.productDetail .sc-1kk87y0-1') \
                    or soup.select_one('.productDetail .sc-algx62-1.liOVwN') or soup.select_one('div.sc-16okr1k-0.gRAvyG span') \
                    or soup.select_one('div.productDetail div.gabtXl strong')    
                        # CHECK IF PRICE DISPLAYED IN PDP
            tag = soup.findAll('script',type="application/ld+json")[2]
            _json = json.loads(tag.get_text())
            if _json and 'offers' in _json and 'price' in _json['offers'] and pdp_tag:
                source = str(tag)
                value = _json['offers']['price']
            elif pdp_tag:
                source,value = str(pdp_tag),pdp_tag.text.strip()

            if value == '' or value == 'NOT_FOUND':
                tag = soup.select_one('.sc-algx62-1.cwhzPP') or soup.select_one('.sc-18ppxou-4.eUvjHX')
                if tag:
                    first_span_tag = tag.next_element
                    if first_span_tag:
                        second_span_tag = first_span_tag.next_element.get_text().strip()
                        source, value = str(tag), second_span_tag if second_span_tag else None
                    else:
                        source, value = str(tag), tag.get_text().strip()
            # else:
            #     if _json and 'offers' in _json and 'price' in _json['offers']:
            #         source = str(tag)
            #         value = _json['offers']['price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.findAll('script',type="application/ld+json")[2]
            _json = json.loads(tag.get_text())
            if 'offers' in _json:
                source = str(tag)
                value = _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('meta[property="og:availability"]')
            if tag:
                if tag.has_attr('content'):
                    value = str(tag['content'])
                    source = str(tag)
            else:
                tag = soup.select_one('div[data-test="header-availability"]')
                if tag:
                    value = tag.get_text().strip()
                    source = str(tag)

            if value == 'NOT_FOUND':
                tag = soup.select_one('#addToCartButton:not([disabled])')
                if tag:
                    source, value = str(tag),tag.text.strip()
                else:
                    tag = soup.findAll('script',type="application/ld+json")[2]
                    _json = json.loads(tag.get_text())
                    if 'offers' in _json:
                        source = _json['offers']
                        value = _json['offers']['availability']

            if value == Defaults.GENERIC_NOT_FOUND.value:
                tag = soup.select_one('span.availabilityText')
                if tag:
                    value = tag.get_text(strip=True)
                    source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.findAll('script',type="application/ld+json")[2]
            _json = json.loads(tag.get_text())
            if 'aggregateRating' in _json:
            #Ratings
                score_value = str(_json['aggregateRating']['ratingValue'])
                score_source = str(_json['aggregateRating'])
            #Reviews
                reviews_value = str(_json['aggregateRating']['ratingCount'])
                reviews_source = str(_json['aggregateRating'])
            else:
                
                # Ratings
                score_tag = soup.select_one('meta[property="og:rating"]')
                if score_tag and score_tag.has_attr('content'):
                    score_source,score_value = str(score_tag),str(score_tag['content'])

                # Reviews
                reviews_tag = soup.select_one('meta[property="og:rating_count"]')
                if reviews_tag and reviews_tag.has_attr('content'):
                    reviews_source,reviews_value = str(reviews_tag),str(reviews_tag['content'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.findAll('script',type="application/ld+json")[2]
            _json = json.loads(tag.get_text())
            if 'name' in _json:
                source = _json['name']
                value = _json['name']
                if value == 'digitec':
                    tag = soup.select_one('.sc-jqo5ci-0.dohIju span') or soup.select_one('title')
                    if tag:
                        source,value = str(tag),tag.text.strip()
            else:
                tag = soup.select_one('.sc-jqo5ci-0.dohIju span') or soup.select_one('title')
                if tag:
                    source,value = str(tag),tag.text.strip()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('meta[property="og:brand"]')
            if tag:
                if tag.has_attr('content'):
                    value = str(tag['content'])
                    source = str(tag)
            else:
                tag = soup.select_one('div.productDetail .sc-jqo5ci-0 strong')
                if tag:
                    value = tag.get_text().strip()
                    source = str(tag)

            if value == 'NOT_FOUND':
                tag = soup.findAll('script',type="application/ld+json")[2]
                _json = json.loads(tag.get_text())
                if 'brand' in _json:
                    source = _json['brand']
                    value = _json['brand']['name']
                else:
                    tag = soup.select_one('.sc-jqo5ci-0.dohIju strong')
                    if tag:
                        source,value = str(tag),tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            # tag = soup.findAll('script',type="application/ld+json")[2]
            # _json = json.loads(tag.get_text())
            # if 'description' in _json:
            #     source = _json['description']
            #     value = _json['description']

            tag = soup.find('script',{'id':'__NEXT_DATA__'},type = 'application/json')
            if tag:
                _json = json.loads(tag.text.strip())
                if _json and 'props' in _json.keys() and 'apolloState' in _json['props']:
                    temp_dict = {}
                    root_data = _json['props']['apolloState']['ROOT_QUERY'] if 'ROOT_QUERY' in _json['props']['apolloState'] else None
                    for _key in _json['props']['apolloState']:
                        if 'productDetails' in _key:
                            if 'marketingDescription' in _json['props']['apolloState'][_key]:
                                temp_dict = _json['props']['apolloState'][_key]
                                break
                    if not temp_dict and root_data:
                        key_directory = [_key for _key in root_data if 'productDetailsV3' in _key]
                        if key_directory:
                            if 'productDetails' in root_data[key_directory[0]] and 'marketingDescription' in root_data[key_directory[0]]['productDetails']:
                                temp_dict = root_data[key_directory[0]]['productDetails']

                    if temp_dict:
                        source = str(tag)
                        desc.append(temp_dict['marketingDescription'])
            if desc:
                value = ' '.join(desc)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:

            tag = soup.find('script',{'id':'__NEXT_DATA__'},type = 'application/json')
            if tag:
                _json = json.loads(tag.text.strip())
                if _json and 'props' in _json.keys() and 'apolloState' in _json['props']:
                    temp_id_list = []
                    for _key in _json['props']['apolloState']: #get specification keys for _json to fetch properties
                        if 'productDetails' in _key:
                            if 'specifications' in _json['props']['apolloState'][_key] and _json['props']['apolloState'][_key]['specifications']:
                                for item in _json['props']['apolloState'][_key]['specifications']:
                                    temp_id_list.append(item['id'])
                                break

                    if temp_id_list:
                        source = str(tag)
                        for _ids in temp_id_list:

                            # variables to navigate in _json
                            temp_properties = []      


                            if 'properties' in _json['props']['apolloState'][_ids] and _json['props']['apolloState'][_ids]['properties']:
                                for props in _json['props']['apolloState'][_ids]['properties']:
                                    if 'id' in props:
                                        temp_properties.append(props['id'])

                            if temp_properties: 
                                for prop in temp_properties:
                                    temp_values = []

                                    # variables to add to dict
                                    temp_result_key = ''
                                    temp_result_value = ''

                                    if 'name' in _json['props']['apolloState'][prop]:
                                        temp_result_key = str(_json['props']['apolloState'][prop]['name'])

                                    if 'values' in _json['props']['apolloState'][prop] and _json['props']['apolloState'][prop]['values']:
                                        for vals in _json['props']['apolloState'][prop]['values']:
                                            if 'id' in vals:
                                                temp_values.append(vals['id'])

                                    if temp_values: #fetch value
                                        for item in temp_values:
                                            if 'value' in _json['props']['apolloState'][item]:
                                                temp_result_value += ' '+str(_json['props']['apolloState'][item]['value'])

                                    if temp_result_key and temp_result_value:
                                        value[temp_result_key.strip()] = temp_result_value.strip()
           
            else:
                tag = soup.select('.sc-1dj0gc8-2.kaPYYN.sc-1wsbwny-0.pvTwK table tbody')
                if tag:
                    source = str(tag)
                    for tags in tag:
                        key = tags.select_one('td')
                        val = tags.select_one('div')
                        if key and val:
                            value[key.get_text()] = val.get_text()
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            
            
            tag = soup.find('script',{'id':'__NEXT_DATA__'},type = 'application/json')
            if tag:
                _json = json.loads(tag.text.strip())
                if _json and 'props' in _json.keys() and 'apolloState' in _json['props']:
                    temp_list = []
                    root_data = _json['props']['apolloState']['ROOT_QUERY'] if 'ROOT_QUERY' in _json['props']['apolloState'] else None

                    for _key in _json['props']['apolloState']:
                        if 'productDetails.images' in _key:
                            temp_list.append(_json['props']['apolloState'][_key])

                    if not temp_list and root_data:
                        key_directory = [_key for _key in root_data if 'productDetailsV3' in _key]
                        if key_directory:
                            if 'productDetails' in root_data[key_directory[0]] and 'images' in root_data[key_directory[0]]['productDetails'] \
                                and root_data[key_directory[0]]['productDetails']['images']:
                                temp_list = root_data[key_directory[0]]['productDetails']['images']

                    if temp_list:
                        source = str(tag)
                        for item in temp_list:
                            if 'fileUrl' in item:
                                value.append(item['fileUrl'])
                    

            else:
                tag = soup.findAll('script',type="application/ld+json")[2]
                _json = json.loads(tag.get_text())
                if _json:
                    source = str(tag)
                    if 'image' in _json:
                        if isinstance(_json['image'],list):
                            value = _json['image']
                        else:
                            value.append(_json['image'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script',{'id':'__NEXT_DATA__'},type = 'application/json')
            if tag:
                _json = json.loads(tag.text.strip())
                if _json and 'props' in _json.keys() and 'apolloState' in _json['props']:
                    temp_list = []
                    for _key in _json['props']['apolloState']:
                        if 'productDetails.youtubeVideos' in _key and 'thumbnails' not in _key:
                            temp_list.append(_json['props']['apolloState'][_key])
                    if temp_list:
                        source = str(tag)
                        for item in temp_list:
                            if 'videoId' in item:
                                value.append('https://youtu.be/'+item['videoId'])
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', type="application/ld+json")
            _json = json.loads(tag.get_text())
            if 'offers' in _json:
                source = _json['offers']
                value = _json['offers']['availabilityDate']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
