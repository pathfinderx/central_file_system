import logging
import re
import json
import urllib.parse
import codecs

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ShoptimeComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shoptime.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            data_price = None
            tag_container = soup.find('div', {'class': re.compile('^product-page__')})

            if tag_container:
                tag_script = tag_container.select_one('script', type='application/ld+json')
                if tag_script and tag_script.string:
                    data = json.loads(tag_script.string)
                    if data and '@graph' in data.keys():
                        data_graph = data['@graph']
                        if data_graph and len(data_graph):
                            for data_graph_item in data_graph:
                                data_type = data_graph_item['@type']
                                if data_type and data_type.lower() == 'product':
                                    data_product_offers = data_graph_item['offers']
                                    if data_product_offers and 'price' in data_product_offers:
                                        source = str(data_product_offers)
                                        data_price = data_product_offers['price']

                                        if data_price:
                                            value = data_price
                                    break
            
            if not data_price:
                tag = soup.find('span', {"class" : re.compile('.*price__SalesPrice.*')}) or soup.find('div',{'class':re.compile('src__BestPrice-*')})

                if tag:
                    source, value = str(tag), tag.get_text(strip=True)
            


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value
        
        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'
        
        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('button[value="comprar"]')

            if tag and tag.has_attr('value'):
                source, value = str(tag), str(tag['value'])
            else:
                tag = soup.find('span',{'class':re.compile('src__BuyButtonText-.*')}) or soup.find('button',{'class':re.compile('src__PrimaryButton-.*')}) \
                    or soup.find('button',{'class':re.compile(r'styles__PrimaryButton-.*')})
                if tag:
                    source,value = str(tag),tag.text.strip()


            if not tag:
                tag = soup.select_one('div.product-page__ViewUI-l3vu3y-0 script')
                
                if tag:
                    jsonData = json.loads(tag.string)
                    if jsonData and '@graph' in jsonData:
                        for i in jsonData['@graph']:
                            if i['@type'] == 'Product':
                                source = str(jsonData)
                                value = i['offers']['availability']
                                break

            pdp_tag = soup.select_one('.ViewForm-sc-1ijittn-1.feKtIQ > span')
            if pdp_tag and pdp_tag.get_text() in ['encontre a loja mais proxima', 'Loja parceira perto de você', 'Calcular frete e prazo']:
                source = str(pdp_tag)
                value = str(pdp_tag.get_text())

            pdp_tag2 = soup.find('div', class_="src__ProductOffer-sc-1a23x5b-6 kpLkBz")
            if 'Produto sem estoque' in str(pdp_tag2): # pdp checker
                outOfStock = pdp_tag2.find('strong', class_='styles__Title-sc-13a2o83-1 jWlwIr')
                if outOfStock: 
                    source, value = str(pdp_tag2), outOfStock.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value
        
        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.product-page__ViewUI-l3vu3y-0 script')
            if tag:
                jsonData = json.loads(tag.string)
                for i in jsonData['@graph']:
                    if i['@type'] == 'Product':
                        if 'aggregateRating' in i.keys():
                            score_source = reviews_source = str(i['aggregateRating'])
                            score_value = str(i['aggregateRating']['ratingValue'])
                            reviews_value = str(i['aggregateRating']['reviewCount'])

                        break
            else:
                # Ratings
                rating_tag = soup.find('span',{'class':re.compile('header__RatingValue-.*')})
                if rating_tag:
                    score_source,score_value = str(rating_tag),rating_tag.text.strip()

                # Reviews
                review_tag = soup.find('span',{'class':re.compile('src__Count-.*')})
                if review_tag:
                    reviews_source,reviews_value = str(review_tag),review_tag.text.strip()
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_container = soup.find('div', {'class': re.compile('^product-page__')})
            if tag_container:
                tag_script = tag_container.select_one('script', type='application/ld+json')
                if tag_script and tag_script.string:
                    data = json.loads(tag_script.string)
                    data_graph = data['@graph']
                    if data_graph and len(data_graph):
                        for data_graph_item in data_graph:
                            data_type = data_graph_item['@type']
                            if data_type and data_type.lower() == 'product':
                                source = str(data_graph_item)
                                data_product_name = data_graph_item['name']
                                if data_product_name:
                                    value = data_product_name
                                break
            else:
                tag = soup.find('span',{'class':re.compile('src__Title-.*')})
                if tag:
                    source,value = str(tag),tag.text.strip()
                else:
                    tag = soup.select_one('h1.src__Title-sc-79cth1-0.giPKel')
                    if tag:
                        source,value = str(tag),tag.text.strip()
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value
        
        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.product-page__ViewUI-l3vu3y-0 script')
            if tag:
                jsonData = json.loads(tag.string)
                for i in jsonData['@graph']:
                    if i['@type'] == 'Product'and 'brand' in i:
                        source = str(i['brand'])
                        value = i['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value
        
        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _desc_list = []
            tag = soup.select_one('div.info-description-frame-inside') or soup.find('div',{'class':re.compile('old__Description-.*')}) or soup.select_one('div#info-section div')

            if tag:
                source = str(tag)
                # value = tag.get_text().strip()
                _desc_list.append(tag.text.strip())
            
            _id = None
            id_tag = soup.select_one(".product-header__CodUI-sc-3cf9pw-0")
            if id_tag:
                rgx = re.search(r'[0-9]+',id_tag.text.strip())
                if rgx:
                    _id = rgx.group(0)
            else:
                temp_id = None
                tag = soup.select_one('script[type="application/ld+json"]')
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json and '@graph' in _json and _json['@graph']:
                        for item in _json['@graph']:
                            if '@type' in item and item['@type'] == 'Product':
                                temp_id = item['@id']
                                break
                if temp_id:
                    rgx = re.search(r'[0-9]+',temp_id.strip())
                    if rgx:
                        _id = rgx.group(0)
            if _id:
                if _id == '1962216937': #temporarily defaulted --- these ids uses different ids for description
                    _id = '1962216902'  
                if _id == '1952648509':
                    _id = '1951732645'
                try:
                    api_url = f'https://statics-shoptime.b2w.io/special/products/{_id}/index.html'
                    response = self.downloader.extra_download(api_url)
                    if response:
                        desc_soup = BeautifulSoup(response,'lxml')
                        desc_tag = desc_soup.select_one('#conteudo')
                        if desc_tag:
                            desc_tag = self._sanitize(desc_tag)
                            source += ' '+api_url
                            _desc_list.append(' '.join(desc_tag.stripped_strings))
                            # value += ' '+ ' '.join(desc_tag.stripped_strings)
                            # value = value.strip()
                except:
                    pass

            if _desc_list:
                value = ' '.join(_desc_list)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('tr.Tr-sc-1wy23hs-3')

            if len(tags) > 0:
                source = str(tags)
                for i in tags:
                    pair = i.select('span.TextUI-sc-12tokcy-0')
                    key = pair[0].text
                    val = pair[-1].text
                    value[key] = val
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value
        
        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('div.image-gallery-thumbnail-inner img') or soup.select('.thumb__WrapperImages-sc-1aoaee7-1 img')
            source = str(tags)

            if tags:
                for i in tags:
                    value.append(i['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value
        
        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tags = soup.find_all('script')

            if tags:
                for i in tags:
                    if i.string and '__PRELOADED_STATE__' in i.string:
                        source = str(i.string)
                        unicodeDecodedString = codecs.decode(i.string, 'unicode-escape')
                        urlDecodedString = urllib.parse.unquote(unicodeDecodedString)
                        splitRes = urlDecodedString.split('/embed/')
                        for sr in splitRes[1:]:
                            value.append(f'https://www.youtube.com/watch?v={sr[:11]}')
            value = list(dict.fromkeys(value))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value
        
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Notes: Delivery is behind a zip code requirement

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Notes: Shipping is behind a zip code requirement

        return source, value

    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag
