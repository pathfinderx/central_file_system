from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import re

class KabumComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'
            }

        if not isinstance(cookies, dict):
            cookies = None

        self.url = url

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            status_code = res.status_code

            if status_code in [200, 201]:
                if 'name="title"' in res.text: #PDP Checker
                    return res.text
                else:
                    raise DownloadFailureException('Download Failed - Auto rerun is possible')

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.content.decode('utf-8')

            return None
            # raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            return None
            # raise DownloadFailureException('Download failed - Unhandled Exception')

    def get_special_description(self, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            pass
            
        if not isinstance(cookies, dict):
            pass

        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
        }

        html_file = 'Nintendo-switch-2021-may.html' # temporary (subject to changes)
        product_id = self.url.split('/')[-2] if re.search('[0-9]+', self.url.split('/')[-2]) else None
        url = f'https://www.kabum.com.br/conteudo/descricao/{product_id}/{html_file}'

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            if res.status_code in [200, 201]:
                return res.text
            else:
                return None
        except Exception as e:
            print('error in get_extra_description', e)
            return None
            