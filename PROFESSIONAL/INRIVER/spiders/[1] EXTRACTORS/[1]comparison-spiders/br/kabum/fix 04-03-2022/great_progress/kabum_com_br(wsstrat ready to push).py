import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class KabumComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.kabum.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        data = self.get_data(soup)
        script_data = self.get_script(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, data)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(data, script_data)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup, data, script_data)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(data)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup, data, script_data)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup,data)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup,data)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(data)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(result)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(data)

        return result

    def get_price(self, _soup, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if soup and 'product' in soup and soup['product'] and 'price' in soup['product']:
                if soup['product']['available']:
                    if str(soup['product']['offer']) != 'None':
                        value = str(soup['product']['offer']['priceWithDiscount'])
                    else:
                        value = str(soup['product']['price'])
                    source = soup['source']
            else:
                tag = _soup.select_one('[itemprop="price"]')
                if tag:
                    source, value = str(tag), tag.get_text().strip()
            # if 'product' in soup.keys() and 'code' in soup['product'] and str(soup['product']['code']) == '153548': #Temporary Fix Only 
            #     value = str(soup['product']['priceWithDiscount'])
            #     source = soup['source']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'

        return source, value

    def get_availability(self, _soup, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup and 'product' in soup and soup['product'] and 'available' in soup['product']:
                value = str(soup['product']['available'])
                source = soup['source']
            else:
                tag = _soup.select_one('[class="sc-caiLqq gighgS"]') or _soup.select_one('[class="sc-ehIJor czAIrm"]')
                if tag:
                    source, value = str(tag), "instock"     
                else:
                    tag = _soup.select_one('script[type="application/ld+json"]')
                    if tag:
                        _json = json.loads(tag.get_text(strip=True).replace('\n',''))
                        if 'offers' in _json.keys():
                            source = str(_json)
                            value = _json['offers']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup, script_data):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup:
                if soup['product'] and 'rating_value' in soup['product'] and 'rating_count' in soup['product']:
                    score_value = str(soup['product']['rating_value'])
                    score_source = soup['source']
                    reviews_value = str(soup['product']['rating_count'])
                    reviews_source = soup['source']
                elif soup['product'] and 'rating' in soup['product'] and 'ratingCount' in soup['product']:
                    score_value = str(soup['product']['rating'])
                    score_source = soup['source']
                    reviews_value = str(soup['product']['ratingCount'])
                    reviews_source = soup['source']

            if script_data and score_value == 'NOT_FOUND':
                if script_data and 'aggregateRating' in script_data and 'ratingValue' in script_data['aggregateRating']:
                    score_source, score_value = str(script_data), script_data['aggregateRating']['ratingValue']
            if script_data and reviews_value == 'NOT_FOUND':
                if script_data and 'aggregateRating' in script_data and 'reviewCount' in script_data['aggregateRating']:
                    reviews_source, reviews_value = str(script_data), script_data['aggregateRating']['reviewCount']
                    
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup, data, script_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            tag = soup.select_one('h1.titulo_det')

            if tag and tag.get_text().strip():
                source, value = str(tag), tag.get_text().strip()
            
            elif data['product'] and 'name' in data['product']:
                source, value = str(data['source']), data['product']['name']   

            elif script_data and 'name' in script_data:
                source, value = str(script_data), script_data['name']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if soup['product'] and 'brand' in soup['product']:
                value = soup['product']['brand']
            elif soup['product'] and 'manufacturer' in soup['product'] and 'name' in soup['product']['manufacturer']:
                value = soup['product']['manufacturer']['name']
            
            source = soup['source']                

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup, data, script_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src_list = []
            desc_list = []
            tag = soup.select_one('div.content_tab p:first-child')

            if tag:
                source, value = str(tag), tag.get_text().strip()
                if value == '':
                    tag = soup.select_one('.content_tab p')
                    if tag:
                        description = tag.nextSibling
                        if description:
                            # source, value = str(description), description.get_text(strip=True)
                            src_list.append(str(description))
                            desc_list.append(' '.join(description.stripped_strings))
            elif data['product'] and 'description' in data['product']:
                description = BeautifulSoup(data['product']['description'], 'lxml')
                # source , value = data['source'], description.get_text()
                src_list.append(str(description))
                desc_list.append(' '.join(description.stripped_strings))

            script_desc = False
            if script_data and 'description' in script_data:
                script_desc = True
                # _desc = re.compile(r'<.*?>|(&nbsp;)') # Remove html tags from text
                # _value = _desc.sub(' ', script_data['description'])
                # source, value = str(script_data), _value.replace('   ','').replace('  ',' ').strip()
                descriptionTag = BeautifulSoup(script_data.get('description'), 'lxml')
                if descriptionTag:
                    source, value = str(script_data), ' '.join(descriptionTag.stripped_strings)

            tag = soup.select_one('#iframeDescricao[src]')
            if tag and not script_desc:
                api_url = 'https://www.kabum.com.br'+tag['src']
                res = self.downloader.extra_download(api_url)
                if res:
                    desc_soup = BeautifulSoup(res,'lxml')
                    desc_tag = desc_soup.select_one('body center')
                    if desc_tag:
                        src_list.append(api_url)
                        desc_list.append(' '.join(desc_tag.stripped_strings))

            if desc_list:
                source = ' + '.join(src_list)
                value = ' '.join(desc_list)

            raw_special_description = self.downloader.get_special_description() # request for special description
            if raw_special_description:
                special_soup = BeautifulSoup(raw_special_description, 'lxml')
                # stripped_description = special_soup.select_one('table#bodyTable').select('.mcnTextContent') # backup 
                stripped_description = ' '.join(special_soup.select_one('table#bodyTable').stripped_strings) if special_soup.select_one('table#bodyTable') else None

                if stripped_description:
                    encoded_stripped_description = stripped_description.encode('latin-1') # use latin-1 instead of utf8 encoding to convert obfuscated data to latin encoded bytes
                    decoded_latin_description = re.sub(b'\xa0', b'', encoded_stripped_description).decode() # use bytes(b) instead of raw_data(r) since 'encoded_stripped_description' is byte encoded
                    if decoded_latin_description:
                        source, value = str(stripped_description), decoded_latin_description

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def decodeUTF8 (self, data):
        encoded = str(data).encode('utf_8') # encode to bytes
        decoded = encoded.decode('unicode_escape')
        stripped = decoded.replace('\\','')
        strippedSoup = BeautifulSoup(stripped, 'lxml')
        return strippedSoup

    def decodeToLatin(self, data):
        byte_string = str(data)
        return byte_string

    def get_specifications(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('div.content_tab p')
            warrantyTag = soup.find('script', text = re.compile('props'))
            
            if len(tags):
                source = str(tags)
                for i in tags:
                    tag_str_regex = re.search(r'-\s*([^:]+):(.+)', i.get_text())
                    if tag_str_regex:
                        key = tag_str_regex.group(1).strip()
                        val = tag_str_regex.group(2).strip()
                        value[key] = val

            elif data['product'] and 'html' in data['product']:
                specsSoup = BeautifulSoup(data.get('product').get('html').strip(), 'lxml')
                hasExtractedValues, tempKey, tempValues = False, [], []
                specsTag = specsSoup.find_all('p')

                if specsTag: # specs
                    for index, p in enumerate(specsTag):
                        if p.strong and not str(p.strong.text).isspace():
                            if tempKey and tempValues and hasExtractedValues:
                                value.update({tempKey[-1]:'\n'.join(tempValues)})
                                tempKey, tempValues = [], []
                                tempKey.append(p.strong.text.strip())
                            else:
                                tempKey.append(p.strong.text.strip())
                        elif not p.strong:
                            if not str(p.text).isspace():
                                hasExtractedValues = True
                                tempValues.append(p.text.strip())
                            elif not tempValues:
                                tempKey = []
                        if index == len(specsTag) - 1 and tempKey and tempValues:
                            value.update({tempKey[-1]:'\n'.join(tempValues)})
                            tempKey, tempValues = [], []

                if warrantyTag: # warranty
                    _json = json.loads(warrantyTag.text.strip())
                    if _json and 'props' in _json and 'pageProps' in _json['props']:
                        if 'productCatalog' in _json['props']['pageProps'] and 'warranty' in _json['props']['pageProps']['productCatalog']:
                            key = 'Garantia' if 'warranty' in _json.get('props').get('pageProps').get('productCatalog').keys() else None
                            value[key] = _json['props']['pageProps']['productCatalog']['warranty']
                            
                        if 'productData' in _json['props']['pageProps'] and 'weightString' in _json['props']['pageProps']['productData']:
                            key = 'Peso' if 'weightString' in _json.get('props').get('pageProps').get('productData').keys() else None
                            value[key] = _json['props']['pageProps']['productData']['weightString']
            #
            # elif data and 'specs' in data:
            #     soup = BeautifulSoup(data['specs'], "lxml")
            #     spec_tags = soup.select('p')
            #     val = ''
            #     key = ''
            #     for row in spec_tags:
            #         if row.select_one('strong'):
            #             key = row.get_text().strip()[:-1]
                        
            #         if "-" in row.get_text() and key:
            #             dec_list = row.get_text().strip()
            #             val = val + ''.join(dec_list)
    
            #         if val != '' and key != '':
            #             value[key] = val
                    
            #     source = data['specs'] 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup,data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('ul#imagens-carrossel li img')

            if len(tags):
                source = str(tags)
                value = [i.get('src') for i in tags]
            elif data['product'] and 'images' in data['product']:
                for img in data['product']['images']:
                    value.append(img)
                
                source = data['source']
            if not value:
                tags = soup.select('button[class="sc-feYDSs SKneF image"] img') or soup.select('button[class="sc-gSQFLo mXHFL image"] img')
                if tags:
                    source = ''.join(str(ii) for ii in tags)
                    for _img_lst in tags:
                        if _img_lst.has_attr('src'):
                            value.append(_img_lst.get('src').replace('p.jpg','g.jpg'))
                            
            if not value:
                tags = soup.select('[id="carrosselLateralDescricao"] div.slick-slide img')
                if tags:
                    source = str(tags)
                    for img in tags:
                        if img.has_attr('src'):
                            value.append(img['src'].replace('p.jpg','gg.jpg'))
                     
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # DALIVERY VARY FROM STOCKS AVAILABILITY
            if result['availability']['value'] == 'True':
                value = 'Em estoque'
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value
            
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value

    def get_data(self, soup):
        data = {}
        tags = soup.select('script')

        regex = re.compile(r'dataLayer\s*=\s*(\[[^;]+);')
        tag = next((i for i in tags if i.string and re.search(regex, i.string)), None)

        if tag:
            data_str = re.search(regex, tag.string)
            raw_data = json.loads(data_str.group(1))

            if len(raw_data) and 'productsDetail' in raw_data[0]:
                data['source'] = str(tag)
                data['product'] = raw_data[0]['productsDetail'][0]
        else:
            tag = soup.select_one('script#__NEXT_DATA__')
            specs = soup.select_one('section#secaoInformacoesTecnicas')
            if tag:
                _json = json.loads(tag.get_text())
                data['source'] = str(tag)
                if 'props' in _json.keys() and 'initialProps' in _json['props'] and 'pageProps' in _json['props']['initialProps'] and 'productCatalog' in _json['props']['initialProps']['pageProps']:
                    data['product'] = _json['props']['initialProps']['pageProps']['productCatalog']
                elif 'props' in _json.keys() and 'pageProps' in _json['props'] and 'productCatalog' in _json['props']['pageProps']:
                    data['product'] = _json['props']['pageProps']['productCatalog']
                else:
                    data['product'] = None
                
                if specs:
                    data['specs'] = str(specs)
        return data

    def get_script(self, soup):
        data = None
        script_tag = soup.find('script', attrs={"type": "application/ld+json"})

        if script_tag:
            _rgx = re.search(r'"@type": "AggregateRating"', script_tag.get_text())
            if _rgx:
                source = str(script_tag)
                stripped_data = re.sub(r'[\n\t]*', "", script_tag.get_text().strip())
                data = json.loads(stripped_data)
        return data