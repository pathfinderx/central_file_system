import logging
import re

import requests as r
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class TwentyfivebestComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.25best.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span[data-app="product.price"]') or \
                soup.select_one('span#variacaoPreco') or \
                soup.select_one('span[data-tray-tst="price_product"]') or \
                soup.select_one('div.hello_product_item_box_info > span')
            if tag:
                value = tag.get_text().strip()
                source = str(value)
            else:
                tag = soup.select_one('.PrecoPrincipal input#preco_atual')
                if tag and tag.has_attr('value'):
                    value = str(tag['value'])
                    source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.currency')

            if tag and tag.has_attr('title'):
                value = tag['title']
                source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self.get_datalayer_json(soup)
            if _json and 'availability' in _json:
                value = _json['availability']
                source = str(_json)
            elif (_json and
                'offers' in _json and 
                len(_json['offers']) > 0 and
                'availability' in _json['offers'][0]):
                
                source, value = str(_json), _json['offers'][0]['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self.get_datalayer_json(soup)
            if _json and 'rating' in _json:
                if 'count' in _json['rating']:
                    reviews_value = str(_json['rating']['count'])
                    reviews_source = str(_json)
                if 'average' in _json['rating']:
                    score_value = str(_json['rating']['average'])
                    score_source = str(_json)
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.product-colum-right .product-name') or soup.select_one('.entry-summary > h1')

            if tag:
                value = tag.get_text().strip()
                source = str(tag)
            else:
                _json = self.get_datalayer_json(soup)
                if _json and 'productName' in _json:
                    value = _json['productName']
                    source = str(_json)
                else:
                    _json = self.get_json(soup)
                    if _json and 'name' in _json:
                        value = _json['name']
                        source = str(_json)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = self.get_json(soup)

            if _json and 'brand' in _json:
                if 'name' in _json['brand']:
                    value = _json['brand']['name']
                    source = str(_json)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div[data-tab-id="#descricao"] .board_htm') or soup.select_one('.description .board_htm')

            if tag:
                h3_rgx = re.compile(r'<h3>|</h3>') # Remove h3 tags from text as it creates spacing issue
                temp = h3_rgx.sub(' ', str(tag))    

                temp_soup = BeautifulSoup(temp, 'lxml') # convert back to beautiful soup by not remove all html tags to also prevent weird spacing.
                tag = temp_soup

                txt = ' '.join(tag.stripped_strings)
                value = ' '.join([string.strip() for string in txt.split(' ') if string.strip() != ''])
                source = str(tag)
            else:
                _json = self.get_datalayer_json(soup)
                if (_json and 'description' in _json):
                    source, value = str(_json), _json['description']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select_one('#ficha')
            
            if tag:
                specs = tag.select('table tr')
                for spec in specs:
                    tds = spec.select('td')
                    key = tds[0].get_text().strip()
                    val = tds[1].get_text().strip()
                    value.update({key:val})
                source = str(tag)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list


        try:
            tags = soup.select('.nav-images .box-img img')
            if tags:
                for img in tags:
                    if img.has_attr('src'):
                        # remove the 90_ to make the image larger.
                        value.append(img['src'].replace('/90_', '/'))
            else:
                _json = self.get_datalayer_json(soup)
                if (_json and 'image' in _json):
                    source = str(_json)
                    value.append(_json['image'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        # not available
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # not available
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # not available
        return source, value

    def get_datalayer_json(self, soup):
        try:
            try: # first json conversion test
                tag = soup.find('script', text=re.compile(r'dataLayer \= '))
                if tag:
                    txt = tag.get_text().strip('dataLayer =').strip()
                    _json = json.loads(txt)
                    if _json:
                        return _json[0]
                else:
                    _json = self.convert_alternative_source(soup) # second json conversion test
                    return _json if _json else None
            except Exception as e:
                print('Error at: wsstrat -> get_datalayer_json -> failure to convert to json')
                print('converting alternative source to json...')
                _json = self.convert_alternative_source(soup)
                return _json if _json else None

        except Exception as e:
            print('Error at: wsstrat -> get_datalayer_json -> with message:', e)
        return None

    def convert_alternative_source (self, soup):
        try: 
            script_tag = soup.select('script[type="application/ld+json"]')
            for x in script_tag:
                if (str(x).find('product') > -1):
                    _json = json.loads(x.get_text())
                    return _json if _json else None
        except Exception as e:
            print('Error at: wsstrat -> convert_alternative_source -> with message: ', e)
        
    def get_json(self, soup):
        try:
            tag = soup.select_one("script[type='application/ld+json']") or soup.find('script', {"type": "application/ld+json" })
            if tag:
                txt = tag.get_text().strip()
                _json = json.loads(txt)
                if _json:
                    return _json
        except:
            raise Exception
        return None
