import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MirandaComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.miranda.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('span.preco1.precoMenorValor') or soup.select_one('span.preco1') or soup.select_one('[itemprop="price"]')

            if tag:
                source, value = str(tag), tag.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[itemprop="availability"]')

            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')
            if not tag:
                tag = soup.select_one('.bxBtnComprar')
                if tag:
                    source, value = str(tag), tag.get_text(strip=True)
            if not tag:
                value = "outofstock"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = soup.select_one('p.pontuacaoAval')
            tag_review = soup.select_one('div.ratePrd')

            if tag_score and tag_review:
                reviews_source, score_source = str(tag_review), str(tag_score)
                reviews_value, score_value = tag_review.text.rsplit(' ', 1)[0].strip(), tag_score.text

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h3.tituloPrd') or soup.select_one('[itemprop="name"]')

            if tag:
                source, value = str(tag), tag.text
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('p.prodMarca')

            if tag:
                source, value = str(tag), tag.text.rsplit(':', 1)[-1].strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # src_list = []
            # desc_list = []
            #moved due to order issues ---> must match description order in PDP
            # tag = soup.find('script',text = re.compile('Webcollage.loadProductContent'))
            # if tag:
            #     rgx = re.search(r',.*\'([0-9]+)\',.*',tag.text.strip())
            #     if rgx:
            #         _id = rgx.group(1)
            #         api_url = f'https://mirandainformatica.com.br/marketing/produtos/nintendo/{_id}/'
            #         response = self.downloader.extra_download(api_url)
            #         if response:
            #             desc_soup = BeautifulSoup(response,'lxml')
            #             description = desc_soup.select_one('body')
            #             if description:
            #                 description = self._sanitize(description)
            #                 if description.select_one('.mcnPreviewText'):
            #                     description.select_one('.mcnPreviewText').extract()
            #                 # value += ' '.join(description.stripped_strings)
            #                 src_list.append(api_url)
            #                 desc_list.append(' '.join(description.stripped_strings))

            # tag = soup.select_one('[property="og:description"]')

            # if tag and tag.has_attr('content'):
            #     # source, value = str(tag), tag.get('content')
            #     src_list.append(str(tag))
            #     desc_list.append(tag.get('content'))
            
            # tag = soup.select_one('#descricao')
            # if tag:
            #     # source, value = str(tag), ' '.join(tag.stripped_strings)
            #     src_list.append(str(tag))
            #     desc_list.append(' '.join(tag.stripped_strings))

            # if desc_list:
            #     source = ' + '.join(src_list)
            #     value = ' '.join(desc_list)
            
            extra_desc_tag = soup.find("div", {"class":"ui bottom attached tab segment basic active infos" ,"data-tab":"informacoes"})
            if extra_desc_tag:
                if extra_desc_tag.select_one('iframe') and extra_desc_tag.select_one('iframe').has_attr('src'):
                    api_url = extra_desc_tag.select_one('iframe')['src']
                    response = self.downloader.extra_download(api_url)
                    if response:
                        desc_soup = BeautifulSoup(response,'lxml')
                        description_tag = desc_soup.select_one('body')
                        if description_tag:
                            description_tag = self._sanitize(description_tag)
                            desc = re.sub(' +', ' ', description_tag.get_text().replace('*|MC_PREVIEW_TEXT|*', '').replace('\n',' ').replace('\xa0',' ').strip()) 
                            extra_desc = re.sub(' +', ' ', extra_desc_tag.get_text().strip().replace('\t',' '))
                            source, value = str(api_url), desc +" "+extra_desc
                else:
                    source, value = str(extra_desc_tag), re.sub(' +', ' ', extra_desc_tag.get_text().strip().replace('\t',' '))
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.find('div', {'id':re.compile('\d*.*productDetails')})
            tags = tag.select('ul')[-1].select('li') if tag else []

            if len(tags):
                source = str(tags)

                for i in tags:
                    if ':' in i.get_text():
                        key = i.get_text().split(':', 1)[0].strip()
                        val = i.get_text().split(':', 1)[-1].strip()
                        value[key] = val
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('img.dimi') or soup.select('.car-gallery.thumbnails.slick-dotted img')

            if len(tags):
                source = str(tags)
                for i in tags:
                    if '//miranda.cdn.plataformaneo' in i.get('src'):
                        value.append('https:'+i.get('src'))
                    else:
                        value.append('https://www.miranda.com.br'+i.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script',text = re.compile('Webcollage.loadProductContent'))
            if tag:
                rgx = re.search(r',.*\'([0-9]+)\',.*',tag.text.strip())
                if rgx:
                    _id = rgx.group(1)
                    api_url = f'https://mirandainformatica.com.br/marketing/produtos/nintendo/{_id}/'
                    response = self.downloader.extra_download(api_url)
                    if response:
                        video_soup = BeautifulSoup(response,'lxml')
                        video = video_soup.select_one('iframe[title="YouTube video player"]')
                        if video and video.has_attr('src'):
                            source = str(video)
                            value.append(video.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value
        
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support delivery
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value

    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag