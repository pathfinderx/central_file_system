import logging
import re
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MagazineluizaComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.magazineluiza.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self.json_data = self.get_data(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_data(self, soup):
        data = soup.select_one('script#__NEXT_DATA__')
        if data:
            _json = json.loads(data.get_text(strip=True))
            if 'props' in _json.keys() and 'pageProps' in _json['props'] and 'data' in _json['props']['pageProps'] and 'product' in _json['props']['pageProps']['data']:
                json_data = _json['props']['pageProps']['data']['product']

        return json_data

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('span.price-template__text')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'price' in _json.keys() and 'price' in _json['price']:
                        source = str(_json['price'])
                        value = str(_json['price']['bestPrice'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.button__notify.unavailable__product-button.js-unavailable-button') or \
                soup.select_one('.button__buy.button__buy-product-detail.js-add-cart-button.js-main-add-cart-button.js-add-box-prime')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'available' in _json.keys():
                        source = str(_json)
                        value = str(_json['available'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            ratings = soup.select_one('div.interaction-client__rating-info')

            if ratings:
                rating_spans = ratings.select('span')

                if len(rating_spans) > 0:
                
                    score_source = str(rating_spans[1])
                    score_value = rating_spans[1].get_text(strip=True)
                    reviews_source = str(rating_spans[2])
                    reviews_value = rating_spans[2].get_text(strip=True).replace('(','').replace(')','')
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'rating' in _json.keys():
                        #ratings
                        score_source = str(_json['rating'])
                        score_value = str(_json['rating']['score'])
                        #reviews
                        reviews_source = str(_json['rating'])
                        reviews_value = str(_json['rating']['count'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1.header-product__title') or soup.select_one('h1.header-product__title--unavailable')

            if tag:
                source, value = str(tag), tag.get_text().strip()
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'title' in _json.keys() and 'reference' in _json.keys():
                        title = _json['title']
                        reference = _json['reference']
                        if title and reference:
                            source, value = str(_json), title + ' - ' + reference
                        else:
                            source, value = str(_json), _json['title']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('a.header-product__text-interation span')
            if tag:
                source = str(tag)
                value = tag.text.strip()
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'brand' in _json.keys() and 'label' in _json['brand']:
                        source, value = str(_json['brand']), _json['brand']['label']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source_list = []
            desc_list = []
            tag = soup.select_one('div.description div.description__container-text')

            if tag:
                # Updated for order purposes --> must match description order in PDP
                source_list.append(str(tag))
                # desc_list.append(' '.join(tag.stripped_strings))
                # source, value = str(tag), tag.get_text().strip()

                api_tag = tag.select_one('iframe')
                if api_tag and api_tag.has_attr('src'):
                    api_url = api_tag['src']
                    res = self.downloader.extra_download(api_url)
                    if res:
                        source_list.append(api_url)
                        desc_soup = BeautifulSoup(res,'lxml')
                        desc_tag = self._sanitize(desc_soup.select_one('body'))
                        if desc_tag.select_one('.text_rodape'):
                            desc_tag.select_one('.text_rodape').extract()
                        api_tag.string = ' '.join(desc_tag.stripped_strings)

                desc_list.append(' '.join(tag.stripped_strings))

            if not desc_list:
                if self.json_data:
                    _json = self.json_data
                    if 'description' in _json.keys():
                        desc_list.append(_json['description'])
                        source_list.append(str(_json))
                
                tag = soup.select_one('div.sc-jRQBWg.jCpMkp iframe')
                if tag:
                    api_url = tag['src']
                    res = self.downloader.extra_download(api_url)
                    if res:
                        last_part = None
                        source_list.append(api_url)
                        desc_soup = BeautifulSoup(res,'lxml')
                        desc_tag = self._sanitize(desc_soup.select_one('body'))
                        if desc_tag.select_one('.text_rodape'):
                            last_part = desc_tag.select_one('.text_rodape').extract()
                        temp_desc = ' '.join(desc_tag.stripped_strings).replace('\n',' ')
                        if temp_desc:
                            desc_list.append(temp_desc)
                            if last_part:
                                desc_list.append(' '.join(last_part.stripped_strings))

            if desc_list:
                source = ' + '.join(source_list)
                value = ' '.join(desc_list)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            if self.json_data:
                _json = self.json_data
                if 'factsheet' in _json.keys():
                    source = str(_json['factsheet'])
                    for data in _json['factsheet']:
                        if 'elements' in data.keys():
                            for _data in data['elements']:
                                if 'elements' in _data.keys() and 'keyName' in _data.keys():
                                    for specs in _data['elements']:
                                        key = specs['keyName']
                                        val = specs['value']
                                        if key and val:
                                            value[key] = val
                                        else:
                                            if key is None and _data['keyName'] in ['Compatibilidade','Cor','Quantidade','Conectividade','Certificações', \
                                                'SAC do fornecedor (E-mail)','Conteúdo da embalagem']:
                                                key = specs['value']
                                                if key:
                                                    value[key] = ''

            #Causing -3 value in specs in all item_id

            # tags = soup.select_one('div.description__container-text').findChildren('table', recursive=False)

            # if len(tags) > 0:
            #     source = str(tags)

            #     for i in tags:
            #         mainKey = i.select_one('td.description__information-left').text
            #         mainValue = {}

            #         secondaryKeyValues = i.select('table.description__box')
            #         # print(len(secondaryKeyValues))
            #         for skv in secondaryKeyValues:
            #             secondKey = skv.select_one('td.description__information-box-left').text.strip()
            #             secondValue = skv.select_one('td.description__information-box-right').text.strip()
            #             mainValue[secondKey] = secondValue
            #         value[mainKey] = mainValue
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('img.carousel-product__item-img')

            if tags:
                for i in tags:
                    value.append(i['data-src'])
            else:
                if self.json_data:
                    _json = self.json_data
                    if 'media' in _json.keys() and 'images' in _json['media']:
                        for image in _json['media']['images']:
                            value.append(image.replace("{w}x{h}",'1000x1000'))
                    source = str(_json['media']['images'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            pass
            #Scrape video even if video not found in pdp
            #Feel free to uncomment if video is found in pdp

            # tags = soup.select('iframe.carousel-product__item-video')

            # if tags:
            #     source = str(tags)
            #     for i in tags:
            #         value.append(i['data-video'])
            # if not tags:
            #     #API URL is Static
            #     api_url = f'https://conteudoproduto.magazineluiza.com.br/99/043082200/index.html'
            #     response = self.downloader.extra_download(api_url)
            #     if response:
            #         video_soup = BeautifulSoup(response,'lxml')
            #         video = video_soup.select_one('div.back_vi iframe')
            #         if video and video.has_attr('src'):
            #             source = str(video)
            #             value.append(video.get('src'))


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Notes: Delivery needs special handling / api request.

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Notes: Shipping needs special handling / api request.

        return source, value

    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag