import json
import cloudscraper
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class MagazineluizaComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        self.scraper = cloudscraper.create_scraper()

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.scraper.get(url, proxies = self.requester.session.proxies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
        
    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                return res.content.decode('utf-8')
            else:
                return None
            # raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            return None
            # raise DownloadFailureException('Download failed - Unhandled Exception')