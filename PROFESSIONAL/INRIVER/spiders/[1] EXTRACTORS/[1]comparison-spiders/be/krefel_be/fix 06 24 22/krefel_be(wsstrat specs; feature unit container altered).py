import re, json
import datetime
import logging
import datetime
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class KrefelBeNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.krefel.be/nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = json.loads(raw_data)
        # soup = BeautifulSoup(raw_data, "lxml")
        # self.json_data = self.get_json_script(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result
    
    # def get_json_script(self, soup):
    #     source = Defaults.GENERIC_NOT_FOUND.value
    #     value = {}

    #     script_tag = soup.find("script", text=re.compile(".*priceCurrency.*")) or soup.find('script', text=re.compile('"@type": "Product"', re.IGNORECASE))

    #     if script_tag:
    #         try:
    #             source = str(script_tag)
    #             value = json.loads(script_tag.get_text(), strict=False)
    #         except ValueError:
    #             try:
    #                 value = re.sub('=(".*")\s*', "", script_tag.text, re.MULTILINE)
    #                 value = json.loads(value, strict=False)
    #             except ValueError:
    #                 rgx = re.search('=(".*")\s*', ' '.join(script_tag.stripped_strings))
    #                 if rgx:
    #                     txt = ' '.join(script_tag.stripped_strings).replace(rgx.group(1), '')
    #                     try:
    #                         value = json.loads(txt, strict=False)
    #                     except:
    #                         value = {}
                    


    #     return source, value

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # price_tag = soup.find('div',{'class':'right-content col-md-6'})
            # if price_tag:
            #     tag = self.json_data[1]
            #     source = self.json_data[0]
                
            #     if tag and 'offers' in tag.keys() and 'price' in tag['offers']:
            #         value = tag['offers']['price']
            #     else:
            #         value = tag['price']
            if 'price' in soup:
                value = str(soup['price']['value'])
                source = str(soup)

            if 'atp' in soup and 'addToCartMessage' in soup['atp']:
                availability = str(soup['atp']['addToCartMessage'])
                # as price is not hidden on the api but hidden on the PDP
                # override the price if no stock permanently.
                if availability == 'atp_button_nostock_perm':
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # tag = self.json_data[1]
            # source = self.json_data[0]
            
            # if tag and 'offers' in tag.keys() and 'priceCurrency' in tag['offers']:
            #     value = tag['offers']['priceCurrency']
            source = str(soup)
            if 'price' in soup:
                value = soup['price']['currencyIso']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            # tag = self.json_data[1]
            # source = self.json_data[0]
            # btn_tag = soup.select_one('div#add-to-cart-btn')
            # if btn_tag:
            #     value = btn_tag.get_text().strip()
            # elif tag and 'offers' in tag.keys() and 'availability' in tag['offers']:
            #     value = tag['offers']['availability']
            # else:
            #     tag = soup.select_one('.product-name')
            #     if tag and tag.has_attr('data-tracking-commerce'):
            #         data = tag['data-tracking-commerce']
            #         if data:
            #             _json = json.loads(data)
            #             if 'variant' in _json.keys():
            #                 source = str(_json)
            #                 value = _json['variant']
            # if value == '':
            #     tag = soup.find("script", text=re.compile('window.__data={"reduxAsyncConnect'))
            #     if tag:
            #         script_tag = tag.get_text().strip().split('window.__data=')[1]
            #         script_tag1 = script_tag.replace('{}}};','{}}}')
            #         _data = json.loads(script_tag1)
            #         if _data and 'product' in _data.keys() and 'productStockStatus' in _data['product']:
            #             value = str(_data['product']['productStockStatus'])
            
            if 'atp' in soup and 'addToCartMessage' in soup['atp']:
                value = str(soup['atp']['addToCartMessage'])
                source = str(soup)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews
            # tag = self.json_data[1]
            # source = self.json_data[0]
            
            # if tag and 'aggregateRating' in tag.keys() and 'ratingCount' in tag['aggregateRating']:
            #     reviews_source = source
            #     reviews_value = tag['aggregateRating']['ratingCount']
            # else:
            #     _data, _url = self.downloader._get_ratings_and_review()

            #     if _data:
            #         _json = json.loads(_data)

            #         if 'numberOfReviews' in _json:
            #             reviews_source, reviews_value = str(_url), str(_json['numberOfReviews'])

            # # Ratings
            # if tag and 'aggregateRating' in tag.keys() and 'ratingValue' in tag['aggregateRating']:
            #     score_source = source
            #     score_value = tag['aggregateRating']['ratingValue']
            # else:
            #     _data, _url = self.downloader._get_ratings_and_review()

            #     if _data:
            #         _json = json.loads(_data)

            #         if 'averageRating' in _json:
            #             score_source, score_value = str(_url), str(_json['averageRating'])

            # if reviews_value == 'NOT_FOUND':
            #     tag = soup.find('script',text=re.compile('"sku": "'))
            #     if tag:
            #         _json = json.loads(tag.get_text().strip())
            #         prod_id = _json['sku']
                
            #         req_url = f'https://api.krefel.be/api/v2/krefel/products/{prod_id}/reviews?pageSize=4&currentPage=0&sort=CREATIONTIME_DESC&fields=FULL'
            #         headers = {
            #                 "accept": "application/json",
            #                 "accept-encoding": "gzip, deflate, br",
            #                 "accept-language": "nl",
            #                 "origin": "https://www.krefel.be",
            #                 "referer": "https://www.krefel.be/",
            #                 "sec-ch-ua": "\"Google Chrome\";v=\"93\", \" Not;A Brand\";v=\"99\", \"Chromium\";v=\"93\"",
            #                 "sec-ch-ua-mobile": "?0",
            #                 "sec-ch-ua-platform": "\"Windows\"",
            #                 "sec-fetch-dest": "empty",
            #                 "sec-fetch-mode": "cors",
            #                 "sec-fetch-site": "same-site",
            #                 "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
            #                 }
            #         res = self.downloader.download(req_url,headers=headers)
            #         if res:
            #             _data = json.loads(res)
            #             reviews_source = str(req_url)
            #             score_source = str(req_url)
            #             if len(_data['reviews']) >= 2:
            #                 reviews_value = str(_data['pagination']['totalResults'])
            #                 score_value = str(_data['reviews'][1]['userExperienceRating'])
            #             if len(_data['reviews']) == 1:
            #                 reviews_value = str(_data['pagination']['totalResults'])
            #                 score_value = str(_data['reviews'][0]['rating'])
            

            #reviews
            reviews_source = str(soup)
            if 'numberOfReviews' in soup:
                reviews_value = str(soup['numberOfReviews'])

            #ratings
            score_source = str(soup)
            if 'averageRating' in soup:
                score_value = str(soup['averageRating'])


        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # tag = self.json_data[1]
            # source = self.json_data[0]
            
            # if tag and 'name' in tag:
            #     value = tag['name']
            # else:
            #     tag = soup.select_one('span.product')
            #     if tag:
            #         source, value = str(tag), tag.get_text(strip=True)
            source = str(soup)
            if 'name' in soup:
                value = soup['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # tag = self.json_data[1]
            # source = self.json_data[0]
            
            # if tag and 'brand' in tag.keys() and 'name' in tag['brand']:
            #     value = tag['brand']['name']
            
            # else:
            #     tag = soup.select_one('span.brand')
            #     if tag:
            #         source, value = str(tag), tag.get_text(strip=True)
            source = str(soup)
            if 'brand' in soup:
                value = soup['brand']['code']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # tag = soup.select_one('#product-description div.content-wrap')
            # if tag:
            #     source, value = str(tag), ' '.join(tag.stripped_strings)
            # else:
            #     tag = soup.select_one('#product-description')

            #     if tag:
            #         source, value = str(tag), ' '.join(tag.stripped_strings)
            source = str(soup)
            if 'description' in soup:
                tag = BeautifulSoup(soup['description'], "lxml")
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            # tags = soup.select("#product-specifications ul.block-list li")
            
            # if len(tags):
            #     source, value = str(tags), self._get_initial_specs(tags)

            # moved from row 392, wrong order of specs
            if ('brand' in soup and
                'code' in soup['brand'] and
                'code' in soup and
                'ean' in soup):

                value = {
                    'Krëfel code':soup['code'],
                    'Merk':soup['brand']['code'],
                    'EAN':soup['ean'],
                }
                
            source = str(soup)
            if 'classifications' in soup:
                specs_lst = soup['classifications']
                for specs in specs_lst:
                    if 'features' in specs:
                        lst = specs['features']
                        for i in lst:
                            feature_unit_name = ''
                            if 'name' in i and 'featureValues' in i:
                                if ('featureUnit' in i and 
                                    i['featureUnit'] and 
                                    'name' in i['featureUnit']):
                                    
                                    feature_unit_name = i['featureUnit']['name'] # feature unit container changed: https://prnt.sc/0PtxN1c6iMVB

                                if feature_unit_name:
                                    value[i['name']] = i['featureValues'][0]['value'] +' '+ feature_unit_name

                                elif (len(i['featureValues']) > 1): # if there are multiple values for each key https://prnt.sc/TC-fiaNRr0pz
                                    value[i['name']] = ','.join([x.get('value') for x in i['featureValues']]) 

                                else:
                                    value[i['name']] = i['featureValues'][0]['value']
            
            # additional specification required by validators but diversified sources; modification applied
            # common structure on comparisons pdp: https://prnt.sc/gwRYQNIhwEhh
            # if ('brand' in soup and
            #     'code' in soup['brand'] and
            #     'code' in soup and
            #     'ean' in soup):

            #     value.update({
            #         'Krëfel code':soup['code'],
            #         'Merk':soup['brand']['code'],
            #         'EAN':soup['ean'],
            #     })

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            # img_tag = soup.select('section.product-image-slider .c-product-main-slider .slick-list .slick-slide img')

            # if img_tag:
            #     source = str(img_tag)
            #     for imgs in img_tag:
            #         if imgs.has_attr("src"):
            #             img = re.sub(r'\d+x\d+.', '', imgs['src'])
            #             value.append(img)
            source = str(soup)
            if 'images' in soup:
                images = soup['images']
                for img in images:
                    value.append(img['url'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
        # temporary fix currently no sample item ids that has video url to compare all item ids are ACO https://prnt.sc/20aoqor
        # gtin = None
        # gtin_tag = self.json_data[1]
        # if gtin_tag and 'gtin13' in gtin_tag.keys():
        #     gtin = gtin_tag['gtin13']
        # date_time = datetime.datetime.now()
        # payload = [{
        #     'batch-id': "40de98ed-0dc0-45bc-8972-61e51d8c7ae8",
        #     'ean': gtin,
        #     'embedcode-version': "2.5.1",
        #     'is-mobile-device': False,
        #     'is-touch-device': False,
        #     'language': ["nl", "en"],
        #     'local-time': date_time.strftime("%X"), #"10:22:42",
        #     'location-url': self.downloader.url,
        #     'pageview-id': "216cb180-e802-4fff-8609-7e0deb1c48d8",
        #     'request-id': "4a912bc2-b2fc-495d-8861-ff74de607f94",
        #     'session-id': "61724e82-a3bf-4ca9-9383-deaa66df74cc"
        # }]
        # api_url = 'https://query.autheos.com/v3/video'

        # response = self.downloader.extra_download(api_url,data=payload)
        # if response:
        #     _json = json.loads(response)
        #     if _json and _json[0] and 'result' in _json[0][0].keys() and len(_json[0][0]['result']):
        #         source = api_url
        #         for item in (_json[0][0]['result']):
        #             if 'streams' in item and len(item['streams']):
        #                 value.append(item['streams'][-1]['url'])
        

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
        #     # tag = soup.find('script', text=re.compile('window.__data'))

        #     # if tag:
        #     #     _json = json.loads(tag.text.split('window.__data=')[-1][:-1])
        #     #     key = _json['product']['product']['atp']['stockMessage']
        #     #     if key in _json['i18n']['messages']['nl'].keys():
        #     #         source = str(tag)
        #     #         value = _json['i18n']['messages']['nl'][key]
                
        #     #     if 'overruleStockMessage' in _json['product']['product']['atp'] and _json['product']['product']['atp']['overruleStockMessage']:
        #     #         value = _json['product']['product']['atp']['overruleStockMessage']

        #     #     # New indicator
        #     #     tag = soup.select_one('.product-image-slider.disabled .product-end-of-life-message')

        #     #     # Value should be NOT_FOUND if tag exists
        #     #     if tag:
        #     #         source = value = Defaults.GENERIC_NOT_FOUND.value
        #     # source = str(soup)

            url = soup.get('url', None)
            id = self.get_product_code(url)
            raw = self.get_realtime_data(id)    
            if raw:
                try:
                    _json = json.loads(raw)
                    if _json and 'products' in _json:
                        product = _json['products'][0]
                        if product and 'deliveryPromises' in product and 'atp' in product:
                            stock_message = product['atp'].get('stockMessage', None)
                            if 'atp_message_stock' in stock_message:
                                promise_date = product['deliveryPromises'][0]['deliveryPromise']
                                end = promise_date.find('+')
                                iso_pdate = promise_date[:end] + '.000+00:00'
                                pro_date = datetime.datetime.fromisoformat(iso_pdate).replace(tzinfo=None)
                                date_now = datetime.datetime.now()
                                difference = pro_date - date_now
                                if int(difference.days) > 2:
                                    day_month = f'{str(pro_date.day).zfill(2)}/{str(pro_date.month).zfill(2)}'
                                    if pro_date.hour > 17:
                                        day_month = f'{str(pro_date.day+1).zfill(2)}/{str(pro_date.month).zfill(2)}'
                                    value = f'{day_month} in huis'
                                elif int(difference.days) == 2:
                                    value = 'Overmorgen in huis'
                                elif int(difference.days) < 2:
                                    value = 'Morgen in huis'
                                source = str(_json)

                except:
                    # if exception occured try the other source
                    pass
            if value == 'NOT_FOUND':
                if 'stockMessage' in soup['atp']:
                    value = str(soup['atp']['stockMessage'])
                    source = str(soup)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()

        for tag in tags:
            tag_key = tag.select_one('span.name')
            tag_val = tag.select_one('span.value')

            if tag_key and tag_val:
                key = tag_key.get_text().strip()
                if tag_val.select_one('.check-green-inversed'):
                    value = "Yes"
                elif tag_val.select_one('.cross-red-inversed'):
                    value = "No"
                else:
                    value = " ".join(tag_val.stripped_strings)
                result[key] = value

        return result

    @staticmethod
    def strip_html_tags(tags):
        clean = re.compile('<.*?>')
        sanitized_text = re.sub(clean, " ", tags)
        return sanitized_text

    def get_realtime_data(self, product_code):
        try:
            headers = {
                "authority":"api.krefel.be",
                "accept":"application/json",
                "accept-language":"nl",
                "origin":"https://www.krefel.be",
                "referer":"https://www.krefel.be/",
                "sec-ch-ua":'" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
                "sec-ch-ua-mobile":"?0",
                "sec-ch-ua-platform":"Windows",
                "sec-fetch-dest":"empty",
                "sec-fetch-mode":"cors",
                "sec-fetch-site":"same-site",
                "user-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36"
            }

            api_url = f'https://api.krefel.be/api/v2/krefel/products/realtimeinformation?productCodes={product_code}&lang=nl'

            raw = self.downloader.extra_download(api_url, headers=headers)
            if raw:
                return raw

        except:
            pass
            # do nothing if errro
        return None

    def get_product_code(self, url):
        match = re.search(r'p/(\d+)-', url)
        if match:
            return str(match.group(1))
        return None
