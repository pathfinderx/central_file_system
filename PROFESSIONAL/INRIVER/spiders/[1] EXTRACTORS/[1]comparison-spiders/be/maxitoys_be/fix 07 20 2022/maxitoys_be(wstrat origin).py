import logging
import re, json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MaxitoysBeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.maxitoys.be'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup, result)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup, result)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('#fiche-produit > div.produit-content-right > div > div.produit-refs > div span.prix')

            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = soup.select('script[type="application/ld+json"]')
            for tag in tags:
                json_data = json.loads(tag.contents[0])
                if 'offers' in json_data:
                    source, value = str(tag), json_data['offers'][0]['priceCurrency']
                    break

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = soup.select('script[type="application/ld+json"]')
            for tag in tags:
                json_data = json.loads(tag.contents[0])
                if 'offers' in json_data:
                    source, value = str(tag), json_data['offers'][0]['availability']
                    break

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
        #  Not Available

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.produit-titre-tags h1')

            if tag:
                source, value = str(tag), tag.text.strip()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.produit-attributs div.inline a.attribut-value')

            if tag:
                source, value = str(tag), tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:

            long_tag = soup.select_one('#produit-desc-content div.descarticle')

            if long_tag:
                source = str(long_tag)
                value = ' '.join(long_tag.stripped_strings)
            # DON'T DELETE SHORT DESCRIPTION  
            # else:
            #     short_tag = soup.select_one('div.produit-refs p.short-desc')
            #     if short_tag:
            #         source = str(short_tag)
            #         value = short_tag.text.strip()

            if value == '':
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('#produit-cara-content tr')

            if len(tags):
                source = str(tags)

                for i in tags:
                    if ':' in i.get_text():
                        key = i.select_one('td:first-child').text.strip()
                        val = i.select_one('td:nth-child(2)').text.strip()
                        value[key] = val
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('#productPageImages picture')

            if len(tags):
                source = str(tags)
                for k in tags:
                    sourcetag = k.select_one('source')
                    if sourcetag and sourcetag.has_attr('data-frz-srcset'):
                        value.append(sourcetag.get('data-frz-srcset'))
                    else:
                        imgtag = k.select_one('img')
                        if imgtag and imgtag.has_attr('src'):
                            value.append(imgtag.get('src'))
                # value = [i.contents[0].get('data-frz-srcset') for i in tags]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup, result):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.dispo.inline') or soup.select_one('div.pasdispo.inline')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)

            if result['availability']['value'] in ['http://schema.org/OutOfStock']:
                value = 'Non disponible'

            if result['availability']['value'] in ['http://schema.org/InStock']:
                value = 'En stock'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value
        
        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value
