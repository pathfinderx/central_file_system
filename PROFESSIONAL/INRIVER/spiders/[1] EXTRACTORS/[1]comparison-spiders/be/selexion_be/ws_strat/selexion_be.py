import json
import logging
import re
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class SelexionBeWebsiteStrategy(WebsiteStrategy):
	WEBSITE = 'www.selexion.be'

	def __init__(self, downloader):
		self.downloader = downloader
		self.logger = logging.getLogger(__name__)
		
	def execute(self, raw_data):
		result = get_result_base_template()
		soup = BeautifulSoup(raw_data, "lxml")
		_json = self.get_json(soup)

		# Extract price
		result["price"]["source"], result["price"]["value"] = self.get_price(soup, _json)

		# Extract currency
		result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup, _json)

		# Extract availability
		result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, _json)

		# Extract ratings (score and reviews)
		result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
			result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
			result["rating"]["reviews"]["value"] = self.get_rating(soup)

		# Extract title
		result["title"]["source"], result["title"]["value"] = self.get_title(soup, _json)

		# Extract brand
		result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup, _json)

		# Extract description
		result["description"]["source"], result["description"]["value"] = self.get_description(soup, _json)

		# Extract specifications
		result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

		# Extract image urls
		result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

		# Extract video urls
		result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

		# Extract delivery
		result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup, _json)

		# Extract shipping
		result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

		return result

	def get_json (self, soup):
		_json = {}
		try:
			tag = soup.find('script', {'type':'application/ld+json'})
			if tag:
				_json = json.loads(tag.text.strip().replace('\n', "").replace('\t', ""))
		except Exception as e:
			self.logger.exception(e)
		return _json

	def get_price(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value
		
		try:
			if _json and'offers' in _json and 'price' in _json.get('offers'):
				source, value = str(_json), _json['offers']['price']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value

		return source, value

	def get_currency(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and  'offers' in _json and 'priceCurrency' in _json.get('offers'):
				source, value = str(_json), _json['offers']['priceCurrency']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.CURRENCY_EXTRACTION.value

		return source, value

	def get_availability(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and  'offers' in _json and 'availability' in _json.get('offers'):
				source, value = str(_json), _json['offers']['availability']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value
			
		return source, value

	def get_rating(self, soup):
		score_source = Defaults.GENERIC_NOT_FOUND.value
		score_value = Defaults.GENERIC_NOT_FOUND.value
		score_max_value = "5"  # For this site, max rating is 5 stars
		reviews_source = Defaults.GENERIC_NOT_FOUND.value
		reviews_value = Defaults.GENERIC_NOT_FOUND.value
 
		try:
			pass # no ratings seen during product search and development

		except Exception as e:
			self.logger.exception(e)
			score_value = FailureMessages.RATING_EXTRACTION.value
			reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

		return score_source, score_value, score_max_value, reviews_source, reviews_value

	def get_title(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and 'name' in _json:
				source, value = str(_json), _json['name']
				
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_brand(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and 'brand' in _json and 'name' in _json.get('brand'):
				source, value = str(_json), _json['brand']['name']
				
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_description(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and 'description' in _json and _json.get('description'):
				source, value = str(_json), _json['description'].strip()
				
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_specifications(self, soup):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = {}

		try:
			tag = soup.find_all('div', class_="techinfo-wrap")
			if tag:
				for i in tag:
					value.update({i.select_one('strong').text.strip(): i.select_one('span').text.strip()})
				source = str(tag)
					
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

		return source, value        

	def get_image_urls(self, soup):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = []  # image_urls data type must be list

		try:
			tag = soup.find_all('div', class_="thumb")
			if tag:
				for i in tag:
					if i.select_one('a').get('href'):
						value.append(i.select_one('a')['href'])
				source = str(tag)

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.IMAGE_URLS_EXTRACTION.value

		return source, value

	def get_video_urls(self, soup):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = []  # video_urls data type must be list

		# website doesn't support videos
		return source, value

	def get_delivery(self, soup, _json):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if _json and  'offers' in _json and 'availability' in _json.get('offers'):
				source, value = str(_json), _json['offers']['availability']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_shipping(self, soup):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value
	
		try:
			pass

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.SHIPPING_EXTRACTION.value

		return source, value
