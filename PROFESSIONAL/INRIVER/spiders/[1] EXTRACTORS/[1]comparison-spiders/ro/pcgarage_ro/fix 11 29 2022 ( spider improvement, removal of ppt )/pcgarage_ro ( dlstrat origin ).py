from ..base import DownloadStrategy
from ..exceptions import DownloadFailureException
import cloudscraper, bs4

class PcgarageRoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            scraper = cloudscraper.CloudScraper()
            
            for i in range(5):
                try:
                    response = scraper.get(url)
                    if response.status_code in [200]:
                        break
                    else:
                        continue
                except Exception as e:
                    print(e)
                    continue

            base_url = 'https://www.pcgarage.ro'
            path = url.split(base_url)[-1]
            headers = {
                "host": "www.pcgarage.ro",
                "Accept": "*/*",
                "Content-Type": "application/json",
                ":path:": f'{path}',
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.50",
            }

            while True:
                try:
                    response = scraper.post(url, headers)
                    soup = bs4.BeautifulSoup(response.text, 'lxml')

                    if response.status_code in [200] and soup.select_one('span.price_num'):
                        break
                    else:
                        continue

                except:
                    # continue because scraper.post throws an exception
                    continue

            return response.text

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
