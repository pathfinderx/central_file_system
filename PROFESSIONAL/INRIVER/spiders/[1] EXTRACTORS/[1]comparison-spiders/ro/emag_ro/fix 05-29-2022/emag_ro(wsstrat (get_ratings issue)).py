import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class EmagRoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.emag.ro'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.raw_data = None
        
    def execute(self, raw_data):
        result = get_result_base_template()
        self.raw_data = raw_data
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_delivery(soup)

        return result

    def get_price(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # price_tag = data.select_one("p.product-new-price")
            price_tag = data.select_one(".product-highlight.product-page-pricing .product-new-price")
            
            if price_tag:
                source, value = str(price_tag), ''.join(price_tag.find_all(text=True, recursive=False)).replace(".", "").strip()
                
            if price_tag and price_tag.select_one("sup"):
                before_fraction = ''.join(price_tag.find_all(text=True, recursive=False)).replace(".", "").strip()
                after_fraction = price_tag.select_one("sup").text.strip()
                value = str(before_fraction + "." + after_fraction)
                source = str(price_tag)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = "RON"

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_container = soup.select_one('form.main-product-form')
            tag = tag_container.find('span', class_=re.compile(r'label-.+')) if tag_container else None
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
            else:
                tag = soup.select_one('.main-product-form .product-highlight a.btn.btn-primary') or soup.select_one('.product-buy-area-wrapper div a') \
                    or soup.select_one('.product-buy-area-wrapper button[type="submit"]')
                if tag:
                    source, value = str(tag),tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = soup.select_one('span.star-rating-text')
            tag_score_option = soup.select_one('a.rating-text.gtm_rp101318' or 'a[href="#reviews-section"]')
            tag_review = soup.select('a[href="#reviews-section"]')[-1]
            if tag_score:
                score_value = tag_score.text.strip()
                score_source = str(tag_score)

            if tag_score_option and score_value == '':
                rating_score_only = re.search(r'([\d]+\.[\d]+)', tag_score_option.get_text().strip())
                if rating_score_only:
                    score_source, score_value = str(tag_score_option), rating_score_only.group(0)

            if score_value == '':
                score_source = Defaults.GENERIC_NOT_FOUND.value
                score_value = Defaults.GENERIC_NOT_FOUND.value
                score_max_value = "5"

            if tag_review:
                reviews_value = str(re.sub("[^0-9]", "", tag_review.text))
                reviews_source = str(tag_review)
            if reviews_value == '':
                reviews_source = Defaults.GENERIC_NOT_FOUND.value
                reviews_value = Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.page-header .page-title')
            if tag:
                source, value = str(tag),tag.text.strip()
            else:
                tag = soup.select_one('[property="og:title"]')

                if tag and tag.has_attr('content'):
                    source, value = str(tag), tag.get('content').replace(' - eMAG.ro', '')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = soup.find_all('script', type='application/ld+json')
            tag = next((i for i in tags if i.string and re.search(r'"@type":\s*"Product"', i.string)), None)

            if tag:
                data = json.loads(tag.string)
                if data['brand']['name']:
                    source, value = str(tag), data['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#description-body') or soup.select_one('div.product-page-description-text')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings) #tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select_one('table[class="table table-striped specifications-table"]')
            tags = tag.select('tr')

            if len(tags):
                source = str(tags)

                for i in tags:
                    key, val = i.findChildren()[:2]
                    key = key.text.strip()
                    val = val.text.strip()
                    value[key] = val            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
                #Incorrect Image Count
            # tags = soup.select('a.product-gallery-image')

            # if len(tags):
            #     source = str(tags)
            #     for img in tags:
            #         img_url = img.get('href')
            #         if not img_url.startswith('http'):
            #             img_url = "https://www.emag.ro" + img_url

            #         value.append(img_url)

            tags = soup.select('.product-gallery a')

            if len(tags):
                source = str(tags)
                for img in tags:
                    if img.has_attr('href'):
                        value.append(img.get('href'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.select_one('[data-ph-id="video"] a')
            if tag and tag.has_attr('data-videos'):
                vid_data = json.loads(tag.get('data-videos'))
                if len(vid_data):
                    source = str(tag)
                    for vid in vid_data:
                        value.append(vid['mp4']['high']['url'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.label.label-in_stock') or soup.select_one('.label.label-limited_stock_qty')\
                or soup.select_one('.label.label-out_of_stock')
            if tag:
                source, value = str(tag), tag.get_text(strip=True)
               

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # website doesn't support shipping
        return source, value
