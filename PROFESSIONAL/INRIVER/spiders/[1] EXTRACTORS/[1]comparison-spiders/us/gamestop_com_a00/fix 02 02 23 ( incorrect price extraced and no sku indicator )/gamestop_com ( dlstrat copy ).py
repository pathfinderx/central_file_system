import cloudscraper
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class GamestopComDownloadStrategy(DownloadStrategy):

    def __init__(self, requester):
        self.requester = requester
        self.proxies = self.requester.proxies
        self.scraper = cloudscraper.create_scraper()
    def download(self, url, timeout=60, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)
        
        if not isinstance(headers, dict):
            headers = {
                "accept" : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                "accept-encoding" : 'gzip, deflate, br',
                "accept-language" : 'en-US,en;q=0.9',
                "cache-control" : 'max-age=0',
                "upgrade-insecure-requests" : '1',
                "user-agent" : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
            }        

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                response = self.scraper.get(url, timeout=timeout, headers=headers, proxies=self.proxies)

            status_code = response.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in response.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return response.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')