import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template
from strategies.download.exceptions import DownloadFailureException
from strategies.website.us.gamestop_com import GamestopComWebsiteStrategy


class GamestopComA00WebsiteStrategy(GamestopComWebsiteStrategy):
    WEBSITE = 'www.gamestop.com'

    def __init__(self, downloader):
        super().__init__(downloader)
        self.logger = logging.getLogger(__name__)
        self.__product_id = None

    def get_price(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if data['offers']:
                    sku = None
                    url = data['url']
                    final_data = {}
                    rgx = re.search(r'/(\d+).html',url)
                    if rgx:
                        sku = rgx.group(1)
                    if sku and [x for x in data['offers'] if x['sku'] == sku]:
                        final_data['offers']  = [x for x in data['offers'] if x['sku'] == sku]
                    else:
                        final_data['offers'] = data['offers']
                    if 'name' in final_data['offers'][0]:
                        name = final_data['offers'][0]['name']
                        if name in ['New','Refurbished']:
                            if 'offers' in final_data and 'price' in final_data['offers'][0]:
                                value = final_data['offers'][0]['price']
                        elif name == 'Pre-Owned':
                            # https://trello.com/c/2cwVXOD6/1214-ms-price-comparisons-incorrect-data-in-gamestopcom
                            value = Defaults.GENERIC_NOT_FOUND.value
                            pre_owned = True
                else:
                    tag = soup.select_one('.primary-details-row .selling-price-redesign')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()
            # for promo:
            tag = soup.select_one('.primary-details-row .selling-price-redesign.has-savings span')
            if tag and not pre_owned:
                sale_tag = soup.select_one('div.prices.has-condition span.badge-container-redesign.has-savings') or soup.select_one('.primary-details-row .selling-price-redesign.has-savings span')
                if sale_tag:
                    source = str(tag)
                    value = tag.get_text().strip()
            else:
                tag = soup.select_one('#__NEXT_DATA__')
                if tag:
                    _json = json.loads(tag.text.strip())
                    if _json:
                        props_data = _json['props']['pageProps']['product']  
                        if props_data:
                            badge = props_data['badges'][0] if props_data['badges'] else None
                            if badge:
                                promo = [x for x in props_data['promotions'] if x['promotionHeading'] == badge]
                                if promo:
                                    value = str(promo[0]['promotionalPrice'])


            stock_tag = soup.select_one('.product-unavailable-redesign:not(.d-none)')
            if stock_tag:
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value

            bypass_titles = ['Microsoft Xbox Series X Controller Shock Blue','Microsoft Xbox Series X Wireless Controller Aqua Shift',
                'Microsoft Xbox Series X Wireless Controller Shock Blue']  

            #temporary fix
            if self.get_title(data)[1] in bypass_titles:
                tag_price = soup.select_one('#primary-details-row .actual-price')
                if tag_price:
                    source, value = str(tag_price), tag_price.get_text().strip()
                
            
            """ check if availability is OOS price should be -1.
            In special cases, check if offers is InStock then extract the value.
            FIND ANOTHER CHECKER FOR PRICE FOR MORE OPTIONS
            """
            if (data['offers'] and 
                'name' in data['offers'][0]):

                name = data['offers'][0]['name']

                if (name == 'New' and 
                    'offers' in data and 
                    'availability' in data['offers'][0]):

                    availability = data['offers'][0]['availability']

                    if (availability == 'https://schema.org/OutOfStock'):
                        source = Defaults.GENERIC_NOT_FOUND.value
                        value = Defaults.GENERIC_NOT_FOUND.value

                    elif (availability == 'https://schema.org/InStock'):
                        source, value = str(data['offers']), data['offers'][0]['price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_availability(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if data['offers']:
                    if 'name' in data['offers'][0]:
                        name = data['offers'][0]['name']
                        if name == 'New':
                            if 'offers' in data and 'availability' in data['offers'][0]:
                                value = data['offers'][0]['availability']
                        elif name == 'Pre-Owned':
                            value = name
                else:
                    tag = soup.select_one('.add-to-cart')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()
                
            #check if OOS 
            check_oos = soup.select_one('#add-to-cart-buttons button')
            if check_oos and check_oos.get_text().strip() == 'Unavailable':
                source, value = str(check_oos), check_oos.get_text().strip().lower()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value