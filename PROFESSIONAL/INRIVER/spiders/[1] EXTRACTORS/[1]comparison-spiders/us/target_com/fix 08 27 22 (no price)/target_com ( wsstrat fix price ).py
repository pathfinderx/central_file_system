import json
import logging
import re,time,urllib

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class TargetComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.target.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        _json = json.loads(raw_data)
        base_url = _json['base_url']
        soup = BeautifulSoup(_json['result_text'], "lxml")
        self.tcin = None
        self.totaltime = None
        starttime=time.time()
        lasttime=starttime
        self.source,self.api = self.get_api_data(soup)
        lasttime = self.timer(starttime,lasttime,'API Data Request')

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)
        lasttime = self.timer(starttime,lasttime,'Price')

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)
        lasttime = self.timer(starttime,lasttime,'Currency')

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, base_url)
        lasttime = self.timer(starttime,lasttime,'Availability')

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)
        lasttime = self.timer(starttime,lasttime,'Ratings and Reviews')

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)
        lasttime = self.timer(starttime,lasttime,'Title')

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)
        lasttime = self.timer(starttime,lasttime,'Brand')

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup,base_url)
        lasttime = self.timer(starttime,lasttime,'Description')

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)
        lasttime = self.timer(starttime,lasttime,'Specifications')

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)
        lasttime = self.timer(starttime,lasttime,'Image URLs')

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)
        lasttime = self.timer(starttime,lasttime,'Currency')

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)
        lasttime = self.timer(starttime,lasttime,'Video URLs')

        print("Total Scrape Time: "+str(self.totaltime)+" s")
        return result

    def timer(self,starttime,lasttime,metric):
        laptime=round((time.time() - lasttime), 2)
        self.totaltime=round((time.time() - starttime), 2)
        print("Scrape Time ("+metric+"): "+str(laptime)+" s")
        lasttime=time.time()

        return lasttime

    def get_api_data(self, soup):
        result = {}
        api_url = None
        key = None
        tcin = None
        try:
            temp_json = self.get_product_data(soup)
            tag = soup.find("script", text = re.compile('window.__PRELOADED_STATE__= {')) 
            if tag:
                _rgx = re.search("({.*})", tag.get_text().strip())
                
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    _json = json.loads(json_data)
                    key = _json["config"]["firefly"]['apiKey']
                    tcin = _json["product"]["selectedTcin"]

            else:
                tag = soup.find('script',text = re.compile('apiKey'))
                if tag:
                    _rgx = re.search("({.*})", tag.get_text().strip())
                    
                    # remove undefined and any object instantation (new ...)
                    # from object literal so that it can be qualified as
                    # valid json
                    json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                    json_data = re.sub(r':undefined', ':null', json_data)
                    
                    if _rgx: 
                        _json = json.loads(json_data.replace('\\"','"'))
                        key = _json['defaultServicesApiKey']
                        if temp_json and 'sku' in temp_json:
                            tcin = temp_json['sku']
                        else:
                            sku_tag = soup.select_one('[data-tcin]')
                            if sku_tag:
                                tcin = sku_tag['data-tcin']

            if key and tcin:
                api_url = f'https://redsky.target.com/redsky_aggregations/v1/web/pdp_client_v1?key={key}&tcin={tcin}&store_id=3229&pricing_store_id=3229&scheduled_delivery_store_id=3229&has_financing_options=true&visitor_id=017C53F55DA60201B1867594C3728D20&has_size_context=true&latitude=40.720&longitude=-74.000&state=NY&zip=10013'
                raw_data = self.downloader.extra_download(api_url)
                _json = json.loads(raw_data)
                if _json:
                    result['pdp_client'] = json.loads(_json['result_text'])
                    self.tcin = tcin

                req_url = f'https://redsky.target.com/redsky_aggregations/v1/web/pdp_fulfillment_v1?key={key}&tcin={tcin}&store_id=1771&store_positions_store_id=1771&has_store_positions_store_id=true&zip=52404&state=IA&latitude=41.9831&longitude=-91.6686&scheduled_delivery_store_id=1771&pricing_store_id=1771&fulfillment_test_mode=grocery_opu_team_member_test&is_bot=false'
                response = self.downloader.extra_download(req_url)
                if response:
                    response_json = json.loads(response)
                    if response_json:
                        result['fulfillment'] = json.loads(response_json['result_text'])

        except:
            result = {}

        return api_url,result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if self.api:
                data = self.api['pdp_client']
                tcin = self.tcin
                if data and  'data' in data.keys() and 'product' in data['data'] and 'children' in data['data']['product']:
                    children = data['data']['product']['children']
                    for child in children:
                        if tcin in str(child):
                            value = child['price']['current_retail']
                            source = str(child['price'])
                            break

                else:
                    if data and  'data' in data.keys() and 'product' in data['data'] and 'price' in data['data']['product']:
                        if data['data']['product']['price']['is_current_price_range'] is True:
                            value = str(data['data']['product']['price']['current_retail_min'])
                            source = str(data['data']['product']['price'])
                        else:
                            source = str(data['data']['product']['price'])
                            value = str(data['data']['product']['price']['formatted_current_price'])
            else:
                tag = soup.select_one('span[data-test="product-price"]')
                if tag:
                    source, value = str(tag), tag.get_text().strip()

            if value == 'NOT_FOUND':
                _json = self.get_product_data(soup)
                if _json and 'offers' in _json:
                    offers = _json['offers']
                    if offers:
                        value = str(offers['price'])
                        source = str(_json)
                else:
                    script_tag, data, _json = None, None, None
                    for x in soup.select('script'):
                        if (re.search(r'window\.__CONFIG__ = JSON.parse\(',str(x))):
                            script_tag = x
                
                    data = re.search(r'window\.__TGT_DATA__ = JSON\.parse\(\"([\s\S]+)}\"\);', script_tag.get_text(strip=True).replace('\\', '')).group(1) + '}'
                    _json = json.loads(data) if data else None

                    if ('__PRELOADED_QUERIES__' in _json and 
                        'queries' in _json['__PRELOADED_QUERIES__']):

                        queries = _json.get('__PRELOADED_QUERIES__').get('queries')[1][1]
                        if (queries and 
                            'product' in queries and 
                            'price' in queries['product'] and
                            'formatted_current_price' in queries['product']['price']):

                            source, value = str(_json), queries['product']['price']['formatted_current_price']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                source = str(tag)
                if "offers" in _json['@graph'][0].keys():
                    content = _json['@graph'][0]['offers']['priceCurrency']
                
                    if content:
                        value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup, base_url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # prod_id = base_url.split('?')[0].split('-')[-1].split('#')[0]
            # req_url = f'https://redsky.target.com/redsky_aggregations/v1/web/pdp_fulfillment_v1?key=ff457966e64d5e877fdbad070f276d18ecec4a01&tcin={prod_id}&store_id=1771&store_positions_store_id=1771&has_store_positions_store_id=true&zip=52404&state=IA&latitude=41.9831&longitude=-91.6686&scheduled_delivery_store_id=1771&pricing_store_id=1771&fulfillment_test_mode=grocery_opu_team_member_test&is_bot=false'

            # result = self.downloader.download(req_url)
            # _json = json.loads(result)
            # if _json:
            if self.api:
                data = self.api['fulfillment'] #json.loads(_json['result_text'])
                source  = self.source #req_url
                value = data['data']['product']['fulfillment']['shipping_options']['availability_status']

            else: 
                _json = None
                tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
                if tag:
                    _json = json.loads(tag.get_text())

                if _json:
                    source = str(tag)
                    if "offers" in _json['@graph'][0].keys():
                        content = _json['@graph'][0]['offers']['availability']
                    
                        if content:
                            value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                if "aggregateRating" in _json['@graph'][0].keys():
                    # Reviews
                    if "reviewCount" in _json['@graph'][0]["aggregateRating"].keys():
                        reviews_source = str(tag)
                        reviews_value = str(_json['@graph'][0]["aggregateRating"]["reviewCount"])

                    # Ratings
                    if "ratingValue" in _json['@graph'][0]["aggregateRating"].keys():
                        score_source = str(tag)
                        score_value = str(_json['@graph'][0]["aggregateRating"]["ratingValue"])
                else:
                    # if "sku" in _json['@graph'][0].keys():
                    #     tcin = _json['@graph'][0]['sku']
                    #     res = self.downloader.download_ratings(tcin)
                    #     if res:
                    if self.api:
                        _json = {}
                        ratings = self.api['pdp_client']
                        if ratings and 'data' in ratings and 'product' in ratings['data'] and 'ratings_and_reviews' in ratings['data']['product'] and 'statistics' in ratings['data']['product']['ratings_and_reviews'] and 'rating' in ratings['data']['product']['ratings_and_reviews']['statistics']:
                            #score  
                            if ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['average'] or ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['average'] == 0.0:
                                _json['score'] = ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['average']
                
                            #review    
                            if ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['count'] or ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['count'] == 0:
                                _json['review'] = ratings['data']['product']['ratings_and_reviews']['statistics']['rating']['count']

                        if _json and _json['score'] != 0.0 and _json['review'] != 0:
                            score_value = str(_json['score'])
                            reviews_value = str(_json['review'])
                            score_source = self.source #_json['url']
                            reviews_source = self.source #_json['url']
                        else:
                            score_value = Defaults.GENERIC_NOT_FOUND.value
                            reviews_value = Defaults.GENERIC_NOT_FOUND.value
            else:
                payload_prerequisites, tag = {
                    'storeID': 2830  # as observed, regardless of altering values for store_id -> values for ratings and reviews remains unchanged (https://prnt.sc/6pmkfsQQsxKs)
                }, soup.select('script')

                if tag:
                    for k,v in enumerate(tag):
                        if (re.search(r'apiKey', str(v)) or re.search(r'apiKey', str(v))):
                            raw_string = str(v).replace('\\','')
                            apiKey_matcher = (re.search(r'apiKey":"([\s\S]+?)",', raw_string)).group(1)
                            tcin_matcher = (re.search(r'tcin":"([\s\S]+?)",', raw_string)).group(1)
                            payload_prerequisites['apiKey'] = apiKey_matcher if apiKey_matcher else None
                            payload_prerequisites['tcin'] = tcin_matcher if tcin_matcher else None

                    if (payload_prerequisites and
                        'apiKey' in payload_prerequisites and
                        'tcin' in payload_prerequisites):

                        # api for ratings can't be searched in network activity. Use network activity filtering instead (https://prnt.sc/LpYbd_xAwI1X)
                        ratings_api_url = f'https://redsky.target.com/redsky_aggregations/v1/web/pdp_client_v1?key={payload_prerequisites["apiKey"]}&tcin={payload_prerequisites["tcin"]}&is_bot=false&member_id=0&store_id={payload_prerequisites["storeID"]}&pricing_store_id={payload_prerequisites["storeID"]}&has_pricing_store_id=true&scheduled_delivery_store_id={payload_prerequisites["storeID"]}&has_financing_options=true'

                        raw_data = self.downloader.extra_download(ratings_api_url)

                        if raw_data:
                            _json = json.loads(json.loads(raw_data).get('result_text'))

                            if (_json and 'data' in _json and
                                'product' in _json['data'] and
                                'ratings_and_reviews' in _json['data']['product'] and
                                'statistics' in _json['data']['product']['ratings_and_reviews'] and 
                                'rating' in _json['data']['product']['ratings_and_reviews']['statistics']):

                                score_source, score_value = str(_json), _json['data']['product']['ratings_and_reviews']['statistics']['rating']['average']
                                reviews_source, reviews_value = str(_json), _json['data']['product']['ratings_and_reviews']['statistics']['rating']['count']
        
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title = soup.select_one('h1[data-test="product-title"]')
            if title:
                source = str(title)
                value = title.text.strip()
            else:
                tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
                if tag:
                    _json = json.loads(tag.get_text())

                    if _json:
                        source = str(tag)
                        content = _json['@graph'][0]['name']
            
                        if content:
                            value = content         
      

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
        #     _json = None
        #     tag = soup.find("script", text=re.compile('.*"@type":"Product".*'), attrs= {"type":"application/ld+json"})
        #     if tag:
        #         _json = json.loads(tag.get_text())

        #     if _json:
        #         source = str(tag)
        #         content = _json['@graph'][0]['brand']
            
        #         if content:
        #             value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup,base_url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc = []
            src = []
            _json = None
            sku = None
            if self.api:
                if 'product_description' in self.api['pdp_client']['data']['product']['item'] and 'soft_bullets' in self.api['pdp_client']['data']['product']['item']['product_description']\
                    and self.api['pdp_client']['data']['product']['item']['product_description']['soft_bullets']['bullets']:
                    desc.append('Highlights')
                    for item in self.api['pdp_client']['data']['product']['item']['product_description']['soft_bullets']['bullets']:
                        desc.append(item)
            else:
                tag = soup.find('h3',text = re.compile('Highlights'))
                if tag:
                    tag = tag.parent
                    src.append(str(tag))
                    desc.append(' '.join(tag.stripped_strings))

            tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                sku = _json['@graph'][0]['sku']
            
                api_url = "https://scontent.webcollage.net/target/power-page?ird=true&channel-product-id=%s" % sku
                raw_data = self.downloader.extra_download(api_url)
                _json = json.loads(raw_data) if raw_data else None
                if _json:
                    src.append(api_url)
                    _rgx = re.search(r'html.*\"(<div.*)\"[\s\S]*\}[\s\S]*\}\;', _json['result_text'])
                    if _rgx:
                        desc_soup = BeautifulSoup(_rgx.group(1), 'lxml')
                        
                        for _script in desc_soup.select('script'):
                            _script.decompose()
                        for _style in desc_soup.select('style'):
                            _style.decompose()

                        content = " ".join(desc_soup.stripped_strings)

                        if content:
                            desc.append(content)
            if sku:
                temp_desc = []
                params = {
                    'u': '3EFB2931-F311-4A61-ABA1-DF240A9ADA87',
                    'siteid': '72d23878-8496-46b3-a3b1-9fe02eda2e53',
                    'pageid': sku,
                    's': int(time.time()),
                    'v': 'v1.0.231',
                    'visitid': '20DB735E-CE26-45CC-8881-53D217789A21',
                    'ref': '',
                    'r': '0.22133186637249813',
                    'pageurl': base_url
                }
                api_url = f'https://content.syndigo.com/page/72d23878-8496-46b3-a3b1-9fe02eda2e53/{sku}.json?{urllib.parse.urlencode(params)}'
                response = self.downloader.extra_download(api_url)
                if response:
                    result_json = json.loads(response)
                    _json = json.loads(result_json['result_text'])
                    if _json:
                        for k in _json['experiences'].keys():
                            data = _json['experiences'][k]['experiences']['power-page']['widgets']
                            for _k in data.keys():
                                _data = data[_k]
                                if 'widgetType' in _data and (_data['widgetType'] == 'FeatureSet'):
                                    for i in _data['items']:
                                        if i.get('features'):
                                            for x in i['features']:
                                                if x.get('caption'):
                                                    temp_desc.append(x['caption'])
                                                if x.get('description'):
                                                    desc_soup = BeautifulSoup(x['description'],'lxml')
                                                    temp_desc.append(' '.join(desc_soup.stripped_strings))
                if temp_desc:
                    src.append(api_url)
                    desc.append(' '.join(temp_desc))

            tag = soup.find('h3',text = re.compile('Description'))
            if tag:
                desc_tag = tag.parent
                if desc_tag:
                    # if desc_tag.select_one('[data-test="wellnessBadgeAndDescriptionsContainer"]'):
                    #     desc_tag.select_one('[data-test="wellnessBadgeAndDescriptionsContainer"]').extract()
                        
                    src.append(str(desc_tag))
                    desc.append(' '.join(desc_tag.stripped_strings))
            else:
                tag = soup.select_one('#specAndDescript .styles__StyledCol-sc-ct8kx6-0.yqCvw')
                if tag:
                    src.append(str(desc_tag))
                    desc.append(' '.join(tag.stripped_strings))

            if desc:
                source = " + ".join(src)
                value = " ".join(desc)
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            _json = {}
            tag = soup.find("script", text = re.compile(r"window\.__PRELOADED_STATE__"))
            if tag:
                _rgx = re.search("({.*})", tag.get_text().strip())

                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)

                if _rgx: 
                    _json = json.loads(json_data)
                    if _json:
                        specs = _json['product']['productDetails']['item']['itemDetails']['bulletDescription']

            elif self.api:
                if 'product_description' in self.api['pdp_client']['data']['product']['item'] and 'bullet_descriptions' in self.api['pdp_client']['data']['product']['item']['product_description']\
                    and self.api['pdp_client']['data']['product']['item']['product_description']['bullet_descriptions']:
                    specs = self.api['pdp_client']['data']['product']['item']['product_description']['bullet_descriptions']
                
            if specs:
                source = str(tag)
                value = self._get_initial_specs(specs)
                            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find("script", text = re.compile(r"window\.__PRELOADED_STATE__"))
            if tag:
                _rgx = re.search("({.*})", tag.get_text().strip())
                
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)

                if _rgx: 
                    _json = json.loads(json_data)
                    if _json:
                        source = str(tag)
                        tcin = None
                        if 'selectedTcin' in _json['product'] and _json['product']['selectedTcin']:
                            tcin = _json['product']['selectedTcin']
                        if tcin and 'children' in _json["product"]["productDetails"]["item"] and _json["product"]["productDetails"]["item"]['children']\
                            and tcin in _json["product"]["productDetails"]["item"]['children']:
                            value.append('https:'+_json["product"]["productDetails"]["item"]['children'][tcin]['images']['imageUrl']) #main image url
                            images = _json["product"]["productDetails"]["item"]['children'][tcin]['images']['altImageUrls']
                        elif 'product' in _json.keys() and 'productDetails' in _json['product'] and 'item' in _json["product"]["productDetails"]\
                                and 'images' in _json["product"]["productDetails"]["item"]:
                            value.append('https:'+_json["product"]["productDetails"]["item"]["images"]["imageUrl"])
                            images = _json["product"]["productDetails"]["item"]["images"]["altImageUrls"]
                        else:
                            images = _json["product"]["productDetails"]["item"]["images"]["altImageUrls"]

                        for image in images:
                            if 'https' not in image:
                                image = 'https:' + image
                            value.append(image)
            else:
                sku = None
                prod_data = self.get_product_data(soup)
                if prod_data and 'sku' in prod_data:
                    sku = prod_data['sku']
                _json = self.get_preload_queries(soup)
                
                if _json and 'queries' in _json:
                    source = str(_json)
                    for queryItems in _json['queries']:
                        for query in queryItems:
                            if 'product' in query and 'item' in query['product']:
                                if 'children' in query['product'] and sku:
                                    children = query['product']['children']
                                    for child in children:
                                        if 'tcin' in child and child['tcin'] == sku:
                                            productItem = child['item']
                                            if 'enrichment' in productItem and 'images' in productItem['enrichment']:
                                                images = productItem['enrichment']['images']
                                                if 'primary_image_url' in images:
                                                    value.append(images['primary_image_url'])
                                                if 'alternate_image_urls' in images: 
                                                    for img in images['alternate_image_urls']:
                                                        value.append(img)

                                else:
                                    productItem = query['product']['item']
                                    if 'enrichment' in productItem and 'images' in productItem['enrichment']:
                                        images = productItem['enrichment']['images']
                                        if 'primary_image_url' in images:
                                            value.append(images['primary_image_url'])
                                        if 'alternate_image_urls' in images: 
                                            for img in images['alternate_image_urls']:
                                                value.append(img)




        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find("script", text = re.compile(r"window\.__PRELOADED_STATE__"))
            if tag:
                _rgx = re.search("({.*})", tag.get_text().strip())

                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)

                if _rgx: 
                    _json = json.loads(json_data)
                    if _json:
                        source = str(tag)
                        videos = _json["product"]["productDetails"]["item"]["videos"]
                        if videos:
                            try:
                                for video in videos:
                                    if 'https' not in video:
                                        video = 'https:' + video
                                    value.append(video)
                            except:
                                for video in videos:
                                    for video_source in video['videoSource']:
                                        if 'https' not in video:
                                            video = 'https:' + video_source['path']
                                            if 'quality' in video_source:
                                                quality = video_source['quality']
                                                if quality == 'high':
                                                    value.append(video_source['path'])
                        else:
                            p_url = soup.select_one('link[rel="canonical"]')
                            if p_url and p_url.has_attr('href'):
                                page_url = p_url['href']
                            if 'product' in _json.keys() and 'selectedTcin' in _json['product']:
                                page_id = _json['product']['selectedTcin']
                            payload = {
                                'u':'5920AE8C-167C-4BF6-A4B7-E32D937A71F5',
                                'siteid':'72d23878-8496-46b3-a3b1-9fe02eda2e53',
                                'pageid':page_id,
                                's':int(time.time()),
                                'v':'v1.0.260',
                                'visitid':'D948B159-0986-42E0-903F-65F0D3421868',
                                'ref':'',
                                'r':'0.31385282402517434',
                                'pageurl':page_url,
                            }
                            api_url = f'https://content.syndigo.com/page/72d23878-8496-46b3-a3b1-9fe02eda2e53/{page_id}.json?{urllib.parse.urlencode(payload)}'
                            response = self.downloader.extra_download(api_url)
                            if response:
                                result_json = json.loads(response)
                                _json = json.loads(result_json['result_text'])
                                if _json:
                                    source = str(api_url)
                                    for k in _json['experiences']:
                                        data = _json['experiences'][k]['experiences']['power-page']['widgets']
                                        for _k in data.keys():
                                            _data = data[_k]
                                            if 'widgetType' in _data.keys() and _data['widgetType'] == 'VideoGallery':
                                                video_data = _data['items']
                                                for _video in video_data:
                                                    if 'video' in _video:
                                                        video_source = _video['video']['sources']
                                                        for _vid in video_source:
                                                            if 'url' in _vid:
                                                                value.append(_vid['url'])
            else:
                _json = self.get_preload_queries(soup)
                if _json and 'queries' in _json:
                    for queryItems in _json['queries']:
                        for query in queryItems:
                            if 'product' in query and 'item' in query['product']:
                                productItem = query['product']['item']
                                if 'enrichment' in productItem and 'videos' in productItem['enrichment']:
                                    videos = productItem['enrichment']['videos']
                                    for video in videos:
                                        for files in video['video_files']:
                                            if 'video_url' in files:
                                                value.append(files['video_url'])


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('"@type":"Product"'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                source = str(tag)
                content = _json['@graph'][0]["offers"]["availableDeliveryMethod"]
                if content:
                    value = content

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_preload_queries(self, soup):
        tag = soup.find("script", text = re.compile(r"__PRELOADED_QUERIES__"))
        if tag:
            _rgx = re.search(r"__TGT_DATA__ = JSON.parse(.*?)\)\;", tag.get_text().strip())
            if _rgx:
                txt = _rgx.group(0).replace('\\', '')
                cleansed = txt.strip('__TGT_DATA__ = JSON.parse("{').strip('"__PRELOADED_QUERIES__":').strip('}");')
                _json = json.loads(cleansed)
                return _json
        return None

    def get_product_data(self, soup):
        tag = soup.select_one('script[type="application/ld+json"]#json')
        try:
            if tag:
                _json = json.loads(tag.get_text())
                if _json:
                    if '@graph' in _json:
                        for item in _json['@graph']:
                            if item['@type'].lower() == 'product':
                                return item
        except:
            pass
        return None

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()

        for spec in tags:
            try:
                spec = spec.replace('<B>', '').replace('</B>', '')
                spec_split = spec.split(': ')
                if len(spec_split) == 2 :
                    key = spec_split[0]
                    data = spec_split[1]
                                
                result[key] = data

            except:
                continue

        return result
