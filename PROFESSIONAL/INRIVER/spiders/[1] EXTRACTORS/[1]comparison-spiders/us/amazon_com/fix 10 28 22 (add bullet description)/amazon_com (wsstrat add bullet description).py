from audioop import add
import json
import logging
import re
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template, get_result_buybox_template


class AmazonComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.amazon.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        buybox_template = get_result_buybox_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)  
        
        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)        

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)  

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)               

        # Extract Buybox
        result["buybox"] = self._get_buybox(soup, buybox_template)

        return result            

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = self.check_tag(
                #soup.select_one('span.a-offscreen'),
                soup.select_one('.apexPriceToPay span.a-offscreen'),
                soup.select_one('[id="tp_price_block_total_price_ww"] span'),
                soup.select_one('#priceblock_dealprice'),
                soup.select_one('#priceblock_ourprice'),
                soup.select_one("span#priceblock_usedprice"),
                soup.select_one('#price_inside_buybox'),
                soup.select_one('#usedOnlyBuybox #usedBuySection .offer-price'),
                soup.select_one('#corePrice_feature_div .a-offscreen'), #('.a-price .a-offscreen'),
                soup.select_one('#mbc-price-1'),
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one("span.a-price"),
                soup.select_one('[class="_p13n-desktop-sims-fbt_price_p13n-sc-price__bCZQt"]'),
                attrs = ['data-asin-price']
            )

            if tag:
                source, value = tag
            else:
                tag = soup.select_one('input#base-product-price') or soup.find('input', {'id': "base-product-price"})
                if tag and tag.has_attr('data-base-product-price'):
                    value = str(tag['data-base-product-price'])
                    source = str(tag)

            # AS PER VALIDATOR, Daily run issues product#3274 - 2022-02-09
            # added temporary checker for specific availability
            # temp_checker_not_available = soup.select_one('#availability')
            # if temp_checker_not_available:
            #     checker_value = self.__get_sanitized_tag(temp_checker_not_available)
            #     hidden_price_texts = [
            #         "Currently unavailable. We don't know when or if this item will be back in stock.",
            #         "Temporarily out of stock. We are working hard to be back in stock. Place your order and we’ll email you when we have an estimated delivery date. You won’t be charged until the item ships."

            #     ] 
            #     if checker_value in hidden_price_texts:
            #         value = Defaults.GENERIC_NOT_FOUND.value

            # AS PER VALIDATOR, IF ITEM IS USED, PRICE SHOULD BE -1
            use_checker_tag = soup.select_one('.a-column.a-span12.a-text-left > span')
            if use_checker_tag:
                if use_checker_tag.get_text().strip() == 'Buy used:' or use_checker_tag.get_text().strip() == 'Usado:':
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value

            #added condition to set price value like this'$59.99$59.99' or '$159.99$159.99' or '$1,249.99$1,249.99' to NOT FOUND
            if len(value) == 10:
                if value[5:] == value[:5]:
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value
            if len(value) == 12:
                if value[6:] == value[:6]:
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value
            if len(value) == 14:
                if value[7:] == value[:7]:
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value
            if len(value) == 18:
                if value[9:] == value[:9]:
                    source = Defaults.GENERIC_NOT_FOUND.value
                    value = Defaults.GENERIC_NOT_FOUND.value
            #end condition

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('#attach-base-product-currency-symbol'),
                attrs=['data-asin-currency-code', 'value']
                )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # sometimes add-to-cart button appears but still unavailable"
            oos_div_tag = soup.select_one('div#outOfStock span')
            availability_tag = soup.select_one('div#availability span')
            availability_txt = None
            if availability_tag:
                availability_txt = availability_tag.get_text().strip()
            if oos_div_tag:
                value = oos_div_tag.get_text().strip()
                source = str(oos_div_tag)
            # purposely checked as sometimes its empty. 

            elif availability_txt: 
                source = str(availability_tag)
                value = availability_txt
            elif availability_txt and availability_txt.lower() == 'temporarily out of stock.':
                value = availability_txt
                source = str(availability_tag)
            else:
                add_to_cart = soup.select_one('.a-box-group .a-box-inner #add-to-cart-button') or soup.select_one('[id="add-to-cart-button"]')
                if add_to_cart:
                    value = add_to_cart['value']
                    source = str(add_to_cart)
            # old code
            if value == 'NOT_FOUND':
                tag = soup.select_one('#availability')
                if tag:
                    source = str(tag)
                    value = self.__get_sanitized_tag(tag)

                    if value == '':
                        value = 'Currently unavailable.'
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = self.check_tag(
                soup.select_one('[data-hook="rating-out-of-text"]'),
                soup.select_one('#acrPopover'),
                attrs = ['title']
            )
            tag_review = self.check_tag(
                soup.select_one('#acrCustomerReviewText'),
                soup.select_one('[id="acrCustomerReviewText"]')
                )

            if tag_score and tag_review:
                reviews_source, reviews_value  = tag_review
                score_source, score_value = tag_score
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#productTitle')
            if tag:
                source, value = str(tag), tag.get_text().strip()    
            if value == Defaults.GENERIC_NOT_FOUND.value or not tag:
                tag = soup.select_one('#title')
                if tag:
                    source, value = str(tag), tag.get_text().strip() 


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = soup.findAll('th', text=re.compile(r'Brand')) # Brand in Specs
            if tags:
                for tag in tags:
                    if tag.get_text().strip() == 'Brand':
                        parent_tag = tag.find_parent('tr')
                        if parent_tag:
                            brand_tag = parent_tag.select_one('td')
                            if brand_tag:
                                source = str(parent_tag)
                                value = brand_tag.get_text().strip()
                                break
            # elif soup.find('th', text=re.compile(r'Manufacturer')): # Brand in Specs/ Product Information
            #     tags = soup.findAll('th', text=re.compile(r'Manufacturer'))
            #     for tag in tags:
            #         if 'Manufacturer' == tag.get_text().strip():
            #             parent_tag = tag.find_parent('tr')
            #             if parent_tag:
            #                 brand_tag = parent_tag.select_one('td')
            #                 if brand_tag:
            #                     source = str(parent_tag)
            #                     value = brand_tag.get_text().strip()
            elif soup.find('span', text=re.compile(r'.*Brand.*')): # Brand in top Desc
                tag = soup.find('span', text=re.compile(r'.*Brand.*'))
                if tag:
                    parent_tag = tag.find_parent('tr')
                    if parent_tag:
                        brand_tag = parent_tag.select('td')
                        if len(brand_tag) == 2:
                            source = str(parent_tag)
                            value = brand_tag[-1].get_text().strip()
            else:
                tag = self.check_tag(
                    soup.select_one('#mbc'),
                    soup.select_one('#bylineInfo'),
                    attrs=['data-brand']
                )

                if tag:
                    source, value = tag
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            _src = []
            description = []
            bullet_elements, span_tag = None, None

            # Product Description Top
            bullet_elements = soup.select('div#feature-bullets ul li')
            if len(bullet_elements):
                _src.append(str(bullet_elements))
                for li in bullet_elements:
                    if not li.has_attr('id'):
                        description.append(' '.join(li.stripped_strings))

            # if bullet_elements
            bullet_elements = soup.select_one('[data-csa-c-content-id="productFactsDesktop"]')
            span_tag = bullet_elements.select('span') if bullet_elements else None
            if (span_tag and not description):
                for x in span_tag:
                    if ('class' in x.attrs and 'a-color-secondary' in x.attrs.get('class')):
                        description.append(' '.join(x.stripped_strings))
    
            # From Manufacturer
            # tag = soup.find('div',{'class':'apm-centerthirdcol apm-wrap'})
            # if tag:
            #     _src.append(str(tag))
            #     description = description + ' ' + ' '.join(tag.stripped_strings)


            # there are 2 divs with same id #aplus
            # https://prnt.sc/OFRo28yI5-fS

            # there are more than 1 #aplus
            # aplus feature may co-exist with other aplus features.
            
            # aplus 1
            tag = soup.select_one('#aplusBrandStory_feature_div #aplus') or soup.select_one('#dpx-aplus-product-description_feature_div') or soup.select_one('#aplus_feature_div') #soup.select_one('#aplus')
            if tag:
                _src.append(str(tag))
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()
                description.append(' '.join(tag.stripped_strings))

            # aplus 2
            tag = soup.select_one('#aplus-2_feature_div #aplus')
            if tag:
                _src.append(str(tag))
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()
                description.append(' '.join(tag.stripped_strings))

            # aplus 3
            tag = soup.select_one('#aplus3p-2_feature_div #aplus')
            if tag:
                _src.append(str(tag))
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()
                description.append(' '.join(tag.stripped_strings))

            # Product Description Bottom
            tag = soup.find('div',{'id':'productDescription'})
            if tag:
                _src.append(str(tag))
                description.append(' '.join(tag.stripped_strings))
            else:
                tags = soup.select('td.apm-top')
                if len(tags):
                    _src.append(str(tags))
                    for tag in tags:
                        description.append(' '.join(tag.stripped_strings))
                        
            # For H10
            legal_feature_tag = soup.select_one('#legal_feature_div')
            
            if legal_feature_tag:
                _src.append(str(legal_feature_tag))
                description.append(' '.join(legal_feature_tag.stripped_strings))
                print(' '.join(legal_feature_tag.stripped_strings))
            # DO NOT UNCOMMENT DUPLICATE DESCRIPTION FROM MANUFACTURER
            # tags = soup.select('div.apm-sidemodule-textright')
            # if tags:
            #     _src.append(str(tags))
            #     for tag in tags:
            #         description.append(' '.join(tag.stripped_strings))

            # tag = soup.find('div',{'class':'apm-sidemodule-textleft'})
            # if tag:
            #     _src.append(str(tag))
            #     description.append(' '.join(tag.stripped_strings))

            if description:
                source = " + ".join(_src)
                value = " + ".join(description)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.select_one('.a-expander-content.a-expander-section-content.a-section-expander-inner')
            if tag:
                specification_tag = tag.find('table',{'class':'a-keyvalue prodDetTable'})
                if specification_tag:
                    source = str(specification_tag)
                    specifications = specification_tag.select('tr')
                    value = {}
                    for spec in specifications:
                        key_value = spec.select_one('th')
                        v_value = spec.select_one('td')
                        if key_value and v_value:
                            value[key_value.get_text().strip()] = v_value.get_text().strip()
            else:
                tag = soup.findAll('div', {'class':'a-section a-spacing-small a-spacing-top-small'})
                if tag:
                    specification_tag = tag[-1].find('table', {'class': 'a-normal a-spacing-micro'})
                    if specification_tag:
                        source = str(specification_tag)
                        specifications = specification_tag.select('tr')
                        value = {}
                        for spec in specifications:
                            values = spec.select('td')
                            if values:
                                if len(values) == 2:
                                    value[values[0].get_text().strip()] = values[1].get_text().strip()
                                    
            table_tag = soup.select_one('#product-specification-table')
            if table_tag:
                spec_key = table_tag.select('tr > th')
                spec_value = table_tag.select('tr > td')
                if spec_key and spec_value:
                    for x in range(len(spec_key)):
                        value.update({spec_key[x].get_text() : spec_value[x].get_text().strip()})
                    source = str(table_tag)
            else:
                tag = soup.select('.a-keyvalue.prodDetTable tr')
                if tag:
                    source = str(tag)
                    for spec in tag:
                        key = spec.select_one('th')
                        val = spec.select_one('td')
                        if key and val:
                            value[key.get_text(strip=True)] = val.get_text(strip=True)
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value         

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            # tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            # if tag:
            #     text = tag.get_text().strip().split('jQuery.parseJSON')[1]
            #     rgx = re.search('(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
            #     source = str(tag)
                
            #     if rgx:
            #         _json = json.loads(rgx.group(1).replace('l\\', ''))
            #         img_list = _json['landingAsinColor']
            #         if img_list in _json['colorImages']:
            #             value =[img['hiRes'] for img in _json['colorImages'][img_list] if 'hiRes' in img]
            #         else:
            #             tag = soup.find('script', text=re.compile('.*colorImages*'))
            #             if tag:
            #                 _json = tag.get_text().strip()
            #                 _json = _json.split("'colorImages':")[1]\
            #                         .split("'colorToAsin'")[0].strip()[:-1]\
            #                         .replace("'",'"')
            #                 _json = json.loads(_json)
            #                 source = str(tag)
            #                 if 'initial' in _json:
            #                     value = [i['hiRes'] for i in _json['initial']]

            # uncommented this temporarily since this code fetches the correct image count for most items https://prnt.sc/02RgHzr3ofCv
            tag = soup.find("script", text = re.compile(r"register\(\"ImageBlockATF\""), attrs = {"type":"text/javascript"})
            if tag:
                img_js_text = " ".join(tag.get_text().strip().replace("'", "\"").split())
                if img_js_text:
                    img_js_data = re.search(r"var data = (\{.*\});", img_js_text)
                    if img_js_data:
                        source = str(tag)
                        try:
                            rgx = re.search(r'"colorImages":(\{\"initial\":\[\{.*\}\]\}),',img_js_data.group(1).replace(' ',''))
                            if rgx:
                                _json = json.loads(rgx.group(1))
                                if _json:
                                    for x in _json['initial']:
                                        if 'hiRes' in x and x['hiRes']:
                                            value.append(x['hiRes'])
                                        elif 'large' in x and x['large']:
                                            value.append(x['large'])

                        except:
                            value = []

                        if not value:
                            value = re.findall(r'\"hiRes\".*?\"(.*?\.jpg)\",', img_js_data.group(1))     

            if not value:
                tag = soup.select('.a-button-thumbnail')           
                if len(tag) > 0:
                    for tags in tag:
                        if tags('img'):
                            images = tags('img')
                            img = images[0]
                            if '.gif' not in img['src']:
                                if 'thumb' not in img['src']:
                                    if 'overlay' not in img['src']:
                                        img_ = img['src'].split('._AC')
                                        value.append(img_[0]+'._AC_UL1500_.jpg')
                else:
                    tag = soup.findAll('img')
                    if len(tag) > 0:
                        for img in tag:
                            if '_AC_SY1000_' in img['src']:
                                img_ = img['src'].split('._AC')
                                if img_[0]+'._AC_UL1500_.jpg' not in value:
                                    source = str(img)
                                    value.append(img_[0]+'._AC_UL1500_.jpg')
                
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile('jQuery.parseJSON'))
            if tag:
                text = tag.get_text().strip().split('jQuery.parseJSON')[1]
                rgx = re.search(r'(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
                source = str(tag)
                
                if rgx:
                    txt = rgx.group(1).replace('null', '"False"').replace('l\\', '').replace('true', '"True"').replace('false','"False"').replace('\\','').replace('1-1/2"',"1-1/2''")
                    txt_to_replace = re.search('GPM (.+?) Cover Plate', txt)
                    if txt_to_replace:
                        txt_to_replace = 'GPM ' + txt_to_replace.group(1)
                        if txt_to_replace in txt:
                            txt = txt.replace(txt_to_replace, txt_to_replace[:-1])

                    
                    
                    _json = json.loads(txt)    
                    if 'videos' in _json:
                        value = [i['url'] for i in _json['videos']]
            
            #tags = soup.select('.vse-carousel-container a.vse-carousel-item')
            #if tags:
            #    for item in tags:
            #        if item.has_attr('href'):
            #            value.append('https://www.amazon.com'+item['href'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value  

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one("#delivery-message"),
                soup.select_one("#ddmDeliveryMessage")
                )
            if tag:
                source, value = tag
            if not tag:
                tag = soup.select_one('#availability')
                if tag:
                    source, value = str(tag), self.__get_sanitized_tag(tag) #tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
             soup.select_one("#olp-upd-new-freeshipping .a-color-base"),
             soup.select_one('#shippingMessageInsideBuyBox_feature_div'),
             soup.select_one(".a-size-base.a-color-secondary")
             )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def _get_buybox(self, soup, template):
        buybox_result = deepcopy(template)

        try:
            buybox_tag = soup.select_one("#buybox") or soup.select_one(".a-box-group")

            if not buybox_tag:
                temp_tag = soup.select_one(".a-box")
                if 'a-alert' not in temp_tag.get('class'):
                    buybox_tag = temp_tag
            
            if buybox_tag:
                buybox_result['source'] = str(buybox_tag)

                # Extract text
                buybox_result['text'] = self.__get_sanitized_tag(buybox_tag)
                # Extract price and currency
                buybox_result["price"]["value"], buybox_result["price"]["currency"] = self.__get_buybox_price_and_currency(buybox_tag)
                # Extract delivery
                buybox_result["delivery"] = self.__get_buybox_delivery(buybox_tag)
                # Extract shipping
                buybox_result["shipping"] = self.__get_buybox_shipping(buybox_tag)
                # Extract availability
                buybox_result["availability"] = self.__get_buybox_availability(soup, buybox_tag)
                # Extract retailer
                buybox_result["retailer"] = self.__get_buybox_retailer(soup, buybox_tag)
                # Extract used_price
                buybox_result["used_price"] = self.__get_buybox_used_price(soup, buybox_tag)
                # Extract new_price
                buybox_result["new_price"] = self.__get_buybox_new_price(soup, buybox_tag)
                # Extract cartable
                buybox_result["cartable"]['source'], buybox_result["cartable"]['value'] = self.__get_buybox_cartable(soup, buybox_tag)
                # Extract buyable
                buybox_result["buyable"]['source'], buybox_result["buyable"]['value'] = self.__get_buybox_buyable(soup, buybox_tag)
                # Extract add_on
                buybox_result["add_on"] = self.__get_buybox_add_on(buybox_tag)            
                
        except Exception as e:
            self.logger.exception(e)
            buybox_result['text'] = FailureMessages.BUYBOX_EXTRACTION.value

        return buybox_result

    def __get_buybox_price_and_currency(self, buybox_tag):
        price = Defaults.GENERIC_NOT_FOUND.value
        currency = 'USD' # Defaulted

        try:
            tag = buybox_tag.select_one("#price_inside_buybox") or\
                        buybox_tag.select_one("#newBuyBoxPrice") or\
                        buybox_tag.select_one("#unqualifiedBuyBox .a-color-price")

            if not tag:
                tag = buybox_tag.select_one(".a-color-price")
                if tag:
                    if "a-size-medium" not in tag.get("class") or tag.parent.get("id") in ["availability", "outOfStock"]:
                        tag = None

            if tag:
                price = tag.get_text().strip()
                currency = 'USD' # Defaulted
            

            if not bool(re.search('\d', price)):
                tag = buybox_tag.select_one('.a-price.a-text-price.a-size-medium span') or \
                    buybox_tag.select_one('.a-offscreen')

                if tag:
                    price = tag.get_text().strip()
                                
        except Exception as e:
            self.logger.exception(e)
            price = FailureMessages.BUYBOX_EXTRACTION.value
            currency = FailureMessages.BUYBOX_EXTRACTION.value

        return price, currency
    
    def __get_buybox_delivery(self, buybox_tag):
        delivery = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#ddmDeliveryMessage")
            if tag:
                delivery = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            delivery = FailureMessages.BUYBOX_EXTRACTION.value

        return delivery

    def __get_buybox_shipping(self, buybox_tag):
        shipping = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#price-shipping-message")
            if tag:
                shipping= tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            shipping = FailureMessages.BUYBOX_EXTRACTION.value

        return shipping
    
    def __get_buybox_availability(self, soup, buybox_tag):
        availability = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#availability") or buybox_tag.select_one("#outOfStock .a-text-bold") or soup.select_one("#availability")
                
            if tag:
                availability = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            availability = FailureMessages.BUYBOX_EXTRACTION.value

        return availability
    
    def __get_buybox_retailer(self, soup, buybox_tag):
        retailer = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#merchant-info")
            new_tag = buybox_tag.select_one('#newAccordionRow')
            if new_tag:
                tag = new_tag.select_one('#sellerProfileTriggerId')
                if tag:
                    retailer = tag.get_text().strip()
                else:
                    tags = new_tag.select('.tabular-buybox-text')
                    if tags:
                        # Expecting 2 only https://prnt.sc/10dc10n
                        if len(tags) == 2:
                            retailer = tags[-1].get_text().strip()
                        elif len(tags) > 2:
                            retailer = tags[1].get_text().strip()
                    else:
                        retailer = "Amazon"
            
            elif tag:
                if tag.select_one('#sellerProfileTriggerId'):
                    retailer = tag.select_one('#sellerProfileTriggerId').get_text().strip()
                else:
                    retailer = re.sub(r'[^\w ]+', '', tag.get_text()
                                                .split("by ")[-1].split(".")[0]).strip()
            elif buybox_tag.select_one('#sellerProfileTriggerId'):
                retailer = buybox_tag.select_one('#sellerProfileTriggerId').get_text().strip()
            elif soup.select('.tabular-buybox-text'):
                tags = soup.select('.tabular-buybox-text')
                if tags:
                    # Expecting 2 only https://prnt.sc/10dc10n
                    if len(tags) == 2:
                        retailer = tags[-1].get_text().strip()
                    elif len(tags) > 2:
                        retailer = tags[1].get_text().strip()
                else:
                    retailer = "Amazon"
            else:
                # For Zound Only. Defaulted to Amazon if No Buybox div
                retailer = "Amazon" # Defaulted in Ultima 
            
        except Exception as e:
            self.logger.exception(e)
            retailer = FailureMessages.BUYBOX_EXTRACTION.value

        return retailer

    def __get_buybox_used_price(self, soup, buybox_tag):
        used_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #usedPrice")
            if tag:
                used_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            used_price = FailureMessages.BUYBOX_EXTRACTION.value

        return used_price
    
    def __get_buybox_new_price(self, soup, buybox_tag):
        new_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #newBuyBoxPrice")
            if tag:
                new_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            new_price = FailureMessages.BUYBOX_EXTRACTION.value

        return new_price

    def __get_buybox_cartable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        cartable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box .a-icon-cart")
            if tag:
                source = str(tag)
                cartable = str(True)
            else:
                cartable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            cartable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, cartable

    def __get_buybox_buyable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        buyable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-icon-buynow")
            if tag:
                source = str(tag)
                buyable = str(True)
            else:
                buyable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            buyable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, buyable

    def __get_buybox_add_on(self, buybox_tag):
        add_on = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = buybox_tag.select(".abbListItem")
            if tags:
                add_on = ", ".join([self.__get_sanitized_tag(tag) for tag in tags])
            
        except Exception as e:
            self.logger.exception(e)
            add_on = FailureMessages.BUYBOX_EXTRACTION.value

        return add_on        

    @staticmethod
    def check_tag(*tags, attrs=[]):
        for tag in tags:
            if not isinstance(tag, list):
                if tag and attrs:
                    for attr in attrs:
                        if tag.has_attr(attr):
                            if len(tag.get(attr)) > 0:
                                return str(tag), tag.get(attr)
                        else:
                            if len(tag.get_text().strip()) > 0:
                                return str(tag), tag.get_text().strip()
                elif tag:
                    return str(tag), tag.get_text().strip()
            else:
                if tag:
                    return tag
        return None

   
    def classroom_cleaner(self, tag):
        text = str(tag)
        if '<style type="text/css">' in text or '<script type="text/javascript">' in text:
            soup = BeautifulSoup(text, "html.parser") # create a new bs4 object from the html data loaded
            for script in soup(["script", "style"]): # remove all javascript and stylesheet code
                script.extract()
            # get text
            text = soup.get_text()
            # break into lines and remove leading and trailing space on each
            lines = (line.strip() for line in text.splitlines())
            # break multi-headlines into a line each
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            # drop blank lines
            text = '\n'.join(chunk for chunk in chunks if chunk)
            return text
        else:
            return tag 
    
 
    def _build_spec(self, key_tag, val_tag):
        result = {}
        for k,v in zip(key_tag, val_tag):
            v = self.classroom_cleaner(v)
            if not isinstance(v, str):
                v = v.get_text().strip()

            result["%s" % (k.get_text().strip())] =  v.replace("\n", ' ')

        return str(key_tag + val_tag), result
   
    @staticmethod
    def __get_sanitized_tag(tag):
        tags_to_extract = ['script', 'style']

        for i in tags_to_extract:
            for j in tag.select(i):
                j.extract()

        return ' '.join(tag.stripped_strings)