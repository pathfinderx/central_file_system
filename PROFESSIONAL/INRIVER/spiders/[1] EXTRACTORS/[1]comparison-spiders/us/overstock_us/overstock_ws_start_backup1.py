import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class OverstockComUsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.overstock.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.find('span', {'data-cy': 'product-price'})

            if tag:
                source, value = str(tag), tag.text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and len(_json) > 0 and 'offers' in _json[0] and 'priceCurrency' in _json[0].get('offers').keys():
                    source, value= str(_json), _json[0].get('offers').get('priceCurrency')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and (len(_json) > 0) and ('offers' in _json[0]) and ('availability' in _json[0].get('offers').keys()):
                    source, value= str(_json), _json[0].get('offers').get('availability')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value
            
        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('adobe_page_type'))
            if tag:
                _json = json.loads(tag.text[tag.text.find("adobe_page_type")-2:])
                if _json and ('product_num_reviews' in _json) and ('product_rating' in _json):
                    reviews_source, reviews_value = str(_json), str(_json.get('product_num_reviews'))
                    if len(_json.get('product_rating')) > 0:
                        score_source, score_value = str(_json), str(_json.get('product_rating')[0])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and (len(_json) > 0) and ('name' in _json[0]):
                    source, value= str(_json), _json[0].get('name')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and (len(_json) > 0) and ('description' in _json[0]):
                    source, value= str(_json), _json[0].get('description')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and (len(_json) > 0) and ('description' in _json[0]):
                    source, value= str(_json), _json[0].get('description')
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
           tag = soup.find('script', text = re.compile('clientLoading'))
           if tag:
                specs_start, specs_end = tag.text.find('"specs":[{"name"'), tag.text.find('queriedSpecs')
                _json = json.loads('{' + tag.text[specs_start:specs_end-2] + '}')

                if _json and ('specs' in _json) and (len(_json.get('specs')) > 0):
                    source = str(_json)
                    for index in _json.get('specs'):
                        value.update({index['name']:index['value']})
                        print(value)
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('script', text = re.compile('description'))
            if tag:
                _json = json.loads(tag.text)
                if _json and (len(_json) > 0) and ('image' in _json[0]):
                    if len(_json[0]['image'] > 0):
                        for img_src in _json[0]['image']:
                            value.append(_json[0]['image'][img_src])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        # website doesn't support videos
        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find_all('span', class_="nS7RQMH _3cE5lAD")
            if tag and len(tag) > 0:
                source, value = str(tag), tag[1].text

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
    
        try:
            tag = soup.find('p', class_="_1EWumHE _3cE5lAD")
            if tag:
                source = str(tag), tag.text
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value
