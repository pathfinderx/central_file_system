import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BuildComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.build.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        data = dict()
        json_tags = soup.find_all('script', type='application/ld+json')
        if json_tags:
            product_tag = next((i for i in json_tags if i.string and '"@type":"Product"' in i.string), None)
            if product_tag:
                data = {'source': str(product_tag), 'value': json.loads(product_tag.string)}


        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(data)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(data)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(data)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(data)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = soup.select_one('[property="product:price:amount"]')
            if tag and tag.has_attr('content'):
                source, value = str(tag), str(tag.get('content'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[property="product:price:currency"]')
            if tag and tag.has_attr('content'):
                source, value = str(tag), tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if data:
                source, value = data['source'], data['value']['offers']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, data):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if data and 'aggregateRating' in data['value']:
                score_source, score_value = data['source'], str(data['value']['aggregateRating']['ratingValue'])
                reviews_source, reviews_value = data['source'], str(data['value']['aggregateRating']['reviewCount'])
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.RATING_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if data and 'description' in data['value']:
                source, value = data['source'], data['value']['description']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if data and 'brand' in data['value']:
                source, value = data['source'], data['value']['brand']['name']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.lh-copy._5n43l') or soup.select_one('.lh-copy.fxOgM')
            if tag and tag.get_text().strip():
                source, value = str(tag), ' '.join(tag.stripped_strings)
                
            else:
                script_tag, rgx, _json = None, None, None
                product_url_tag, tags = soup.select_one('meta[property="og:url"]'), soup.select('script')
                script_tag = [x for x in tags if re.search(r'__APOLLO_STATE__', x.get_text())] if tags else None
                rgx = re.search(r'window\.__APOLLO_STATE__=([\s\S]+),\"Image:', script_tag[0].get_text()).group(1).replace('\\"','')+'}' if script_tag else None
                
                try:
                    if (rgx and 
                        product_url_tag and
                        product_url_tag.has_attr('content')):

                        product_id = soup.select_one('meta[property="og:url"]').get('content').split('/s')[-1]
                        _json = json.loads(rgx)

                        if (_json and 
                            f'ProductFamily:{product_id}' in _json and 
                            'description' in _json[f'ProductFamily:{product_id}']):

                            description_soup = BeautifulSoup(_json[f'ProductFamily:{product_id}']['description'] ,'lxml')

                            if description_soup:
                                source, value = str(script_tag[0]), ' '.join(description_soup.stripped_strings)
                    
                except Exception as e:
                    print('Error at: wsstrat -> get_description() -> with message: ', e)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tags = soup.select('div.w-100.w-third-ns table tr')
            if tags:
                source = str(tags)
                for tag in tags:
                    contents = tag.find_all('td')
                    key = contents[0].get_text().strip()
                    val = contents[1].get_text().strip()
                    value[key] = val
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value
        
        # No product specs for this site
        return source, value        

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            # tags = soup.select('div.slick-slide img')

            # if len(tags):
            #     source = str(tags)
            #     value = [tag.get('src') for tag in tags]
            
            tag = soup.find('script', text=re.compile(r'window.__APOLLO_STATE__'))
            if tag:
                _rgx = re.search(r'({.*})', tag.get_text())
                if _rgx:
                    _json = json.loads(_rgx.group(1))
                    image_json = [_json[item] for item in _json.keys() if 'Image:product/' in item]
                    var_rgx = re.search(r'uid\=(.*)\&', self.downloader.product_url) 
                    product_color = None
                    if not var_rgx:
                        var_rgx = re.search(r'uid\=(.*)', self.downloader.product_url)
                    if var_rgx:
                        product_variant = var_rgx.group(1)
                        product_color = [_json[item] for item in _json.keys() if product_variant in item][0]['title']
                        source = str(image_json)
                        for item in image_json:
                            if 'Alternate View' in item['description'] or product_color == item['description'] or \
                                'Alternate' in item['description'] or 'Infographic' in item['description'] or 'Lifestyle View' in item['description']:
                                base_url = 'https://s3.img-b.com/image/private/t_base,c_pad,f_auto,dpr_2,w_680,h_680/'
                                value.append(base_url + item['id'])
                    else:
                        if image_json:
                            source = str(image_json)
                            for img in image_json:
                                if img['__typename'] == 'Image':
                                    base_url = 'https://s3.img-b.com/image/private/t_base,c_pad,f_auto,dpr_2,w_680,h_680/'
                                    value.append(base_url + img['id'])
                    if not value:
                        if _json:
                            source = str(_json)
                            for items in _json.keys():
                                if product_color and product_color in items or 'Alternate Image' in items or 'Chrome' in items:
                                    _rgx = re.search(r'"id":"(.*)","description', items)
                                    if _rgx:
                                        value.append('https://s3.img-b.com/image/private/t_base,c_pad,f_auto,dpr_2,w_450,h_450/' + _rgx.group(1))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            # tags = soup.find_all('script', type=None)
            # tag = next((tag for tag in tags if tag.string and '__APOLLO_STATE__' in tag.string), None)
            tag = soup.find('script', text=re.compile(r'__APOLLO_STATE__'))

            if tag:
                hash_keys = re.findall(r'(?:\"hashKey\":\")([^"]+)', tag.string)
                if hash_keys:
                    video_sources = list()
                    base_api_url = f'https://fast.wistia.com/embed/medias/%s.jsonp'
                    for key in hash_keys:
                        api_url = base_api_url % key
                        video_sources.append(api_url)
                        res = self.downloader.download_video(api_url)
                        data_str = re.search(r'(?!=\s*)(\{[^;]+)', res) if res else None
                        if data_str:
                            video_data = json.loads(data_str.group())
                            if 'media' in video_data:
                                value.append(video_data['media']['assets'][0]['url'])

                    source = str(video_sources)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.mt1.dib')
            if tag and tag.get_text().strip():
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.theme-accent.lh-copy.b.db')
            if tag and tag.get_text().strip():
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        # website doesn't support shipping
        return source, value
