import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template
from strategies.download.exceptions import DownloadFailureException


class GamestopComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.gamestop.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.__product_id = None

    def __set_product_id(self, soup):
        tag = soup.select_one('div.product-detail.product-wrapper')
        if tag:
            self.__product_id = tag['data-pid']
        else:
            raise Exception

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        data = json.loads(self.__parse_data_from_script(self.__get_script(soup)))

        self.__set_product_id(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, data)

        # # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(data)

        # # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup, data)

        # # Extract ratings score
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"] = self.get_rating_score(data)

        # # Extract ratings reviews
        result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(data)

        # # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(data)

        # # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(data)

        # # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(data)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(soup, data)

        result["shipping"]["source"], result["shipping"]["value"] = self.get_shipping(soup)
        
        result["video_urls"]["source"], result["video_urls"]["value"] = self.get_video_urls(soup)
        
        result["delivery"]["source"], result["delivery"]["value"] = self.get_delivery(soup)

        return result

    def __get_script(self, soup):
        # get only the first one which contains products
        script = soup.find('script', type='application/ld+json')
        return script

    def __parse_data_from_script(self, script):
        return script.text

    def get_price(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if data['offers']:
                    if 'name' in data['offers'][0]:
                        name = data['offers'][0]['name']
                        if name == 'New':
                            if 'offers' in data and 'price' in data['offers'][0]:
                                value = data['offers'][0]['price']
                        elif name == 'Pre-Owned':
                            if 'offers' in data and len(data['offers']) > 1 and 'price' in data['offers'][1]:
                                value = data['offers'][1]['price']
                else:
                    tag = soup.select_one('.primary-details-row .selling-price-redesign')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()
            # for promo:
            tag = soup.select_one('.primary-details-row .selling-price-redesign.has-savings span')
            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if data['offers']:
                    if 'priceCurrency' in data['offers'][0]:
                        value = data['offers'][0]['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if data['offers']:
                    if 'name' in data['offers'][0]:
                        name = data['offers'][0]['name']
                        if name == 'New':
                            if 'offers' in data and 'availability' in data['offers'][0]:
                                value = data['offers'][0]['availability']
                        elif name == 'Pre-Owned':
                            if 'offers' in data and len(data['offers']) > 1 and 'availability' in data['offers'][1]:
                                value = data['offers'][1]['availability']
                            else:
                                value = name
                else:
                    tag = soup.select_one('.add-to-cart')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"
        
        try:
            if data:
                source = str(data)
                if 'aggregateRating' in data and 'ratingValue' in data['aggregateRating']:
                    value = data['aggregateRating']['ratingValue']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_rating_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if 'aggregateRating' in data and 'reviewCount' in data['aggregateRating']:
                    value = data['aggregateRating']['reviewCount']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if 'name' in data:
                    value = data['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if 'brand' in data:
                    value = data['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if data:
                source = str(data)
                if 'description' in data:
                    value = data['description']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = dict()
        
        try:
            specification_container = soup.select_one('div.product-features')
            if specification_container:
                specification_rows = specification_container.select('table tbody tr')
                for row in specification_rows:
                    row_stripped = [item for item in row if row != '\n']
                    if len(row.contents) == 2:
                        key = row.contents[0].get_text(strip=True)
                        if key:
                            source = str(soup)
                            value[key] = row.contents[1].get_text(strip=True)


                return source, value

            specification_container = soup.select_one('div.full-description')
            if specification_container:
                specification_rows = specification_container.select('table tbody tr')
                for row in specification_rows:
                    row_stripped = [item for item in row if str(item) != "\n"]
                    if len(row_stripped) == 2:
                        key = row_stripped[0].get_text(strip=True)
                        if key:
                            source = str(soup)
                            value[key] = row_stripped[1].get_text(strip=True)
                return source, value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def get_image_urls(self, soup, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = list()
        
        try:
            # get images in thumbnails
            img_container = soup.select_one('div.product-carousel')
            if img_container:
                img_tags = img_container.select('div span img')

                if img_tags:
                    source = str(img_tags)
                    for img_tag in img_tags:
                        value.append(img_tag['src'])

            # get image from screenshots
            if data:
                if 'screenshot' in data:
                    
                    if not source == "NOT_FOUND":
                        source = source + ' + ' + str(data['screenshot'])
                    else:
                        source = str(data['screenshot'])

                    for url in data['screenshot']:
                        if url not in value:
                            value.append(url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            if soup:
                source = str(soup)
                tag = soup.select_one('div.shipping-promotions')
                if tag:
                    temp_tag = tag.select('div.callout')
                    if len(temp_tag):
                        temp_tag = temp_tag[-1]
                        shipping_tag = temp_tag.select_one('span')
                        value = shipping_tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            response_text = self.downloader.download('https://www.gamestop.com/on/demandware.store/Sites-gamestop-us-Site/default/Product-Variation?dwvar_{id}_condition=New&pid={id}&quantity=1'.format(id=self.__product_id))
            source = response_text
            data = json.loads(response_text)
            value = data['product']['availability']['additionalmessages'][0]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []

        
        try:
            if soup:
                source = str(soup)
                tag = soup.select_one('#myPlayerID')
                if tag:
                    account_id = tag['data-account']
                    playlist_id = tag['data-playlist-id'].split(':')[-1]
                    video_url = 'https://edge.api.brightcove.com/playback/v1/accounts/{account_id}/playlists/ref%3A{playlist_id}'.format(account_id=account_id, playlist_id=playlist_id)
                    headers = {
                        'accept': 'application/json;pk=BCpkADawqM0OuKDJ5R38w0cVfD7gnsu03wqoIVW0v2Q8Dt-29KOcaCLXKNdvki0FepOHdBou7VlqRYAehRaW_zxGrdyA3wu4g2nMhqKcnVRSfRRkFXrHz2FkheU',
                        'host': 'edge.api.brightcove.com',
                        'connection': 'keep-alive',
                        'accept-encoding': 'gzip, deflate, br',
                        'cache-control': 'no-cache',
                        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
                    }

                    response_text = self.downloader.download(video_url, headers=headers)
                    playlist_data = json.loads(response_text)
                    video_data = playlist_data['videos']
                    for video in video_data:
                        value.append(video['sources'][1]['src'])
                    

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value
        return source, value
