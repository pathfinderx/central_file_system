import json, logging, re
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class GraingerComWebsiteStrategy (WebsiteStrategy):
    WEBSITE = 'www.grainger.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.api_data = None
        self.sku = None

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        self.get_api_data () # execute api data extraction

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

         # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls 
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result

    def get_api_data (self):
        res = self.downloader.get_api_data ()

        try:
            if res and len(res) == 2: 
                data = json.loads(res[0]) # json value
                sku = res[1] # product array that can be used to access json key
                if data and sku:
                    self.api_data = data
                    self.sku = sku

        except Exception as e:
            self.logger.exception(e)

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.product-detail__heading')

            if tag:
                source = str(tag)
                value = tag.text.strip()

            elif self.api_data and self.sku and \
                self.sku in self.api_data and \
                'sellPrice' in self.api_data[self.sku]:

                source, value = str(self.api_data), str(self.api_data[self.sku]['sellPrice'])

            elif self.api_data and self.sku:
                if self.sku in self.api_data and \
                     self.sku in self.api_data and \
                    'price' in self.api_data[self.sku] and \
                    'sell' in self.api_data[self.sku]['price'] and \
                    'price' in self.api_data[self.sku]['price']['sell']:

                    source, value = str(self.api_data), str(self.api_data[self.sku]['price']['sell']['price'])
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        value = 'USD'

        try:
            tag = soup.select_one('meta[itemprop="priceCurrency"]')

            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag.get('content').strip()

            elif self.api_data and self.sku and \
                self.sku in self.api_data and \
                'priceCurrencyIso' in self.api_data[self.sku]:

                source, value = str(self.api_data), self.api_data[self.sku]['priceCurrencyIso']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if self.api_data and self.sku and \
                self.sku in self.api_data and \
                'product' in self.api_data[self.sku] and \
                'stockStatus' in self.api_data[self.sku]['product']:

                source, value = str(self.api_data), self.api_data[self.sku]['product']['stockStatus']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews not available in pdp and api
            # Ratings not available in pdp and api
            pass

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if self.sku and self.api_data and \
                'name' in self.api_data[self.sku]:
                
                source, value = str(self.api_data), self.api_data[self.sku]['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
           if self.sku and self.api_data and \
                'brand' in self.api_data[self.sku]:
                
                source, value = str(self.api_data), self.api_data[self.sku]['brand']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value        

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # no description found
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            if self.api_data and self.sku and \
                'techSpecs' in self.api_data[self.sku]:

                for x in self.api_data[self.sku]['techSpecs']:
                    if 'name' in x and 'value' in x:
                        value.update({x.get('name'):x.get('value')})

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            # tag = soup.select('#enhanced-content__lists > li')
            tag = soup.select('#enhanced-content__lists')
            if tag and tag[0]:
                img_tag = tag[0].select('li > div > button > img')

                if img_tag:
                    for x in img_tag:
                        if x.has_attr('data-src'):
                            value.append(x.get('data-src'))
                        
                    source = str(tag)

            elif self.api_data and self.sku and \
                'pictureUrl' in self.api_data[self.sku]:    

                source = str(self.api_data)
                value.append(self.api_data[self.sku]['pictureUrl'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        try:
            # video not available
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_shipping (self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            res = self.downloader.get_shipping_data (self.sku)
            if res:
                _json = json.loads(res)
                if _json and 'rtaResponseItems' in _json and \
                    _json.get('rtaResponseItems')[0] and 'rtaMessage' in _json['rtaResponseItems'][0]:

                    lxml_converted = BeautifulSoup(_json.get('rtaResponseItems')[0].get('rtaMessage'), 'lxml')
                    if lxml_converted:
                        source, value = str(_json), lxml_converted.get_text().strip()

        except Exception as e:
            self.logger.exception(e)


        return source, value