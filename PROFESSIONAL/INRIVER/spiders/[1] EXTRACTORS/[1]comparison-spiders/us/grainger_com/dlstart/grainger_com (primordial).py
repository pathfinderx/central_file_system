from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class GraingerComDownloadStrategy (DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.global_url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        self.global_url = url # initialize global url

        if not isinstance(headers, dict):
            headers = {
                'sec-fetch-dest': 'document',
                "sec-fetch-mode": 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def get_api_data (self, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(self.global_url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        sku = self.global_url.split('-')[-1]
        url = f'https://www.grainger.com/product/info?productArray={sku}&n=true'

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return [res.text, sku]

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    
    def get_shipping_data (self, sku, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(sku, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        payload = {
            'itemIDs': [sku],
            'itemQtys': ['1']
        }

        headers = {
            # 'accept': 'application/json, text/javascript, */*; q=0.01',
            # 'accept-encoding': 'gzip, deflate, br',
            # 'accept-language': 'en-US,en;q=0.9',
            # 'cache-control': 'no-cache',
            # 'content-length': '39',
            # 'content-type': 'application/json',
            # 'cookie': 'btps=false; sitetype=full; AB1=A; AB2=B; AB4=B; DS1=A; DS2=B; geo=07011|CLIFTON|NJ|US; country=US; TLTSID=5BBA88887C34A47F352C04C28967A145; gws_lcp=1; __wwgmui=8333f886-d7ca-4065-8936-b55a30de5456; O=1p; at_check=true; AMCVS_FC80403D53C3ED6C0A490D4C%40AdobeOrg=1; s_ecid=MCMID%7C13491677739190621474080501021446779327; AC=T; s_cc=true; aam_uuid=13592165948637365424090683210315784162; Monetate=seg%3D1139434; _gcl_au=1.1.1599969597.1643420465; _gid=GA1.2.950823987.1643420473; _clck=1p1arey|1|eyj|0; insightEligible=false; qc=0; compare=; _fbp=fb.1.1643420532442.2033667852; ak_bmsc=D3A494CB0D6F9605FF2609CF15C6B1F5~000000000000000000000000000000~YAAQRIPXF2R+7pR+AQAA0KXCpA4nWGlRtrcqAX7ICNsU9PYx2/MiE4kZM+Ogjgqeq5+IZ+BIvsWRmg1T9MJXsnpG+w+GkxoM7CFoCANmx0BAd6Tl0OTBRv89zPESLConoQuzAH/sidkd9LfEuTbbJcbScdpbKx+9jqdIGHtx14r0jT7B+P8LrKo0mCL0E/BU1PfUjvpOqg5cJUt5UAkhD9ARQhaHNbmfJn8oiXNxmgLMW+JVl6hN7meV/njd1Gb9sWcvbWLRm9VP0nTbezss9L678e+j2Z7mXXOPvhBrsaO7ddLHvmvsUSYes7WFqcSLaua5XI4y7gpTIL+hjJEjHIdMvW6VGdeVcfeTkygMgeHuVD2f0+7nXQf7Z6Jl5pUKoUQSRcu7wY3n9CpgmRA=; bm_sz=70CF82E7B5C5379C7E13214A5A6F45DD~YAAQRIPXF2V+7pR+AQAA0KXCpA6GQ4ZnYY9N86NWTHR6V4m1I1ImdVf2pXkYdL9jXNLNHk71WZOKjgFdAaetPjUeLxFQ9ep4GoiXBgP2OZ995ZwDeVYELBleObdH/eVisKztgOTwRr1UO7+1dPXk3F4TcBcMxbo6Q5Q16HfJK4csn8ha/id1rGxexegAJnNLyej35VkV6s1itZ/DgON/V01HIjYeC+xqiZ9usin2XBi6AtrZWWHb4OamrP/sdaSIK8q7uBgcX8xUU1JTckvL7sFYhdXCkcbGfVugIzsEZRIjwlmwdQ==~3420721~3355449; ttc=1643442204063; _abck=D125564172E1110BCB4FA269360B1CF8~0~YAAQRIPXF2K47pR+AQAANfjJpAdeEKyEKll3rh6a0LQtKjVon71Do8qHz54uOIl6o/sWrMrdYyJfOziaRZYcaqwyAfo8FekZWvQtEh9SBYCWhpVrvFtScKZBz8LH+SEXCC6ovla/yYC9M8rx3puue4YUyoUqv5T0SkKEk5aNQgGlnZxmEkbaHZ8C9lBKnCnRdadsA1x53CVkunsT4n6iYa7yFkpojWFFoJjBEgIAn6n4+CznKqzXDqrndtAKsBO9ydMLqE/LjBNNBlthaHqgGUaZNIKr8lxGIPlKgaoEIJIMLy9jYNrtN7cXfrGg0Vp0fvwej+MoDf60s0tt0QXVfqh5hAn0YPWMCDjt/98MO/OpeokLy9+nf8iRnkAvqDRa6iBCbB3AJQrqdPO6Yo5Zrgo0c6Zcw3XDFyo=~-1~-1~1643445763; JSESSIONID=6413049BADCEA2CBC13BFFA0BD0DAD8C.3ae2f95c; s_sess=%20hbx_lt%3D%3B; CIP=108.61.86.26; AMCV_FC80403D53C3ED6C0A490D4C%40AdobeOrg=-2121179033%7CMCIDTS%7C19022%7CMCMID%7C13491677739190621474080501021446779327%7CMCAAMLH-1644047584%7C7%7CMCAAMB-1644047584%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1643449984s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.3.0; maId={"cid":"52eed90414528939807cab2cd5bdf918","sid":"6b52b2a1-fb5f-416c-ab43-235041755686","isSidSaved":true,"sessionStart":"2022-01-29T07:54:22.000Z"}; gpv_v10=grainger%2Fproduct%2Fcleaning%20and%20janitorial%2Ffloor%20cleaning%20machines%2Fcarpet%20%26%20floor%20sweepers; mbox=PC#5ebaccc135d34eab866d88adb43b0f96.35_0#1706687888|session#695f9af4f7494ecd853a08041179d2de#1643444653; s_nr30=1643443089448-Repeat; s_pers=%20s_vnum%3D1643644800783%2526vn%253D6%7C1643644800783%3B%20s_invisit%3Dtrue%7C1643444889448%3B; OptanonConsent=isIABGlobal=false&datestamp=Sat+Jan+29+2022+15%3A58%3A14+GMT%2B0800+(China+Standard+Time)&version=6.10.0&hosts=&consentId=24c76e92-35e0-4b96-b7e5-ce66cf14289d&interactionCount=1&landingPath=NotLandingPage&groups=BG9%3A1%2CC0007%3A1%2CBG10%3A1%2CC0003%3A1%2CC0001%3A1%2CC0002%3A1&AwaitingReconsent=false; _uetsid=fc7f62d0809b11eca149cb8d3947b626; _uetvid=fc7f8310809b11ec8d9f2ba67e5c9eb1; _ga=GA1.2.1527694289.1643420469; _clsk=m7ipi7|1643444455379|9|0|b.clarity.ms/collect; _ga_94DBLXKMHK=GS1.1.1643442535.5.1.1643444460.0; RT="z=1&dm=grainger.com&si=a6546ad0-4e96-4918-a265-c397a500c859&ss=kyzjgpwv&sl=1&tt=78n&bcn=%2F%2F173bf108.akstat.io%2F"; bm_sv=AB1914333DFE578B43CA76D0883D7041~zJcVnlMDffWxscPZrZ/k4rc4fnD1ICaDcUgU16Pv1pCBbYbDAiRHB4OYjucddGH28Ar7CI4vtwkmULq/GA2MJfti0hPefz0LZ+zZsT4qKEdijhjlZ5R48UDMze9jOyfCHR/+79ws9EErzR5c9HOqypMpudXyF8ugRYgyyF3ZNxA=',
            # 'origin': 'https://www.grainger.com',
            # 'pragma': 'no-cache',
            # 'referer': 'https://www.grainger.com/product/BISSELL-Walk-Behind-Sweeper-61TX41',
            # 'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            # 'sec-ch-ua-mobile': '?0',
            # 'sec-ch-ua-platform': '"Windows"',
            # 'sec-fetch-dest': 'empty',
            # 'sec-fetch-mode': 'cors',
            # 'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
            # 'x-requested-with': 'XMLHttpRequest'
        }

        url = f'https://www.grainger.com/rta/getrtamessages'

        try:
            if payload:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=None, data=json.dumps(payload))

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return [res.text, sku]

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')