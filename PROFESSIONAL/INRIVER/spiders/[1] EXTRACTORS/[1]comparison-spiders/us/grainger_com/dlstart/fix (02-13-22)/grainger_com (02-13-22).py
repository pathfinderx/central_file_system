from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json, cloudscraper
class GraingerComDownloadStrategy (DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
        self.global_url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        self.global_url = url # initialize global url

        if not isinstance(headers, dict):
            headers = {
                'sec-fetch-dest': 'document',
                "sec-fetch-mode": 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def get_api_data (self, sku=None, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(self.global_url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        if not sku:
            sku = self.global_url.split('-')[-1]

        url = f'https://www.grainger.com/product/info?productArray={sku}&n=true'

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return [res.text, sku]

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    
    def get_shipping_data (self, sku, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(sku, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            # Just change this header if api response is unsuccessful
            headers = {
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'content-length': '226',
                'content-type': 'application/json',
                'cookie': '__wwgmui=c473b437-0bbd-4f40-8d4b-dda59de443a3; O=1p; insightEligible=false; qc=0; btps=false; sitetype=full; AB1=A; AB2=B; AB4=B; DS1=B; DS2=A; CIP=192.241.244.170; geo=10001|NEWYORK|NY|US; country=US; TLTSID=CD3FBFA8FEB4F5799F36EC9E3AF313C7; gws_lcp=1; signin=A; bm_sz=54A18E7B824EC6D8E996250545D5ACD9~YAAQyZw6F8CO/bB+AQAA60ic8A4zO8VpT/VJfWWiniraXrW/7/hQCKureQWlLrO54HpKnDde6CCLYqKQpyME+g9kAo5kt2D5RPpyhbH+ocRBx6ePEIHK1AcqCkjkVw94nGCYJHKr+iiGJtioYOxcEsNH+EUquK33fUS71d5cN7xYeLlbiSkiv7aCnfC5hu7Rg1blVTEW7Qpmgg0zQnoTAr5ujbh1dJUdSeqYf2FVe3kQQ4xdnJBUrPKdlCiI1Tmbw7WoJPO5Jm/j/UVV8w0XeFL+2769rnTVcc6q6uvxsne8+pjMrw==~4403507~3294517; at_check=true; compare=; AMCVS_FC80403D53C3ED6C0A490D4C%40AdobeOrg=1; s_ecid=MCMID%7C13442217791533665860534798627037816197; s_sess=%20hbx_lt%3D%3B; s_cc=true; Monetate=seg%3D1139434; aam_uuid=13517815724987679750508379263322704856; AMCV_FC80403D53C3ED6C0A490D4C%40AdobeOrg=-2121179033%7CMCIDTS%7C19037%7CMCMID%7C13442217791533665860534798627037816197%7CMCAAMLH-1645319088%7C7%7CMCAAMB-1645319088%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1644721490s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-19044%7CvVersion%7C5.3.0; _gcl_au=1.1.453544494.1644714293; _gid=GA1.2.971546292.1644714296; _clck=jx0wbd|1|eyy|0; _fbp=fb.1.1644714303484.1711880434; ttc=1644719893523; gpv_v10=grainger%2Fproduct%2Fplumbing%2Fplumbing%20valves%2Fcheck%20valves%20and%20backflow%20preventers%2Fbackflow%20preventers; _abck=B62AEFA744C5360EAFE77B87146E6760~0~YAAQx8jZFyzr6rd+AQAAC+bx8Afhi01F1+BOK8P1kljaZTLwcM8wP1VVF3vdwBzaxKfh8a7ukfdjI70Zu41MygePJbbR5YapELjeJxDZ1+bKcUTNjdn+QaHRmUADCCINWS8WMqPsi9i57JlEOsuTkND1gzLG46noWcEGYt4tbXn5nHpZynUStQDbiFkS28sCq1KhF0SYLUnwBULoz4CwsZlDok9B6ZTt9CCbGRj32A5GXrYiwyz2WsWeGjDgJCuznAyHYgVTeIF/f/0HJX83l897+MN/TL1CcbRQ/qTt2R3dG3tWtnnYGmr36o3RshfpMJTtlUlpyEGipPzx/UEuA+siKu6V8V+olRd0LA+5zakME/p3Otkc/gvJVvv82VtdquGNwWNjU3u3/pVlWLCKnmhK9lkWlFx6hP8=~-1~-1~1644723400; JSESSIONID=167EC0DEC96787DA1B5C5C6DB5EC30F5.a94e3f90; gpv_c40=1%5Cx2d375; _uetsid=f5680d608c6811eca1bc25ddb21375fd; _uetvid=f56823108c6811eca8e2515aac1326b9; OptanonConsent=isIABGlobal=false&datestamp=Sun+Feb+13+2022+10%3A40%3A58+GMT%2B0800+(China+Standard+Time)&version=6.10.0&hosts=&consentId=3fef90fb-aa07-4f80-a127-e9172f98937b&interactionCount=1&landingPath=https%3A%2F%2Fwww.grainger.com%2Fproduct%2FZURN-WILKINS-Reduced-Pressure-Zone-Backflow-6AVY0%3FsearchQuery%3D1-375%26searchBar%3Dtrue&groups=BG9%3A1%2CC0007%3A1%2CBG10%3A1%2CC0003%3A1%2CC0001%3A1%2CC0002%3A1; _ga=GA1.2.1263192045.1644714293; _clsk=1q2eaz7|1644722121625|1|1|h.clarity.ms/collect; s_nr30=1644722685802-Repeat; s_pers=%20s_vnum%3D1646064000232%2526vn%253D2%7C1646064000232%3B%20s_invisit%3Dtrue%7C1644724485802%3B; ak_bmsc=68940AFA2D92992AF5CFEDBBC58FC088~000000000000000000000000000000~YAAQp8jZFzoUubd+AQAAepIc8Q4g54oJCy7muJ0LIoRwXsz2ETH3zElVI59MKw9b7tWWV08f9F+ASejo9FEKsPw6cdkaDcYDGpLM8YKSv9C74v9goYOnBPuREp+pqlf4zw3eqfFEl8ysU6umKeOdku/CWXdbuidhYXWpTCLAPxP88wrSJsfy28crw8hVsJrDeJQSrdr7Ts/qG7bysYdqHv2TMYockR4sVpsM67A2g4qF+2zr1WMxLHgzwOGO8Fuj5UmLTJnLWwt8J44DJU9kTj17+uH2aFKAmDCt+3gf90+5yFSmxrEDUpxoh0USU6ubbN4Xv1vDpQr9jzobPgNsJDgaNnrhyaCcdxxFlaKnwpbEeLaI3E3M1Vn9wkFoTb0W0WBXupihje8/hqOqDw==; _ga_94DBLXKMHK=GS1.1.1644722690.3.0.1644722690.0; RT="z=1&dm=grainger.com&si=36c61bfb-1bbf-4ecd-86d3-937e42e9ce19&ss=kzknujcd&sl=1&tt=5jt&bcn=%2F%2F173bf10e.akstat.io%2F&ld=1kwkl"; mbox=PC#5a54e4c778f240ed80bdb86da0990236.34_0#1707964853|session#b1a15ec92d1c43a593e4079e50d17e70#1644724564; bm_sv=817D4FD467F4EF3385DFCE43DFD9E13A~gZtR1Kduh66l3vvls+RZfx6NN8+PXCy3vr95lkbYCBIY2nQfGhV2JIgVK4qMUDI9k/awIRSnNp05ccBXT+y1UDOH4GRlCDDSoLs/FN6XuQhad4IX8gZAM1HygT5m9TcYMNMuDTTyhsuraXiT0MsPp2WhCTx4Dm9fvDAmTqzo0/0=',
                'origin': 'https://www.grainger.com',
                'pragma': 'no-cache',
                'referer': 'https://www.grainger.com/product/ZURN-WILKINS-Reduced-Pressure-Zone-Backflow-6AVY0?searchQuery=1-375&searchBar=true',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest'
            }

        if not isinstance(cookies, dict):
            cookies = None

        payload = {
            "itemIDs":[sku],
            "itemQtys":["1"]
        }

        url = f'https://www.grainger.com/rta/getrtamessages'

        try:
            if payload:
                res = self.scraper.post(url, timeout=timeout, cookies=None, data=json.dumps(payload))

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')