from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class GraingerComDownloadStrategy (DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.global_url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        self.global_url = url # initialize global url

        if not isinstance(headers, dict):
            headers = {
                'sec-fetch-dest': 'document',
                "sec-fetch-mode": 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    def get_api_data (self, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(self.global_url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        sku = self.global_url.split('-')[-1]
        url = f'https://www.grainger.com/product/info?productArray={sku}&n=true'

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return [res.text, sku]

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
    
    
    def get_shipping_data (self, sku, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(sku, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'content-length': '212',
                'content-type': 'application/json',
                'cookie': 'sitetype=full; AB2=B; DS2=B; geo=07011|CLIFTON|NJ|US; country=US; TLTSID=5BBA88887C34A47F352C04C28967A145; __wwgmui=8333f886-d7ca-4065-8936-b55a30de5456; O=1p; at_check=true; AMCVS_FC80403D53C3ED6C0A490D4C%40AdobeOrg=1; s_ecid=MCMID%7C13491677739190621474080501021446779327; AC=T; s_cc=true; aam_uuid=13592165948637365424090683210315784162; Monetate=seg%3D1139434; _gcl_au=1.1.1599969597.1643420465; _gid=GA1.2.950823987.1643420473; insightEligible=false; qc=0; compare=; _fbp=fb.1.1643420532442.2033667852; AMCV_FC80403D53C3ED6C0A490D4C%40AdobeOrg=-2121179033%7CMCIDTS%7C19022%7CMCMID%7C13491677739190621474080501021446779327%7CMCAAMLH-1644063327%7C7%7CMCAAMB-1644063327%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1643465727s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.3.0; _uetsid=fc7f62d0809b11eca149cb8d3947b626; _uetvid=fc7f8310809b11ec8d9f2ba67e5c9eb1; OptanonConsent=isIABGlobal=false&datestamp=Sat+Jan+29+2022+20%3A16%3A01+GMT%2B0800+(China+Standard+Time)&version=6.10.0&hosts=&consentId=24c76e92-35e0-4b96-b7e5-ce66cf14289d&interactionCount=1&landingPath=NotLandingPage&groups=BG9%3A1%2CC0007%3A1%2CBG10%3A1%2CC0003%3A1%2CC0001%3A1%2CC0002%3A1&AwaitingReconsent=false; _ga=GA1.2.1527694289.1643420469; s_nr30=1643459328842-Repeat; s_pers=%20s_vnum%3D1643644800783%2526vn%253D8%7C1643644800783%3B%20s_invisit%3Dtrue%7C1643461128843%3B; s_sess=%20hbx_lt%3D%3B; _clck=1p1arey|1|eyk|0; _clsk=1gi5jum|1643510364674|1|0|b.clarity.ms/collect; btps=false; AB1=A; AB4=B; DS1=A; CIP=192.241.250.184; gws_lcp=1; ak_bmsc=1BDB7102B06D6051560D2A9366F8A690~000000000000000000000000000000~YAAQl5EvFy5nS4J+AQAAGYsTqQ5SKUP8Sb4tXTxZIeYpX1heKz8jVJUPrzKGZpw5WjFeVBJHGIaXT6hQ5xrclK+ZUtbEH/GfZmDfvVfGEV/WXmsMP9uNEi7un4cCETQ+Ttkvtex82P0UY/6yaT3cqkEg0b7c62VC/vkaf6MMpXy/7pr8RRDP/puTw5LSGwh7pHbL9jO9hj4k+KvcIYMkyHWvws3LcHRdgIZA0cHfStPMefnIp99kSq3NYQhtVG4ZuY1OmlI/jFCGzaoR3WFKskfJMOjOSzoDO+QZ48kZ+lW+n/3NYGPijmwQ9L4eQ/rE1iZoI9da+4ytiJCbAtO0wLt3IWobo6aoXBNdsyQB2Dt27hZekYAWv2Iqxyw9klzQrJH/py9EbH92VYgmF2I=; bm_sz=A7E78BB3BC14E9D1DC5663F9A696BDE4~YAAQl5EvFy9nS4J+AQAAGYsTqQ58Ic3q624zRV85iSUEjkJGuoAqS0EsMh1ui2OTdGgEQQPdKCgYc33V4iPA+fOyqO91+CH7cvcojM5TcphNClPjNuGoOinbQ48FoPZKxVrcZNxRb6fJlvTwemKTRpoPMWWtWDMNTZwKWtHlAeJ4vXRF/he+T6QSsRSJOmPF4Xrm03OjE8ZP9219AO6ywQ/Kw2Fn0zdD1hk8L24uTwmqzN0I2VOYb7U/VHScuN4jaO/ubJDhppfs/a2iEVEp9vxAgnJIsisoBjlGrSze2DQsjTXqcA==~3487024~3229237; _ga_94DBLXKMHK=GS1.1.1643514137.8.0.1643514137.0; _abck=D125564172E1110BCB4FA269360B1CF8~0~YAAQl5EvF4JnS4J+AQAAFZ4TqQfzKl0pVSxEcKzrGdQlNLKGPU4tcnqq+3IgyrTM5TpM2IyJKBu/sCcHBhu0uyPipWnyx0+XZArnQTTvQu/gUo57F8ZSvdU4uJTBOmWJFIYuDGuz7Ix3+HI3VAMzNtrBnyBJX/4FMNJGaTv8Y6pvDIRPgAGzL/ELDnfYCJhXgZMy/ZVPpBjVf0Y/5MovveS3LD7NRuxcle36in57889h7Hny42gnWGIxuZ7sHOXSlvh54l628Ddt17DxVIqKVGFsWyEaYwIVY6Pie2+uULRrjAUPyoIHl2qZ9hXUtPKu9+Phm4egpaX/YUoWRyMoGZ/G++kCz0Bs6c+imexRlNQ2g/QjQ6INlpWUFDOXUpGpYSnjD5PnvpnvXgSEyMA94LAIRTQLeLEcpLI=~-1~-1~1643517700; RT="z=1&dm=grainger.com&si=a6546ad0-4e96-4918-a265-c397a500c859&ss=kyzsrow2&sl=0&tt=0&bcn=%2F%2F173bf111.akstat.io%2F"; JSESSIONID=52DD47EFC620C7575040746765D95758.3ae2f95c; mbox=PC#5ebaccc135d34eab866d88adb43b0f96.35_0#1706703352|session#81b3de70c11a4de6bce13e67fd6305c1#1643516006; bm_sv=491896386B49CEFF556626723794167F~saT21Kbmz+MYMA6xI6SCsPgjrkjkHvdW7eFBxxnfvD7YzMxZrBPilYiZfYPD5YlM+iMVW3yANtUmqqKWzxNxO6Bj3XlMnw+zimBqEOgDkKYZCv44B8Cs4dKVth04NCC/Z/jVpTdM8KuCmdBsRW7fpgTPJ4VXjvGNTQd6YxnytA8=',
                'origin': 'https://www.grainger.com',
                'pragma': 'no-cache',
                'referer': 'https://www.grainger.com/product/RUBBERMAID-COMMERCIAL-PRODUCTS-Stick-Sweeper-5M865',
                'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest'
            }

        if not isinstance(cookies, dict):
            cookies = None

        payload = {
            'itemIDs': [sku],
            'itemQtys': ['1']
        }

        url = f'https://www.grainger.com/rta/getrtamessages'

        try:
            if payload:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=None, data=json.dumps(payload))

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')