import re, json
import logging
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MediamarktEsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.es'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        self.script_data = self.get_json(soup)

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    # JSON
    def get_json(self, soup):
        src = Defaults.GENERIC_NOT_FOUND.value
        val = None

        tag = soup.find(lambda tag: tag.name == "script" and "priceCurrency" in tag.text)

        if tag:
            src = str(tag)
            val = json.loads(tag.text, strict=False)
        
        return src, val

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, tag = self.script_data

            
            if tag:
                if 'offers' in tag.keys() and 'price' in tag['offers']:
                    value = str(tag['offers']['price'])
           
            if not tag:
                tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

                if tag:
                    _rgx = re.search(r"({.*})", tag.get_text().strip())
                
                    # remove undefined and any object instantation (new ...)
                    # from object literal so that it can be qualified as
                    # valid json
                    json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                    json_data = re.sub(r':undefined', ':null', json_data)
                    
                    if _rgx: 
                        source = str(tag)
                        _json = json.loads(json_data)

                        for keys in _json['apolloState']['ROOT_QUERY']:
                            if 'price({' in keys:
                                if _json['apolloState']['ROOT_QUERY'][keys]['__typename'] == 'GraphqlPrice':
                                    value = str(_json['apolloState']['ROOT_QUERY'][keys]['price'])
                                break
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, tag = self.script_data

            if tag:
                if 'offers' in tag.keys() and 'priceCurrency' in tag['offers']:
                    value = str(tag['offers']['priceCurrency'])
          
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try: 
            tag = soup.select_one('span.StyledInfoTypo-sc-1jga2g7-0.jlGGjh')
            if tag:
                source = str(tag)
                value = tag.get_text().strip()
            else:
                source, tag = self.script_data
                if tag: 
                    if 'offers' in tag.keys() and 'availability' in tag['offers']:
                        value = str(tag['offers']['availability']) 
                elif not tag:
                    tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))
                    if tag:
                        source = str(tag)
                        rgx = re.search('\"quantity\":(\d+)', tag.text)
                        if rgx:
                            value = rgx.group(1)
                    # not_available_tag = soup.select_one('[data-test="pdp-product-not-available"]')
                    # if not_available_tag:
                    #     source,value = str(not_available_tag),not_available_tag.text.strip()       
                elif soup.select_one('[data-test="pdp-product-not-available"]'): # https://prnt.sc/1uu6j8k
                    tag = soup.select_one('[data-test="pdp-product-not-available"]')
                    source = str(tag)
                    value = tag.get_text().strip()
    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            score_source, score_tag = self.script_data
            reviews_source, reviews_tag = self.script_data

            if reviews_tag:
                if 'aggregateRating' in reviews_tag and 'ratingCount' in reviews_tag['aggregateRating']:
                    reviews_value = str(reviews_tag['aggregateRating']['ratingCount'])

            if score_tag:
                if 'aggregateRating' in score_tag and 'ratingValue' in score_tag['aggregateRating']:
                    score_value = str(score_tag['aggregateRating']['ratingValue'])

            if not reviews_tag and not score_tag:
                tag = soup.find('script', {"type" : "application/ld+json"})

                if tag:
                    reviews_source = str(tag)
                    score_source = str(tag)
                    _json = json.loads(tag.text, strict=False)

                    if _json and 'aggregateRating' in _json.keys():
                        if 'ratingCount' in _json['aggregateRating']:
                            reviews_value = str(_json['aggregateRating']['ratingCount'])
                            
                        if 'ratingValue' in _json['aggregateRating']:
                            score_value = str(_json['aggregateRating']['ratingValue'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('title')
            if tag:
                source = str(tag)
                value = " ".join(tag.stripped_strings) 
            else:
                source, tag = self.script_data   
                if tag:
                    if 'name' in tag:
                        value = tag['name']
                if not tag:
                    tag = soup.find("script", {"type" : "application/ld+json"})
                    if tag:
                        source = str(tag)
                        _json = json.loads(tag.text, strict=False)
                        if _json and 'name' in _json:
                            value = _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source, tag = self.script_data

            if tag:
                if 'brand' in tag.keys() and 'name' in tag['brand']:
                    value = str(tag['brand']['name'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div[data-test="mms-accordion-description"]')

            if tag:
                # desc = ' '.join(tag.stripped_strings)
                # listStrings = tag.get_text().split('\n')
                # desc = ' '.join([ string.strip() for string in listStrings if string != ''])
                # if desc:
                #    source, value = str(tag), desc

                # source = str(tag)
                # value = tag.get_text().strip().replace('\n', ' ')

                desc_tags, description = tag.find_all(['p', 'h3']), str()
                if desc_tags:
                    source = str(tag)
                    for x in desc_tags:
                        description += x.get_text() + ' '

                value = description.replace('\n', '').strip() if value else None

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            # tags = soup.select('[data-test="mms-accordion-features"] tbody tr')
            
            # if len(tags):
            #     source, value = str(tags), self._get_initial_specs(tags)
            tag = soup.find("script", text = re.compile(r".*window\.__PRELOADED_STATE__.*"))
            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
                
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    for keys in _json['apolloState']:
                        if 'GraphqlProduct:' in keys:
                            for spec_group in _json['apolloState'][keys]['featureGroups']:
                                for specs in spec_group['features']:
                                    if not specs['unit']:
                                        value[specs['name']] = specs['value']
                                    else:
                                        value[specs['name']] = specs['valuesAndUnits'][0]['value'] + " " + specs['valuesAndUnits'][0]['unit']
                            break

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find("script", text = re.compile(r".*window\.__PRELOADED_STATE__.*"))
            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
                
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    for keys in _json['apolloState']:
                        if 'GraphqlProduct:' in keys:
                            for img in _json['apolloState'][keys]['assets']:
                                if img['usageType'] in ['Product Image', 'Lifestyle Image']:
                                    value.append(img['link'])
                            break
               
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    # VIDEO URL
    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value =  [] # video_urls data type must be list
      
        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}
        for nth in tags:
            key = nth.select('td:nth-of-type(1)')
            val = nth.select('td:nth-of-type(2)')
            result["%s" % key[0].get_text()] = val[0].get_text().strip()
        return result

    @staticmethod
    def filter_text(text):
        text = text.replace('meta name="description" content="', '').replace('/p', '').replace('/strong', '').replace('h3', '').replace('/  p', '').strip()
        clean = re.compile('<.*?>')
        sanitized_text = re.sub(clean, " ", text)
        return sanitized_text