import json
import logging
import re
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template, get_result_buybox_template


class AmazonEsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.amazon.es'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        
    def execute(self, raw_data):
        result = get_result_base_template()
        buybox_template = get_result_buybox_template()
        soup = BeautifulSoup(raw_data, "lxml")
        soup = BeautifulSoup(str(soup), "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)  
        
        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)        

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)  

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)                    

        # Extract Buybox
        result["buybox"] = self._get_buybox(soup, buybox_template)

        return result            

    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            # check if used, if used value should be NOT_FOUND
            used = soup.select_one('#usedOnlyBuybox #usedBuySection span.a-text-bold')
            if used:
                if used.get_text().strip().lower() in ['comprar de 2ª mano', 'comprar de segunda mano']:
                    return source, value
            
            tag = self.check_tag(
                soup.select_one(".a-price.priceToPay span.a-offscreen"),
                soup.select_one('div#corePrice_feature_div .a-price span.a-offscreen'),
                soup.select_one('[class="a-price aok-align-center a-text-bold priceSizeOverride"] span'),
                soup.select_one('[class="a-price a-text-price a-size-medium"] span'),
                soup.select_one('span.a-price.a-text-price.a-size-medium.apexPriceToPay > span'),
                soup.select_one('#priceblock_ourprice'),
                soup.select_one('#price_inside_buybox'),
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('.a-size-large.a-color-price.olpOfferPrice.a-text-bold'),
                soup.select_one('#mbc-price-1'),
                attrs = ['data-asin-price']

                
            )

            if tag:
                source, value = tag

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one('#cerberus-data-metrics'),
                soup.select_one('#attach-base-product-currency-symbol'),
                attrs=['data-asin-currency-code', 'value']
                )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    # GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#availability')

            if tag:
                tag = self._sanitize(tag)
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    # GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_score = self.check_tag(
                soup.select_one('[data-hook="rating-out-of-text"]'),
                soup.select_one('#acrPopover'),
                soup.select_one('#olpProductDetails i.a-icon.a-icon-star span'),
                attrs = ['title']
            )
            tag_review = self.check_tag(
                soup.select_one('#acrCustomerReviewText'),
                soup.select_one('#olpProductDetails span.a-size-small a')
                )

            if tag_score and tag_review:
                reviews_source, reviews_value  = tag_review
                score_source, score_value = tag_score
            
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value


    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('#productTitle') or \
                soup.select_one('#olpProductDetails .a-size-large.a-spacing-none')

            if tag:
                source, value = str(tag), ' '.join(tag.stripped_strings)                         
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    # GET BRAND
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('th', text=re.compile(r'Marca', re.IGNORECASE)) # Brand in Specs
            if tag:
                parent_tag = tag.find_parent('tr')
                if parent_tag:
                    brand_tag = parent_tag.select_one('td')
                    if brand_tag:
                        source = str(parent_tag)
                        value = brand_tag.get_text().strip()
            elif soup.find('span', text=re.compile(r'.*Marca.*', re.IGNORECASE)): # Brand in top Desc
                tag = soup.find('span', text=re.compile(r'.*Marca.*', re.IGNORECASE))
                if tag:
                    parent_tag = tag.find_parent('tr')
                    if parent_tag:
                        brand_tag = parent_tag.select('td')
                        if len(brand_tag) == 2:
                            source = str(parent_tag)
                            value = brand_tag[-1].get_text().strip()
            else:
                tag = self.check_tag(
                    soup.select_one('#mbc'),
                    soup.select_one('#bylineInfo'),
                    attrs=['data-brand']
                )

                if tag:
                    source, value = tag
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    # GET DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _src = []
            description = ''
            # Product Description Top
            tags = soup.select('div#feature-bullets ul li')
            if len(tags):
                _src.append(str(tags))
                for li in tags:
                    if not li.has_attr('id'):
                        description = description + ' ' + ' '.join(li.stripped_strings)

            # From Manufacturer
            tag = soup.select_one('#dpx-aplus-product-description_feature_div') or soup.select_one('#aplus_feature_div')
            if tag:
                _src.append(str(tag))
                tag = self._sanitize(tag)
                description = description + ' ' + ' '.join(tag.stripped_strings)

            # Product Description Bottom
            tag = soup.find('div',{'id':'productDescription'})
            if tag:
                _src.append(str(tag))
                description = description + ' ' + ' '.join(tag.stripped_strings)
            # else:
            #     tags = soup.select('td.apm-top')
            #     if len(tags):
            #         _src.append(str(tags))
            #         for tag in tags:
            #             description = description + ' ' + ' '.join(tag.stripped_strings)

            # DO NOT UNCOMMENT -- this description is duplicate (From Manufacturer tags)
            # ADD description tags that correspond to positions above
            # tags = soup.select('div.apm-sidemodule-textright')
            # if tags:
            #     _src.append(str(tags))
            #     for tag in tags:
            #         description = description + ' ' + ' '.join(tag.stripped_strings)

            # tag = soup.find('div',{'class':'apm-sidemodule-textleft'})
            # if tag:
            #     _src.append(str(tag))
            #     description = description + ' ' + ' '.join(tag.stripped_strings)

            # tag = soup.select_one('[id="productDescription"] > p')
            # if tag:
            #     _src.append(str(tag))
            #     description = description + ' ' + ' '.join(tag.stripped_strings)

            if description:
                source = " + ".join(_src)
                value = description

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    # GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}
                    
        try:
            #Not Working
            # tag_key = self.check_tag(
            #     soup.select('#prodDetails tr td.label'),
            #     soup.select('.wrapper.CAlocale tr td.label'),
            #     soup.select('#productDetails_detailBullets_sections1 tr th'),
            #     soup.select('.tsRow span:nth-of-type(1)'),
            #     soup.select('#product-details-grid_feature_div tr td.label'),
            #     )
            # tag_val = self.check_tag(
            #     soup.select('#prodDetails tr td.value'),
            #     soup.select('.wrapper.CAlocale tr td.value'),
            #     soup.select('#productDetails_detailBullets_sections1 tr td'),
            #     soup.select('.tsRow span:nth-of-type(2)'),
            #     soup.select('#product-details-grid_feature_div tr td.value'),
            # )

            # if tag_key and tag_val:
            #     source, value = self._build_spec(tag_key, tag_val)
            tag = soup.select_one('.a-expander-content.a-expander-section-content.a-section-expander-inner')
            if tag:
                specification_tag = tag.find('table',{'class':'a-keyvalue prodDetTable'})
                if specification_tag:
                    source = str(specification_tag)
                    specifications = specification_tag.select('tr')
                    value = {}
                    for spec in specifications:
                        key_value = spec.select_one('th')
                        v_value = spec.select_one('td')
                        if key_value and v_value:
                            value[key_value.text] = v_value.text
            else:
                tag = soup.findAll('div', {'class':'a-section a-spacing-small a-spacing-top-small'})
                if tag and len(tag) > 1:
                    specification_tag = tag[1].find('table', {'class': 'a-normal a-spacing-micro'})
                    if specification_tag:
                        source = str(specification_tag)
                        specifications = specification_tag.select('tr')
                        value = {}
                        for spec in specifications:
                            values = spec.select('td')
                            if values:
                                value[values[0].text] = values[1].text

            other_spec_tag = soup.select_one('#product-specification-table')
            if other_spec_tag:
                source = str(other_spec_tag)
                list_tag = other_spec_tag.findAll('tr')

                if list_tag:
                    for spec in list_tag:
                        key_val = spec.select_one('th')
                        v_val = spec.select_one('td')
                        value[key_val.get_text(strip=True)] = v_val.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value        

    # GET IMAGES
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            if tag:
                text = tag.get_text().strip().split('jQuery.parseJSON')[1]
                rgx = re.search(r'(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
                source = str(tag)
                
                if rgx:
                    try:
                        _json = json.loads(rgx.group(1).replace('l\\', '').replace('\\',''))
                    except:
                        _json = json.loads(rgx.group(1).replace('l\\', ''))
                    img_list = _json['landingAsinColor']
                    if img_list in _json['colorImages']:
                        for img in _json['colorImages'][img_list]:
                            if 'hiRes' in img and img['hiRes']:
                                value.append(img['hiRes'])
                            elif 'large' in img and img['large']:
                                value.append(img['large'])
                    else:
                        tag = soup.find('script', text=re.compile('.*colorImages*'))
                        if tag:
                            _json = tag.get_text().strip()
                            _json = _json.split("'colorImages':")[1]\
                                    .split("'colorToAsin'")[0].strip()[:-1]\
                                    .replace("'",'"')
                            _json = json.loads(_json)
                            source = str(tag)
                            if 'initial' in _json:
                                for i in _json['initial']:
                                    if i['hiRes']:
                                        value.append(i['hiRes'])
                                    elif i['large']:
                                        value.append(i['large'])                                

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tag = soup.find('script', text=re.compile('.*jQuery.parseJSON.*'))

            if tag:
                text = tag.get_text().strip().split('jQuery.parseJSON')[1]
                rgx = re.search(r'(\{(.*)\})\'', text, re.DOTALL|re.MULTILINE)
                source = str(tag)
                
                if rgx:
                    _json = json.loads(rgx.group(1).replace('l\\', ''))    
                    if 'videos' in _json:
                        value = [i['url'] for i in _json['videos']]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value  

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
                soup.select_one("#delivery-message"),
                soup.select_one("#ddmDeliveryMessage")
                )
            if tag:
                source, value = tag

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = self.check_tag(
             soup.select_one("#olp-upd-new-freeshipping .a-color-base"),
             soup.select_one('#shippingMessageInsideBuyBox_feature_div'),
             soup.select_one(".a-size-base.a-color-secondary")
             )

            if tag:
                source, value = tag
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value 

    def _get_buybox(self, soup, template):
        buybox_result = deepcopy(template)

        try:
            buybox_tag = soup.select_one("#buybox") or soup.select_one(".a-box-group")

            if not buybox_tag:
                temp_tag = soup.select_one(".a-box")
                if 'a-alert' not in temp_tag.get('class'):
                    buybox_tag = temp_tag
            
            if buybox_tag:
                buybox_result['source'] = str(buybox_tag)

                # Extract text
                buybox_result['text'] = self.__get_sanitized_tag(buybox_tag)
                # Extract price and currency
                buybox_result["price"]["value"], buybox_result["price"]["currency"] = self.__get_buybox_price_and_currency(buybox_tag)
                # Extract delivery
                buybox_result["delivery"] = self.__get_buybox_delivery(buybox_tag)
                # Extract shipping
                buybox_result["shipping"] = self.__get_buybox_shipping(buybox_tag)
                # Extract availability
                buybox_result["availability"] = self.__get_buybox_availability(soup, buybox_tag)
                # Extract retailer
                buybox_result["retailer"] = self.__get_buybox_retailer(buybox_tag)
                # Extract used_price
                buybox_result["used_price"] = self.__get_buybox_used_price(soup, buybox_tag)
                # Extract new_price
                buybox_result["new_price"] = self.__get_buybox_new_price(soup, buybox_tag)
                # Extract cartable
                buybox_result["cartable"]['source'], buybox_result["cartable"]['value'] = self.__get_buybox_cartable(soup, buybox_tag)
                # Extract buyable
                buybox_result["buyable"]['source'], buybox_result["buyable"]['value'] = self.__get_buybox_buyable(soup, buybox_tag)
                # Extract add_on
                buybox_result["add_on"] = self.__get_buybox_add_on(buybox_tag)            
                
        except Exception as e:
            self.logger.exception(e)
            buybox_result['text'] = FailureMessages.BUYBOX_EXTRACTION.value

        return buybox_result

    def __get_buybox_price_and_currency(self, buybox_tag):
        price = Defaults.GENERIC_NOT_FOUND.value
        currency = 'EUR' # Defaulted

        try:
            tag = buybox_tag.select_one("#price_inside_buybox") or\
                        buybox_tag.select_one("#newBuyBoxPrice") or\
                        buybox_tag.select_one("#unqualifiedBuyBox .a-color-price")

            if not tag:
                tag = buybox_tag.select_one(".a-color-price")
                if tag:
                    if "a-size-medium" not in tag.get("class") or \
                    tag.parent.get("id") in ["availability", "outOfStock"]:
                        tag = None

            if tag:
                price = tag.get_text().strip()
                currency = 'EUR' # Defaulted
            
            if not bool(re.search('\d', price)):
                tag = buybox_tag.select_one('.a-price.a-text-price.a-size-medium span') or \
                    buybox_tag.select_one('.a-offscreen')

                if tag:
                    price = tag.get_text().strip()
                     
        except Exception as e:
            self.logger.exception(e)
            price = FailureMessages.BUYBOX_EXTRACTION.value
            currency = FailureMessages.BUYBOX_EXTRACTION.value

        return price, currency
    
    def __get_buybox_delivery(self, buybox_tag):
        delivery = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#ddmDeliveryMessage")
            if tag:
                delivery = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            delivery = FailureMessages.BUYBOX_EXTRACTION.value

        return delivery

    def __get_buybox_shipping(self, buybox_tag):
        shipping = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#price-shipping-message")
            if tag:
                shipping= tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            shipping = FailureMessages.BUYBOX_EXTRACTION.value

        return shipping
    
    def __get_buybox_availability(self, soup, buybox_tag):
        availability = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#availability") or buybox_tag.select_one("#outOfStock .a-text-bold") or soup.select_one("#availability")
                
            if tag:
                availability = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            availability = FailureMessages.BUYBOX_EXTRACTION.value

        return availability
    
    def __get_buybox_retailer(self, buybox_tag):
        retailer = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = buybox_tag.select_one("#merchant-info")
            if tag:
                if tag.select_one('#sellerProfileTriggerId'):
                    retailer = tag.select_one('#sellerProfileTriggerId').get_text().strip()
                else:
                    retailer = re.sub(r'[^\w ]+', '', tag.get_text()
                                                .split("por ")[-1].split(".")[0]).strip()
            elif buybox_tag.select_one('#sellerProfileTriggerId'):
                retailer = buybox_tag.select_one('#sellerProfileTriggerId').get_text().strip()
            else:
                retailer = "Amazon" # Defaulted in Ultima
            
        except Exception as e:
            self.logger.exception(e)
            retailer = FailureMessages.BUYBOX_EXTRACTION.value

        return retailer

    def __get_buybox_used_price(self, soup, buybox_tag):
        used_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #usedPrice") or soup.select_one('#usedOnlyBuybox span.offer-price')
            if tag:
                used_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            used_price = FailureMessages.BUYBOX_EXTRACTION.value

        return used_price
    
    def __get_buybox_new_price(self, soup, buybox_tag):
        new_price = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box #newBuyBoxPrice")
            if tag:
                new_price = tag.get_text().strip()
            
        except Exception as e:
            self.logger.exception(e)
            new_price = FailureMessages.BUYBOX_EXTRACTION.value

        return new_price

    def __get_buybox_cartable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        cartable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-box .a-icon-cart")
            if tag:
                source = str(tag)
                cartable = str(True)
            else:
                cartable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            cartable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, cartable

    def __get_buybox_buyable(self, soup, buybox_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        buyable = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one(".a-icon-buynow")
            if tag:
                source = str(tag)
                buyable = str(True)
            else:
                buyable = str(False)
            
        except Exception as e:
            self.logger.exception(e)
            buyable = FailureMessages.BUYBOX_EXTRACTION.value

        return source, buyable

    def __get_buybox_add_on(self, buybox_tag):
        add_on = Defaults.GENERIC_NOT_FOUND.value

        try:
            tags = buybox_tag.select(".abbListItem")
            if tags:
                add_on = ", ".join([self.__get_sanitized_tag(tag) for tag in tags])
            
        except Exception as e:
            self.logger.exception(e)
            add_on = FailureMessages.BUYBOX_EXTRACTION.value

        return add_on 

    @staticmethod
    def check_tag(*tags, attrs=[]):
        for tag in tags:
            if not isinstance(tag, list):
                if tag and attrs:
                    for attr in attrs:
                        if tag.has_attr(attr):
                            if len(tag.get(attr)) > 0:
                                return str(tag), tag.get(attr)
                        else:
                            if len(tag.get_text().strip()) > 0:
                                return str(tag), tag.get_text().strip()
                elif tag:
                    return str(tag), tag.get_text().strip()
            else:
                if tag:
                    return tag
        return None

    def _build_spec(self, key_tag, val_tag):
        key_tag = [self._sanitize(tag) for tag in key_tag]
        val_tag = [self._sanitize(tag) for tag in val_tag]
        value = {" ".join(k.stripped_strings):" ".join(v.stripped_strings) for k,v in zip(key_tag, val_tag)}
        return str(key_tag+val_tag), value 
    
    def _sanitize(self, tag):
        for _script in tag.select('script'):
            _script.decompose()
        for _style in tag.select('style'):
            _style.decompose()
        return tag
    
    @staticmethod
    def __get_sanitized_tag(tag):
        tags_to_extract = ['script', 'style']

        for i in tags_to_extract:
            for j in tag.select(i):
                j.extract()

        return ' '.join(tag.stripped_strings)   