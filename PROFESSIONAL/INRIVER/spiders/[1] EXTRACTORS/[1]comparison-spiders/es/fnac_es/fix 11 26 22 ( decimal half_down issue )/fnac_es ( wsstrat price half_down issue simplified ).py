import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

import copy, math

class FnacEsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.fnac.es'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        _soup = self.download_from_tab(soup)
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup, _soup)

        # Extract price listed
        result["price_listed"]["source"], result["price_listed"]["value"] = self.get_price_listed(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup,_soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        return result
    
    def download_from_tab(self, soup):
        try:
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                _id = tag.get('data-tracking-prid')
            else:
                tag = soup.select_one('.f-productPage')
                if tag and tag.has_attr('data-prid'):
                    _id = tag.get('data-prid')
                else:
                    tag = soup.select_one('[rel="canonical"]')
                    if tag and tag.has_attr('href'):
                        _id = tag['href'].split('/')[-1]
            if _id:
                api_url = f'https://www.fnac.es/Nav/API/FnacOfferTab/{_id}/1?page=1'
                self._api_url = api_url
                _rawdata = self.downloader.do_get_download(api_url)

                if _rawdata:
                    if 'offerTab' in _rawdata:
                        _json = json.loads(_rawdata)
                        return BeautifulSoup(_json['offerTab'],'lxml')
                    elif 'buyBox' in _rawdata:
                        _json = json.loads(_rawdata)
                        return BeautifulSoup(_json['buyBox'],'lxml')
                    else:
                        return BeautifulSoup(_rawdata, "lxml")
                else:
                    return None
        except:
            return None

    def get_price(self, soup, _soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            # Requesting to get prices from left tab
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                if _soup:                    
                    if soup.select_one('.js-fnacTabContent.isActive') or soup.select_one('li.f-productOffers-tab.js-fnacOffersTab'):
                        tag = _soup.select_one('.f-priceBox .f-priceBox-price.f-priceBox-price--reco') #or soup.select_one('.f-priceBox .f-priceBox-price.f-priceBox-price--reco')
                        if tag:
                            source = str(self._api_url)
                            value = "".join(tag.stripped_strings)
                    else:
                        tag = soup.select_one('.f-priceBox .f-priceBox-price.f-priceBox-price--reco') or soup.select_one('.f-faPriceBox__price')
                        if tag:
                            source = str(tag)
                            value = "".join(tag.stripped_strings)
                else:
                    # For OOS products
                    if soup.select_one('.f-priceBox'): # FOR HANDLING HIDDEN PRICES IN PDP
                        tag = soup.find("script", text=re.compile('.*"@type":"Product".*'), attrs= {"type":"application/ld+json"})
                        if tag:
                            _json = json.loads(tag.get_text())

                            if _json and "offers" in _json.keys() and 'price' in _json['offers']:
                                source, value = str(tag), str(_json['offers']['price'])
                                
            # if Requesting to get prices from left tab return None
            else:
                tag = soup.select_one('span.f-faPriceBox__price.userPrice.js-ProductBuy-standardCheckable.checked')
                if tag:
                    source = str(tag)
                    value = "".join(tag.stripped_strings)
                
            # Removed this code for price that are not in pdp
            # else:
            #     # For OOS products
            #     if soup.select_one('.f-priceBox'): # FOR HANDLING HIDDEN PRICES IN PDP
            #         tag = soup.find("script", text=re.compile('.*"@type":"Product".*'), attrs= {"type":"application/ld+json"})
            #         if tag:
            #             _json = json.loads(tag.get_text())

            #             if _json and "offers" in _json.keys() and 'price' in _json['offers']:
            #                 source, value = str(tag), str(_json['offers']['price'])         

            if value:
                preserved_value = copy.deepcopy(value) # preserve the variable 'value' for ambiguity prevention during testing phase
                rounded_value = self.round_half_up(float(preserved_value), decimal=2) # first arg static, 2nd arg is keyword arg: adjust to target decimal place
                formatted_value = float("{0:.2f}".format(rounded_value)) # replace formatting e.g {0:.2f} format to hundredths place
                value = formatted_value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_price_listed(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('.f-priceBox-price.f-priceBox-price--old')
            
            if tag:
                source, value = str(tag), tag.get_text()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_LISTED_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'offers' in _json.keys() and 'priceCurrency' in _json['offers']:
                    source, value = str(tag), _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup,_soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            src = None
            stock = None
            
            # Requesting to get prices from left tab
            _id = None
            tag = soup.select_one('button.js-ProductBuy-add')
            if tag and tag.has_attr('data-tracking-prid'): 
                _id = tag.get('data-tracking-prid')
            else:
                tag = soup.select_one('.f-productPage')
                if tag and tag.has_attr('data-prid'):
                    _id = tag.get('data-prid')
                else:
                    tag = soup.select_one('[rel="canonical"]')
                    if tag and tag.has_attr('href'):
                        _id = tag['href'].split('/')[-1]
            if _soup and soup.select_one('li.f-productOffers-tab.js-fnacOffersTab'):
                api_url = f'https://www.fnac.es/Nav/API/FnacOfferTab/{_id}/1?page=1'
                # _rawdata = self.downloader.do_get_download(api_url)

                # if _rawdata:
                #     if '{"offerTab"' in _rawdata:
                #         _raw_json = json.loads(_rawdata)
                #         _soup = BeautifulSoup(_raw_json['offerTab'],'lxml')
                #     else:
                #         _soup = BeautifulSoup(_rawdata, "lxml")

                tag = _soup.select_one('.f-buyBox-availability')
                if tag:
                    src = str(api_url)
                    stock = " ".join(tag.stripped_strings)
            else:
                tag = soup.select_one('.f-buyBox-availability')
                if tag:
                    src = str(tag)
                    stock = " ".join(tag.stripped_strings)


            if not stock:
                tag = soup.find("script", text=re.compile("tc_vars*"))
                if tag:
                    tc_vars = re.search("({.*})", tag.get_text().strip())
                    if tc_vars:
                        tc_vars = json.loads(tc_vars.group(1))
                        if '103' in tc_vars['product_availability'] or '115' in tc_vars['product_availability']: 
                            src = str(tag)
                            stock = str(tc_vars['product_availability'])
                        elif '' in tc_vars['product_availability'] and '0' in str(tc_vars['product_pricemode']):
                            src = str(tag)
                            stock = str(tc_vars['product_pricemode'])
                        else:
                            tag = soup.find('span',{'class':'f-buyBox-availabilityStatus'})
                            if tag:
                                src = str(tag)
                                stock = tag.get_text().strip()
                            else:
                                tag = soup.find('script',{'type':'application/ld+json'})
                                if tag:
                                    data = json.loads(tag.get_text().strip())
                                    if data:
                                        src = str(tag)
                                        stock = str(data['offers']['availability'])

            #Temporary Fix, Overriding the value of stock to OOS for some item_ids
            tag_id = soup.select_one('script#digitalData')
            if tag_id:
                _json = json.loads(tag_id.get_text(strip=True))
                if 'page' in _json.keys() and 'pageInfo' in _json['page'] and 'pageID' in _json['page']['pageInfo']:
                    p_id = _json['page']['pageInfo']['pageID']
                    if p_id in ['a7405239','a7405238','a8919320','a8919323']:
                        stock = 'http://schema.org/outofstock'
                        src = str(tag)
            if stock:
                source = src
                value = stock

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _json = None
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(tag.get_text())

            if _json:
                reviews_source = str(tag)
                score_source = str(tag)
                if "aggregateRating" in _json.keys():
                    # Reviews
                    if "ratingCount" in _json["aggregateRating"].keys():
                        reviews_value = str(_json["aggregateRating"]["ratingCount"])

                    # Ratings
                    if "ratingValue" in _json["aggregateRating"].keys():
                        score_value = str(_json["aggregateRating"]["ratingValue"])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})
            
            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'name' in _json:
                    source, value = str(tag), _json['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or \
                soup.find('script', text=re.compile('.*"@type":"Product".*'))

            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'brand' in _json.keys() and 'name' in _json['brand']:
                    source, value = str(tag), _json['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            desc_list = []
            source_list = []
            _json = None
            
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"}) or soup.find("script", attrs= {"type":"application/ld+json"})

            if tag:
                _json = json.loads(tag.get_text())

                if _json and 'description' in _json:
                    source_list.append(str(tag))
                    desc_list.append(' '.join(re.sub('<[^>]+>', '', _json['description']).split())) 

            if value in ['', ' ', Defaults.GENERIC_NOT_FOUND.value]:

                tag = soup.select_one('.productStrate__raw.summaryStrate__raw')

                if tag:
                    source_list.append(str(tag))
                    desc_list.append(' '.join(tag.stripped_strings))

            try:
                tag = soup.select_one('.productStrate.brandWordStrate.js-expand script')

                if tag and tag.has_attr('src'):
                    url = tag['src']
                    res = self.downloader.do_get_download(url)

                    if res:
                        id_rgx = re.search("window.flixJsCallbacks.pid ='(.*)'", res)
                        dist_rgx = re.search("distributor: '(.*)'", res)

                        if id_rgx and dist_rgx:
                            _id = id_rgx.group(1)
                            dist = dist_rgx.group(1)  

                            api_url = 'https://media.flixcar.com/delivery/inpage/show/{}/fr/{}/json?&complimentary=0&type=.html'.format(dist, _id)     
                            _res = self.downloader.do_get_download(api_url)         

                            _json_data = json.loads(_res[1:][:-1])     
                            soup = BeautifulSoup(_json_data['html'], 'lxml')  
                            [i.decompose() for i in soup.select('style')]
                            [i.decompose() for i in soup.select('script')]     
                            source_list.append(api_url) 
                            desc_list.append(' '.join(soup.stripped_strings))                       

            except Exception as e:
                print(e)
                pass

            # tag = soup.select_one('.productStrate.includedAccessoriesStrate')

            # if tag:
            #     source_list.append(str(tag))
            #     desc_list.append(' '.join(tag.stripped_strings))            

            # if source_list and desc_list:
            #     source, value = ' '.join(source_list), ' '.join(desc_list)

            # else:
                # tag = soup.find("meta", { "name":"description" })
                # if tag and tag.has_attr('content'):
                #     source = str(tag)
                #     value = tag["content"]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
    
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tags = soup.select('.characteristicsStrate__lists .characteristicsStrate__list')
            if tags:
                source = str(tags)
                value = self._get_initial_specs(tags)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('.f-productVisuals-thumbnailsWrapper .f-productVisuals-thumbnail[data-type="image"]') or soup.select('div.f-productVisuals__thumbnail')

            if tags:
                source = str(tags)
                for img in tags:
                    if img.has_attr('data-src-zoom'):
                        image_url = img.get('data-src-zoom')
                        value.append(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            tags = soup.select('.f-productVisuals .VideoPlayerPlaylist-list .VideoPlayerPlaylist-item')

            if tags:
                source = str(tags)
                for vid in tags:
                    if vid.has_attr('data-video-0-url'):
                        video_url = vid.get('data-video-0-url')
                        value.append(video_url)
            
            if not value or len(value) <= 0:
                url = 'https://media.flixcar.com/delivery/js/hotspot/75/fr/ean/'

                ean_tag = soup.select_one('.f-productPage.clearfix.js-articleView')

                if ean_tag and ean_tag.has_attr('data-ean13'):
                    ean = ean_tag.get('data-ean13')
                    url += ean
                    res = self.downloader.do_get_download(url)

                    if res:
                        rgx = re.search(r"value='(\{.*\})'", res)
                        _json = json.loads(rgx.group(1).replace("\\", "")) if rgx else {}

                        if 'playlist' in _json:
                            source = url
                            value = ['https:'+i['file'] for i in _json['playlist'] if 'file' in i]

            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one("p.f-buyBox-shipping")
            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value       

    @staticmethod
    def _get_initial_specs(tags):
        result = dict()
        for nth in tags:
            rows = nth.select(".characteristicsStrate__item")

            for row in rows:
                try:
                    tag = row.select_one('.characteristicsStrate__term')
                    if tag:
                        key = " ".join(tag.stripped_strings)

                    tag = row.select_one('.characteristicsStrate__definition')
                    if tag:
                        data = " ".join(tag.stripped_strings)
                    
                    result[key] = data

                except:
                    continue

        return result

    def round_half_up(self, num, **kwargs): # adjust the decimal argument for the targetted decimal place 
        tens,half = 10.0, 0.5
        try:
            if kwargs['decimal'] in [0]:
                return math.floor(num + half)

            arg1 = math.floor(num * math.pow(tens, kwargs['decimal']) + half)  # mathematical fomula to resolve half_down issue
            arg2 = math.pow(tens, kwargs['decimal']) # target decimal place

            return arg1 / arg2 if (arg1 and arg2) else Defaults.GENERIC_NOT_FOUND.value
        except Exception as e:
            print('Error in: half_up() -> with message: ', e)