import json
import logging
import re
from bs4 import BeautifulSoup, Tag, NavigableString
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class LazadaComPhWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.lazada.com.ph'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        sku = self.get_sku(soup)
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup, sku)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup, sku)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result

    def get_sku(self, soup):
        sku = 0

        try:
            tag = soup.find('script', text = re.compile('pdpTrackingData'))
            if tag:
                script_start, script_end = tag.text.find('pdpTrackingData'), tag.text.find("pdpTrackingData = JSON.parse")
                _json = json.loads(('{' + tag.text[script_start+21:script_end-21]).replace('\\', "").replace('6.5"', '6.5'))
                if _json and _json['pdt_simplesku']:
                    sku = _json['pdt_simplesku']
        except Exception as e:
            self.logger.exception(e)
            sku = -1

        return sku

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]

                sku_start, sku_end = str_tag.find('skuInfos'), str_tag.find('Breadcrumb\\":[{')
                _json = json.loads(("{" + str_tag[sku_start-1:sku_end]).replace("\\","").replace('6.5" |', "6.5 |").replace(']}},"', "]}}}"))
                if _json and _json['skuInfos']['0']['price']['salePrice']['value']:
                    source = str_tag
                    value = _json['skuInfos']['0']['price']['salePrice']['value']
                    
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "")
                _json = json.loads(str_tag)
                if _json and _json['offers']['priceCurrency']:
                    source = str(tag)
                    value = _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "")
                _json = json.loads(str_tag)
                if _json and _json['offers']['availability']:
                    source = str(tag)
                    value = _json['offers']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5" 
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
       
        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "")
                _json = json.loads(str_tag)

                if _json:
                    if _json['aggregateRating']['ratingValue']:
                        score_source = str(tag)
                        score_value = _json['aggregateRating']['ratingValue']
                    if _json['review']:
                        reviews_source = score_source = str(tag)
                        reviews_value = len([x for x in _json['review']]) # dont count, use pdp value, optional: use reviews count
                
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1', class_=".page-title")
            if tag:
                source = str(tag)
                value = tag.get_text() # direct value
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "")
                _json = json.loads(str_tag)
                if _json and _json['brand']['name']:
                    source = str(tag)
                    value = _json['brand']['name']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "")
                _json = json.loads(str_tag)
                if _json and _json['description']:
                    source = str(str_tag)
                    value = _json['description']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
        
    def get_specifications(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                specs_start, specs_finish = str_tag.find('specification'), str_tag.find('tracking')
                _json = json.loads(("{" + str_tag[specs_start-1:specs_finish-3] + "}").replace("\\",""))
                if _json and _json['specifications'][str(sku)]['features']:
                    source = str(str_tag)
                    value = _json['specifications'][str(sku)]['features'] # checker # variant # find unique number based on PDP
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                dict = {}
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]

                skuGal_start, skuGal_end = str_tag.find("skuGalleries"), str_tag.find('"img\\"}]},\\"product\\"')
                _json = json.loads(('{' + str_tag[skuGal_start-2:skuGal_end+9] + '}').replace('\\', ""))
                if _json and _json['skuGalleries']['0']:
                    for i in range(0,len(_json['skuGalleries']['0'])):
                        if (_json['skuGalleries']['0'][i]['poster']):
                            dict.update({i:_json['skuGalleries']['0'][i]['poster']})
                    source = str(str_tag)
                    value = dict

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        return source, value

    def _get_shipping(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                deliveryOption_start, deliveryOption_end = str_tag.find('\\"deliveryOptions\\":{\\'), str_tag.find('warranties\\')
                _json = json.loads(('{' + str_tag[deliveryOption_start:deliveryOption_end-3] + '}').replace('\\', "").replace('<a href="',"").replace('">',"").replace("</a>", ""))
                

                # if _json and _json['deliveryOptions'][sku][]:

                temp = []
                if _json and _json['deliveryOptions']:
                    for values in _json['deliveryOptions']:
                        for index in _json['deliveryOptions'][values]:
                            if 'feeValue' in index:
                                temp.append(index['feeValue'])
                    value = temp[0]
                    source = str(str_tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value
        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                deliveryOption_start, deliveryOption_end = str_tag.find('\\"deliveryOptions\\":{\\'), str_tag.find('warranties\\')
                extracted_tag = ('{' + str_tag[deliveryOption_start:deliveryOption_end-3] + '}').replace('\\', "").replace('<a href="',"").replace('">',"").replace("</a>", "")                                
                _json = json.loads(extracted_tag)
                temp = []
                for values in _json['deliveryOptions']:
                    for index in _json['deliveryOptions'][values]:
                        if 'duringTime' in index:
                            value = index['duringTime']
                source = str(str_tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
         

    