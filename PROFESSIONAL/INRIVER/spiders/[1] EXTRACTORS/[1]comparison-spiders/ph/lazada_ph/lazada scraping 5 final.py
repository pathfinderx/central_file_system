import json
import logging
import re
from bs4 import BeautifulSoup, Tag, NavigableString
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class LazadaComPhWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.lazada.com.ph'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        _json_rawdata = json.loads(raw_data)
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        sku = _json_rawdata['sku']
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup, sku)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup, sku)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup, sku)

        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))
                if _json and 'offers' in _json.keys() and 'lowPrice' in _json.get('offers').keys():
                    source, value= str(tag), _json['offers']['lowPrice']
             
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))
                if _json and 'offers' in _json and 'priceCurrency' in _json.get('offers').keys():
                    source, value = str(tag), _json['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))
                if _json and 'offers' in _json.keys() and 'availability' in _json.get('offers').keys():
                    source, value= str(tag), _json['offers']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5" 
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
       
        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))

                if _json and 'aggregateRating' in _json.keys():
                    if 'ratingValue' in _json.get('aggregateRating').keys():
                        reviews_source, score_value = str(tag), _json['aggregateRating']['ratingValue']
                    if 'ratingCount' in _json.get('aggregateRating').keys():
                        score_source, reviews_value = str(tag), _json['aggregateRating']['ratingCount']
                
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1', class_=".page-title")
            if tag:
                source, value = str(tag), tag.get_text() # direct
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))
                if _json and 'brand' in _json.keys() and 'name' in _json.get('brand').keys():
                    source, value = str(tag), _json['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                _json = json.loads(tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", ""))
                if _json and 'description' in _json.keys():
                    source, value = str(tag), _json['description']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
        
    def get_specifications(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                specs_start, specs_finish = str_tag.find('specification'), str_tag.find('tracking')
                _json = json.loads(("{" + str_tag[specs_start-1:specs_finish-3] + "}").replace("\\",""))
                
                if _json and 'specifications' in _json and str(sku) in _json.get('specifications').keys():
                    if 'features' in _json.get('specifications').get(str(sku)).keys():
                        source, value = str(str_tag), _json['specifications'][str(sku)]['features'] # checker # variant # find unique number based on PDP
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]

                skuGal_start, skuGal_end = str_tag.find("skuGalleries"), str_tag.find('"img\\"}]},\\"product\\"')
                _json = json.loads(('{' + str_tag[skuGal_start-2:skuGal_end+9] + '}').replace('\\', ""))

                if _json and 'skuGalleries' in _json and '0' in _json.get('skuGalleries').keys():
                    for i in range(0,len(_json['skuGalleries']['0'])):
                        if 'src' in _json['skuGalleries']['0'][i]:
                            value.update({i:_json['skuGalleries']['0'][i]['src']})
                    source = str(str_tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        return source, value

    def _get_shipping(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                deliveryOption_start, deliveryOption_end = str_tag.find('\\"deliveryOptions\\":{\\'), str_tag.find('warranties\\')
                _json = json.loads(('{' + str_tag[deliveryOption_start:deliveryOption_end-3] + '}').replace('\\', "").replace('<a href="',"").replace('">',"").replace("</a>", ""))
                
                if _json and 'deliveryOptions' in _json and str(sku) in _json.get('deliveryOptions').keys():
                    if _json['deliveryOptions'][str(sku)][0]['feeValue']:
                        source, value = str(tag), _json['deliveryOptions'][str(sku)][0]['feeValue']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value
        return source, value

    def _get_delivery(self, soup, sku):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('var __hasSSR__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                deliveryOption_start, deliveryOption_end = str_tag.find('\\"deliveryOptions\\":{\\'), str_tag.find('warranties\\')
                _json = json.loads(('{' + str_tag[deliveryOption_start:deliveryOption_end-3] + '}').replace('\\', "").replace('<a href="',"").replace('">',"").replace("</a>", ""))
                
                if 'deliveryOptions' in _json and str(sku) in _json.get('deliveryOptions').keys():
                    if 'duringTime' in _json['deliveryOptions'][str(sku)][0]:
                        source = str(str_tag)
                        value = _json['deliveryOptions'][str(sku)][0]['duringTime']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value
         

    