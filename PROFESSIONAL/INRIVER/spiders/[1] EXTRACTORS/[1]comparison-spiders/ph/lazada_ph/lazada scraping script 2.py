import json
import logging
import re
from bs4 import BeautifulSoup, Tag, NavigableString
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class LazadaComPhWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.lazada.com.ph'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")
        
        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications 
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)


        return result

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]

                sku_start, sku_end = str_tag.find('skuInfos'), str_tag.find('Breadcrumb\\":[{')
                sku_extracted = ("{" + str_tag[sku_start-1:sku_end]).replace("\\","").replace('6.5" |', "6.5 |").replace(']}},"', "]}}}")
                _json = json.loads(sku_extracted)
                value = _json['skuInfos']['0']['price']['salePrice']['value']
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "").strip()
                json_tag = json.loads(str_tag)
                source = str(tag)
                value = json_tag['offers']['priceCurrency']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "").strip()
                json_tag = json.loads(str_tag)
                availability_string = json_tag['offers']['availability']
                substring_start = availability_string.find('org/')
                source = str(tag)
                value = availability_string[substring_start+4:]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5" 
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value
       
        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "").strip()
                json_tag = json.loads(str_tag)
                score_source = str(tag)
                score_value = json_tag['aggregateRating']['ratingValue']
                reviews_source = score_source = str(tag)
                reviews_value = len([x for x in json_tag['review']])
                
        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('h1', class_=".page-title")
            if tag:
                title = tag.get_text()
                source = str(tag)
                value = title
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "").strip()
                json_tag = json.loads(str_tag)
                source = str(tag)
                value = json_tag['brand']['name']


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('https://schema.org'))
            if tag:
                str_tag = tag.text.replace("\\n", "").replace('\\"','"').replace('\\\\"',"''").replace("\'\'", "").replace("\\", "").strip()
                json_tag = json.loads(str_tag)
                source = str(str_tag)
                value = json_tag['description']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value
        
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]
                
                specs_start, specs_finish = str_tag.find('specification'), str_tag.find('tracking')
                spec_extracted = ("{" + str_tag[specs_start-1:specs_finish-3] + "}").replace("\\","")
                _json = json.loads(spec_extracted)
                source = str(spec_extracted)
                value = _json['specifications']['10534482395']['features']
                       
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script', text = re.compile('window.__i18n__'))
            if tag:
                dict = {}
                script_start, script_end = tag.text.find('var __moduleData__'), tag.text.find('var __googleBot__')
                str_tag = tag.text[script_start+21:script_end]

                skuGal_start, skuGal_end = str_tag.find("skuGalleries"), str_tag.find('"img\\"}]},\\"product\\"')
                skuGal_extracted = ('{' + str_tag[skuGal_start-2:skuGal_end+9] + '}').replace('\\', "")
                _json = json.loads(skuGal_extracted)
                for i in range(0,len(_json['skuGalleries']['0'])):
                    dict.update({i:_json['skuGalleries']['0'][i]['poster']})
                source = str(skuGal_extracted)
                value = dict

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        return source, value

    def _get_shipping(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value
         

    