from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ShopeePhDownloadStrategy(DownloadStrategy):

	def __init__(self, requester):
		self.requester = requester

	def download(self, url, timeout=10, headers=None, cookies=None, data=None):
		shopid = url.split('.')[-2]

		temp = url.split('?')[0]
		itemid = temp.split('.')[-1]

		url = f'https://shopee.ph/api/v4/item/get?itemid={itemid}&shopid={shopid}'

		assert isinstance(url, str)
		assert isinstance(timeout, int)
		
		if not isinstance(headers, dict):
			headers = {
				'Accept': '*/*',
				'Accept-Encoding': 'gzip, deflate',
				'Accept-Language': 'en-US,en;q=0.9',
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
						'Chrome/86.0.4240.198 Safari/537.36'
			}

		if not isinstance(cookies, dict):
			cookies = None

		try:
			if not data:
				response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
			else:
				response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

			status_code = response.status_code

			if status_code in [200, 201]:
				is_redirect = False

				for item in response.history:
					if item.status_code in [302, 307]:
						is_redirect = True
						status_code = item.status_code
						break

				if not is_redirect:
					if '"item":null' in response.text:
						raise DownloadFailureException('Download failed - may need auto rerun')
					else:
						return response.text

			raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

		except:
			raise DownloadFailureException('Download failed - Unhandled Exception')
		
		
	def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(url, str)
		assert isinstance(timeout, int)
		
		if not isinstance(headers, dict):                         
			headers = {
				'Accept': '*/*',
				'Accept-Encoding': 'gzip, deflate',
				'Accept-Language': 'en-US,en;q=0.9',
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
						'Chrome/86.0.4240.198 Safari/537.36'
			}

		if not isinstance(cookies, dict):
			cookies = None

		try:
			if not data:
				response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
			else:
				response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

			status_code = response.status_code

			if status_code in [200, 201]:
				is_redirect = False

				for item in response.history:
					if item.status_code in [302, 307]:
						is_redirect = True
						status_code = item.status_code
						break

				if not is_redirect:
					if '"item":null' in response.text:
						raise DownloadFailureException('Download failed - may need auto rerun')
					else:
						return response.text

			raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

		except:
			raise DownloadFailureException('Download failed - Unhandled Exception')
		