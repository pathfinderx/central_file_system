import json
import logging
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ShopeePhWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shopee.ph'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        _json = json.loads(raw_data)

        src_itemid = _json['data']['itemid']
        src_shopid = _json['data']['shopid']
        self.src = f'https://shopee.sg/api/v2/item/get?itemid={src_itemid}&shopid={src_shopid}'

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(_json)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(_json)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(_json)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(_json)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(_json)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(_json)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(_json)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(_json)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(_json)

        # Extract video urls
        # result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(_json)

        # Extract shipping
        result["shipping"]["source"], result["shipping"]["value"] = self._get_shipping(_json)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(_json)

        return result

    def get_price(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'item' in _json.keys() and 'price' in _json['item']:
                source = self.src
                value = str(_json['item']['price'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'item' in _json.keys() and 'currency' in _json['item']:
                source = self.src
                value = str(_json['item']['currency'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'item' in _json.keys() and 'stock' in _json['item']:
                source = self.src
                value = str(_json['item']['stock'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, _json):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'data' in _json and 'item_rating' in _json.get('data'):
                if 'rating_star' in _json['data']['item_rating']:
                    score_source, score_value = self.src, str(_json['data']['item_rating']['rating_star'])
                if 'rating_count' in _json['data']['item_rating'] and len(_json['data']['item_rating']['rating_count']) > 0:
                    reviews_source, reviews_value = self.src, str(_json['data']['item_rating']['rating_count'][0])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
           if _json and 'data' in _json and 'name' in _json.get('data'):
               source, value = self.src, _json['data']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value
    
    def get_brand(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'data' in _json and 'brand' in _json.get('data'):
                source, value = self.src, _json['data']['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_description(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'data' in _json and 'description' in _json.get('data'):
                source, value = self.src, _json['data']['description']
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        
        return source, value

    def get_specifications(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            if _json and 'data' in _json and 'description' in _json.get('data'):
                temp = _json['data']['description'].replace('\n', "").split('•')
                for content in temp:
                    print(content)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    def _get_image_urls(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if _json and 'item' in _json.keys() and 'images' in _json['item']:
                source = self.src
                for li in _json['item']['images']:
                    value.append('https://cf.shopee.sg/file/' + li)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list
        return source, value

    def _get_shipping(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'item' in _json.keys() and 'show_free_shipping' in _json['item']:
                source = self.src
                value = str(_json['item']['show_free_shipping'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SHIPPING_EXTRACTION.value

        return source, value

    def _get_delivery(self, _json):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _json and 'item' in _json.keys() and 'estimated_days' in _json['item']:
                source = self.src
                value = str(_json['item']['estimated_days'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value   
