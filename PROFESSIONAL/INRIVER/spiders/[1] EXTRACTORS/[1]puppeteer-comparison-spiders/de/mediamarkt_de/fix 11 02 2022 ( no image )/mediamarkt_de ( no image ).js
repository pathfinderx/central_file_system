// const puppeteer = require('puppeteer')
const puppeteer = require('puppeteer-extra')
const requestPromise = require('request-promise')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { UnhandledStrategyError } = require('../errors')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
// const puppeteerAfp = require('puppeteer-afp');

class MediamarktDeWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            var nbRequest = 0
            var contentLength = 0
            var encodedDataLength = 0
            var dataLength = 0

            puppeteer.use(StealthPlugin())

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                this.proxy.server = 'zproxy.lum-superproxy.io:22225'
                this.proxy.user = 'lum-customer-c_9c4e20a3-zone-be_unblocker-country-de-unblocker'
                this.proxy.pwd = '5ae5cfm7v22g'
                // this.proxy = ''

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${ this.proxy.server }`
                    args.push(`--proxy-server=${ this.proxy.server }`)
                }
                // console.log(this.proxy.user)
                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    // browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})

                    //Needed for unblocker -- comment when unblocker is removed
                    browser = await puppeteer.connect(
                        { 
                            browserWSEndpoint: browserEndpoint,
                            ignoreHTTPSErrors: true
                        }
                    )
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = (await browser.pages())[0]

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {} // Do nothing
                })

                // Bandwidth tracking
                await page.setCacheEnabled(false)
                await page.setBypassCSP(true)

                page._client.on('Network.dataReceived', (event) => {
                    dataLength += parseInt(event.dataLength)
                })

                page._client.on("Network.responseReceived", data => {
                    if (!data.response.url.startsWith('data:')) {
                        nbRequest++
                        if (typeof data.response.headers['content-length'] !== 'undefined') contentLength += parseInt(data.response.headers['content-length'])
                    }
                })

                page._client.on("Network.loadingFinished", data => {
                    if (typeof data.encodedDataLength !== 'undefined') encodedDataLength += parseInt(data.encodedDataLength)
                })

                //temp user agent
                userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/105.0.0.0 Safari/537.36'

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                    console.log('[*] Proxy authentication done')
                }
                else if (this.proxy && this.proxy.user == '' && this.proxy.pwd == '') {
                    console.log('[*] No user pwd for proxy authentication')
                    await page.authenticate()
                } else {
                    console.log('[*] No proxy implemented.')
                }

                // Open URL in browser
                await page.goto(url, { timeout: timeout })

                // Set page view port size
                await page.setViewport({ width: 1200, height: 1200 })
                
                try {
                    // await page.waitForTimeout(3000)
                    await page.evaluate(function(){
                        let element = document.querySelector('button.privacy-layer__button--accept-all') || document.querySelector('button#pwa-consent-layer-accept-all-button')
                        
                        if (element) {
                            element.click()
                        }
                    })
                    await page.waitForSelector('script[type="application/ld+json"]')
                    
                }
                catch(e) {}  // Do nothing

                await page.waitForTimeout(11000)
                
                
                // Get script data
                let error_pdp = {'message': 'PDP Checker Error'}
                let error_captcha = {'message': 'Blocked By Captcha'}
                try{
                    
                    let err_pdp_checker = new UnhandledStrategyError(error_pdp)
                    let err_captcha_issue = new UnhandledStrategyError(error_captcha)
                    var scriptText = await page.evaluate(async (err_pdp_checker, err_captcha_issue) => {
                        var innerText = null
                        var element = document.querySelector('script[type="application/ld+json"]')
                        var captcha = document.querySelector('p[data-translate="why_captcha_detail"]')
                        
                        if (element) {
                            
                            innerText = element.innerText
                            
                        }else if (captcha){
                            
                            return JSON.stringify(err_pdp_checker.data)
                        }else {
                            
                            return JSON.stringify(err_captcha_issue.data)
                            
                        }
    
                        return innerText
                    }, err_pdp_checker, err_captcha_issue)
                }catch (e) {
                    console.log(e)

                }

                
                if (scriptText) {
                    var scriptData = JSON.parse(scriptText)
                    if (scriptData.error){
                        
                        throw new UnhandledStrategyError(scriptData.error)
                    }

                    if (scriptData) {
                        // Price
                        try {
                            if (scriptData.offers && scriptData.offers.price) {
                                result.price.source = scriptText
                                result.price.value = scriptData.offers.price.toString()
                            }
                        }
                        catch(e) {
                            result.price.source = FailureMessages.GENERIC_NOT_FOUND
                            result.price.value = FailureMessages.PRICE_EXTRACTION
                        }

                        // Currency
                        try {
                            if (scriptData.offers && scriptData.offers.priceCurrency) {
                                result.currency.source = scriptText
                                result.currency.value = scriptData.offers.priceCurrency
                            }
                        }
                        catch(e) {
                            result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                            result.currency.value = FailureMessages.CURRENCY_EXTRACTION
                        }

                        // Availability
                        try {
                            var availResult = await page.evaluate(async (FailureMessages) => {
                                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                    var innerText = FailureMessages.GENERIC_NOT_FOUND
                
                                    try {

                                        var element = document.querySelector('[data-product-online-status="NO_VALID_MP_OFFER_PRICE"] span')

                                        if (element) {
                                            outerHTML = element.outerHTML
                                            innerText = element.innerText
                                        }
        
                                        document.querySelectorAll('h4').forEach((a) => {
                                            if (a.innerText.indexOf('Dieser Artikel ist aktuell nicht verfügbar') > -1 || a.innerText.indexOf('Nicht verfügbar') > -1) {
                                                outerHTML = a.outerHTML
                                                innerText = a.innerText
                                                return
                                            }
                                        })

                                        if ( innerText == FailureMessages.GENERIC_NOT_FOUND) {
                                            var element = document.querySelector('div[data-test="pdp-product-not-available"]') || document.querySelector('div[data-product-online-status="NOT_IN_ASSORTMENT"]')

                                            if (element) {
                                                outerHTML = element.outerHTML
                                                innerText = element.innerText
                                            }
                                            else {
                                                element = document.querySelector('#pdp-add-to-cart-button[disabled]')
                                                if (element) {
                                                    outerHTML = element.outerHTML
                                                    innerText = "dieser artikel ist aktuell nicht verfügbar." // Static since no OOS indicator
                                                }
                                            }
                                        }
                                        if ( innerText == FailureMessages.GENERIC_NOT_FOUND){
                                            var element = document.querySelector('span[class="StyledInfoTypo-sc-1jga2g7-0 eeCCqM StyledAvailabilityTypo-sc-901vi5-7 egVdxU"]')
                                            
                                            if (element){
                                                outerHTML = element.outerHTML
                                                innerText = element.innerText
                                            }
                                        }

                                        if ( innerText == FailureMessages.GENERIC_NOT_FOUND){
                                            var element = document.querySelector('[class="StyledHeadline-hcrbcp-1 jeJdem"] span')
                                            
                                            if (element){
                                                outerHTML = element.outerHTML
                                                innerText = element.innerText
                                            }
                                        }

                                        if ( innerText == FailureMessages.GENERIC_NOT_FOUND){
                                            var element = document.querySelector('[class="StyledInfoTypo-sc-1jga2g7-0 jlGGjh"]')
                                            
                                            if (element){
                                                outerHTML = element.outerHTML
                                                innerText = element.innerText
                                            }
                                        }
                                    }
                                    catch(e) {
                                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                                    }
                                    
                                    return { outerHTML, innerText }
                                }, FailureMessages)
    
                            if (availResult.innerText != FailureMessages.GENERIC_NOT_FOUND) {
                                result.availability.source = availResult.outerHTML
                                result.availability.value = availResult.innerText
                            }
                            else if (scriptData.offers && scriptData.offers.availability) {
                                result.availability.source = scriptText
                                result.availability.value = scriptData.offers.availability

                                // For Handling https://prnt.sc/wmenrm which should be IS
                                if ('http://schema.org/OutOfStock' === result.availability.value){
                                    var stockResult = await page.evaluate(async (FailureMessages, result) => {
                                    
                                    try {
                                        var element = document.querySelector('span.Availabilitystyled__StyledAvailabilityTypo-sc-901vi5-7')
                
                                        if (element) {
                                            outerHTML = element.outerHTML
                                            innerText = element.innerText
                                        }
                                        else {
                                            outerHTML = result.availability.source
                                            innerText = result.availability.value
                                        }
                                    }
                                    catch(e) {
                                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                                    }
                                    
                                    return { outerHTML, innerText }

                                    }, FailureMessages, result)

                                    result.availability.source = stockResult.outerHTML
                                    result.availability.value = stockResult.innerText
                                }
                            }
                            else if (scriptData.offers && scriptData.offers[0].availability) {
                                result.availability.source = scriptText
                                result.availability.value = scriptData.offers[0].availability
                            }
                            else if('offers' in scriptData === false) {
                                var stockResult = await page.evaluate(async (FailureMessages, result) => {
                                    
                                try {
                                    var element = document.querySelector('span.Availabilitystyled__StyledAvailabilityTypo-sc-901vi5-7')
            
                                    if (element) {
                                        outerHTML = element.outerHTML
                                        innerText = element.innerText
                                    }
                                    else {
                                        outerHTML = result.availability.source
                                        innerText = result.availability.value
                                    }
                                }
                                catch(e) {
                                    outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                    innerText = FailureMessages.AVAILABILITY_EXTRACTION
                                }
                                
                                return { outerHTML, innerText }

                                }, FailureMessages, result)

                                result.availability.source = stockResult.outerHTML
                                result.availability.value = stockResult.innerText
                            }
                        }
                        catch(e) {
                            result.availability.source = FailureMessages.GENERIC_NOT_FOUND
                            result.availability.value = FailureMessages.AVAILABILITY_EXTRACTION
                        }

                        // Rating
                        result.rating.score.max_value = "5" // Hardcoded 5 star based rating system

                        try {
                            if (scriptData.aggregateRating && scriptData.aggregateRating.ratingValue) {
                                result.rating.score.source = scriptText
                                result.rating.score.value = scriptData.aggregateRating.ratingValue.toString()
                            }
                        }
                        catch(e) {
                            result.rating.score.source = FailureMessages.GENERIC_NOT_FOUND
                            result.rating.score.value = FailureMessages.RATING_EXTRACTION
                        }
                        
                        // Reviews
                        try {
                            if (scriptData.aggregateRating && scriptData.aggregateRating.ratingCount) {
                                result.rating.reviews.source = scriptText
                                result.rating.reviews.value = scriptData.aggregateRating.ratingCount.toString()
                            }
                        }
                        catch(e) {
                            result.rating.reviews.source = FailureMessages.GENERIC_NOT_FOUND
                            result.rating.reviews.value = FailureMessages.REVIEWS_EXTRACTION
                        }

                        // Brand
                        try {
                            if (scriptData.brand && scriptData.brand.name) {
                                result.brand.source = scriptText
                                result.brand.value = scriptData.brand.name
                            }
                        }
                        catch(e) {
                            result.brand.source = FailureMessages.GENERIC_NOT_FOUND
                            result.brand.value = FailureMessages.BRAND_EXTRACTION
                        }

                        // Title
                        try {
                            if (scriptData.name) {
                                result.title.source = scriptText
                                result.title.value = scriptData.name
                            }
                        }
                        catch(e) {
                            result.title.source = FailureMessages.GENERIC_NOT_FOUND
                            result.title.value = FailureMessages.TITLE_EXTRACTION
                        }
                    }
                }
                // check tag if this item is permanently out of stock force price to -1
                if (result.price.value && result.availability.value == 'http://schema.org/OutOfStock'){
                    var priceResult = await page.evaluate(async (FailureMessages) => {
                        //var check_tag = document.querySelector('[class="BaseTypo-sc-1jga2g7-0 izkVco StyledInfoTypo-sc-1jga2g7-1 ecJNXP"]')
                        //if (check_tag){
                            //var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            //var innerText = FailureMessages.GENERIC_NOT_FOUND
                        //} 
                        var check_tag_base = document.querySelector(' div[class="StyledBox-sc-1vld6r2-0 foKbNS"] > div[class="StyledBox-sc-1vld6r2-0 goTwsP"] > span') || document.querySelector('.BrandedPriceFlexWrapper-sc-1r6586o-5')
                        if (check_tag_base){
                            outerHTML = check_tag_base.outerHTML
                            innerText = check_tag_base.innerText
                        } else {
                            var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            var innerText = FailureMessages.GENERIC_NOT_FOUND
                        }
                        return { outerHTML, innerText }
                    }, FailureMessages)
                    result.price.source = priceResult.outerHTML
                    result.price.value = priceResult.innerText
                }

                // If price is not found in the application/ld+json
                if (result.price.value == FailureMessages.GENERIC_NOT_FOUND) {
                    var priceResult = await page.evaluate(async (FailureMessages) => {
                        var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        var innerText = FailureMessages.GENERIC_NOT_FOUND
    
                        try {
                            var element = document.querySelector('span[font-family="price"]')
                            
                            if (element) {
                                element = element.parentElement
                            }

                            if (!element){
                                element = document.querySelector('div[data-test="mms-product-price"] div[display="flex"] span[class^="ScreenreaderTextSpan"]')
                            }
    
                            if (element) {
                                outerHTML = element.outerHTML
                                innerText = element.innerText
                            }


                        }
                        catch(e) {
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.PRICE_EXTRACTION
                        }
                        
                        return { outerHTML, innerText }
                    }, FailureMessages)
    
                    result.price.source = priceResult.outerHTML
                    result.price.value = priceResult.innerText
                }
       

                // If availability is not found in the application/ld+json
                if (result.availability.value == FailureMessages.GENERIC_NOT_FOUND) {
                    var stockResult = await page.evaluate(async (FailureMessages) => {
                        var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        var innerText = FailureMessages.GENERIC_NOT_FOUND
    
                        try {
                            var element = document.querySelector('div[data-test="pdp-product-not-available"]')
    
                            if (element) {
                                outerHTML = element.outerHTML
                                innerText = element.innerText
                            }
                        }
                        catch(e) {
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.PRICE_EXTRACTION
                        }
                        
                        return { outerHTML, innerText }
                    }, FailureMessages)
    
                    result.availability.source = stockResult.outerHTML
                    result.availability.value = stockResult.innerText
                }

                // returning PRICE value even if the item is permanently OOS. price and stocks force to -1 
                var oosTag = await page.evaluate(async (FailureMessages) => {
                    var price_pdp_tag = document.querySelector(' div[class="StyledBox-sc-1vld6r2-0 foKbNS"] > div[class="StyledBox-sc-1vld6r2-0 goTwsP"] > span') || document.querySelector('.BrandedPriceFlexWrapper-sc-1r6586o-5')
                    var checkAvailabilityTag = document.querySelector('.StyledHeadline-sc-1k9glgd-1.eslmbE')
                    var checkMarketTag = document.querySelector('[class="BaseTypo-sc-1jga2g7-0 izkVco StyledInfoTypo-sc-1jga2g7-1 ecJNXP"]')
                    if (!checkMarketTag || checkMarketTag.textContent.trim() != 'Dieser Artikel könnte in unseren Märkten verfügbar sein'){
                        if (checkAvailabilityTag && checkAvailabilityTag.textContent.trim() == 'Dieser Artikel ist dauerhaft ausverkauft' || 
                        checkAvailabilityTag && checkAvailabilityTag.textContent.trim() == 'Dieser Artikel ist bald wieder verfügbar' && !price_pdp_tag){
                            
                            var outerHTMLOosTag = FailureMessages.GENERIC_NOT_FOUND
                            var innerTextOosTag = FailureMessages.GENERIC_NOT_FOUND
                        }
                    }
                    return { outerHTMLOosTag, innerTextOosTag }
                }, FailureMessages)
                if(oosTag.outerHTMLOosTag && oosTag.innerTextOosTag){
                    result.price.source = oosTag.outerHTMLOosTag
                    result.price.value = oosTag.innerTextOosTag
                    result.availability.source = oosTag.outerHTMLOosTag  
                    result.availability.value = 'http://schema.org/OutOfStock'
                }

                // If title is not found in the application/ld+json
                if (result.title.value == FailureMessages.GENERIC_NOT_FOUND) {
                    var titleResult = await page.evaluate(async (FailureMessages) => {
                        var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        var innerText = FailureMessages.GENERIC_NOT_FOUND
    
                        try {
                            var element = document.querySelector('h1[color="black"]')
    
                            if (element) {
                                outerHTML = element.outerHTML
                                innerText = element.innerText
                            }
                        }
                        catch(e) {
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.TITLE_EXTRACTION
                        }
                        
                        return { outerHTML, innerText }
                    }, FailureMessages)
    
                    result.title.source = titleResult.outerHTML
                    result.title.value = titleResult.innerText
                }

                // Description
                var descResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var innerText = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var element = document.querySelector('[data-test="mms-accordion-description"]')

                        // element.childNodes.forEach(c=>{
                        //     c.childNodes.forEach(cn=>{
                        //         if(cn.getAttribute('class').includes('mms-select__pdp-special-gallery')){
                        //             cn.innerText = ''
                        //         }
                        //     })
                        // });

                        if (element) {
                            outerHTML = element.outerHTML
                            innerText = element.innerText
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                    }
                    
                    return { outerHTML, innerText }
                }, FailureMessages)

                result.description.source = descResult.outerHTML
                result.description.value = descResult.innerText

                // Get script data
                var preloadedScript = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var innerText = null

                    document.querySelectorAll('script').forEach((script) => {
                        if (script.innerText.indexOf('__PRELOADED_STATE__') > -1 ) {
                            innerText = script.innerText
                            outerHTML = script.outerHTML
                            return
                        }
                    }) 
                    return { outerHTML, innerText }

                }, FailureMessages)

                // Specifications
                if (preloadedScript) {
                    var valuesArray = {}

                    var _rgx = preloadedScript.innerText.match('({.*})')
                    if (_rgx){
                        try{
                            var jsonData = _rgx[1].replace(':new[\s\S]+?,', ":null,")
                            jsonData = jsonData.replace(':undefined', ':null').replace(':undefined}', ':null}')

                            var _json = JSON.parse(jsonData)

                            if (_json){
                                for ( var keys in _json['apolloState']) {
                                    if (keys.indexOf('GraphqlProduct:') > -1){
                                        _json['apolloState'][keys]['featureGroups'].forEach((specGroup) => {
                                            specGroup['features'].forEach((specs) => {
                                                if (!specs['unit']){
                                                    valuesArray[specs['name']] = specs['value']
                                                }else{
                                                    valuesArray[specs['name']] = specs['valuesAndUnits'][0]['value'] + " " + specs['valuesAndUnits'][0]['unit']
                                                }
                                            })
                                        })
                                    }
                                }

                                result.specifications.source = preloadedScript.outerHTML
                                result.specifications.value = valuesArray
                            }
                        }
                        catch(e){
                            //Do nothing
                        }
                    }
                }

                // Images
                if (preloadedScript){
                    var valuesArray = []

                    var _rgx = preloadedScript.innerText.match('({.*})')
                    if (_rgx){
                        try{
                            var jsonData = _rgx[1].replace(':new[\s\S]+?,', ":null,")
                            jsonData = jsonData.replace(':undefined', ':null').replace(':undefined}', ':null}')
                            var _json = JSON.parse(jsonData)

                            if (_json) {
                                for (var keys in _json['apolloState']) {
                                    if (keys.indexOf('GraphqlProduct:') > -1) {
                                        if (_json['apolloState'][keys].hasOwnProperty('assetProductImages')) {
                                            for (var img in _json['apolloState'][keys]['assetProductImages']) {
                                                if (_json['apolloState'][keys]['assetProductImages'].hasOwnProperty(img)) {
                                                    valuesArray.push(_json['apolloState'][keys]['assetProductImages'][img]['link'])
                                                }
                                            }
                                        } else if (_json['apolloState'][keys].hasOwnProperty('assets')) {
                                             if (['Product Image', 'Lifestyle Image', 'Detail View'].includes(_json['apolloState'][keys]['assets'][img]['usageType'])) {
                                                if(!valuesArray.includes(_json['apolloState'][keys]['assets'][img]['link'])) {
                                                    valuesArray.push(_json['apolloState'][keys]['assets'][img]['link'])
                                                }
                                            }
                                        }
                                    }
                                }

                                result.image_urls.source = preloadedScript.outerHTML
                                result.image_urls.value = valuesArray
                            }
                        }
                        catch(e){}  // Do nothing
                    }
                }

                // Videos
                var html = await page.content()

                try {
                    var matches = html.match('\{\\\".*(http.*api_token=.*?)\",')
                    
                    if (matches && matches.length > 1) {
                        var preloadURL = matches[1].replace(/u002F/g, '').replace(/\\/g, '/')

                        var downloadResult = await this.downloadVideos(preloadURL)

                        if (downloadResult.values && downloadResult.values.length > 0) {
                            result.video_urls.source = downloadResult.source
                            result.video_urls.value = downloadResult.values
                        }
                        else {
                            result.video_urls.source = FailureMessages.GENERIC_NOT_FOUND
                            result.video_urls.value = result.video_urls.value
                        }
                    }
                }catch(e) {
                    result.video_urls.source = FailureMessages.GENERIC_NOT_FOUND
                    result.video_urls.source = FailureMessages.VIDEO_URLS_EXTRACTION
                }

                // Delivery
                var deliveryResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var innerText = FailureMessages.GENERIC_NOT_FOUND
                    var sources = []
                    var values = ""

                    try {
                        document.querySelectorAll('div[data-test*="mms-delivery"]').forEach((div) => {
                            sources.push(div.outerHTML)
                            values = values + div.innerText + " "
                        })

                        if (values.length > 0) {
                            outerHTML = JSON.stringify(sources)
                            innerText = values
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                    }
                    
                    return { outerHTML, innerText }
                }, FailureMessages)

                result.delivery.source = deliveryResult.outerHTML
                result.delivery.value = deliveryResult.innerText

                let totalMegabytes = dataLength / 1048576
                console.log(`Total:  ${totalMegabytes}`)
                
                resolve({ html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes })
            } 
            catch (err) {
                console.error(err)
                let totalMegabytes = dataLength / 1048576
                reject({ error: err, actualProxyIP: actualProxyIP, totalMegabytes: totalMegabytes })
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }

    downloadVideos(url) {
        return new Promise(async (resolve, reject) => {
            requestPromise({
                url: url,
                rejectUnauthorized: false,
                proxy: `http://${this.proxy.user}:${this.proxy.pwd}@${this.proxy.server}`,
            })
                .then((source) => {
                    var values = []
                    var jsonList = JSON.parse(source)
    
                    if (jsonList.length > 0) {
                        var jsonData = jsonList[0]
    
                        if (jsonData.videos) {
                            var videos = jsonData.videos

                            for (var i = 0; i < videos.length; i++) {
                                var video = videos[i]
    
                                if (video.links) {
                                    var links = video.links
    
                                    for (var j = 0; j < links.length; j++) {
                                        var link = links[j]
    
                                        if (link.name && link.name == 'thumb' && link.location) {
                                            values.push(link.location.replace('thumb', 'vm'))
                                        }
                                    }
                                }
                            }
                        }
                    }
    
                    resolve({ source, values })
                })
                .catch((err) => {
                    reject({error: err })
                })
        })
    }
}

module.exports = MediamarktDeWebsiteStrategy