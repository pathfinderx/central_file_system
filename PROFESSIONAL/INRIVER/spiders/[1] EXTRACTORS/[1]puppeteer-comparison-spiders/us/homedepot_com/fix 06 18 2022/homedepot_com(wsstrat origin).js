const puppeteer = require('puppeteer-extra')
// const puppeteer = require('puppeteer')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')

class LowesComWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    // if prices are inaccurate per validator, use puppeteer-extra
    // and StealthPlugin

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            puppeteer.use(StealthPlugin())

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }
                // Set page view port size
                // await page.setViewport({width: 1920, height: 1080})

                // Open URL in browser

                // const geoLoc = {latitude: 34.0477436, longitude:-118.33668 }
                // const cookie = {"name":"region", "value":"central"}
                // await page.setGeolocation(geoLoc)
                
                /*
                await page.goto(url, {
                    timeout: timeout ,
                    waitUntil: 'networkidle2'
                })
                */
                // Open Homepage in browser to set location
                await page.goto(url, {
                    'waitUntil': 'networkidle0',
                    timeout: (timeout * 2) // triple the standard timeout
                })

                // selection location

                // let location = "Mid city Los Angeles" 
                //let zip = ""
                // let storeId=  'store-2714'
                /*
                try {
                    // Perform set location

                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location) 

                    // Check if the location is not correct
                    if (location.toLowerCase() != "Mid-city Los Angeles Lowe's".toLowerCase()){

                        // Click the element so the modal will pop up
                        await search_location.click()

                        // Wait for the modal to pop up
                        await page.waitForSelector('[class*="ModalWrapper-sc"]')

                        try{
                            var clearTextButton = await page.waitForSelector('.modal-handler .icon-close-circle-filled');
                            await clearTextButton.click()
                        }
                        catch(e){
                            //Do nothing
                        }
                        

                        // After the modal pops up, search for the location using zip as input
                        await page.type(".modal-handler input.textinput", zip)  //(".modal-header-title input.textinput", zip)  

                        await page.waitFor(5000)

                        // Click search button after input
                        var submitButton = await page.waitForSelector('.modal-handler button[type="submit"]') //(".modal-header-title button")
                        await submitButton.click()

                        // Wait to load
                        await page.waitFor(12000)
                        

                        // After the location appeared, wait and click the button of the 1st li element item 
                        await page.waitForSelector('button.met-sl-set-as-my-store') //page.waitForSelector('button[data-linkid="shop-this-store"]')
                        var shopButton = await  page.waitForSelector('button.met-sl-set-as-my-store') //page.waitForSelector('button[data-linkid="shop-this-store"]')
                        await shopButton.click()
                        
                    }

                }
                catch (error) {
                    console.log(error)
                }
                */
                
                /*
                try {
                    // Refresh the page
                    await page.reload({waitUntil:"networkidle2"})
                    
                    // Sanity check    
                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location)
                    console.log(`[*] Changed Zip to ${location}`)

                    
                } catch (error) {
                    console.log(error)
                    // Sanity check    
                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location)
                    console.log(`[*] Changed Zip to ${location}`)                    
                    
                    // do nothing
                }
                */
                
                // NEED TO SCROLLDOWN TO LOAD DATA FROM RETAILER
                //await page.waitForTimeout(10000)
                //await page.evaluate('window.scrollTo(0, document.body.scrollHeight)')
  

                // add wait timer to avoid errors
                // await page.waitForTimeout(7500)

                // let html = await page.content()
                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */
                await page.waitForTimeout(3500)
                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = document.querySelector('div[class="price-format__large price-format__main-price"]');
                        if (priceElement){
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('h1.product-details__title')
                        if (titleElement) {
                            innerText = titleElement.innerHTML.trim()
                            outerHTML = titleElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('span[class="product-details__brand--link"]');
                        if (brandElement){
                            innerText = brandElement.textContent.trim()
                            branouterHTMLdElement = brandElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                let currencyResult = await page.evaluate(function(FailureMessages) {
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let currencyElement = document.querySelector('span[class="price-format__large-symbols"]');
                        if (currencyElement){
                            innerText = currencyElement.textContent.trim()
                            outerHTML = currencyElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.CURRENCY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.currency.value = currencyResult.innerText
                result.currency.source = currencyResult.outerHTML
                
                // To load missing descript and specs
                async function autoScroll(page){
                    await page.evaluate(async () => {
                        await new Promise((resolve, reject) => {
                            var totalHeight = 0;
                            var distance = 100;
                            var timer = setInterval(() => {
                                var scrollHeight = document.body.scrollHeight;
                                window.scrollBy(0, distance);
                                totalHeight += distance;
                
                                if(totalHeight >= scrollHeight - window.innerHeight){
                                    clearInterval(timer);
                                    resolve();
                                }
                            }, 100);
                        });
                    });
                }
                await autoScroll(page);
                await page.waitForTimeout(3500)
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let description = []
                    let src = []

                    try{
                        var element = document.querySelector('div[class="zone-card col__12-12"]');
                        if (element) {
                            src.push(element.outerHTML);
                            description.push(element.innerText.trim());
                        }
                        var element = document.querySelector('div[class="grid desktop-content-wrapper__main-description"]')
                        if (element) {
                            src.push(element.outerHTML);
                            description.push(element.innerText.trim());
                        }
                        if (description.length > 0){
                            outerHTML = src.join(' ');
                            innerText = description.join(' ');
                        }
                    }catch(error){
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try{
                        
                        let keys = document.querySelectorAll('div[class="specs__cell specs__cell__label col__6-12"]');
                        let values = document.querySelectorAll('div[class="specs__cell col__6-12"]');  
                        if (keys.length == 0)
                            keys = document.querySelectorAll('[class="specifications__cell specifications__cell__label"]');
                            values = document.querySelectorAll('[class="specifications__cell"]');  

                        for (let i =0; i< keys.length; i++) {
                            //valuesMap[keys.length] = valuesMap[keys.length]
                            valuesMap[keys[i].innerHTML.trim()] = values[i].innerHTML.trim()
                        }
                    }catch(error){
                        valuesMap = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let script = document.querySelector('script[type="application/ld+json"]')
                        if (script) {
                            let toJson = script.innerHTML
                            outerHTML = script.outerHTML
                            if (toJson.length > 0){
                                json = JSON.parse(toJson)
                                imgs = json["image"]
                                imgs.forEach((img, index)=>{
                                    img = img.replace("_100", "_1000");
                                    if(!img.includes('http')){
                                        images.push(`https:${img}`)
                                    }
                                    else {
                                        images.push(img)
                                    }
                                })
                            }
                        } 
                    }catch(error){
                        images = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        //add cart button
                        var availability = document.querySelector('div.buybox__atc > div span.bttn__content');
                        if (availability) {
                            innerText = availability.innerText.trim()
                            outerHTML = availability.outerHTML
                        } else {
                            //notify me field
                            let currencyElement = document.querySelector('fieldset[class="fulfillment__unavailable--fieldset"]');
                            if (currencyElement){
                                innerText = 'out-stock'
                                outerHTML = currencyElement.outerHTML
                            } else {
                                let availability = document.querySelector('script[type="application/ld+json"]')
                                if (script) {
                                    let toJson = availability.innerHTML
                                    outerHTML = availability.outerHTML
                                    if (toJson.length > 0){
                                        json = JSON.parse(toJson)
                                        innerText = json["offers"]["availability"]
                                        if (innerText || innerText == undefined) {
                                            innerText = 'out-stock'
                                        }
                                    }
                                }
                            }
                        }  
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let maxRating = "5"
                    try{
                        let rating = document.querySelector('script[type="application/ld+json"]')
                        if (rating) {
                            let toJson = rating.innerHTML
                            outerHTML = rating.outerHTML
                            if (toJson.length > 0) {
                                json = JSON.parse(toJson)
                                innerText = json["aggregateRating"]["ratingValue"].toString()
                            }
                        }  
                    }catch(error){
                        innerText = error
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, maxRating, outerHTML }
                }, FailureMessages);

                if (ratingResult.innerText.constructor == Object && Object.keys(ratingResult.innerText).length === 0){
                    ratingResult.innerText = FailureMessages.GENERIC_NOT_FOUND
                }

                result.rating.score.max_value = ratingResult.maxRating
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let script = document.querySelector('script[type="application/ld+json"]')
                        if (script) {
                            let toJson = script.innerHTML
                            outerHTML = script.outerHTML
                            if (toJson.length > 0){
                                json = JSON.parse(toJson)
                                innerText = json["aggregateRating"]["reviewCount"]
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.GENERIC_NOT_FOUND
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // SHIPPING
                let shippingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let elements = document.querySelector('fieldset[class="fulfillment__unavailable--fieldset"]');
                        if (elements) {
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.GENERIC_NOT_FOUND
                        } else {
                            let elements = document.querySelector('a[class="card mobile--margin-right card-container card-selected card-enabled"]');
                            if (elements) {
                                outerHTML = elements.outerHTML
                                innerText = elements.innerText.trim()
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.SHIPPING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.shipping.value = shippingResult.innerText
                result.shipping.source = shippingResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let elements = document.querySelector('fieldset[class="fulfillment__unavailable--fieldset"]');
                        if (elements) {
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.GENERIC_NOT_FOUND
                        } else {
                            let elements = document.querySelector('a[class="card mobile--margin-right card-container u__text--primary card-unclickable"]');
                            if (elements) {
                                outerHTML = elements.outerHTML
                                innerText = elements.innerText.trim()
                            } 
                        }
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML
                
                // Videos 
                var vidResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []
                    var src = ''
                    let tempSource = ''
                    try { 
                        nextElement = document.querySelectorAll('div[class="mediagallery__thumbnail"]')    
                        nextElement.forEach((element) => {
                            var play_btn = element.querySelector('button > span.play-icon')
                            if (play_btn) {
                                tempSource = element.outerHTML
                                var src_pubid = element.querySelector('button > img').getAttribute("src")
                                var word_after = "pubId="
                                var pubid = src_pubid.slice(src_pubid.indexOf(word_after) + word_after.length);
                                var videoId = element.querySelector('button > img').getAttribute("alt")
                                if (videoId) {
                                    //https://players.brightcove.net/66036796001/4JE-ofbdg_default/index.html?videoId=3803299523001
                                    src = 'https://players.brightcove.net/'+ pubid +'/4JE-ofbdg_default/index.html?videoId='+ videoId
                                    valuesArray.push(src)
                                    outerHTML = outerHTML + ' ' + tempSource
                                }
                            }
                        })
                        
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        valuesArray = FailureMessages.VIDEO_URLS_EXTRACTION
                    }
                    return { outerHTML, valuesArray}
                }, FailureMessages)
                
                result.video_urls.source = vidResult.outerHTML
                result.video_urls.value = vidResult.valuesArray
                

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = LowesComWebsiteStrategy