const puppeteer = require('puppeteer')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { UnhandledStrategyError } = require('../errors')


class MicrosoftComEnUsWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout
                })

                let https_404_error = {'message': 'PDP Checker / 404 Error'}
                let redirected_to_category_error = {'message': 'PDP Checker / Redirected to category URL'}

                let isDeadLink = await page.evaluate(function(){
                        let message = document.querySelector('#mainContent .c-heading-3')

                        if (message){
                            if (message.innerText.toLowerCase().includes('page you requested cannot be found')){
                                return true
                            }
                        }
                        return false
                })

                let hasRedirectedToCategory = await page.evaluate(function(){
                    let message = document.querySelector('#productPlacementList')

                    if (message){
                        return true
                    }
                    return false
            })

                if (isDeadLink){
                    throw new UnhandledStrategyError(https_404_error)

                }else if (hasRedirectedToCategory){
                    throw new UnhandledStrategyError(redirected_to_category_error)

                }

                try {
                    try{
                        await page.waitForSelector('.modal-content')
                        await page.click('.modal-content [aria-label="Close dialog window"]')
                    }catch (error){
                        await page.waitForSelector('.sfw-dialog [aria-label="Close"]')
                        await page.click('.sfw-dialog [aria-label="Close"]')
                    }
                    console.log('Modal closed')
                    
                } catch (error) {
                    console.log('Modal element does not exist: '+ String(error))
                }
                // Variant
                let idSelector = null
                try {

                    if (variant.hasOwnProperty('screen_size') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage')) {
                        let variantScreenSize = variant.screen_size
                        let variantProcessor = variant.processor
                        let variantStorage = variant.storage
                        
                        await page.evaluate(function(variantScreenSize, variantProcessor, variantStorage){
                            let size = variantScreenSize
                            let processor = variantProcessor
                            let storage = variantStorage

                            let sizeBtnElement = document.querySelectorAll('.fc-richtext-button.c-button')
                            let text = null
                            if (size) {
                                text = size.toLowerCase()
                            
                            }else if (processor){
                                text = processor.toLowerCase()

                            }else {
                                text = storage.toLowerCase()
                            }
                            if (text){
                                for (let sizeBtn of sizeBtnElement){
                                    if (sizeBtn.innerText.toLowerCase().includes(text)) {
                                        sizeBtn.click();
                                        break;
                                    }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
                                        sizeBtn.click();
                                        break;
                                    }

                                }
                            }


                        }, variantScreenSize, variantProcessor, variantStorage)
                    }
                    if (variant.hasOwnProperty('color')) {
                        let variantColor = variant.color
                        
                        
                        await page.evaluate(function(variantColor){
                            let color = variantColor

                            let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
                            if (colorBtnElement.length <= 0){
                                colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')

                            }
                            for (let colorBtn of colorBtnElement){
                                if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                                    colorBtn.click()
                                    break
                                }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                                    colorBtn.click()
                                    break
                                }
                            }

                        }, variantColor)
                    }      
                    if (variant.hasOwnProperty('uid')){
                        console.log('Variant detected')
                        let variantId = variant.uid
                        // let idSelector = `.custom-control.custom-radio [for="${id}"]`
    
                        idSelector = await page.evaluate(function(variantId){
                            // ui indicator
                            let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')
    
    
                            let colorRadioBtnSelector = null
    
                            if (ui1IndicatorElement){
    
                                colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
                               
                            }else {
                                let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
                                let selector2 = `.tileproductplacement button[data-aid="${variantId}"]`

                                if (document.querySelector(selector1)){
                                    colorRadioBtnSelector = selector1
                                }else {
                                    colorRadioBtnSelector = selector2
                                }

                            }
                            
                            if (colorRadioBtnSelector){
                                let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
                                if (colorRadioBtnElement){
                                    colorRadioBtnElement.click()
                                    return colorRadioBtnSelector
                                }
    
                            }
                        
                            if (ui1IndicatorElement){
                                return null
                            }
                            return colorRadioBtnSelector
    
                        }, variantId)
     
                    }
                    

                } catch (error) {
                    
                    console.log(error)
                }
                


                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages, idSelector){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = null
                        let selectors = null
                        
                        if (idSelector){

                            let drawerElement = document.querySelector('#drawer-0') || document.querySelector('#drawer0 button')
                            if (drawerElement) {
                                if (drawerElement.hasAttribute('aria-expanded') && drawerElement.getAttribute('aria-expanded').toLowerCase() == 'false') {
                                    try{
                                        document.querySelector('#drawer-0 .drawertitle.c-glyph').click()
                                    }catch (error){
                                        drawerElement.click()
                                    }
                                    
                                }
                            }


                            selectors = [
                                `${idSelector} .module-standard-price span`,
                                `${idSelector} span.x-hidden-focus`,
                                `${idSelector} span:nth-child(4)`,
                                '.h3.font-weight-normal span:nth-of-type(2)'
                                
                            ]

                        }else{
                            selectors = [
                                '.col-lg-3.buy-box-right-col span.x-hidden-focus',
                                '.pb-4.pr-lg-4 p .font-weight-semibold',
                                'div[data-reactroot] div.pi-price-text span',
                                '.price-xaa-lc'
                            ]
                        }

                        for (let selector of selectors){
                            if (document.querySelector(selector)){
                                priceElement = document.querySelector(selector)
                                break;
                            }
                        }
                        if (priceElement) {
                            outerHTML = priceElement.outerHTML
                            innerText = priceElement.innerText.trim()
                        } else {
                            let elements = document.querySelectorAll('.tileproductplacement.mini button span')

                            if (elements.length > 0){
                                priceElement = elements[elements.length - 1]

                                if (priceElement){
                                    outerHTML = priceElement.outerHTML
                                    innerText = priceElement.innerText.trim()

                                }
                            }
                        } 

                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages, idSelector);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('#rootContainer_BuyBox .h2')

                        if (titleElement){
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('.glyph_text') || document.querySelector('span[itemprop="name"]');

                        if (brandElement){
                            innerText = brandElement.textContent.trim()
                            outerHTML = brandElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                result.currency.value = 'USD'
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let showMoreButtonElement = document.querySelector('[data-mount="showMoreShowLess"]');

                        if (showMoreButtonElement){
                            showMoreButtonElement.click()
                        }

                        let descriptionElement = document.querySelector('[data-container="showMoreShowLess"] div');
                        
                        if (descriptionElement){
                            outerHTML = descriptionElement.outerHTML
                            innerText = descriptionElement.innerText.trim()
                            
                        }
                    } catch (error) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SCPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try{
                        let specTabElement = document.querySelector('a[data-bi-ecn="Tech specs"]');

                        if (specTabElement) {
                            specTabElement.click()
                        }


                        let keyElements = document.querySelectorAll('.table-first-col-highlight tbody th');
                        let valueElements = document.querySelectorAll('.table-first-col-highlight tbody td');
                        for (let idx = 0; idx < keyElements.length; idx++){
                            valuesMap[keyElements[idx].innerText.trim()] = valueElements[idx].innerText.trim()
                            outerHTML = keyElements[idx].outerHTML
                        }
                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages, idSelector){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        if (idSelector){

                            // let stockSelector = `${idSelector} .badge.text-white.bg-black.px-1`
                            // let stockElement = document.querySelector(stockSelector)

                            // if (!stockElement){
                            //     stockElement = document.querySelector('button.c-button.f-primary')
                            // }
                            // if (stockElement) {
                            //     outerHTML = stockElement.outerHTML
                            //     if (stockElement.hasAttribute('disabled')){
                            //         innerText = 'disabled'
                            //     } else {
                            //         innerText = 'enabled'
                            //     }
                            // }
                            let stockElement = document.querySelector(idSelector)

                            if (stockElement){
                                if (stockElement.hasAttribute('disabled')){
                                    outerHTML = stockElement.outerHTML
                                    innerText = 'disabled'
                                    
                                }else {
                                    innerText = 'enabled'
                                }
                            }

                        }else {
                            let addToCartButtonElement = document.querySelector('#rootContainer_BuyBox .btn.btn-primary.btn-block') || document.querySelector('.tileproductplacement.mini.c-group button') || document.querySelector('.tileproductplacement.mini button') || document.querySelector('#ButtonPanel_buttons button') || document.querySelector('.c-call-to-action.c-glyph.purchButton') || document.querySelector('fieldset.form-group').nextElementSibling

                            if (addToCartButtonElement){
                                outerHTML = addToCartButtonElement.outerHTML
                                if (addToCartButtonElement.hasAttribute('disabled')){
                                    innerText = 'disabled'
                                    if (addToCartButtonElement.getAttribute('disabled').toLowerCase().includes('disabled')){
                                        innerText = 'disabled'
                                    }

                                }else if (addToCartButtonElement.hasAttribute('aria-disabled')){
                                    if (addToCartButtonElement.getAttribute('aria-disabled').toLowerCase().includes('false')) {
                                        innerText = 'enabled'
                                    } else if (addToCartButtonElement.getAttribute('aria-disabled').toLowerCase().includes('true')) {
                                        innerText = 'disabled'
                                    } 
                                }else if (addToCartButtonElement.hasAttribute('disabled')){
                                    if (addToCartButtonElement.getAttribute('disabled').toLowerCase().includes('true')){
                                        innerText = 'disabled'
                                    }
                                }else if (addToCartButtonElement.hasAttribute('data-bi-name')){
                                    if (addToCartButtonElement.getAttribute('data-bi-name').toLowerCase().includes('sold out')) {
                                        innerText = 'disabled'
                                    }
                                }else if (addToCartButtonElement.innerText.toLowerCase().includes('not available')) {
                                    innerText = 'disabled'
                                }
                                else {

                                    outerHTML = addToCartButtonElement.outerHTML
                                    innerText = 'enabled'

                                }
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages, idSelector);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML


                // RATING

                result.rating.score.value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.score.max_value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.score.source = FailureMessages.GENERIC_NOT_FOUND                          


                // REVIEWS
  
                result.rating.reviews.value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.reviews.source = FailureMessages.GENERIC_NOT_FOUND
   

                
                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let buttonModalElement = document.querySelector('.glyph-prepend-photo-collection');

                        if (buttonModalElement){
                            buttonModalElement.click()
                        }

                        let imageElements = document.querySelectorAll('.modal-content img');

                        if (imageElements.length <= 0){
                            imageElements = document.querySelectorAll('section[aria-label="Product image"] img')
                        }
                        if (imageElements.length <= 0){
                            imageElements = document.querySelectorAll('#image-slide-0 img')
                        }                        

                        for(let img of imageElements){
                            images.push(img.getAttribute('src'))
                            outerHTML = img.outerHTML

                        }

                        let closeButtonElement = document.querySelector('button[aria-label="Close dialog window"]');

                        if (closeButtonElement) {
                            closeButtonElement.click()
                        }


                    }catch(error){
                        images = FailureMessages.IMAGE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML
                
                // VIDEO
                let videosResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let videos = []

                    return { videos, outerHTML }
                }, FailureMessages);

                result.video_urls.value = videosResult.videos
                result.video_urls.source = videosResult.outerHTML

                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = MicrosoftComEnUsWebsiteStrategy