const puppeteer = require('puppeteer-extra')
// const puppeteer = require('puppeteer')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')

class LowesComWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    // if prices are inaccurate per validator, use puppeteer-extra
    // and StealthPlugin

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            puppeteer.use(StealthPlugin())

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                // await page.setViewport({width: 1920, height: 1080})

                // Open URL in browser

                // const geoLoc = {latitude: 34.0477436, longitude:-118.33668 }
                // const cookie = {"name":"region", "value":"central"}
                // await page.setGeolocation(geoLoc)
                
                await page.goto(url, {
                    timeout: timeout //,
                    //waitUntil: 'networkidle0'
                })

                // await page.setCookie(cookie)
                // selection location

                // let location = "Mid city Los Angeles" 
                let zip = "90011"
                // let storeId=  'store-2714'

                try {
                    // Perform set location

                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location) 

                    // Check if the location is not correct
                    if (location.toLowerCase() != "Mid-city Los Angeles Lowe's".toLowerCase()){

                        // Click the element so the modal will pop up
                        await search_location.click()

                        // Wait for the modal to pop up
                        await page.waitForSelector('[class*="ModalWrapper-sc"]')

                        try{
                            var clearTextButton = await page.waitForSelector('.modal-handler .icon-close-circle-filled');
                            await clearTextButton.click()
                        }
                        catch(e){
                            //Do nothing
                        }
                        

                        // After the modal pops up, search for the location using zip as input
                        await page.type(".modal-handler input.textinput", zip)  //(".modal-header-title input.textinput", zip)  

                        await page.waitFor(5000)

                        // Click search button after input
                        var submitButton = await page.waitForSelector('.modal-handler button[type="submit"]') //(".modal-header-title button")
                        await submitButton.click()

                        // Wait to load
                        await page.waitFor(12000)
                        

                        // After the location appeared, wait and click the button of the 1st li element item 
                        await page.waitForSelector('button.met-sl-set-as-my-store') //page.waitForSelector('button[data-linkid="shop-this-store"]')
                        var shopButton = await  page.waitForSelector('button.met-sl-set-as-my-store') //page.waitForSelector('button[data-linkid="shop-this-store"]')
                        await shopButton.click()
                        
                    }

                }
                catch (error) {
                    console.log(error)
                }

                try {
                    // Refresh the page
                    await page.reload({waitUntil:"networkidle2"})
                    
                    // Sanity check    
                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location)
                    console.log(`[*] Changed Zip to ${location}`)

                    
                } catch (error) {
                    console.log(error)
                    // Sanity check    
                    var search_location = await page.waitForSelector("#store-search-handler")
                    var location = await page.evaluate(element => element.innerText, search_location)
                    console.log(`[*] Changed Zip to ${location}`)                    
                    
                    // do nothing
                }

                await page.waitForTimeout(10000)
                
                // add wait timer to avoid errors
                // await page.waitForTimeout(7500)

                // let html = await page.content()
                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = document.querySelector('div.priceWrapper span.finalPrice span.aPrice') || document.querySelector('div.newPriceWrapper div.main-price') || document.getElementsByClassName('main-price')[0];
                        if (priceElement){
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                        // else {
                        //     let script = document.querySelector('script[type="application/ld+json"]')
                        //     if (script){
                        //         let toJson = script.innerHTML
                        //         if (toJson.length > 0) {
                        //             let json = JSON.parse(toJson)
                        //             if (json.length >= 3){
                        //                 price = json[2]["offers"]['price']
                        //                 if(price == null) {
                        //                     innerText = FailureMessages.GENERIC_NOT_FOUND
                        //                 }
                        //                 else{
                        //                     innerText = String(price)
                        //                 }
                        //                 outerHTML = script.outerHTML
                        //             }
                        //         }
                        //     }
                        // }
                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('.title-ivm-wrapper h1')
                        let dup = titleElement.cloneNode(true)
                        dup.querySelector('.desc-brand').remove()

                        if (titleElement){
                            innerText = dup.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('.title-ivm-wrapper span.desc-brand');
                        if (brandElement){
                            innerText = brandElement.textContent.trim()
                            branouterHTMLdElement = brandElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                let currencyResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let currencyElement = document.querySelector('div.priceWrapper span.finalPrice span.aPrice sup');
                        if (currencyElement){
                            innerText = currencyElement.getAttribute('content')
                            outerHTML = currencyElement.outerHTML
                        }
                        else {
                            let script = document.querySelector('script[type="application/ld+json"]')

                            if (script){
                                let toJson = script.innerHTML
                                if (toJson.length > 0) {
                                    let json = JSON.parse(toJson)
                                    if (json.length >= 3){
                                        currency = json[2]["offers"]['priceCurrency']
                                        innerText = currency
                                        outerHTML = script.outerHTML
                                    }
                                }
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.CURRENCY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.currency.value = currencyResult.innerText
                result.currency.source = currencyResult.outerHTML
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let description = []
                    let src = []

                    try{
                        var element = document.querySelector('.overviewWrapper');

                        if (element) {
                            src.push(element.outerHTML);
                            description.push(element.innerText);
                        }

                        element = document.querySelector('.overviewWrapper .specs');

                        if (element) {
                            src.push(element.outerHTML);
                            description.push(element.innerText);
                        }

                        if (description.length > 0){
                            outerHTML = src.join(' + ');
                            innerText = description.join(' + ');
                        }

                    }catch(error){
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try{
                        let keys = document.querySelectorAll('.table.specs .key');
                        let values = document.querySelectorAll('.table.specs .value');
                        for (let i =0; i< keys.length; i++){
                            if (values[i].childElementCount == 1 ){
                                let svg = values[i].querySelector("svg")
                                valuesMap[keys[i].textContent ] = svg.getAttribute("alt")
                                continue
                            }
                            valuesMap[keys[i].textContent] = values[i].textContent
                        }
                      
                    }catch(error){
                        valuesMap = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let moreThanFour = document.querySelector('div.thumblist .gal-thumbnail li span[data-testid="gal-thumbnail-excess"]') || document.querySelector('.gallery-section #main-section li') 
                        let imageElements = document.querySelectorAll('div.thumblist .gal-thumbnail li') || document.querySelectorAll('.gallery-list li')
                        if(moreThanFour) {
                            let script = document.querySelector('script[type="application/ld+json"]')
                            if (script) {
                                let toJson = script.innerHTML
                                if (toJson.length > 0){
                                    json = JSON.parse(toJson)
                                    imgs = json[2]["image"]
                                    imgs.forEach((img, index)=>{
                                        if(!img.includes('http')){
                                            images.push(`https:${img}`)
                                        }
                                        else {
                                            images.push(img)
                                        }
                                    })

                                }
                            }
                        }
                        else {
                            if (imageElements.length == 0) {
                                imageElements = document.querySelectorAll('.gallery-list li')
                            }
                            imageElements.forEach((element, index) => {
                                let el = element.querySelector("img");
                                if (el){
                                    let url = el.getAttribute('src')
                                    if (url){
                                        url = url.replace('size=mthb','size=pdhi')
                                        if (!url.includes('http')){
                                            images.push(`http:${url}`)
                                            // images[index] = `http:${url}`
                                        }else{
                                            images.push(url)
                                            // images[index] = url
                                        }
                                    }
                                }

                            });
                        }

                    }catch(error){
                        images = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // // AVAILABILITY

                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{

                        let warnMessage = document.querySelector(".warnMessage span")
                        let restrictionMessageElement = document.querySelector("[class*=RestrictionMessageBox] p.subTitle")

                        if(warnMessage) {
                            innerText = warnMessage.getAttribute("class")
                            outerHTML = warnMessage.outerHTML
                            if (innerText === 'title'){
                                innerText = warnMessage.textContent.trim();
                            }
                        }
                        else if (restrictionMessageElement){
                            innerText = restrictionMessageElement.innerHTML
                            outerHTML = restrictionMessageElement.outerHTML

                        }
                        else {
                            let script = document.querySelector('script[type="application/ld+json"]')

                            if (script){
                                let toJson = script.innerHTML
                                if (toJson.length > 0) {
                                    let json = JSON.parse(toJson)
                                    if (json.length >= 3){
                                        availability = json[2]["offers"]['availability']
                                        innerText = availability
                                        outerHTML = script.outerHTML
                                    }
                                }
                            }

                        }

                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let maxRating = "5"
                    try{
                        let ratingElement = document.querySelector('.reviewDefault .rating');
                        if (ratingElement){
                            let label = ratingElement.getAttribute('aria-label')
                            if(label){
                                innerText = label
                            }
                        }
                        else{
                            ratingElement = document.querySelector('.reviewDefault div');
                            if(ratingElement){
                                let re =  /(\d+(?:\.\d+)?) out/
                                let label = ratingElement.getAttribute('aria-label')
                                let matches = re.exec(label)
                                if (matches){
                                    innerText = matches[1]
                                    outerHTML = reviewsElement.outerHTML
                                }
                            }
                        }
                        
                    }catch(error){
                        innerText = error
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, maxRating, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = ratingResult.maxRating
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewsElement = document.querySelector('.reviewDefault .rating-count');
                        if (reviewsElement){
                            innerText = reviewsElement.textContent.trim()
                        }
                        else{
                            reviewsElement = document.querySelector('.reviewDefault div');
                            if(reviewsElement){
                                let re =  /(\d+) review/
                                let label = reviewsElement.getAttribute('aria-label')
                                let matches = re.exec(label)
                                if (matches){
                                    innerText = matches[1]
                                    outerHTML = reviewsElement.outerHTML
                                }
                            }
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.GENERIC_NOT_FOUND
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // SHIPPING
                let shippingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let elements = document.querySelectorAll('pwr-buyarea-usps div.row > div');
                        if (elements.length > 2){
                            let shippingElement = elements[2].querySelector('p');
                            
                            if (shippingElement){
                                innerText = shippingElement.textContent.trim()
                                outerHTML = shippingElement.outerHTML
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.SHIPPING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.shipping.value = shippingResult.innerText
                result.shipping.source = shippingResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let deliveryElement = document.querySelector('.shipping-message')
                        if (deliveryElement){
                            innerText = deliveryElement.textContent.trim()
                            outerHTML = deliveryElement.outerHTML
                        }

                        if (!innerText){
                            let deliveryElement = document.querySelector('.pdp-inventory .shipping-title')

                            if (deliveryElement) {
                                innerText = deliveryElement.textContent.trim()
                                outerHTML = deliveryElement.outerHTML

                            }
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML
     

                var vidResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []
                    let videos = []
                    let length = 0
                    try { 

                        // find videos on the WINDOW PRELOAD
                        // finding during run time some the script results is just 19 or 23 and the scripts are hidden
                        let videoElement = document.querySelector('img[data-met-type="video"]')
                        if(videoElement) {
                            let scriptElements = document.querySelectorAll('script[charSet="UTF-8"]')
                            let preFix = "window['__PRELOADED_STATE__'] = "
                            length = scriptElements.length
                            if (length == 22) {
                                let windowPreload = scriptElements[17].innerHTML
                                let toJson = windowPreload.replace(preFix, '')
                                let json = JSON.parse(toJson)
                                let prodId = json['productId']
                                let product = json['productDetails'][prodId]['product']
                                let apiUrls = product['epc']['videoStreams']
                                if (apiUrls){
                                    videos = apiUrls.map( (item) => item['url'] )
                                }else{
                                    let videoIds = product['epc']['videos']
                                    videos = videoIds.map((id) => `https://lda.lowes.com/is/content/Lowes/${id}-AVS.m3u8`)
                                } 
                                
                            }
                            else if (scriptElements.length) {
                                let data = ""
                                let temp = []
                                for (let elements of scriptElements) {
                                    let expression = /(window\['__PRELOADED_STATE__'\]?)/
                                    expression.test(elements.innerHTML) ? data = elements.innerHTML : null
                                }
                                if (data) {
                                    let rawJsonString = /(window\['__PRELOADED_STATE__'\] = )([\s\S]+[}\]])/.exec(data)
                                    let jsonString = rawJsonString[2] + '}}}}'
                                    let json = JSON.parse(jsonString)
                                } 
                                // d1 = 
                            }
                            else {
                                scriptElements.forEach((el, index) => {
                                    if( el.innerHTML.indexOf(preFix) >= 0 ) {
                                        let windowPreload = el.innerHTML
                                        let toJson = windowPreload.replace(preFix, '')
                                        let json =JSON.parse(toJson)
                                        let prodId = json['productId']
                                        let product = json['productDetails'][prodId]['product']
                                        let apiUrls = product['epc']['videoStreams']
                                        if (apiUrls){
                                            videos = apiUrls.map( (item) => item['url'] )
                                        }else{
                                            let videoIds = product['epc']['videos']
                                            videos = videoIds.map((id) => `https://lda.lowes.com/is/content/Lowes/${id}-AVS.m3u8`)
                                        } 
                                        
                                    }
                                })
                            }

                            if(videos.length > 0) {
                                valuesArray = await Promise.all(videos.map( async(apiurl) =>{
                                    let options = {
                                        'method': 'GET',
                                        'headers': {
                                            "User-Agent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0",
                                            "Accept":"*/*",
                                            "Accept-Language":"en-US,en;q=0.5",
                                            "Origin":"https://www.lowes.com",
                                            "DNT":1,
                                            "Connection":"keep-alive",
                                            "Referer":"https://www.lowes.com/",
                                            "Sec-Fetch-Dest":"empty",
                                            "Sec-Fetch-Mode":"cors",
                                            "Sec-Fetch-Site":"cross-site"
                                        }
                                    }

                                    let response = await fetch(apiurl, options);
                                    if (response.status == 200){
                                        let resultString = await response.text();
                                        let re = /RESOLUTION=1280x720\s(http.*)/gm;
                                        let matches = re.exec(resultString);
                                        if (matches){
                                            return matches[1]
                                        }else{
                                            re = /(http.*)/g;
                                            matches = re.exec(resultString);
                                            if (matches){
                                                return matches[1]
                                            }
                                        }  
                                    }
                                    else{
                                        throw new Error('Failed to access Videos API')
                                    }
                
                                }))
                            }  
                        } else {
                            let videoElement = document.querySelector('video.vjs-tech')
                            if (videoElement) {
                                let apiUrl = 'https://mobileimages.lowes.com/hls/p/4215793/sp/421579300/serveFlavor/entryId/1_kntmn5bp/v/1/ev/7/flavorId/1_m07b27hm/name/a.mp4/seg-1-v1-a1.ts'
                                             'https://mobileimages.lowes.com/hls/p/4215793/sp/421579300/serveFlavor/entryId/1_awhzgwmr/v/1/ev/7/flavorId/1_wqlp1zbp/name/a.mp4/seg-1-v1-a1.ts'
                                let requestAttrbutes = {
                                    'method' : 'GET',
                                    'headers' : {
                                        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
                                        'sec-ch-ua-mobile': '?0',
                                        'sec-ch-ua-platform': '"Windows"',
                                        'sec-fetch-dest': 'empty',
                                        'sec-fetch-mode': 'cors',
                                        'sec-fetch-site': 'same-site',
                                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36',
                                        'accept': '*/*',
                                        'accept-encoding': 'gzip, deflate, br',
                                        'accept-language': 'en-US,en;q=0.9'
                                    }
                                }

                                let response = await fetch ()
                            }
                        }

                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        valuesArray = FailureMessages.VIDEO_URLS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesArray}
                }, FailureMessages)
           
                result.video_urls.source = vidResult.outerHTML
                result.video_urls.value = vidResult.valuesArray

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = LowesComWebsiteStrategy