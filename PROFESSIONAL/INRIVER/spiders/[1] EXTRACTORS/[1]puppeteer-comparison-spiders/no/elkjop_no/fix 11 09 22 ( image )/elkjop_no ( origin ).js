const puppeteer = require('puppeteer-extra')
// const puppeteer = require('puppeteer')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { PageRedirectStrategyError, UnhandledStrategyError } = require('../errors')
const thirdPartyTemplate = require('../static/base_third_party.json')

function debugPrint(message){
    console.log(message);
}


class ElkjopNoWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            // Added puppeteer stealth as some static files are blocked returning 403 error then the page does not load properly.
            puppeteer.use(StealthPlugin())

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint
                
                this.proxy = null

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = (await browser.pages())[0] //await browser.newPage()

                await page.exposeFunction('debugPrint', debugPrint);

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                //temp user agent
                userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/105.0.0.0 Safari/537.36'
                
                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout,
                    waitUntil: 'networkidle0'
                })
                
                try{
                    // await page.waitForSelector('#coiPage-1 > div.coi-banner__page-footer > div.coi-button-group > button.coi-banner__accept')
                    // await page.click('#coiPage-1 > div.coi-banner__page-footer > div.coi-button-group > button.coi-banner__accept')
                    // await page.evaluate( () => {
                    //     let acceptBannerButton = document.querySelector('[aria-label="JEG GODTAR"]')
                    //     if (acceptBannerButton){
                    //         acceptBannerButton.click()
                    //     }
                    // })
                    // let acceptCookiesButton =  await page.waitForSelector('[aria-label="JEG GODTAR"]')
                    // await Promise.all([
                    //     console.log('been here'),
                    //     await page.waitForTimeout(5000),
                    //     await page.evaluate( () => {
                    //         let acceptBannerButton = document.querySelector('[aria-label="JEG GODTAR"]')
                    //         console.log('acceptBannerButton >>>>>', acceptBannerButton)
                    //         if (acceptBannerButton){
                    //             console.log('attempting click')
                    //             acceptBannerButton.click()
                    //             console.log('clicked')
                    //         }
                    //     })
                    // ]);
                    // await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
                    let acceptCookiesButton =  await page.waitForSelector('[aria-label="JEG GODTAR"]')
                    await acceptCookiesButton.click()
                    // await page.evaluate( () => {
                    //     let acceptBannerButton = document.querySelector('[aria-label="JEG GODTAR"]')
                    //     console.log('acceptBannerButton >>>>>', acceptBannerButton)
                    //     if (acceptBannerButton){
                    //         console.log('attempting click')
                    //         acceptBannerButton.click()
                    //         console.log('clicked')
                    //     }
                    // })

                }
                catch(e){
                    console.log(e)
                    // Do nothing
                }

                // Reload page as the Deliveries does not load after click on the cookies modal
                // try{
                //     console.log('[*] Reloading page')
                //     await Promise.all([
                //         page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] }),
                //         console.log('[*] Page loaded')
                //     ]);

                // }
                // catch(e) {}

                // Adjusted timeout as there is already a reload added
                await page.waitForTimeout(5000)

                // PDP Checker
                await page.evaluate(async () => {
                    var element = document.querySelector('.not-found__headline')

                    if (element){
                        throw new UnhandledStrategyError('PDP Checker Error')
                    }

                })

                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */
                
                await page.waitForTimeout(5500)

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        // var priceElement = document.evaluate('//*[@id="buy-box"]/div[1]/div/elk-price/span/span/font/font', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent

                        let priceElement = document.querySelector('elk-price.pdp__price > span.price__value') || 
                            document.querySelector("#buy-box > div.buy-box__content.ng-star-inserted > div > elk-price > span") ||
                            document.querySelector("#main > ng-component > div > div > aside > div > elk-product-sticky-header > div > elk-price > span > span")||
                            document.querySelector('div.buy-box__content .price__value') ||
                            document.querySelector('elk-price.buy-box__price.price.price--200.ng-star-inserted > span > span')

                        if (priceElement) {
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{   
                        let titleElement = document.querySelector('div.product-sticky-header__title')
                        if (titleElement) {
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        } else { 
                            let titleElement = document.querySelector('title')
                            if (titleElement) {
                                innerText = titleElement.textContent.trim()
                                outerHTML = titleElement.outerHTML
                            } else {
                                let titleElement = document.querySelector('h1.product-title');
                                if (titleElement) {
                                    innerText = titleElement.textContent.trim()
                                    outerHTML = titleElement.outerHTML
                                }
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('a[href*="/brand/"]');
                        innerText = brandElement.getAttribute('title')
                        outerHTML = brandElement.outerHTML
                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                result.currency.value = 'DKK'
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                
                //Scroll down to load description
                async function autoScroll(page){
                    await page.evaluate(async () => {
                        await new Promise((resolve, reject) => {
                            var totalHeight = 0;
                            var distance = 100;
                            var timer = setInterval(() => {
                                var scrollHeight = document.body.scrollHeight;
                                window.scrollBy(0, distance);
                                totalHeight += distance;
                
                                if(totalHeight >= scrollHeight - window.innerHeight){
                                    clearInterval(timer);
                                    resolve();
                                }
                            }, 100);
                        });
                    });
                }
                
                await autoScroll(page);
                await page.waitForTimeout(500)
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let description = '';

                        let shortDescriptionElement = document.querySelector('elk-short-description');
                        if (shortDescriptionElement){
                            let tempShortDesc = []
                            let shortDescHeadElement = shortDescriptionElement.querySelector('.short-description__headline');
                            if (shortDescHeadElement){
                                tempShortDesc.push(shortDescHeadElement.textContent.trim());
                            }
                            let shortDescTextElement = shortDescriptionElement.querySelector('p.short-description__text');
                            if (shortDescTextElement){
                                tempShortDesc.push(shortDescTextElement.textContent.trim());
                            }
                            if (tempShortDesc.length > 0){
                                description += tempShortDesc.join(' ') + '\n\n';
                            }
                        }

                        let containerElement = document.querySelector('elk-product-features > div') || document.querySelector('.l-default-indentation.pdp__detail-section.ng-star-inserted');

                        for(let index = 0; index < containerElement.childNodes.length; index++){
                            let element = containerElement.childNodes[index];

                            if(element.nodeName == '#text'){
                                description += element.nodeValue + '\n\n';
                            }else if(element.nodeName == 'STRONG' ){
                                description += element.textContent.trim() + '\n\n';
                            }else if(element.nodeName == 'DIV' ){
                                if (element.textContent.trim().length > 0){
                                    description += element.textContent.trim() + '\n\n';
                                }
                            }else if (element.nodeName == 'P'){
                                description += element.textContent.trim() + '\n\n';
                            }
                        }

                        description = description.trim();

                        if(description){
                            innerText = description
                            outerHTML = containerElement.outerHTML
                        }
                            
                    }catch(error){
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SCPECIFICATIONS

                let specificationsResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try{
                        let specificationsElement = document.querySelectorAll('elk-specifications.pdp__specs  li.product-attributes__item')
                        specificationsElement.forEach(async function(element){
                        let tableElement = element.querySelector('table.spec-attributes');
                        tableElement.querySelectorAll('tr').forEach(function(tr){
                            let keytag = tr.querySelector('td.spec-attributes__cell.spec-attributes__cell--name');
                            let valtag = tr.querySelector('td.spec-attributes__cell.spec-attributes__cell--value');
                            valuesMap[keytag.textContent.trim()] = valtag.textContent.trim()
                            // let tds = tr.querySelectorAll('td');
                            // valuesMap[tds[0].textContent.trim()] = tds[1].textContent.trim()
                        });
                        });
                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let imageElements = document.querySelectorAll('swiper.swiper.swiper__wrapper--align-vertically.swiper-initialized.swiper-horizontal.swiper-pointer-events.swiper-autoheight.swiper-backface-hidden > div.swiper-wrapper > div > img:not(.video-player__play-icon)');
                
                        if (imageElements.length === 0){
                            // this is an alternate query for selecting MULTIPLE images
                            imageElements =  document.querySelectorAll('.pdp__media-intro elk-product-media-selector ul li img.media-selector__preview-image')
                        }   
                        
                        if (imageElements.length === 0){
                            // For only 1 image in PDP
                            // getting error on old tag
                            //imageElements = document.querySelectorAll('elk-product-media-viewer img:not([alt="product.detail.badge"]):not(.badge.ng-star-inserted)')
                            imageElements = document.querySelectorAll('div.swiper-slide.ng-star-inserted.swiper-slide-active  > img');
                        }
                        if (imageElements.length > 0){
                            for(let index = 0; index < imageElements.length; index++){
                                let imageElement = imageElements[index];
                                let imageUrl = imageElement.getAttribute('src');
                                if (imageUrl.includes('--product_thumbnail-60')) {
                                    imageUrl = imageUrl.replace('--product_thumbnail-60', '');
                                }
                                images.push(imageUrl);
                            }
                        }
                    }catch(error){
                        images = FailureMessages.IMAGE_URLS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let availabilityElement = document.querySelector('button.tab-group__item.tab-group__item--active');
                        if (availabilityElement){ // https://prnt.sc/1teqk6f

                            let info = document.querySelector('div.store-info-card__availability.ng-star-inserted') || 
                                       document.querySelector('button.tab-group__item.tab-group__item--active.ng-star-inserted span.tab-group__title')
                            

                            if (info) {
                                innerText = info.textContent.trim();
                                outerHTML = availabilityElement.outerHTML
                            } else {
                                innerText = availabilityElement.textContent.trim();
                                outerHTML = availabilityElement.outerHTML
                            }
                        }else{
                            availabilityElement = document.querySelector('.unavailable--content');
                            if (availabilityElement){
                                innerText = availabilityElement.textContent.trim();
                                outerHTML = availabilityElement.outerHTML
                            }
                            else{
                                availabilityElement = availabilityElement = document.querySelector('.buy-box__unavailable');
                                if (availabilityElement){
                                    innerText = availabilityElement.textContent.trim();
                                    outerHTML = availabilityElement.outerHTML
                                } else {
                                    //if did not load ().buy-box__unavailable)
                                    // temporary fix for now    
                                    innerText = 'false'
                                    outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                }
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let ratingElement = document.querySelector('elk-product-ratings.product-meta__ratings span.rating__score');
                        if(ratingElement){
                            innerText = ratingElement.textContent.trim()
                            outerHTML = ratingElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.RATING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = "5"
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewsElement = document.querySelector('elk-product-ratings.product-meta__ratings span.rating__count');
                        if(reviewsElement){
                            innerText = reviewsElement.textContent.trim()
                            outerHTML = reviewsElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.REVIEWS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let deliveryElement = document.querySelector('elk-store-availability div.store-availability__title') || document.querySelector('div.unavailable--content.icon-addon__label span') || document.querySelector('div.store-availability__title > span'); //remove incorrect tag || document.querySelector('button.tab-group__item.tab-group__item--disabled.ng-star-inserted > span.tab-group__subline')
                        if (deliveryElement){
                            innerText = deliveryElement.textContent.trim();
                            outerHTML = deliveryElement.outerHTML
                        }
                        if (!deliveryElement) {
                            let deliveryTag = document.querySelector('div.store-availability__title > span')
                            if (deliveryTag) {
                                innerText = deliveryTag.textContent.trim()
                                outerHTML = deliveryTag.outerHTML
                            }
                        }
                        if (!deliveryElement) {
                            let deliveryTag = document.querySelector('div.buy-box__unavailable span')
                            if (deliveryTag) {
                                innerText = deliveryTag.textContent.trim()
                                outerHTML = deliveryTag.outerHTML
                            }
                        }
                        if (!innerText){
                            let tabElements = document.querySelectorAll('elk-buy-box elk-tab-group button.tab-group__item');
                            if (tabElements.length > 1){
                                if (tabElements[0].className.includes('tab-group__item--disabled')){ // https://prnt.sc/1tfcvcx
                                    deliveryElement = tabElements[0].querySelector('span.tab-group__subline')
                                    if (deliveryElement){
                                        innerText = deliveryElement.textContent.trim();
                                        outerHTML = deliveryElement.outerHTML
                                    }
                                }
                            }
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                // SHIPPING

                // VIDEO
                let videosResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let videos = []
                    try{

                        // this is video are on the UPPER section of the pdp along with the product images
                        let videoElements_upper_section = document.querySelectorAll('video source');

                        if (videoElements_upper_section.length > 0){
                            for(let index = 0; index < videoElements_upper_section.length; index++){
                                let videoElement = videoElements_upper_section[index];
                                let videoUrl = videoElement.getAttribute('src');
                                videos.push(videoUrl);
                            }
                        }
                         
                        // this videos are on the LOWER section along with the descriptions
                        let videoElements = []
                        let iframeElement =  document.querySelector('#videoly-videobox-placeholder iframe')
                        if (iframeElement){
                            videoElements = iframeElement.contentWindow.document.querySelectorAll('div.b-video-item-tile')
                        }

                        if (videoElements.length > 0){
                            console.log('went here',videoElements.length )
                            for(let index = 0; index < videoElements.length; index++){
                                let videoElement = videoElements[index];
                                let videoUrl = videoElement.getAttribute('data-videoid');
                                videos.push('https://www.youtube.com/watch?v=' + videoUrl);
                            }
                        }
                        else{  
                            // if ever the videos tags don't load property this is an alternate query direct to the API but is the same videos
                            // of the b-video-item-tile element from the iframe                     
                            let videolyUrl =''
                            try {
                                let category = ''
                                let scriptTag = Array.from(document.querySelectorAll('script')).find(el => el.textContent.match('"@type": "Product"'))
                                if (scriptTag){
                                    let scriptData = JSON.parse(scriptTag.textContent)
                                    if (scriptData){
                                        sku = scriptData['sku']
                                        
                                        prodId = sku
                                        title = scriptData['name']
                                        ean = scriptData['gtin']
                                    }
                                }

                                videolyUrl = "https://dapi.videoly.co/1/videos/0/411/?SKU="+sku+"&productId="+prodId+"&productTitle="+title.replaceAll(' ','%20')+"&" +
                                "ean="+ean+"oos=0&maxItems=15&ytwv=&_b=Chrome&_bv=78.0&"+
                                "p=1&_w=1903&_h=937&_pl=&callback=_vdly7c9e3f86d7&hn=www.elkjop.no"

                                console.log('videolyUrl: ', videolyUrl)
                                console.log('sku: ', sku)

                                let byPass = false
                                // if the iframe does not load properly but the item_id should have a video. add on the bypass to allow query without the iframe
                                let by_pass_skus = ['PLAY5G2BK']

                                if (by_pass_skus.includes(sku)) {
                                    byPass = true
                                }
                                // this prevents having false positive, api does return sometimes videos but not visible on the PDP
                                // if ( iframeElement || byPass){
                                // DO NOT UNCOMMENT CHECKER ABOVE FOR NOW -- API RETURNS VIDEO URLS IF THERE IS PRESENT IN PDP, CHECK IN LOCAL BROWSER NOT IN PUPPETEER BROWSER
                                if (videolyUrl){
                                    try{
                                        outerHTML = videolyUrl
                                        const response = await fetch(videolyUrl, {
                                            'method': 'GET',
                                            'headers': {
                                                'accept':'*/*',
                                                'accept-encoding':'gzip, deflate',
                                                'accept-language':'en-US,en;q=0.9',
                                                'referer':'https://www.elkjop.no/',
                                                'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
                                            }
                                        })
                                    
                                        const responseText = await response.text()
                                        var _rgx = responseText.match('({.*})')
                                        if (_rgx){
                                            var _json = JSON.parse(_rgx[1])
                                            if (_json){
                                                _json['items'].forEach((item) => {
                                                    videos.push("https://www.youtube.com/embed/" +  item['videoId'])
                                                })
                                            }
                                        }
                                    }
                                    catch(err){
                                        console.log('requester fail')
                                    }
                                }

                                // }
                               
                                
                            }
                            catch (err){ } // do nothing
                            
                        }

                    }catch(error){
                        videos = FailureMessages.IMAGE_URLS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { videos, outerHTML }
                }, FailureMessages);

                result.video_urls.value = videosResult.videos
                result.video_urls.source = videosResult.outerHTML

                try{
                    await page.waitForSelector('iframe.videoly-box')
                }catch(e){
                    console.log('Frame does not exists ' + e)
                }
                if (result.video_urls.value.length < 1){
                    try {
                        var element = await page.$('iframe.videoly-box')
                        if (element){
                            var frame = await element.contentFrame()
                            var vidResult = await frame.evaluate(async (FailureMessages) => {
                                var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                var valuesArray = []
                                
            
                                try { 
                                    let videoElements = document.querySelectorAll('.b-video-list-wrapper .b-video-item-tile')
            
                                    if (videoElements.length >= 1){
            
                                        for (let i = 0; i < videoElements.length; i++){
                                            var url = 'https://www.youtube.com/embed/' + videoElements[i].getAttribute('data-videoid')
            
                                            valuesArray.push(url)
            
                                        } 
                                    }
                                } catch(e) {
                                    outerHTML = FailureMessages.GENERIC_NOT_FOUND
                                    valuesArray = FailureMessages.VIDEO_URLS_EXTRACTION
                                }
                                
                                return { outerHTML, valuesArray}
                            }, FailureMessages)
            
                            result.video_urls.source = vidResult.outerHTML
                            result.video_urls.value = vidResult.valuesArray                        
                        }
                        else {
                            result.video_urls.source = FailureMessages.GENERIC_NOT_FOUND
                            result.video_urls.value = []           
    
                        }
                        
                    } catch (error) {
                        result.video_urls.source = FailureMessages.GENERIC_NOT_FOUND
                        result.video_urls.value = []   
                    }
                }

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = ElkjopNoWebsiteStrategy