const puppeteer = require('puppeteer-extra')
// const puppeteer = require('puppeteer')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { PageRedirectStrategyError, UnhandledStrategyError } = require('../errors')
const thirdPartyTemplate = require('../static/base_third_party.json')

function debugPrint(message){
    console.log(message);
}


class WalmartCaWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            // Added puppeteer stealth as some static files are blocked returning 403 error then the page does not load properly.
           puppeteer.use(StealthPlugin())

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                //Unblocker
                this.proxy.server = 'zproxy.lum-superproxy.io:22225'
                this.proxy.user = 'lum-customer-c_9c4e20a3-zone-be_unblocker-country-ca-unblocker'
                this.proxy.pwd = '5ae5cfm7v22g'

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`
                    // browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    // browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                    
                    //Needed for unblocker -- comment when unblocker is removed
                    browser = await puppeteer.connect(
                        { 
                            browserWSEndpoint: browserEndpoint,
                            ignoreHTTPSErrors: true
                        }
                    )
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = (await browser.pages())[0] //await browser.newPage()

                await page.exposeFunction('debugPrint', debugPrint);

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                //temp user agent
                userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/105.0.0.0 Safari/537.36'

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                // Open URL in browser
                await page.goto(url, {
                    // timeout: 0,
                    timeout: timeout,
                    waitUntil: 'networkidle2'
                })

                try{
                    await page.evaluate(function(){
                        let element = document.querySelector('button.css-1hvjcps.ey74ga00');
                        if(element){
                            element.click();  
                        }
                    });
                }
                catch(err){
                    // Do nothing
                }

                var color_checker = null;
                
                try {
                    await page.waitForTimeout(5000)
                    console.log('Color variant detected')     
                    if (variant.hasOwnProperty('color')){
                        let variantColor = variant.color
                        var colorSelector = `button.e30m82v2[aria-label="${variantColor}"]`

                        color_checker = await page.evaluate(function(colorSelector) {
                            let colorElement = document.querySelector(colorSelector)
                            if (colorElement) {
                                if (!colorElement.getAttribute('class').includes('css-yopveh')){ // Check if the button has been clicked.
                                    value = document.querySelector(colorSelector).getAttribute('class')
                                    if (value.includes('css-yopveh')) {
                                        return false;
                                    } else {
                                        colorElement.click()
                                        return true;
                                    }
                                }
                            }
                        }, colorSelector)

                        if (!color_checker){ // For when variant is unavailable
                            colorSelector = `button.e30m82v2[aria-label="${variantColor}; not available"]`
                            color_checker = await page.evaluate(function(colorSelector) {
                                let colorElement = document.querySelector(colorSelector)
                                if (colorElement) {
                                    if (!colorElement.getAttribute('class').includes('css-yopveh')){
                                        value = document.querySelector(colorSelector).getAttribute('class')
                                        if (value.includes('css-yopveh')) {
                                            return false;
                                        } else {
                                            colorElement.click()
                                            return true;
                                        }
                                    }
                                }
                            }, colorSelector)
                        }
                    }
                }
                catch (error) {
                    //pass
                    console.log(error)
                }

                // Reload page as the specs tag does not load
                try{
                    console.log('[*] Reloading page')
                    await Promise.all([
                        page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] }),
                        console.log('[*] Page loaded')
                    ]);

                }
                catch(e) {}

                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let priceElement = document.querySelector('[data-automation="price-area"] span.css-hwf4mc.esdkp3p2')

                        if (priceElement) {
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('div.css-jl2ki2.e1yn5b3f3 [data-automation="product-title"]')
                        if(titleElement) {
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('div.css-xligv4.e1yn5b3f7 span a')
                        if(brandElement) {
                            innerText = brandElement.innerText
                            outerHTML = brandElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                result.currency.value = 'CAD'
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let descriptionElement = document.querySelector('section.css-1g6crsj.eqaamsw0 div.css-uug7pg.eqaamsw1') || document.querySelector('div#product-description-section')
                        if(descriptionElement) {
                            innerText = descriptionElement.innerText
                            outerHTML = descriptionElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SPECIFICATIONS

                let specificationsResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    let res = []
                    try{
                        let specificationsElement = document.querySelectorAll('div.css-g0jze5.e1cuz6d12 table.css-ltqa49.e1cuz6d13 tr.css-krfoc6.e1cuz6d15')
                        if(specificationsElement) {
                            outerHTML = ''
                            specificationsElement.forEach((item) => {
                                outerHTML = outerHTML + `${item.innerHTML.toString()}`
                                kvPair = item.innerText.split('\n')
                                res.push(kvPair)
                            });
                            valuesMap = Object.fromEntries(res)
                        }
                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let imageElements = document.querySelectorAll('body > div.js-content.privacy-open > div > div:nth-child(4) > div > div > div.css-0.eewy8oa0 > div.css-1s4j31p.eewy8oa2 > div.css-1aby6aq.eewy8oa3 > div > div > div > div > div.css-t3i4bz.e6g1c3f2 > div > div > div > div div.css-8ue2ds.effq510');
                        if(imageElements) {
                            outerHTML = imageElements.outerHTML
                            imageElements.forEach(img => {
                                let imgTag = img.querySelector('.css-128ipej')
                                if(imgTag) {
                                images.push(imgTag.getAttribute('style').replaceAll("=180", "=450"))    
                                }
                            })
                        }

                    }catch(error){
                        images = FailureMessages.IMAGE_URLS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let availabilityElement = document.querySelector('[type="application/ld+json"]')
                        if (availabilityElement) { 
                            jsonValue = JSON.parse(availabilityElement.innerText)
                            if(jsonValue.hasOwnProperty('offers') && jsonValue.offers.hasOwnProperty('availability')) {
                                innerText = jsonValue.offers.availability
                                outerHTML = availabilityElement.outerHTML
                            } 
                        } 
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let ratingElement = document.querySelector('[data-automation="rating-stars"]') 
                        if(ratingElement){
                            if(ratingElement.hasAttribute('aria-label')) {
                            innerText = ratingElement.getAttribute('aria-label').trim()
                            outerHTML = ratingElement.outerHTML    
                            } else {
                            innerText = ratingElement.querySelector('div.css-1hyfx7x.e1tubcqa2').innerText
                            outerHTML = ratingElement.outerHTML
                            }
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.RATING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = "5"
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewsElement = document.querySelector('[data-automation="review-link"]') 
                        if(reviewsElement){
                            innerText = reviewsElement.textContent.trim()
                            outerHTML = reviewsElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.REVIEWS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let deliveryElement = document.querySelector('#add-to-cart-button-wrapper')
                        if (deliveryElement){
                            innerText = deliveryElement.textContent.trim();
                            outerHTML = deliveryElement.outerHTML
                        }
      
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                // SHIPPING
                let shippingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        let shippingElement = document.querySelector('[data-automation="fulfillment-shipping-message"]')
                        if (shippingElement){
                            innerText = shippingElement.textContent.trim();
                            outerHTML = shippingElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.SHIPPING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.shipping.value = shippingResult.innerText
                result.shipping.source = shippingResult.outerHTML
             

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = WalmartCaWebsiteStrategy