const puppeteer = require('puppeteer')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')

class MediamarktBeNlWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1920, height: 1080})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout,
                    waitUntil: 'networkidle0'
                })

                // accept cookies
                // await page.evaluate(function(){
                //     let element = document.querySelector('button.coi-banner__accept');
                //     if(element){
                //         element.click();
                //     }
                // });

                // await page.waitForTimeout(5000)
                // await page.waitForSelector('.pwr-icon.s-32.pwr-icon-arrow-down')

                // await page.evaluate(function(){
                //     let element = document.querySelector('.pwr-icon.s-32.pwr-icon-arrow-down');
                //     if(element){
                //         element.click();
                //     }
                // });   

                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = document.querySelector('#product-details div.price-sidebar div.price-details.has-old-price div.price.big.length-5')
                        if (priceElement){
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('#product-details div.details h1');
                        if (titleElement){
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('#product-details div.details div.model div a img')
                        innerText = brandElement.getAttribute('alt')
                        outerHTML = brandElement.outerHTML

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                let currencyResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let currencyElement = document.querySelector('meta[property="gtm-prop-currency-code"]')
                        innerText = currencyElement.getAttribute('content')
                        outerHTML = currencyElement.outerHTML
                    }catch(error){
                        innerText = FailureMessages.CURRENCY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.currency.value = currencyResult.innerText
                result.currency.source = currencyResult.outerHTML
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let description = []
                    let src = []

                    try {
                        let descriptionElement = document.querySelector("#productomschrijving-")

                        if (descriptionElement) {
                            let childNodes = descriptionElement.childNodes
                            for (let i = 0; i <= childNodes.length - 1; i++) {
                                let temp = childNodes[i].textContent.trim()
                                if (!!temp && temp.toLowerCase() != "description") {
                                    description.push(temp)
                                }     
                            }

                            if (description) {
                                innerText = description
                                outerHTML = descriptionElement
                            }
                        }

                    } catch(error) {
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                let valuesMap = {}
                    try{
                        let specificationElement = document.querySelector("#features")

                        if (specificationElement) {
                            let sectionNode = specificationElement.childNodes
                            for (let i = 0; i <= sectionNode.length -1; i++) {
                                if (sectionNode[i].hasChildNodes && sectionNode[i].textContent.trim() != '') { // filter the sections tag
                                    if (sectionNode[i].querySelector('.specification').hasChildNodes()) {
                                        let specificationNodes = sectionNode[i].querySelector('.specification').children
                                        for (let x = 0; x <= specificationNode.length -1; x++) {
                                            if (specificationNode[x].nodeName == 'DT') {
                                                specificationNode[x].textContent.trim()
                                            } else if (specificationNode[x].nodeName == 'DD') {
                                                specificationNode[x].textContent.trim()
                                            } else {
                                                console.log('Check pdp if specification element node name has changed')
                                            }
                                        }
                                    }
                                }
                            }

                            // childNodes[3].querySelector('.specification dt').textContent.trim()
                        }

                    }catch(error){
                        valuesMap = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let imageElementsContainer = document.querySelector('div#product-image-carousel')  || document.querySelector('pwr-product-image-carousel')
                        let imageElements = imageElementsContainer.querySelectorAll('.swiper-slide:not(.swiper-slide-duplicate) picture > source:first-child') || imageElementsContainer.querySelectorAll('div.product-image-container > picture > source:first-child')
                        for(let index = 0; index < imageElements.length; index++){
                            let imageElement = imageElements[index];
                            let srcset = imageElement.getAttribute('data-srcset').split(',')
                            let src = srcset[srcset.length - 1].trim()
                            images.push(src.split(' ')[0])
                        }
                    }catch(error){
                        images = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // // AVAILABILITY

                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let availabilityElement = document.querySelector('meta[itemprop="availability"]');
                        if (availabilityElement){
                            innerText = availabilityElement.getAttribute('content')
                            outerHTML = availabilityElement.outerHTML
                        }
                        // For stocks with expected date --> must be IS
                        if (innerText === 'http://schema.org/OutOfStock'){
                            let stockDateElement = document.querySelector('.buy-area__webshop__status span.stock-available-soon')
                            if (stockDateElement){
                                innerText = stockDateElement.textContent.trim()
                                outerHTML = stockDateElement.outerHTML
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let maxRating = "5"
                    try{
                        let ratingElement = document.querySelector('div[itemprop="ratingValue"]');
                        if(ratingElement){
                            innerText = ratingElement.textContent.trim()
                            outerHTML = ratingElement.outerHTML
                        }

                        let maxRatingElement = document.querySelector('meta[itemprop="bestRating"]')
                        if(maxRatingElement){
                            maxRating = maxRatingElement.getAttribute('content')
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.RATING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, maxRating, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = ratingResult.maxRating
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewsElement = document.querySelector('meta[itemprop="reviewCount"]');
                        if(reviewsElement){
                            innerText = reviewsElement.getAttribute('content')
                            outerHTML = reviewsElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.REVIEWS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // SHIPPING
                let shippingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let elements = document.querySelectorAll('pwr-buyarea-usps div.row > div');
                        if (elements.length > 2){
                            let shippingElement = elements[2].querySelector('p');
                            
                            if (shippingElement){
                                innerText = shippingElement.textContent.trim()
                                outerHTML = shippingElement.outerHTML
                            }
                        }

                        

                    }catch(error){
                        innerText = FailureMessages.SHIPPING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.shipping.value = shippingResult.innerText
                result.shipping.source = shippingResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let deliveryElement = document.querySelector('pwr-product-stock-label .stock-web')
                        if (!deliveryElement){
                            let elements = document.querySelectorAll('pwr-buyarea-usps div.row > div');
                            if (elements.length > 0){
                                deliveryElement = elements[1].querySelector('p');
                            }
                        }

                        if (!deliveryElement){
                            // FOR OOS Products
                            deliveryElement = document.querySelector('.buy-area .buy-area__webshop .status-reason')
                        }

                        if (deliveryElement){
                            innerText = deliveryElement.textContent.trim()
                            outerHTML = deliveryElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                // VIDEO
                var vidResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []
                    var videolyUrl = ""

                    try { 
                        // FROM description videos
                        document.querySelectorAll('.video-wrapper iframe').forEach((el) => {
                            try {
                                if (el.hasAttribute('src')){
                                    valuesArray.push(el.getAttribute('src'));
                                }
                            }
                            catch(err) {} // Do nothing 
                        })
                        
                        // For Under image carousel
                        try {
                            brandName = document.querySelector('meta[itemprop="brand"]').getAttribute('content')
                            sku = document.querySelector('meta[itemprop="sku"]').getAttribute('content')
                            price = document.querySelector('meta[itemprop="price"]').getAttribute('content')
                            prodId = document.querySelector('meta[itemprop="productId"]').getAttribute('content')
                            title = document.querySelector('meta[itemprop="name"]').getAttribute('content')
                            ean = document.querySelector('meta[itemprop="gtin13"]').getAttribute('content')
                            url = document.querySelector('meta[name="og:url"]').getAttribute('content')

                            videolyUrl = "https://dapi.videoly.co/1/videos/0/394/?brandName="+ brandName +
                            "&SKU="+ sku +"&productPrice="+ price +"&productId="+ prodId +"&productTitle="+ title + "&ean="+ ean +
                            "&oos=0&maxItems=15&ytwv=&_b=Chrome&_bv=92.0.4515.159&p=1&_w=1903&_h=937&_pl=no&_cl=no&callback=_vdly5adea7697d&hn=www.power.no&" +
                            "sId=s%3Ao4uWGA6ODgdOZ3DtlIDAdG5otryuAmjg.lezXKYnHP%2FadoRXTLclkmoEIJhST3eiIqlKyDhpkj2M"
                        }
                        catch (err){ } // do nothing

                        // document.querySelectorAll('script[type="text/javascript"]').forEach((el) => {
                        //     try {
                        //         if (el.getAttribute('href').includes('dapi.videoly.co/1/videos/0/394')){
                        //             videolyUrl = el.getAttribute('href')
                        //             console.log(videolyUrl)
                        //         }
                        //     }
                        //     catch(err) {} // Do nothing 
                        // })
                        
                        if (videolyUrl){
                            outerHTML = videolyUrl
                            const response = await fetch(videolyUrl, {
                                'method': 'GET',
                                'headers': {
                                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                                }
                            })
                        
                            const responseText = await response.text()
                            var _rgx = responseText.match('({.*})')
                            if (_rgx){
                                var _json = JSON.parse(_rgx[1])
                                if (_json){
                                    _json['items'].forEach((item) => {
                                        valuesArray.push("https://www.youtube.com/embed/" +  item['videoId'])
                                    })
                                }
                            }
                        }

                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        valuesArray = FailureMessages.VIDEO_URLS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesArray}
                }, FailureMessages)

                result.video_urls.source = vidResult.outerHTML
                result.video_urls.value = vidResult.valuesArray

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = MediamarktBeNlWebsiteStrategy