const puppeteer = require('puppeteer')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')

class MediamarktBeNlWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1920, height: 1080})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout,
                    waitUntil: 'networkidle0'
                })

                /************************************************ START SCRAPING ************************************************/

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){    
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = document.querySelector('#product-details div.price-sidebar div.price-details.has-old-price div.price.big.length-5')
                        let priceElement2 = document.querySelector('meta[property="product:price:amount"]')
                        if (priceElement){
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        } else if (priceElement2 && priceElement2.hasAttribute('content')) {
                            innerText = priceElement2.getAttribute('content').trim()
                            outerHTML = priceElement2
                        }
                    } catch(error) {
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let titleElement = document.querySelector('#product-details div.details h1');
                        if (titleElement){
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('#product-details div.details div.model div a img')
                        innerText = brandElement.getAttribute('alt')
                        outerHTML = brandElement.outerHTML

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                let currencyResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let currencyElement = document.querySelector('meta[property="gtm-prop-currency-code"]')
                        innerText = currencyElement.getAttribute('content')
                        outerHTML = currencyElement.outerHTML
                    }catch(error){
                        innerText = FailureMessages.CURRENCY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.currency.value = currencyResult.innerText
                result.currency.source = currencyResult.outerHTML
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let description = []
                    let src = []

                    try {
                        let descriptionElement = document.querySelector("#productomschrijving-")

                        if (descriptionElement) {
                            let childNodes = descriptionElement.childNodes
                            for (let i = 0; i <= childNodes.length - 1; i++) {
                                let temp = childNodes[i].textContent.trim()
                                if (!!temp && temp.toLowerCase() != "description") {
                                    description.push(temp)
                                }     
                            }

                            if (description) {
                                innerText = description.join(' + ')
                                outerHTML = descriptionElement
                            }
                        }

                    } catch(error) {
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                let valuesMap = {}
                    try{
                        let specificationElement = document.querySelector("#features")
                        let map = new Map()
                        let tempList = []

                        if (specificationElement) {
                            let sectionList = specificationElement.querySelectorAll('section')
                            for (let x = 0; x <= sectionList.length - 1; x++) {
                                let specificationClass = sectionList[x].querySelector('dl')
                                if (specificationClass) {
                                    for (let y = 0; y <= specificationClass.childNodes.length - 1; y++) {
                                        if (specificationClass.childNodes[y].nodeName == 'DT') {
                                            tempList.push(specificationClass.childNodes[y].textContent.trim())
                                        } else if ((specificationClass.childNodes[y].nodeName) == 'DD') {
                                            if (specificationClass.childNodes[y].hasAttribute('class') || specificationClass.childNodes[y].hasAttribute('style')) {
                                                tempList.push(specificationClass.childNodes[y].textContent.trim())
                                            }   
                                        }
                                        if (tempList.length == 2) {
                                            map.set(tempList[0], tempList[1])
                                            tempList = []
                                        }
                                    }
                                }
                            }
                        }

                        if (map) {
                            valuesMap = Object.fromEntries(map);
                            outerHTML = specificationElement
                        }

                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate (function(FailureMessages) {
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try {
                        let imageContainer = document.querySelector("#product-sidebar > div.thumbnail-carousel.js-thumbnail-carousel.thumbnail-carousel--first-page > div > div > ul")
                        
                        if (imageContainer) {
                            let imageElements = imageContainer.querySelectorAll('a')
                            if (imageElements) {
                                for (let x = 0; x <= imageElements.length - 1; x++) {
                                    if (imageElements[x].hasAttribute('data-preview')) {
                                        images.push(imageElements[x].getAttribute('data-preview'))
                                    }
                                }
                                outerHTML = imageContainer
                            }
                        }
                    } catch(error) {
                        images = String(error)
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages) {
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let availabilityElement = document.querySelector('meta[property="og:availability"]')
                        if (availabilityElement && availabilityElement.hasAttribute('content')) {
                            innerText = availabilityElement.getAttribute('content').trim()
                            outerHTML = availabilityElement
                        }
                    } catch(error) {
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING
                let ratingResult = await page.evaluate(function(FailureMessages) {
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    let maxRating = "5"

                    try{
                        let ratingElement = document.querySelector('div[itemprop="ratingValue"]');
                        if (ratingElement) {
                            innerText = ratingElement.textContent.trim()
                            outerHTML = ratingElement.outerHTML
                        }

                        let maxRatingElement = document.querySelector('meta[itemprop="bestRating"]')
                        if(maxRatingElement){
                            maxRating = maxRatingElement.getAttribute('content')
                        }
                        
                    } catch(error) {
                        innerText = FailureMessages.RATING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, maxRating, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = ratingResult.maxRating
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewsElement = document.querySelector('meta[itemprop="reviewCount"]');
                        if(reviewsElement){
                            innerText = reviewsElement.getAttribute('content')
                            outerHTML = reviewsElement.outerHTML
                        }
                    }catch(error){
                        innerText = FailureMessages.REVIEWS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // SHIPPING
                let shippingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        // NOT AVAILABLE
                    } catch(error) {
                        innerText = FailureMessages.SHIPPING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.shipping.value = shippingResult.innerText
                result.shipping.source = shippingResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let deliveryElement = document.querySelector("#product-details > div.price-sidebar > div.price-details.has-old-price > div.box.infobox.availability > ul > li.false.cluster7 > div")
                        let deliveryElement2 = document.querySelector("#product-details > div.price-sidebar > div.price-details.has-old-price > div.box.infobox.availability > ul > li.online > div.delvrmsg")

                        if (deliveryElement) {
                            innerText = deliveryElement.textContent.trim()
                            outerHTML = deliveryElement
                        } else if (deliveryElement2) {
                            innerText = deliveryElement2.textContent.trim()
                            outerHTML = deliveryElement2
                        }

                    } catch(error) {
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                // VIDEO
                var vidResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []
                    var videolyUrl = ""

                    try { 
                        // VIDEO NOT AVAILABLE
                    } catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        valuesArray = FailureMessages.VIDEO_URLS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesArray}
                }, FailureMessages)

                result.video_urls.source = vidResult.outerHTML
                result.video_urls.value = vidResult.valuesArray

                /************************************************ END SCRAPING ************************************************/
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = MediamarktBeNlWebsiteStrategy