import re, html
import unicodedata
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from transformers.exceptions import TransformationFailureException
from constants import ETDefaults
import copy

class MicrosoftComFrFrTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super(TransformationStrategy, self).__init__()

        self.instock_values = [
            'true',
            'inStock',
            'instock',
            'in-stock',
            'available',
            'ajouter au panier',
            'Ajouter au panier',
            'aria-disabled=false',
            'précommander',
            'https://schema.org/instock',
            'Acheter et télécharger',
            'acheter et télécharger',
            'enabled'
        ]
        self.outofstock_values = [
            'not available',
            'non disponible',
            'false',
            'soldout',
            'sold out',
            'outstock',
            'outofstock',
            'out of stock',
            'out-of-stock',
            'discontinued',
            'http://schema.org/outofstock',
            'https://schema.org/outofstock',
            'http://schema.org/outstock',
            'https://schema.org/outstock',
            'aria-disabled=true',
            'en rupture de stock',
            'disabled'
        ]

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        if 'n’est pas disponible pour' in availability:
            return Availability.OUT_OF_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value
    
    def transform_price(self, value):
        try:
            preserved_value = copy.deepcopy(value)
            value = re.sub('[^0-9.]', '', value.replace(",", "."))

            if (re.search(r'full price was', preserved_value.lower())):
                extracted_value, numeric_value = None, None
                extracted_value = preserved_value.split('now')[-1].replace('\xa0', '')
                numeric_value = re.search(r'([\d]+[,|\.]+[\d]+)', extracted_value) if extracted_value else None
                return round(float(numeric_value.group(1).replace(',','.'))) if numeric_value else value
            return round(float(value), 2)
        except:
            return ETDefaults.NOT_FOUND.value

        

    def transform_description(self, value):
        value = html.unescape(value)
        return value
    
    def transform_rating(self, value, max_value):
        try:
            value = value.replace(',', '.')
            return super().transform_rating(str(value), max_value)
        except Exception:
            raise TransformationFailureException('Ratings transformation failed.')
            
    def transform_title(self, value):

        try:
            if value:
                return unicodedata.normalize("NFKD", value)
            else:
                return ETDefaults.NOT_FOUND.value
                # raise Exception

        except Exception:
            raise TransformationFailureException('Title transformation failed')