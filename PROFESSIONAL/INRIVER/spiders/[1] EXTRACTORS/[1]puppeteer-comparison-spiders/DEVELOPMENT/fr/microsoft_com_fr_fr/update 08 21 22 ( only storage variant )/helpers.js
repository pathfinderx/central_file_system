for (let btn of tileBtnElements){
    console.log(btn)
    let score = 0
    for (variant of _variants){
        let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))

        if (hasCorrectVariant){
            score += 1
        } else {
            if (variant.toLowerCase().includes('tb')) {
                variant = variant.replaceAll('TB', ' TB')
            }else if (variant.toLowerCase().includes('gb')){
                variant = variant.replaceAll('GB', ' GB')
            }
            let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
            
            if (hasCorrectVariant){
                score += 1
                
            }else{
                // break
                let btnFilteredTxt = btn.innerText.toLowerCase().replace(/\s+/g, "").replaceAll('-', '–')
                let variantFilteredTxt = variant.toLowerCase().replaceAll('wi-fi', 'wifi').replace(/\s+/g, "")
                console.log('1.', variant, ' ; ', variantFilteredTxt, ' ; ', btnFilteredTxt , ' ; ', btnFilteredTxt.search(variantFilteredTxt))
            }
        }
    }
    if (score == _variants.length){
        tileBtn = btn
        break
    }
}

// using includes
for (let btn of tileBtnElements){
    console.log(btn)
    let score = 0
    for (variant of _variants){
        let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
        if (hasCorrectVariant){
            score += 1
        } else {
            if (variant.toLowerCase().includes('tb')) {
                variant = variant.replaceAll('TB', ' TB')
            }else if (variant.toLowerCase().includes('gb')){
                variant = variant.replaceAll('GB', ' GB')
            }
            let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
            
            if (hasCorrectVariant){
                score += 1
                
            } else {
                let btnFilteredTxt = btn.innerText.toLowerCase().replaceAll('-', '–').replace(/\s+/g, "") // replace any whitespace
                let variantFilteredTxt = variant.toLowerCase().replaceAll('wi-fi', 'wifi').replace(/\s+/g, "") // // replace any whitespace
                console.log('1.', variant, ' ; ', variantFilteredTxt, ' ; ', btnFilteredTxt , ' ; ', btnFilteredTxt.includes(variantFilteredTxt))

                if (btnFilteredTxt.includes(variantFilteredTxt)) {
                    score += 1
                } else {
                    break
                }
            }
        }
    }
    if (score == _variants.length){
        tileBtn = btn
        break
    }
}