// ppt dev console tester 2 (microsoft-fr-fr)
// Variant
var variant = {"processor":"Intel Core M3 - Wi-Fi", "storage": "128 Go eMMC, 8 Go RAM", "color": "Platine"}

let idSelector = null
let stockId = null
let variantScreenSize = variant.screen_size
let variantProcessor = variant.processor
let variantStorage = variant.storage
let variantColor = variant.color               
let sizeBtnCliked = false
let colorBtnClicked = false
let configure_item = false
let hasProcessorTiles = false


let configElement = document.querySelector('ol.step-indicator')
if (configElement){
    configure_item = true
}

if (configure_item === true){
    //select size if present
    if (variant.hasOwnProperty('screen_size')) {
        let variantsize = variantScreenSize

        let text = variantsize.toLowerCase()
    
        let sizeBtnElement = document.querySelectorAll('section[aria-labelledby="tiles-title-0-0"] ul.v3tile li.mini.sku-spec')

        // if (sizeBtnElement.length <= 0){
        //     sizeBtnCliked = false
        // }
            
        if (sizeBtnElement.length > 0){
            for (let sizeBtn of sizeBtnElement){
                if (sizeBtn.innerText.toLowerCase().replace(/\s+/g, "").includes(text.replace(/\s+/g, ""))) {
                    if (sizeBtn.getAttribute('class').includes('is-checked')){
                        sizeBtnCliked = true
                    } else {
                        button_element = sizeBtn.querySelector('button')
                        if (button_element) {
                            button_element.click();
                            sizeBtnCliked = true
                        }
                    }
                }

            }
        }
    }

        // for processor configuration
    if (variant.hasOwnProperty('processor')) {

        let processor_section_indicator = document.querySelector('#tiles-title-0-0')
        if (processor_section_indicator.innerText.toLowerCase().replace(/\s+/g, "").includes(`processeur`)) { // indicator (https://prnt.sc/-YWGIYERLUDA)
            let processor_text = variantProcessor.replaceAll('-', '–').toLowerCase()
            let tiles = document.querySelectorAll('section[aria-labelledby="tiles-title-0-0"] > ul.v3tile > li.tile.mini.sku-spec')
            
            if (tiles.length > 0) {
                for (let x of tiles) {
                    let tile_text = x.innerText.toLowerCase().replaceAll('-', '–')

                    if (tile_text.includes(processor_text) ? true : false) { // processor from given variants matches to the processor tiles

                        if (x.getAttribute('aria-current') == 'false') { // processor tiles not clicked?
                            x.querySelector('button').click()
                            hasProcessorTiles = true
                        } else { // processor tiles already click?
                            hasProcessorTiles = true
                        }
                    }
                }
            }
        }
    }


    if (variant.hasOwnProperty('color') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('color')){
            
        let processor = null
        if (variantProcessor) {
            processor = variantProcessor.replaceAll('-', '–')
        }
        
        let storage = variantStorage.split(',')
        let color = variantColor
        let storageSsd = null
        let storageRam = null
        
        if (storage){
            if (storage.length >= 2){
                storageSsd = storage[0].trim()
                storageRam = storage[1].trim()
                storage = null
            }else {
                storage = storage[0]
            }
        }
        let variants = [processor, storage, storageSsd, storageRam]

        if (hasProcessorTiles == true) {
            variants = [storage, storageSsd, storageRam]
        }

        let _variants = []
        
        for (let variant of variants){
            if (variant){
                _variants.push(variant)
            }
        }

        console.log(_variants)
        
        //select color
        if (color && !hasProcessorTiles){
            let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
            if (colorBtnElement.length <= 0){
                colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')
            } else if ((colorBtnElement.length <= 0)) {
                colorBtnElement = document.querySelectorAll('div.c-group.f-wrap-items button')    
            }
            
            if (colorBtnElement.length <= 0){
                stockId = false
            }  
            if (colorBtnElement.length >0){
                for (let colorBtn of colorBtnElement){
                    if(colorBtn.parentElement.hasAttribute('class') && colorBtn.parentElement.getAttribute('class').includes('disabled')){
                        continue;
                    }
                    if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                        let colorBtnInfoElement = colorBtn.parentElement.querySelector('input')
                        if (colorBtnInfoElement.hasAttribute('checked')){
                            break;
                        }
                        colorBtn.click();
                        break;
                    }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                        let colorBtnInfoElement = colorBtn.parentElement.querySelector('input')
                        if (colorBtnInfoElement.hasAttribute('checked')){
                            break;
                        }
                        colorBtn.click();
                        break;
                    }
                }
            }
       }  
        
        let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
        }
        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li.mini.sku-spec')
        }
        let tileBtn = null
        if (tileBtnElements.length > 0){
            for (let btn of tileBtnElements){

                if (btn.hasAttribute('class')){
                    let removed_btn = btn.getAttribute('class').includes('removed')
                    if (removed_btn){
                        continue;
                    }
                }
                let score = 0
                for (variant of _variants){
                    let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                    if (hasCorrectVariant){
                        score += 1
                    } else {
                        if (variant.toLowerCase().includes('tb')) {
                            variant = variant.replaceAll('TB', ' TB')
                        }else if (variant.toLowerCase().includes('gb')){
                            variant = variant.replaceAll('GB', ' GB')
                        }
                        let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                        if (hasCorrectVariant){
                            score += 1
                            
                        }else{
                            break
                        }
                    }
                }
                if (score == _variants.length){
                    tileBtn = btn
                    break
                }
            }
        }
        

        if (tileBtn){
            if (tileBtn.hasAttribute('data-aid')){
                stockId = tileBtn.getAttribute('data-aid')
            }else if (tileBtn.hasAttribute('id')){
                stockId = tileBtn.getAttribute('id')
            }
        }
    }
    
    if (!stockId) {

    }

    if (variant.hasOwnProperty('uid') || stockId){
        console.log('Variant detected')
        let variantId = variant.uid
        // let idSelector = `.custom-control.custom-radio [for="${id}"]`

        // ui indicator
        let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

        if (stockId){
            if (stockId != variantId){
                variantId = stockId
            }
        }

        let colorRadioBtnSelector = null

        if (ui1IndicatorElement){

            colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
        
        }else {
            let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
            let selector2 = `li[id="${variantId}"]`
            let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
            

            if (document.querySelector(selector1)){
                colorRadioBtnSelector = selector1
            }
            else if (document.querySelector(selector2)){
                colorRadioBtnSelector = selector2
            }
            else {
                colorRadioBtnSelector = selector3
            }

        }
        
        if (colorRadioBtnSelector){
            let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
            if (colorRadioBtnElement){
                colorRadioBtnElement.click()
                idSelector = colorRadioBtnSelector
            }

        }
    
        if (ui1IndicatorElement){
            idSelector = null
        }
        idSelector = colorRadioBtnSelector
    }
}//END VARIANT SELECTION FOR CONFIGURE ITEMS

else {
    if (variant.hasOwnProperty('color')) {
        let variantColor = variant.color

        let color = variantColor
        
        let colorBtnElement = document.querySelectorAll('div.c-group.f-wrap-items button')     
        for (let colorBtn of colorBtnElement){
            if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                colorBtn.click()
                colorBtnClicked = true
            }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                colorBtn.click()
                colorBtnClicked = true
            }
        }
    }

    if (variant.hasOwnProperty('screen_size') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('color')) {

        if (!colorBtnClicked){
            variantColor = null
        }

        let size = variantScreenSize
        let processor = null
        if (variantProcessor){
            processor = variantProcessor.replaceAll('-', '–')
        }
        
        let storage = variantStorage.split(',')
        let color = variantColor
        let storageSsd = null
        let storageRam = null
        
        if (storage){
            if (storage.length >= 2){
                storageSsd = storage[0].trim()
                storageRam = storage[1].trim()
                storage = null
            }else {
                storage = storage[0]
            }
        }

        let variants = [size, processor, storage, color, storageSsd, storageRam]

        let _variants = []
        
        for (let variant of variants){
            if (variant){
                _variants.push(variant)
            }
        }

        let sizeBtnElement = document.querySelectorAll('.fc-richtext-button.c-button')

        if (sizeBtnElement.length <= 0){
            sizeBtnElement = document.querySelectorAll('[aria-labelledby="filterscreensize"] button')
        }
        if (sizeBtnElement.length <=0){
            sizeBtnElement = document.querySelectorAll('ul[class="v3tile action-hide"] li[class="tile mini sku-spec"]')
        }
        if (sizeBtnElement.length > 0){
            let text = null
            if (size) {
                text = size.toLowerCase()
            
            }else if (processor){
                text = processor.toLowerCase()    

            }else if (storage){
                text = storage.toLowerCase()
            }
            if (text){
                for (let sizeBtn of sizeBtnElement){
                    if (sizeBtn.innerText.toLowerCase().includes(text)) {
                        sizeBtn.click();
                        break;
                    }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
                        sizeBtn.click();
                        break;
                    }

                }
            }
        }
        
        let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
        }
        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li[class="tile mini sku-spec"]')
        }
        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('div[class="tileproductplacement mini c-group"] button')
        }
        let tileBtn = null
        for (let btn of tileBtnElements){
            let score = 0
            for (variant of _variants){
                let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                if (hasCorrectVariant){
                    score += 1
                } else {
                    if (variant.toLowerCase().includes('tb')) {
                        variant = variant.replaceAll('TB', ' TB')
                    }else if (variant.toLowerCase().includes('gb')){
                        variant = variant.replaceAll('GB', ' GB')
                    }
                    let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                    if (hasCorrectVariant){
                        score += 1
                        
                    }else{
                        break
                    }
                }
            }
            if (score == _variants.length){
                tileBtn = btn
                break
            }
        }

        if (tileBtn){
            if (tileBtn.hasAttribute('data-aid')){
                stockId = tileBtn.getAttribute('data-aid')
            }else if (tileBtn.hasAttribute('id')){
                stockId = tileBtn.getAttribute('id')
            }else if (tileBtn.hasAttribute('data-pid')){
                stockId = tileBtn.getAttribute('data-pid')
            }
        }
    }

    if (variant.hasOwnProperty('uid') || stockId){
        console.log('Variant detected')
        let variantId = variant.uid

        // ui indicator
        let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

        if (stockId){
            if (stockId != variantId){
                variantId = stockId
            }
        }

        let colorRadioBtnSelector = null

        if (ui1IndicatorElement){

            colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
        
        } else {
            let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
            let selector2 = `li[id="${variantId}"]`
            let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
            

            if (document.querySelector(selector1)){
                colorRadioBtnSelector = selector1
            }
            else if (document.querySelector(selector2)){
                colorRadioBtnSelector = selector2
            }
            else {
                colorRadioBtnSelector = selector3
            }

        }
        
        if (colorRadioBtnSelector){
            let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
            if (colorRadioBtnElement){
                colorRadioBtnElement.click()
                idSelector = colorRadioBtnSelector
            }

        }
    
        if (ui1IndicatorElement){
            idSelector = null
        }

        idSelector = colorRadioBtnSelector
    }
}

