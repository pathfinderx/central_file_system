const puppeteer = require('puppeteer')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { UnhandledStrategyError } = require('../errors')


class MicrosoftComFrFrWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout
                })

                let https_404_error = {'message': 'PDP Checker / 404 Error'}
                let redirected_to_category_error = {'message': 'PDP Checker / Redirected to category URL'}
                
                let isDeadLink = await page.evaluate(function(){
                        let message = document.querySelector('#mainContent .c-heading-3') || document.querySelector('#rootContainer_ConfiguratorV2 p')
                        let mainContent = document.querySelectorAll('#mainArea div')
                        if (mainContent.length == 0){
                            mainContent = document.querySelectorAll('#primaryArea div')
                        }
                        if (mainContent.length == 0){
                            mainContent = document.querySelectorAll('#mainContent div')
                        }
                        if (message){
                            if (message.innerText.toLowerCase().includes('page you requested cannot be found')){
                                return true
                            }
                            else{ 
                                if (message.innerText.toLowerCase().includes('für dieses produkt sind derzeit keine konfigurationen verfügbar.')){
                                    return true
                                }
                            }
                        }
                        else if (mainContent.length <= 3){
                            return true
                        }
                        return false
                })
                let hasRedirectedToCategory = await page.evaluate(function(){
                    let message = document.querySelector('#productPlacementList')

                    if (message){
                        return true
                    }
                    return false
            })

                if (isDeadLink){
                    throw new UnhandledStrategyError(https_404_error)

                }else if (hasRedirectedToCategory){
                    throw new UnhandledStrategyError(redirected_to_category_error)

                }

                try {
                    try{
                        await page.waitForSelector('.modal-content')
                        await page.click('.modal-content [aria-label="Close dialog window"]')
                    }catch (error){
                        await page.waitForSelector('.sfw-dialog [aria-label="Close"]')
                        await page.click('.sfw-dialog [aria-label="Close"]')
                    }
                    console.log('Modal closed')
                    
                } catch (error) {
                    console.log('Modal element does not exist: '+ String(error))
                }

                // Variant
                let idSelector = null
                let stockId = null
                let variantScreenSize = variant.screen_size
                let variantProcessor = variant.processor
                let variantStorage = variant.storage
                let variantColor = variant.color               
                let sizeBtnCliked = false
                let colorBtnClicked = false
                let configure_item = false

                try {

                    //configure item indicator
                    configure_item = await page.evaluate(function(){
                    
                        let configElement = document.querySelector('ol.step-indicator')
                        if (configElement){
                            return true
                        }

                        
                    })

                    //select variant here if item is configure
                    if (configure_item === true){
                        //select size if present
                        if (variant.hasOwnProperty('screen_size')) {
                            let variantsize = variantScreenSize

                            sizeBtnCliked = await page.evaluate(function(variantsize){
                                let text = variantsize.toLowerCase()
                            
                                let sizeBtnElement = document.querySelectorAll('section[aria-labelledby="tiles-title-0-0"] ul.v3tile li.mini.sku-spec')

                                if (sizeBtnElement.length <= 0){
                                    return false
                                }
                                    
                                if (sizeBtnElement.length > 0){
                                    for (let sizeBtn of sizeBtnElement){
                                        if (sizeBtn.innerText.toLowerCase().replace(/\s+/g, "").includes(text.replace(/\s+/g, ""))) {
                                            if (sizeBtn.getAttribute('class').includes('is-checked')){
                                                return true
                                            } else {
                                                button_element = sizeBtn.querySelector('button')
                                                if (button_element) {
                                                    button_element.click();
                                                    return true
                                                }
                                            }
                                        }
                        
                                    }
                                }
                            }, variantsize)
                        }
                    
                        if (variant.hasOwnProperty('color') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('color')){
                        
                            stockId = await page.evaluate(function(variantProcessor, variantStorage, variantColor){
                                
                                let processor = null
                                if (variantProcessor){
                                    processor = variantProcessor.replaceAll('-', '–')
                                }
                                
                                let storage = variantStorage.split(',')
                                let color = variantColor
                                let storageSsd = null
                                let storageRam = null
                                
                                if (storage){
                                    if (storage.length >= 2){
                                        storageSsd = storage[0].trim()
                                        storageRam = storage[1].trim()
                                        storage = null
                                    }else {
                                        storage = storage[0]
                                    }
                                }
                                let variants = [processor, storage, storageSsd, storageRam]

                                let _variants = []
                                
                                for (let variant of variants){
                                    if (variant){
                                        _variants.push(variant)
                                    }
                                }
                                
                                //select color
                                if (color){
                                    let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
                                    if (colorBtnElement.length <= 0){
                                        colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')

                                    }
                                    if (colorBtnElement.length <= 0){
                                        return false
                                    }  
                                    if (colorBtnElement.length >0){
                                        for (let colorBtn of colorBtnElement){
                                            if(colorBtn.parentElement.hasAttribute('class') && colorBtn.parentElement.getAttribute('class').includes('disabled')){
                                                continue;
                                            }
                                            if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                                                let colorBtnInfoElement = colorBtn.parentElement.querySelector('input')
                                                if (colorBtnInfoElement.hasAttribute('checked')){
                                                    break;
                                                }
                                                colorBtn.click();
                                                break;
                                            }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                                                let colorBtnInfoElement = colorBtn.parentElement.querySelector('input')
                                                if (colorBtnInfoElement.hasAttribute('checked')){
                                                    break;
                                                }
                                                colorBtn.click();
                                                break;
                                            }
                                        }
                                    }
                               }  
                                
                                let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

                                if (tileBtnElements.length <= 0){
                                    tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
                                }
                                if (tileBtnElements.length <= 0){
                                    tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li.mini.sku-spec')
                                }
                                let tileBtn = null
                                if (tileBtnElements.length > 0){
                                    for (let btn of tileBtnElements){
        
                                        if (btn.hasAttribute('class')){
                                            let removed_btn = btn.getAttribute('class').includes('removed')
                                            if (removed_btn){
                                                continue;
                                            }
                                        }
                                        let score = 0
                                        for (variant of _variants){
                                            let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                                            if (hasCorrectVariant){
                                                score += 1
                                            } else {
                                                if (variant.toLowerCase().includes('tb')) {
                                                    variant = variant.replaceAll('TB', ' TB')
                                                }else if (variant.toLowerCase().includes('gb')){
                                                    variant = variant.replaceAll('GB', ' GB')
                                                }
                                                let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                                                if (hasCorrectVariant){
                                                    score += 1
                                                    
                                                }else{
                                                    break
                                                }
                                            }
                                        }
                                        if (score == _variants.length){
                                            tileBtn = btn
                                            break
                                        }
                                    }
                                }
                                

                                if (tileBtn){
                                    if (tileBtn.hasAttribute('data-aid')){
                                        return tileBtn.getAttribute('data-aid')
                                    }else if (tileBtn.hasAttribute('id')){
                                        return tileBtn.getAttribute('id')
                                    }
                                }
                                
                            }, variantProcessor, variantStorage, variantColor)

                        }
                        
                        if (!stockId) {

                        }

                        if (variant.hasOwnProperty('uid') || stockId){
                            console.log('Variant detected')
                            let variantId = variant.uid
                            // let idSelector = `.custom-control.custom-radio [for="${id}"]`
        
                            idSelector = await page.evaluate(function(variantId, stockId){
                                // ui indicator
                                let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

                                if (stockId){
                                    if (stockId != variantId){
                                        variantId = stockId
                                    }
                                }

                                let colorRadioBtnSelector = null
        
                                if (ui1IndicatorElement){
        
                                    colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
                                
                                }else {
                                    let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
                                    let selector2 = `li[id="${variantId}"]`
                                    let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
                                    

                                    if (document.querySelector(selector1)){
                                        colorRadioBtnSelector = selector1
                                    }
                                    else if (document.querySelector(selector2)){
                                        colorRadioBtnSelector = selector2
                                    }
                                    else {
                                        colorRadioBtnSelector = selector3
                                    }

                                }
                                
                                if (colorRadioBtnSelector){
                                    let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
                                    if (colorRadioBtnElement){
                                        colorRadioBtnElement.click()
                                        return colorRadioBtnSelector
                                    }
        
                                }
                            
                                if (ui1IndicatorElement){
                                    return null
                                }
                                return colorRadioBtnSelector
        
                            }, variantId, stockId)
        
                        }
                    }//END VARIANT SELECTION FOR CONFIGURE ITEMS

                    else{
                        //VARIANT SELECTION FOR NON CONFIGURE ITEMS
                    
                    
                        if (variant.hasOwnProperty('color')) {
                            let variantColor = variant.color
                            
                            
                            colorBtnClicked = await page.evaluate(function(variantColor, variant){
                                let color = variantColor
                                

                                let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
                                if (colorBtnElement.length <= 0){
                                    colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')

                                }
                                if (colorBtnElement.length <= 0){
                                    return false
                                }                
                                for (let colorBtn of colorBtnElement){
                                    if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                                        colorBtn.click()
                                        return true
                                    }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                                        colorBtn.click()
                                        return true
                                    }
                                }

                                                        

                            }, variantColor, variant)
                        }
                        if (variant.hasOwnProperty('screen_size') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('color')){

                            if (!colorBtnClicked){
                                variantColor = null
                            }
                            stockId = await page.evaluate(function(variantScreenSize, variantProcessor, variantStorage, variantColor){
                                let size = variantScreenSize
                                let processor = null
                                if (variantProcessor){
                                    processor = variantProcessor.replaceAll('-', '–')
                                }
                                
                                let storage = variantStorage.split(',')
                                let color = variantColor
                                let storageSsd = null
                                let storageRam = null
                                
                                if (storage){
                                    if (storage.length >= 2){
                                        storageSsd = storage[0].trim()
                                        storageRam = storage[1].trim()
                                        storage = null
                                    }else {
                                        storage = storage[0]
                                    }
                                }
                                let variants = [size, processor, storage, color, storageSsd, storageRam]

                                let _variants = []
                                
                                for (let variant of variants){
                                    if (variant){
                                        _variants.push(variant)
                                    }
                                }


                                let sizeBtnElement = document.querySelectorAll('.fc-richtext-button.c-button')

                                if (sizeBtnElement.length <= 0){
                                    sizeBtnElement = document.querySelectorAll('[aria-labelledby="filterscreensize"] button')
                                }
                                if (sizeBtnElement.length <=0){
                                    sizeBtnElement = document.querySelectorAll('ul[class="v3tile action-hide"] li[class="tile mini sku-spec"]')
                                }
                                if (sizeBtnElement.length > 0){
                                    let text = null
                                    if (size) {
                                        text = size.toLowerCase()
                                    
                                    }else if (processor){
                                        text = processor.toLowerCase()    

                                    }else if (storage){
                                        text = storage.toLowerCase()
                                    }
                                    if (text){
                                        for (let sizeBtn of sizeBtnElement){
                                            if (sizeBtn.innerText.toLowerCase().includes(text)) {
                                                sizeBtn.click();
                                                break;
                                            }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
                                                sizeBtn.click();
                                                break;
                                            }

                                        }
                                    }
                                }
                                
                                let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

                                if (tileBtnElements.length <= 0){
                                    tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
                                }
                                if (tileBtnElements.length <= 0){
                                    tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li[class="tile mini sku-spec"]')
                                }
                                if (tileBtnElements.length <= 0){
                                    tileBtnElements = document.querySelectorAll('div[class="tileproductplacement mini c-group"] button')
                                }
                                let tileBtn = null
                                for (let btn of tileBtnElements){
                                    let score = 0
                                    for (variant of _variants){
                                        let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                                        if (hasCorrectVariant){
                                            score += 1
                                        } else {
                                            if (variant.toLowerCase().includes('tb')) {
                                                variant = variant.replaceAll('TB', ' TB')
                                            }else if (variant.toLowerCase().includes('gb')){
                                                variant = variant.replaceAll('GB', ' GB')
                                            }
                                            let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
                                            if (hasCorrectVariant){
                                                score += 1
                                                
                                            }else{
                                                break
                                            }
                                        }
                                    }
                                    if (score == _variants.length){
                                        tileBtn = btn
                                        break
                                    }
                                }

                                if (tileBtn){
                                    if (tileBtn.hasAttribute('data-aid')){
                                        return tileBtn.getAttribute('data-aid')
                                    }else if (tileBtn.hasAttribute('id')){
                                        return tileBtn.getAttribute('id')
                                    }else if (tileBtn.hasAttribute('data-pid')){
                                        return tileBtn.getAttribute('data-pid')
                                    }
                                }
                                
                            }, variantScreenSize, variantProcessor, variantStorage, variantColor)

                        }
                        
                        if (!stockId) {

                        }

                        if (variant.hasOwnProperty('uid') || stockId){
                            console.log('Variant detected')
                            let variantId = variant.uid
                            // let idSelector = `.custom-control.custom-radio [for="${id}"]`
        
                            idSelector = await page.evaluate(function(variantId, stockId){
                                // ui indicator
                                let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

                                if (stockId){
                                    if (stockId != variantId){
                                        variantId = stockId
                                    }
                                }

                                let colorRadioBtnSelector = null
        
                                if (ui1IndicatorElement){
        
                                    colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
                                
                                }else {
                                    let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
                                    let selector2 = `li[id="${variantId}"]`
                                    let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
                                    

                                    if (document.querySelector(selector1)){
                                        colorRadioBtnSelector = selector1
                                    }
                                    else if (document.querySelector(selector2)){
                                        colorRadioBtnSelector = selector2
                                    }
                                    else {
                                        colorRadioBtnSelector = selector3
                                    }

                                }
                                
                                if (colorRadioBtnSelector){
                                    let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
                                    if (colorRadioBtnElement){
                                        colorRadioBtnElement.click()
                                        return colorRadioBtnSelector
                                    }
        
                                }
                            
                                if (ui1IndicatorElement){
                                    return null
                                }
                                return colorRadioBtnSelector
        
                            }, variantId, stockId)
        
                        }
                    }
                    
                } catch (error) {
                    
                    console.log(error)
                }
                
                // Wait for a button to fully load
                // to get the correct status of the 
                // button (enabled/disables)
                try {
                    let purchaseBtnSelector = await page.evaluate(() => {
                        let selector = '.c-call-to-action.c-glyph.purchButton'
                        if (document.querySelector(selector)){
                            return selector
                        }
                        return null
                    })

                    if (purchaseBtnSelector){
                        await page.waitForTimeout(5000)
                        await page.waitForSelector(purchaseBtnSelector)
                    }
                } catch (error) {
                    console.log(error)
                }

                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages, idSelector){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let priceElement = null
                        let selectors = null
                        
                        if (idSelector){

                            let drawerElement = document.querySelector('#drawer-0') || document.querySelector('#drawer0 button')
                            if (drawerElement) {
                                if (drawerElement.hasAttribute('aria-expanded') && drawerElement.getAttribute('aria-expanded').toLowerCase() == 'false') {
                                    try{
                                        document.querySelector('#drawer-0 .drawertitle.c-glyph').click()
                                    }catch (error){
                                        drawerElement.click()
                                    }
                                    
                                }
                            }


                            selectors = [
                                `${idSelector} .module-standard-price span`,
                                `${idSelector} span.x-hidden-focus`,
                                `${idSelector} span:nth-child(4)`,
                                `${idSelector} div.v3tile__tilefooter span`,
                                '.h3.font-weight-normal span:nth-of-type(2)'
            
                                
                            ]

                        }else{
                            selectors = [
                                '.col-lg-3.buy-box-right-col span.x-hidden-focus',
                                '.pb-4.pr-lg-4 p .font-weight-semibold',
                                'div[data-reactroot] div.pi-price-text span',
                                '.price-xaa-lc'
                            ]
                        }

                        for (let selector of selectors){
                            if (document.querySelector(selector)){
                                priceElement = document.querySelector(selector)
                                break;
                            }
                        }
                        if (priceElement) {
                            outerHTML = priceElement.outerHTML
                            innerText = priceElement.innerText.trim()
                        }

                        if (innerText.length <= 0){
                            outerHTML = FailureMessages.GENERIC_NOT_FOUND
                            innerText = FailureMessages.GENERIC_NOT_FOUND
                        }



                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages, idSelector);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let titleElement = document.querySelector('#rootContainer_BuyBox .h2') || document.querySelector('#DynamicHeading_productTitle') || document.querySelector('div.configuratorV3-title h1.x-hidden-focus') || document.querySelector('div.configuratorV3-title h1') || document.querySelector('h1#title') || document.querySelector('.c-heading-4');

                        if (titleElement){
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.TITLE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('.glyph_text') || document.querySelector('span[itemprop="name"]');

                        if (brandElement){
                            innerText = brandElement.textContent.trim()
                            outerHTML = brandElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                result.currency.value = 'USD'
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let showMoreButtonElement = document.querySelector('[data-mount="showMoreShowLess"]');

                        if (showMoreButtonElement){
                            showMoreButtonElement.click()
                        }

                        let descriptionElement = document.querySelector('[data-container="showMoreShowLess"] div');
                        
                        if (descriptionElement){
                            outerHTML = descriptionElement.outerHTML
                            innerText = descriptionElement.innerText.trim()
                            
                        }
                    } catch (error) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SCPECIFICATIONS
                let specificationsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try{
                        let specTabElement = document.querySelector('a[data-bi-ecn="Tech specs"]');

                        if (specTabElement) {
                            specTabElement.click()
                        }


                        let keyElements = document.querySelectorAll('.table-first-col-highlight tbody th');
                        let valueElements = document.querySelectorAll('.table-first-col-highlight tbody td');
                        for (let idx = 0; idx < keyElements.length; idx++){
                            valuesMap[keyElements[idx].innerText.trim()] = valueElements[idx].innerText.trim()
                            outerHTML = keyElements[idx].outerHTML
                        }
                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages, idSelector){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        if (idSelector){

                            // let stockSelector = `${idSelector} .badge.text-white.bg-black.px-1`
                            // let stockElement = document.querySelector(stockSelector)

                            // if (!stockElement){
                            //     stockElement = document.querySelector('button.c-button.f-primary')
                            // }
                            // if (stockElement) {
                            //     outerHTML = stockElement.outerHTML
                            //     if (stockElement.hasAttribute('disabled')){
                            //         innerText = 'disabled'
                            //     } else {
                            //         innerText = 'enabled'
                            //     }
                            // }
                            let stockElement = document.querySelector(idSelector)

                            if (stockElement){
                                if (stockElement.hasAttribute('disabled' ) || stockElement.hasAttribute('aria-disabled')){
                                    outerHTML = stockElement.outerHTML
                                    innerText = 'disabled'
                                    
                                }else {
                                    innerText = 'enabled'
                                }
                            }

                        }else {
                            let addToCartButtonElement = null
                            try {
                                addToCartButtonElement = document.querySelector('#rootContainer_BuyBox .btn.btn-primary.btn-block') || document.querySelector('.tileproductplacement.mini.c-group button') || document.querySelector('.tileproductplacement.mini button') || document.querySelector('#ButtonPanel_buttons button') || document.querySelector('.c-call-to-action.c-glyph.purchButton') || document.querySelector('fieldset.form-group').nextElementSibling
                            } catch (error) {
                                addToCartButtonElement = document.querySelector('.pi-overflow-ctrl button') || document.querySelector('.PricingMessage')
                            }

                            if (addToCartButtonElement){
                                outerHTML = addToCartButtonElement.outerHTML
                                if (addToCartButtonElement.hasAttribute('disabled')){
                                    innerText = 'disabled'
                                    if (addToCartButtonElement.getAttribute('disabled').toLowerCase().includes('disabled')){
                                        innerText = 'disabled'
                                    }

                                }else if (addToCartButtonElement.hasAttribute('aria-disabled')){
                                    if (addToCartButtonElement.getAttribute('aria-disabled').toLowerCase().includes('false')) {
                                        innerText = 'enabled'
                                    } else if (addToCartButtonElement.getAttribute('aria-disabled').toLowerCase().includes('true')) {
                                        innerText = 'disabled'
                                    } 
                                }else if (addToCartButtonElement.hasAttribute('disabled')){
                                    if (addToCartButtonElement.getAttribute('disabled').toLowerCase().includes('true')){
                                        innerText = 'disabled'
                                    }
                                }else if (addToCartButtonElement.hasAttribute('data-bi-name')){
                                    if (addToCartButtonElement.getAttribute('data-bi-name').toLowerCase().includes('sold out')) {
                                        innerText = 'disabled'
                                    }
                                }else if (addToCartButtonElement.innerText.toLowerCase().includes('not available')) {
                                    innerText = 'disabled'
                                }
                                else if (addToCartButtonElement.innerText.toLowerCase().includes('verfügbar')) {
                                    innerText = 'disabled'
                                }
                                else {
                                    

                                    outerHTML = addToCartButtonElement.outerHTML
                                    innerText = 'enabled'

                                }
                            }
                        }
                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages, idSelector);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML


                // RATING

                result.rating.score.value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.score.max_value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.score.source = FailureMessages.GENERIC_NOT_FOUND                          


                // REVIEWS
  
                result.rating.reviews.value = FailureMessages.GENERIC_NOT_FOUND
                result.rating.reviews.source = FailureMessages.GENERIC_NOT_FOUND
   

                
                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    try{
                        let buttonModalElement = document.querySelector('.glyph-prepend-photo-collection');

                        if (buttonModalElement){
                            buttonModalElement.click()
                        }

                        let imageElements = document.querySelectorAll('.modal-content img');

                        if (imageElements.length <= 0){
                            imageElements = document.querySelectorAll('section[aria-label="Product image"] img')
                        }
                        if (imageElements.length <= 0){
                            imageElements = document.querySelectorAll('#image-slide-0 img')
                        }      
                        
                        if (imageElements.length <= 0){
                            imageElements = document.querySelectorAll('.pi-product-image img')
                        }    

                        for(let img of imageElements){
                            images.push(img.getAttribute('src'))
                            outerHTML = img.outerHTML

                        }

                        let closeButtonElement = document.querySelector('button[aria-label="Close dialog window"]');

                        if (closeButtonElement) {
                            closeButtonElement.click()
                        }


                    }catch(error){
                        images = FailureMessages.IMAGE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML
                
                // VIDEO
                let videosResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let videos = []

                    return { videos, outerHTML }
                }, FailureMessages);

                result.video_urls.value = videosResult.videos
                result.video_urls.source = videosResult.outerHTML

                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */
                
                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = MicrosoftComFrFrWebsiteStrategy