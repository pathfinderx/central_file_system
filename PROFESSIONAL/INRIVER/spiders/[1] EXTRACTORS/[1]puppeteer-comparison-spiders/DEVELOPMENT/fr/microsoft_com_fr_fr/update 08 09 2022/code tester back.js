// printing variant size
for (let sizeBtn of sizeBtnElement){
    console.log(sizeBtn)
    console.log(sizeBtn.innerText.toLowerCase(), text)
    if (sizeBtn.innerText.toLowerCase().includes(text)) {
        if (sizeBtn.getAttribute('class').includes('is-checked')){
            sizeBtnCliked = true
        }
        sizeBtn.click();
        sizeBtnCliked = true
    }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
        if (sizeBtn.getAttribute('class').includes('is-checked')){
            sizeBtnCliked = true
        }
        sizeBtn.click();
        sizeBtnCliked = true
    }
}

// the seemingly equal string but not equal: ref (https://stackoverflow.com/questions/54195474/two-seemingly-equal-strings-not-equal-in-javascript)
// to resolve: remove the whitespaces
String(theInnerText.replace(/\s+/g, "")) === String(text.replace(/\s+/g, ""))


// Variant
var variant = {"processor": "AMD Ryzen™ 7 4980U", "storage": "512 Go SSD, 16 Go RAM", "screen_size": "15 pouces", "color": "Noir mat"}

let idSelector = null
let stockId = null
let variantScreenSize = variant.screen_size
let variantProcessor = variant.processor
let variantStorage = variant.storage
let variantColor = variant.color               
let sizeBtnCliked = false
let colorBtnClicked = false
let configure_item = false

let configElement = document.querySelector('ol.step-indicator')
if (configElement){
    configure_item = configElement = true
}

if (configure_item === true){
    //select size if present
    if (variant.hasOwnProperty('screen_size')) {
        let variantsize = variantScreenSize

        let text = variantsize.toLowerCase()
    
        let sizeBtnElement = document.querySelectorAll('section[aria-labelledby="tiles-title-0-0"] ul.v3tile li.mini.sku-spec ')

        if (sizeBtnElement.length <= 0){
            sizeBtnCliked = false
        }
            
        if (sizeBtnElement.length > 0){
            for (let sizeBtn of sizeBtnElement){
                if (sizeBtn.innerText.toLowerCase().replace(/\s+/g, "").includes(text.replace(/\s+/g, ""))) {
                    if (sizeBtn.getAttribute('class').includes('is-checked')){
                        sizeBtnCliked = true
                    } else {
                        button_element = sizeBtn.querySelector('button')
                        if (button_element) {
                            button_element.click();
                            sizeBtnCliked = true
                        }
                    }
                }

            }
        }
    }
}