var variant = {"storage":"128 Go"}

let idSelector = null
let stockId = null
let variantScreenSize = variant.screen_size
let variantProcessor = variant.processor
let variantStorage = variant.storage
let variantColor = variant.color       
let colorBtnClicked = false

let color = variantColor
// var variant = {"processor": "SQ2", "storage": "256GB SSD, 16GB RAM", "color": "Platine"}
// let color = variant['color']
let colorBtnElement = document.querySelectorAll('div.c-group.f-wrap-items button')
for (let colorBtn of colorBtnElement) {
    if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
        colorBtn.click()
        colorBtnClicked =  true
    }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
        colorBtn.click()
        colorBtnClicked = true
    }
}


let size = variantScreenSize
let processor = null
if (variantProcessor){
    processor = variantProcessor.replaceAll('-', '–')
}

let storage = variantStorage.split(',')
let storageSsd = null
let storageRam = null

if (storage){
    if (storage.length >= 2){
        storageSsd = storage[0].trim()
        storageRam = storage[1].trim()
        storage = null
    }else {
        storage = storage[0]
    }
}

let variants = [size, processor, storage, color, storageSsd, storageRam]

let _variants = []

for (let variant of variants){
    if (variant){
        _variants.push(variant)
    }
}

let sizeBtnElement = document.querySelectorAll('.fc-richtext-button.c-button')

if (sizeBtnElement.length <= 0){
    sizeBtnElement = document.querySelectorAll('[aria-labelledby="filterscreensize"] button')
}

let text = null
if (size) {
    text = size.toLowerCase()

}else if (processor){
    text = processor.toLowerCase()    

}else if (storage){
    text = storage.toLowerCase()
}
if (text){
    for (let sizeBtn of sizeBtnElement){
        if (sizeBtn.innerText.toLowerCase().includes(text)) {
            sizeBtn.click();
            break;
        }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
            sizeBtn.click();
            break;
        }

    }
}

let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

if (tileBtnElements.length <= 0){
    tileBtnElements = document.querySelector('.tileproductplacement.mini button.listboxitem-mini') ? document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini') : document.querySelectorAll('li.tile.mini.sku-spec')

}

let tileBtn = null
for (let btn of tileBtnElements){
    let score = 0
    for (variant of _variants){
        let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
        if (hasCorrectVariant){
            score += 1
        } else {
            if (variant.toLowerCase().includes('tb')) {
                variant = variant.replaceAll('TB', ' TB')
            }else if (variant.toLowerCase().includes('gb')){
                variant = variant.replaceAll('GB', ' GB')
            }
            let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
            if (hasCorrectVariant){
                score += 1
                
            }else{
                break
            }
        }
    }
    if (score == _variants.length){
        tileBtn = btn
        break
    }
}

if (tileBtn){
    if (tileBtn.hasAttribute('data-aid')){
        stockId = tileBtn.getAttribute('data-aid')
    }else if (tileBtn.hasAttribute('id')){
        stockId = tileBtn.getAttribute('id')
    }
}


if (variant.hasOwnProperty('uid') || stockId){
    console.log('Variant detected')
    let variantId = variant.uid

    let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

    if (stockId){
        if (stockId != variantId){
            variantId = stockId
        }
    }

    let colorRadioBtnSelector = null

    if (ui1IndicatorElement){

        colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
       
    }else {
        let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
        let selector2 = `.tileproductplacement button[data-aid="${variantId}"]`

        if (document.querySelector(selector1)){
            colorRadioBtnSelector = selector1
        }else {
            colorRadioBtnSelector = selector2
        }

    }
    
    if (colorRadioBtnSelector){
        let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
        if (colorRadioBtnElement){
            colorRadioBtnElement.click()
            idSelector =  colorRadioBtnSelector
        }

    }

    if (ui1IndicatorElement){
        idSelector =  null
    }
    idSelector = colorRadioBtnSelector
}


////////// cache
// for (let btn of tileBtnElements){
//     let score = 0
//     console.log(btn)
//     for (let variant of _variants) {
//         let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
//         console.log(hasCorrectVariant)
//     }
// }

// for (let btn of tileBtnElements){
//     let score = 0
//     console.log(btn.innerText.toLowerCase().replaceAll('-', '–'))
//     for (let variant of _variants) {
//         // let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
//         // console.log(hasCorrectVariant)
//         console.log(variant.toLowerCase().replaceAll('wi-fi', 'wifi'), btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi')))  
//     }
//     console.log('-')
// }


// let tileBtn = null
// for (let btn of tileBtnElements){
//     let score = 0
//     console.log('site variant:', btn.innerText.toLowerCase().replaceAll('-', '–'))
//     for (variant of _variants){
//         let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
//         console.log(variant.toLowerCase().replaceAll('wi-fi', 'wifi'), btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi')))  
//         if (hasCorrectVariant){
//             score += 1
//         } else {
//             if (variant.toLowerCase().includes('tb')) {
//                 variant = variant.replaceAll('TB', ' TB')
//             } else if (variant.toLowerCase().includes('gb')){
//                 variant = variant.replaceAll('GB', ' GB')
//             } else if (variant.toLowerCase().includes('go')) {
//                 variant = variant.replaceAll('GO', ' Go')
//             }
//             let hasCorrectVariant = btn.innerText.toLowerCase().replaceAll('-', '–').includes(variant.toLowerCase().replaceAll('wi-fi', 'wifi'))
//             if (hasCorrectVariant){
//                 score += 1
                
//             }else{
//                 break
//             }
//         }
//     }
//     if (score == _variants.length){
//         tileBtn = btn
//         break
//     }
// }

// // FOR LIST SPECIAL CASE AS SUBSTITUTE FOR BTN
// tileBtnElements = document.querySelector('.tileproductplacement.mini button.listboxitem-mini') ? document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini') : document.querySelectorAll('li.tile.mini.sku-spec > div.v3tile__tilebody')

// for (let btn of tileBtnElements) {
//     console.log(btn)
//     // for (let x = 0; x <= btn.childNodes.length -1; ++x) {
//     //     // console.log(btn.childNodes[x])
//     // }
// }