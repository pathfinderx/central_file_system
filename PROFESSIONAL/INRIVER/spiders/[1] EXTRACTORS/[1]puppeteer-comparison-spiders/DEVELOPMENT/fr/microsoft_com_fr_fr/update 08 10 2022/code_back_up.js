for (let colorBtn of colorBtnElement){
    if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
        colorBtn.click()

        // Check if color is not present in tiles (https://prnt.sc/3IfpVKUB8t0t)
        let tile_options = document.querySelectorAll('ul[class="v3tile"] li[class="tile mini sku-spec"]')
        if (tile_options) {

            let color_in_selection = colorBtn.getAttribute('title').toLowerCase()
            for (let x of tile_options) {

                // assuming every tiles shares common properties when color button is clicked. So, only 1 tile required for matching
                console.log(!x.innerText.includes(color_in_selection) ? true : false)
                break
            }
        }

    }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
        colorBtn.click()
    }
}