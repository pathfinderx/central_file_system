
require('dotenv').config()
const models = require('./models')
const utils = require('./utils')
const StrategyFactory = require('./strategies/factory')
const fs = require('fs')
function resultValueOnly(result) {
    result.third_parties.forEach(function(thirdParty){
        console.log('--------------------------')
        console.log('Rank: ' + thirdParty.rank)
        console.log('Is direct seller: ' + thirdParty.is_direct_seller)
        console.log('Price: ' + thirdParty.price.value)
        console.log('Currency: ' + thirdParty.currency.value)
        console.log('Retailer name: ' + thirdParty.retailer_name.value)
        console.log('Retailer url: ' + thirdParty.retailer_url.value)
        console.log('Logo: ' + thirdParty.logo.value)
        console.log('Rating: ' + thirdParty.rating.score.value)
        console.log('Reviews: ' + thirdParty.rating.reviews.value)
        console.log('Url: ' + thirdParty.url.value)
        console.log('Description: ' + thirdParty.description.value)
        console.log('Availability: ' + thirdParty.availability.value)
        console.log('Condition: ' + thirdParty.condition.value)
        console.log('Delivery: ' + thirdParty.delivery.value)
        console.log('Shipping: ' + thirdParty.shipping.value)
        console.log('--------------------------')
    })
    return {
        price: result.price.value,
        price_listed: result.price_listed.value,
        currency: result.currency.value,
        availability: result.availability.value,
        rating: {
            score: result.rating.score.value,
            reviews: result.rating.reviews.value,
            max_value: result.rating.score.max_value
        },
        brand: result.brand.value,
        title: result.title.value,
        description: result.description.value,
        specifications: result.specifications.value,
        image_urls: result.image_urls.value,
        video_urls: result.video_urls.value,
        delivery: result.delivery.value,
        shipping: result.shipping.value,
        // third_parties: result.third_parties,
        buybox: result.buybox
    }
}
async function testStrategy() {
    var url = 'https://www.microsoft.com/fr-fr/store/configure/Surface-Laptop-4/946627fb12t1?crosssellid=&selectedColor=9A9CA3'
    var website = 'www.microsoft.com/fr-fr'
    var countryCode = 'fr'

    var browserEndpoint = process.env.BROWSER_ENDPOINT
    var timeout = 150000//process.env.BROWSER_TIMEOUT
    var userAgent = 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'
    var isHeadless = false;
    var useLocalBrowser = true;

    var variant = {"processor": "AMD Ryzen™ 7 4980U", "storage": "512 Go SSD, 8 Go RAM", "screen_size": "15 pouces", "color": "Platine"}

    console.log('[*] Init proxy')
    var proxyConnection = new models.ProxyConnection(process.env.LUMINATI_SERVER, process.env.LUMINATI_USER + countryCode.toLowerCase(), process.env.LUMINATI_PWD)
    console.log('[*] Identifying strategy')
    var strategyFactory = new StrategyFactory()
    var StrategyContext = strategyFactory.getWebsiteStrategy(countryCode.toLowerCase(), website)
    if (!StrategyContext) {
        console.error('[x] Strategy identification failed')
    }
    else {
        var strategy = new StrategyContext(browserEndpoint, isHeadless, proxyConnection)
        // var strategy = new StrategyContext(browserEndpoint, false, proxyConnection)
        console.log('[*] Executing strategy')
        var [strategyError, strategyResult] = await utils.tryCatchPromiseWrapper(strategy.execute(url, userAgent, timeout, useLocalBrowser, variant))
        if (strategyError) {
            console.error(`[x] Strategy execution failed: ${strategyError}`)
        }
        else {
            console.log(resultValueOnly(strategyResult.result))
            fs.writeFileSync('../be-comparison-et/et_test.json', JSON.stringify(strategyResult.result), function(error){
            });
        }
    }
}

testStrategy()