let variant = {"processor": "Intel Core i7", "storage": "2TB SSD, 32GB RAM", "color": "Platinum"}


let idSelector = null
let stockId = null
let variantScreenSize = variant.screen_size
let variantProcessor = variant.processor
let variantStorage = variant.storage
let variantColor = variant.color               
let variantLanguage = variant.language
let sizeBtnCliked = false
let colorBtnClicked = false
let configure_item = false       

let configElement = document.querySelector('ol.step-indicator')
if (configElement){
    configure_item = true
}

if (configure_item === true){
    if (variantScreenSize) {
        try {
            let isScreeSizeFoundAndClicked = false
            let tileBtnElements = document.querySelectorAll('.v3tile.action-hide .tile__button')
            for (let tileBtn of tileBtnElements){
                if (tileBtn.hasAttribute('data-m')){
                    let tileInfo = JSON.parse(tileBtn.getAttribute('data-m'))
                    try{
                        if (variantScreenSize.toLowerCase() == tileInfo.cN.toLowerCase()){
                            if (!tileBtn.parentElement.getAttribute('class').toLowerCase().includes('is-checked')){
                                tileBtn.click()
                            }
                            isScreeSizeFoundAndClicked = true
                        }
                    }catch (e) {
                        isScreeSizeFoundAndClicked = false
                    }
                }
            }
        } catch (e) {
            console.log('Unable to click on screen size tile')
        }
    }


    if (variant.hasOwnProperty('color') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('language')){
        let processor = null
        if (variantProcessor){
            processor = variantProcessor.replaceAll('-', '–')
        }

        let storage = (variantStorage == null ? null : variantStorage.split(','))
        let color = variantColor
        let storageSsd = null
        let storageRam = null
        let language = variantLanguage
    

        if (storage){
            if (storage.length >= 2){
                storageSsd = storage[0].trim()
                storageRam = storage[1].trim()
                storage = null
            }else {
                storage = storage[0]
            }
        }
        let variants = [language, processor, storage, storageSsd, storageRam]

        console.log('variants', variants)

        let _variants = []
        
        for (let variant of variants){
            if (variant){
                _variants.push(variant)
            }
        }

        console.log('_variants ', _variants)

        //select color
        if (color){
            let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
            if (colorBtnElement.length <= 0){
                colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')

            }
            if (colorBtnElement.length <= 0){
                stockId =  false
            }  
            if (colorBtnElement.length >0){
                console.log(colorBtnElement)
                for (let colorBtn of colorBtnElement){
                    if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                        console.log('color', colorBtn.previousSibling, ' color not checked: ', !colorBtn.previousSibling.hasAttribute('checked'))
                        if (!colorBtn.previousSibling.hasAttribute('checked')){
                            colorBtn.click();
                        }
                        
                        break;
                    }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){

                        if (!colorBtn.previousSibling.hasAttribute('checked')){
                            colorBtn.click();
                        }
                        break;
                    }
                }
            }
       }  
        
        
        let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
        }
        if (tileBtnElements.length <= 0){
            let presentTileButton = []
            tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li.mini.sku-spec')
            for (let btn of tileBtnElements){
                if (!btn.getAttribute('class').toLowerCase().includes('removed')){
                    presentTileButton.push(btn)
                }
            }

            tileBtnElements = presentTileButton
        }
        let tileBtn = null
        if (tileBtnElements.length > 0){
            console.log(_variants)
            for (let btn of tileBtnElements){ 
                let score = 0
                for (variant of _variants){
                    btn_content = btn.innerText.toLowerCase().replaceAll('-', '–').replace(/\s+/g, "")
                    variant_content = variant.toLowerCase().replaceAll('wi-fi', 'wifi').replace(/\s+/g, "")
                    
                    let hasCorrectVariant = btn_content.includes(variant_content)
                    console.log('hasCorrectVariant 1: ', variant_content,  hasCorrectVariant, btn_content)
                    if (hasCorrectVariant){
                        score += 1
                    } else {
                        if (variant.toLowerCase().includes('tb')) {
                            variant = variant.replaceAll('TB', ' TB')
                        }else if (variant.toLowerCase().includes('gb')){
                            variant = variant.replaceAll('GB', ' GB')
                        }
                        // console.log('changed variant space tb and space gb: ', variant)
                        let hasCorrectVariant = btn_content.includes(variant_content)
                        console.log('hasCorrectVariant 2: ', variant_content,  hasCorrectVariant, btn_content)
                        if (hasCorrectVariant){
                            score += 1
                            
                        }else{
                            break
                        }
                    }
                }
                console.log('score: ', score, ' ', _variants.length)
                if (score == _variants.length){
                    tileBtn = btn
                    break
                }
            }
        }
        

        if (tileBtn){
            if (tileBtn.hasAttribute('data-aid')){
                stockId = tileBtn.getAttribute('data-aid')
            }else if (tileBtn.hasAttribute('id')){
                stockId = tileBtn.getAttribute('id')
            }
        }
    }

    if (variant.hasOwnProperty('uid') || stockId){
        console.log('Variant detected')
        let variantId = variant.uid
        // let idSelector = `.custom-control.custom-radio [for="${id}"]`

        // ui indicator
        let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

        if (stockId){
            if (stockId != variantId){
                variantId = stockId
            }
        }

        let colorRadioBtnSelector = null

        if (ui1IndicatorElement){

            colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
        
        }else {
            let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
            let selector2 = `li[id="${variantId}"]`
            let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
            

            if (document.querySelector(selector1)){
                colorRadioBtnSelector = selector1
            }
            else if (document.querySelector(selector2)){
                colorRadioBtnSelector = selector2
            }
            else {
                colorRadioBtnSelector = selector3
            }

        }
        
        if (colorRadioBtnSelector){
            let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
            if (colorRadioBtnElement){
                colorRadioBtnElement.click()
                idSelector = colorRadioBtnSelector
            }

        }
    
        if (ui1IndicatorElement){
            idSelector = null
        }
        idSelector = colorRadioBtnSelector

    }
}

else {
    if (variantLanguage){

        let languageBtnElements = document.querySelectorAll('.picker.textpicker button.c-button')

        if (languageBtnElements.length > 0){
            for (let btn of languageBtnElements){
                if (btn.innerText.toLowerCase().includes(variantLanguage.toLowerCase())){
                    btn.click()
                    break
                }
            }
        }
    }
    if (variant.hasOwnProperty('color')) {
        let variantColor = variant.color
        
        let color = variantColor
        

        let colorBtnElement = document.querySelectorAll('.fc-color-picker label')
        if (colorBtnElement.length <= 0){
            colorBtnElement =  document.querySelectorAll('.picker.colorpicker button')

        }
        if (colorBtnElement.length <= 0){
            colorBtnClicked = false
        }                
        for (let colorBtn of colorBtnElement){
            if (colorBtn.hasAttribute('for') && colorBtn.getAttribute('for').toLowerCase().includes(color.toLowerCase())){
                colorBtn.click()
                colorBtnClicked = true
            }else if (colorBtn.hasAttribute('data-prdname') && colorBtn.getAttribute('data-prdname').toLowerCase().includes(color.toLowerCase())){
                colorBtn.click()
                colorBtnClicked = true
            }
        }
    }
    if (variant.hasOwnProperty('screen_size') || variant.hasOwnProperty('processor') || variant.hasOwnProperty('storage') || variant.hasOwnProperty('color')) {
        if (!colorBtnClicked){
            variantColor = null
        }
        let size = variantScreenSize
        let processor = null
        if (variantProcessor){
            processor = variantProcessor.replaceAll('-', '–')
        }
        
        let storage = (variantStorage == null ? null : variantStorage.split(','))
        let color = variantColor
        let storageSsd = null
        let storageRam = null
        
        if (storage){
            if (storage.length >= 2){
                storageSsd = storage[0].trim()
                storageRam = storage[1].trim()
                storage = null
            }else {
                storage = storage[0]
            }
        }
        let variants = [size, processor, storage, color, storageSsd, storageRam]

        let _variants = []
        
        for (let variant of variants){
            if (variant){
                _variants.push(variant)
            }
        }


        let sizeBtnElement = document.querySelectorAll('.fc-richtext-button.c-button')

        if (sizeBtnElement.length <= 0){
            sizeBtnElement = document.querySelectorAll('[aria-labelledby="filterscreensize"] button')
        }
        if (sizeBtnElement.length <=0){
            sizeBtnElement = document.querySelectorAll('ul[class="v3tile action-hide"] li[class="tile mini sku-spec"]')
        }
        if (sizeBtnElement.length > 0){
            let text = null
            if (size) {
                text = size.toLowerCase()
            
            }else if (processor){
                text = processor.toLowerCase()    

            }else if (storage){
                text = storage.toLowerCase()
            }
            if (text){
                for (let sizeBtn of sizeBtnElement){
                    if (sizeBtn.innerText.toLowerCase().includes(text)) {
                        sizeBtn.click();
                        break;
                    }else if (sizeBtn.innerText.toLowerCase().replaceAll(' ', '').includes(text.replaceAll(' ', ''))){
                        sizeBtn.click();
                        break;
                    }

                }
            }
        }
        
        let tileBtnElements = document.querySelectorAll('#ProductConfiguratorModule_0 .tileproductplacement button')

        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('.tileproductplacement.mini button.listboxitem-mini')
        }
        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('ul[class="v3tile"] li[class="tile mini sku-spec"]')
        }
        if (tileBtnElements.length <= 0){
            tileBtnElements = document.querySelectorAll('div[class="tileproductplacement mini c-group"] button')
        }
        let tileBtn = null
        for (variant of _variants){
            btn_content = btn.innerText.toLowerCase().replaceAll('-', '–').replace(/\s+/g, "")
            variant_content = variant.toLowerCase().replaceAll('wi-fi', 'wifi').replace(/\s+/g, "")

            let hasCorrectVariant = btn_content.includes(variant_content)
            console.log('hasCorrectVariant', hasCorrectVariant, variant)
            if (hasCorrectVariant){
                score += 1
            } else {
                if (variant.toLowerCase().includes('tb')) {
                    variant = variant.replaceAll('TB', ' TB')
                }else if (variant.toLowerCase().includes('gb')){
                    variant = variant.replaceAll('GB', ' GB')
                }
                let hasCorrectVariant = btn_content.includes(variant_content)
                console.log('hasCorrectVariant', hasCorrectVariant, variant)
                if (hasCorrectVariant){
                    score += 1
                    
                }else{
                    break
                }
            }
        }

        if (tileBtn){
            if (tileBtn.hasAttribute('data-aid')){
                stockId = tileBtn.getAttribute('data-aid')
            }else if (tileBtn.hasAttribute('id')){
                stockId = tileBtn.getAttribute('id')
            }else if (tileBtn.hasAttribute('data-pid')){
                stockId = tileBtn.getAttribute('data-pid')
            }
        }
    }

    if (variant.hasOwnProperty('uid') || stockId){
        console.log('Variant detected')
        let variantId = variant.uid
        // let idSelector = `.custom-control.custom-radio [for="${id}"]`

        // ui indicator
        let ui1IndicatorElement = document.querySelector('#rootContainer_BuyBox')

        if (stockId){
            if (stockId != variantId){
                variantId = stockId
            }
        }

        let colorRadioBtnSelector = null

        if (ui1IndicatorElement){

            colorRadioBtnSelector = `.custom-control.custom-radio [for="${variantId}"]`
        
        }else {
            let selector1 = `.tileproductplacement.mini button[id="${variantId}"]`
            let selector2 = `li[id="${variantId}"]`
            let selector3 = `.tileproductplacement button[data-aid="${variantId}"]`
            

            if (document.querySelector(selector1)){
                colorRadioBtnSelector = selector1
            }
            else if (document.querySelector(selector2)){
                colorRadioBtnSelector = selector2
            }
            else {
                colorRadioBtnSelector = selector3
            }

        }
        
        if (colorRadioBtnSelector){
            let colorRadioBtnElement = document.querySelector(colorRadioBtnSelector)
            if (colorRadioBtnElement){
                colorRadioBtnElement.click()
                idSelector = colorRadioBtnSelector
            }

        }
    
        if (ui1IndicatorElement){
            idSelector = null
        }
        idSelector = colorRadioBtnSelector
    }
}