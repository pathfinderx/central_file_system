const puppeteer = require('puppeteer-extra')
// const puppeteer = require('puppeteer')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { PageRedirectStrategyError, UnhandledStrategyError } = require('../errors')
const thirdPartyTemplate = require('../static/base_third_party.json')

function debugPrint(message){
    console.log(message);
}


class GolfskyFiFiWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            // sample url used: https://golfsky.fi/fi/golfkellot/garmin-approach-s62-premium-gps-golfkello-2984/10680#/5706-vari-white
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                await page.exposeFunction('debugPrint', debugPrint);

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout,
                    waitUntil: 'networkidle0'
                })
                
                try{
                    //wait & click for cookies
                    await page.waitForSelector('.lgcookieslaw-button-container [class="lgcookieslaw-button lgcookieslaw-accept-button lggoogleanalytics-accept"]')
                    await page.click('.lgcookieslaw-button-container [class="lgcookieslaw-button lgcookieslaw-accept-button lggoogleanalytics-accept"]')
                }
                catch(e){
                    // Do nothing
                }
                await page.waitForTimeout(25000)

                // PDP Checker
                await page.evaluate(async () => {
                    var element = document.querySelector('#main .product-information')

                    if (!element){
                        throw new UnhandledStrategyError('PDP Checker Error')
                    }

                })

                /********************************* */
                /********** START SCRAPING *********/
                /********************************* */
                
                await page.waitForTimeout(5500)

                // PRICE
                let priceResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try {
                        let priceElement = document.querySelector('.product-prices .current-price .current-price-display');

                        if (priceElement) {
                            innerText = priceElement.textContent.trim()
                            outerHTML = priceElement.outerHTML
                        }
                    } catch(error) {
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.price.value = priceResult.innerText
                result.price.source = priceResult.outerHTML

                // TITLE
                let titleResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{   
                        let titleElement = document.querySelector('.manufacturer-wrapper h1');

                        if (titleElement) {
                            innerText = titleElement.textContent.trim()
                            outerHTML = titleElement.outerHTML
                        } 

                    }catch(error){
                        innerText = FailureMessages.PRICE_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.title.value = titleResult.innerText
                result.title.source = titleResult.outerHTML

                // BRAND
                let brandResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let brandElement = document.querySelector('[property="product:brand"]') || 
                        document.querySelector('.manufacturer-wrapper img');

                        if (brandElement && brandElement.hasAttribute('content')){
                            innerText = brandElement.getAttribute('content')
                            outerHTML = brandElement.outerHTML
                        }else if (brandElement && brandElement.hasAttribute('alt')){
                            innerText = brandElement.getAttribute('alt')
                            outerHTML = brandElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.BRAND_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.brand.value = brandResult.innerText
                result.brand.source = brandResult.outerHTML

                // CURRENCY
                result.currency.value = 'EUR'
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                
                
                // DESCRIPTION
                let descriptionResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        // let description = '';
                        
                        // let checkDescTab = document.querySelector('.row.custom-product-description ul li a');
                        // if (checkDescTab){
                        //     if (checkDescTab.textContent.trim() == 'Kuvaus'){
                        //         let descTag = document.querySelectorAll('.product-description section');
                        //         if (descTag.length > 1){
                        //             for (let index = 0; index < descTag.length; index++) {
                        //                 if (descTag[index].textContent.trim() != ''){
                        //                     innerText = descTag[index].textContent.trim().replace(/\s\s+/g, ' ');
                        //                     outerHTML = descTag[index].outerHTML
                        //                     break
                        //                 }
                                              
                        //             }     
                        //         }else{
                        //             let descTagVariant = document.querySelector('.product-description');
                        //             if (descTagVariant){
                        //                 innerText = descTagVariant.textContent.trim().replace(/\s\s+/g, ' ');
                        //                 outerHTML = descTagVariant.outerHTML
                        //             }
                        //         }
                        //     }
                        // }
                        let tag = document.querySelector('.product-description section').querySelectorAll('section')
                        let collapse_button = document.querySelectorAll('div.elementor-toggle-item')
                        let desc_cont = new Array()

                        // click (https://prnt.sc/sWnggdLcKKLf)
                        if (collapse_button) {
                            for (let x = 0; x <= collapse_button.length -1; x++) {
                                collapse_button[x].querySelector('a').click()
                            }
                        }

                        for (let x = 0; x <= tag.length - 1; x++) {
                            try {
                                let contents = tag[x].querySelector('div.elementor-widget-container').querySelector('.elementor-text-editor.elementor-clearfix')
                                if (contents) {
                                    for (let i = 0; i <= contents.childNodes.length - 1; i++) {
                                        if (contents.childNodes[i].hasChildNodes()) {
                                            for (let u = 0; u <= contents.childNodes[i].childNodes.length - 1; u++) {
                                                if (!desc_cont.includes(contents.childNodes[i].childNodes[u].textContent)) {
                                                    desc_cont.push(contents.childNodes[i].childNodes[u].textContent)
                                                }
                                            }
                                        }
                                    
                                    }
                                } else {
                                    let tab_contents = tag[x].querySelector('div.elementor-widget-container').querySelectorAll('.elementor-toggle-item > div')
                                    if (tab_contents) {
                                        for (let i = 0; i <= tab_contents.length -1; i++) {
                                            if (tab_contents[i].getAttribute('class') == 'elementor-tab-content elementor-clearfix elementor-active') {
                                                for (let u = 0; u <= tab_contents[i].childNodes.length - 1; u++) {
                                                    text_content = tab_contents[i].childNodes[u].textContent
                                                    if (text_content && (text_content.search(/[\w]{2,}/) != -1)) {
                                                        desc_cont.push(tab_contents[i].childNodes[u].textContent)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (error) {
                                console.log(error)
                            }
                        }

                        if (desc_cont){
                            innerText = desc_cont.join(' ');
                            outerHTML = desc_cont.tag
                        }
   
                    }catch(error){
                        innerText = FailureMessages.DESCRIPTION_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.description.value = descriptionResult.innerText
                result.description.source = descriptionResult.outerHTML

                // SCPECIFICATIONS

                let specificationsResult = await page.evaluate(async function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let valuesMap = {}
                    try {
                        try {
                            let specs_tag = document.querySelector('.custom-product-description-short');
                            let temp_cont = new Array(); map = new Map(); last_id = 0; 
                            for (let x = 0; x <= specs_tag.childNodes.length - 1; x++) {
                                if (specs_tag.childNodes[x].nodeName == 'P' && specs_tag.childNodes[x].hasChildNodes()) {
                                    for (let y = 0; y <= specs_tag.childNodes[x].childNodes.length - 1; y++) {
                                        if (specs_tag.childNodes[x].childNodes[y].textContent.search(/[\w|\d]{1,}/) > -1) {
                                            if (last_id == x && x != 0 && temp_cont.length > 0) {
                                                if (!map.get(temp_cont[0])) { // not existing on map
                                                    map.set(temp_cont[0], specs_tag.childNodes[x].childNodes[y].textContent)
                                                } else { // append to existing key/value pair
                                                    map.set(temp_cont[0], map.get(temp_cont[0]) + ' ' + specs_tag.childNodes[x].childNodes[y].textContent)
                                                } 
                                            } else {
                                                temp_cont = new Array()
                                                keyVal = specs_tag.childNodes[x].childNodes[y].textContent.split(':')
                                                if (keyVal.length == 3) {
                                                    map.set(keyVal[0], keyVal[1] + ':' + keyVal[2])
                                                } else {
                                                    if (keyVal[1].search(/[\w|\d]{1,}/) != -1) {
                                                        map.set(keyVal[0], keyVal[1])
                                                    } else {
                                                        temp_cont.push(keyVal[0])
                                                    }
                                                }
                                            }
                                            last_id = x
                                        }
                                    }
                                }
                                outerHTML = specs_tag.outerHTML
                            }
                        } catch (err) {
                            console.log(err)
                        }

                        if (!map) {
                            let specKeyElements = document.querySelector('.custom-product-description-short p');
                            if (specKeyElements && specKeyElements.textContent.trim().includes(':')){
                                for (let i = 0; i < specKeyElements.innerHTML.split('<br>').length; i++) {
                                    key = specKeyElements.innerHTML.split('<br>')[i].split(':')[0].trim().replace('</span>','').replace('<span>','')
                                    val = specKeyElements.innerHTML.split('<br>')[i].split(':')[1].trim().replace('&nbsp;','').replace('</span>','').replace('<span>','')
                                    if (key && val){
                                        valuesMap[key] = val
                                    }
                                }
                                outerHTML = specKeyElements.outerHTML
    
                            }  
                        } 
                        
                        if (map) {
                            valuesMap = Object.fromEntries(map);
                        }

                    }catch(error){
                        valuesMap = FailureMessages.SPECIFICATIONS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { valuesMap, outerHTML }
                }, FailureMessages);

                result.specifications.value = specificationsResult.valuesMap
                result.specifications.source = specificationsResult.outerHTML

                // IMAGES
                let imagesResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let images = []
                    let unique = []
                    try{
                        
                        let thumbContainerElements = document.querySelectorAll('.images-container img')
                        // multiple images
                        if (thumbContainerElements.length > 0){
                            for (let i of thumbContainerElements){
                                if (i.hasAttribute('data-src')){
                                    unique.push(i.getAttribute('data-src').replace("-small_default", ""))
                                }
                            }
                            //filter duplicate image url
                            images = [...new Set(unique)];
                        } else {
                            // single image
                            let main_image = document.querySelector('.product-img img')
                            if (main_image && main_image.hasAttribute('src')){
                                images.push(main_image.getAttribute('src'))
                            }

                        }

                    }catch(error){
                        images = FailureMessages.IMAGE_URLS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { images, outerHTML }
                }, FailureMessages);

                result.image_urls.value = imagesResult.images
                result.image_urls.source = imagesResult.outerHTML

                // AVAILABILITY
                let availabilityResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let availabilityElement = document.querySelector('.product-add-to-cart button.btn.btn-primary');
                        if (availabilityElement){
                            innerText = availabilityElement.textContent.trim();
                            outerHTML = availabilityElement.outerHTML
                        }

                    }catch(error){
                        innerText = FailureMessages.AVAILABILITY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.availability.value = availabilityResult.innerText
                result.availability.source = availabilityResult.outerHTML
               
                // RATING 
                let ratingResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let ratingElement = document.querySelector('[itemprop="aggregateRating"] [itemprop="ratingValue"]');

                        if (ratingElement && ratingElement.hasAttribute('content')) {
                            innerText = ratingElement.getAttribute('content')
                            outerHTML = ratingElement.outerHTML
                        } 
                        
                    }catch(error){
                        innerText = FailureMessages.RATING_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.score.max_value = "5"
                result.rating.score.value = ratingResult.innerText
                result.rating.score.source = ratingResult.outerHTML


                // REVIEWS 
                let reviewsResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let reviewElement = document.querySelector('[itemprop="aggregateRating"] [itemprop="reviewCount"]');

                        if (reviewElement && reviewElement.hasAttribute('content')) {
                            innerText = reviewElement.getAttribute('content')
                            outerHTML = reviewElement.outerHTML
                        } 
                    }catch(error){
                        innerText = FailureMessages.REVIEWS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.rating.reviews.value = reviewsResult.innerText
                result.rating.reviews.source = reviewsResult.outerHTML

                // DELIVERY
                let deliveryResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let innerText = FailureMessages.GENERIC_NOT_FOUND
                    try{
                        let deliveryElement = document.querySelector('#product-availability');
                        if (deliveryElement){
                            innerText = deliveryElement.textContent.trim();
                            outerHTML = deliveryElement.outerHTML
                        }
                        
                    }catch(error){
                        innerText = FailureMessages.DELIVERY_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { innerText, outerHTML }
                }, FailureMessages);

                result.delivery.value = deliveryResult.innerText
                result.delivery.source = deliveryResult.outerHTML

                // SHIPPING

                // VIDEO
                let videosResult = await page.evaluate(function(FailureMessages){
                    let outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    let videos = []
                    let uniqueVid = []
                    try{
                        let videoElements = document.querySelectorAll('.product-description .elementor-video-iframe');

                        if (videoElements.length > 0){
                            for (let i of videoElements){
                                if (i.hasAttribute('src')){
                                    uniqueVid.push(i.getAttribute('src'))
                                }
                            }
                            //filter duplicate video url
                            videos = [...new Set(uniqueVid)];
                        } 

                    }catch(error){
                        videos = FailureMessages.IMAGE_URLS_EXTRACTION
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    }

                    return { videos, outerHTML }
                }, FailureMessages);

                result.video_urls.value = videosResult.videos
                result.video_urls.source = videosResult.outerHTML

                


                /********************************* */
                /************ END SCRAPING *********/
                /********************************* */

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = GolfskyFiFiWebsiteStrategy