
const amqp = require("amqplib/callback_api")
const fs = require('fs').promises
const lzma = require('lzma-native')
const models = require('./models')
const utils = require('./utils')
const { Features, JobStatus, LogType, ProcessName, Reasons } = require('./constants')
const { FailureMessages } = require("./strategies/constants")
const BlobStorageService = require('./services/blob')
const RMQPublisher = require('./services/rmq-publisher')
const RunningManService = require('./services/rm')
const StrategyFactory = require('./strategies/factory')
const { DataCouchDBService, JobCouchDBService } = require('./services/couch')
const { PageRedirectStrategyError, UnhandledStrategyError } = require('./strategies/errors')
const path = require('path')
const _fs = require('fs')

const basePath = path.dirname(__filename)
const productionEnvPath = path.join(basePath, 'production.env')
require("dotenv").config({path: productionEnvPath})

function getErrorFields(extractData) {
    var errorFields = {}
    
    // Collect fields with warnings
    if (extractData) {
        // Base
        if (extractData.price.value == FailureMessages.PRICE_EXTRACTION) {
            errorFields.price = extractData.price.value
        }

        if (extractData.currency.value == FailureMessages.CURRENCY_EXTRACTION) {
            errorFields.currency = extractData.currency.value
        }

        if (extractData.availability.value == FailureMessages.AVAILABILITY_EXTRACTION) {
            errorFields.availability = extractData.availability.value
        }

        if (extractData.rating.score.value == FailureMessages.RATING_EXTRACTION) {
            errorFields.rating_score = extractData.rating.score.value
        }

        if (extractData.rating.reviews.value == FailureMessages.REVIEWS_EXTRACTION) {
            errorFields.rating_reviews = extractData.rating.reviews.value
        }

        if (extractData.brand.value == FailureMessages.BRAND_EXTRACTION) {
            errorFields.brand = extractData.brand.value
        }

        if (extractData.title.value == FailureMessages.TITLE_EXTRACTION) {
            errorFields.title = extractData.title.value
        }

        if (extractData.description.value == FailureMessages.DESCRIPTION_EXTRACTION) {
            errorFields.description = extractData.description.value
        }

        if (extractData.specifications.value == FailureMessages.SPECIFICATIONS_EXTRACTION) {
            errorFields.specifications = extractData.specifications.value
        }

        if (extractData.image_urls.value == FailureMessages.IMAGE_URLS_EXTRACTION) {
            errorFields.image_urls = extractData.image_urls.value
        }

        if (extractData.video_urls.value == FailureMessages.VIDEO_URLS_EXTRACTION) {
            errorFields.video_urls = extractData.video_urls.value
        }

        if (extractData.delivery.value == FailureMessages.DELIVERY_EXTRACTION) {
            errorFields.delivery = extractData.delivery.value
        }

        if (extractData.shipping.value == FailureMessages.SHIPPING_EXTRACTION) {
            errorFields.shipping = extractData.shipping.value
        }

        // Buybox
        if (('buybox' in extractData) && (extractData.buybox != null && extractData.buybox != undefined && extractData.buybox != {})) {
            if (extractData.buybox.text == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-text'] = extractData.buybox.text
            }

            if (extractData.buybox.price == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-price'] = extractData.buybox.price
            }

            if (extractData.buybox.currency == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-currency'] = extractData.buybox.currency
            }

            if (extractData.buybox.delivery == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-delivery'] = extractData.buybox.delivery
            }

            if (extractData.buybox.shipping == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-shipping'] = extractData.buybox.shipping
            }

            if (extractData.buybox.availability == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-availability'] = extractData.buybox.availability
            }

            if (extractData.buybox.retailer == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-retailer'] = extractData.buybox.retailer
            }

            if (extractData.buybox.used_price == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-used_price'] = extractData.buybox.used_price
            }
            
            if (extractData.buybox.new_price == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-new_price'] = extractData.buybox.new_price
            }

            if (extractData.buybox.cartable == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-cartable'] = extractData.buybox.cartable
            }

            if (extractData.buybox.buyable == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-buyable'] = extractData.buybox.buyable
            }

            if (extractData.buybox.add_on == FailureMessages.BUYBOX_EXTRACTION) {
                errorFields['buybox-add_on'] = extractData.buybox.add_on
            }
        }

        // 3P
        if (('third_parties' in extractData) && extractData.third_parties == FailureMessages.THIRD_PARTY_EXTRACTION) {
            errorFields.third_parties = extractData.third_parties
        }

        // Variety
        if (('variety' in extractData) && extractData.variety.value == FailureMessages.THIRD_PARTY_EXTRACTION) {
            errorFields.variety = extractData.variety.value
        }
    }
    return errorFields
}


async function startScraping(document){
    try {
        // Get message object
        console.log(" [*] Processing. . .")
        var job = JSON.parse(document)
        var docId = job._id
        var jobParameters = job.job_parameters
        var features = jobParameters.features || []
        var jobMetadata = job.metadata
        var variant = jobParameters.variant || {}
        var companyCode = jobParameters.destination.company.code
        var countryCode = jobParameters.retailer.country_iso_code
        var website = jobParameters.retailer.name
        var url = jobParameters.sku.sku_url
        var triggerTime = job.metadata.trigger_time
        // var attempts = job.runtime_statistics.crawl.attempts + 1
        var attempts = jobParameters.attempt_count || 0
        var taskUuid = job.metadata.task_uuid || null
        var userUuid = job.metadata.user_uuid || null
        var docDate = docId.split("@")[0]

        var cdbUser = process.env.CDB_USER
        var cdbPwd = process.env.CDB_PWD
        var cbdHost = process.env.CDB_HOST
        var cdbPort = process.env.CDB_PORT
        var account = process.env.ABS_STORAGE_ACCOUNT
        var key = process.env.ABS_STORAGE_KEY
        var container = process.env.ABS_STORAGE_CONTAINER
        var browserEndpoint = process.env.BROWSER_ENDPOINT
        var timeout = process.env.BROWSER_TIMEOUT
        var proxyConnection = new models.ProxyConnection(process.env.LUMINATI_SERVER, process.env.LUMINATI_USER + countryCode.toLowerCase(), process.env.LUMINATI_PWD)

        if (website.includes('mediamarkt.de')) {
            console.log(`[*] Using proxy manager: ${process.env.LUMINATI_SERVER_PPT}`)
            proxyConnection = new models.ProxyConnection(process.env.LUMINATI_SERVER_PPT, '', '')
        }

        var userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
        var didFailBeforeSaving = true
        var didFail = false
        var localFilePath = `./${docId}.tar.gz`
        var uploadFileName = `${docId}.tar.gz`
        var extraReason = []

        // RunningMan message
        var rmPayload = {
            env: companyCode,
            product_url: jobParameters.sku.sku_url,
            status: "",
            crawl_started: "",
            crawl_finished: "",
            reason: Reasons.NOT_SPECIFIED,
            strategy: 'LUMINATI', // All spiders use Luminati
            ip_address: "",
            attempts: attempts,
            doc_id: docId,
            special_id: 'Puppeteer',
            request_size: 0,
            task_uuid: taskUuid,
            user_uuid: userUuid
        }
        
        var processName = ProcessName.RERUN

        var rmMessage = {
            data_id: jobParameters.sku.sku_item_id,
            company: jobParameters.destination.company.code,
            retailer: jobParameters.retailer.name,
            log_type: "",
            process_name: processName,
            payload: {}
        }
        
        console.log(rmPayload.task_uuid)
        console.log(rmPayload.user_uuid)
        // Deadlink message
        var deadlinkMsg = {
            is_dead_link: true,
            date: docDate,
            doc_id: docId,
            company: companyCode,
            website: website,
            item_id: jobParameters.sku.sku_item_id,
            attempts: attempts, // TODO: To be updated
            is_confirmed: 0, // For null urls and handling confirmed deadlinks
            feature: 'Puppeteer', // Might support more in the future
            metadata: {
                task_uuid: taskUuid,
                user_uuid: userUuid
            }
        }

        // Init CouchDB
        console.log('[*] Init CouchDB')
        var jobDBName = process.env.CDB_DB_JM
        var dbName = process.env.CDB_DB_DATA.replace('COMPANY_CODE', companyCode.toLowerCase())
        // var jobCouchDBService = new JobCouchDBService(cdbUser, cdbPwd, cbdHost, cdbPort, jobDBName)
        var jobCouchDBService = null
        var dataCouchDBService = new DataCouchDBService(cdbUser, cdbPwd, cbdHost, cdbPort, dbName)

        // Find strategy
        console.log('[*] Identifying strategy')
        var strategyFactory = new StrategyFactory()
        var StrategyContext = strategyFactory.getWebsiteStrategy(countryCode, website)
        var strategyError = null
        var strategyResult = null

        // // Misc validation
        // if (!url || url.length < 1) {
        //     throw new Error ('Invalid URL')
        // }
    }
    catch(err) {
        console.error(`[x] Failed prematurely: ${err}`)

        // Update job document
        
        if (jobCouchDBService) {
            console.log('[*] Update Couch job document')
            var [couchJobError, couchJobResult] = await utils.tryCatchPromiseWrapper(jobCouchDBService.updateDocument(docId, JobStatus.FAILED_PREMATURELY, attempts))

            if (couchJobError) {
                console.error(`-- [x] ${couchJobError.error}`)
            }
            else {
                console.log(`-- [*] ${couchJobResult.message}`)
            }
        }
        else {
            console.error('-- [x] CouchDB not defined prior to failure. Will not update job document')
        }

        // Publish deadlink message
        console.log('[*] Publishing message to DL queue')

        if (deadlinkMsg) {
            var rmqPublisher = new RMQPublisher(process.env.RMQ_CONN_STR, process.env.RMQ_QNAME_DL)
            var [pubError, pubResult] = await utils.tryCatchPromiseWrapper(rmqPublisher.publish(deadlinkMsg))
            var pubError = null
            if (pubError) {
                console.error('-- [x] Sending DL message failed')
            }
            else {
                console.log('-- [*] DL message sent successfuly')
            }
        }
        else {
            console.error('-- [x] DL message not defined prior to failure. Will not publish to DL queue')
        }

        // Send message to RunningMan
        console.log('[*] Sending message to RunningMan')

        if (rmMessage && rmPayload) {
            rmMessage.log_type = LogType.FAILURE
            rmPayload.crawl_started = new Date().toISOString().split('.')[0] + 'Z'
            rmPayload.crawl_finished = new Date().toISOString().split('.')[0] + 'Z'
            rmPayload.status = JobStatus.FAILED_PREMATURELY
            rmPayload.reason = Reasons.FAILED_PREMATURELY
            rmMessage.payload = rmPayload
            rmPayload.task_uuid = taskUuid
            rmPayload.user_uuid = userUuid

            const rmService = new RunningManService(process.env.RM_HOST, process.env.RM_PORT, process.env.RM_API_KEY, process.env.RM_API_PATH)
            var [rmError, rmResult] = await utils.tryCatchPromiseWrapper(rmService.send(rmMessage))
            // let rmError = null
            
            if (rmError) {
                console.error('-- [x] Sending RM message failed')
            }
            else {
                console.log('-- [*] RM message sent successfuly')
            }
        }
        else {
            console.error('-- [x] RM message not defined prior to failure. Will not send message to RunningMan')
        }
        
        // channel.ack(msg)
        // console.log('\n[*] Message acknowledged')
        return
    }
    try {
        rmPayload.crawl_started = new Date().toISOString().split('.')[0] + 'Z'
        
        // Strategy not found
        if (!StrategyContext) {
            console.error('[x] Strategy identification failed')
            rmPayload.reason = Reasons.STRATEGY_ID_FAILED
            didFail = true
            deadlinkMsg.is_confirmed = 1
        }
        else if (!url || url.length < 1){ // SKU is a placeholder and no url is setup yet
            console.error('[x] URL is empty or NULL')
            rmPayload.reason = Reasons.URL_NOT_FOUND
            didFail = true
            deadlinkMsg.is_confirmed = 1
        }
        else {
            var strategy = new StrategyContext(browserEndpoint, false, proxyConnection)
        
            // Execute strategy
            console.log('[*] Executing strategy')
            
            var timeoutPromise = 120000

            if (website == 'dns-shop.ru') { 
                timeoutPromise = 150000
            }

            var [strategyError, strategyResult] = await utils.tryCatchPromiseWrapper(utils.promiseTimeout(timeoutPromise, strategy.execute(url, userAgent, timeout, true, variant)))

            if (strategyError) {
                didFail = true
                didFailBeforeSaving = false

                if (strategyError.error instanceof PageRedirectStrategyError) {
                    rmPayload.reason = Reasons.URL_REDIRECT_FAILED
                    console.error(`-- [x] Strategy execution failed: ${strategyError.error.message}`)
                } 
                else if (strategyError.error instanceof UnhandledStrategyError) {
                    rmPayload.reason = Reasons.RAW_DOWNLOAD_FAILED
                    console.error(`-- [x] Strategy execution failed: ${strategyError.error.message}`)
                }
                else {
                    console.error(`-- [x] Strategy execution failed: ${strategyError.error}`)
                    rmPayload.reason = Reasons.RAW_DOWNLOAD_FAILED
                }

                rmPayload.ip_address = strategyError.actualProxyIP
                rmPayload.request_size = strategyError.totalMegabytes || 0

                console.log('[*] Insert / update Couch data document')
                var [couchError, couchResult] = await utils.tryCatchPromiseWrapper(dataCouchDBService.upsertDocument(docId, triggerTime, jobParameters, jobMetadata, {}))

                if (couchError) {
                    console.error(`-- [x] ${couchError.error}`)
                    rmPayload.reason = Reasons.DATA_WRITE_FAILED
                    didFail = true
                }

                var etMessage = couchResult.body
                var rmqPublisher = new RMQPublisher(process.env.RMQ_CONN_STR, process.env.RMQ_QNAME_ET)
                var [pubError, pubResult] = await utils.tryCatchPromiseWrapper(rmqPublisher.publish(etMessage))
                // let pubError = null

                if (pubError) {
                    console.error('-- [x] Sending ET message failed')
                    rmPayload.reason = Reasons.ET_QUEUE_PUBLISH_FAILED
                    didFail = true
                }
                else {
                    console.log('-- [*] ET message sent successfuly')
                }
            }
            else {
                console.log('-- [*] Strategy executed successfully')
                var extractHTML = strategyResult.html
                var extractData = strategyResult.result
                rmPayload.ip_address = strategyResult.actualProxyIP
                rmPayload.request_size = strategyResult.totalMegabytes || 0

                console.log(`-- [*] Total request_size (MB): ${rmPayload.request_size}`)
                
                // Upload raw HTML to blob storage
                console.log('[*] Uploading blob')
                var blobStorageService = new BlobStorageService(account, key)
                var lzmaResult = lzma.compress(extractHTML, 6)
                var [fsError, fsResult] = await utils.tryCatchPromiseWrapper(fs.writeFile(localFilePath, lzmaResult))

                if (fsError) {
                    console.warn('-- [x] Local file creation failed. Rebuild is recommended. Continue anyway')
                    extraReason.push('WARN: Local file creation failed. Rebuild is recommended. Continue anyway')
                }
                else {
                    var [blobError, blobResult] = await utils.tryCatchPromiseWrapper(blobStorageService.upload(container, localFilePath, uploadFileName))

                    if (blobError) {
                        console.warn('-- [x] Blob upload failed. Rebuild is recommended. Continue anyway')
                        extraReason.push('WARN: Blob upload failed. Rebuild is recommended. Continue anyway')
                    }
                    else {
                        var uploadUrl = `https://${account}.blob.core.windows.net/${container}/${uploadFileName}`
                        console.log(`-- [*] Blob uploaded successfully: ${uploadUrl}`)

                        // Delete local file copy
                        var [fsuError, fsuResult] = await utils.tryCatchPromiseWrapper(fs.unlink(localFilePath))

                        if (fsuError) {
                            console.warn('-- [x] Local file deletion failed. Rebuild is recommended. Continue anyway')
                            extraReason.push('WARN: Local file deletion failed. Rebuild is recommended. Continue anyway')
                        }
                    }
                }

                // Insert / update CouchDB Document
                console.log('[*] Insert / update Couch data document')
                var [couchError, couchResult] = await utils.tryCatchPromiseWrapper(dataCouchDBService.upsertDocument(docId, triggerTime, jobParameters, jobMetadata, extractData))

                if (couchError) {
                    console.error(`-- [x] ${couchError.error}`)
                    rmPayload.reason = Reasons.DATA_WRITE_FAILED
                    didFail = true
                }
                else {
                    console.log(`-- [*] ${couchResult.message}`)

                    // Disable dummy doc creation
                    didFailBeforeSaving = false

                    
                    if (jobCouchDBService){
                        // Update job document
                        console.log('[*] Update Couch job document')                                    
                        var [couchJobError, couchJobResult] = await utils.tryCatchPromiseWrapper(jobCouchDBService.updateDocument(docId, JobStatus.FINISHED, attempts))

                        if (couchJobError) {
                            console.error(`-- [x] ${couchJobError.error}`)
                            extraReason.push(Reasons.JOB_STATUS_FAILED)
                        }
                        else {
                            console.log(`-- [*] ${couchJobResult.message}`)
                        }
                    }else {
                        console.log('-- [x] CouchDB not defined prior to failure. Will not update job document')
                    }
                    
                    // Publish to ET queue
                    console.log('[*] Publishing message to ET queue')

                    var etMessage = JSON.parse(JSON.stringify(couchResult.body))

                    if (!features.includes(Features.THIRD_PARTIES)) {
                        // etMessage.product_data.third_parties = []
                        delete etMessage.product_data.third_parties
                    }

                    if (!features.includes(Features.SELLERS)) {
                        etMessage.product_data.sellers = []
                    }

                    if (!features.includes(Features.BUYBOX)) {
                        etMessage.product_data.buybox = {}
                    }
                    
                    var rmqPublisher = new RMQPublisher(process.env.RMQ_CONN_STR, process.env.RMQ_QNAME_ET)
                    var [pubError, pubResult] = await utils.tryCatchPromiseWrapper(rmqPublisher.publish(etMessage))
                    // let pubError = null

                    if (pubError) {
                        console.error('-- [x] Sending ET message failed')
                        rmPayload.reason = Reasons.ET_QUEUE_PUBLISH_FAILED
                        didFail = true
                    }
                    else {
                        console.log('-- [*] ET message sent successfuly')
                    }
                }
            }
        } 
    }
    catch(err) {
        didFail = true
        console.error(`[x] Unhandled exception: ${err}`)
        rmPayload.reason = Reasons.UNHANDLED_EXCEPTION
    }
    finally {
        console.log('\n BEGIN POST PROCESS, IF ANY \n')
        rmPayload.crawl_finished = new Date().toISOString().split('.')[0] + 'Z'

        // Create dummy document if failed before saving to CouchDB
        if (didFailBeforeSaving) {
            console.log('[*] Creating dummy document')

            var [couchError, couchResult] = await utils.tryCatchPromiseWrapper(dataCouchDBService.insertDummyDocument(docId, triggerTime, jobParameters))

            if (couchError) {
                console.warn(`-- [x] WARN: ${couchError.error}`)
                extraReason.push(`WARN: ${couchError.error}`)
            }
            else {
                console.log(`-- [*] ${couchResult.message}`)
            }
        }
        
        // If process failed at some point, mark as error for RunningMan
        if (didFail) {
            rmPayload.status = JobStatus.FAILED
            rmMessage.log_type = LogType.FAILURE
            deadlinkMsg.is_dead_link = true

            // Update job document
            if (jobCouchDBService){
                var [couchJobError, couchJobResult] = await utils.tryCatchPromiseWrapper(jobCouchDBService.updateDocument(docId, rmPayload.status, attempts))
                console.log('[*] Update Couch job document')
                if (couchJobError) {
                    console.error(`-- [x] ${couchJobError.error}`)
                    extraReason.push(Reasons.JOB_STATUS_FAILED)
                }
                else {
                    console.log(`-- [*] ${couchJobResult.message}`)
                }
            }else{
                console.log('-- [x] CouchDB not defined prior to failure. Will not update job document')
            }

        } 
        else {
            rmPayload.status = JobStatus.FINISHED
            rmMessage.log_type = LogType.SUCCESS
            deadlinkMsg.is_dead_link = false
            rmPayload.reason = Reasons.NO_REASON

            // If any errors in the extractData keys, mark as warning for RunningMan
            console.log('[*] Listing error fields')

            try {
                var errorFields = getErrorFields(extractData)

                if (Object.keys(errorFields).length != 0) {
                    rmMessage.log_type = LogType.WARNING
                    rmPayload.fields = errorFields
                    console.log(`-- [*] Error fields found. log_type set to ${LogType.WARNING}`)
                } else {
                    console.log('-- [*] No error fields found')
                }
            }
            catch(err) {
                console.warn(`-- [x] WARN: Listing error fields failed: ${err}`)
                extraReason.push('WARN: Listing error fields failed')
            }
        }

        // Send message to RunningMan
        console.log('[*] Sending message to RunningMan')

        // If there are extra warnings, include said warnings. JSON stringify to avoid character errors
        if (extraReason && extraReason.length > 0) {
            rmPayload.reason = JSON.stringify({reason: rmPayload.reason, extraReason: extraReason})
        }

        rmMessage.payload = rmPayload
        const rmService = new RunningManService(process.env.RM_HOST, process.env.RM_PORT, process.env.RM_API_KEY, process.env.RM_API_PATH)
        var [rmError, rmResult] = await utils.tryCatchPromiseWrapper(rmService.send(rmMessage))
        // let rmError = null
        if (rmError) {
            console.log(process.env.RM_HOST)
            console.error('-- [x] Sending RM message failed '+ rmError)
        }
        else {
            console.log('-- [*] RM message sent successfuly')
        }

        // Publish deadlink message
        console.log('[*] Publishing message to DL queue')
        deadlinkMsg.attempts = attempts
        var rmqPublisher = new RMQPublisher(process.env.RMQ_CONN_STR, process.env.RMQ_QNAME_DL)
        var [pubError, pubResult] = await utils.tryCatchPromiseWrapper(rmqPublisher.publish(deadlinkMsg))
        // let pubError = null

        if (pubError) {
            console.error('-- [x] Sending DL message failed')
        }
        else {
            console.log('-- [*] DL message sent successfuly')
        }
        console.log('\n[*] Done scraping')
    }
}


class DocumentCouchDBService extends DataCouchDBService {
    constructor(user, pwd, host, port, dbName) {
        super(user, pwd, host, port, dbName)
    }
    /**
     * Fetch couch data document/s from couchdb
     * @param {String} date Date of document/s
     * @param {Array} itemIds Item id/s that will be fetched
     * @param {String} retailer Retailer name
     * @returns {Array} documents
     */
    async getDocuments(date, itemIds, retailer) {
        return new Promise(async (resolve, reject) => {
            try {
                let query = {
                    "selector": {
                        "_id": {
                            "$gte": `${date}@`,
                            "$lte": `${date}@香`
                        },
                        "job_parameters": {}
                    }
                }
                if (itemIds.length > 0){
                    query.selector.job_parameters.sku = {
                        "sku_item_id": {
                            "$in": itemIds
                        }
                    }
                }
                if (retailer){
                    query.selector.job_parameters.retailer = {
                        "url": retailer
                    }
                }

                this.db.find(query)
                    .then((res) => {
                        resolve({'documents': res.docs})
                    })
                    .catch((error) => {
                        resolve({'error': error})
                    })
                
            } catch (error) {
                reject({'error': error})
            }
        })
    }
}


function getItemIdsFromFile(filePath){
    let ItemIdsFilePath = filePath || path.join(path.dirname(__filename), 'item_ids.txt');
    let itemIds = _fs.readFileSync(ItemIdsFilePath, 'utf-8').split('\n')
    let cleanedItemIds = []
    try {
        if (itemIds.length >= 1){
            for (let i = 0; i < itemIds.length; i++){
                cleanedItemIds.push(itemIds[i].trim())
            }
        }        
    } catch (error) {
        console.log(error)
    }

    return cleanedItemIds
}

function getCommandLineArgs(){
    let cmdArgs = process.argv.slice(2)
    let extractedArgs = {}

    for (let i = 0; i < cmdArgs.length; i++){
        if (i % 2 == 0){
            if (cmdArgs[i] == '-i'){
                extractedArgs[cmdArgs[i]] = cmdArgs[i + 1].split(',')
            } else {
                extractedArgs[cmdArgs[i]] = cmdArgs[i + 1]
            }
            
        }    
    }

    return extractedArgs
}
async function startLocalRerun(){
    return new Promise(async (resolve, reject) => {
        let cdbUser = process.env.CDB_USER
        let cdbPwd = process.env.CDB_PWD
        let cdbHost = process.env.CDB_HOST
        let cdbPort = process.env.CDB_PORT
        let commandLineArgs = getCommandLineArgs()
        let companyCode = commandLineArgs['-c'] || '100'
        let dbName = `company-${companyCode.toLowerCase()}-productdata`

        let date = commandLineArgs['-d'] || '2022-09-07'
        let retailer = commandLineArgs['r'] || 'www.power.dk'

        try{
            let itemIds = commandLineArgs['-i'] || getItemIdsFromFile()
            let dataCouchDBService = new DocumentCouchDBService(cdbUser, cdbPwd, cdbHost, cdbPort, dbName);
            let [couchError, couchResult] = await utils.tryCatchPromiseWrapper(dataCouchDBService.getDocuments(date, itemIds, retailer));
            if (couchError){
                reject({message:couchError})
            }
            console.log(`Fetched ${couchResult.documents.length} documents`)
            for (let i = 0; i < couchResult.documents.length; i++){
                document = couchResult.documents[i]
                await startScraping(JSON.stringify(document))
            }
            resolve({'message': 'Done'})
            
        } catch (error){
            console.log(error)
            reject({'message': error})
        }

    })
}

startLocalRerun()