const puppeteer = require('puppeteer')
const requestPromise = require('request-promise')
const FailureMessages = require('../constants').FailureMessages
const WebsiteStrategy = require('../base')
const { UnhandledStrategyError } = require('../errors')


class LiverpoolComMxWebsiteStrategy extends WebsiteStrategy {
    constructor(browserEndpoint, isHeadless, proxy) {
        super(browserEndpoint, isHeadless, proxy)
    }

    execute(url, userAgent, timeout, useLocalBrowser, variant) {
        return new Promise(async (resolve, reject) => {
            var actualProxyIP = "" 
            var browser = null
            var result = JSON.parse(JSON.stringify(this.baseTemplate))

            try {
                var args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ]

                // Attach proxy to browser options
                var browserEndpoint = this.browserEndpoint

                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}`
                    args.push(`--proxy-server=${this.proxy.server}`)
                }

                // Use local or remote browser
                if (useLocalBrowser) {
                    // Build browser options
                    var options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true // for latest version of node and puppeteer
                    }

                    // Launch browser
                    browser = await puppeteer.launch(options)
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint})
                }

                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                })

                // Open new page
                const page = await browser.newPage()

                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) {
                        // Do nothing
                    }
                    
                })

                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
                }

                await page.setUserAgent(userAgent)

                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    })
                }

                // Open URL in browser
                await page.goto(url, {
                    timeout: timeout
                })
                
                // Set page view port size
                await page.setViewport({width: 1200, height: 1200})

                if (variant.hasOwnProperty('uid')){
                    try{
                        console.log(`Detected variant: ${variant.uid}`)
                        var id = variant.uid
                        var selector = `a[data-skuid="${id}"]`
                        await page.waitForSelector(selector);
                        await page.click(selector);
                        await page.waitFor(20000)
                    }
                        

                    catch(e){
                        console.log(e)
                        throw new UnhandledStrategyError('Variant not present in the PDP')
                    }
                }
                else{
                    console.log('No detected variant')
                    await page.waitFor(20000)

                }

                await page.waitForSelector('#o-product__productSpecsDetails')
                
                // Get script data
                var scriptText = await page.evaluate(async () => {
                    var innerText = null
                    var element = document.querySelector('script#__NEXT_DATA__')
                    
                    if (element) {
                        innerText = element.innerText
                        outerHTML = element.outerHTML
                    }
                    else {
                        throw new UnhandledStrategyError('PDP Checker Error')
                    }

                    return {innerText, outerHTML}
                }) 
                var scriptData = {}
                if (scriptText){
                    scriptData = JSON.parse(scriptText.innerText)
                };

                // Price
                var priceResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND
                    
                    try {
                        var baseElement = document.querySelector("p.a-product__paragraphDiscountPrice.m-0.d-inline")

                        if (baseElement) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.innerText
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = e
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.price.source = priceResult.outerHTML
                result.price.value = priceResult.value

                // Currency
                result.currency.source = FailureMessages.GENERIC_NOT_FOUND
                result.currency.value = 'USD' // Defaulted

                // Availability
                var availabilityResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var baseElement = document.querySelector('button#opc_pdp_buyNowButton')
                        if (baseElement) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.innerHTML
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.AVAILABILITY_EXTRACTION
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.availability.source = availabilityResult.outerHTML
                result.availability.value = availabilityResult.value

                // Rating Reviewers
                var reviewsResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var baseElement = document.querySelector(`meta[itemprop="reviewCount"]`)

                        if (baseElement && baseElement.hasAttribute("content")) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.getAttribute("content")
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.RATING_EXTRACTION
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.rating.reviews.source = reviewsResult.outerHTML
                result.rating.reviews.value = reviewsResult.value
                    

                // Ratings Scores
                result.rating.score.max_value = "5" // Defaulted

                var scoreResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var baseElement = document.querySelector(`meta[itemprop="ratingValue"]`)

                        if (baseElement && baseElement.hasAttribute("content")) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.getAttribute("content")
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.BRAND_EXTRACTION
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.rating.score.source = scoreResult.outerHTML
                result.rating.score.value = scoreResult.value

                //brand
                if (scriptData){
                    try{
                        if (scriptData.query.data.mainContent.records[0].allMeta.brand){
                            result.brand.value = scriptData.query.data.mainContent.records[0].allMeta.brand    
                            result.brand.source = scriptText.outerHTML                     
                        }
                        
                    }
                    catch(e){
                        result.brand.source = FailureMessages.GENERIC_NOT_FOUND
                        result.brand.value = FailureMessages.BRAND_EXTRACTION
                    }

                }
                // Title
                var titleResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var baseElement = document.querySelector('h1.a-product__information--title')

                        if (baseElement) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.innerHTML
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.TITLE_EXTRACTION
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.title.source = titleResult.outerHTML
                result.title.value = titleResult.value

                // Description
                var descResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var value = FailureMessages.GENERIC_NOT_FOUND

                    try {
                        var baseElement = document.querySelector('[data-tab="description"] .productDetailTab')

                        if (baseElement) {
                            outerHTML = baseElement.outerHTML
                            value = baseElement.innerHTML
                        }
                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.DESCRIPTION_EXTRACTION
                    }
                    
                    return { outerHTML, value }
                }, FailureMessages)

                result.description.source = descResult.outerHTML
                result.description.value = descResult.value

                // Specifications
                var specsResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesMap = {}

                    try {
                        var keyElement = document.querySelectorAll('span.productSpecsGrouped_bold') 
                        var valElement = document.querySelectorAll('span.productSpecsGrouped_regular')
                        
                        if (keyElement.length == 0 || valElement.length == 0){
                            keyElement = document.querySelectorAll('.table.a-product__paragraphProductSpecs.table-border-detail span.productSpecsDetails_bold')
                            valElement = document.querySelectorAll('.table.a-product__paragraphProductSpecs.table-border-detail span.productSpecsDetails_light')

                        }
                        if (keyElement && valElement){
                            for (let i = 0; i <= keyElement.length; i++) {
                                try {
                                    valuesMap[keyElement[i].innerHTML.trim()] = valElement[i].innerHTML.trim()
                                    outerHTML = keyElement[i].outerHTML
                                } catch (e) {
                                    console.log('Tolerable error at: ', 'get_specification -> ', e)
                                }
                            }
                        }

                        if (!valuesMap) {
                            let tableElement = document.querySelector('div.wrapper_table')
                            let twoWayList = []
                            let map = new Map()

                            if (tableElement) {
                                try {
                                    for (let i = 0; i <= tableElement.childNodes.length - 1; i++) {
                                        let tbody = tableElement.childNodes[i]
                                        if (tbody.hasChildNodes) {
                                            let properties = ['0', '1'] 
                                            for (let x = 0; x <= tbody.childNodes.length; x++) {
                                                try {
                                                    // As obeserved, span class properties alters incrementally in every iteration of table
                                                    let tableKey = tbody.childNodes[x].querySelector(`span[class="productSpecsDetails_bold ${i}"]`) 
                                                    let tableValue = tbody.childNodes[x].querySelector('span[class="productSpecsDetails_light"]')
                                                    if (tableKey && tableValue) {
                                                        console.log(tableKey.innerHTML.trim(), ' : ', tableValue.innerHTML.trim())
                                                        twoWayList.push(tableKey.innerHTML.trim())
                                                        twoWayList.push(tableValue.innerHTML.trim())
                                                    }
                                                    if (twoWayList.length == 2) {
                                                        map.set(twoWayList[0], twoWayList[1])
                                                        twoWayList = []
                                                    }
                                                } catch (e) {
                                                    console.log('Tolerable error: empty query selector; ', e)
                                                }
                                            }
                                        }
                                    }
                                } catch (e) {
                                    console.log('Failure to extract specification using table elements')
                                }
                            }
                            if (map) {
                                valuesMap = Object.fromEntries(map);
                                outerHTML = tableElement.outerHTML
                            }
                        }

                    }
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        value = FailureMessages.SPECIFICATIONS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesMap }
                }, FailureMessages)

                result.specifications.source = specsResult.outerHTML
                result.specifications.value = specsResult.valuesMap

                // Images
                var imgResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []

                    try { 

                        element = document.querySelectorAll('.pdpthumb.lazyloaded') || document.querySelectorAll('.img-viewer img.pdpthumb.lazyloaded')

                        if (element){
                            value = element.forEach((image) => {
                                if (image.parentElement.getAttribute('class') != 'pzlvideo'){    
                                if (image.hasAttribute('src')){
                                    valuesArray.push(image.getAttribute('src'))
                                    outerHTML = image.outerHTML
                                }
                            }
                            }
                        )
                        }
                    }
                    
                    catch(e) {
                        outerHTML = FailureMessages.GENERIC_NOT_FOUND
                        values = FailureMessages.IMAGE_URLS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesArray }
                }, FailureMessages)
                
                result.image_urls.source = imgResult.outerHTML
                result.image_urls.value = imgResult.valuesArray

                //CLICK VIDEO FIRST
                try{
                    await page.evaluate(function(){
                        let element = document.querySelectorAll('.pdpthumb.lazyloaded') || document.querySelectorAll('.img-viewer img.pdpthumb.lazyloaded')

                        if (element){
                            value = element.forEach((image) => {
                                if (image.parentElement.getAttribute('class') == 'pzlvideo'){    
                                image.click()
                                
                            }
                            }
                        )
                        }
                    });
                    await page.waitForTimeout(10000)
                }
                catch(error){
                    //DO NOTHING
                }
                // Videos 
                var vidResult = await page.evaluate(async (FailureMessages) => {
                    var outerHTML = FailureMessages.GENERIC_NOT_FOUND
                    var valuesArray = []

                    try { 
                        let videoElement = document.querySelector('.pzlcontainerviewer.viewerImg iframe') || 
                            document.querySelector('div.pzlcontainerviewer.viewerImg > video > source');
                        if( videoElement){
                            valuesArray.push(videoElement.getAttribute('src'))
                        }
                        // let element = document.querySelectorAll('.pdpthumb.lazyloaded') || document.querySelectorAll('.img-viewer img.pdpthumb.lazyloaded')

                        // if (element){
                        //     value = element.forEach((image) => {
                        //         if (image.parentElement.getAttribute('class') == 'pzlvideo'){    
                        //         image.click()
                        //         video_tag = document.querySelector('.pzlcontainerviewer.viewerImg iframe');
                        //         if(video_tag){
                        //             valuesArray.push(video_tag.getAttribute('src'))
                        //         }
                        //     }
                        //     }
                        // )
                        // }
                    }
                    catch(e) {
                        sources = FailureMessages.GENERIC_NOT_FOUND
                        values = FailureMessages.VIDEO_URLS_EXTRACTION
                    }
                    
                    return { outerHTML, valuesArray }
                }, FailureMessages)

                result.video_urls.source = vidResult.outerHTML
                result.video_urls.value = vidResult.valuesArray

                // Delivery - Not indicated

                // Shipping - None by default

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing

                resolve({html: await page.content(), actualProxyIP: actualProxyIP, result: result, totalMegabytes: totalMegabytes})
            } 
            catch (err) {
                console.error(err)
                reject({error: `Unhandled exception for URL: ${url}`, actualProxyIP: actualProxyIP})
            }
            finally {
                if (browser) {
                    await browser.close()
                }
            }
        })
    }
}

module.exports = LiverpoolComMxWebsiteStrategy