import { WebsiteStrategy } from "../base.js";
import puppeteer from 'puppeteer';
import { FailureMessages } from '../constants.js';


function debugPrint(message){
    console.log(message);
}

class PcgarageRoWebsiteStrategy extends WebsiteStrategy{

    constructor(browserEndpoint, isHeadless, proxy){
        super(browserEndpoint, isHeadless, proxy);
        this.browserEndpoint = browserEndpoint;
        this.isHeadless = isHeadless;
        this.proxy = proxy;
    }

    async execute(keyword, userAgent, timeout, useLocalBrowser=false, startRank=1, limit=100){
        return new Promise(async (resolve, reject) => {
            let actualProxyIP = '';
            let browser = null;

            try{
                const args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ];
    
                // Attach proxy to browser options
                let browserEndpoint = this.browserEndpoint;

                // this.proxy.server ='zproxy.lum-superproxy.io:22225'
                // this.proxy.user = 'lum-customer-c_9c4e20a3-zone-be_unblocker-country-ro-unblocker'
                // this.proxy.pwd = '5ae5cfm7v22g'
    
                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth`;
                    args.push(`--proxy-server=${this.proxy.server}`);
                }
                
                // Use local or remote browser
                if (useLocalBrowser) {
                    const options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true
                    };
                    browser = await puppeteer.launch(options);
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint, ignoreHTTPSErrors: true});
                    
                }
    
                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                });
    
                // Open new page
                const page = await browser.newPage()
                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) { }
                });
    
                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36';
                }
    
                await page.setUserAgent(userAgent);
    
                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    });
                }

                await page.exposeFunction('debugPrint', debugPrint);
                

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200});
                await page.goto('https://www.pcgarage.ro', {
                    waitUntil: 'networkidle0',
                    timeout: timeout
                });

                await page.waitForTimeout(5000); // add extra waiting time

                let initialUrl = 'https://www.pcgarage.ro/cauta/' + keyword.replace(' ', '+') + '/pagina1';
                let initialResult = await page.evaluate(async function(initialUrl){
                    let response = await fetch(initialUrl, {
                        method: 'GET',
                        credentials: 'include'
                    })
                    let responseText = await response.text();
                    let responseUrl = response.url;
                    return { responseUrl, responseText };
                }, initialUrl);

                let pageLayout = await this._determinePageLayout(page, initialResult.responseText);
                console.log(pageLayout);
                await page.waitForTimeout(5000)

                // clean the url
                initialResult.responseUrl = initialResult.responseUrl.replace('pagina1', '');

                /** START SCRAPING FROM HERE */

                let products = [];
                if(pageLayout == 'RANKINGS'){
                    products = await this._getRankingsProducts(page, initialResult.responseUrl, startRank, limit, timeout);
                }else if(pageLayout == 'LISTINGS'){
                    products = await this._getListingsProducts(page, initialResult.responseUrl, startRank, limit, timeout);
                }else{
                    // do nothing, might check further for 404 or other scenarios
                }

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing
                
                resolve({ actualProxyIP: actualProxyIP, result: products, totalMegabytes: totalMegabytes });
      
            }catch(error){
                console.error(error)
                reject({error: `Unhandled exception for URL: https://www.pcgarage.ro, keyword: ${keyword}`, actualProxyIP: actualProxyIP});
            }finally{
                if (browser) {
                    await browser.close();
                }
            }
        });
    }

    async _determinePageLayout(page, initialPageText){
        // create a DOM element
        
        let pageLayout = await page.evaluate(async function(initialPageText){
            let dom = new DOMParser().parseFromString(initialPageText, 'text/html');

            try{

                // if we find the search box, then its rankings
                let containerElement = dom.querySelector('div#searchpage_form');
                if(containerElement)
                    return 'RANKINGS';

                // if it contains products but its not rankings, then its likely listings
                containerElement = dom.querySelector('div#category_content');
                if(containerElement)
                    return 'LISTINGS';
    
                // cant determine
            }catch(error){
                throw error;
            }
        }, initialPageText);

        return pageLayout;
    }

    async _getRankingsProducts(page, url, startRank, limit, timeout){
        let result = [];

        try{
            while(true){
                let responseText = await page.evaluate(async function(url){
                    let response = await fetch(url, {
                        method: 'GET',
                        credentials: 'include'
                    });
    
                    return await response.text();
                }, url);
    
                let statistics = await page.evaluate(function(responseText, startRank, baseTemplate, FailureMessages){
                    let statistics = { products: [], hasNextPage: false, nextUrl: null };
                    let productsContainer = productElements = null;
                    try{
    
                        let dom = new DOMParser().parseFromString(responseText, 'text/html');
    
                        productsContainer = dom.querySelector('div#wrapper_listing_products');
                        productElements = productsContainer.querySelectorAll('div.product_b_container');
    
                        if(productElements.length > 0){
    
                            // SCRAPE PRODUCTS
                            for(let index = 0; index < productElements.length; index++){
                                let template = JSON.parse(JSON.stringify(baseTemplate));
                                let element = productElements[index];
    
                                template.rank = startRank.toString();
    
                                // get title
                                try{
                                    template.title.value = element.querySelector('div.product_box_name h2 a').textContent.trim();
                                    template.title.source = element.outerHTML;
                                }catch(error){
                                    template.title.value = FailureMessages.TITLE_EXTRACTION;
                                    template.title.source = FailureMessages.GENERIC_NOT_FOUND;
                                }
    
                                // // no brand
    
                                // get url
                                try{
                                    template.url.value = element.querySelector('div.product_box_name h2 a').getAttribute('href');
                                    template.url.source = element.outerHTML;
                                }catch(error){
                                    template.url.value = FailureMessages.URL_EXTRACTION;
                                    template.url.source = FailureMessages.GENERIC_NOT_FOUND;
                                }
    
                                statistics.products.push(template);
                                startRank++;
                            }
    
                            // DETERMINE IF THERES NEXT PAGE
                            let paginationContainer = dom.querySelector('ul.pagination');
                            if(paginationContainer){
                                let paginationNextElement = paginationContainer.querySelector('li.active + li > a');
                                if(paginationNextElement){
                                    statistics.hasNextPage = true;
                                    statistics.nextUrl = paginationNextElement.getAttribute('href');
                                }
                            }
                        }
                    }catch(error){
                        return { products: [], hasNext: false, nextUrl: null }
                    }
                    
                    return statistics;
                }, responseText, startRank, this.baseTemplate, FailureMessages);
    
                result.push(...statistics.products);
                startRank = result.length + 1;
    
                if(result.length >= limit)
                    break;
                if(statistics.products.length == 0)
                    break;
                if(!statistics.hasNextPage)
                    break;
    
                url = statistics.nextUrl;

                await page.waitFor(5000);
            }
        }catch(error){
            throw error;
        }

        result = result.slice(0, limit);
        return result;
    }


    async _getListingsProducts(page, url, startRank, limit, timeout){
        let result = [];

        try{
            while(true){
                let responseText = await page.evaluate(async function(url){
                    let response = await fetch(url, {
                        method: 'GET',
                        credentials: 'include'
                    });
    
                    return await response.text();
                }, url);
    
                let statistics = await page.evaluate(async function(responseText, startRank, baseTemplate, FailureMessages){
                    let statistics = { products: [], hasNextPage: false, nextUrl: null };
                    let productsContainer = productElements = null;
                    try{
    
                        let dom = new DOMParser().parseFromString(responseText, 'text/html');
    
                        // productsContainer = dom.querySelector('div#wrapper_listing_products');
                        productElements = dom.querySelectorAll('div#wrapper_listing_products > div');
    
                        if(productElements.length > 0){
    
                            // SCRAPE PRODUCTS
                            for(let index = 0; index < productElements.length; index++){
                                let template = JSON.parse(JSON.stringify(baseTemplate));
                                let element = productElements[index];
    
                                template.rank = startRank.toString();
    
                                // get title
                                try{
                                    template.title.value = element.querySelector('div.product_box_image a').getAttribute('title');
                                    template.title.source = element.outerHTML;
                                }catch(error){
                                    template.title.value = FailureMessages.TITLE_EXTRACTION;
                                    template.title.source = FailureMessages.GENERIC_NOT_FOUND;
                                }
    
                                // // no brand
    
                                // get url
                                try{
                                    template.url.value = element.querySelector('div.product_box_image a').getAttribute('href');
                                    template.url.source = element.outerHTML;
                                }catch(error){
                                    template.url.value = FailureMessages.URL_EXTRACTION;
                                    template.url.source = FailureMessages.GENERIC_NOT_FOUND;
                                }
    
                                statistics.products.push(template);
                                startRank++;
                            }
    
                            // DETERMINE IF THERES NEXT PAGE
                            let paginationContainer = dom.querySelector('ul.pagination');
                            if(paginationContainer){
                                let paginationNextElement = paginationContainer.querySelector('li.active + li > a');
                                if(paginationNextElement){
                                    statistics.hasNextPage = true;
                                    statistics.nextUrl = paginationNextElement.getAttribute('href');
                                }
                            }
                        }
                    }catch(error){
                        return { products: [], hasNext: false, nextUrl: null }
                    }
                    
                    return statistics;
                }, responseText, startRank, this.baseTemplate, FailureMessages);

                result.push(...statistics.products);
                startRank = result.length + 1;
    
                if(result.length >= limit)
                    break;
                if(statistics.products.length == 0)
                    break;
                if(!statistics.hasNextPage)
                    break;
    
                url = statistics.nextUrl;

                await page.waitFor(5000);
            }
        }catch(error){
            throw error;
        }

        result = result.slice(0, limit);
        return result;
    }
}

export default PcgarageRoWebsiteStrategy;