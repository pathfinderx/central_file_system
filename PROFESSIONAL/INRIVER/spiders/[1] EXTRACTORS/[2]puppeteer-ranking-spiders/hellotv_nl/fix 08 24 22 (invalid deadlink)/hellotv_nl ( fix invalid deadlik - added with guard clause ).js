import { WebsiteStrategy } from "../base.js";
import puppeteer from 'puppeteer';
import { FailureMessages } from '../constants.js';

class HellotvNlWebsiteStrategy extends WebsiteStrategy{

    constructor(browserEndpoint, isHeadless, proxy){
        super(browserEndpoint, isHeadless, proxy);
        this.browserEndpoint = browserEndpoint;
        this.isHeadless = isHeadless;
        this.proxy = proxy;
    }

    async execute(keyword, userAgent, timeout, useLocalBrowser=false, startRank=1, limit=100){
        return new Promise(async (resolve, reject) => {
            let actualProxyIP = '';
            let browser = null;

            try{
                const args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ];
    
                // Attach proxy to browser options
                let browserEndpoint = this.browserEndpoint;
    
                if (this.proxy) {
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&stealth=true&blockAds=false`;
                    args.push(`--proxy-server=${this.proxy.server}`);
                }
                
                // Use local or remote browser
                if (useLocalBrowser) {
                    const options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true
                    };
                    browser = await puppeteer.launch(options);
                }
                else {
                    browser = await puppeteer.connect({browserWSEndpoint: browserEndpoint});
                }
    
                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                });
    
                // Open new page
                const page = await browser.newPage()
                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) { }
                });
    
                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36';
                }
    
                await page.setUserAgent(userAgent);
    
                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    });
                }

                // Set page view port size
                await page.setViewport({width: 1200, height: 1200});
                await page.goto('https://www.hellotv.nl', {
                    waitUntil: 'networkidle0',
                    timeout: timeout
                });

                //accept cookies
              
                await page.waitForSelector('[data-testid="cookieConsent.button.accept"]')
                await page.click('[data-testid="cookieConsent.button.accept"]')

                /** START SCRAPING FROM HERE */
                page.waitForTimeout(10000)
                await page.type('[name="search"]', keyword);
                page.waitForTimeout(2000)
                page.keyboard.press(String.fromCharCode(13));
                
                // wait for ajax reload
                await page.waitForTimeout(20000);
                
                let products = await this._getProducts(page, startRank, limit, timeout);

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing
                
                resolve({ actualProxyIP: actualProxyIP, result: products, totalMegabytes: totalMegabytes });

                
            }catch(error){
                console.error(error)
                reject({error: `Unhandled exception for URL: https://www.hellotv.nl/, keyword: ${keyword}`, actualProxyIP: actualProxyIP});
            }finally{
                if (browser) {
                    await browser.close();
                }
            }
        });
    }

    async _getProducts(page, startRank, limit, timeout){
        let result = [];

        while(true){
            let tempResult = await page.evaluate(async function(startRank, baseTemplate, FailureMessages){
                let result = [];
                let productElements = null;
                try{
                    productElements = document.querySelectorAll('div.css-181rts3.e1churjz0 div.css-1id9gl1.e1edlm460');

                    if(productElements){

                        for(let index = 0; index < productElements.length; index++){
                            let template = JSON.parse(JSON.stringify(baseTemplate));
                            let element = productElements[index];

                            template.rank = startRank.toString();

                            // get title
                            try{
                                let title_element = element.querySelector('h3.e1p9icne10.css-1enb5i7.ezvizj33')
                                if (title_element) {
                                    template.title.value = title_element.innerText.trim();
                                    template.title.source = element.outerHTML;
                                }
                                
                            }catch(error){
                                template.title.value = FailureMessages.TITLE_EXTRACTION;
                                template.title.source = FailureMessages.GENERIC_NOT_FOUND;
                            }

                            // no brand

                            // get url
                            try{
                                let url_element = element.querySelector('a') || element.querySelector('h3.e1p9icne10.css-1enb5i7.ezvizj33')
                                if (url_element && url_element.hasAttribute('href')) {
                                    template.url.value = 'https://www.hellotv.nl/product' + url_element
                                    template.url.source = element.outerHTML;
                                }
                                
                            }catch(error){
                                template.url.value = FailureMessages.URL_EXTRACTION;
                                template.url.source = FailureMessages.GENERIC_NOT_FOUND;
                            }

                            result.push(template);
                            startRank++;
                        }
                    }
                }catch(error){
                    return [];
                }
                
                return result;
            }, startRank, this.baseTemplate, FailureMessages);

            // extend the array
            result.push(...tempResult);

            // // check if we still need to paginate
            let hasNextPage = await page.evaluate(function(limit, resultLength, tempResultLength){

                try{
                    let nextElement = false
                    let paginationContainer = document.querySelectorAll('a.e1kxb4nh0.css-1e19trk.evwtio30');
                    let pagination = null

                    if (paginationContainer.length == 1){
                        pagination = paginationContainer[0].innerText.trim()
                        if (pagination.includes('Volgende')){
                            nextElement = true
                        }
                    }
                    if (paginationContainer.length == 2){
                        pagination = paginationContainer[1].innerText.trim()
                        if (pagination.includes('Volgende')){
                            nextElement = true
                        }
                    }
                    
                    if(nextElement==false){
                        return false;
                    }
                    if(resultLength > limit){
                        return false;
                    }
                    if(tempResultLength == 0){
                        return false;
                    }
                }catch(error){
                    return false;
                }
                return true;
            }, limit, result.length, tempResult.length);

            if(!hasNextPage)
                break;

            // click pagination
            await page.evaluate(function(){
                let nextElement = document.querySelector('a.e1kxb4nh0.css-1e19trk.evwtio30');
                nextElement.click();
            }),

            // wait for the ajax reload
            await page.waitFor(10000);
            startRank = result.length + 1;
        }

        result = result.slice(0, limit);
        return result;
    }
}

export default HellotvNlWebsiteStrategy;