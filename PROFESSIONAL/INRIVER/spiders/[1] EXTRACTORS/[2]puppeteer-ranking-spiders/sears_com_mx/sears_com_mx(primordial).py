import { WebsiteStrategy } from "../base.js";
import puppeteer from 'puppeteer';
import { FailureMessages } from '../constants.js';

class SearsComMxWebsiteStrategy extends WebsiteStrategy{

    constructor(browserEndpoint, isHeadless, proxy){
        super(browserEndpoint, isHeadless, proxy);
        this.browserEndpoint = browserEndpoint;
        this.isHeadless = isHeadless;
        this.proxy = proxy;
    }

    async execute(keyword, userAgent, timeout, useLocalBrowser=false, startRank=1, limit=100){
        return new Promise(async (resolve, reject) => {
            let actualProxyIP = '';
            let browser = null;

            try{
                const args = [
                    '--ignore-certificate-errors', // for lower version of node and puppeteer
                    '--disable-gpu',
                    '--disable-dev-shm-usage',
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--allow-running-insecure-content'
                ];
    
                // Attach proxy to browser options
                let browserEndpoint = this.browserEndpoint;
    
                if (this.proxy) {
                    // browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&headless=false&stealth&--ignore-certificate-errors&--disable-gpu&--disable-dev-shm-usage&--no-sandbox&--disable-setuid-sandbox&--allow-running-insecure-content`;
                    browserEndpoint = `${browserEndpoint}?--proxy-server=${this.proxy.server}&headless=false`;
                    args.push(`--proxy-server=${this.proxy.server}`);
                }
                
                // Use local or remote browser
                if (useLocalBrowser) {
                    const options = {
                        args: args,
                        headless: this.isHeadless,
                        ignoreHTTPSErrors: true
                    };
                    browser = await puppeteer.launch(options);
                }
                else {
                    browser = await puppeteer.connect({
                        browserWSEndpoint: browserEndpoint,
                        ignoreHTTPSErrors: true,
                    });
                }
    
                // Brower event handler
                browser.on('disconnected', async () => {
                    console.log('[A*] Browser disconnected')
                    try {
                         // DEV HAX: close blowser completely just in case
                        if (browser) {
                            await browser.close()
                        }
                    }
                    catch(e) {
                        // Do nothing
                        console.log('[AX] Browser close failed')
                    }
                });
    
                // Open new page
                const page = await browser.newPage()
                page.on('response', async(response) => {
                    try {
                        if (!actualProxyIP || actualProxyIP.length < 1) {
                            actualProxyIP = response.remoteAddress().ip
                        }
                    }
                    catch(e) { }
                });
    
                // Set user agent
                if (!userAgent) {
                    userAgent = 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36';
                }
    
                await page.setUserAgent(userAgent);
    
                // Authenticate to proxy
                if (this.proxy && (this.proxy.user && this.proxy.pwd)) {
                    await page.authenticate({
                        username: this.proxy.user,
                        password: this.proxy.pwd
                    });
                }

                // Set page view port size
                await page.setViewport({width: 1920, height: 1080});
                let searchUrl = 'https://www.sears.com.mx/resultados/q=' + keyword
                await page.goto(encodeURI(searchUrl), {
                    waitUntil: 'networkidle0',
                    timeout: timeout
                });

                // wait for ajax reload
                await page.waitFor(10000);

                let products = await this._getProducts(page, keyword, startRank, limit, timeout);

                await page.waitFor(5000);

                let totalMegabytes = 0

                try {
                    // Get performance statistics
                    const performanceEntry = await page.evaluate(() => {
                        try {
                            return JSON.stringify(performance.getEntries())
                        }
                        catch(e) {
                            return null
                        }
                    })

                    // Calculate total Megabytes used
                    let performance = null

                    if (performanceEntry) {
                        performance = JSON.parse(performanceEntry)
                        let totalBytes = 0

                        for (let x = 0; x < performance.length; x++) {
                            if (!isNaN(performance[x].encodedBodySize) && typeof(performance[x].encodedBodySize) !== "undefined") {
                                totalBytes += performance[x].encodedBodySize
                            }
                        }

                        totalMegabytes = totalBytes * 0.000001
                    }
                }
                catch(ex) {}  // Do nothing
                
                resolve({ actualProxyIP: actualProxyIP, result: products, totalMegabytes: totalMegabytes })

            }catch(error){
                console.error(error)
                reject({error: `Unhandled exception for URL: https://www.sears.com.mx, keyword: ${keyword}`, actualProxyIP: actualProxyIP});
            }finally{
                if (browser) {
                    await browser.close();
                }
            }
        });
    }

    async _getProducts(page, keyword, startRank, limit, timeout){
        let result = [];

        try{
            while(true){
                let jsonData = await page.evaluate(async function(keyword, offset){
                    try{
                        let queryParams = {
                            'cliente': 'sears', 'proxystylesheet': 'xml2json',
                            'oe': 'UTF-8', 'getfields': '*',
                            'sort': '', 'start': offset.toString(),
                            'num': '48', 'q': keyword,
                            'requiredfields': '', 'ds': 'marcas:attribute_marca:0:0:8:0:1.sale_precio:sale_price:1:1:40.discount:discount:0:0:1000:0:1',
                            'do': 'categories:categories:id,name:10:1', 'requiredobjects': ''
                        }
        
                        let baseUrl = 'https://www.sears.com.mx/anteater/search?'
        
                        let queryParamsString = '';
                        for(let key in queryParams){
                            if(queryParams.hasOwnProperty(key)){
                                queryParamsString += `${key}=${queryParams[key]}&`
                            }
                        }
        
                        queryParamsString = queryParamsString.substring(0, queryParamsString.length - 1);
                        let url = baseUrl + encodeURI(queryParamsString);
        
                        let response = await fetch(url, {
                            method: 'GET',
                            credentials: 'include'
                        })

                        if(response.status == 200){
                            return await response.json();
                        }
        
                        return null;
                    }catch(error){
                        return null;
                    }
                }, keyword, (startRank - 1));
                
                let tempResult = [];
                if(jsonData){
                    
                    tempResult = function(jsonData, startRank, baseTemplate, FailureMessages){
                        let result = [];

                        let products = jsonData.GSP.RES.R;
                        for(let index = 0; index < products.length; index++){
                            let product = products[index];
                            let template = JSON.parse(JSON.stringify(baseTemplate));

                            template.rank = startRank.toString();
                            let stringified = JSON.stringify(product);

                            try{
                                template.title.value = product.T;
                                template.title.source = stringified;
                            }catch(error){
                                template.title.value = FailureMessages.TITLE_EXTRACTION;
                                template.title.source = FailureMessages.GENERIC_NOT_FOUND;
                            }

                            // try{
                            //     let mt = product.MT;
                            //     for(let i = 0; i < mt.length; i++){
                            //         let obj = mt[i];
                            //         if(obj.N == 'store'){
                            //             template.brand.value = obj.V;
                            //             template.brand.source = stringified;
                            //             break;
                            //         }
                            //     }
                            // }catch(error){
                            //     template.brand.value = FailureMessages.BRAND_EXTRACTION;
                            //     template.brand.source = FailureMessages.GENERIC_NOT_FOUND;
                            // }

                            try{
                                template.url.value = product.U;
                                template.url.source = stringified;
                            }catch(error){
                                template.title.value = FailureMessages.URL_EXTRACTION;
                                template.title.source = FailureMessages.GENERIC_NOT_FOUND;
                            }

                            result.push(template);
                            startRank++;
                        }

                        return result;
                    }(jsonData, startRank, this.baseTemplate, FailureMessages);
                }
                
                result.push(...tempResult);
                startRank = result.length + 1;

                if(result.length >= limit)
                    break;
                if(tempResult.length == 0)
                    break;

                await page.waitFor(5000);
            }
        }catch(error){
            console.log(error);
            throw error;
        }

        result = result.slice(0, limit);
        return result;
    }
}

export default SearsComMxWebsiteStrategy;