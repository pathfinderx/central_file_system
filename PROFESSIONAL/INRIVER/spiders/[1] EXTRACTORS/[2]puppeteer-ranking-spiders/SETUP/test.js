import fs from 'fs'
import { DocumentStatus } from "./services/constants.js";
import { BlobStorage } from './services/blob.js';
import template from './strategies/static/result_base_template.js';
import { StrategyFactory } from './strategies/factory.js';
import { ProxyConnection } from "./models.js";
import { tryCatchPromiseWrapper } from "./utils.js";
import dotenv from 'dotenv'
dotenv.config()
function formatReadableResult(result){
    console.log('printing results:')
    for(let index = 0; index < result.length; index++){
        console.log(`Rank: ${result[index].rank}`);
        console.log(`Title: ${result[index].title.value}`);
        console.log(`Brand: ${result[index].brand.value}`);
        console.log(`URL: ${result[index].url.value}`);
        console.log('-----------------');
    }
}
async function main(){
    const countryCode = 'pt';
    const website = 'pcdiga.com'
    const keyword = 'headset'
    const browserEndpoint = process.env.BROWSER_ENDPOINT
    const timeout = process.env.BROWSER_TIMEOUT
    const userAgent = 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'
    const isHeadless = false;
    const useLocalBrowser = true;
    console.log('[*] Init proxy')
    var proxyConnection = new ProxyConnection(process.env.LUMINATI_SERVER, process.env.LUMINATI_USER + countryCode.toLowerCase(), process.env.LUMINATI_PWD)
    const factory = new StrategyFactory();
    const context = await factory.getWebsiteStrategy(countryCode.toLowerCase(), website);
    if (!context) {
        console.error('[x] Strategy identification failed')
    }
    else {
        const strategy = new context(browserEndpoint, isHeadless, proxyConnection);
        let [strategyError, strategyResult] = await tryCatchPromiseWrapper(strategy.execute(keyword, userAgent, timeout, useLocalBrowser, 1, 20));
        formatReadableResult(strategyResult.result);
        fs.writeFileSync('../extract-transform-rankings/sample_batch.json', JSON.stringify(strategyResult.result), function(error){
        });
    }
}
main()
