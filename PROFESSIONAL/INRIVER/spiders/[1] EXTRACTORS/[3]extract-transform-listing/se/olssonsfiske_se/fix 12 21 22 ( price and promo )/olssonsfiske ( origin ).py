import re
import unicodedata
from transformers.base import TransformationStrategy
from transformers.exceptions import TransformationFailureException
from transformers.constants import Availability
from constants import ETDefaults

class OlssonsfiskeSeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super(TransformationStrategy, self).__init__()
        
        self.instock_values = [
            "http://schema.org/instock",
            "https://schema.org/instock",
            "http://schema.org/instoreonly",
            "https://schema.org/instoreonly",
            "http://schema.org/onlineonly",
            "https://schema.org/onlineonly",
            "http://schema.org/limitedavailability",
            "https://schema.org/limitedavailability",
            "http://schema.org/preorder",
            "https://schema.org/preorder",
            "http://schema.org/presale",
            "https://schema.org/presale"
        ]
        self.outofstock_values = [
            "http://schema.org/discontinued",
            "https://schema.org/discontinued",
            "http://schema.org/outofstock",
            "https://schema.org/outofstock",
            "http://schema.org/soldout",
            "https://schema.org/soldout"
        ]

    # ET PRICE
    def transform_price(self, value):
        value = re.sub('[^0-9.]', '', value.replace(',', '.'))
        return value
    
    # ET stocks
    def transform_availability(self, value):
        value = value.replace("Ã¤", "ä")
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        else: 
            pattern = re.compile(r'tillfälligt', re.IGNORECASE)
            results = pattern.findall(availability)
                
            if results:
                return Availability.OUT_OF_STOCK.value
            
            pattern = re.compile(r'(\d+) st', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.IN_STOCK.value

        return Availability.NOT_FOUND.value 