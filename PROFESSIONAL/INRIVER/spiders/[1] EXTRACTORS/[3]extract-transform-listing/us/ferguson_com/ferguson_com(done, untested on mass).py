import re
import unicodedata
import html
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException

class FergusonComTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

    def transform_price(self, value):
        try:
            value = value[1:]
            value = value.replace(",", "")
            return round(float(value), 2)
        except Exception:
            raise TransformationFailureException('Price transformation failed')

    def transform_availability(self, value):
        try:
            availability = value.lower()

            count = availability[0]
            if count:
                isAvailable = True if int(count) > 0 else False
                return Availability.IN_STOCK.value if isAvailable else Availability.OUT_OF_STOCK.value # ternary operation

            else:
                return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')
 
    