import re
import unicodedata
import html
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException

class GraingerComTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

    def transform_price(self, value):
        try:
            value = value[1:]
            value = value.replace(",", "")
            return round(float(value), 2)
        except Exception:
            raise TransformationFailureException('Price transformation failed')