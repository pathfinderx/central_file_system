from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException
from copy import deepcopy
import re


class ParisClTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'true'
        ])

        self.outofstock_values.extend([
            'false',
        ])   

    def transform_price(self, value):
        try:
            preserved_value = deepcopy(value)
            value = re.sub('[^0-9\.]', '', str(value))

            if self.hasDoubleDecimalPoints(preserved_value):
                replaced_value = value.replace('.', '')
                value = f"{replaced_value[:-3]}.{replaced_value[-3:]}"

            return super().transform_price(value)

        except Exception as e:
            raise TransformationFailureException('Price transformation failed')

    # transform_price() sub-function
    def hasDoubleDecimalPoints(self, value:str) -> bool:
        try:
            list_cont = [x for x in value if x in ['.']]
            return True if (len(list_cont)) > 1 else False

        except Exception as e:
            print('Error at: transformer -> hasDoubleDecimalPoints() -> with message: ', e)

    def transform_availability(self, value):
        try:
            availability = value.lower()

            if availability in self.instock_values:
                return Availability.IN_STOCK.value

            elif availability in self.outofstock_values:
                return Availability.OUT_OF_STOCK.value

            return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')