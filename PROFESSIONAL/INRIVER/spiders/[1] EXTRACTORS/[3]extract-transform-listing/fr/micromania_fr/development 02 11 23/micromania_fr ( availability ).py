from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException

class MicromaniaFrTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'http://schema.org/InStock',
        ])
        
    def transform_availability(self, value):
        try:
            availability = value.lower()

            if availability in self.instock_values:
                return Availability.IN_STOCK.value

            elif availability in self.outofstock_values:
                return Availability.OUT_OF_STOCK.value

            return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')