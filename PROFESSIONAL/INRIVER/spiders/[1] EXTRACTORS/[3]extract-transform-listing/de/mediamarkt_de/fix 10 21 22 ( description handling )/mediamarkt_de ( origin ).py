import re
import unicodedata
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from transformers.exceptions import TransformationFailureException

class MediamarktDeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'Lieferung',
            'lieferung'
        ])
    
    def transform_price(self, value):
        try:
            value = re.sub(r"[^0-9.]", "", unicodedata.normalize('NFKD', value))
            return super().transform_price(value)
        except Exception:
            raise TransformationFailureException('Price transformation failed.')

    def transform_rating(self, value, max_value):
        try:
            if "von" in value:
                value = value.split("von")
                if len(value):
                    value = re.sub(r"[^0-9.]", "", value[0].replace(".", "").replace(",", "."))
            return super().transform_rating(value, max_value)
        except Exception:
            raise TransformationFailureException('Rating transformation failed.')

    def transform_reviews(self, value):
        try:
            value = value.replace('(', '').replace(')', '')
            return super().transform_reviews(value)
        except Exception as e:
            raise TransformationFailureException('Reviews transformation failed')
    