import re, html
import unicodedata
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from transformers.exceptions import TransformationFailureException
import copy
from constants import ETDefaults

class MediamarktDeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'Lieferung',
            'lieferung',
            'in_warehouse'
        ])
    
    def transform_price(self, value):
        try:
            origin_value = copy.copy(value)

            if (isinstance(origin_value, float) or isinstance(origin_value, int)):
                price = re.sub(r"[^0-9.]", "", unicodedata.normalize('NFKD', str(value)))
                return super().transform_price(price)
            else:
                value = re.sub(r"[^0-9.]", "", unicodedata.normalize('NFKD', value))

                return super().transform_price(value)

        except Exception as e:
            raise TransformationFailureException('Price transformation failed.')

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        else:
            return Availability.OUT_OF_STOCK.value

    def transform_description(self, value):
        if value:
            value = html.unescape(value)
        else:
            value = ETDefaults.NOT_FOUND.value
        return super().transform_description(str(value))


    def transform_rating(self, value, max_value):
        try:
            origin_value = copy.copy(value)

            if (isinstance(origin_value, float) or isinstance(origin_value, int)):
                ratings_value = str(origin_value)
                return super().transform_rating(ratings_value, max_value)

            elif "von" in value:
                value = value.split("von")
                if len(value):
                    value = re.sub(r"[^0-9.]", "", value[0].replace(".", "").replace(",", "."))
           
                return super().transform_rating(value, max_value)

            else:
                value = ETDefaults.NOT_FOUND.value
                return super().transform_rating(str(value), max_value)

        except Exception:
            raise TransformationFailureException('Rating transformation failed.')

    def transform_reviews(self, value):
        try:
            origin_value = copy.copy(value)
            if (isinstance(origin_value, float) or isinstance(origin_value, int)):
                reviews = str(origin_value)
                return super().transform_reviews(reviews)
            else:
                value = value.replace('(', '').replace(')', '')
                return super().transform_reviews(value)
        except Exception as e:
            raise TransformationFailureException('Reviews transformation failed')
    