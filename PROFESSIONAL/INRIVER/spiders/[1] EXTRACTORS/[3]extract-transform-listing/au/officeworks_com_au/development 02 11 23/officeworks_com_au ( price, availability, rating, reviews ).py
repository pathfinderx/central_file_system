from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException
import re

class OfficeworksComAuTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'nearest',
            'standard'
        ])

        self.outofstock_values.extend([
            'outOfStock',
        ])   

    def transform_price(self, value):
        try:
            value = re.sub('[^0-9\.]', '', str(value))
            
            if isinstance(value, str):
                value = str(float(int(value) / 100))
                
            return super().transform_price(value)

        except Exception as e:
            raise TransformationFailureException('Price transformation failed')

    def transform_availability(self, value):
        try:
            availability = value.lower()

            if availability in self.instock_values:
                return Availability.IN_STOCK.value

            elif availability in self.outofstock_values:
                return Availability.OUT_OF_STOCK.value

            return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')

    def transform_rating(self, value, max_value):
        try:
            value = round(float(value), 2)
            return value

        except Exception:
            raise TransformationFailureException('Rating transformation failed')  

    def transform_reviews(self, value):
        try:
            value = float(value)
            return value

        except Exception:
            raise TransformationFailureException('Reviews transformation failed')            