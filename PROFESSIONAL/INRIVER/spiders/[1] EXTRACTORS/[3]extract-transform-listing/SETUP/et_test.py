import json

from pprint import pprint
from transformers import Transformer
from post_transformers import PostTransformer
from builder import ETPayloadBuilder
from validators import RunningmanETDataValidator

def transform(retailer, country_code, data):

    transformer = Transformer(retailer, country_code=country_code)
    post_transformer = PostTransformer(retailer, country_code=country_code)

    payload_builder = ETPayloadBuilder(transformer, post_transformer)
    
    # raises an exception
    transformed_data = payload_builder.build(data)

    return transformed_data

if __name__ == "__main__":

    retailer = 'www.utomhusliv.se'
    country_code = 'se'
    
    with open('sample.json', 'r') as f:
        data = json.load(f)
        warnings = []
        fields = []
        transformed_data_results = []   

        for d in data:
            transformed_data = transform(retailer, country_code, d)
            transformed_data_results.append(transformed_data)
            # print(d['price']['value'], ' -- ',d['availability']['value'])
            # print(d['rank'], d['availability']['value'])
            
            
            # print(d['price']['value'] + ' -- ' + d['promo']['price']['value'])
            # print(d['rating']['score']['value'], ' -- ', d['rating']['reviews']['value'])
            # print(d['promo']['description']['value'])
            # print(d['availability']['value'])
            
            rm_et_data_validator = RunningmanETDataValidator(transformed_data)
            warning_fields_mapping = rm_et_data_validator.get_warning_fields_mapping()

            if warning_fields_mapping:
                warnings.append({
                    'rank': transformed_data['rank'],
                    'warnings': warning_fields_mapping
                })

            for key in warning_fields_mapping:
                if not key in fields:
                    fields.append(key)

        
        print()
        pprint(transformed_data_results)
        print()
        print(warnings)
        print()
        print('Fields w issues:' + str(fields))