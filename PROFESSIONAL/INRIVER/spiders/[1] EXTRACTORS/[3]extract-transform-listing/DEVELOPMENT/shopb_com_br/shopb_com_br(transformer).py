import re
import unicodedata, html
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException

class ShopbComBrTransformationStrategy(TransformationStrategy):

    def __init__(self):
        super().__init__()

        self.instock_values = [
            "disponível",
            "adicionar ao carrinho"
        ]
        self.outofstock_values = [
            "avise-me quando disponível",
            "indisponível"
        ]

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value

    def transform_price(self, value):
    
        try:
            if 'r' in value.lower(): # For R$ 539,10 format
                value = re.sub(r'[^0-9,]', '', value)
                value = value.replace('.', '').replace(',', '.')

            return round(float(value), 2)

        except Exception:
            raise TransformationFailureException('Price transformation failed')


    def transform_rating(self, value, max_value):
        try:
            value = value.replace(',', '.')
            value = (float(value) / float(max_value)) * Defaults.MAX_RATING.value
            return round(value, 1)

        except Exception:
            raise TransformationFailureException('Rating transformation failed')

    def transform_reviews(self, value):
        try:
            return int(value.replace('(', '').replace(')', ''))

        except Exception:
            raise TransformationFailureException('Reviews transformation failed')