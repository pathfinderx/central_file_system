import cloudscraper
from bs4 import BeautifulSoup
from urllib.parse import quote
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

class AllegroPlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.67',
                "path": "/listing?string=tablet",
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9,no;q=0.8,sv;q=0.7,nl;q=0.6"
            }
        
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        for page in range(0, len(urls)):
            try:
                res = self.scraper.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    has_redirect = False
                    for item in res.history:
                        if item.status_code in [301, 307]:
                            has_redirect = True
                            break

                    if has_redirect:
                        break

                    if self.pdp_checker(res.text):
                        raw_data_list.append(res.text)
                    else:
                        break            
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        q_search_string = quote(search_string)
        
        urls = [
            f"https://allegro.pl/listing?string={q_search_string}",
            f"https://allegro.pl/listing?string={q_search_string}&p=2",
            f"https://allegro.pl/listing?string={q_search_string}&p=3",
            f"https://allegro.pl/listing?string={q_search_string}&p=4"
        ]

        return urls

    def pdp_checker(self, text):
        bs = BeautifulSoup(text, 'lxml')
        tag = bs.select('div.opbox-listing article')

        if tag:
            return True
        else:
            return False