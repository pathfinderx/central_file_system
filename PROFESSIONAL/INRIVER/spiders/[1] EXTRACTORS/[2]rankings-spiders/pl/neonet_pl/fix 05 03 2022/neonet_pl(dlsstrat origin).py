import urllib
from bs4 import BeautifulSoup
import json

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

class NeonetPlDownloadStrategy(DownloadStrategy):

    LIMIT = 100

    def __init__(self, requester):
        self.requester = requester

        # shared state across method calls, provides isolation
        self.__state = None

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        # NOTE: Reset the state on every download
        # every download is different state of the instance
        self.__state = {}

        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                'accept': '*/*',
                'host': 'www.neonet.pl'
        }

        if not isinstance(cookies, dict):
            cookies = None

        self.__state['search_string'] = search_string
        self.__state['raw_data'] = []
        self.__state['num_products'] = 0
        self.__state['page'] = 1

        
        self.__state['item_ids'] = self.__get_item_ids() # get the list item_ids first
        self.__state['items_per_iteration'] = 50
        
        while True:
            # slice the array
            item_ids = self.__state['item_ids'][self.__state['num_products']: self.__state['num_products'] + self.__state['items_per_iteration']]
            url = self.__generate_url(item_ids)
            try:
                response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                if response.status_code in [414]:
                    self.__state['item_ids'] = self.__get_item_ids()
                    item_ids = self.__state['item_ids'][self.__state['num_products']: self.__state['num_products'] + self.__state['items_per_iteration']]
                    url = self.__generate_new_url(item_ids)
                    response = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

                if response.status_code in [200, 201]:
                    self.__state['json'] = json.loads(response.text)

                    self.__state['current_num_products'] =  self.__count_products(self.__state['json'])
                    self.__state['num_products'] += self.__state['current_num_products']

                    if self.__state['current_num_products'] > 0:
                        sorted_data = self.__get_sorted_data(item_ids, response.text)
                        self.__state['raw_data'].append(json.dumps(sorted_data))

                    if not self.__has_next_page():
                        break

                    self.__state['page'] += 1
                
                else:
                    raise PaginationFailureException
            except PaginationFailureException as e:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(self.__state['page']+1)))
            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return self.__state['raw_data']
    
    
    def __get_sorted_data(self, item_ids: list, reponse_text: str):
        
        data = json.loads(reponse_text, strict=False)
        sorted_data = []
        for _id in item_ids:
            for item in data['data']['products']['items']:
                if _id == int(item['sku']):
                    sorted_data.append(item)
        
        return sorted_data
                
    def __get_item_ids(self):
        try:
            variables = {
                "attributes": [],
                "sortOrder": "DESC",
                "sortBy": "score",
                "query": self.__state['search_string']
            }
            params = {
                'query': 'query getProductSearch($query: String!, $attributes: [String], $sortBy: String, $sortOrder: String) { result: msSearch(query: $query, autosuggest: false) { redirect { type id canonical_url } products: msProducts(filter: {attributes: $attributes}, sort: {sort_by: $sortBy, sort_order: $sortOrder}, facet: true) { total_count items_ids corrected_query stats { code values { min max count } } facets { code values { value_id count } } } } \n}',
                'variables': json.dumps(variables, ensure_ascii=False),
                'v': '2.122.2'
            }
            headers = {
                'user-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                'accept': '*/*',
                'host': 'www.neonet.pl',
                'referer': 'https://www.neonet.pl/search.html?order=score&query={}'.format(self.__state['search_string'])
            }

            url = 'https://www.neonet.pl/graphql?{}'.format(urllib.parse.urlencode(params, quote_via = urllib.parse.quote))
            response = self.requester.get(url, headers=headers)

            data = json.loads(response.text)
            item_ids = data['data']['result']['products']['items_ids']

            return item_ids
        except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

    def __get_new_item_ids(self):
        try:
            params = {
                'query': 'query getProductSearch($query: String!, $attributes: [String], $sortBy: String, $sortOrder: String) { result: msSearch(query: $query, autosuggest: false) { redirect { type id canonical_url } products: msProducts(filter: {attributes: $attributes}, sort: {sort_by: $sortBy, sort_order: $sortOrder}, facet: true) { total_count items_ids corrected_query stats { code values { min max count } } facets { code values { value_id count } } } } }',
                'v': '2.64.0'
            }
            variables = {
                "attributes": [],
                "sortOrder": "DESC",
                "sortBy": "score",
                "query": self.__state['search_string']
            }
            params['variables'] = json.dumps(variables, ensure_ascii=False)
            headers = {
                'user-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
                'accept': '*/*',
                'host': 'www.neonet.pl'
            }

            url = 'https://www.neonet.pl/graphql?{}'.format(urllib.parse.urlencode(params))
            response = self.requester.get(url, headers=headers)

            data = json.loads(response.text)
            item_ids = data['data']['result']['products']['items_ids']

            return item_ids
        except:
                raise DownloadFailureException('Download failed - Unhandled Exception')                



    def __generate_url(self, item_ids):
        variables = {"ids":item_ids}
        params = {
            'query': 'query msProducts( $ids: [Int] ) { products: msProducts( filter: { skus: $ids } attributes: true ) { items { id,sku,gid,name,price,final_price,request_path,is_preorder,is_pre_release,is_ext,is_wpn,is_virtual_code,release_date,main_category,thumbnail,images_gallery,max_sale_qty,min_sale_qty,availability,available_only_in_store,available_in_store,energy_class,euro_energy_class,rich_banner,rich_reviews_banner,energy_class_label,product_card,manufacturer,variants,series,main_category,attributes { value attribute_code },request_path,availability_status_element,labels,review_count,review_summary,seo_description,seo_additional_description,seo_additional_description_images,path_to_category,category_id,brand_plan,industry,y2b_product_preview,is_wzs_available gid } } }',
            'variables': json.dumps(variables, ensure_ascii=False),
            'v': '2.122.2'
        }
        
        url = 'https://www.neonet.pl/graphql?{}'.format(urllib.parse.urlencode(params))
        return url

    def __generate_new_url(self, item_ids):
        params = {
            'query': 'query msProducts( $ids: [Int] ) { products: msProducts( filter: { skus: $ids } attributes: true ) { items { id,sku,gid,name,price,final_price,request_path,is_preorder,is_pre_release,is_ext,is_wpn,is_virtual_code,release_date,main_category,thumbnail,images_gallery,max_sale_qty,min_sale_qty,availability,available_only_in_store,available_in_store,energy_class,euro_energy_class,rich_banner,rich_reviews_banner,energy_class_label,product_card,manufacturer,variants,series,main_category,attributes { value attribute_code },request_path,availability_status_element,labels,review_count,review_summary,seo_description,seo_additional_description,seo_additional_description_images,path_to_category,category_id,brand_plan,industry,y2b_product_preview,is_wzs_available gid } } }',
            'v': '2.122.2'
        }

        variables = {
            "ids": item_ids  
        }

        params['variables'] = json.dumps(variables, ensure_ascii=False)

        url = 'https://www.neonet.pl/graphql?{}'.format(urllib.parse.urlencode(params))

        return url


    def __has_next_page(self):
        if self.__state['num_products'] >= self.__class__.LIMIT:
            return False

        if self.__state['current_num_products'] == 0:
            return False

        return True

    def __count_products(self, data):
        try:
            return len(data['data']['products']['items'])
        except Exception:
            return 0