import json
import re
from tkinter.messagebox import NO
from urllib.parse import urlencode, quote, unquote

from bs4 import BeautifulSoup
from request.unblocker import UnblockerSessionRequests

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class MediaexpertPlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        # self.requester = requester
        self.requester = UnblockerSessionRequests('pl')

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "accept": "application/vnd.enp.api+json;version=v1",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                "content-type": "application/json",
                "content-website": "4",
                "referer": "",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36",
                "x-legacy-offers-mode": "1"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls, referer = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        hits_per_page = 30 # based from website
        for page in range(0, len(urls)):
            try:
                headers['referer'] = referer
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                
                if res.status_code in [200, 201]:
                    data = res.json()
                    data['source'] = urls[page]
                    data['category_redirect'] = self.category_products if self.category_products else None
                    data = json.dumps(data)
                    raw_data_list.append(data)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        
        urls = []
        params = {
            'query[menu_item]': "",
            'query[querystring]': search_string

        }

        for page in range(1, 2):
            if page < 1:
                params['page'] = page
            
            url = referer = 'https://www.mediaexpert.pl/search?{}'.format(urlencode(params))
            
            api_url, _params = self.get_prerequisites(url)

            _url = '{}?{}'.format(api_url, urlencode(_params))
            urls.append(_url)


        return urls, referer
    

    def get_prerequisites(self, url):

        res = self.requester.get(url)

        if res.ok:
            soup = BeautifulSoup(res.text, 'lxml')

            tag = soup.select_one('script#state') or soup.select_one('div#state')

            if tag:
                data = json.loads(unquote(tag.text))

                query = None
                
                product_ids = data['Service:GenericProductListService.state']['state']['productIds']

                self.category_products = data['Service:GenericProductListService.state']['state']['loadedOffers']

                logs = data['Spark:LoggerService.state']['state']['logs'] if 'Spark:LoggerService.state' in data else []

                # Get product_ids from tags
                tags = soup.select('[class*="offer-box offer-"]')

                for idx, tag in enumerate(tags):
                    prod_id = tag['class'][-1].split('-')[-1]
                    product_ids.insert(idx, int(prod_id))

                # Remove duplicates    
                new_product_ids = []
                for _id in product_ids:
                    if _id not in new_product_ids:
                        new_product_ids.append(_id)


                for log in logs:
                    if len(log['arguments']) <= 1:
                        for argument in log['arguments']:
                            if 'productSlug' in argument:
                                query = argument
                                break
                
                if not query:
                    query = 'query QuerySimpleProductOfferByProduct{byId(identifierName:"productId",identifierValues:["2959788"]){id,name,productId,productParentId,addToCart,link,type,serviceType,internalCode,stockShipment,stockShop,gallery,availability{id,name,displayName,description,descriptionDetails,colorHash,icon,weight,isDefault,type},price{priceGross,priceNet,promoPrice{campaignId,actionId,showNewPrice,showOldPrice,clubPrice,forActionPrice,forActionCampaignId,forActionCode,newPrice,newPriceNet,oldPrice,discountInfoCollection{infoType,value},discountInfoBasicCollection{infoType,value},productPrice,dateFrom,dateTo},discount{value,oldPrice,percentage,dateTimeStart,dateTimeEnd}},flags{systemName,systemCode,frontName,frontIcon,frontColor,frontBackgroundColor,frontClass,displayOnFront},postscripts{id,content,displayPriceStrategy,tooltip,place,zone,url,gtm{name,position}},attributeCollection{originalName,displayName,attributes},specification{name,attributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}}},categories{id,name,mainCategory},systemAttributes(originalName:["erp_id","producer","pre_index","display_name","model","ean","points","GSPREMIA","additional_text","energy_label_new","skala_energetyczna_front","energy_label_photo","Klasa_energetyczna_global","karta towarowa","przelicznik","jm_zawartosci","outlet_description"]){id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image,settings{setting,value}}},menuItemAttributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}},opinionsAverage{average,count},hotDeals{quantityPromoted,remainingQuantity,promotionName},variants{label,displayMethod,hideSingleValue,variantProductOffer{label,offer{productId,id}}},keyAttributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}}}}'
                    
                identifiers = re.search('identifierValues:(\["\d+"\])', query)

                if identifiers:
                    str_product_ids = json.dumps(["{}".format(i) for i in product_ids])
                    query = query.replace(identifiers.group(1), str_product_ids, 1)
                    if query.startswith('Q: '):
                        query = query[3:]

        params = {
            'query': query
        }

        api_url = 'https://www.mediaexpert.pl/api/graphql/product-offer/query'
        
        return api_url, params

                        

    def get_page_hits(self, raw_data):
        hits = 0
        next_page = False
        soup = BeautifulSoup(raw_data, 'lxml')
        tags = soup.select('div.c-offerBox.is-wide[data-zone="OFFERBOX"]') or soup.select('.list-items .offer-box')
        if tags:
            hits = len(tags)

        tag = soup.select_one('a.is-nextLink')
        if tag:
            next_page = True
        
        return hits, next_page

    def __has_data(self, raw_data):
        #If data count is equal to 1, it redirects directly to the pdp.
        soup = BeautifulSoup(raw_data, 'lxml')

        product_tags = soup.select('div.c-offerBox.is-wide[data-zone="OFFERBOX"]') or soup.select('.list-items .offer-box') #for redirected to category

        if len(product_tags) <= 1:
            return False


        return True