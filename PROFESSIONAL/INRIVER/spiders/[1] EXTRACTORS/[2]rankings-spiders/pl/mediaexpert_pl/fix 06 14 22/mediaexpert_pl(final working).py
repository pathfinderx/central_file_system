import json
import re
from tkinter.messagebox import NO
from urllib.parse import urlencode, quote, unquote

from bs4 import BeautifulSoup
from request.unblocker import UnblockerSessionRequests

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class MediaexpertPlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        # self.requester = requester
        self.requester = UnblockerSessionRequests('pl')
        self.headers = None

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "accept": "application/vnd.enp.api+json;version=v1",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                "content-type": "application/json",
                "content-website": "4",
                "referer": "",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36",
                "x-legacy-offers-mode": "1"
            }

        self.headers = {
            'accept': 'application/vnd.enp.api+json;version=v1',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'content-website': '4',
            'cookie': '_ga=GA1.2.157155054.1655175257; _gid=GA1.2.1649891097.1655175275; _gcl_au=1.1.1240898452.1655175276; _fbp=fb.1.1655175275690.539913956; PHPSESSID=qh1nab6c7pq2dgjobqdg3eiqsa; meips=undefined; enp_wish_list_token=%5B%2241972f263b277fb20d81e36e155306a3%22%5D; _snrs_uuid=b75f7314-e041-45b6-a0dd-2da060aaa80a; _snrs_puuid=b75f7314-e041-45b6-a0dd-2da060aaa80a; _snrs_cid=178640129540; SEARCH_STORED_KEY=; _snrs_sa=ssuid:5e2ef25c-5554-4337-a75d-3237a576c53d&appear:1655175280&sessionVisits:2; _snrs_sb=ssuid:5e2ef25c-5554-4337-a75d-3237a576c53d&leaves:1655175297; _snrs_p=host:www.mediaexpert.pl&permUuid:b75f7314-e041-45b6-a0dd-2da060aaa80a&uuid:b75f7314-e041-45b6-a0dd-2da060aaa80a&identityHash:&user_hash:&init:1655175280&last:1655175280&current:1655175295&uniqueVisits:1&allVisits:2; SEARCH_INPUT_USE=0; _dc_gtm_UA-6214116-3=1; pageviewCount=3; _ga_WEJ054CFRT=GS1.1.1655175257.1.1.1655175346.32; _uetsid=4865a2f0eb8d11eca815693011ab1a04; _uetvid=4865c2a0eb8d11ec8d5f71202c2cec5d; cto_bundle=YhnnRF9uVnpkN1dROHE0ZmFtYkhrbTVrQTRSYTJqczFqR09yQVJMWmxvVjBOa3lCc3ZNUjNwbnNJVjdjUG5SNnF1QnN0bk1ibVZ2bCUyQmZocDd4TCUyRnI3OXY0YVRGd3ZvQ2NhQnVXN2llY1VsTTh0bmZnNzBkYTh3eFdtZFExSWR3Z2p3elVoNWx3T0ZmUUNtQnZOUUJBdFRyV1RRJTNEJTNE; SPARK_TEST=1',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
            'x-legacy-offers-mode': '1',
        }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            # first option (self.get_urls -> self.get_prerequisites())
            # urls = self.get_urls(search_string)

            # second option (self.get_urls2 -> self.get_prerequisites2()) if 1st payload prerequisite failed
            urls = self.get_urls2(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        hits_per_page = 30 # based from website
        for page in range(0, len(urls)):
            try:
                # headers['referer'] = referer
                res = self.requester.get(urls[page], timeout=timeout, headers=self.headers, cookies=cookies)
                
                if res.status_code in [200, 201]:
                    data = res.json()
                    data['source'] = urls[page]
                    data['category_redirect'] = self.category_products if self.category_products else None
                    data = json.dumps(data)
                    raw_data_list.append(data)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        
        urls = []
        params = {
            'query[menu_item]': "",
            'query[querystring]': search_string

        }

        for page in range(1, 2):
            if page < 1:
                params['page'] = page
            
            url = referer = 'https://www.mediaexpert.pl/search?{}'.format(urlencode(params))
            
            api_url, _params = self.get_prerequisites(url)

            _url = '{}?{}'.format(api_url, urlencode(_params))
            urls.append(_url)


        return urls, referer

    # second option if 1st payload prerequisite failed
    def get_urls2(self, search_string):
        
        urls = []
        params = {
            'query[menu_item]': "",
            'query[querystring]': search_string

        }

        for page in range(1, 2):
            if page < 1:
                params['page'] = page

            url = 'https://www.mediaexpert.pl/search?{}'.format(urlencode(params))
            api_url = self.get_prerequisites2(url)
            urls.append(api_url)

        return urls

    def get_prerequisites(self, url):

        res = self.requester.get(url)

        if res.ok:
            soup = BeautifulSoup(res.text, 'lxml')

            tag = soup.select_one('script#state') or soup.select_one('div#state')

            if tag:
                data = json.loads(unquote(tag.text))

                query = None
                
                product_ids = data['Service:GenericProductListService.state']['state']['productIds']

                self.category_products = data['Service:GenericProductListService.state']['state']['loadedOffers']

                logs = data['Spark:LoggerService.state']['state']['logs'] if 'Spark:LoggerService.state' in data else []

                # Get product_ids from tags
                tags = soup.select('[class*="offer-box offer-"]')

                for idx, tag in enumerate(tags):
                    prod_id = tag['class'][-1].split('-')[-1]
                    product_ids.insert(idx, int(prod_id))

                # Remove duplicates    
                new_product_ids = []
                for _id in product_ids:
                    if _id not in new_product_ids:
                        new_product_ids.append(_id)


                for log in logs:
                    if len(log['arguments']) <= 1:
                        for argument in log['arguments']:
                            if 'productSlug' in argument:
                                query = argument
                                break
                
                if not query:
                    query = 'query QuerySimpleProductOfferByProduct{byId(identifierName:"productId",identifierValues:["2959788"]){id,name,productId,productParentId,addToCart,link,type,serviceType,internalCode,stockShipment,stockShop,gallery,availability{id,name,displayName,description,descriptionDetails,colorHash,icon,weight,isDefault,type},price{priceGross,priceNet,promoPrice{campaignId,actionId,showNewPrice,showOldPrice,clubPrice,forActionPrice,forActionCampaignId,forActionCode,newPrice,newPriceNet,oldPrice,discountInfoCollection{infoType,value},discountInfoBasicCollection{infoType,value},productPrice,dateFrom,dateTo},discount{value,oldPrice,percentage,dateTimeStart,dateTimeEnd}},flags{systemName,systemCode,frontName,frontIcon,frontColor,frontBackgroundColor,frontClass,displayOnFront},postscripts{id,content,displayPriceStrategy,tooltip,place,zone,url,gtm{name,position}},attributeCollection{originalName,displayName,attributes},specification{name,attributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}}},categories{id,name,mainCategory},systemAttributes(originalName:["erp_id","producer","pre_index","display_name","model","ean","points","GSPREMIA","additional_text","energy_label_new","skala_energetyczna_front","energy_label_photo","Klasa_energetyczna_global","karta towarowa","przelicznik","jm_zawartosci","outlet_description"]){id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image,settings{setting,value}}},menuItemAttributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}},opinionsAverage{average,count},hotDeals{quantityPromoted,remainingQuantity,promotionName},variants{label,displayMethod,hideSingleValue,variantProductOffer{label,offer{productId,id}}},keyAttributes{id,name,originalName,isHintLong,validationType,hintLink,values{id,name,hint,image}}}}'
                    
                identifiers = re.search('identifierValues:(\["\d+"\])', query)

                if identifiers:
                    str_product_ids = json.dumps(["{}".format(i) for i in product_ids])
                    query = query.replace(identifiers.group(1), str_product_ids, 1)
                    if query.startswith('Q: '):
                        query = query[3:]

        params = {
            'query': query
        }

        api_url = 'https://www.mediaexpert.pl/api/graphql/product-offer/query'
        
        return api_url, params

    # second option if 1st payload prerequisite failed
    def get_prerequisites2 (self, url):
        try:
            res = self.requester.get(url)
            if res.status_code in [200]:
                soup = BeautifulSoup(res.text, 'lxml')
                tag = soup.select_one('script#state') or soup.select_one('div#state')

                if tag:
                    try:
                        data, query, product_ids, logs= json.loads(unquote(tag.text)), None, None, None
            
                        if (data and 'Service:GenericProductListService.state' in data):
                            if (data['Service:GenericProductListService.state']['productIds']):
                                product_ids = data['Service:GenericProductListService.state']['productIds']

                            elif data['Service:GenericProductListService.state']['state']['productIds']:
                                product_ids = data['Service:GenericProductListService.state']['state']['productIds']

                        if (data and 'Service:GenericProductListService.state' in data):
                            if (data['Service:GenericProductListService.state']['loadedOffers']):
                                self.category_products = data['Service:GenericProductListService.state']['loadedOffers']
                            elif (data['Service:GenericProductListService.state']['state']['loadedOffers']):
                                self.category_products = data['Service:GenericProductListService.state']['state']['loadedOffers']

                        logs = data['Spark:LoggerService.state']['state']['logs'] if 'Spark:LoggerService.state' in data else []
                    except Exception as e:
                        print('Error at: dlstrat -> get_prerequisites() -> with message: ', e)

                    # Get product_ids from tags
                    tags = soup.select('[class*="offer-box offer-"]')

                    for idx, tag in enumerate(tags):
                        prod_id = tag['class'][-1].split('-')[-1]
                        product_ids.insert(idx, int(prod_id))

                    # Remove duplicates    
                    new_product_ids = []
                    for _id in product_ids:
                        if _id not in new_product_ids:
                            new_product_ids.append(_id)

                    for log in logs:
                        if len(log['arguments']) <= 1:
                            for argument in log['arguments']:
                                if 'productSlug' in argument:
                                    query = argument
                                    break
                    
                    if not query:
                        item_ids = str()

                        for x in product_ids:
                            item_ids += (f'%22{x}%22%2C')
                        
                        query = f'https://www.mediaexpert.pl/api/graphql/product-offer/query?query=query+QuerySimpleProductOfferByProduct%7BbyId%28identifierName%3A%22productId%22%2CidentifierValues%3A%5B{item_ids}%5D%29%7Bid%2Cname%2CproductId%2CproductParentId%2CaddToCart%2Clink%2Ctype%2CserviceType%2CinternalCode%2CstockShipment%2CstockShop%2Cgallery%2Cavailability%7Bid%2Cname%2CdisplayName%2Cdescription%2CdescriptionDetails%2CcolorHash%2Cicon%2Cweight%2CisDefault%2Ctype%7D%2Cprice%7BpriceGross%2CpriceNet%2CpromoPrice%7BcampaignId%2CactionId%2CshowNewPrice%2CshowOldPrice%2CclubPrice%2CforActionPrice%2CforActionCampaignId%2CforActionCode%2CnewPrice%2CnewPriceNet%2ColdPrice%2CdiscountInfoCollection%7BinfoType%2Cvalue%7D%2CdiscountInfoBasicCollection%7BinfoType%2Cvalue%7D%2CproductPrice%2CdateFrom%2CdateTo%7D%2Cdiscount%7Bvalue%2ColdPrice%2Cpercentage%2CdateTimeStart%2CdateTimeEnd%7D%7D%2Cflags%7BsystemName%2CsystemCode%2CfrontName%2CfrontIcon%2CfrontColor%2CfrontBackgroundColor%2CfrontClass%2CdisplayOnFront%7D%2Cemblems%7Bid%2Cname%2Cplace%2Czone%2Ccontent%2CrawContent%2Ctooltip%2CrawTooltip%2CcontentImages%2Curl%2Cdeeplink%2Cdiscount%2Cgtm%7Bname%2Cposition%7D%2CopenUrlInNewWindow%7D%2Cpostscripts%7Bid%2Ccontent%2CdisplayPriceStrategy%2Ctooltip%2Cplace%2Czone%2Curl%2Cgtm%7Bname%2Cposition%7D%7D%2Ccategories%7Bid%2Cname%2CmainCategory%2Croute%7D%2CsystemAttributes%7Bid%2Cname%2CoriginalName%2CisHintLong%2CvalidationType%2ChintLink%2Cvalues%7Bid%2Cname%2Chint%2Cimage%2Csettings%7Bsetting%2Cvalue%7D%7D%7D%2CkeyAttributes%7Bid%2Cname%2CoriginalName%2CisHintLong%2CvalidationType%2ChintLink%2Cvalues%7Bid%2Cname%2Chint%2Cimage%7D%7D%2CmenuItemAttributes%7Bid%2Cname%2CoriginalName%2CisHintLong%2CvalidationType%2ChintLink%2Cvalues%7Bid%2Cname%2Chint%2Cimage%7D%7D%2CattributeSettings%7BattributeId%2CattributeCode%2Cvalue%7D%2CopinionsAverage%7Baverage%2Ccount%7D%2ChotDeals%7BquantityPromoted%2CremainingQuantity%2CpromotionName%7D%2Cvariants%7Blabel%2CdisplayMethod%2ChideSingleValue%2CvariantProductOffer%7Blabel%2Coffer%7Bid%7D%7D%7D%2CunassignedAttributes%28originalName%3A%5B%22data_awizo%22%5D%29%7Bid%2Cname%2CoriginalName%2CisHintLong%2CvalidationType%2ChintLink%2Cvalues%7Bid%2Cname%2Chint%2Cimage%7D%7D%7D%7D'
                        
            query.replace(f'{product_ids[-1]}%22%2C', f'{product_ids[-1]}%22')

            return query   
        except Exception as e:
            print(e)

    def get_page_hits(self, raw_data):
        hits = 0
        next_page = False
        soup = BeautifulSoup(raw_data, 'lxml')
        tags = soup.select('div.c-offerBox.is-wide[data-zone="OFFERBOX"]') or soup.select('.list-items .offer-box')
        if tags:
            hits = len(tags)

        tag = soup.select_one('a.is-nextLink')
        if tag:
            next_page = True
        
        return hits, next_page

    def __has_data(self, raw_data):
        #If data count is equal to 1, it redirects directly to the pdp.
        soup = BeautifulSoup(raw_data, 'lxml')

        product_tags = soup.select('div.c-offerBox.is-wide[data-zone="OFFERBOX"]') or soup.select('.list-items .offer-box') #for redirected to category

        if len(product_tags) <= 1:
            return False


        return True