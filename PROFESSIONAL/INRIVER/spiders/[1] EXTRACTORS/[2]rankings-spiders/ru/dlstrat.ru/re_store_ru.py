import json
import re, requests
import cloudscraper
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from urllib.parse import quote_plus, urlencode, quote
from request.unblocker import UnblockerSessionRequests

class DnsShopRuDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.proxies = self.requester.proxies
        self.scraper = cloudscraper.create_scraper(sess=requester) 
        # Removing proxy to use basic requests session
        self.requester.proxies = {'http': '', 'https': '' } 
        self.requester.session.proxies = {'http': '', 'https': '' } 
        self.is_scraper = True

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
            print(headers)

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        hits_per_page = 18 # based from website
        for page in range(0, len(urls)):
            try:
                # Untrap keywords resulting to suggested products
                # if page == 0: 
                #     # Trap for keywords with explicitly saying no results found but displays different list of products
                #     if not self.check_if_has_valid_results(urls[page]):
                #         raise DownloadFailureException()
                # print(urls[page])

                if self.is_scraper:
                    res = self.scraper.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                else:
                    res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                
                if res.status_code not in [200, 201]:
                    print('RETRY USING BASIC REQUESTS')
                    res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                
                print(res.status_code)
                if res.status_code in [200, 201]:
                    if page == 0 and self.__has_redirected_to_pdp(res.text):
                        raise PaginationFailureException('Result not in rankings. Triggers auto rerun. . .')
                    else:
                        raw_data_list.append(res.text)
                
                    # for total number of products is not displayed in site
                    page_hits, has_next_page = self.get_page_hits(res.text) 

                    if page_hits < hits_per_page:
                        break
                    elif page_hits == hits_per_page:
                        if not has_next_page:
                            break
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "%20").replace("-", "%2d")

        payload = {
            'q': search_string,
            'p': '1',
            'order': 'popular',
            'stock': 'all',
        }

        # 18 per page
        # temp = f"https://www.dns-shop.ru/search/?q={}&p=1&order=popular&stock=all".urlencode(q_search_string, quote_via=quote)
        # temp = "https://www.dns-shop.ru/search/?q={}&p=1&order=popular&stock=all".format(params=urlencode(q_search_string))
        
        # urls = [
            # "https://www.dns-shop.ru/search/?q={}&p=1&order=popular&stock=all".format(urlencode(q_search_string)),
            # "https://www.dns-shop.ru/search/?q={}&p=2&order=popular&stock=all" % q_search_string,
            # "https://www.dns-shop.ru/search/?q={}&p=3&order=popular&stock=all" % q_search_string,
            # "https://www.dns-shop.ru/search/?q={}&p=4&order=popular&stock=all" % q_search_string,
            # "https://www.dns-shop.ru/search/?q={}&p=5&order=popular&stock=all" % q_search_string,
            # "https://www.dns-shop.ru/search/?q={}&p=6&order=popular&stock=all" % q_search_string,
        # ]

        urls = [
            # "https://www.dns-shop.ru/search/?{}".format(urlencode(payload, quote_via=quote))
           "https://www.dns-shop.ru/search/?q=%s&p=1&order=popular&stock=all" % q_search_string
        ]

        return urls

    def __has_redirected_to_pdp(self, raw_data):
        try:
            _json = json.loads(raw_data)
            soup = BeautifulSoup(_json['html'], "lxml")
        except:
            soup = BeautifulSoup(raw_data, "lxml")

        pdp_tag = soup.select_one('[itemtype="http://schema.org/Product"]')

        if pdp_tag:
            return True

        if not pdp_tag:
            pdp_tag = soup.select('.catalog-product__name.ui-link.ui-link_black') or soup.select('.product-info__title-link a')
            try:
                if len(pdp_tag) <= 1:
                    return True
            except:
                pass

        return False
    
    def get_page_hits(self, raw_data):
        hits = 0
        next_page = False
        try:
            _json = json.loads(raw_data)
            soup = BeautifulSoup(_json['html'], "lxml")
        except:
            soup = BeautifulSoup(raw_data, "lxml")
            
        tags = soup.select('.catalog-items-list .catalog-item') or \
            soup.select('.catalog-product.ui-button-widget')
            
        if tags:
            hits = len(tags)

        tag = soup.select_one('.pagination-widget__page-link_next.pagination-widget__page-link_disabled')
        if not tag:
            next_page = True
        
        return hits, next_page
    
    def get_headers(self):
        url = 'https://www.dns-shop.ru'
        headers = {
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36'
        }
        try:
            # print('1 Requesting headers using cloudscraper')
            # res = self.scraper.get(url)
            
            # if not 'csrf-token' in res.text:
            self.is_scraper = False
            print('2 Requesting headers without proxy')
            res = self.requester.get(url, timeout=100, headers=headers)

            if not 'csrf-token' in res.text:
                self.is_scraper = False
                # If no proxy does not work retry using with proxy
                self.requester.proxies = {'http': self.proxies, 'https': self.proxies } 
                self.requester.session.proxies = {'http': self.proxies, 'https': self.proxies }
                print('1 Requesting headers with proxy')
                res = self.requester.get(url, timeout=100, headers=headers, proxies=self.proxies)

            if res.status_code in [200, 201]:
                print('Done ', url )
                soup = BeautifulSoup(res.text, 'lxml')
                tag = soup.find('meta', {'name': 'csrf-token'})

                if tag and tag.has_attr('content'):
                    headers['x-csrf-token'] = tag.get('content')
                    headers['x-requested-with'] = 'XMLHttpRequest'
                    print('Fetched x-csrf-token')
                    return headers

        except Exception as e:
            print(str(e))
        
        return headers
        #     else:
        #         raise DownloadFailureException() 
        # else:
        #     raise DownloadFailureException()
    
    def check_if_has_valid_results(self, url):
        headers = {
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36'
        }
        res = self.requester.get(url, timeout=100, headers=headers)

        if res.status_code in [200, 201]:
            print('Done ', url )
            if not 'low-relevancy' in res.text:
                print('Products Displayed are valid')
                return True

        print('Products Displayed are invalid')
        return False
