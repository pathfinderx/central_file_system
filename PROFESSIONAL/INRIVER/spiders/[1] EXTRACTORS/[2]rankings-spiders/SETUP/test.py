from pprint import pprint
import json
import urllib3
from dotenv import load_dotenv
from request.luminati import LuminatiSessionRequests
from request.crawlera import CrawleraSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
if __name__ == '__main__':
    try: 
        website = 'www.playerone.be'
        country = 'be'
        keyword = 'nba 2k22'
        limit = 25

        factory = StrategyFactory()
        requester = LuminatiSessionRequests(country)
        country = 'uk' if country == 'gb' else country
        dl_strategy = factory.get_download_strategy(website, country)(requester)
        ws_strategy = factory.get_website_strategy(website, country)(dl_strategy)

        # Get raw data
        raw_data_list = dl_strategy.download(keyword)
        # Get Proxy IP info
        pprint(dl_strategy.requester.proxy_info)
        # Get extracted data
        start_rank = 1
        final_list = [] # SC code

        for raw_data in raw_data_list:
            res, last_rank = ws_strategy.execute(raw_data, start_rank, limit)
            final_list.append(res) # SC code

            ###############################################
            # PATCH FOR INCORRECT NUMBER OF PRODUCT SCRAPED
            ###############################################
            if final_list[-1]:
                if int(final_list[-1][-1]['rank']) >= limit:
                    break
            else:
                count = 0
                for data_list in final_list:
                    if data_list:
                        count+= len(data_list)
                if count >= limit:
                    break
            ###############################################
            # PATCH FOR INCORRECT NUMBER OF PRODUCT SCRAPED
            ###############################################
            start_rank = last_rank
            if final_list[-1]:
                print(final_list[-1][-1]['rank'])
            else:
                count = 0
                for data_list in final_list:
                    if data_list:
                        count+= len(data_list)
                print(count)
        print('Total products ', final_list[-1][-1]['rank'])
        with open('../extract-transform-rankings/et_test.json', 'w', encoding='utf-8') as f:
            json.dump(final_list, f, ensure_ascii=False, indent=2)
    except Exception as err:
        print(err)
    '''
    extract_data_list = [[{'rank':99}], []]
    '''