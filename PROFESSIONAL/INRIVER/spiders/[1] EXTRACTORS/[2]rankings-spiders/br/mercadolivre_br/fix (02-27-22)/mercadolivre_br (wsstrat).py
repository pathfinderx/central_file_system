import logging, re
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MercadolivreComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mercadolivre.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.select('ol.ui-search-layout li.ui-search-layout__item')
        if tags:
            return tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title_tag = tag.select_one('h2.ui-search-item__title')
            if title_tag:
                source, value = str(title_tag), title_tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No brand for this website
        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_tag = tag.select_one('a.ui-search-item__group__element') or tag.select_one('a.ui-search-result__content.ui-search-link')
            if url_tag and url_tag.has_attr('href') and url_tag.has_attr('title'):
                isObfuscatedURL = re.search('.click', url_tag.get('href'))
                if isObfuscatedURL:
                    item_id = tag.select_one('.ui-search-result__bookmark').select_one('input[name="itemId"]')
                    if item_id and item_id.has_attr('value'):
                        mlb = re.split('(\d+)', item_id.get('value'))
                        title = url_tag.get('title').lower().replace(' - ', '-').replace(' ', '-')
                        params = f'https://produto.mercadolivre.com.br/{mlb[0]}-{mlb[1]}-{title}-_JM'
                        source, value = str(url_tag), params
                else:
                    value = url_tag.get('href')
                    source = str(url_tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
