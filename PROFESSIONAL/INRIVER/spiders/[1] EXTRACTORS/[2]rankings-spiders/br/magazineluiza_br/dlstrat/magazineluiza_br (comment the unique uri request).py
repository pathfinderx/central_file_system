from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class MagazineluizaComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'same-origin',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                    raw_data_list.append(res.text)
                    if not self.__has_next_page(res.text):
                        break                    

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        urls = []
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.magazineluiza.com.br/busca/{q_search_string}/',
            # f'https://www.magazineluiza.com.br/busca/{q_search_string}/?page=2'
        ]
        # temporary fix: unique request for specific keyword
        # if search_string.lower() in ['mini video game']:
        #     urls = [
        #         f'https://www.magazineluiza.com.br/_next/data/oBEL943StqHAHTW8Jpw0u/busca/{q_search_string}.json?slug=busca&slug={q_search_string}'
        #     ]
        
        return urls

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tags = soup.select('li.product a.product-li') or soup.select('li.sc-gpAxqO.cEqyhT a')

        if len(tags) >= 18:
            return True

        return False