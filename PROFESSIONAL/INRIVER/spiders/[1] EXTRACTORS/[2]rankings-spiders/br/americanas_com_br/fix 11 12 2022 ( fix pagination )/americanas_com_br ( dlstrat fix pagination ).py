import json, re

from urllib.parse import urlencode
import cloudscraper
from strategies.download.base import DownloadStrategy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests


class AmericanasComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
        self.unblocker = UnblockerSessionRequests('br')
        # self._headers = {
        #     "accept": "*/*",
        #     "accept-encoding": "gzip, deflate",
        #     "accept-language": "en-US,en;q=0.9",
        #     "apollographql-client-name": "catalogo-v3",
        #     "content-type": "application/json",
        #     "device": "desktop",
        #     "opn": "WZRBJFFW",
        #     "origin": "https://www.americanas.com.br",
        #     "referer": "https://www.americanas.com.br/",
        #     "sec-ch-ua": "\"Google Chrome\";v=\"89\", \"Chromium\";v=\"89\", \";Not A Brand\";v=\"99\"",
        #     "sessionid": "653.242667720669820212911132952",
        #     "test-ab": "searchTestAB=old",
        #     "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
        #     "userid": "va_2021291113243_52.85020148923836"
        # }
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding':'gzip, deflate',
                'accept-language':'en-US,en;q=0.9',
                'cache-control':'no-cache',
                'pragma':'no-cache',
                'referer': 'https://www.americanas.com.br/',
                'sec-ch-ua':'".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
                'sec-ch-ua-mobile':'?0',
                'sec-ch-ua-platform':'"Windows"',
                'sec-fetch-dest':'document',
                'sec-fetch-mode':'navigate',
                'sec-fetch-site':'same-origin',
                'sec-fetch-user':'1',
                'upgrade-insecure-requests':'1',
                'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code
                print('Status Code from Normal Request ---', res.status_code)
                print('Url ---, ',urls[page])
                if status_code in [200, 201]:
                    # since data is from api, source should be the api url
                    # try:
                    #     json_data = json.loads(res.text)
                    #     if json_data['products']:
                    #         temp_res = (res.text, urls[page])
                    #         raw_data_list.append(temp_res)
                    # except Exception:
                    #     temp_res = (res.text, urls[page])
                    #     raw_data_list.append(temp_res)
                    
                    # source has updated from API to view-source
                    raw_data_list.append(res.text)
                    
                else:
                    res = self.scraper.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                    status_code = res.status_code
                    print('Status Code from Cloud Scraper Request ---', res.status_code)
                    print('Url ---, ',urls[page])
                    if status_code in [200, 201]:
                        raw_data_list.append(res.text)
                        
                    else:
                        res = self.unblocker.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                        status_code = res.status_code
                        print('Status Code from Unblocker Request ---', res.status_code)
                        print('Url ---, ',urls[page])
                        if status_code in [200, 201]:
                            raw_data_list.append(res.text)
                        
                        raise PaginationFailureException()

            except Exception:

                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list


    # def get_urls(self, search_string):
    #     urls = []

    #     params = {
    #         'operationName': 'pageSearch',
    #         'device': 'desktop',
    #         'oneDayDelivery': 'undefined',
    #         'opn': 'WZRBJFFW'
    #     }

    #     variables = {
    #         "path": "/busca/console?rc={}&limit=24&offset=24".format(search_string),
    #         "content": search_string,
    #         "offset": 24,
    #         "limit": 24,
    #         "segmented": False,
    #         "filters": [],
    #         "oneDayDelivery": False
    #     }

    #     extensions = {
    #         "persistedQuery": {
    #             "version": 1,
    #             "sha256Hash": "cb31d8502a2c4e30a5000896074507e400eee2a8183365f7dd0a587919d3210d" # TODO: Update this variable when spider fails
    #             #"72ee090499158fe6704b8d2fbf893486473f6af6f3848514b7767fa2c34a58ec" 
    #             # 68c764fad4cc03250239ebd6bac4585f04536c79cb4ef6ac4cf2d6863fe763f4
    #             # 512eaa1434a117cb7fc262e43b8917efdeacacd5c40eaa9ba0e7f73d569e7fa0
    #             # ca53058832f76dbbaa5d65a0970b2189c432ab07e755ab723a4a4738ff2fadaa
    #             # "3d3c6f8387125815f8888f7217f05eb0a2209d2e8bff076bb08667b4ab8af2cf" 
    #             #464b5b75599ba0b7a407360a64a622a91af8f090eadabe6a7e598ba545330eae
    #             #31bf18f5b4e7eb3beae84fd32defc2daaf15f19f8da3bb6027bc3bcc6f6ad344
    #         }
    #     }
    #     params['extensions'] = json.dumps(extensions, ensure_ascii=False)


        
    #     for i in range(0, 2):
    #         variables['offset'] = i * 24
    #         params['variables'] = json.dumps(variables, ensure_ascii=False)
    #         url = 'https://catalogo-bff-v2-americanas.b2w.io/graphql?{}'.format(urlencode(params))
    #         urls.append(url)

    #     return urls

    # source has updated from API to view-source
    def get_urls(self, search_string):
        q_search_string = search_string.replace(' ', '-').lower()
        urls = [
            f'https://www.americanas.com.br/busca/{q_search_string}?limit=25&offset=0',
            f'https://www.americanas.com.br/busca/{q_search_string}?limit=25&offset=25'
        ]

        if search_string.lower() in ['aparelho de video game']:
            dashed_search_string = search_string.replace(' ', '-').lower()
            q_search_string = search_string.replace(' ', '+')
            
            urls = [
                f'https://www.americanas.com.br/busca/{dashed_search_string }?rc={q_search_string}'
            ]

        return urls