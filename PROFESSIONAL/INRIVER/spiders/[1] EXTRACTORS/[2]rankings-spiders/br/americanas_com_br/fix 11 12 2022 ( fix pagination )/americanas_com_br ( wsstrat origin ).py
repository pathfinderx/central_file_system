import logging
import json
from copy import deepcopy
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class AmericanasComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.americanas.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags, urls = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx], urls)

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], urls)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx], urls)

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        # (json_data, urls) = raw_data
        # raw_tags = json.loads(json_data)
        # tag = []
        # tags = raw_tags['data']['search']['products']
        # if tags:
        #     for data in tags:
        #         if 'product' in data.keys() and data['product'] != None:
        #             tag.append(data)
        # return tag, urls

        # source has updated from API to view-source
        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.select('.col__StyledCol-sc-1snw5v3-0.jGlQWu.src__ColGridItem-sc-122lblh-1.cJnBan')

        if tags:
            return tags, None
        else:
            return []

    def get_title(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # source has updated from API to view-source
            # source = str(url)
            # value =  str(tag['product']['name'])

            # source has updated from API to view-source
            _tag = tag.select_one('.product-name__Name-sc-1shovj0-0.gUjFDF')
            if _tag:
                source, value = str(_tag), _tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # brand is integrated into title

        return source, value

    def get_url(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        base_url = 'https://www.americanas.com.br'

        try:
            # source = str(url)
            # temp = tag['product']['id']
            # value =  f'{base_url}{temp}'

            # source has updated from API to view-source
            _tag = tag.select_one('[aria-current="page"]')
            if _tag and _tag.has_attr('href'):
                _value = _tag.get('href')
                source, value = str(tag), f'{base_url}{_value}'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
