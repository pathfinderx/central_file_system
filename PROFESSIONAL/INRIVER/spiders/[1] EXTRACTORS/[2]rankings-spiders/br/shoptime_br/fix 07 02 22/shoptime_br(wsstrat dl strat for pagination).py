from urllib.parse import quote_plus

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class ShoptimeComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'same-origin',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60'
            }

        if search_string == 'Console':
            headers['Set-Cookie'] = 'bm_sv=30DFBAA7BB4FECB0839AAF95B470F46E~WrJN/qotACOlLpG83dAFMJpve0FWU8NUOfEM4x8ehZhe3BJR36mmkEJIMNQB/jn1/z/DjVwO42cpgUubTh9DrrkJ/c17uCFeig1XdnYfwGOlOgc9lPOr3GWNcLctwFJHXyAAxVmVexwFLKziiZ0t9gdZ5ADubX65cDxK1NMRJSw=; Domain=.shoptime.com.br; Path=/; Max-Age=6921; HttpOnly'
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                print(urls[page])
                status_code = res.status_code

                if status_code in [200, 201]:
                    raw_data_list.append(res.text)
                    if self.hasNextPage(res.text):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        
        urls = [
           'https://www.shoptime.com.br/busca/{}?limite=24&offset=0&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string)),
           'https://www.shoptime.com.br/busca/{}?limite=24&offset=24&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string)),
        #    'https://www.shoptime.com.br/busca/{}?limite=24&offset=48&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string)),
        #    'https://www.shoptime.com.br/busca/{}?limite=24&offset=72&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string)),
        #    'https://www.shoptime.com.br/busca/{}?limite=24&offset=96&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string)),
        #    'https://www.shoptime.com.br/busca/{}?limite=24&offset=120&rc={}'.format(search_string.replace(' ','-').lower(),quote_plus(search_string))

        ]

        return urls

    def hasNextPage(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tags = soup.select('div.product-grid-item') or soup.select('.col__StyledCol-sc-1snw5v3-0.epVkvq.src__ColGridItem-sc-1yqw8tm-0.eniJHe') or \
            soup.select('[class="col__StyledCol-sc-1snw5v3-0 jGlQWu src__ColGridItem-sc-1yqw8tm-1 dzzXSP"]')

        return True if len(tags) >= 25 else False