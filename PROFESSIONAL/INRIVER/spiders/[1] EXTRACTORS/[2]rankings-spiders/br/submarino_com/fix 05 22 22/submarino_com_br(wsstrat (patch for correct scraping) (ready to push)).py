import logging
import json, re
import sys
from copy import deepcopy

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup


class SubmarinoComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.submarino.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags, urls = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx], urls)

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], urls)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx], urls)

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        try:
            (json_data, urls) = raw_data
            raw_tags = json.loads(json_data)
            tags = raw_tags['products']

            return tags, urls
        except Exception as e:
            print('Error in: wsstrat -> get_product_tags() -> failure to parse json -> with message: ', e)

            product_list = self.get_raw_products(raw_data)
            return product_list, urls if product_list else None

    def get_raw_products(self, raw_data):
        # The process of breaking the chunk (https://prnt.sc/VnNJcOa7RbiM) in order to identify the 'itemListElement'
        # rgx1 = re.search(r'(title id=\"searchIcon\")([\s\S]+}}</script>)', raw_data[0])
        # rgx2 = re.search(r'(src-input-wpr{position)([\s\S]+}}<\/script>)', rgx1.group(0))
        # rgx3 = re.search(r'(src-btn{background:0 0;width:3em})([\s\S]+}})', rgx2.group(0))
        # rgx4 = re.search(r'(application/ld\+json\">)([\s\S]+}})', rgx3.group(0))
        # rgx5 = re.search(r'(itemListElement\":)([\s\S]+@type)', rgx4.group(2))
        # raw_products = rgx5.group(2).replace('}],"@type', "")
        try:
            soup = BeautifulSoup(raw_data[0], 'lxml')
            script_tag, root_query = soup.select('script'), None
            if script_tag:
                for x in script_tag: 
                    if re.search(r'__APOLLO_STATE__', str(x)):
                        root_query = x.next_element.replace('window.__APOLLO_STATE__ = ', '')
                if root_query:
                    _json, unfiltered_products, product_list = json.loads(root_query), list(), list()
                    if _json:
                        for i,v in enumerate(_json.get('ROOT_QUERY')):
                            if (re.search(r'content', v) and 'products' in _json['ROOT_QUERY'][v]):
                                for index, value in enumerate(_json['ROOT_QUERY'][v]['products']):
                                    unfiltered_products.append(value)
                    filtered_products = [products for products in unfiltered_products if products.get('product')] if unfiltered_products else None # exclude this: (https://prnt.sc/6jBoMyIev1Mv)
                    if filtered_products:
                        for products in filtered_products:
                            product_list.append(products.get('product'))

                    return product_list if product_list else None

            elif raw_data and raw_data[0]:
                raw_products, _json = re.search(r'(itemListElement\":)([\s\S]+@type)', raw_data[0]), None
                _json = json.loads(raw_products.group(2).replace('}],"@type', "")) if raw_products else None
                if _json:
                    product_list = [products for products in _json if bool(products.get('name'))] # filter this (https://prnt.sc/xC9xoD5hVQMo)
                    return product_list if product_list else None
        except Exception as e:
            print('Error in: wsstrat -> get_raw_products() -> with message: ', e)
            return None

    def get_title(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'name' in tag:
                source, value = str(url), str(tag['name']).strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # brand is not structured
        # brand is usually integrated into title
        return source, value

    def get_url(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        base_url = 'https://www.submarino.com.br/produto/'

        try:
            if 'id' in tag:
                source, value = str(url), base_url + str(tag['id'])
            elif 'image' in tag:
                product_id = re.search(r'([\d]{3,})(_)', tag.get('image'))
                if product_id:
                    source, value = str(url), f'{base_url}{product_id.group(1)}'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
