import logging
import json
import sys
from copy import deepcopy

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class SubmarinoComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.submarino.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags, urls = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx], urls)

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], urls)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx], urls)

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        (json_data, urls) = raw_data
        raw_tags = json.loads(json_data)
        tags = raw_tags['products']

        return tags, urls

    def get_title(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(url)
            value =  str(tag['name'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # brand is not structured
        # brand is usually integrated into title
        return source, value

    def get_url(self, tag, url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        base_url = 'https://www.submarino.com.br/produto/'

        try:
            source = str(url)
            value =  base_url + str(tag['id'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
