from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

import json

class SubmarinoComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self._headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-Agent': "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Mobile Safari/537.36",
        }
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self._headers
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                    # since data is from api, source should be the api url                    
                    try:
                        json_data = json.loads(res.text)
                        if json_data['products']:
                            temp_res = (res.text, urls[page])
                            raw_data_list.append(temp_res)
                    except Exception:
                        temp_res = (res.text, urls[page])
                        raw_data_list.append(temp_res)
                else:
                    raise PaginationFailureException()

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '%20')
        # each page has 24 items
        # we can readily get all pages by adding 24 to the offset
        # so for products that are less than a hundred,
        # pages with higher value offset simply do not exist
        # so we need to get the valid pages first

        offsets = [i*24 for i in range(0,5)]
        urls = []

        for offset in offsets:
            url = f'https://mystique-v2-submarino.juno.b2w.io/search?content={q_search_string}&offset={offset}&sortBy=relevance&source=nanook&limit=24&suggestion=true&c_b2wSid=658.6471704101248202022222284&c_b2wUid=va_202022222225_51.19363854686865'

            urls.append(url)

        # print(urls)

        return urls
