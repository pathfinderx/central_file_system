from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

import json

class SubmarinoComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self._headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-Agent': "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Mobile Safari/537.36",
        }
        self.isHeadersModified = False
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self._headers
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = None
                if self.isHeadersModified:
                    res = self.requester.get(urls[page], timeout=timeout, headers=self._headers, cookies=cookies)
                else:
                    res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                    # since data is from api, source should be the api url                    
                    try:
                        json_data = json.loads(res.text)
                        if json_data['products']:
                            temp_res = (res.text, urls[page])
                            raw_data_list.append(temp_res)
                    except Exception:
                        temp_res = (res.text, urls[page])
                        raw_data_list.append(temp_res)
                else:
                    raise PaginationFailureException()

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    def get_urls(self, search_string):
        q_search_string, urls = search_string.replace(' ', '%20'), list()
        # each page has 24 items
        # we can readily get all pages by adding 24 to the offset
        # so for products that are less than a hundred,
        # pages with higher value offset simply do not exist
        # so we need to get the valid pages first

        # temporary bypass for keyword 'aparelho de video game' (invalid product count and wrong scraping of data (api url not found on network but active))
        if search_string in ['aparelho de video game']:
            modified_search_string = search_string.replace(' ', '-')
            url = f'https://www.submarino.com.br/busca/{modified_search_string}'
            urls.append(url)

            self._headers = self.get_headers()
            self.isHeadersModified = True

            return urls
        else:
            offsets = [i*24 for i in range(0,5)]

            for offset in offsets:
                url = f'https://mystique-v2-submarino.juno.b2w.io/search?content={q_search_string}&offset={offset}&sortBy=relevance&source=nanook&limit=24&suggestion=true&c_b2wSid=658.6471704101248202022222284&c_b2wUid=va_202022222225_51.19363854686865'

                urls.append(url)

            return urls

    def get_headers(self):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'cookie': 'B2W-SID=1653101729083.0.632186166434966; B2W-PID=1653101729083.0.730146659333087; B2W-UID=va_1653101729083.0.973154292494155; MobileOptOut=1; b2wDevice=eyJvcyI6IldpbmRvd3MgTlQiLCJvc1ZlcnNpb24iOiIxMC4wIiwidmVuZG9yIjoiQ2hyb21lIiwidHlwZSI6ImRlc2t0b3AiLCJta3ROYW1lIjoiQ2hyb21lIDEwMSIsIm1vZGVsIjoiMTAxIiwibW9iaWxlT3B0T3V0IjoiZmFsc2UifQ==; b2wDeviceType=desktop; b2wChannel=INTERNET; B2W-IU=false; _gcl_au=1.1.274650964.1653101730; _px_uAB=OTg2Njl8dHJ1ZQ==; pxcts=799ff1d2-d8b1-11ec-bcae-725275657551; _pxvid=799fe250-d8b1-11ec-bcae-725275657551; __gads=ID=c504f69b8e8cde36:T=1653101730:S=ALNI_MZFECGyl9pShEg6tZ2fg-aXmOnqvA; _px_f394gi7Fvmc43dfg_user_id=NzllNDc4YjEtZDhiMS0xMWVjLWFhZDctYzE4YTUyOTRjZTBm; searchTestAB=new; catalogTestAB=out; ak_bmsc=67CEACB29C89A96D00ADBB8C983AD95E~000000000000000000000000000000~YAAQKz6xy+hrNt2AAQAAmcgA6g+EnpUgg7WTr5w1Q+JklKBh/RNTlYjG30JXgulNTqthIljKVjahJngRYDipxgcHsjxgjiXJ2T05RVCWNJPSYw1PHJ/Whrc/dzXjHgAAwIE2TFgJpF97adLcDVBl2VQ55r3XQxmlDPOngUNVKbOsEQh91WC5/97R4FcmxurWX3OfhJ7An7qDC9V7chWzKdW2g3zZ9IDnPLelIhtEQRINLa3uxDm9GjMMxnGbUChFPaJR1XlyCTQkncTUa6+wUaF0EGt7Le8FuK83JlZ9KC8DeGznxCUbcnLXkK7GcP0rNFy9mOS7tW40qd+Vyh6Ft5eGAdvj1y7g/sFh2WVbrGRp6oVUnCb6Ogxkr4erAkYWeJuNXxFZuN/E2/0ejJ3whxXNtxNtnQYXYKcI1VwkdVQ4pIMtMSsHR7A/zH+EOKpBkbLiUN+/eft7SBl26vRzAovbDnHH5gYGGbT9M8U/xn7t8W700GgqorNib2TRjbWYTbY=; _gid=GA1.3.336402027.1653193362; __gpi=UID=0000059cad46517c:T=1653101730:RT=1653193362:S=ALNI_MZ7xzwGdF7jQ9Rs04aWKwf_o1q5DQ; feather.rank=%7B%22search%22%3A%22aparelho%20de%20video%20game%E2%86%872%E2%86%88console%E2%86%872%E2%86%88condole%20%E2%86%871%22%7D; bm_sv=0DB0B7E15C377D82F37D3906D89FE1D1~YAAQMj6xy07tLd6AAQAA4jJk6g9ViZmHRYdy9b5bf0Mo/kTbGIU5Fze0/kN5YgiFw9fYfimaxZ+UXhZftzxRNUPmBe5e9ag4ntC0zdTyhDUuxQZj8XmpUgatWAOUdwXAarUMSuVAYqkQbxfltVftpt0uhiOFfii/T4PLlYPeUv7W8OaM7dqkAVoNNlK0NwymRECaFtG04MZj5OEBB7X2xWI/1gepGmpjyyfmINc06hyf0ld5Rxg/j6JKu39FRhrf2DnWOdqZ~1; _ga_PSKS7W3QCJ=GS1.1.1653199879.4.0.1653199879.60; _ga=GA1.3.1264340793.1653101730; cto_bundle=cC_O519CVWgzTUtIMlhHcTJ0RjdvU214MWFWZXowVmVGcWM0d3UlMkZLR1JHS05HT2p5Nk5sV0MlMkJmdWlWVXZBWUZvTFEyakY3N0FOMUUwVU1jTm9LWGh6Q2hhZTIyRkRXNnExTDgzMzlTZm5Kd2dsY2NDbEdwNFc0dHpyTHN0bHd0Tkt6Q3IyNWgxbk9kYkJPbVFYSUJkZmFiNHNqeERsR2dpZWZxak83bERzWENoSGJnJTNE; _px3=1ee811419b4d82be593abe59a292b47e99578a3b59615bb3f2b0662fe9d74b49:qxbDS1BE1ile2Ay/EEwVu8aAE5H0/dTeyzyqf90BRp6cOK8L6IGdUy+JWNHbe0Ngj0xG5Kj98buC7rDpgXP2MA==:1000:5f2hVwf9Pze+27v8bC8Vf68Pu+ACZDHMBk6zRnT7+greny2TxI2YzM887OeMPpJn/ZSNjwmzhJF3UbPF+ndVx+0JOVxmyTzDOCjs8QfmrbMxDobf9AWtATis3NPEA9FGb8iOVAdZGmfEMWpmXzcJ+D89qi2txgadOS+C7KSD7FZ/4wGV7g+k7dUimUXuMhnNMpr4nDTeniuRXbiz7iBSbA==',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="101", "Google Chrome";v="101"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36'
        }

        return headers