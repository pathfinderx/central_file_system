import json
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
	URLPreparationFailureException


class AbensonComDownloadStrategy(DownloadStrategy):
	def __init__(self, requester):
		self.requester = requester
		self._search_string = None

	def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(search_string, str)
		assert isinstance(timeout, int)
		self._search_string = search_string
		raw_data_list = []

		if not isinstance(headers, dict):
			headers = {
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
							  'Chrome/57.0.0.12335 Safari/537.36',
			}
		if not isinstance(cookies, dict):
			cookies = None

		try:
			urls = self.get_urls(search_string)
		except:
			raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
		
		hits_per_page = 24
		for page in range(0, len(urls)):
			try:
				res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

				if res.status_code in [200, 201,202]:
					raw_data_list.append(res.text)
					# for total number of products is not displayed in site
					page_hits, has_next_page = self.get_page_hits(res.text) 
					if page_hits < hits_per_page:
						break
					elif page_hits == hits_per_page:
						if not has_next_page:
							break
				else:
					raise PaginationFailureException()

			except PaginationFailureException:
				raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

			except:
				raise DownloadFailureException('Download failed - Unhandled Exception')

			return raw_data_list

	def get_urls(self, search_string):
		urls = []
		q_search_string = search_string.replace(" ", "+").lower()

		# 24 per page
		urls = [
			f"https://www.abenson.com/search/{q_search_string}",
			f"https://www.abenson.com/search/{q_search_string}/page/2"
		]

		return urls
	
	def get_page_hits(self, raw_data):
		hits = 0
		next_page = False
		soup = BeautifulSoup(raw_data, 'lxml')
		tag = soup.find('script', type='application/ld+json', text=re.compile(r'.*itemList.*'))
		if tag:
			_json = json.loads(tag.get_text().strip())
			hits = len(_json['itemListElement'])
		
		tag = soup.find('link', { "data-react-helmet":"true", "rel":"next" })
		if tag:
			next_page = True
		
		return hits, next_page

	def api_data(self):
		q_search_string = self._search_string.replace(" ", "%20")
		url = f'https://www.liverpool.com.mx/getSearchFacadeService?s={q_search_string}'
		raw_data = None
		headers = {
			'accept': 'application/json, text/plain, */*',
			'accept-encoding': 'gzip, deflate',
			'accept-language': 'en-US,en;q=0.9',
			# authorization: [object Object]
			'cache-control': 'no-cache',
			# cookie: genero=x; segment=fuero; _evga_fb08={%22uuid%22:%22d8c72ab6cdf15e92%22}; _gcl_au=1.1.300260749.1633511137; gbi_visitorId=ckufaculr00013883q4ka2py9; _scid=57d53648-b55a-41b3-8cb5-2fcb09cfc1c5; _fbp=fb.2.1633511143089.234125295; _sctr=1|1633449600000; TURNTO_TEASER_SHOWN=1633511148910; AKA_A2=A; PIM-SESSION-ID=Zul6HoUEzRropM0f; rxVisitor=1633760495071UGGJO12ULE9J33TN1EEHFST4KRF6CIG1; session_dc_qv=1; _gid=GA1.3.1852427739.1633760497; _pin_unauth=dWlkPU1XSXpaamN3TVRFdE5UZzFOUzAwTldGaExUZzVaakF0T0RGaU5UQTJZV05tTURKaw; JSESSIONID=MPxjtteFwtIqBR3b48D5Z8v07z1Jy1opyW84PN7cC0DVvYzhU25T!1443695721; ActiveDataCenterCookie=SiteC; nearByStore=; homeDeliveryStore=; dtCookie=v_4_srv_11_sn_558RTSFNK896D9JI5935PO6D069I0MQH_perc_100000_ol_0_mul_1_app-3Afb4b113cea6706c5_0; DCID_www=ogngcpe; gbi_sessionId=ckujetmqr0000387hdwsj2ooo; dtSa=-; dtLatC=393; RT="z=1&dm=www.liverpool.com.mx&si=f1203306-9de2-4774-b4d9-3d2a35762b7b&ss=kujetbsh&sl=2&tt=9m0&obo=1&rl=1"; mf_4b65f806-7a98-46e9-bed7-78c70a09f4dd=|.2270909444.1633760597323|1633760495318||0|||0|0|89.82841; _ga_171XPPQ282=GS1.1.1633760500.2.1.1633760600.44; _ga=GA1.3.1011684264.1633511134; _dc_gtm_UA-4668284-1=1; _gat_UA-4668284-55=1; _uetsid=2b6a0e3028c911ec8d2f0bc87734dbc4; _uetvid=34eb9440267f11ec895b05b47608e666; dtPC=11$560594826_519h-vWMHPUATHFWBARHCPBLPFAKOSSCGQEPKQ-0e0; rxvt=1633762411282|1633760495072; akavpau_allow=1633760671~id=b4c5015e1efa27eb98719296deea2543
			'pragma': 'no-cache',
			'referer': 'https://www.liverpool.com.mx/tienda/home',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
							  'Chrome/57.0.0.12335 Safari/537.36',
		}
		try:
			res = self.requester.get(url, timeout=10, headers=None, cookies=None)
			raw_data = res.text

			return raw_data
		except:
			return raw_data