from pprint import pprint
import json
import urllib3
from dotenv import load_dotenv
from request.luminati import LuminatiSessionRequests
from request.crawlera import CrawleraSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
if __name__ == '__main__':
    try: 
        website = 'www.abenson.com'
        country = 'mx'
        keyword = 'smart phone'
        # postal_code = "28850"
        # keyword = 'apple macbook pro 16" 2019 200gb'
        limit = 25
        factory = StrategyFactory()
        requester = LuminatiSessionRequests(country)  # Init requester
        # requester = CrawleraSessionRequests()  # Init requester
        country = 'uk' if country == 'gb' else country
        dl_strategy = factory.get_download_strategy(website, country)(requester)  # Init download strategy
        ws_strategy = factory.get_website_strategy(website, country)(dl_strategy)  # Init website strategy
        # Get raw data
        # raw_data_list = dl_strategy.download(keyword, postal_code=postal_code)
        raw_data_list = dl_strategy.download(keyword)
        # Get Proxy IP info
        pprint(dl_strategy.requester.proxy_info)
        # Get extracted data
        start_rank = 1
        final_list = [] # SC code
        for raw_data in raw_data_list:
            res, last_rank = ws_strategy.execute(raw_data, start_rank, limit)
            final_list.append(res) # SC code
            # pprint(res)
            # if last_rank >= limit:
            #     break
            # else:
            ###############################################
            # PATCH FOR INCORRECT NUMBER OF PRODUCT SCRAPED
            ###############################################
            if final_list[-1]:
                if int(final_list[-1][-1]['rank']) >= limit:
                    break
            else:
                count = 0
                for data_list in final_list:
                    if data_list:
                        count+= len(data_list)
                if count >= limit:
                    break
            ###############################################
            # PATCH FOR INCORRECT NUMBER OF PRODUCT SCRAPED
            ###############################################
            start_rank = last_rank
            # url_list = {final['rank'] : final['url']['value'] for final in final_list[0]}
            if final_list[-1]:
                print(final_list[-1][-1]['rank'])
            else:
                count = 0
                for data_list in final_list:
                    if data_list:
                        count+= len(data_list)
                print(count)
        print('Total products ', final_list[-1][-1]['rank'])
        with open('../extract-transform-rankings/et_test.json', 'w', encoding='utf-8') as f:
            json.dump(final_list, f, ensure_ascii=False, indent=2)
        # pprint(final_list) # SC code
    except Exception as err:
        print(err)
    # with open('sample.json', encoding="utf-8") as sample_json:
    #     _json = json.load(sample_json)
    #     process_job(json.dumps(_json), env_vars) 
    # https://www.amazon.co.uk/s?k=FRONTLINE+spot+on+flea&ref=nb_sb_noss
    '''
    extract_data_list = [[{'rank':99}], []]
    '''
    # print('Total products ', final_list[-1][-1]['rank'])
    # with open('../extract-transform-rankings/sample_batch.json', 'w', encoding='utf-8') as f:
    #     json.dump(final_list, f, ensure_ascii=False, indent=2)
    # # pprint(final_list) # SC code
    # except Exception as err:
    #     print(err)
    # for page in final_list:
    #     for data in page:
    #         print(f"{data['rank']}| {data['title']['value']}: {data['url']['value']}")