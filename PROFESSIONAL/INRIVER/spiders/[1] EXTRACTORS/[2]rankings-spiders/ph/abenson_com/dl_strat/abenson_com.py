import json
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
	URLPreparationFailureException


class AbensonComDownloadStrategy(DownloadStrategy):
	def __init__(self, requester):
		self.requester = requester
		self._search_string = None

	def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(search_string, str)
		assert isinstance(timeout, int)
		self._search_string = search_string
		raw_data_list = []

		if not isinstance(headers, dict):
			headers = {
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
							  'Chrome/57.0.0.12335 Safari/537.36',
			}
		if not isinstance(cookies, dict):
			cookies = None

		try:
			urls = self.get_urls(search_string)
		except:
			raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
		
		for i in range(0, len(urls)):	
			try:
				res = self.requester.get(urls[i], timeout=timeout, headers=headers, cookies=cookies)

				if res.status_code in [200, 201, 202]:
					raw_data_list.append(res.text)
					if not self.should_next_page(res.text):
						break

			except PaginationFailureException:
				raise PaginationFailureException(f'Pagination Failed - Page {i+1}')

			except:
				raise DownloadFailureException('Download failed - Unhandled Exception')

		return raw_data_list

	def get_urls(self, search_string):
		urls = []
		q_search_string = search_string.replace(" ", "+").lower()

		urls = [
			f"https://www.abenson.com/search/{q_search_string}",
			f"https://www.abenson.com/search/{q_search_string}/page/2"
		]

		return urls

	def should_next_page(self, raw_data):
		soup = BeautifulSoup(raw_data, 'lxml')
		tag = soup.find_all('div', class_="product details product-item-details")
		if tag:
			if len(tag) >= 24:
				return True