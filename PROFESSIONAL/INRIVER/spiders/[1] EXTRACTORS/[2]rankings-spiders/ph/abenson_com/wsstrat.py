import logging
from copy import deepcopy
import json
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class AbensonComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.abenson.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1
            
        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        tag = soup.find('script', type='application/ld+json', text=re.compile(r'.*itemList.*')) or soup.find('script', type='application/json')
        if tag:
            _json = json.loads(tag.get_text().strip())
            if 'itemListElement' in _json:
                return _json['itemListElement']
            else:
                if 'query' in _json and 'data' in _json['query'] and 'mainContent' in _json['query']['data'] and 'contentItem' in _json['query']['data']['mainContent']\
                    and 'contents' in _json['query']['data']['mainContent']['contentItem'] and _json['query']['data']['mainContent']['contentItem']['contents']\
                        and 'mainContent' in _json['query']['data']['mainContent']['contentItem']['contents'][0]:
                    item_directory = None
                    for item in _json['query']['data']['mainContent']['contentItem']['contents'][0]['mainContent']:
                        if 'name' in item.keys() and item['name'] == 'Results List Collection':
                            item_directory = item
                            break
                    if item_directory and 'contents' in item_directory and item_directory['contents'] and 'records' in item_directory['contents'][0]:
                        return item_directory['contents'][0]['records']
                    else:
                        return []
                else:
                    raw_data = self.downloader.api_data()
                    if raw_data:
                        try:
                            _json = json.loads(raw_data)
                            return _json['mainContent']['records']
                        except Exception as e:
                            return []
        else:
            return []

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            if 'item' in tag:
                value = tag['item']['name']
            else:
                if 'productDisplayName' in tag and tag['productDisplayName']:
                    value = tag['productDisplayName'][0]

            if value == 'NOT_FOUND' and '_t' in tag:
                value = tag['_t']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            if 'item' in tag:
                link = tag['item']['url']

                if "https://{}".format(self.__class__.WEBSITE) not in link.lower():
                    value = "https://{website}{link}".format(website=self.__class__.WEBSITE, link=link)

                elif self.__class__.WEBSITE in link.lower() and 'http' not in link.lower():
                    value = 'https://{}'.format(link)

                else:
                    value = link
            else:
                if 'productDisplayName' in tag and tag['productDisplayName'] and 'productId' in tag and tag['productId']:
                    name_url = tag['productDisplayName'][0].replace(' ','-')
                    prod_id = tag['productId'][0]
                    value = 'https://www.liverpool.com.mx/tienda/pdp/'+name_url+'/'+prod_id

            if value == 'NOT_FOUND' and 'allMeta' in tag and 'id' in tag['allMeta']:
                _name = tag['_t']
                _id = tag['allMeta']['id']
                name_url = _name.replace(' ','-')

                value = 'https://www.liverpool.com.mx/tienda/pdp/'+name_url+'/'+_id

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
