import logging
from copy import deepcopy
import json
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class AbensonComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.abenson.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for i in range(0, len(tags)):
            result = deepcopy(template)
            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[i])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[i])

            results.append(result)

            if last_rank == limit:
                break

            last_rank = last_rank + 1
                
        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        tag = soup.find_all('div', class_="product details product-item-details")
        if tag:
            return tag

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title_tag = tag.find('a', class_="product-item-link")
            if title_tag:
                source, value = str(tag), title_tag.get_text()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_tag = tag.find('a', class_="product-item-link")
            if url_tag and url_tag['href']:
                source, value = str(tag), url_tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value