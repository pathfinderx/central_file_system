import json
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
	URLPreparationFailureException


class ShopeePhDownloadStrategy(DownloadStrategy):
	def __init__(self, requester):
		self.requester = requester
		self._search_string = None

	def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(search_string, str)
		assert isinstance(timeout, int)
		self._search_string = search_string
		raw_data_list = []

		if not isinstance(headers, dict):
			headers = {
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
				'x-api-source': 'pc',
				'x-requested-with': 'XMLHttpRequest',
				'x-shopee-language': 'en'
			}

		if not isinstance(cookies, dict):
			cookies = None

		try:
			urls = self.get_urls(search_string)
		except:
			raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

		for i in range(len(urls)):
			try: 
				res = self.requester.get(urls[i], timeout=timeout, headers=headers, cookies=cookies)

				if res.status_code in [200, 201, 202]:
					raw_data_list.append(res.text)
					
					if not self.should_next_page(res.text):
						break
					
			except PaginationFailureException:
				raise PaginationFailureException(f'Pagination Failed - Page {i+1}')

			except:
				raise DownloadFailureException("Download Failed - Unhandkled Exception")

		return raw_data_list

	def get_urls(self, search_string):
		urls = []
		search_string = search_string.replace(" ", "+").lower()

		urls = [
			f'https://shopee.ph/api/v4/search/search_items?by=relevancy&keyword={search_string}&limit=60&newest=0&order=desc&page_type=search&scenario=PAGE_GLOBAL_SEARCH&version=2',
			f'https://shopee.ph/api/v4/search/search_items?by=relevancy&keyword={search_string}&limit=60&newest=60&order=desc&page_type=search&scenario=PAGE_GLOBAL_SEARCH&version=2'
		]

		return urls

	def should_next_page(self, raw_data):
		soup = BeautifulSoup(raw_data, 'lxml')
		tag = soup.find_all('div', class_="product details product-item-details")
		if tag:
			if len(tag) >= 26:
				return True