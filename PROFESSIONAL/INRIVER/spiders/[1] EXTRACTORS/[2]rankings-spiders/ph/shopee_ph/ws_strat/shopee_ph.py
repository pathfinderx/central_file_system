import logging
from copy import deepcopy
import json
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class ShopeePhWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shopee.ph'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for i in range(0, len(tags)):
            result = deepcopy(template)
            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[i])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[i])

            results.append(result)

            if last_rank == limit:
                break

            last_rank = last_rank + 1
                
        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        _json = json.loads(raw_data)
        if _json and 'items' in _json:
            return _json['items']

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag and 'item_basic' in tag and 'name' in tag['item_basic']:
                source, value = str(tag), tag['item_basic']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag and 'item_basic' in tag:
                shopid = tag['item_basic']['shopid']
                itemid = tag['item_basic']['itemid']
                name = re.sub('[^A-Za-z0-9]+', '-', tag['item_basic']['name'])
                url = f'https://shopee.ph/{name}-i.{str(shopid)}.{str(itemid)}?'
                source, value = str(tag), url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value