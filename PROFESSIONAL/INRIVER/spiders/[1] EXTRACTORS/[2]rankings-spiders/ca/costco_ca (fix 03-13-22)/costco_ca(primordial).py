from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from bs4 import BeautifulSoup


class CostcoCaDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code
                
                if status_code in [200, 201]:
                    
                    product_list = self.get_raw_data(res.text)
                    if product_list:
                        raw_data_list.append(product_list)

                    if not self.next_page(res.text):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.costco.ca/CatalogSearch?dept=All&keyword={q_search_string}',
            f'https://www.costco.ca/CatalogSearch?currentPage=2&dept=All&pageSize=24&keyword={q_search_string}',
            f'https://www.costco.ca/CatalogSearch?currentPage=3&dept=All&pageSize=24&keyword={q_search_string}',
            f'https://www.costco.ca/CatalogSearch?currentPage=4&dept=All&pageSize=24&keyword={q_search_string}',
            f'https://www.costco.ca/CatalogSearch?currentPage=5&dept=All&pageSize=24&keyword={q_search_string}'
        ]

        return urls

    @staticmethod
    def get_raw_data(raw_data):
        data = None

        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.find('div',{'class':'product-list grid'})
        
        if tags and tags.findAll('div',{'class':'col-xs-6 col-lg-4 col-xl-3 product'}):
            data = str(tags)
        else:
             data = None

        return data

    def next_page(self,raw_data):
        soup = BeautifulSoup(raw_data,'lxml')
        tag = soup.select_one('li.forward a')
        if tag:
            return True
        else:
            return False