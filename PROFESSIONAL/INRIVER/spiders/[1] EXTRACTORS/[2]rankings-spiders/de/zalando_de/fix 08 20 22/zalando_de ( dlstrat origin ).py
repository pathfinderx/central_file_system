import json
import re
from urllib.parse import quote, quote_plus


from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class ZalandoDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                        "accept-encoding": "gzip, deflate, br",
                        "accept-language": "en-US,en;q=0.9,fil;q=0.8",
                        'referer': 'https://en.zalando.de/?_rfl=de',
                        "cache-control": "max-age=0",
                        "upgrade-insecure-requests": "1",
                        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.54 Safari/537.36"}
                 
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                self.requester.session.headers = headers
                if page == 0:
                    self._obtain_cookies() #Get cookies once

                res = self.requester.get(urls[page], timeout=timeout)
                print(urls[page])

                if res.status_code in [200, 201]:
                    raw_data_list.append(res.text)
                    if not self.__has_next_page(res.text):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        keyword = quote_plus(search_string)
        # if this url is used, it will let the website sever choose to which you should be redirected; but is inaccurate
        # just commented for future reference.
        # urls = [
        #     f'https://www.zalando.de/catalogue/?q={keyword}',
        #     f'https://www.zalando.de/catalogue/?q={keyword}&=2'
        # ]
        urls = [
            f"https://www.zalando.de/herren/?q={keyword}",
            f"https://www.zalando.de/herren/?q={keyword}&p=2"
        ]

        if search_string.lower() ==  'multiroom systeme':
            urls = [
                f'https://www.zalando.de/herren/?q={keyword}',
                f'https://www.zalando.de/herren/?q={keyword}&p=2'
            ]

        #temporary fixes for these keywords --- remove when not redirected anymore
        if search_string.lower() ==  'mobile lautsprecher':
            urls = [
                f'https://www.zalando.de/herren/?q=mobile+lautsprecher',
                f'https://www.zalando.de/herren/?q=mobile+lautsprecher&p=2'
            ]

        if search_string.lower() ==  'lautsprecher':
            urls = [
                # f'https://www.zalando.de/kids/?q={keyword}'
                f'https://www.zalando.de/herren/?q=lautsprecher'
            ]

        if search_string.lower() == 'kopfhörer noise cancelling':
            urls = [
                'https://www.zalando.de/herren/?q=kopfh%C3%B6rer+noise+cancelling',
                'https://www.zalando.de/herren/?q=kopfh%C3%B6rer+noise+cancelling&p=2'
            ]

        if search_string in ['polo shirt','short sleeve polo','sleeveless polo']:
            q_keyword = search_string.replace(' ','+')
            urls = [
                f'https://www.zalando.de/catalogue/?q={q_keyword}'
            ]

        if search_string.lower() in ['anc kopfhörer', 'bluetooth in ear kopfhörer','lautsprecher bluetooth','mini lautsprecher','smart lautsprecher']:
            q_keyword = search_string.replace(' ', '+')
            urls = [
                f'https://www.zalando.de/herren/?q={q_keyword}',
                f'https://www.zalando.de/herren/?q={q_keyword}&p=2',
            ]

        return urls

    def _obtain_cookies(self):
        self.requester.get('https://www.zalando.de/?_rfl=de')
        cookies = {
                    'Zalando-Client-Id':'d53db1f9-a172-4ddd-ae96-10231a88a3f4',  #TODO: Always update this when tags in site are not showing https://prnt.sc/HYzr7CvNluTL
                    'language-preference':'de'
                }
        self.requester.session.cookies.update(cookies)
        
        return None

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        _json = {}
        tag = soup.find('script', text=re.compile('current_page'))

        if tag:
            txt = re.search('\{.*\}', tag.get_text().strip(), re.DOTALL|re.MULTILINE)
            _json = json.loads(txt.group()) if txt else {}

            if 'articles' in _json:
                
                if len(_json['articles']) >= 84:
                    return True
        else:
            products = soup.select('div.DT5BTM.w8MdNG article')
            if products:
                # check and remove invalid products (as per request from validator)
                index_of_invalid_products = []
                for i, v in enumerate(products):
                    url_to_test = products[i].select_one('a._LM.JT3_zV.CKDt_l.CKDt_l.LyRfpJ').get('href')
                    if url_to_test:
                        hasValidUrl = re.search(r'(zalando|html)', url_to_test) # temporary indicator for valid products (url that contains 'zalando' or 'html')
                        if not hasValidUrl:
                            index_of_invalid_products.append(i)
                if index_of_invalid_products:
                    products = [values for index, values in enumerate(products) if index not in index_of_invalid_products] # deletion alternative to prevent inaccuracy of deletion due to shifting of index
                if products:
                    products = [x for x in products if 'Gesponsert' not in str(x.parent.parent)]
                    if len(products) <= 24:
                        return True

        return False