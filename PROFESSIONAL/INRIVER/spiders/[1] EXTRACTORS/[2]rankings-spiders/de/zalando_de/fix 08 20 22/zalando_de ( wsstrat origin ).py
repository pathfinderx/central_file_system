import logging
from copy import deepcopy
import json, re
from operator import index

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup, element

class ZalandoDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.zalando.de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        source, tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(source, tags[idx])

            # # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(source, tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(source, tags[idx])

            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1

        return results, last_rank


    def get_product_tags(self, raw_data):
        # returns a JSON or soup tag

        soup = BeautifulSoup(raw_data, 'lxml')
        _json = {}
        tag = soup.find('script', text=re.compile('current_page'))

        products = soup.select('div.DT5BTM.w8MdNG article')
        if products:
            # check and remove invalid products (as per request from validator)
            index_of_invalid_products = []
            for i, v in enumerate(products):
                url_to_test = products[i].select_one('a._LM.JT3_zV.CKDt_l.CKDt_l.LyRfpJ').get('href')
                if url_to_test:
                    hasValidUrl = re.search(r'(zalando|html)', url_to_test) # temporary indicator for valid products (url that contains 'zalando' or 'html')
                    if not hasValidUrl:
                        index_of_invalid_products.append(i)
            if index_of_invalid_products:
                products = [values for index, values in enumerate(products) if index not in index_of_invalid_products] # deletion alternative to prevent inaccuracy of deletion due to shifting of index
            if products:
                # products = [x for x in products if 'Gesponsert' not in str(x.parent.parent)]
                # products = [x for x in products if str(x.parent.parent) not in ['Sponsored', 'Gesponsert']]
                products = [x for x in products if not str(x.parent.parent).find('Gesponsert' or 'Sponsored') > -1]
            return str(products), products

        if not products:
            if tag:
                txt = re.search('\{.*\}', tag.get_text().strip(), re.DOTALL|re.MULTILINE)
                _json = json.loads(txt.group()) if txt else {}
                if 'articles' in _json:
                    _json = _json['articles']

                return str(tag),  _json   

            else:
                # copied from listings
                tag = soup.find("script", text=re.compile('"id":"ern:product*'), attrs= {"type":"application/json"})
                if tag:
                    _json = json.loads(tag.get_text().strip())
                    prodtags = []
                    if 'graphqlCache' in _json:
                        src = _json['graphqlCache']
                        for ks in _json['graphqlCache']:
                            if '"id":"ern:product' in ks and 'product' in _json['graphqlCache'][ks]['data']:
                                prodtags.append(_json['graphqlCache'][ks]['data']['product'])

                        return str(src), prodtags

        return None

    def get_url(self, _source, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(data, element.Tag):
                # has 2 a tags, just find the first one.
                tag = data.select_one('a')
                if tag and tag.has_attr('href'):
                    value = tag['href']
                    source = str(tag)
            else:
                source = _source
                url_pref = 'https://%s/' % self.__class__.WEBSITE
                if 'url_key' in data:
                    if url_pref in data['url_key']:
                        value = data['url_key']
                    else:
                        value = '%s%s.html' % (url_pref, data['url_key'])
                elif 'uri' in data:
                    value = data['uri']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_title(self, _source, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(data, element.Tag):
                tag = data.select('h3')
                if len(tag) > 1:
                    value = tag[1].get_text().strip()
                    source = str(tag)
                else:
                    # if only 1 use the 0 index
                    value = tag[0].get_text().strip()
                    source = str(tag)
            else:
                source = _source
                if 'name' in data:
                    value = data['name']
            

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, _source, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(data, element.Tag):
                # # has 3 span tags, just find the first one.
                # tag = data.select_one('header div span')
                # as of 07/05/2022 the brand is move to the first h3
                # classes are unrealiable
                tag = data.select_one('h3')
                if tag:
                    value = tag.get_text().strip()
                    source = str(tag)
            else:
                source = _source
                if 'brand_name' in data:
                    value = data['brand_name']
                elif 'brand' in data and 'name' in data['brand']:
                    value = data['brand']['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value