import logging, json,requests
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BergzeitDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.bergzeit.de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)
        
        last_rank = start_rank
        # if tags:
        # import pdb; pdb.set_trace()
        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        # 
        # if json.loads(raw_data):
        #     jsonData = json.loads(raw_data)
        # else:
        #     soup = BeautifulSoup(raw_data, 'lxml')
        
        
        # if type(raw_data) is list:
        #     return raw_data
        # else:
        #     # import pdb; pdb.set_trace()
        #     tags = raw_data.select('div.list--product_small.is_element_list_article ')
        #     return tags
        tags = []
        try:
            _json = json.loads(raw_data)
            if 'elementsList' in _json.keys() and _json['elementsList']:
                tags = _json['elementsList']
        except:
            soup = BeautifulSoup(raw_data,'lxml')
            tags = soup.select_one('recommendations-teaser')
            if tags and tags.has_attr(':initial-products'):
                jsontag = tags.get(':initial-products')
                tags = json.loads(jsontag)

        return tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag:
                if type(tag) is dict:
                    if 'Title' in tag:
                        title = tag['Title']
                        value = title
                        source = str(tag)
                    elif 'title' in tag:
                        value = tag['title']
                        source = str(tag)
                    elif 'data' in tag and 'name' in tag['data']:
                        source = str(tag)
                        value = tag['data']['name']
                    elif 'name' in tag:
                        source = str(tag)
                        value = tag['name']
                else:
                    title = tag.find('strong',{'class':'title'}) 
                    value = title.text.strip().replace('\n','')
                    source = str(title)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag:
                if type(tag) is dict:
                    if 'Manufacturer' in tag:
                        brand = tag['Manufacturer']
                        value = brand
                        source = str(tag)
                    elif 'brand' in tag and 'name' in tag['brand']:
                        value = tag['brand']['name']
                        source = str(tag)
                    elif 'data' in tag and 'brand' in tag['data'] and 'name' in tag['data']['brand']:
                        value = tag['data']['brand']['name']
                        source = str(tag)
                else:
                    brand = tag.find('strong',{'class':'brand'}) 
                    value = brand.text.strip().replace('\n','')
                    source = str(brand)
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if type(tag) is dict:
                if 'Link' in tag:
                    url = tag['Link']
                    value = url
                    source = str(tag)
                elif 'deeplink_orig' in tag:
                    value = tag['deeplink_orig']
                    source = str(tag)
                elif 'url' in tag:
                    value = 'https://www.bergzeit.de/'+tag['url']
                    source = str(tag)
                elif 'data' in tag and 'url' in tag['data']:
                    value = 'https://www.bergzeit.de/'+tag['data']['url']
                    source = str(tag)
            else:
                # url = tag.find('a') 
                # value = url.get('href')
                # source = str(url)
                url_tag = tag.find('a',{'class':'is_product_link'})

                if tag:
                    url = url_tag.get('href')
                    _id = None
                    variant = None
                    size = None
                    variant_tag = tag.select_one('.selected_variant[data-config]')
                    if variant_tag:
                        variant_json = json.loads(variant_tag['data-config'])
                        if variant_json:
                            if 'variant' in variant_json.keys():
                                variant = variant_json['variant']
                            if 'id' in variant_json.keys():
                                _id = variant_json['id']

                            if variant and _id:
                                size_tags = tag.select('.available_sizes [data-config]')
                                if size_tags:
                                    for item in size_tags:
                                        size_json = json.loads(item['data-config'])
                                        if size_json:
                                            if _id == size_json['id']:
                                                size = size_json['variant']
                                                break
                                if size:
                                    temp_orig_url = url.split('brg')[0] if 'brg' in url else url
                                    temp_url = '{}-{}-{}/'.format(temp_orig_url[:-1],variant.replace(' ','-').replace('.','-').lower(),size.replace(' ','-').lower().replace('.','-'))
                                    if temp_url:
                                        res = requests.get(temp_url)
                                        
                                        if res.status_code in [200,201]:
                                            url = temp_url
                                        # print(temp_url+' '+str(res.status_code))

                    source, value = str(tag), url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
