from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from bs4 import BeautifulSoup
import json, urllib, re

class BergzeitDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None
            # headers = {
            #     'cookie': 'sid=daj1r409ug8849au3ctjevrmb1; sid_key=oxid; APE_COUNTRY_ID=8f241f110957e6ef8.56458418; APE_COUNTRY=DK; APE_INITIAL_COUNTRY=DK; isMobileCheckout=0; COUNTRYSELECT_OVERLAY_SHOWN=1',
            #     'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
            # }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:                   
                    if search_string in ['Regenhosen männer', 'Shelljacken frauen']:
                        raw_data = res.text.replace('_trbo.app.layerApi.callback(', '')
                        if raw_data:
                            raw_data1 = raw_data.replace('[]}]});', '[]}]}').replace(');','')
                            if raw_data1:
                                try:
                                    _json = json.loads(raw_data1)
                                except ValueError as e:
                                    print(e)

                                if _json:
                                    raw = _json.get('content_api2')
                                    matches = re.search(r'(#trboModule_13014_94284_61_container.trboModuleContainer .trbo-content .trbo-canvas .trbo-stage .trbo-item .trbo-badge)([\s\S]+)(</script)',raw)
                                    if matches

                                elif 'instance.setupProductSlider(' in _json['content_api2']:
                                    soup = BeautifulSoup(_json['content_api2'], 'lxml')

                                else:
                                    soup = BeautifulSoup(_json['additional_layers'][0]['content_api2'], 'lxml')
                                    
                                if soup:
                                    jsonData = soup.select('script')[0].get_text()
                                    script_text = str(soup).split('instance.setupProductSlider(')[1].split(');\n});\n')[0]
                                    _json = json.loads(script_text)
                                    raw_data_list.append(_json)
                    elif page == 0 and search_string not in ['Regenjacken frauen','Skihosen herren männer']:#,'Regenhosen männer']:
                        jsonData = json.loads(res.text)
                        raw_data_list.append(jsonData)
                    else:
                        soup = BeautifulSoup(res.text, 'html.parser')
                        if soup.text != '':
                            raw_data_list.append(soup)
                        else:
                            break
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_api_url(self, search_string):
        headers = {
            'accept': '*/*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'referer': 'https://www.bergzeit.de/',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'script',
            'sec-fetch-mode': 'no-cors',
            'sec-fetch-site': 'cross-site',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36'
        }

        payload = {
            'g': '{"st":2,"p":"https://www.bergzeit.de/?cl=search&ref=header&cat=search&selectedType=&articleId=&categoryId=&deeplink=&query=Shelljacken+frauen","t":1647056351,"sh":"13014","u":"f40c777ec14f9cc5db0a2a9efb887977","ts":0,"ust":1647053078,"sdt":469,"sc":2,"bsc":2,"bv":null,"sid":1715361475,"pt":2,"cb":{"value":0,"num":0,"voucher":0,"p":[],"c":[]},"bw":978,"bh":969,"ct":3,"cic":[],"cic2":[],"ref":"https://www.bergzeit.de/"}',
            's': '[{"sst":1647055893,"pvc":2,"sd":458},{"sst":1647053078,"pvc":2,"sd":11}]',
            'p': '{}',
            'pc': '{}',
            'sp': '{}',
            'cp': '[]',
            'nbp': '{}',
            'c': '{}',
            'mc': '[]',
            'b': '{}',
            'm': '[]',
            'bl': '[]',
            'dl': '{"categoryPath_latest":"Suche","category_path":"Suche","is_archived":"no","currentProduct":"n/a","override_segment_id":-1,"productCount":"false","pageTitle":"Deine Suche ergab leider keine Treffer","trbo_module_blacklist":[]}',
            'seg': '{"1":{"Suche":1647056024}}',
            'se': '{"e":{"c":[],"m":[]},"i":{"c":[],"m":[]}}',
            'cds': '[]',
            'geo': '{"latitude":"50.110922","longitude":"8.682127","postal_code":false,"country":"DE","region":"he","city":"frankfurt","source":"gcp"}',
            'dd': '{"type":"desktop","device":"desktop","os":"windows","isTouch":false}',
            '_': '1647056351903'
        }

        url = 'https://api-v4.trbo.com/r.php?{}'.format(urllib.parse.urlencode(payload))
        return url

    def get_urls(self, search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query={q_search_string}&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&fnc=getArticleListJson&_artperpage=100',
            f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query={q_search_string}&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiIiwicGFyYW1zIjpbXX0=&loadCount=1',
            f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query={q_search_string}&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiIiwicGFyYW1zIjpbXX0=&loadCount=2',
            f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query={q_search_string}&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiIiwicGFyYW1zIjpbXX0=&loadCount=3',
            f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query={q_search_string}&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiIiwicGFyYW1zIjpbXX0=&loadCount=4',
            
        ]
        if search_string == 'Hüttenschuhe herren':
            urls = [
                f'https://www.bergzeit.de/?cnid=942&Gender=Herren&lang=0&cat=search&key=H%C3%BCttenschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=942&Gender=Herren&lang=0&cat=search&key=H%C3%BCttenschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9odWV0dGVuc2NodWhlLWhlcnJlblwvIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiY2F0ZWdvcnkiLCJwYXJhbXMiOnsiY2F0ZWdvcnkiOiI5NDIiLCJvdXRsZXQiOiJmYWxzZSIsImdlbmRlciI6ImhlcnJlbiJ9fQ==&loadCount=1'
            ]
        elif search_string == 'Regenjacken frauen':
            urls = [
                f'https://www.bergzeit.de/?cl=search&ref=header&cat=search&selectedType=&articleId=&categoryId=&deeplink=&query=Regenjacken+frauen',
                f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query=Regenjacken+frauen&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiIiLCJzaG9wIjoiMSIsImxhbmciOiIxIiwicGFnZXR5cGUiOiIiLCJwYXJhbXMiOltdfQ==&loadCount=1'
                # f'https://www.bergzeit.de/index.php?articleId=&cat=search&categoryId=&cl=search&deeplink=&query=Regenjacken+frauen&ref=header&selectedType=&pgNr=1',
            ]

        # elif search_string =='Freizeithosen männer':
        #     urls = [
        #         f'https://www.bergzeit.de/?cnid=931&Gender=Herren&lang=0&cat=search&key=Freizeithosen+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
        #         f'https://www.bergzeit.de/?cnid=931&Gender=Herren&lang=0&cat=search&key=Freizeithosen+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9qZWFucy1oZXJyZW5cLyIsInNob3AiOiIyIiwibGFuZyI6IjAiLCJwYWdldHlwZSI6ImNhdGVnb3J5IiwicGFyYW1zIjp7ImNhdGVnb3J5IjoiOTMxIiwib3V0bGV0IjoiZmFsc2UiLCJnZW5kZXIiOiJoZXJyZW4ifX0=&loadCount=1'
        #     ]
        elif search_string == 'Laufhosen frauen':
            urls = [
                f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query=Laufhosen+frauen&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query=Laufhosen+frauen&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiIiLCJzaG9wIjoiMSIsImxhbmciOiIxIiwicGFnZXR5cGUiOiIiLCJwYXJhbXMiOltdfQ==&loadCount=1'
            ]
            
        # elif search_string == 'Laufhosen männer':
        #     urls = [
        #     f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query=Laufhosen+frauen&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&fnc=getArticleListJson&_artperpage=100',
        #     f'https://www.bergzeit.de/?cnid=91013&Gender=Herren&lang=0&cat=search&key=Laufhosen+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9sYXVmaG9zZW4taGVycmVuXC8iLCJzaG9wIjoiMiIsImxhbmciOiIwIiwicGFnZXR5cGUiOiJjYXRlZ29yeSIsInBhcmFtcyI6eyJjYXRlZ29yeSI6IjkxMDEzIiwib3V0bGV0IjoiZmFsc2UiLCJnZW5kZXIiOiJoZXJyZW4ifX0=&loadCount=1'
        #     ]
        # elif search_string =='Regenhosen männer':
        #     urls = [
        #     f'https://www.bergzeit.de/?cnid=934&Gender=Herren&lang=0&cat=search&key=Regenhosen+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
        #     f'https://www.bergzeit.de/?cnid=934&Gender=Herren&lang=0&cat=search&key=Regenhosen+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9yZWdlbmhvc2UtaGVycmVuXC8iLCJzaG9wIjoiMiIsImxhbmciOiIwIiwicGFnZXR5cGUiOiJjYXRlZ29yeSIsInBhcmFtcyI6eyJjYXRlZ29yeSI6IjkzNCIsIm91dGxldCI6ImZhbHNlIiwiZ2VuZGVyIjoiaGVycmVuIn19&loadCount=1'
        #     #f'https://api-v4.trbo.com/r.php?g=%7B%22st%22%3A1%2C%22p%22%3A%22https%3A%2F%2Fwww.bergzeit.de%2Fmonnet-regenhose%2F%3Fkey%3DRegenhosen%2Bm%25C3%25A4nner%26cat%3Dsearch%22%2C%22t%22%3A1633512076%2C%22sh%22%3A%2213014%22%2C%22u%22%3A%2278b7d1799c59b9c8ba4137180ee2840b%22%2C%22ts%22%3A0%2C%22ust%22%3A1633511968%2C%22sdt%22%3A108%2C%22sc%22%3A1%2C%22bsc%22%3A1%2C%22bv%22%3Anull%2C%22sid%22%3A493371604%2C%22pt%22%3A8%2C%22cb%22%3A%7B%22value%22%3A0%2C%22num%22%3A0%2C%22voucher%22%3A0%2C%22p%22%3A%5B%5D%2C%22c%22%3A%5B%5D%7D%2C%22bw%22%3A1920%2C%22bh%22%3A969%2C%22ct%22%3A3%2C%22cic%22%3A%5B%5D%2C%22cic2%22%3A%5B%5D%2C%22ref%22%3A%22https%3A%2F%2Fwww.bergzeit.de%2F%22%7D&s=%5B%7B%22sst%22%3A1633511968%2C%22pvc%22%3A3%2C%22sd%22%3A108%7D%5D&p=%7B%7D&pc=%7B%7D&sp=%7B%7D&cp=%5B%5D&nbp=%7B%7D&c=%7B%7D&mc=%5B%5D&b=%7B%7D&m=%5B%5D&bl=%5B%5D&dl=%7B%22category_path%22%3A%22Bekleidung%7CHosen%7CHardshellhosen%20%26%20Regenhosen%22%2C%22categoryPath%22%3A%22Bekleidung%7CHosen%7CHardshellhosen%20%26%20Regenhosen%22%2C%22is_archived%22%3A%22no%22%2C%22currentProduct%22%3A%22n%2Fa%22%2C%22override_segment_id%22%3A-1%2C%22productCount%22%3A%22false%22%2C%22pageTitle%22%3A%22Kategorie%20leer%22%7D&seg=%7B%221%22%3A%7B%22Bekleidung%22%3A1633512152%7D%7D&se=%7B%22e%22%3A%7B%22c%22%3A%5B%5D%2C%22m%22%3A%5B%5D%7D%2C%22i%22%3A%7B%22c%22%3A%5B%5D%2C%22m%22%3A%5B%5D%7D%7D&cds=%5B%5D&geo=%7B%22latitude%22%3A%2250.110922%22%2C%22longitude%22%3A%228.682127%22%2C%22postal_code%22%3Afalse%2C%22country%22%3A%22DE%22%2C%22region%22%3A%22he%22%2C%22city%22%3A%22frankfurt%22%2C%22source%22%3A%22gcp%22%7D&_=1633512076353'
        #     ]
        elif search_string =='Regenjacken herren':
            urls = [
                f'https://www.bergzeit.de/?cnid=908&Gender=Herren&lang=0&cat=search&key=Regenjacken+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=908&Gender=Herren&lang=0&cat=search&key=Regenjacken+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9yZWdlbmphY2tlLWhlcnJlblwvIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiY2F0ZWdvcnkiLCJwYXJhbXMiOnsiY2F0ZWdvcnkiOiI5MDgiLCJvdXRsZXQiOiJmYWxzZSIsImdlbmRlciI6ImhlcnJlbiJ9fQ==&loadCount=1'
            ]
        elif search_string =='Skihosen herren männer':
            urls = [
                # f'https://www.bergzeit.de/?cnid=936&Gender=Herren&lang=0&cat=search&key=Skihosen+herren+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?articleId=&cat=search&categoryId=&deeplink=&query=Skihosen+herren+m%C3%A4nner&ref=header&selectedType=&_artperpage=100&cl=apeViewAjaxList&refClass=search&proxyParams=eyJ1cmwiOiIiLCJzaG9wIjoiMSIsImxhbmciOiIxIiwicGFnZXR5cGUiOiIiLCJwYXJhbXMiOltdfQ==&loadCount=0',
                f'https://www.bergzeit.de/?cnid=936&Gender=Herren&lang=0&cat=search&key=Skihosen+herren+m%C3%A4nner&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9za2lob3NlLWhlcnJlblwvIiwic2hvcCI6IjIiLCJsYW5nIjoiMCIsInBhZ2V0eXBlIjoiY2F0ZWdvcnkiLCJwYXJhbXMiOnsiY2F0ZWdvcnkiOiI5MzYiLCJvdXRsZXQiOiJmYWxzZSIsImdlbmRlciI6ImhlcnJlbiJ9fQ==&loadCount=1'
            ]
        elif search_string.lower() == 'Skijacken herren'.lower():
            urls = [
                f'https://www.bergzeit.de/?cnid=909&Gender=Herren&lang=0&cat=search&key=Skijacken+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=909&Gender=Herren&lang=0&cat=search&key=Skijacken+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC9za2lqYWNrZS1oZXJyZW5cLyIsInNob3AiOiIyIiwibGFuZyI6IjAiLCJwYWdldHlwZSI6ImNhdGVnb3J5IiwicGFyYW1zIjp7ImNhdGVnb3J5IjoiOTA5Iiwib3V0bGV0IjoiZmFsc2UiLCJnZW5kZXIiOiJoZXJyZW4ifX0=&loadCount=1'
            ]
        elif search_string.lower() == 'Trekkingschuhe herren'.lower():
            urls = [
                f'https://www.bergzeit.de/?cnid=952&Gender=Herren&lang=0&cat=search&key=Trekkingschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=952&Gender=Herren&lang=0&cat=search&key=Trekkingschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC93YW5kZXJzY2h1aGUtaGVycmVuXC8iLCJzaG9wIjoiMiIsImxhbmciOiIwIiwicGFnZXR5cGUiOiJjYXRlZ29yeSIsInBhcmFtcyI6eyJjYXRlZ29yeSI6Ijk1MiIsIm91dGxldCI6ImZhbHNlIiwiZ2VuZGVyIjoiaGVycmVuIn19&loadCount=1'
            ]
        elif search_string.lower() == 'Wanderschuhe herren'.lower():
            urls = [
                f'https://www.bergzeit.de/?cnid=952&Gender=Herren&lang=0&cat=search&key=Wanderschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=952&Gender=Herren&lang=0&cat=search&key=Wanderschuhe+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC93YW5kZXJzY2h1aGUtaGVycmVuXC8iLCJzaG9wIjoiMiIsImxhbmciOiIwIiwicGFnZXR5cGUiOiJjYXRlZ29yeSIsInBhcmFtcyI6eyJjYXRlZ29yeSI6Ijk1MiIsIm91dGxldCI6ImZhbHNlIiwiZ2VuZGVyIjoiaGVycmVuIn19&loadCount=1'
            ]
        elif search_string.lower() == 'Windbreaker herren'.lower():
            urls = [
                f'https://www.bergzeit.de/?cnid=90990&Gender=Herren&lang=0&cat=search&key=Windbreaker+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&fnc=getArticleListJson&_artperpage=100',
                f'https://www.bergzeit.de/?cnid=90990&Gender=Herren&lang=0&cat=search&key=Windbreaker+herren&_artperpage=100&cl=apeViewAjaxList&refClass=alist&proxyParams=eyJ1cmwiOiJodHRwczpcL1wvd3d3LmJlcmd6ZWl0LmRlXC93aW5kYnJlYWtlci1oZXJyZW5cLyIsInNob3AiOiIyIiwibGFuZyI6IjAiLCJwYWdldHlwZSI6ImNhdGVnb3J5IiwicGFyYW1zIjp7ImNhdGVnb3J5IjoiOTA5OTAiLCJvdXRsZXQiOiJmYWxzZSIsImdlbmRlciI6ImhlcnJlbiJ9fQ==&loadCount=1'
            ]

        elif search_string.lower() == 'Shelljacken frauen'.lower():
            temp = self.get_api_url(search_string)
            if temp:
                urls = [
                    temp
                ]

        return urls

    