import logging
from copy import deepcopy
import json
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class OttoDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.otto.de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

             # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1
            
        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        # product_tags = soup.select('#san_resultSection article.product')
        product_tags = soup.select('#san_resultSection article.product .find_tile.find_tile--visible:not(.find_tile--sponsored)')
        # Commented temporarily -- Move below if for suggested products (will cause count mismatch when here)
        if not product_tags:
            similar_prod_tag = soup.select('article.product.similar') #suggested products

            if similar_prod_tag:
                for _spd in similar_prod_tag:
                    product_tags.append(_spd)
                    
        temp_prod_tags  = []
        for i in product_tags:
            result = None
            # check if product tag is a sponsored item ---> sponsored items have data in page source
            # if not sponsored get api url and download data for each item ---> no API found that has all items
            # if not i.select_one('.find_tile__sponsoredContainer'): --> this tag still exist without a visible word sponsored but inactive
            # tag = i.select_one('.find_tile')
            # if tag and tag.has_attr('data-href'):
            href = None
            if i.has_attr('data-href'):
                href = i['data-href']
            else: 
                tag = i.select_one('li')
                if tag and tag.has_attr('data-href'):
                    href = tag['data-href']
            api_url = 'https://www.otto.de'+ href
            response = self.downloader.extra_download(api_url)
            if response:
                res_soup = BeautifulSoup(response,'lxml')
                result = res_soup

            if result:
                temp_prod_tags.append(result)



        product_tags = temp_prod_tags

        return product_tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            title_tag = tag.select_one('a.productLink') or tag.select_one('.find_tile__name')
            if title_tag:
                value = title_tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)

            url_tag = tag.select_one('a.productLink') or tag.select_one('a.find_tile__productLink')
            if url_tag and url_tag.has_attr('href'):
                link = url_tag.get('href')

                if "https://{}".format(self.__class__.WEBSITE) not in link.lower():
                    value = "https://{website}{link}".format(website=self.__class__.WEBSITE, link=link)

                elif self.__class__.WEBSITE in link.lower() and 'http' not in link.lower():
                    value = 'https://{}'.format(link)

                else:
                    value = link

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
    
    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            brand_tag = tag.find('script', type="text/html", text = re.compile(r'product.*'))
            if brand_tag:
                _json = json.loads(brand_tag.get_text().strip())
                if 'brand' in _json['product'].keys():
                    value = _json['product']['brand']['value']
            else:
                brand_tag = tag.select_one('.find_tile__brand')
                if brand_tag:
                    value = brand_tag.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value
