import json
import re
from urllib.parse import quote, urlencode

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests

class SaturnDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = UnblockerSessionRequests('de')

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-US,en;q=0.9',
                # 'apollographql-client-name': 'pwa-client', # TODO: might be changed over time
                # 'apollographql-client-version': '6.93.1', # TODO: might be changed over time
                'cache-control': 'no-cache',
                # 'content-type': 'application/json',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
                'x-cacheable': 'true',
                # 'x-flow-id': '46de8898-3e66-4c31-80a2-0a2cefdc6be9', # TODO: might be changed over time
                'x-mms-country': 'DE',
                'x-mms-language': 'de',
                'x-mms-salesline': 'Saturn',
                # 'x-operation': 'SearchV4' # TODO: might be changed over time
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        

        for page in range(0, len(urls)):
            try:
                
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                 
                if res.status_code in [200, 201]:
                    result = self.get_result_from_script(res.text)

                    if res.headers.get('x-unblocker-redirected-to'):
                        print(res.headers.get('x-unblocker-redirected-to'))
                        url = res.headers['x-unblocker-redirected-to'] + '&page={}'.format(page+1)

                        res = self.requester.get(url, timeout=timeout, headers=headers)
                            
                    result = self.get_result_from_script(res.text)
                    
                    if result and result[1]:
                        raw_data_list.append(result[0])
                    else:
                        
                        # FOR SUGGESTED PRODUCTS
                        url = 'https://www.saturn.de/api/v1/graphql'
                        params = {}
                        if search_string == 'motorola': #Top Level category suggested products
                            params['operationName'] = 'GetOneRecommendationsGroupedByType'
                            variables = {
                                "withMarketingInfos":False,
                                "touchpoint":"WEB_DESKTOP",
                                "recommendationContext":"BRAND",
                                "ref":"57",
                                "types":["HYPER_PERSONAL"],
                                "personalization":"ca9cbaab-e887-4968-a330-fe142346cc26",
                                "limit":25,
                                "manualProducts":[[]]
                            }
                            extensions = {
                            "pwa":{
                                "salesLine":"Saturn",
                                "country":"DE",
                                "language":"de"},
                            "persistedQuery":{
                                "version":1,
                                "sha256Hash":"3b9d6912a9bdfeda5df0e2d7e8160c16f8405db4ddafb369bc0257ad77a5eab3"} # TODO: please update this incase of failure
                        }
                            
                        else:
                            params['operationName'] = 'GetProductRecommendations'
                            variables = {
                                "productsToResolve":6,
                                "boxName":"box01_User2Products",
                                "recommendationContext":"noresult-search",
                                "params":f"&searchterm={quote(search_string)}",
                                "sid":"05523a73-6561-49c3-b238-2d017d4ed6ee",
                                "tracking":True,
                                "manualProducts":[]
                            }
                            extensions = {
                                "pwa":{
                                    "salesLine":"Saturn",
                                    "country":"DE",
                                    "language":"de"},
                                "persistedQuery":{
                                    "version":1,
                                    "sha256Hash":"4b85fe630f8f5edbd180f6f760e8aa07f7ec9544fa892d3cab084bc4694b6a4e"} # TODO: please update this incase of failure
                            }
                        params['variables'] = json.dumps(variables, ensure_ascii=False)
                        params['extensions'] = json.dumps(extensions, ensure_ascii=False)
                        url = 'https://www.saturn.de/api/v1/graphql?{}'.format(urlencode(params))

                        res = self.requester.get(url, timeout=timeout, headers=None)
                        if res.status_code in [200, 201]:
                            result = {}
                            _json = res.json()
                            result['source'] = urls[page]
                            result['raw'] = _json
                            raw_data_list.append(json.dumps(result, ensure_ascii=False))
                            
                            if 'productRecommendations' in _json['data']:
                                product_ids = [product['productId'] for product in _json['data']['productRecommendations']['products'] if not product['product']]
                            elif 'productRecommendationsV2GroupedByType' in _json['data']:
                                product_ids = [product['productId'] for product in _json['data']['productRecommendationsV2GroupedByType'][0]['products'] if not product['product']]
                            else:
                                product_ids = []
                            _params = {}
                            _params['operationName'] = 'GetRecoProductsById'
                            if search_string == 'motorola':
                                variables = {"ids": product_ids,'withMarketingInfos':False}
                                extensions = {
                                    "pwa":{"salesLine":"Saturn","country":"DE","language":"de"},
                                    "persistedQuery":{"version":1,
                                        "sha256Hash":"ba1251c7568dcaaa8829135e77f9a490820e5cb020a2336df176894ec4cddcae"} # TODO: please update this incase of failure
                                    }
                            else:
                                variables = {"ids": product_ids}
                                extensions = {
                                    "pwa":{"salesLine":"Saturn","country":"DE","language":"de"},
                                    "persistedQuery":{"version":1,
                                        "sha256Hash":"edd8a7c57ffafa6a33b55cd07b2d587ff050d4b3cad4ce77a4af01a19f2c123e"} # TODO: please update this incase of failure
                                    }
                            _params['variables'] = json.dumps(variables, ensure_ascii=False, separators=(',', ':'))
                            _params['extensions'] = json.dumps(extensions, ensure_ascii=False, separators=(',', ':'))
                            url = 'https://www.saturn.de/api/v1/graphql?{}'.format(urlencode(_params))
                            res = self.requester.get(url, timeout=timeout, headers=None)
                            if res.status_code in [200, 201]:
                                result = {}
                                _json = res.json()
                                result['source'] = urls[page]
                                result['raw'] = _json
                                raw_data_list.append(json.dumps(result, ensure_ascii=False))
                        break                    
 
                    if not self.has_next_page(res.text, page):
                        break
                    # data = {
                    #     'raw': res.json(),
                    #     'source': urls[page]
                    # }
                    # raw_data_list.append(json.dumps(data))
                    
                    # # Page break check
                    # _json = res.json()
                    # if 'paging' in _json['data']['searchV4'].keys():
                    #     if _json['data']['searchV4']['paging']['currentPage'] == _json['data']['searchV4']['paging']['pageCount']:
                    #         break
                # else:
                #     if 'category' in res.url:

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(' ','+')
        sha256Hash = '9fb05c43e35132d1652195e729b67ea5bffe51ed3f87338aeaceafd29004b150' # TODO: might be changed over time
        base_api_url = 'https://www.saturn.de/api/v1/graphql?'
        for x in range(1, 5): # 12 items per page
            url = f'{base_api_url}operationName=SearchV4&variables=%7B%22hasMarketplace%22%3Atrue%2C%22isRequestSponsoredSearch%22%3Atrue%2C%22maxNumberOfAds%22%3A2%2C%22isDemonstrationModelAvailabilityActive%22%3Afalse%2C%22withMarketingInfos%22%3Afalse%2C%22experiment%22%3A%22mp%22%2C%22filters%22%3A%5B%5D%2C%22page%22%3A{x}%2C%22query%22%3A%22{quote(search_string)}sessionId%22%3A%2226084b76-4e9c-4e27-8a13-c19ccfe64c0d%22%2C%22customerId%22%3A%2226084b76-4e9c-4e27-8a13-c19ccfe64c0d%22%2C%22pageType%22%3A%22Search%22%2C%22productFilters%22%3A%5B%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22{sha256Hash}%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Saturn%22%2C%22country%22%3A%22DE%22%2C%22language%22%3A%22de%22%2C%22ccr%22%3Atrue%7D%7D'

            '''
            url = 'https://www.saturn.de/api/v1/graphql?operationName=SearchV4&' \
                    'variables=%7B%22hasMarketplace%22%3Atrue%2C%22query%22%3A%22'+ quote(search_string) +'%22%2C%22page%22%3A'+ str(x) +'%2C%22experiment%22%3A%22mp%22%7D&' \
                    'extensions=%7B%22pwa%22%3A%7B%22salesLine%22%3A%22Saturn%22%2C%22country%22%3A%22DE%22%2C%22language%22%3A%22de%22%7D%2C%22' \
                    'persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22'+ sha256Hash +'%22%7D%7D'
            '''
            # url = f'https://www.saturn.de/de/search.html?q={q_search_string}&ids_only=true&page_size=12&page={x}'

            urls.append(url)                        

        return urls

    def get_result_from_script(self, raw_data):
        result = {}
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            result['source'] = str(tag)
                            result['raw'] = _json['apolloState']['ROOT_QUERY'][keys]['resultItems']
                            has_data = True if result['raw'] else False
                            return json.dumps(result), has_data
                        
        except:
            return result, False

    def has_next_page(self, raw_data, page):
        result = []
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            if page+1 < _json['apolloState']['ROOT_QUERY'][keys]['paging']['pageCount']:
                                return True
                            else:
                                return False
        except:
            return False
