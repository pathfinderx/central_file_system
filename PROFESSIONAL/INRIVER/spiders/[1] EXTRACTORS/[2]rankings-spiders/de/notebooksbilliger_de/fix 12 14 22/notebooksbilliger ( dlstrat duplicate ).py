import json
import re
from socket import timeout
import time
from urllib.parse import unquote, urlencode, quote
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
import cloudscraper

class NotebooksbilligerDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
        

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9",
                "upgrade-insecure-requests": "1",
                "referer": "https://www.notebooksbilliger.de/produkte/{}".format(quote(search_string)),
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
                # Dont hesitate to uncomment this line if something went wrong or result does not match from the website
                # "x-newrelic-id": "VwcHU15SDxABU1VVAQkGU1w="
            }

        if not isinstance(cookies, dict):
            cookies = None
        
        raw_data_list = []

        try:
            urls = self.get_urls(search_string)
            
        except Exception as e:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        for page in range(0, len(urls)):
            try:
                if page == 0:
                    headers['referer'] = 'https://www.notebooksbilliger.de'
                else:
                    headers['referer'] = urls[page - 1]
                    
                res = self.scraper.get(urls[page], headers=headers, timeout=timeout,proxies = self.requester.session.proxies)
                print('Done Downloading ' , urls[page])
                print('Redirected to', res.url)

                if res.status_code in [200, 201]:

                    raw_data_list.append(res.text)

                    if not self.__has_next_page(res.text):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list


    def get_urls(self, search_string):
        if search_string in []:#'Gaming Mousemat',
        # 'Gaming-Headset','Gaming-Lautsprecher','Gaming-Maus','Gaming-Notebook',
        # 'Keypad','PC-Headset','PC-Maus',
        # 'Stuhl','kopfhörer'
        # ]:
            q_search_string = search_string.lower().replace(' ','%2520').replace('ö','%25c3%25b6')
            url = [
                
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}',
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}?page=2',
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}?page=3'
            ]

        elif search_string in ['Ergonomischer Stuhl','Keypad','PC Keypad','PC Tastatur','beste Kopfhörer']:
            q_search_string = search_string.lower().replace(' ','%2520').replace('ö','%25c3%25b6')
            url = [
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}'
            ]

        elif search_string in ['Gaming Stuhl']:
            q_search_string = search_string.lower().replace(' ','+')
            url = [
                f'https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/gaming+stuehle',
                'https://www.notebooksbilliger.de/categories/fetch-more/21660/10',
                'https://www.notebooksbilliger.de/categories/fetch-more/21660/20'
                # f'https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/{q_search_string}',
                # f'https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/{q_search_string}?page=2',
                # f'https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/{q_search_string}?page=3'
            ]
        elif search_string in ['Mauspad','Kabellose Maus']:
            if search_string.lower() != 'kabellose maus':
                q_search_string = search_string.lower().replace(' ','+')
                url = [
                    'https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/mauspads',
                    'https://www.notebooksbilliger.de/categories/fetch-more/5774/20',
                    'https://www.notebooksbilliger.de/categories/fetch-more/5774/30'
                    # f'https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/{q_search_string}',
                    # f'https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/{q_search_string}?page=2',
                    # f'https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/{q_search_string}?page=3'
                ]
            else:
                search_string == 'wireless maeuse'
                q_search_string = search_string.lower().replace(' ','+')
                url = [
                    'https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/wireless+maeuse',
                    'https://www.notebooksbilliger.de/categories/fetch-more/1651/10',
                    'https://www.notebooksbilliger.de/categories/fetch-more/1651/20'
                ]
        elif search_string in ['Notebook','Tastatur','Laptop','Gaming Laptop','headset','Maus','Tastatur']:
            if search_string.lower() == 'laptop':
                url = [
                    f'https://www.notebooksbilliger.de/notebooks',
                    f'https://www.notebooksbilliger.de/notebooks?page=2',
                    f'https://www.notebooksbilliger.de/notebooks?page=3'
                ]
            elif search_string.lower() == 'Gaming Laptop'.lower():
                url = [
                    'https://www.notebooksbilliger.de/notebooks/gaming+notebooks',
                    'https://www.notebooksbilliger.de/notebooks/gaming+notebooks?page=2',
                    'https://www.notebooksbilliger.de/notebooks/gaming+notebooks?page=3'
                ]
            elif search_string.lower() == 'maus':
                search_string = 'pc maus'
                q_search_string = search_string.lower().replace(' ','+')
                url = [
                    f'https://www.notebooksbilliger.de/{q_search_string}',
                    f'https://www.notebooksbilliger.de/categories/fetch-more/507/10',
                    f'https://www.notebooksbilliger.de/categories/fetch-more/507/20',
                    # f'https://www.notebooksbilliger.de/{q_search_string}?page=2',
                    # f'https://www.notebooksbilliger.de/{q_search_string}?page=3'
                ]
            elif search_string.lower() == 'tastatur':
                search_string = 'pc maus'
                q_search_string = search_string.lower().replace(' ','+')
                url = [
                    f'https://www.notebooksbilliger.de/tastaturen',
                    f'https://www.notebooksbilliger.de/tastaturen?page=2',
                    f'https://www.notebooksbilliger.de/tastaturen?page=3'
                ]
            elif search_string.lower() == 'headset':
                search_string = 'headets'
                # q_search_string = search_string.lower().replace(' ','+')
                url = [
                    'https://www.notebooksbilliger.de/headsets',
                    # 'https://www.notebooksbilliger.de/headsets?page=2',
                    # 'https://www.notebooksbilliger.de/headsets?page=3'
                    'https://www.notebooksbilliger.de/categories/fetch-more/1447/20?page=2',
                    'https://www.notebooksbilliger.de/categories/fetch-more/1447/30?page=3'
                        
                ]
            else:
                q_search_string = search_string.lower().replace(' ','+')
                url = [
                    f'https://www.notebooksbilliger.de/{q_search_string}',
                    f'https://www.notebooksbilliger.de/{q_search_string}?page=2',
                    f'https://www.notebooksbilliger.de/{q_search_string}?page=3'
                ]
        
        elif search_string in ['Mechanische Tastatur']:
            # url = []
            # page_1_params = {
            #     "box_34207_575[]": "16140",
            #     "availability": "alle"
            # }
            # params = {
            #     "availability": "alle",
            #     "box_34207_575[0]": "16140",
            #     "page": "NaN"
            # }
            
            # for page in range(1, 4):
            #     # if page == 1:
            #     #     uri = f'https://www.notebooksbilliger.de/tastaturen/page/{page}?{urlencode(page_1_params)}'
            #     # else:
            #     #     # params['page'] = page
            #     offset = page * 10
            #     uri = f'https://www.notebooksbilliger.de/categories/fetch-more/575/{offset}?{urlencode(params)}'
                    
            #     url.append(uri)

            url = [
                # f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle',
                # f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle&page=2',
                # f'https://www.notebooksbilliger.de/categories/fetch-more/575/20?availability=alle&box_34207_575%5B0%5D=16140'

                # f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle&page',
                # f'https://www.notebooksbilliger.de/categories/fetch-more/575/10?availability=alle&box_34207_575%5B0%5D=16140',
                # f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle'

                f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle',
                f'https://www.notebooksbilliger.de/categories/fetch-more/575/10?availability=alle&box_34207_575%5B0%5D=16140&page=2',
                f'https://www.notebooksbilliger.de/categories/fetch-more/575/20?availability=alle&box_34207_575%5B0%5D=16140&page=3'

                # f'https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle',
                # f'https://www.notebooksbilliger.de/categories/fetch-more/575/10?availability=alle&box_34207_575%5B0%5D=1614',
                # f'https://www.notebooksbilliger.de/categories/fetch-more/575/20?availability=alle&box_34207_575%5B0%5D=16140&page=Nan',

                # f"https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle",
                # f"https://www.notebooksbilliger.de/categories/fetch-more/575/10?availability=alle&box_34207_575%5B0%5D=16140",
                # f"https://www.notebooksbilliger.de/tastaturen?box_34207_575%5B0%5D=16140&availability=alle&page=2"
            ]

        elif search_string in ['2.1 Lautsprecher','Xbox Controller','Mechanical Keypad','PC-Headset','Playstation Controller','Ps4-Controller','PS-Controller','Xbox One X Controller','Gaming Keypad']:
            q_search_string = search_string.lower().replace(' ','%2520')
            url = [
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}',
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}/10'
            ]
        elif search_string in ['lautsprecher','Gaming Mousemat','Gaming-Headset','Gaming-Lautsprecher','Gaming-Maus','Gaming-Notebook','kopfhörer','PC-Maus','Stuhl']:
            q_search_string = search_string.lower().replace(' ','%2520').replace('ö','%25c3%25b6')
            url = [
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}',
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}/10',
                f'https://www.notebooksbilliger.de/produkte/{q_search_string}/20'
            ]
        
        elif search_string in ['Bürostuhl']:
            url = [f'https://www.notebooksbilliger.de/produkte/b%25c3%25bcrostuhl']
        
        elif search_string.lower() in ['gaming tastatur']:
            # url = 'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen?eqsqid=ed8cd367-099f-4b86-b419-9a389097f5f0'
            #10 page per hits #Keyword is being redirect to Category
            url = [
                'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen',
                'https://www.notebooksbilliger.de/categories/fetch-more/7846/10',
                'https://www.notebooksbilliger.de/categories/fetch-more/7846/20'
            ]
        elif search_string.lower() in ['anc-kopfhörer']:
            url = [
                'https://www.notebooksbilliger.de/produkte/anc-kopfh%25c3%25b6rer'
            ]
        
            
        return url
        

    # COMMENT FOR NOW. please do not remove this. thanks

    #     _ids = self.__get_ids(search_string)
    #     params = {
    #         'pids' : ','.join(_ids)

    #     }
        
    #     url = ['https://www.notebooksbilliger.de/extensions/ntbde/getsearchlisting.php?{}'.format(unquote(urlencode(params)))]

    #     if 'Gaming Chair' in search_string: # temporary fix for 'Gaming Chair' --- data is in page source of this url --- remove when keyword returns to normal search
    #         url = 'https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/gaming+stuehle?eqsqid=8ed2c39d-44cf-4d91-9e19-937999759d07'


        # if 'gaming tastatur'.lower() in search_string.lower():
        #     # url = 'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen?eqsqid=ed8cd367-099f-4b86-b419-9a389097f5f0'
        #     #10 page per hits #Keyword is being redirect to Category
        #     url = [
        #         'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen',
        #         'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen?page=2',
        #         'https://www.notebooksbilliger.de/gaming/gaming+peripherie/gaming+tastaturen?page=3'
        #     ]


    #     if 'Laptop' in search_string: # temporary fix for 'Gaming Chair' --- data is in page source of this url --- remove when keyword returns to normal search
    #         #url = 'https://www.notebooksbilliger.de/notebooks?perPage=50&sort=popularity&order=desc&availability=alle'
    #         #10 page per hits #Keyword is being redirect to Category
    #         url = [
    #             'https://www.notebooksbilliger.de/notebooks',
    #             'https://www.notebooksbilliger.de/notebooks?page=2',
    #             'https://www.notebooksbilliger.de/notebooks?page=3'
    #         ]

        
    #     if search_string in ['beste Kopfhörer']:
    #         url = [
    #             'https://www.notebooksbilliger.de/produkte/beste%2520kopfh%25c3%25b6rer'
    #         ]
        
        # if search_string in ['ANC-Kopfhörer']:
        #     url = [
        #         'https://www.notebooksbilliger.de/produkte/anc-kopfh%25c3%25b6rer'
        #     ]

    #     if search_string in ['Bürostuhl']:
    #         url = [
    #             'https://www.notebooksbilliger.de/produkte/b%25c3%25bcrostuhl'
    #         ]
    #     if search_string in ['2.1 Lautsprecher','Ergonomischer Stuhl','Gaming Keypad','Gaming Mousemat']:
    #         q_search_string = search_string.lower().replace(' ','%2520')
    #         url = [
    #             f'https://www.notebooksbilliger.de/produkte/{q_search_string}',
    #             f'https://www.notebooksbilliger.de/produkte/{q_search_string}?page=2'
    #             f'https://www.notebooksbilliger.de/produkte/{q_search_string}?page=3'
    #         ]
            
        
    #     if search_string in ['Mechanische Tastatur']:
    #         url = [
    #             'https://www.notebooksbilliger.de/tastaturen/page/1?box_34207_575%5B%5D=16140&availability=alle',
    #             'https://www.notebooksbilliger.de/tastaturen/page/1?box_34207_575%5B%5D=16140&availability=alle&page=2',
    #             'https://www.notebooksbilliger.de/tastaturen/page/1?box_34207_575%5B%5D=16140&availability=alle&page=3'
    #         ]

    #     return url

        

    # def __get_ids(self, search_string):

    #     ids = []
    #     # Hard bypass (Redirect)
    #     if search_string.lower() == 'mechanische tastatur':
    #         headers = {
    #             "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    #             "accept-encoding": "gzip, deflate",
    #             "accept-language": "en-US,en;q=0.9",
    #             "upgrade-insecure-requests": "1",
    #             "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36"
    #         }
    #         url = 'https://www.notebooksbilliger.de/tastaturen/page/1?box_34207_575[]=16140&availability=alle'

    #         res = self.requester.get(url, headers=headers)

    #         if res.status_code in [200, 201]:
    #             soup = BeautifulSoup(res.text, 'lxml')

    #             tags = soup.select('[data-product-id]')

    #             if tags:
    #                 ids = [i['data-product-id'] for i in tags]
    #     else:
    #         headers = {
    #             "Accept": "*/*",
    #             "Accept-Encoding": "gzip, deflate",
    #             "Accept-Language": "en-US,en;q=0.9",
    #             "Connection": "keep-alive",
    #             "Host": "search.epoq.de",
    #             "Referer": "https://www.notebooksbilliger.de/",
    #             "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"
    #         }            
    #         params = {
    #             "tenantId": "notebooksbilliger",
    #             "format": "json",
    #             "query": search_string,
    #             "sessionId": self.__get_session_id(),
    #             "orderBy": "",
    #             "order": "desc",
    #             "locakey": "",
    #             "style": "onlyId",
    #             "full": "",
    #             "": "",
    #             "limit": "100",
    #             "offset": "0",
    #             "_": str(time.time()).replace('.', '')[:13]
    #         }

    #         url = 'https://search.epoq.de/inbound-servletapi/getSearchResult?{}'.format(urlencode(params))

    #         res = self.requester.get(url, headers=headers)

    #         if res.status_code in [200, 201]:
    #             try:
    #                 _json = json.loads(res.text)
    #             except:
    #                 rgx = re.search('\((.*)\)', res.text)

    #                 if rgx:
    #                     _json = json.loads(rgx.group(1))

                
    #             if isinstance(_json['result']['findings']['finding'],dict): #for only 1 item in search results
    #                 ids.append(_json['result']['findings']['finding']['match-item']['@node_ref'])
    #             else:
    #                 for i in _json['result']['findings']['finding']:
    #                     ids.append(i['match-item']['@node_ref'])

    #     return ids

    # def __get_session_id(self):
    #     headers = {
    #         "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    #         "accept-encoding": "gzip, deflate",
    #         "accept-language": "en-US,en;q=0.9",
    #         "cache-control": "max-age=0",
    #         "upgrade-insecure-requests": "1",
    #         "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"
    #     }

    #     url = 'https://www.notebooksbilliger.de/'
    #     session_id = 'r689fbvnij79bqe71fl1ub9ciqbm9ig3n1d7gbuoe01al066o0e4a0cl20f5gh3667h4de74fsmvboali4eq4pqmeu5adk1qu6igsbh'

    #     res = self.requester.get(url, headers=headers)

    #     if res.status_code in [200, 201]:
    #         soup = BeautifulSoup(res.text, 'lxml')

    #         # tag = soup.find('script', text=re.compile('.*epoq_search_sessionId.*'))
    #         tag = soup.find('script') # quick patch, improve later
    #         if tag:
    #             rgx = re.search('.*epoq_search_sessionId\s=\s"(.*)";', tag.text.strip())

    #             if rgx:
    #                 session_id = rgx.group(1)

    #     return session_id

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tags = soup.select('.js-ado-product-click') or soup.select('div.mouseover.clearfix') or soup.select('div.product-card.js-process-cards') or soup.select('#topProducts-items div.card__daily-offer')

        if len(tags) > 0:
            return True
        else:
            tags = soup.select('.product-listing__more-link.js-product-pagination-link')
            for tag in tags:
                if tag and tag.has_attr('href'):
                    return True

        return False

    ################# DO NOT DELETE BEYOND THIS LINE ######################
    # def get_urls(self, search_string):
    #     urls = []
    #     cookies = None
    #     current_time = str(int(time.time()))
    #     q_searchstring = search_string.replace(" ", "+")
    #     q_searchstring1 = search_string.replace(" ", "%20")

    #     url = "https://www.notebooksbilliger.de/produkte/" + q_searchstring
    #     # Test initial url
    #     headers = {
    #         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36 Avast/69.0.792.82"
    #     }
    #     res_test = self.requester.get(url, headers=headers, timeout=1000)
    #     cookies = res_test.cookies
    #     print('1 ', url)
    #     if url == unquote(res_test.url):
            
    #         init_soup = BeautifulSoup(res_test.content, "lxml")

    #         get_sessionid = init_soup.find("script", text = re.compile(".*epoq_search_sessionId.*"), attrs = {"type":"text/javascript"})
            
    #         if get_sessionid:
    #             session_id = re.search('sessionId.*=.*"(.*?)";', get_sessionid.get_text().strip())
    #             jquery = "jQuery18205453395026624426_" + str(current_time)

    #             find_product_id_url = "https://search.epoq.de/inbound-servletapi/getSearchResult?tenantId=notebooksbilliger&format=json&query=" + q_searchstring1 + "&sessionId=" + session_id.group(1) + "&orderBy=&order=desc&locakey=&style=onlyId&full&callback=" + jquery + "&&limit=100&offset=0&_=" + str(int(current_time) + 1)

    #             get_prodid_res = self.requester.get(find_product_id_url, headers=headers, cookies=cookies, timeout=1000)
    #             print('2 ', find_product_id_url)
    #             if get_prodid_res.status_code in [200, 201]:
    #                 get_prodid =  str(get_prodid_res.text.encode('UTF-8'))

    #             product_id_result = re.search(jquery + "\((.*)\);", get_prodid)
    #             product_id_json = json.loads(product_id_result.group(1).replace('\\', '\\\\'))

    #             prod_id_list = []
    #             if 'findings' not in product_id_json['result'].keys():
    #                 params = {
    #                     "tenantId": "notebooksbilliger",
    #                     "format": "json",
    #                     "query": "*",
    #                     "sessionId": session_id,
    #                     "orderBy": "",
    #                     "order": "desc",
    #                     "locakey": "",
    #                     "style": "onlyId",
    #                     "full": "",
    #                     "callback": jquery,
    #                     "limit": "100",
    #                     "offset": "0",
    #                     "_": "1612938318761"
    #                 }
    #                 find_product_id_url = 'https://search.epoq.de/inbound-servletapi/getSearchResult?{}'.format(urlencode(params))
    #                 get_prodid_res = self.requester.get(find_product_id_url, headers=headers, cookies=cookies, timeout=1000)
    #                 if get_prodid_res.status_code in [200, 201]:
    #                     get_prodid =  str(get_prodid_res.text.encode('UTF-8'))  

    #                 product_id_result = re.search(jquery + "\((.*)\);", get_prodid) 
    #                 product_id_json = json.loads(product_id_result.group(1).replace('\\', '\\\\'))                 


    #             findings = product_id_json["result"]["findings"]["finding"]
    #             for finding in findings:
    #                 if len(findings) > 1:
    #                     prod_id_list.append(finding["match-item"]["@node_ref"])
    #                 elif len(findings) <= 1:
    #                     prod_id_list.append(findings["match-item"]["@node_ref"])

    #             getsearchlisting = "https://www.notebooksbilliger.de/extensions/ntbde/getsearchlisting.php?pids=" + ",".join(prod_id_list)
                  
                
    #             # HARD BYPASS
    #             if 'mechanische tastatur' in search_string.lower():
    #                 getsearchlisting = product_id_json['result']['content-matches']['content-match']['url']['$'].replace('\\\\', '')

    #             # HARD BYPASS
    #             if 'kabellose maus' in search_string.lower():
    #                 getsearchlisting = 'https://www.notebooksbilliger.de/extensions/apii/filter.php?filters=on&listing=on&advisor=&box_42613_1651_min=&box_42613_1651_max=&box_42613_1651_slid=&box_42617_1651_min=&box_42617_1651_max=&box_42617_1651_slid=&action=applyFilters&category_id=1651&page=1&perPage=100&sort=popularity&order=desc&availability=alle'
                
    #             # HARD BYPASS
    #             if 'mauspad' in search_string.lower():
    #                 getsearchlisting='https://www.notebooksbilliger.de/zubehoer/maeuse+zubehoer/mauspads'

    #             # HARD BYPASS
    #             if 'chair' in search_string.lower():
    #                 getsearchlisting='https://www.notebooksbilliger.de/zubehoer/gaming+zubehoer/gaming+stuehle'
                    
    #             urls = [getsearchlisting]

    #     else:
    #         urls = ["https://www.notebooksbilliger.de/" + q_searchstring + "/page/1?perPage=50&availability=alle",
    #                 "https://www.notebooksbilliger.de/" + q_searchstring + "/page/2?perPage=50&availability=alle"]
        
    #     print('Done Generating Urls')
    #     return urls