from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from bs4 import BeautifulSoup

class ExellentBeNlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36'
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                print(res.url)
                status_code = res.status_code

                if status_code in [200, 201]:
                    if 'Bedoelde u' not in res.text:
                        if self.hasNextPage(res.text): 
                            raw_data_list.append(res.text)
                        else:
                            raw_data_list.append(res.text)
                            break
                    else:
                        break

                    if res.history: # its a category
                        for x in range(2,4): # expecting 25 max results, 12 per page
                            new_url = f"{res.url}?page={x}" if 'page=' not in res.url else res.url.replace(f'{x-1}',f'{x}') # for res.url that has 'page' in url 
                            res = self.requester.get(new_url, timeout=timeout, headers=headers, cookies=cookies)
                            print(res.url)
                            status_code = res.status_code

                            if status_code in [200, 201]:
                                if 'Bedoelde u' not in res.text:
                                    if self.hasNextPage(res.text): 
                                        raw_data_list.append(res.text)
                                    else:
                                        raw_data_list.append(res.text)
                                        break
                                else:
                                    break
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=2',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=3',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=4',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=5',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=6',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=7',
            f'https://www.exellent.be/nl/p/zoeken?text={q_search_string}&page=8'
        ]

        return urls

    # temporary fix for next page checker; alter or discard this if selectors changes in pdp
    def hasNextPage (self, raw_data):
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            link_tag = soup.select('body > div.container.content-container > div.content > div > div > div > div.load > nav') or soup.select('nav.pager ')
            return True if link_tag else False
        except Exception as e:
            print("Error in dl start -- hasNextPage(). With message: ", e)
            return False