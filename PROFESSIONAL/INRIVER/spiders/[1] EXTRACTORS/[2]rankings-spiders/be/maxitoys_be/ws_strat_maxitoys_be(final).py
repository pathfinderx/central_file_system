import logging
from copy import deepcopy
import json
import re

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class MaxitoysBeWebsiteStrategy(WebsiteStrategy):
	WEBSITE = 'www.maxitoys.be'

	def __init__(self, downloader):
		self.downloader = downloader
		self.logger = logging.getLogger(__name__)

	def execute(self, raw_data, start_rank, limit):
		assert isinstance(start_rank, int)
		assert isinstance(limit, int)

		results = []
		template = get_result_base_template()
		tags = self.get_product_tags(raw_data)

		last_rank = start_rank

		for i in range(0, len(tags)):
			result = deepcopy(template)
			# Assign rank
			result["rank"] = str(last_rank)

			# Extract title
			result["title"]["source"], result["title"]["value"] = self.get_title(tags[i])

			# Extract Brand
			result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[i])

			# Extract URL
			result["url"]["source"], result["url"]["value"] = self.get_url(tags[i])

			results.append(result)

			if last_rank == limit:
				break

			last_rank = last_rank + 1
				
		return results, last_rank

	def get_product_tags(self, raw_data):
		try:
			_json = json.loads(raw_data)
			if _json and 'results' in _json and _json['results'][0] and 'hitsPerPage' in _json['results'][0]:
				return _json['results'][0]['hits']

		except Exception as e:
			raise e("Error in extracting product tags")
			
	def get_title(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and 'libelle' in tag:
				source, value = str(tag), tag['libelle']
				
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value

	def get_brand(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and 'marque' in tag:
				source, value = str(tag), tag['marque']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value

	def get_url(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and '_highlightResult' in tag and 'reference' in tag['_highlightResult'] and 'value' in tag['_highlightResult']['reference']:
				reference_number = tag['_highlightResult']['reference']['value']
				product_url = f'https://www.maxitoys.be/jouet/ref-{reference_number}.htm'
				source, value = str(tag), product_url

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value