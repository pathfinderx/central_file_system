import json, urllib
import re

from bs4 import BeautifulSoup
from urllib.parse import quote

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
	URLPreparationFailureException

class MaxitoysBeDownloadStrategy(DownloadStrategy):
	def __init__(self, requester):
		self.requester = requester
		self._search_string = None

	def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(search_string, str)
		assert isinstance(timeout, int)
		self._search_string = search_string
		raw_data_list = []

		if not isinstance(headers, dict):
			headers = {
				'Accept': '*/*',
				'Accept-Encoding': 'gzip, deflate, br',
				'Accept-Language': 'en-US,en;q=0.9,fil;q=0.8,es;q=0.7',
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
			}

		if not isinstance(cookies, dict):
			cookies = None

		try:
			urls = self.get_urls(search_string)
		except:
			raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

		for i in range(len(urls)):
			try: 
				modified_url = self.get_modified_url(urls[i])

				form_data = {"requests":[{"indexName":"BE_PROD_Search","params":f"page=0&highlightPreTag=__ais-highlight__&highlightPostTag=__%2Fais-highlight__&clickAnalytics=true&userToken=anonymous-50caed16-5c59-46d1-8ed6-a38e3026711c&query={search_string}&filters=codeMag%3A%20WEB&maxValuesPerFacet=20&facets=%5B%22prix%22%2C%22marque%22%2C%22ages%22%2C%22licence%22%2C%22estDispoWeb%22%2C%22categories.lvl0%22%5D&tagFilters=&analyticsTags=%5B%22Desktop%22%5D"}]}
				try:
					res = self.requester.post(modified_url, data=json.dumps(form_data), timeout=timeout, headers=headers, cookies=cookies)
				except Exception as e:
					print(e)

				if res.status_code in [200, 201, 202]:
					raw_data_list.append(res.text)
					
					if not self.should_next_page(res.text):
						break
					
			except PaginationFailureException:
				raise PaginationFailureException(f'Pagination Failed - Page {i+1}')
			except:
				raise DownloadFailureException("Download Failed - Unhandled Exception")

		return raw_data_list

	def get_urls(self, search_string):
		urls = []
		search_string = search_string.replace(" ", "+").lower()

		urls = [
			f"https://www.maxitoys.be/?search={search_string}",
			f"https://www.maxitoys.be/?search={search_string}&page=2"
		]

		return urls

	def should_next_page(self, raw_data):
		_json = json.loads(raw_data)
		if _json and 'results' in _json and _json['results'][0] and 'hitsPerPage' in _json['results'][0]:
			if len(_json['results'][0]['hits']) >= _json['results'][0]['hitsPerPage']:
				return True

	def get_modified_url (self, url):
		res = self.requester.get(url)

		if res.status_code in [200,201]:
			soup = BeautifulSoup(res.text, 'lxml')
			tag = soup.find('script', text = re.compile('algoliaSearchAppId'))
			query_string_params = {'x-algolia-agent': 'Algolia for JavaScript (4.9.1); Browser (lite); instantsearch.js (4.23.0); Vue (2.6.12); Vue InstantSearch (3.7.0); JS Helper (3.4.4)'}
			
			if tag:
				tag = str(tag)
				keys = tag[tag.find('algoliaSearchAppId'):tag.find('var env')].split('"')
				searchAppId, searchKey  = keys[1], keys[-2]
				url = f'https://{searchAppId}-1.algolianet.com/1/indexes/*/queries?x-algolia-agent={query_string_params["x-algolia-agent"]}&x-algolia-api-key={searchKey}&x-algolia-application-id={searchAppId}'
		return url