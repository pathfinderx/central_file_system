import logging
from copy import deepcopy
import json
import re

import bs4
from bs4 import BeautifulSoup, Tag

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class PlayeroneBeWebsiteStrategy(WebsiteStrategy):
	WEBSITE = 'www.playerone.be'

	def __init__(self, downloader):
		self.downloader = downloader
		self.logger = logging.getLogger(__name__)

	def execute(self, raw_data, start_rank, limit):
		assert isinstance(start_rank, int)
		assert isinstance(limit, int)

		results = []
		template = get_result_base_template()
		tags = self.get_product_tags(raw_data)

		last_rank = start_rank

		for i in range(0, len(tags)):
			result = deepcopy(template)
			# Assign rank
			result["rank"] = str(last_rank)

			# Extract title
			result["title"]["source"], result["title"]["value"] = self.get_title(tags[i])

			# Extract Brand
			result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[i])

			# Extract URL
			result["url"]["source"], result["url"]["value"] = self.get_url(tags[i])

			results.append(result)

			if last_rank == limit:
				break

			last_rank = last_rank + 1
				
		return results, last_rank

	def get_product_tags(self, raw_data):
		try:
			soup = BeautifulSoup(raw_data, 'lxml')
			tag = (soup.find_all('div', class_="body"),soup.find('a', class_='overlay-link'))
			return tag

		except Exception as e:
			raise e("Error in extracting product tags")
			
	def get_title(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and tag[0]:
				href = tag[0].select_one('h3 > a')

				source, value = str(tag[0]), href['title']
				
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value

	def get_brand(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and tag[0]:
				brand = tag[0].select('span')
				if brand[0]:
					source, value = str(tag[0]), brand[0].get_text()

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value

	def get_url(self, tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if tag and tag[1]:
				href = tag.select_one('h3 > a')
				if href['href']:
					link = f"https://playerone.be/{href['href']}"
					source, value = tag, link

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value