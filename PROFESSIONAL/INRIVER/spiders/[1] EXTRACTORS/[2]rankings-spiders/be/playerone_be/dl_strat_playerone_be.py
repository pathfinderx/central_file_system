import json, urllib
import re

from bs4 import BeautifulSoup
from urllib.parse import quote

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
	URLPreparationFailureException

class PlayeroneBeDownloadStrategy(DownloadStrategy):
	def __init__(self, requester):
		self.requester = requester
		self._search_string = None

	def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
		assert isinstance(search_string, str)
		assert isinstance(timeout, int)
		self._search_string = search_string
		raw_data_list = []

		if not isinstance(headers, dict):
			headers = {
				'accept-language': 'en-US,en;q=0.9,fil;q=0.8,es;q=0.7',
				'cache-control': 'no-cache',
				'cookie': '_ga=GA1.2.1853809337.1634819010; _gid=GA1.2.1650179304.1634819010; PHPSESSID=f7e9e5c898fdcd07b7c58fd84eee6499',
				'pragma': 'no-cache',
				'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
				'sec-ch-ua-mobile': '?0',
				'sec-ch-ua-platform': '"Windows"',
				'sec-fetch-dest': 'document',
				'sec-fetch-mode': 'navigate',
				'sec-fetch-site': 'same-origin',
				'sec-fetch-user': '?1',
				'upgrade-insecure-requests': '1',
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36'
			}

		if not isinstance(cookies, dict):
			cookies = None

		try:
			urls = self.get_urls(search_string)
		except:
			raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

		for i in range(len(urls)):
			try: 
				res = self.requester.get(urls[i], timeout=timeout, headers=headers, cookies=cookies)
				if res.status_code in [200, 201, 202]:
					raw_data_list.append(res.text)
					
					if not self.should_next_page(res.text):
						break
					
			except PaginationFailureException:
				raise PaginationFailureException(f'Pagination Failed - Page {i+1}')
			except:
				raise DownloadFailureException("Download Failed - Unhandled Exception")

		return raw_data_list

	def get_urls(self, search_string):
		urls = []
		search_string = search_string.replace(" ", "+").lower()

		urls = [
			f"https://playerone.be/Search?paged=1&RESET=1&keyword={search_string}",
			f'https://playerone.be/ajax/more_result.php?keyword={search_string}&nb_article=25'
		]

		return urls

	def should_next_page(self, raw_data):
		soup = BeautifulSoup(raw_data, 'lxml')
		if raw_data:
			result = soup.find('p', {'id':'AFF_NB_RESULT'})
			if result:
				string = str(result.get_text())
				products_count = int(string[(string.find('on')+3):])
				if products_count >= 25:
					return True 
			