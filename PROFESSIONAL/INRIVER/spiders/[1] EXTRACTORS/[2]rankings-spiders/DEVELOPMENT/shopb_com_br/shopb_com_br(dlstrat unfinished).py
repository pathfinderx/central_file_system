from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

import json

class ShopbComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self._headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9;',
            'cookie': '_ga=GA1.3.838762533.1651652424; _gid=GA1.3.1120804489.1651652424; _gcl_au=1.1.1399748387.1651652424; _fbp=fb.2.1651652426190.316313348; _hjSessionUser_2945039=eyJpZCI6ImQ4YmJmZmUwLTM3Y2UtNWRmOC05NDc5LWQ5ZDM4ZGQ4ZTU0NCIsImNyZWF0ZWQiOjE2NTE2NTI0MjY4NDQsImV4aXN0aW5nIjp0cnVlfQ==; owa_v=cdh%3D%3Ea5719cf8%7C%7C%7Cvid%3D%3E1651652425051527806%7C%7C%7Cfsts%3D%3E1651652425%7C%7C%7Cdsfs%3D%3E2%7C%7C%7Cnps%3D%3E7; _hjSession_2945039=eyJpZCI6IjE4ZDI0YjFjLTMzYjUtNGZmNS1iZDI2LTI5MzY0MjU3N2U1YyIsImNyZWF0ZWQiOjE2NTE3OTg1MjIwMjksImluU2FtcGxlIjpmYWxzZX0=; _hjIncludedInSessionSample=0; _hjAbsoluteSessionInProgress=0; owa_s=cdh%3D%3Ea5719cf8%7C%7C%7Clast_req%3D%3E1651798552%7C%7C%7Csid%3D%3E1651798518693574934%7C%7C%7Cdsps%3D%3E0%7C%7C%7Creferer%3D%3E%28none%29%7C%7C%7Cmedium%3D%3Edirect%7C%7C%7Csource%3D%3E%28none%29%7C%7C%7Csearch_terms%3D%3E%28none%29;',
            'pragma': 'no-cache;',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100";',
            'sec-ch-ua-mobile': '?0;',
            'sec-ch-ua-platform': '"Windows";',
            'sec-fetch-dest': 'document;',
            'sec-fetch-mode': 'navigate;',
            'sec-fetch-site': 'same-origin;',
            'sec-fetch-user': '?1;',
            'upgrade-insecure-requests': '1;',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36;'
        }
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self._headers
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                   raw_data_list.append(res.text)
                else:
                    raise PaginationFailureException()

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    @staticmethod
    def get_urls(search_string):
        search_string = search_string.replace(' ', '+')
        
        urls = [
            f'https://www.shopb.com.br/buscar?q={search_string}',
            f'https://www.shopb.com.br/buscar?q={search_string}&pagina=2'
        ]

        return urls
