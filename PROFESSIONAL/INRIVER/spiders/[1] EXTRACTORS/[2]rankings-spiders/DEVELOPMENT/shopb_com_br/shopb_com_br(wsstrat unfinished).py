import logging
import re
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ShopbComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shopb.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.select_one('div#listagemProdutos')
        if tags:
            product_tags = tags.select('li.span3 > div.listagem-item')
            return product_tags if product_tags else None

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title_tag = tag.select_one('a.produto-sobrepor')
            if title_tag and title_tag.has_attr('title'):
                source, value = str(tag), title_tag.get('title')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value
        print(value)
        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No brand for this website
        return source, value

    def get_url(self, tag):
        baseUrl = 'https://www.shoptime.com.br'
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_tag = tag.select_one('div.info-produto')
            if url_tag:
                a_tag = url_tag.select_one('a.nome-produto.cor-secundaria')
                if a_tag and a_tag.has_attr('href'):
                    source, value = str(tag), a_tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
