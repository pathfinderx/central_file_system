import json
import re
from urllib.parse import unquote, urlencode, quote
from bs4 import BeautifulSoup

from urllib.parse import quote

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
import cloudscraper
from request.unblocker import UnblockerSessionRequests

class CurrysCoUkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker_requester = UnblockerSessionRequests('us')
        self.global_search_string = None # instantiate search string for later use

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        self.global_search_string = search_string

        if not isinstance(headers, dict):
            headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Sec-Fetch-Dest': 'document',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-User': '?1',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.0.12335 Safari/537.36',
                'referer': 'https://www.currys.co.uk/'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            self.redirected_to_category = False
            self.redirected_url = None
            self.api_urls = []
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        self.next_page = None
        for page in range(0, len(urls)):
            try:
                if page == 0:
                    self.next_page = urls[page]

                # res = self.requester_unblocker.get(self.next_page, timeout=timeout)
                res = self.requester.get(self.next_page, timeout=timeout, headers=headers, cookies=cookies)
                print(res.url)
                cookies = res.cookies
                if res.status_code in [200, 201]:
                    if self.redirected_url:
                        if 'meta property="og:type" content="product"' not in res.text:
                            raw_data_list.append(res.text)
                            if not self.__has_next_page(res.text):
                                break
                        else:
                            break
                    else:
                        result = {}
                        result['source'] = urls[0]
                        result['raw'] = res.text
                        raw_data_list.append(json.dumps(result))
                        break

                elif res.status_code in range(400, 499): # status codes under range 400 (bad requests)
                    api_urls = self.get_api_url()

                    try:
                        if api_urls:
                            for url_page in api_urls:
                                res = self.requester.get(url_page, timeout=timeout, headers=headers)
                                if res.status_code in [200, 201]:
                                    raw_data_list.append(res.text) # add new data on as api data
                            break

                    except PaginationFailureException:
                        raise PaginationFailureException('Pagination failed - new api url request failed')

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        _ids = self.__get_ids(search_string)
        # 20 per page

        # Website updated -- data source moved to API
        urls = []
        if _ids and not self.redirected_to_category:
            urls=['https://api.currys.co.uk/smartphone/api/products/'+','.join(_ids)]

        elif self.redirected_to_category: #use redirected url when redirected to category
            for x in range(1, 6):
                urls.append(self.redirected_url)

        # for x in range(1, 6):
        #     urls.append("https://www.currys.co.uk/gbuk/search-keywords/xx_xx_xx_xx_xx/{}/{}_20/relevance-desc/xx-criteria.html".format(search_string, x))

        return urls

    def get_api_url (self):
        search_string = self.global_search_string.replace(' ', '+')
        urls = [
            f'https://api.cdn.dcg-search.com/currys/navigation?do=json&m_results_per_page=20&page=1&q={search_string}&sort=relevance&sp_cs=UTF-8',
            f'https://api.cdn.dcg-search.com/currys/navigation?do=json&m_results_per_page=20&page=2&q={search_string}&sort=relevance&sp_cs=UTF-8'
        ]

        return urls

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        # get next page url
        url_tag = soup.select_one('ul.pagination li a[title="next"]') or soup.select_one('.Paginator__StyledListItem-jxeWFt a[title="next"]') or \
            soup.select_one('.Paginator__StyledListItem-jwXakx.eDHhCu a[title="next"]')
        if url_tag: 
            self.next_page = url_tag.get('href')
            return True
        else:
            self.next_page = None
            return False

    def __get_ids(self, search_string):

        ids = []
        
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "en-US,en;q=0.9",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"
        }            
        params = {
            'do': 'json',
            'm_results_per_page': '100', # get ids for 100 items
            'page': '1',
            'q': search_string,
            'sort': 'relevance',
            'sp_cs': 'UTF-8 '
        }

        url = 'https://api.dcg-search.com/currys/navigation?{}'.format(urlencode(params))

        res = self.requester.get(url, headers=headers)

        if res.status_code in [200, 201]:
            _json = json.loads(res.text)
            if 'resultsets' in _json.keys() and 'default' in _json['resultsets'] and 'results' in _json['resultsets']['default'] and _json['resultsets']['default']['results']:
                for item in _json['resultsets']['default']['results']:
                    if 'id' in item and item['id']:
                        ids.append(item['id'])

            if 'general' in _json.keys() and 'redirect_without_analytics' in _json['general'] and _json['general']['redirect_without_analytics']: # check if keyword is redirected to category
                if 'product' not in _json['general']['redirect_without_analytics']:
                    self.redirected_url = _json['general']['redirect_without_analytics']
                    self.redirected_to_category = True
                
        return ids