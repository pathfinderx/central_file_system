import logging
from copy import deepcopy
import json, re

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup


class CurrysCoUkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.currys.co.uk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        if last_rank > 0 and tags is None:
            return results, last_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])
            
            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        # Website updated -- data source moved to API for normal search

        if 'rel="dns-prefetch"' not in raw_data:
            _json = json.loads(raw_data)
            self.source = _json['source']
            _json_rawdata = json.loads(_json['raw'])
            tags = _json_rawdata['payload']

        else: # Redirected to Category
            soup = BeautifulSoup(raw_data, 'lxml')
            regex = re.compile('.*result-prd.*')
            tags = soup.findAll("article", {"class" : regex})
            
        if tags:
            return tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # source = str(tag)
            if not isinstance(tag,dict):
                tag = tag.find("header", { "class", "productTitle" }).findAll("span")

                if tag:
                    source = str(tag)
                    value = tag[1].get_text().strip()
            else:
                if 'label' and tag.keys():
                    source = self.source
                    value = tag['label']
                elif 'title' in tag.keys():
                    source = self.source
                    value = tag['title']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if not isinstance(tag,dict):
                tag = tag.find("a")

                if tag:
                    source = str(tag)
                    value = tag['href']
            else:
                if 'link' in tag.keys():
                    source = self.source
                    value = tag['link']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # source = str(tag)
            if not isinstance(tag,dict):
                tag = tag.find("header", { "class", "productTitle" }).findAll("span")

                if tag:
                    source = str(tag)
                    value = tag[0].get_text().strip()
            else:
                if 'brand' in tag.keys() and 'label' in tag['brand']:
                    source = self.source
                    value = tag['brand']['label']
                elif 'brand' in tag.keys() and 'title' in tag['brand']:
                    source = self.source
                    value = tag['brand']['title']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
