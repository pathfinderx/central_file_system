import logging
from copy import deepcopy
import json, re

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup


class ArgosCoUkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.argos.co.uk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            # Extract Brand
            # result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1

            

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        tag = soup.select_one('[data-test="product-list"]')
        tags = []
        
        if tag:
            # temp_tags is search result and suggested
            temp_tags = tag.select('[data-test="component-product-card"].ProductCardstyles__Wrapper-gm8lcq-1') or \
                tag.select('div[class="ProductCardstyles__Wrapper-h52kot-1 hRKcWa StyledProductCard-sc-1o1topz-0 fOIrbR"]') or \
                soup.select('div.styles__LazyHydrateCard-sc-1rzb1sn-0.eBfLtA.xs-6--none.sm-4')
                
            for t in temp_tags:
                # filter suggested
                if t.has_attr('data-product-id'):
                    tags.append(t)
                
            # use suggested if nothing found.
            if not tags:  
                tags = temp_tags  
        return tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            _tag = tag.select_one('[data-test="component-product-card-title"]')

            if _tag:
                source, value = str(tag), ' '.join(_tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_prefix = 'https://www.argos.co.uk'
            _tag = tag.select_one('[data-test="component-product-card-title"]')
            
            if _tag.has_attr('href'):
                source = str(_tag)
                if _tag['href'].startswith('https'):
                        value = _tag.get('href')
                else:
                    value = url_prefix + _tag.get('href')
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
    