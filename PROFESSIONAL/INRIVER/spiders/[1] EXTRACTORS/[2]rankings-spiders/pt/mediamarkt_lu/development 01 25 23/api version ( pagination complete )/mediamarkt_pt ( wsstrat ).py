import logging
import json, re
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MediamarktPtWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.pt'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data) -> list:

        try:
            if isinstance(raw_data, dict):
                if (raw_data and
                    'items' in raw_data):

                    return raw_data.get('items')

        except Exception as e:
            print('Error at: wsstrat -> get_product_tags() -> with message: ', e)

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(tag, dict):
                if ('title' in tag):
                    source, value = str(tag), tag['title']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(tag, dict):
                if ('vendor' in tag):
                    source, value = str(tag), tag['vendor']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_html = self.downloader.get_global_html_data()
            soup, collection = None, None
            soup = BeautifulSoup(raw_html, 'lxml') if raw_html else None
            url = None

            if isinstance(tag, dict):
                if (tag and 'handle' in tag):
                    base_url = f"https://{self.__class__.WEBSITE}"
                    source, value = str(tag), f"{base_url}/products/{tag.get('handle')}"  

                if url:
                    source, value = str(tag), url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
