import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from request.unblocker import UnblockerSessionRequests
from bs4 import BeautifulSoup
from typing import Union

class MediamarktPtDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('pt')
        self.initial_data = None # initial html data result storage
        self.isGetRequest = False # used to restrict post request method
        self.next_start_index = 0 # used for api_url(pagination) query parameters
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # for this case: api payload requires initial data from html results
                initial_url = f"https://mediamarkt.pt/pages/search-results-page?q={search_string}"
                res = self.unblocker.get(initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                payload = self.get_payload(page + 1, search_string) if not self.isGetRequest else None

                if payload:
                    res = self.requester.post(urls[page], timeout=timeout, headers=payload, data=None, cookies=cookies)
                else: 
                    url = self.get_api_url(search_string, page+1, self.next_start_index)
                    res = self.requester.get(url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]: 
                    data = self.get_filtered_json_string(res.text)
                    has_next_page = self.has_next_page(data, limit)
                    raw_data_list.append(data)
                    
                    if not has_next_page:
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = []
            url = self.get_api_url(search_string)

            if not self.isGetRequest:
                payload = json.dumps(self.get_payload(1, search_string))
                res = self.requester.get(url, timeout=1000, headers=self.get_headers(), data=payload, cookies={})
            else:
                res = self.requester.get(url, timeout=1000, headers=None, cookies=None)

            if res.status_code in [200,201]:
                data = self.get_filtered_json_string(res.text)
                page_count = self.get_page_count(data, limit)
                
                for page in range(0, page_count):
                    urls.append(url)

            return [url for i in range(0, page_count)]
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    def get_filtered_json_string(self, raw_data) -> dict:
        try:
            _json = None
            _rgx_filters = re.search(r'({\"filters[\s\S]+?)\);', raw_data)
            _rgx2_products = re.search(r'({\"totalItems[\s\S]+?)\);', raw_data)

            if _rgx_filters:
                _json = json.loads(_rgx_filters.group(1))

            elif _rgx2_products:
                _json = json.loads(_rgx2_products.group(1))

            return _json
            
        except Exception as e:
            print('Error at: dlstrat -> get_filtered_json_string() -> with message: ', e)

    @staticmethod
    def get_headers():
        return {
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate",
            "accept-language": "en-US,en;q=0.9",
            "adrum": "isAjax:true",
            "content-type": "application/json",
            "origin": "https://www.fastenal.com",
            "pragma": "no-cache",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36"
        }

    @staticmethod
    def get_payload(page, search_string):
        payload =  {
            "attributeFilters": {},
            "page": str(page),
            "pageSize": "48",
            "query": search_string
        }
        return payload
    
    @staticmethod
    def get_page_count(data, limit) -> int:
        try:
            if (data and
                'totalItems' in data and
                'itemsPerPage' in data):

                number_of_pages = None

                if (isinstance(data.get('totalItems'), int) and 
                    isinstance(data.get('itemsPerPage'), int)):

                    number_of_pages = round(data['totalItems'] / data['itemsPerPage'])

                return number_of_pages if number_of_pages else 1
            return 1

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            if (data and 
                'totalItems' in data and
                'startIndex' in data and
                'currentItemCount' in data):

                if (isinstance(data.get('totalItems'), int) and
                    isinstance(data.get('startIndex'), int) and 
                    isinstance(data.get('currentItemCount'), int)):

                    difference_of_items = data.get('totalItems') - data.get('startIndex')
                    self.next_start_index = data.get('startIndex') + data.get('currentItemCount')

                return True if int(difference_of_items) > 0 else False
            return False

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)

    def get_api_url(self, search_string, page=None, startIndex=None) -> Union[None, str]:
        try:
            api_key = None
            sortBy = 'sales_amount'
            sortOrder = 'desc'
            maxResults = 24
            timeZoneName = 'Asia/Shanghai'
            output = 'jsonp'
            callback = 'jQuery36003217502514904649_1674656056526'

            soup = BeautifulSoup(self.initial_data, 'lxml')

            # extract api_key
            script_tag, _rgx, _json = soup.select('script'), None, None
            for x in script_tag:
                if (str(x).find('asyncLoad') > -1):
                    _rgx = re.search(r'var urls = (\[\"[\s\S]+?\"\])', str(x))

            _json = json.loads(_rgx.group(1)) if _rgx else None

            if (_json and isinstance(_json, list)):
                for x in _json:
                    if (x.find('searchserverapi') > -1):
                        api_key = re.search(r'init\.js\?a=([\s\S]+?)&', x).group(1)

            # api_url query parameters alternator and loader
            if (api_key and
                search_string):

                api_url = None
                self.isGetRequest = True # set to true to restrict post request method

                if (not page or page == 1):
                    api_url = f"https://searchserverapi.com/getresults?api_key={api_key}&q={search_string}&sortBy=sales_amount&sortOrder=desc&startIndex=0&maxResults=24&items=true&pages=true&categories=true&suggestions=true&queryCorrection=true&suggestionsMaxResults=3&pageStartIndex=0&pagesMaxResults=20&categoryStartIndex=0&categoriesMaxResults=20&facets=true&facetsShowUnavailableOptions=false&ResultsTitleStrings=3&ResultsDescriptionStrings=4&timeZoneName=Asia/Shanghai&output=jsonp&callback={callback}"

                elif (page > 1 and startIndex):
                    api_url = f"https://searchserverapi.com/getresults?api_key={api_key}&q={search_string}&sortBy=sales_amount&sortOrder=desc&startIndex={startIndex}&maxResults=24&items=true&pages=true&categories=true&suggestions=true&queryCorrection=true&suggestionsMaxResults=3&pageStartIndex=0&pagesMaxResults=20&categoryStartIndex=0&categoriesMaxResults=20&facets=true&facetsShowUnavailableOptions=false&ResultsTitleStrings=3&ResultsDescriptionStrings=4&page={page}&timeZoneName=Asia/Shanghai&output=jsonp&callback={callback}"

                return api_url
            return None

        except Exception as e:
            print('Error at: dlstrat -> get_api_url -> with message: ', e)