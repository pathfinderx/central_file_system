import json
import re
from urllib.parse import urlencode
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
#from request.unblocker import UnblockerSessionRequests


class GiganttiFiDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.keyword_status = False
        #self.requester = UnblockerSessionRequests('fi')
        
    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                '5436515577786c4d4150': '0.4121920767607463',
                '654152376454375a3450': 'dDwLmyeSXW8YFjSHyaO4KscBN6IxibkVXSuV9MW9mX4QpgGBgJBNgZvWsMqCSy, ab8YG9W6kV5XwdoCIVwvNJTDTx50eqwXMuhT2pv9J9oBeCI6PZL3NAjxEwrfkn',
                '7a634447315a48434c67': 'GoThejbXJX938wTMoNGoThejGoThejWI52IE8wTMoNWI52IE6V0p6w',
                '_ffo': 'aHR0cHM6Ly93d3cuZ2lnYW50dGkuZmk=',
                '_fft': '454086448191660',
                'accept': 'application/json, text/plain, */*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'origin': 'www.gigantti.fi',
                'referer': 'www.gigantti.fi',
                'authority': 'www.gigantti.fi',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = self.get_cookies()

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        #if urls:
        for page in range(0, len(urls)):
            try:
                # if not self.keyword_status:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    results = {
                        'source_url': urls[page],
                        'raw_data': res.text
                    }
                    raw_data_list.append(json.dumps(results))

                    # if "<!DOCTYPE html>" not in res.text:
                        # try:
                        #     new_url = re.search(r"document.location=\'(.*)'", res.text).group(1)

                        #     if "www.gigantti.fi" not in new_url:
                        #         new_url = "https://www.gigantti.fi" + new_url

                        #     new_res = self.requester.get(new_url, timeout=timeout)

                        #     if new_res.status_code in [200, 201]:
                        #         res = new_res

                        # except:
                        #     pass  # Do nothing. Intentional
                    # raw_data_list.append(res.text)
                else:
                    break
                # else:
                #     raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')
        #else:
        #    print('Download failed')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []

        # SITE UPDATED --- DATA MOVED TO API
        # q_search_string = search_string.replace(" ", "+")

        # url_req = "https://www.gigantti.fi/search?SearchTerm=" + q_search_string + "&search=&searchResultTab="
        # headers = {
        #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        #     'Accept-Encoding': 'gzip, deflate, br',
        #     'Accept-Language': 'en-US,en;q=0.8',
        #     'Host': 'www.gigantti.fi',
        #     'Referer': 'https://www.gigantti.fi/',
        #     'User-Agent': 'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko'
        # }
        # cookies = self.get_cookies()
        # res = self.requester.get(url_req, headers=headers, timeout=1000,cookies=cookies)

        # # Redirected. Get new urls
        # if res.status_code in [200, 201]:
        #     soup = BeautifulSoup(res.text, "html.parser")
        #     # self.keyword_status = self.keyword_checker(soup)
        #     try:
        #         match = re.search(r"document.location=('|\")(.+?)('|\")", res.text)

        #         if match:
        #             # Check if "https://www.gigantti.fi" already exists in the url
        #             url_match = re.findall(r"https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+", match.group(2))

        #             if url_match:
        #                 redirect_url = match.group(2)

        #             else:
        #                 redirect_url = "https://www.gigantti.fi" + match.group(2)

        #             res = self.requester.get(redirect_url, headers=headers, timeout=1000)

        #             if res.status_code in [200, 201]:
        #                 soup = BeautifulSoup(res.text, "html.parser")

        #                 try:
        #                     urls = self.find_pages(soup)  # Multiple pages

        #                 except:
        #                     urls.append(redirect_url)  # Single page

        #     except:
        #         # Do nothing. Logs will go here in the future
        #         pass

        #     #if 'www.gigantti.fi/catalog' not in redirect_url:
        #     if len(urls) < 1:
        #         # Default
        #         soup = BeautifulSoup(res.text, "html.parser")

        #         try:
        #             urls = self.find_pages(soup)  # Multiple pages

        #         except:
        #             urls.append(url_req)  # Single page
            #else:
                #if redirected to catalog
            #    urls = None

        query_params = {
            'query': search_string,
            'page': 1,
            'sid': 'T6QUwxlMAPeAR7dT7Z4PzcDG1ZHCLg',
            'hitsPerPage': 100,
            'format': 'json'
        }
        urls=['https://elkjop-prod.fact-finder.de/fact-finder/rest/v4/search/commerce_b2c_OCFIGIG?'+urlencode(query_params)]

        return urls

    @staticmethod
    def find_pages(soup):
        pattern = re.compile(r'endlessScrollUrl|endlessScrollPageCount', re.MULTILINE | re.DOTALL)
        script = soup.find("script", text=pattern)
        json_text = re.search(r"[{]([\s\S]*)[}]", script.text)
        json_data = json.loads(json_text.group())
        url = json_data["endlessScrollUrl"]
        pages = json_data["endlessScrollPageCount"]

        if pages >= 11:
            pages = 10

        urls = []

        for page in range(pages):
            urls.append(url + str(page))

        return urls
    
    @staticmethod
    def keyword_checker(soup):
        tag = soup.find("span", {"class": "box-title"})
        if tag and "Pahoittelut, emme löytäneet hakusanaan" in tag.get_text().strip():
            return True


    def get_cookies(self):
        cookies = None
        url = 'https://www.gigantti.fi'
        headers= {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
        }
        res = self.requester.get(url, timeout=1000, headers=headers)
        cookies = res.cookies

        if res.history:
            cookies.update(res.history[0].cookies)
        
        return cookies