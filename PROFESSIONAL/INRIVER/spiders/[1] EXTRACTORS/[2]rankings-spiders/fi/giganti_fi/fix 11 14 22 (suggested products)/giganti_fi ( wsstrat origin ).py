import logging
from copy import deepcopy
import json
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class GiganttiFiWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.gigantti.fi'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        product_tags = []
        _json = json.loads(raw_data)
        if _json:
            self.source_url = _json['source_url']
            results_json = json.loads(_json['raw_data'])
            if results_json and 'hits' in results_json.keys() and results_json['hits']:
                product_tags = results_json['hits']
        # soup = BeautifulSoup(raw_data, "html.parser")
        # product_tags = soup.select("a.product-image-link")
        return product_tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = self.source_url
            if 'Title' in tag['masterValues']:
                value = tag['masterValues']['Title']

            # source = str(tag)

            # if tag and tag.has_attr('title'):
            #     value = tag.get('title')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = self.source_url
            if 'ProductURL' in tag['masterValues']:
                value = tag['masterValues']['ProductURL']
                
            # source = str(tag)

            # if tag and tag.has_attr('href'):
            #     link = tag.get('href')

            #     if "https://{}".format(self.__class__.WEBSITE) not in link.lower():
            #         value = "https://{website}{link}".format(website=self.__class__.WEBSITE, link=link)

            #     elif self.__class__.WEBSITE in link.lower() and 'http' not in link.lower():
            #         value = 'https://{}'.format(link)

            #     else:
            #         value = link

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
