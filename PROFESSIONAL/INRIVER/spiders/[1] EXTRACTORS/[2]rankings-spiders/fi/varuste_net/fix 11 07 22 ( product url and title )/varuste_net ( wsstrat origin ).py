import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class VarusteNetWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.varuste.net'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break
            
            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        product_tags = soup.select('.grid.rinnan4 .item')
        
        return product_tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            source = str(tag)
            _tag = tag.select_one('.text .rivi_paalinkki a.ajaxlinkki')
                
            if _tag:
                value = ' '.join(_tag.stripped_strings)

            else:
                for _tag in tag.select('.text a.ajaxlinkki'):
                    if _tag.has_attr('class') and _tag.has_attr('href'):
                        if len(_tag['class']) == 1:
                            value = ' '.join(_tag.stripped_strings)
                            break
                            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            tag = tag.select_one('.tuoterivi_tekstit a.ajaxlinkki') or tag.select_one('.ajaxlinkki')

            if tag:
                value = 'https://%s%s' % (self.__class__.WEBSITE, tag.get('href'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
