import json
import re
import urllib
import js2py
from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
import cloudscraper

class GameplanetComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.cookies = None
        self.scraper = cloudscraper.create_scraper()

    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None
        
        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        headers = self.get_headers()
        
        # Bypass JS using cookies
        if not self.cookies:
            self.generate_cookies(urls[0], headers, timeout)
        
        hits_per_page = 72 # based from website
        for page in range(0, len(urls)):
            try:
                # res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=self.cookies)
                res = self.scraper.get(urls[page], timeout=timeout, headers=headers, cookies=self.cookies)

                if res.status_code in [200, 201]:
                    print(res.url)
                    raw_data_list.append(res.text)

                    # for total number of products is not displayed in site
                    page_hits, has_next_page = self.get_page_hits(res.text) 
                    if page_hits < hits_per_page:
                        break
                    elif page_hits == hits_per_page:
                        if not has_next_page:
                            break
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "%20")
        #50 per pages
        # if q_search_string == 'Nintendo%20Pro%20Controller':
        #     urls = [
        #         'https://gameplanet.com/catalogsearch/result/?q=Nintendo%20Pro%20Controller&mode=list&order=popularidad&cat=26'
        #     ]
        # else: 
        #     urls = [
        #         'https://gameplanet.com/catalogsearch/result/?q={}&mostrar_inventario=652,653&mode=list&order=popularidad&cat=26'.format(q_search_string)
        #     ]        
        
        #50 per page
        # urls = [
                  
        #         'https://gameplanet.com/catalogsearch/result/?q={}&mostrar_inventario=652,653&mode=list&order=popularidad&cat=26'.format(q_search_string)
        # ]

        urls = [
            'https://gameplanet.com/?s={}&post_type=product&dgwt_wcas=1'.format(q_search_string)
            ]

        # 24 per page
        # urls = [
        #     "https://gameplanet.com/catalogsearch/result/?q=%s&mode=list&order=popularidad" % urllib.parse.quote(search_string),
        #     "https://gameplanet.com/catalogsearch/result/index/?mode=list&order=popularidad&p=2&q=%s" % urllib.parse.quote_plus(search_string)
        # ]

        return urls
    
    def get_page_hits(self, raw_data):
        hits = 0
        next_page = False
        soup = BeautifulSoup(raw_data, 'lxml')
        tags = soup.select('.custom-catalog-list.catalog-products-new') or soup.select('div.category-products .catalog-products-new .row')
        if tags:
            hits = len(tags)

        tag = soup.select_one('a.next.pager-size')
        if tag:
            next_page = True
        
        return hits, next_page
    
    def generate_cookies(self, url, headers, timeout):
        # Implemented from https://github.com/2020iscancelled/Sucuri-Cloudproxy-Bypass
        cookies = None
        try:
            user_agent_headers = {'user-agent':headers.get('user-agent')}
            res = self.scraper.get(url, timeout=timeout, headers=user_agent_headers)
            soup = BeautifulSoup(res.text, 'lxml')
            tag = soup.find('script')
            
            if tag:
                script = tag.get_text()
                a = script.split("(r)")[0][:-1] + "r=r.replace('document.cookie','var cookie');" # js code (https://prnt.sc/cu_NPw97shfO)
                b = js2py.eval_js(a)
                sucuri_cloudproxy_cookie = js2py.eval_js(b.replace("location.","").replace("reload();",""))

                cookies = {
                    sucuri_cloudproxy_cookie.split("=")[0] : sucuri_cloudproxy_cookie.split("=")[1].replace(";path","")
                }
                
        except Exception as e:
            print('Tolerable error: dlstrat -> generate_cookies() -> with message: ', e)

        self.cookies = cookies if cookies else None

    def get_headers(self):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'cookie': 'sucuri_cloudproxy_uuid_3f578bc4c=2f371e44c0cb9ac64d296f018a9f266f; _gp_geo_lng=-99.12766; _gp_geo_lat=19.42847; _gp_geo_address_short=CDMX%2C%20CP%2006000; _gp_geo_address_long=Talavera%2C%20Rep%C3%BAblica%20de%20El%20Salvador%2C%20Centro%2C%20Cuauht%C3%A9moc%2C%2006000%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX; _gp_geo_pc=06000; _gp_tienda_favorita_id=1; _gp_tienda_favorita_nombre=GP%20Santa%20Fe%20I; _gid=GA1.2.1393845144.1668232964; _gat_gtag_UA_2066378_1=1; _ga_V97PLX1VDX=GS1.1.1668232962.1.1.1668233040.0.0.0; _ga=GA1.2.1475243196.1668232962; _dc_gtm_UA-2066378-1=1',
            'pragma': 'no-cache',
            'referer': 'https://gameplanet.com/',
            'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
        }

        return headers