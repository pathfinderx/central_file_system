import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class GameplanetComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.gameplanet.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1
            
        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        product_tags = soup.select('.custom-catalog-list.catalog-products-new') or soup.select('.category-products .custom-five-col.catalog-products-new.space-responsive') or soup.select('.products .product-small.product')
        return product_tags

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            title_tag = tag.select_one('h2.product-name') or tag.select_one('.product-title')
            if title_tag:
                value = title_tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value
        
        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)

            url_tag = tag.select_one('h2.product-name a') or tag.select_one('.product-title a')
            if url_tag and url_tag.has_attr('href'):
                link = url_tag.get('href')

                if self.__class__.WEBSITE in link.lower() and 'http' not in link.lower():
                    value = 'https://{}'.format(link)
                else:
                    value = link

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            brand_tag = tag.select_one('span.att_plat_contenedor')
            if brand_tag:
                value = " ".join(brand_tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value