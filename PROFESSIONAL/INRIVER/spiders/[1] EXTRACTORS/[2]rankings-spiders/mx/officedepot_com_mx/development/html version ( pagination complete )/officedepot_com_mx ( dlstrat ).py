import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from request.unblocker import UnblockerSessionRequests
from bs4 import BeautifulSoup
from typing import Union

class OfficedepotComMxDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('pt')
        self.initial_url = None # initial request url and used as primary url for pagination
        self.initial_data = None # initial html data result storage
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # for this case: api payload requires initial data from html results
                self.initial_url = f"https://www.officedepot.com.mx/officedepot/en/search/?text={search_string}"
                res = self.requester.get(self.initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]: 
                    raw_data_list.append(res.text)
                    
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = list()
            soup = BeautifulSoup(self.initial_data, 'lxml')
            pagination_tag = soup.select_one('.pagination').select('a')

            if pagination_tag:
                base_url = f"https://www.officedepot.com.mx"
                temp_list = temp_list = [f"{base_url}{x.get('href')}" for x in pagination_tag if x.has_attr('href')]

                if self.initial_url:
                    duplicate_removed_list = [*set(temp_list)] # remove duplicate
                    duplicate_removed_list.sort() # sort to ascending order
                    urls = duplicate_removed_list
                    urls.insert(0, self.initial_url) # insert the primary url to first position

            return urls
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    @staticmethod
    def get_headers():
        return {
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate",
            "accept-language": "en-US,en;q=0.9",
            "adrum": "isAjax:true",
            "content-type": "application/json",
            "origin": "https://www.fastenal.com",
            "pragma": "no-cache",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36"
        }

    @staticmethod
    def get_payload(page, search_string):
        payload =  {
            "attributeFilters": {},
            "page": str(page),
            "pageSize": "48",
            "query": search_string
        }
        return payload
    
    @staticmethod
    def get_page_count(data, limit) -> int:
        try:
            if (data and
                'totalItems' in data and
                'itemsPerPage' in data):

                number_of_pages = None

                if (isinstance(data.get('totalItems'), int) and 
                    isinstance(data.get('itemsPerPage'), int)):

                    number_of_pages = round(data['totalItems'] / data['itemsPerPage'])

                return number_of_pages if number_of_pages else 1
            return 1

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            pass

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)