import logging, json, re
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MagazineluizaComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.magazineluiza.com.br'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.select('li.product a.product-li') or soup.select('li.sc-gpAxqO.cEqyhT a') or soup.select('ul.sc-iktFfs.ekIdSn.sc-iGculD.dqunZI.sc-gmmXAg.ieNBMh li') or soup.select('li.sc-jcUmEK.jibeKN') or \
            soup.select('li.sc-eCVOVf.loRbcV') or soup.select('li.sc-eCFVrV.dvkpWi') or soup.select('li.sc-DWqcv.hlZKLz') or soup.select('li[class="sc-kdneuM cNOGCg"]') or \
                soup.select('.sc-ikJyIC.fvoVSX.sc-hcupDf.dqwCNK.sc-cnHmbd.kNStCA li') or soup.select('li.sc-cQYgkQ.cqCLdW')
        if tags:
            return tags
        else:
            tag = soup.find("script", text=re.compile('.*aggregateRating.*'), attrs= {"type":"application/ld+json"})
            if tag:
                _json = json.loads(str(tag.text))
                if _json and '@graph' in _json:
                    return _json.get('@graph')

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # if json (values could either be json or html tag format)
            if tag and type(tag) == dict:
                if 'name' in tag:
                    source, value = str(tag), tag.get('name').strip()

            # if bs4 Tag or Navigable String
            else: 
                if tag and tag.h3:
                    source, value = str(tag), tag.h3.text.strip()
                else:
                    title_tag = tag.select_one('h2')
                    if title_tag:
                        source,value = str(title_tag),title_tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag and type(tag) == dict:
                if 'brand' in tag:
                    source, value = str(tag), tag.get('brand').strip()
            else:
                if tag and tag.has_attr('data-product'):
                    brand_tag = json.loads(tag['data-product'])
                    if brand_tag and 'brand' in brand_tag:
                        source, value = str(tag), brand_tag['brand']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag and type(tag) == dict:
                if 'offers' in tag and 'url' in tag['offers']:
                    source, value = str(tag), tag['offers']['url'].strip()
            else:
                tag = tag.select_one('a')
                if tag.has_attr('href'):
                    source = str(tag)
                    value = 'https://www.magazineluiza.com.br'+tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
