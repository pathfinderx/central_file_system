from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class NoonComSaudiEnDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;'
                          'q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                              '(KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code
                raw_data = res.text

                if status_code in [200, 201]:
                    for response in res.history:
                        if response.status_code in [302, 301]:
                            raw_data = None
                            break

                    if raw_data:
                        soup = BeautifulSoup(raw_data, 'lxml')

                        if soup.select_one('div.productContainer'):
                            raw_data_list.append(res.text)
                        elif soup.select_one('button[aria-label="Continue Shopping"]'):
                            # No products found
                            continue
                        else:
                            # For unit tests purposes
                            raw_data_list.append(raw_data)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.noon.com/saudi-en/search?limit=100&q={q_search_string}'
        ]

        return urls
