import logging
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class NoonComSaudiEnWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.noon.com/saudi-en/'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup, products = BeautifulSoup(raw_data, "lxml"), list()
        product_list = soup.select("div.productContainer") or soup.select('.productContainer')
        for product in product_list:
            if not product.select_one('.sc-f8165ac8-16.dYszHz'):
                products.append({
                    'product_card': product
                })

        return products

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # class names are always changing.
            # title_tag = data["product_card"].select_one('[data-qa="product-name"]') or data["product_card"].select_one('.sc-e3js0d-10.fyFmgb')
            # if title_tag:
            #     value = str(title_tag.text.strip())
            #     source = str(title_tag)
            # else:

            # find div with title ttribute inside the card a instead of classes
            tag = data['product_card'].select_one('div[title]') 
            if tag and tag.has_attr('title'):
                value = str(tag['title']).strip()
                source = str(tag)
            else:
                tag = data['product_card'].select_one('div span[width]')
                if tag:
                    value = tag.get_text().replace('…','').strip()
                    source = str(tag)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No brand for this website
        return source, value

    def get_url(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_tag = data["product_card"].a
            if url_tag:
                value = "https://www.noon.com" + url_tag['href']
                source = str(url_tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
