from bs4 import BeautifulSoup
import re
import json
import time
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests


class LowesComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('us')
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
                          '*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
                'sec-ch-ua-mobile': '?0',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/89.0.4389.114 Safari/537.36',
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        self.total_count = 0
        current_count = 0
        for page in range(0, len(urls)):
            try:
                res = self.unblocker.get(urls[page], timeout=timeout, headers=None, cookies=cookies)
                status_code = res.status_code

                if 'http-equiv="refresh"' in res.text:
                    soup = BeautifulSoup(res.text,'lxml')
                    tag = soup.select_one('[http-equiv="refresh"]')

                    if tag:
                        rgx = re.search(r'URL=\'(.*)\'',tag['content'])

                        if rgx:
                            url = 'https://www.lowes.com'+rgx.group(1)
                            res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=None)
                            status_code = res.status_code

                            if (status_code in [200, 201] and self.hasRequestUrl(res.text)): # if contains (https://prnt.sc/OAokQ_RjbIwV) -> extract and execute new request
                                url = self.extract_request_url(res.text) # extract request url for new unblocker request

                                if url:
                                    res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=None)
                                    status_code = res.status_code

                print(res.url, status_code)

                if status_code in [200, 201]:
                    if page == 0:
                        self.total_count = self.get_total_count(res.text)

                    raw_data_list.append({
                        'data': res.text,
                        'source': urls[page],
                    })

                    current_count += self.get_current_count(res.text)
                    if current_count >= self.total_count:
                        break
                else:
                    break
                    # raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=24&adjustedNextOffset=24',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=48&adjustedNextOffset=48',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=72&adjustedNextOffset=72',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=96&adjustedNextOffset=96'
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=48&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=72',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=96',
            f'https://www.lowes.com/search?searchTerm={q_search_string}',
            f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24',
        ]
        if search_string == 'bathroom faucet':
            urls = [
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?searchTerm=bathroom%20faucet',
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?offset=24'
            ]
        if search_string == 'touchless faucet':
            urls = [
                f'https://www.lowes.com/pl/Touchless--Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?refinement=4294594619&searchTerm=touchless%20faucet',
                f'https://www.lowes.com/pl/Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?offset=24'
            ]

        if search_string.lower() in ['rpz backflow preventer']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}'
            ]

        if search_string.lower() in ['pressure reducing valve 3/4', 'metering faucet']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
                f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            ]

        return urls

    def get_total_count(self,raw_data):
        count = 0
        soup = BeautifulSoup(raw_data,'lxml')
        tag = soup.find('script',text = re.compile('__PRELOADED_STATE__'))
        if tag:
            _json = json.loads(tag.text.split("window['__PRELOADED_STATE__'] = ")[1])
            if _json and 'itemCount' in _json.keys():
                count = int(_json['itemCount'])
        return count

    def get_current_count(self, raw_data):
        count = 0

        soup = BeautifulSoup(raw_data, "lxml")

        tags = soup.select('script[charset="UTF-8"]')
        tag = [tag.string for tag in tags if '__PRELOADED_STATE__' in tag.string] if tags else None

        if tag:
            data_str = tag[0][tag[0].find('{'):]
            data = json.loads(data_str)
            if 'newItemList' in data:
                count = len(data['itemList']) + len(data['newItemList'])
            else:
                count = len(data['itemList'])
        return count

    def hasRequestUrl (self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        _hasRequestUrl = False # protected var to make distinction for hasRequestUrl() func

        try:
            for x in soup.select('meta'):
                if (isinstance(x.get('content'), str) and 
                    x.get('content').find('search') > 0 and 
                    not x.get('property')): # find the request url (https://prnt.sc/OAokQ_RjbIwV)

                    _hasRequestUrl = True # return True
        
            return _hasRequestUrl

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)

    def extract_request_url (self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        baseUrl = 'https://www.lowes.com'
        url_content = None

        try :
            for x in soup.select('meta'):
                if (x.get('content') and re.search(r"URL='([\s\S]+)", x.get('content'))):
                    url_content = (re.search(r"URL='([\s\S]+)", x.get('content')).group(1))

            if url_content:
                request_url = f'{baseUrl}{url_content}' if (baseUrl and url_content) else None

            return request_url

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)