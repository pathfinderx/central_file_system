from bs4 import BeautifulSoup
import re
import json
import time
from random import randint
import random
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests
import cloudscraper	


class LowesComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('us')
        self.scraper = cloudscraper.create_scraper()
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            # headers = {
            #     'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
            #               '*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            #     'accept-encoding': 'gzip, deflate, br',
            #     'accept-language': 'en-US,en;q=0.9',
            #     'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
            #     'sec-ch-ua-mobile': '?0',
            #     'sec-fetch-dest': 'document',
            #     'sec-fetch-mode': 'navigate',
            #     'sec-fetch-site': 'none',
            #     'sec-fetch-user': '?1',
            #     'upgrade-insecure-requests': '1',
            #     'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) '
            #                   'Chrome/89.0.4389.114 Safari/537.36',
            # }
            user_agents = [
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 
                'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
            ]
           
            # headers = {
            #     'accept': 'application/json, text/plain, */*',
            #     'accept-encoding': 'gzip, deflate, br',
            #     'accept-language': 'en-US,en;q=0.9',
            #     'origin': 'https://www.lowes.com/',
            #     'referer': 'https://www.lowes.com/pd/Zurn-1-2-NR3XL-with-DUC/5002185907',
            #     'sec-ch-ua-mobile': '?0',
            #     'sec-ch-ua-platform': '"Windows"',
            #     'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            #     'sec-fetch-dest': 'empty',
            #     'sec-fetch-mode': 'cors',
            #     'sec-fetch-site': 'same-origin',
            #     'user-agent': random.choice(user_agents),
            #     "x-unblock-zipcode": "90011"
            # }
            headers ={
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-encoding": "gzip, deflate, br",
                "accept-language": "en-US,en;q=0.9",
                "referer": "https://www.lowes.com/",
                "sec-ch-ua": "\"Not_A Brand\";v=\"99\", \"Google Chrome\";v=\"109\", \"Chromium\";v=\"109\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\"",
                "sec-fetch-dest": "iframe",
                "sec-fetch-mode": "navigate",
                "sec-fetch-site": "cross-site",
                "upgrade-insecure-requests": "1",
                # "user-agent": random.choice(user_agents),
                "user-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
                "x-unblock-zipcode": "90011"
            }
       
        if not isinstance(cookies, dict):
            cookies = self.get_cookies(headers)
            if cookies:
                print('Fetched cookies')



        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        self.total_count = 0
        current_count = 0
        for page in range(0, len(urls)):
            try:
                res = self.unblocker.get(urls[page], timeout=10000, headers=None, cookies=None)
                status_code = res.status_code
                rgx = None

                if 'http-equiv="refresh"' in res.text:
                    soup = BeautifulSoup(res.text,'lxml')
                    tag = soup.select_one('[http-equiv="refresh"]')

                    if tag:
                        rgx = re.search(r'URL=\'(.*)\'',tag['content'])

                if rgx:
                    url = 'https://www.lowes.com'+rgx.group(1)
                    res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
                    status_code = res.status_code

                if (status_code in [200, 201] and 
                    self.hasRequestUrl(res.text)): # if contains (https://prnt.sc/OAokQ_RjbIwV) -> extract and execute new request
                    
                    url = self.extract_request_url(res.text) # extract request url for new unblocker request

                    if url:
                        res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
                        status_code = res.status_code

                print(res.url, status_code)

                if status_code in [200, 201]:
                    if page == 0:
                        self.total_count = self.get_total_count(res.text)

                    raw_data_list.append({
                        'data': res.text,
                        'source': urls[page],
                    })

                    current_count += self.get_current_count(res.text)
                    if current_count >= self.total_count:
                        break
                else:
                    break
                    # raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=24&adjustedNextOffset=24',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=48&adjustedNextOffset=48',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=72&adjustedNextOffset=72',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=96&adjustedNextOffset=96'
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=48&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=72',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=96',
            f'https://www.lowes.com/search?searchTerm={q_search_string}',
            f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24',
        ]
        if search_string == 'bathroom faucet':
            urls = [
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?searchTerm=bathroom%20faucet',
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?offset=24'
            ]
        if search_string == 'touchless faucet':
            urls = [
                f'https://www.lowes.com/pl/Touchless--Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?refinement=4294594619&searchTerm=touchless%20faucet',
                f'https://www.lowes.com/pl/Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?offset=24'
            ]

        if search_string.lower() in ['rpz backflow preventer']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}'
            ]

        if search_string.lower() in ['pressure reducing valve 3/4', 'metering faucet', 'caulk', 'door caulk']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
                f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            ]

        return urls

    def get_total_count(self,raw_data):
        count = 0
        soup = BeautifulSoup(raw_data,'lxml')
        tag = soup.find('script',text = re.compile('__PRELOADED_STATE__'))
        if tag:
            _json = json.loads(tag.text.split("window['__PRELOADED_STATE__'] = ")[1])
            if _json and 'itemCount' in _json.keys():
                count = int(_json['itemCount'])
        return count

    def get_current_count(self, raw_data):
        count = 0

        soup = BeautifulSoup(raw_data, "lxml")

        tags = soup.select('script[charset="UTF-8"]')
        tag = [tag.string for tag in tags if '__PRELOADED_STATE__' in tag.string] if tags else None

        if tag:
            data_str = tag[0][tag[0].find('{'):]
            data = json.loads(data_str)
            if 'newItemList' in data:
                count = len(data['itemList']) + len(data['newItemList'])
            else:
                count = len(data['itemList'])
        return count

    def hasRequestUrl (self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        try:
            for x in soup.select('meta'):
                if (isinstance(x.get('content'), str) and 
                    x.get('content').find('search') > 0 and 
                    not x.get('property')): # find the request url (https://prnt.sc/OAokQ_RjbIwV)

                    return True
            return False

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)

    def extract_request_url (self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        baseUrl = 'https://www.lowes.com'
        url_content = None

        try :
            for x in soup.select('meta'):
                if (x.get('content') and re.search(r"URL='([\s\S]+)", x.get('content'))):
                    url_content = (re.search(r"URL='([\s\S]+)", x.get('content')).group(1))

            if url_content:
                request_url = f'{baseUrl}{url_content}' if (baseUrl and url_content) else None

            return request_url

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)

    def get_cookies(self,headers):
        cookies = {}
        
        api_url = 'https://www.lowes.com/pd/5002185907/productdetail/2714/Guest/90011?nearByStore=2714'

        
        res_cookies = self._get_initial_cookies()
        res = self.unblocker.get(api_url, timeout=10000, headers=headers, cookies=res_cookies)
        if res.status_code in[200,201]:
            cookies = res.cookies.get_dict()
            cookies.update(res_cookies)

        return cookies

    def _get_initial_cookies(self):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
        }
        cookie = {}

        response = self.requester.get('https://www.lowes.com/', headers=headers, timeout=1000)

        if response.status_code in [200, 201]:
            cookie = response.cookies.get_dict()
        else:
            print(f'Failed to get initial cookies: {response.reason}; Setting manually')
            cookie = {
                'nearbyid': '2714',
                'region': 'central',
                'sn': '2714',	
                'zipcode': '90011',
                'zipstate': 'CA'
            }
            self.unblocker.session.cookies.update(cookie)
        return cookie