from bs4 import BeautifulSoup
import re
import json
import time
from random import randint
import random
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests
import cloudscraper 


class LowesComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('us')
        self.scraper = cloudscraper.create_scraper()
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_initial_headers()
            user_agents = [
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 
                'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
            ]
           
        if not isinstance(cookies, dict):
            cookies = self.get_cookies(headers)
            if cookies:
                print('Fetched cookies')

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        self.total_count = 0
        current_count = 0
        for page in range(0, len(urls)):
            try:
                res = self.unblocker.get(urls[page], timeout=10000, headers=headers, cookies=None)
                status_code = res.status_code
                rgx = None

                if 'http-equiv="refresh"' in res.text:
                    soup = BeautifulSoup(res.text,'lxml')
                    tag = soup.select_one('[http-equiv="refresh"]')

                    rgx = re.search(r'URL=\'(.*)\'',tag['content']) if tag else None

                if (rgx):
                    url = 'https://www.lowes.com'+rgx.group(1)
                    res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
                    status_code = res.status_code

                # if unblocker request is successful, extract new request url from raw_data using unblocker
                if (status_code in [200, 201] and self.hasRequestUrl(res.text)): # if contains (https://prnt.sc/OAokQ_RjbIwV) -> extract and execute new request
                    url = self.extract_request_url(res.text) # extract request url for new unblocker request

                    if (url):
                        res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=cookies)
                        status_code = res.status_code

                # if unblocker requests are blocked, request data using cloudscraper
                if (status_code not in [200, 201]):
                    print('All unblocker requests are blocked, request data using cloudscraper')
                    res = self.scraper.get(urls[page], timeout=timeout, headers=None, proxies=self.requester.session.proxies)
                    url = self.extract_request_url(res.text) # extract request url for new unblocker request

                    res = self.scraper.get(url, timeout=timeout, headers=None, proxies=self.requester.session.proxies)
                    status_code = res.status_code

                if status_code in [200, 201]:
                    if page == 0:
                        self.total_count = self.get_total_count(res.text)

                    raw_data_list.append({
                        'data': res.text,
                        'source': urls[page],
                    })

                    current_count += self.get_current_count(res.text)
                    if current_count >= self.total_count:
                        break

                else:
                    break
                    # raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=24&adjustedNextOffset=24',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=48&adjustedNextOffset=48',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=72&adjustedNextOffset=72',
            # f'https://www.lowes.com/search/products?searchTerm={q_search_string}&offset=96&adjustedNextOffset=96'
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=48&view=List',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=72',
            # f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=96',
            f'https://www.lowes.com/search?searchTerm={q_search_string}',
            f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24',
        ]
        if search_string == 'bathroom faucet':
            urls = [
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?searchTerm=bathroom%20faucet',
                f'https://www.lowes.com/pl/Bathroom-sink-faucets-Bathroom-faucets-shower-heads-Bathroom/1879636657?offset=24'
            ]
        if search_string == 'touchless faucet':
            urls = [
                f'https://www.lowes.com/pl/Touchless--Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?refinement=4294594619&searchTerm=touchless%20faucet',
                f'https://www.lowes.com/pl/Kitchen-faucets-Kitchen-faucets-water-dispensers-Kitchen/4294735696?offset=24'
            ]

        if search_string.lower() in ['rpz backflow preventer']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}'
            ]

        if search_string.lower() in ['pressure reducing valve 3/4', 'metering faucet', 'caulk', 'door caulk']:
            urls = [
                f'https://www.lowes.com/search?searchTerm={q_search_string}&view=List',
                f'https://www.lowes.com/search?searchTerm={q_search_string}&offset=24&view=List',
            ]

        return urls

    def get_total_count(self,raw_data):
        count = 0
        soup = BeautifulSoup(raw_data,'lxml')
        tag = soup.find('script',text = re.compile('__PRELOADED_STATE__'))
        if tag:
            _json = json.loads(tag.text.split("window['__PRELOADED_STATE__'] = ")[1])
            if _json and 'itemCount' in _json.keys():
                count = int(_json['itemCount'])
        return count

    def get_current_count(self, raw_data):
        count = 0

        soup = BeautifulSoup(raw_data, "lxml")

        tags = soup.select('script[charset="UTF-8"]')
        tag = [tag.string for tag in tags if '__PRELOADED_STATE__' in tag.string] if tags else None

        if tag:
            data_str = tag[0][tag[0].find('{'):]
            data = json.loads(data_str)
            if 'newItemList' in data:
                count = len(data['itemList']) + len(data['newItemList'])
            else:
                count = len(data['itemList'])
        return count

    def hasRequestUrl (self, raw_data) -> bool:
        soup = BeautifulSoup(raw_data, 'lxml')

        try:
            for x in soup.select('meta'):
                if (isinstance(x.get('content'), str) and 
                    x.get('content').find('search') > 0 and 
                    not x.get('property')): # find the request url (https://prnt.sc/OAokQ_RjbIwV)

                    return True
            return False

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)

    def extract_request_url (self, raw_data) -> str:
        soup = BeautifulSoup(raw_data, 'lxml')
        baseUrl = 'https://www.lowes.com'
        url_content = None

        try :
            for x in soup.select('meta'):
                if (x.get('content') and re.search(r"URL='([\s\S]+)", x.get('content'))):
                    url_content = (re.search(r"URL='([\s\S]+)", x.get('content')).group(1))

            if (url_content):
                request_url = f'{baseUrl}{url_content}' if (baseUrl and url_content) else None

            return request_url

        except Exception as e:
            print('Error at wsstart > extract_request_url() > with message: ', e)

    def get_cookies(self,headers):
        cookies = {}
        api_url = 'https://www.lowes.com/pd/5002185907/productdetail/2714/Guest/90011?nearByStore=2714'

        res_cookies = self._get_initial_cookies()
        res = self.unblocker.get(api_url, timeout=10000, headers=headers, cookies=res_cookies)
        if res.status_code in[200,201]:
            cookies = res.cookies.get_dict()
            cookies.update(res_cookies)

        return cookies

    def _get_initial_cookies(self):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
        }
        cookie = {}

        response = self.requester.get('https://www.lowes.com/', headers=headers, timeout=1000)

        if response.status_code in [200, 201]:
            cookie = response.cookies.get_dict()
        else:
            print(f'Failed to get initial cookies: {response.reason}; Setting manually')
            cookie = {
                'nearbyid': '2714',
                'region': 'central',
                'sn': '2714',   
                'zipcode': '90011',
                'zipstate': 'CA'
            }
            self.unblocker.session.cookies.update(cookie)
        return cookie

    def get_initial_headers(self) -> dict:
        headers = {
            'accept': 'mtext/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'mgzip, deflate, br',
            'accept-language': 'men-US,en;q=0.9',
            'cache-control': 'mno-cache',
            'cookie': 'mdbidv2=abe88980-cb99-4bd2-a22d-d35fcfad53a5; EPID=YWJlODg5ODAtY2I5OS00YmQyLWEyMmQtZDM1ZmNmYWQ1M2E1; AKA_A2=A; region=central; al_sess=FuA4EWsuT07UWryyq/3foGlG4yTLxZ3Cy66TwV2rdA4FxUtPh5v5UctrWvqsyo+Z; bm_sz=4166FF9DD210B9CC81788BFF2170DDC5~YAAQRpUzuD7RWPaFAQAAF53z+xJolhXx2q6JExAyHIWP4Sp7EObilUwloE6oqNh+aWGFfGJbSmDz4+14/gUb4B1nnlTUgjYOskQ4P5XLbqbcWwkjKSwgDdo5Jl06BO5ChS7Q0yLWE2W2y5jx/fYSi6Hnx1lY7BTAgyfa4R1La+7pLm37eunu0D9A+IsyhHSD/n9Kr79bvBqipSRHVBI/HNQDNSzn7LuLknih4CIroby/7zQ2k/IwZ+EBXOjlqI5zAYd+CXzEtFWJnTwetC1TLx8jT8IsEeVLGggIUBuAIi7PGg==~3551792~3158322; _lgsid=1674969328520; sn=0692; zipcode=08854; nearbyid=0692; zipstate=NJ; __gads=ID=beda61083d1b1cfc:T=1674969328:S=ALNI_MYVRS8Xp9hrh49SqnxZF-biy0ejvg; __gpi=UID=0000093740cd546e:T=1674969328:RT=1674969328:S=ALNI_Mb-E6XJ-a6xyWvnvnxwF78ulvIg9Q; AMCVS_5E00123F5245B2780A490D45%40AdobeOrg=1; g_amcv_refreshed=1; AMCV_5E00123F5245B2780A490D45%40AdobeOrg=-1303530583%7CMCIDTS%7C20118%7CMCMID%7C82275094303592304851674112503771774038%7CMCAAMLH-1675574132%7C7%7CMCAAMB-1675574132%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1674976532s%7CNONE%7CvVersion%7C3.3.0; fs_uid=#Q8RZE#4532925643411456:4948975937703936:::#0df24526#/1706505332; _gcl_au=1.1.287511036.1674969341; _tt_enable_cookie=1; _ttp=WmjWOnVDlzg7cgtbNyhg0MV3NBl; REVLIFTER={"w":"0l554347-da15-447f-9091-6e8941024cdc","u":"b50832ab-9a09-44bc-90c6-f097ffbe309a","s":"71e53e25-a181-437b-b142-87b8fa4f0cc9","se":1677561344}; _abck=149E7D05BFB892CACF4055442C480CC5~0~YAAQXJUzuDeHLsWFAQAALunz+wmjFa2y+E1vA5eDfSyfIMMRSOLzRea39W/cPTP+37RwNNREkU327bA4TqdISGtmySKH+lY9dkp2ydxDUsztn9A5OTiINPR4g5xwKS7Y9vEWa6DaTnNs3JrO5o02u8AZekmNF0U26IlEuMvnJrAzpuKkno1p04D2nEmApu1d+z13u6MsNZimvZjis8JmiAZ635GTd4g8lhCuXnDBoZnstoknuOz9ODHAGuEeFW0FipFdL0CZCuugcm81cP0T6JarmsAwTPSNpzHTrpbQ741JdiRw7/pVp6DV+GIkVmOqJbx2tJZl9Fl1+XjMlsrJu6+2D7BMJ68vRW4IncUVTml4KF+AaqgHEzuYt66Mc3bjaqyu7jVE3IAB7XcUdDqrjI9u0E9uIliy/Hfn7u8c2RnyzDLUItEPbm3PQbMPfnv4MIgJPUM79yc=~-1~-1~-1; _fbp=fb.1.1674969347877.1119891720; ak_bmsc=E0B58175A8FF02E900112FBBCAD424F6~000000000000000000000000000000~YAAQRpUzuCHXWPaFAQAAuAv0+xJqg8OGdj8Op7IRO3swDb/83yqJn3rCXZO5KUjBNvheFnqsF5ftleN6HdzOJ767GndB0LOAVYXYQqtIfO0gsJWHoYL9YKfBy+e1epNJhM3wE/Hscjfiri0/yl1c55quPbqMSuANZy8Bi+yF4Rjc9ycfk5eLejTLaRzldIbXyZCwbmNFH0LqJpXGPj2bjT+6pfjMURdGfmJ6EwzOEcKglfUBzb2/iVFKdBOOU5+99Z+9vOhFHkq9RxZ1W/afERqxH9yC9E027Q5LdMcDkw//YNWwYaJtke4fYYsXYWt6qJVvQW45f6idWXVfB2ybFdTVP3mnPCrXJAByG8xZ08uW/pcnclQSFpx73w/znQMNq5fQBBduFyHzzHHV/sg1yaqcbAfTu+EyzSDKw0s1mj+lGTaV/cU8wsK7+2ygFzZcXKeUExWFUp92YigwTzMotuLawUKdmwDF8Dye4wMLFZEWF+WuNEGd2JzOHfoN9oCmz7PwrfRXXSaGbxGQ8BWOcWlDQ259f3s72q1KIWhvesEai8MWtJQVS4IuD3c9TwFJVBO8/6ep2wZnvkT6ILiEYxd4QsbiTipkgU5MOl+Rlg==; akavpau_default=1674969654~id=cef6a2b34606c82aec6246f993983d8f; notice_behavior=implied,eu; audience=DIY; p13n=%7B%22zipCode%22%3A%2208854%22%2C%22storeId%22%3A%220692%22%2C%22state%22%3A%22NJ%22%2C%22audienceList%22%3A%5B%5D%7D; g_previous=%7B%22gpvPageLoadTime%22%3A%220.00%22%2C%22gpvPageScroll%22%3A%2214%7C15%7C0%7C6896%22%2C%22gpvSitesections%22%3A%22hp%2Chp%2Chp%2Chp%22%2C%22gpvSiteId%22%3A%22desktop%22%2C%22gpvPageType%22%3A%22hp%22%7D; akaalb_prod_dual=1675055784~op=PROD_GCP_EAST_CTRL_DFLT:PROD_DEFAULT_CTRL|~rv=20~m=PROD_DEFAULT_CTRL:0|~os=352fb8a62db4e37e16b221fb4cefd635~id=5d3f4535e7666d7edefbaf11a9942fd4; bm_sv=05BF48EDFCD4C6BAA86C032899380020~YAAQRpUzuE7cWPaFAQAAV4X0+xKBB7phiOVHkeFIcpbT5io9p56o6NZkE43Rskd8fYkjE0I49G5JNyvnMIEuc8hNEX+s2W9QN0EXFx3Y3EThEsffKpnfuMELlVL9MYtq/Qoxsnq2EQ2TIoSNfJ/E1LN+pOqlE5G4odxgCVwMAnr3071o0g8wmnls5b0W5/0AX493fVTDtqgHNY8V4ZiEFnEa45NBYn0+wIM/0++lW9IswvA3WKC4Re8dr/lvUt9M~1; prodNumber=2; RT="z=1&dm=lowes.com&si=bbfcbc6a-a152-4341-81d6-73f846345ddc&ss=ldgxhkml&sl=3&tt=uu3&bcn=%2F%2F173bf111.akstat.io%2F&ld=1745&nu=1srdqpwq&cl=1epd&ul=1i5n"',
            'pragma': 'mno-cache',
            'referer': 'mhttps://www.lowes.com/search?searchTerm=paint+caulk',
            'sec-ch-ua': 'm"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': 'm?0',
            'sec-ch-ua-platform': 'm"Windows"',
            'sec-fetch-dest': 'mdocument',
            'sec-fetch-mode': 'mnavigate',
            'sec-fetch-site': 'msame-origin',
            'sec-fetch-user': 'm?1',
            'upgrade-insecure-requests': 'm1',
            'user-agent': 'mMozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        }

        # headers = {
        #     'accept': 'application/json, text/plain, */*',
        #     'accept-encoding': 'gzip, deflate, br',
        #     'accept-language': 'en-US,en;q=0.9',
        #     'origin': 'https://www.lowes.com/',
        #     'referer': 'https://www.lowes.com/pd/Zurn-1-2-NR3XL-with-DUC/5002185907',
        #     'sec-ch-ua-mobile': '?0',
        #     'sec-ch-ua-platform': '"Windows"',
        #     'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
        #     'sec-fetch-dest': 'empty',
        #     'sec-fetch-mode': 'cors',
        #     'sec-fetch-site': 'same-origin',
        #     'user-agent': random.choice(user_agents),
        #     "x-unblock-zipcode": "90011"
        # }
    
        # headers ={
        #     "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        #     "accept-encoding": "gzip, deflate, br",
        #     "accept-language": "en-US,en;q=0.9",
        #     "referer": "https://www.lowes.com/",
        #     "sec-ch-ua": "\"Not_A Brand\";v=\"99\", \"Google Chrome\";v=\"109\", \"Chromium\";v=\"109\"",
        #     "sec-ch-ua-mobile": "?0",
        #     "sec-ch-ua-platform": "\"Windows\"",
        #     "sec-fetch-dest": "iframe",
        #     "sec-fetch-mode": "navigate",
        #     "sec-fetch-site": "cross-site",
        #     "upgrade-insecure-requests": "1",
        #     # "user-agent": random.choice(user_agents),
        #     "user-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        #     "x-unblock-zipcode": "90011"
        # }
    
        return headers