import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from bs4 import BeautifulSoup
from typing import Union

class MicrosoftComEnUsDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.initial_url = None # initial request url and used as primary url for pagination
        self.initial_data = None # initial html data result storage
        self.productsPerPage = 0 # current page
        self.totalProductCount = 0 # max result
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # for this case: api payload requires initial data from html results
                q_string = search_string.replace(' ', '+')
                self.initial_url = f"https://www.microsoft.com/en-us/search/shop/devices?q={q_string}"
                res = self.requester.get(self.initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]: 
                    raw_data_list.append(res.text)
                    
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = list()
            soup = BeautifulSoup(self.initial_data, 'lxml')
            pagination_tag, _rgx, _json = soup.select('script'), None, None
            page_count = None

            for x in soup.select('script'):
                if (str(x).find('activePageUrl') > 1):
                    _rgx = re.search(r'Search__=({[\s\S]+})', str(x))

            _json = json.loads(_rgx.group(1)) if _rgx else None

            if _json:
                isPaginationAttributesInitialized = self.set_pagination_attributes(_json)
                if (isPaginationAttributesInitialized):
                    page_count = self.get_page_count(_json, limit)

            if (page_count):
                base_page_count, counter, breaker = self.productsPerPage, 1, False
                viewed_result = self.productsPerPage
                max_result = self.totalProductCount
                
                while not breaker:
                    if (viewed_result >= max_result and counter == 1):
                        urls.append(self.initial_url)

                    elif (viewed_result < max_result):
                        counter += 1
                        viewed_result = base_page_count * counter

                        q_string = search_string.replace(' ', '+')
                        url = f"https://www.microsoft.com/en-us/search/shop/devices?q={q_string}&skip={base_page_count * (counter - 1)}"
                        urls.append(url)

                    elif (viewed_result >= max_result and counter > 1):
                        breaker = True

                    else:
                        urls.append(self.initial_url)

            if urls:
                urls.insert(0, self.initial_url) # insert the primary url to first position

            return urls
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    @staticmethod
    def get_headers():
        pass

    @staticmethod
    def get_payload(page, search_string):
        pass

    def set_pagination_attributes(self, data) -> bool:
        try:
            if (data and
                'limit' in data and
                'shopDepartmentProducts' in data and
                'cards' in data['shopDepartmentProducts'] and
                'totalProductCount' in data['shopDepartmentProducts']['cards']):

                number_of_pages = None

                if (isinstance(data['shopDepartmentProducts']['cards']['totalProductCount'], int) and 
                    isinstance(data.get('limit'), int)):

                    self.productsPerPage = data.get('limit') 
                    self.totalProductCount = data['shopDepartmentProducts']['cards']['totalProductCount']

                return True if (self.productsPerPage and self.totalProductCount) else False
            return False

        except Exception as e:
            print('Error at: dlstrat -> set_pagination_attributes() -> with message: ', e)

    def get_page_count(self, data, limit) -> int:
        try:
            if (self.productsPerPage and self.totalProductCount):
                
                number_of_pages = round(int(self.totalProductCount) / int(self.productsPerPage))

                return number_of_pages if number_of_pages else 1
            return 1

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            pass

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)