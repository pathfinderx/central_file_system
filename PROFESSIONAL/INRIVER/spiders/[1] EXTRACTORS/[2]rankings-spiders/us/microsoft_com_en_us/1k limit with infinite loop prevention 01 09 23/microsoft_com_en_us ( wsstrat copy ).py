import logging
import json, re
from copy import deepcopy

from bs4 import BeautifulSoup, Tag

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MicrosoftComEnUsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.microsoft.com/en-us'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data) -> list:
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            product_tag = soup.select_one('#shopDetailsWrapper')

            if product_tag:
                product_list = product_tag.select('ul > li.col.mb-4.px-2')

                return product_list if product_list else None
            return None

        except Exception as e:
            print('Error at: wsstrat -> get_product_tags() -> with message: ', e)

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title_tag = tag.select_one('.base.mb-2.h6 > a')

            if (title_tag and title_tag.has_attr('aria-label')):
                source, value = str(title_tag), title_tag.get('aria-label')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass # no brand found

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title_tag = tag.select_one('.base.mb-2.h6 > a')

            if (title_tag and title_tag.has_attr('href')):
                source, value = str(title_tag), title_tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
