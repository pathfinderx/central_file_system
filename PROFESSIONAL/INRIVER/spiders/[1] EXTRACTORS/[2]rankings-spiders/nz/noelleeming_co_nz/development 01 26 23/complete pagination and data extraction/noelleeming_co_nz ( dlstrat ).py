import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from bs4 import BeautifulSoup
from typing import Union

class NoelleemingCoNzDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.initial_url = None # initial request url and used as primary url for pagination
        self.initial_data = None # initial html data result storage
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # for this case: api payload requires initial data from html results
                self.initial_url = f"https://www.noelleeming.co.nz/search?q={search_string}"
                res = self.requester.get(self.initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]: 
                    raw_data_list.append(res.text)
                    
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = list()
            soup = BeautifulSoup(self.initial_data, 'lxml')
            pagination_tag = soup.select_one('.result-count.col.text-p-small')

            if pagination_tag:
                base_page_count = 32
                counter = 1

                viewed_result = re.search(r'Viewing ([\d]{1,3})', pagination_tag.get_text(strip=True))
                max_result = re.search(r'of ([\d]{1,3})', pagination_tag.get_text(strip=True))
                breaker = False
                
                if (viewed_result and max_result):
                    viewed_result = int(viewed_result.group(1))
                    max_result = int(max_result.group(1))
                    
                    while not breaker:
                        if (viewed_result >= max_result and counter == 1):
                            urls.append(self.initial_url)

                        elif (viewed_result < max_result):
                            counter += 1
                            viewed_result = base_page_count * counter

                            q_string = search_string.replace(' ', '+')
                            url = f"https://www.noelleeming.co.nz/search?q={q_string}&sz={viewed_result}"
                            urls.append(url)

                        elif (viewed_result >= max_result and counter > 1):
                            breaker = True

                        else:
                            urls.append(self.initial_url)
                if urls:
                    urls.insert(0, self.initial_url) # insert the primary url to first position

                    if (len(urls) > 1):
                        urls = [urls[-1]] # if multiple paginations are created, extract only the last url to prevent duplicate when extracting data

            return urls
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    @staticmethod
    def get_headers():
        pass

    @staticmethod
    def get_payload(page, search_string):
        pass
    
    @staticmethod
    def get_page_count(data, limit) -> int:
        try:
            pass

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            pass

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)