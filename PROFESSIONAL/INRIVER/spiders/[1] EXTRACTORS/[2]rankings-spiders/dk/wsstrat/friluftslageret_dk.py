import logging, json
from copy import deepcopy
from bs4 import BeautifulSoup, Tag, NavigableString

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class FriluftslageretDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.friluftslageret.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, "lxml")
        tags = soup.select('div.col-xl-4.col-6.d-flex') or soup.select('div.col-lg-3.col-4.d-flex')

        if tags:
            return tags
        else:
            _json = json.loads(raw_data)
            if _json and 'product_data' in _json:
                return _json['product_data']

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString): # check if tag is an html tag or navigableString; if left unfiltered, the selector will induce an error
                title = tag.select_one('a.product-title')
                if tag and title:
                    source, value = str(tag), title.get_text().strip()
            elif type(tag) == dict: # check if tag is a dictionary
                if tag and 'name' in tag:
                    source, value = str(tag), tag.get('name').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                brand = tag.select_one('p.small.mb-0.text-muted')
                if tag and brand:
                    source, value = str(tag), brand.get_text().strip()
            elif type(tag) == dict:
                if tag and 'brand' in tag:
                    source, value = str(tag), tag.get('brand').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                url_pref = 'https://www.friluftslageret.dk'
                title = tag.select_one('a.product-title')
                if tag and title:
                    source, value = str(tag), url_pref + title['href']
            elif type(tag) == dict:
                if tag and 'url' in tag:
                    source, value = str(tag), tag.get('url').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
