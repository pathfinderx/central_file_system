import logging, json, re
from copy import deepcopy

from bs4 import BeautifulSoup, Tag, NavigableString

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class Sport24DkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.sport24.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

        # global variable to be accessed for recursive action
        self.raw_data = None
        self.start_rank = None
        self.limit = None
        self.isTagCalled = False
        self.breaker = False

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        self.raw_data = raw_data
        self.start_rank = start_rank
        self.limit = limit

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)
        # hits = self.get_hits(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            
            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        tag = soup.select('ol.products.list.items.product-items li.item.product.product-item') or soup.select('.ProductListItem_wrapper__1AnyJ')

        if tag and not self.isTagCalled:
            self.isTagCalled = True # activate this to neglect this line during next call
            return tag
        elif self.isTagCalled and not self.breaker:
            self.breaker = True # activate this to break this line in case if major attributes still have no values
            if raw_data:
                raw_data_start = raw_data.find('<script id="__NEXT_DATA__"')
                obfuscation_begin = raw_data[raw_data_start:]
                raw_data_end = obfuscation_begin.find('</script>')
                obfuscation_final = obfuscation_begin[:raw_data_end].strip()

                if obfuscation_final:
                    soup = BeautifulSoup(obfuscation_final, 'lxml')
                    soup_script = soup.find('script', text = re.compile('props'))
                    
                    if soup:
                        _json = json.loads(str(soup_script.text).strip())
                        if _json and 'props' in _json and \
                            'pageProps' in _json['props'] and \
                            'pageProps' in _json['props']['pageProps'] and \
                            'searchResults' in _json['props']['pageProps']['pageProps'] and \
                            'results' in _json['props']['pageProps']['pageProps']['searchResults'] and \
                            'products' in _json['props']['pageProps']['pageProps']['searchResults']['results'] and \
                            'items' in _json['props']['pageProps']['pageProps']['searchResults']['results']['products']:
                            
                            return _json['props']['pageProps']['pageProps']['searchResults']['results']['products']['items']
        else:
            return None

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                source = str(tag)
                name = tag.select_one('.product.name.product-item-name') or tag.select_one('.ProductListItem_name__3i5NP')
                
                if name:
                    value = ' '.join(name.stripped_strings)

                if value == '':
                    self.execute (self.raw_data, self.start_rank, self.limit)

            elif isinstance (tag, dict):
                if tag and 'name' in tag:
                    source, value = str(tag), tag.get('name')
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        brand = None

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                tag = tag.select_one('.product.brand.product-item-brand > span') or tag.select_one('.ProductListItem_manufacturer__3zGYY')
                
                if tag:
                    source = str(tag)
                    brand = ' '.join(tag.stripped_strings)

                if brand:
                    value = brand
            elif isinstance (tag, dict):
                if tag and 'name' in tag:
                    source, value = str(tag), tag.get('name')
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            tag = tag.select_one('a')
            url_prefix = 'https://%s' % self.__class__.WEBSITE

            if tag and tag.has_attr('href'):
                if url_prefix not in tag['href']:
                    value = url_prefix + tag['href']
                else:
                    value = tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value


        