import logging, json, re
from copy import deepcopy
from bs4 import BeautifulSoup, Tag, NavigableString
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class Sport24DkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.sport24.dk'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)
        # hits = self.get_hits(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            
            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        tag = soup.select('ol.products.list.items.product-items li.item.product.product-item') or soup.select('.ProductListItem_wrapper__1AnyJ')

        try:
            if raw_data:
                if tag and self.value_checker(tag):  # to check if product tags have values otherwise, utilize the json values
                    return tag

                elif not self.value_checker(tag):
                    # as observed, json data vanishes if parsed with BeautifulSoup
                    # so this procedure directly utilizes the raw data from requester; technically string type. Thus, we have to recourse to substring approach
                    raw_data_start = raw_data.find('<script id="__NEXT_DATA__"')
                    obfuscation_begin = raw_data[raw_data_start:]
                    raw_data_end = obfuscation_begin.find('</script>') # indicates first closing tag; 
                    obfuscation_decoded = obfuscation_begin[:raw_data_end].strip()

                    if obfuscation_decoded:
                        soup = BeautifulSoup(obfuscation_decoded, 'lxml')
                        soup_script = soup.find('script', text = re.compile('props'))
                        # Vertical Elstree Skibukser Dame
                        if soup:
                            _json = json.loads(str(soup_script.text).strip())
                            if _json and 'props' in _json and \
                                'pageProps' in _json['props'] and \
                                'pageProps' in _json['props']['pageProps'] and \
                                'searchResults' in _json['props']['pageProps']['pageProps'] and \
                                'results' in _json['props']['pageProps']['pageProps']['searchResults'] and \
                                'products' in _json['props']['pageProps']['pageProps']['searchResults']['results'] and \
                                'items' in _json['props']['pageProps']['pageProps']['searchResults']['results']['products']:
                                
                                return _json['props']['pageProps']['pageProps']['searchResults']['results']['products']['items']

                            elif _json and 'props' in _json and \
                                'apolloState' in _json['props'] and \
                                'ROOT_QUERY' in _json['props']['apolloState']:

                                json_list = list(_json.get('props').get('apolloState').get('ROOT_QUERY').values())
                                product_id_container = list()
                                json_final_value = list()

                                # this procedure extracts the product identifier (to be used as key for _json)
                                for index, val in enumerate(json_list):
                                    if 'ProductList' in str(json_list[index - 1]) and 'items' in str(json_list[index - 1]): # checker if contains 'ProductList' key
                                        for x in json_list[index - 1]['items']:
                                            product_id_container.append(x) # append product 
                                        break

                                # this procedure extracts all the product values using product identifier as key
                                if bool(product_id_container): # check if product_id_container list has value
                                    for x in product_id_container:
                                        if x.get('__ref') in _json['props']['apolloState']:
                                            json_final_value.append(_json.get('props').get('apolloState').get(x.get('__ref')))
                                    if bool(json_final_value):
                                        return json_final_value
                                    else:
                                        return None
                else:
                    print('get_product_tags: No product values to return')
                    return None

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                source = str(tag)
                name = tag.select_one('.product.name.product-item-name') or tag.select_one('.ProductListItem_name__3i5NP')
                
                if name:
                    value = ' '.join(name.stripped_strings)

            elif isinstance (tag, dict):
                if tag and 'name' in tag:
                    source, value = str(tag), tag.get('name').strip()
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        brand = None

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                tag = tag.select_one('.product.brand.product-item-brand > span') or tag.select_one('.ProductListItem_manufacturer__3zGYY')
                
                if tag:
                    source = str(tag)
                    brand = ' '.join(tag.stripped_strings)

                if brand:
                    value = brand

            elif isinstance (tag, dict):
                if tag and 'manufacturer_value' in tag:
                    source, value = str(tag), tag.get('manufacturer_value').strip()
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance (tag, Tag) or isinstance (tag, NavigableString):
                source = str(tag)
                tag = tag.select_one('a')
                url_prefix = 'https://%s' % self.__class__.WEBSITE

                if tag and tag.has_attr('href'):
                    if url_prefix not in tag['href']:
                        value = url_prefix + tag['href']
                    else:
                        value = tag['href']

            elif isinstance (tag, dict):
                if tag and 'slug' in tag:
                    slug = tag.get('slug')
                    uri = f'https://www.sport24.dk/{slug}'
                    source, value = str(tag), uri

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    # product tags value checker
    def value_checker (self, tags):
        for idx in range(0, len(tags)):
            title_value = self.get_title(tags[idx])
            brand_value = self.get_brand(tags[idx])
            url_value = self.get_url(tags[idx])

            return False if title_value[1] == '' or brand_value[1] == '' or url_value[1] == '' else True