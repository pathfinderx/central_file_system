from http import cookies
import json
import time
import re

from bs4 import BeautifulSoup


from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class Sport24DkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.used_urls = []
        self.headers = None

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            self.headers = {
                
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',                           
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=self.headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    if not self.__has_next_page(res.text):
                        raw_data_list.append(res.text)
                        break
                    
                    raw_data_list.append(res.text)
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "+")

        url = 'https://www.sport24.dk/catalogsearch/result/?q=%s' % q_search_string

        redirected_url = self.get_redirected_url(url)
        if redirected_url:
            self._set_headers ()
            page_1 = redirected_url
            if '?' in redirected_url:
                urls = [page_1] + [redirected_url+'&p=%s&is_scroll=1' % i for i in range(2,5)]
            else:
                urls = [page_1] + [redirected_url+'?p=%s&is_scroll=1' % i for i in range(2,5)]
            return urls
        return []


    def get_redirected_url(self, url):
        headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',                           
            }        
        res = self.requester.get(url, headers=headers)
        soup = BeautifulSoup(res.text, 'lxml')

        tag = soup.select_one('.message.notice.notice-margin')
        if tag:
            text = tag.get_text().strip().lower()
            if 'din søgning gav ingen resultater' in text:
                return None
        else:
            tag = soup.select_one('.message.info.empty')
            if tag:
                text = tag.get_text().strip().lower()
                if 'der er ingen produkter' in text:
                    return None
        return res.url

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tag = soup.select('.item.product.product-item')

        if len(tag) < 28:
            return False

        return True

    def _set_headers (self):
        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'max-age=0',
            'cookie': '_gcl_au=1.1.1084869279.1642736719; _fbp=fb.1.1642736719355.1979997652; _gid=GA1.2.1606170422.1642736719; _pin_unauth=dWlkPU5qVXdOemRpTW1ZdE5qWXpaQzAwWVRjeUxXRTVOVFl0TVRFelkyTTVaalEzWXpWag; _clck=14n5bco|1|eyb|0; mage-cache-storage=%7B%7D; 6283rsa=C64C6016-D040-EB12-0F36-2D43DDB7086A; form_key=X6MipBY9qvFAI74t; mage-cache-storage-section-invalidation=%7B%7D; mage-messages=; recently_viewed_product=%7B%7D; recently_viewed_product_previous=%7B%7D; recently_compared_product=%7B%7D; recently_compared_product_previous=%7B%7D; product_data_storage=%7B%7D; _ALGOLIA=anonymous-3292c83a-8a7c-4800-a48e-e0b36e8bb3d0; form_key=X6MipBY9qvFAI74t; X-Magento-Vary=c58cc7336841735bf5ef13185766282824a9d073; PHPSESSID=tq15769c90l8jlrucubu8rvthp; _scid=89c29fc5-178f-4e5c-a030-9f624dbf49db; mage-cache-sessid=true; next-i18next=da; _hjSessionUser_1567303=eyJpZCI6Ijg2NTgyYWYyLWUxNzktNTA3Ni05MDAzLTBlNDA5Nzk1YWFiNiIsImNyZWF0ZWQiOjE2NDI3MzY3MTk0NTMsImV4aXN0aW5nIjp0cnVlfQ==; 6283rsaSession=9C11BDE2-FCF0-BCEB-EB9F-9DB0261C8F9D; _hjIncludedInSessionSample=0; _hjSession_1567303=eyJpZCI6ImU0NmQ0YThkLTk4NGUtNDNhNC05YmY0LTEwZmU4MTRmY2E4NSIsImNyZWF0ZWQiOjE2NDI3NTMyODA2NTMsImluU2FtcGxlIjpmYWxzZX0=; _hjIncludedInPageviewSample=1; _hjAbsoluteSessionInProgress=0; section_data_ids=%7B%22department%22%3A1642736733%2C%22raptor%22%3A1642753282%7D; privateData=%7B%22search%22%3A%22skibukser%20dame%22%7D; _ga=GA1.1.1332527155.1642736719; _uetsid=855e00b07a6c11ecb5d7e11feaf667d6; _uetvid=855e26507a6c11ecb3e3dd9322a45afe; target_group=%7B%2211253%22%3A%7B%22value%22%3A%2211253%22%2C%22label%22%3A%22Kvinder%22%2C%22admin_label%22%3A%22Kvinder%22%2C%22code%22%3A%22kvinder%22%2C%22url%22%3A%22https%3A%2F%2Fwww.sport24.dk%2Fkvinder%22%2C%22admin_store_label%22%3A%22Dame%22%2C%22show_in_menu%22%3A1%2C%22show_in_footer_menu%22%3A1%2C%22show_in_filter%22%3A1%2C%22parent_option_id%22%3Anull%2C%22direct_link%22%3Anull%2C%22gender%22%3A%22women%22%2C%22age_group%22%3A%22adult%22%2C%22is_generic%22%3A0%2C%22attribute_code%22%3A%22target_group%22%7D%7D; lf-campaigns=%7B%2276530%22%3A%7B%22display_count%22%3A1%2C%22last_seen%22%3A1642753294491%7D%2C%2276532%22%3A%7B%22display_count%22%3A1%2C%22last_seen%22%3A1642753320854%7D%7D; _clsk=1ts5kt0|1642753323465|3|1|h.clarity.ms/collect; _ga_K32MEEWNQG=GS1.1.1642753270.5.1.1642753927.60',
            'referer': "https':/'/www.sport24.dk/kvinder/vinter/vinter-beklaedning/skibukser",
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
        }