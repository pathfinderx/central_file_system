import json
import time
import re

from bs4 import BeautifulSoup


from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class Sport24DkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.used_urls = []

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',                           
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    if not self.__has_next_page(res.text):
                        raw_data_list.append(res.text)
                        break
                    
                    raw_data_list.append(res.text)
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "+")

        url = 'https://www.sport24.dk/catalogsearch/result/?q=%s' % q_search_string

        redirected_url = self.get_redirected_url(url)
        if redirected_url:
            page_1 = redirected_url
            if '?' in redirected_url:
                urls = [page_1] + [redirected_url+'&p=%s&is_scroll=1' % i for i in range(2,5)]
            else:
                urls = [page_1] + [redirected_url+'?p=%s&is_scroll=1' % i for i in range(2,5)]
            return urls
        return []


    def get_redirected_url(self, url):
        headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',                           
            }        
        res = self.requester.get(url, headers=headers)
        soup = BeautifulSoup(res.text, 'lxml')

        tag = soup.select_one('.message.notice.notice-margin')
        if tag:
            text = tag.get_text().strip().lower()
            if 'din søgning gav ingen resultater' in text:
                return None
        else:
            tag = soup.select_one('.message.info.empty')
            if tag:
                text = tag.get_text().strip().lower()
                if 'der er ingen produkter' in text:
                    return None
        return res.url

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tag = soup.select('.item.product.product-item')

        if len(tag) < 28:
            return False

        return True