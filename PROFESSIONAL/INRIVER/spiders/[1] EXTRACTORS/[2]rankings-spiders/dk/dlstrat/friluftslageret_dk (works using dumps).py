from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
import urllib, json

class FriluftslageretDkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(self, search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                    raw_data_list.append(res.text)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(self, search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://friluftslageret.dk/search?q={q_search_string}&ps=240&s=relevans&f=W10='
        ]

        if search_string == 'outdoor bukser dame':
            params = self.get_params(self, search_string)
            urls = [
                f'https://api.clerk.io/v2/?payload={params}'
            ]

            # 'https://api.clerk.io/v2/?payload=query=outdoor+bukser+dame&clerk-content-id=2&template=search-page&facets=%5B%27price%27%2C+%27categories%27%2C+%27brand%27%5D&key=mB7hxs4bqFk5NO80jTqCt9luWKXhphlp&visitor=7NjD482W&_=1642150596'
            
        return urls

    @staticmethod
    def get_params (self, search_string):
        payload = {
            "query": f'{search_string}',
            "clerk-content-id":2,
            "template":"search-page",
            "facets":["price","categories","brand"],
            "key":"mB7hxs4bqFk5NO80jTqCt9luWKXhphlp",
            "visitor":"7NjD482W"
        }
        params = json.dumps(payload)
        return params
