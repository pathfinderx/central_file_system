import json
import time
import re

from urllib.parse import quote, urlencode

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class KomplettNoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                # if res.status_code in [200, 201]:
                #     try:
                #         data = json.loads(res.text)
                #         data.update({'url':urls[page]})
                #         raw_data_list.append(json.dumps(data))
                #     except:
                #         self.extra_urls(search_string)
                #         res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    
                    if search_string in ['ANC-hodetelefoner','baerbar','beste hodetelefoner','Ergonomisk stol','Kontorstol','Playstation-kontroller','Ps4-kontroller','PS-kontroller','Spillmus','Spillstol','Spilltastatur','Xbox One X-kontroller','Xbox-kontroller','PULSKLOKKE','SKRITTELLER']:
                        raw_data_list.append(res.text) # expecting a json
                    else:
                        if 'product-list-item' in res.text:
                            _json = res.json()
                            _json['url'] = urls[page]
                            raw_data_list.append(json.dumps(_json))
                        else:
                            suggested_products = self.suggested_products(search_string)
                            if suggested_products:
                                raw_data_list.append(self.suggested_products(search_string))
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        # epoch = str(int(time.time()))
        q_search_string = search_string.replace(" ", "%20")
        
        # urls = [
        #     'https://www.komplett.no/PagesAsync/applyfacets/NextPage?hits=100'\
        #     f'&offset=0&q={quote(search_string)}&seller=Komplett&_={epoch}'
        # ]

        params = {
            'hits': '100',
            'offset': '0',
            'q': search_string
        }
        urls = [
            'https://www.komplett.no/new-search/applyfacets/NextPage?{}'.format(urlencode(params))
        ]
        # temporary fix
        if search_string in ['GPS BIL','DASHCAM']:
            urls = [
            'https://www.komplett.no/search/applyfacets/NextPage?{}'.format(urlencode(params))
            ]
        
        if search_string in ['Kontorstol','ANC-hodetelefoner','Ergonomisk stol','Playstation-kontroller','Ps4-kontroller','PS-kontroller','Spillmus','Spillstol','Spilltastatur','Xbox-kontroller','beste hodetelefoner','Spillhøyttalere','Spillmousemat','Xbox One X-kontroller','baerbar','PULSKLOKKE','SKRITTELLER']:
            urls = ["https://www.komplett.no/search?q=" + search_string.replace(" ", "+")+ "&hits=48"]

        return urls

    def suggested_products(self, search_string, timeout=10, headers=None, cookies=None, data=None):

        url = "https://www.komplett.no/rr/GetPlacementsAndPromos"

        headers = {
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'content-length': '379',
            'content-type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }

        payload = {
            "pageType":"Search",
            "searchPageDataModel":{"searchTerm":search_string,"foundProducts":[]},
            "recsPlacementIds":["history","recs_top","bottom"],
            "promoPlacementIds":[],
            "requestContext":{"showAsNetPrice":False,"storeId":"310","isBeta":False,"isCrm":False,"visitGuid":"e9c8824d-88f8-4cd3-9fe6-162bab8f3c21","isBusinessCustomer":False,"anonymous":True,"showAsNetPrices":False}
        }

        try:
            res = self.requester.post(url, data=json.dumps(payload), headers=headers)

            if res.status_code in [200, 201]:
                return res.text
        except Exception as e:
            print('fetch suggested product fail')

        return None