import json
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class FjellsportNoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.api_key = None
        self.isInvalidJsonContent = False

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')


        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    if self.is_valid(res.text):
                        raw_data_list.append(res.text)

                    elif self.isInvalidJsonContent:
                        break

                    else:
                        suggested, items = self.is_suggested(res.text, search_string)
                        if suggested:
                            raw_data_list.append(items)
                            break
                        else:
                            break
                            # raise PaginationFailureException('Download failed - No data found to be downloaded : Candidate for auto-rerun')
                else:
                    raise PaginationFailureException('Download - Failed - Possible no data in PDP to fetched. Candidate for auto-rerun')

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "+")
        
        urls = [
            f'https://www.fjellsport.no/api/content/sok?q={search_string}&count=48&skip=0&productsOnly=true',
            f'https://www.fjellsport.no/api/content/sok?q={search_string}&count=48&skip=24'
            ]
        # API updated
        # api_key = None
        # api_url = 'https://www.fjellsport.no/'
        # headers = {
        #     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
        #                     'Chrome/57.0.0.12335 Safari/537.36',
        # }
        # res = self.requester.get(api_url, headers=headers, timeout=10)
        # if res.status_code in [200, 201]:
        #     api_key = None
        #     _rgx = re.search(r"klevu_apiKey\=\'klevu\-(.*)\'\,searchTextBoxName", res.text) or re.search(r"klevu_apiKey.*\=.*\'klevu\-(.*)\'\,", res.text)
        #     if _rgx:
        #         api_key = _rgx.group(1)
        #         self.api_key = api_key

        #     if api_key:
        #         urls = [
        #             f"https://nocs.ksearchnet.com/cloud-search/n-search/search?ticket=klevu-{api_key}&term={q_search_string}&paginationStartsFrom=0&sortPrice=false&ipAddress=undefined&analyticsApiKey=klevu-{api_key}&showOutOfStockProducts=false&klevuFetchPopularTerms=false&klevu_priceInterval=500&fetchMinMaxPrice=true&klevu_multiSelectFilters=true&noOfResults=100&klevuSort=rel&enableFilters=true&filterResults=&visibility=search&category=KLEVU_PRODUCT&klevu_filterLimit=50&sv=1218&lsqt=&responseType=json",
        #             # f"https://nocs.ksearchnet.com/cloud-search/n-search/search?ticket=klevu-{api_key}&term={q_search_string}&paginationStartsFrom=30&sortPrice=false&ipAddress=undefined&analyticsApiKey=klevu-{api_key}&showOutOfStockProducts=false&klevuFetchPopularTerms=false&klevu_priceInterval=500&fetchMinMaxPrice=true&klevu_multiSelectFilters=true&noOfResults=30&klevuSort=rel&enableFilters=true&filterResults=&visibility=search&category=KLEVU_PRODUCT&klevu_filterLimit=50&sv=1218&lsqt=&responseType=json",
        #             # f"https://nocs.ksearchnet.com/cloud-search/n-search/search?ticket=klevu-{api_key}&term={q_search_string}&paginationStartsFrom=60&sortPrice=false&ipAddress=undefined&analyticsApiKey=klevu-{api_key}&showOutOfStockProducts=false&klevuFetchPopularTerms=false&klevu_priceInterval=500&fetchMinMaxPrice=true&klevu_multiSelectFilters=true&noOfResults=30&klevuSort=rel&enableFilters=true&filterResults=&visibility=search&category=KLEVU_PRODUCT&klevu_filterLimit=50&sv=1218&lsqt=&responseType=json",
        #             # f"https://nocs.ksearchnet.com/cloud-search/n-search/search?ticket=klevu-{api_key}&term={q_search_string}&paginationStartsFrom=90&sortPrice=false&ipAddress=undefined&analyticsApiKey=klevu-{api_key}&showOutOfStockProducts=false&klevuFetchPopularTerms=false&klevu_priceInterval=500&fetchMinMaxPrice=true&klevu_multiSelectFilters=true&noOfResults=30&klevuSort=rel&enableFilters=true&filterResults=&visibility=search&category=KLEVU_PRODUCT&klevu_filterLimit=50&sv=1218&lsqt=&responseType=json"
        #         ]

        return urls
    
    def is_valid(self, text):
        if text:
            try:
                _json = json.loads(text, strict=False)
                if (isinstance(_json, list) and not _json): # for json convertable contents but empty value (https://prnt.sc/k-ExIBf2FA58)
                    self.isInvalidJsonContent = True
                    return False
                if _json:
                    if isinstance(_json,dict):
                        if 'result' in _json.keys():
                            if len(_json['result']):
                                return True
                        elif 'products' in _json.keys():
                            if len(_json['products']):
                                return True
                    elif isinstance(_json,list):
                        return True
            except:
                pass
                
        
        return False
    
    def is_suggested(self, text, searchstring):
        suggested_url = f"https://nocs.ksearchnet.com/cloud-search/n-search/search?ticket=klevu-{self.api_key}&term={searchstring}&paginationStartsFrom=0&sortPrice=false&ipAddress=undefined"\
            f"&analyticsApiKey=klevu-{self.api_key}&showOutOfStockProducts=false&klevuFetchPopularTerms=false&klevu_priceInterval=500&fetchMinMaxPrice=true&klevu_multiSelectFilters=true&noOfResults=30&klevuSort=rel&enableFilters=true&filterResults=&visibility=search&category=KLEVU_PRODUCT&klevu_filterLimit=50&sv=1218&lsqt=&responseType=json&resultForZero=1&recentCategory=Mountain%20Equipment%3B%3BTykke%20dunjakker%20dame%3B%3BKlatrejakker&klevu_loginCustomerGroup="
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                            'Chrome/57.0.0.12335 Safari/537.36',
        }
        response = self.requester.get(suggested_url, headers=headers, timeout=60)
        status_code = response.status_code
        
        if status_code in [200, 201]:
            _json = json.loads(response.text, strict=False)

            if _json and 'popularProducts' in _json.keys():
                if len(_json['popularProducts']):
                    return True, response.text
                else:
                    return False, None
            else:
                return False, None
        else:
            return False, None
