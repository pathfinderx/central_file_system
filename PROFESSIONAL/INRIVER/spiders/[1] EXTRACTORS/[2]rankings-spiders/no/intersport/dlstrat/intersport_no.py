import json
import re
from urllib.parse import quote
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class IntersportNoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'application/json',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'cross-site',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        for page in range(0, len(urls)):
            try:
                # data = '{"requests":[{"indexName":"prod_default_products","params":"query='+ quote(search_string) +'&hitsPerPage=100&maxValuesPerFacet=10&'\
                #         'page=0&highlightPreTag=__ais-highlight__&highlightPostTag=__%2Fais-highlight__&ruleContexts=%5B%22%22%5D&clickAnalytics=true&' \
                #         'facets=%5B%22brand%22%2C%22categories_without_path%22%2C%22gender_single%22%2C%22price.NOK.default%22%5D&tagFilters=&' \
                #         'numericFilters=%5B%22visibility_search%3D1%22%5D"}]}'

                # res = self.requester.post(urls[page], timeout=timeout, headers=headers, data=data, cookies=cookies)
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:

                    if 'DINE SØKERESULTATER FOR' in res.text: # PDP CHECKER
                        raw_data_list.append(res.text)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        q_search_string = search_string.replace(" ", "+")
        
        # api_key = None
        # api_url = 'https://www.intersport.no/catalogsearch/result/?q=%s' % search_string
        # headers = {
        #     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
        #                     'Chrome/57.0.0.12335 Safari/537.36',
        # }
        # res = self.requester.get(api_url, headers=headers, timeout=10)
        # if res.status_code in [200, 201]:
        #     _rgx = re.search(r"window.algoliaConfig.*=.(\{.*\})\;\<\/script", res.text)
        #     if _rgx:
        #         _json = json.loads(_rgx.group(1))
        #         app_id = _json['applicationId']
        #         app_key = _json['apiKey']

        #         if app_key and app_id:
        #             urls = [
        #                 "https://ik7q714qqx-2.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia for vanilla JavaScript (lite) 3.27.0;instantsearch.js 2.10.2;Magento2 integration (1.10.0);JS Helper 2.26.0&" \
        #                     "x-algolia-application-id=%s&" \
        #                     "x-algolia-api-key=%s" % (app_id, app_key)
        #             ]
        urls = [
            f"https://www.intersport.no/search?q={q_search_string}&lang=nb_NO"
        ]
        res = self.requester.get(urls[0])
        if res:
            soup = BeautifulSoup(res.text, 'lxml')
            tag = soup.select_one('div.search__loadmore--seeall a')
            if tag and tag.has_attr('data-totalpages'):
                page = tag.get('data-totalpages')
                urls = [
                    f"https://www.intersport.no/search?q={q_search_string}&lang=nb_NO&page={int(page)}"
                ]
            
        return urls