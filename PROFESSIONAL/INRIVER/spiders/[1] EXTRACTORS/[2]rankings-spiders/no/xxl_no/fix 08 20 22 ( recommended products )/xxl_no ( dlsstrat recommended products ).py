import json
import time
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class XxlNoDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'application/json, text/plain, */*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'api-version': 'V3',
                'content-type': 'application/json',
                'origin': 'https://www.xxl.no',
                'referer': 'https://www.xxl.no/',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'cross-site',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                            '(KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36',
                'user-id': 'QRtp/EXRkTGUKWgPlsS5pk194hk+yfWLduuflWo23cPGHOI6feI+SmsQxII8anOGh46EmKluK6Cqe4OPiwsC9A=='
            }
        if search_string.lower() == 'regnjakke kvinner':
            headers['user-id'] = 'KxwfEv+U2WMS8TjQ4G59gIwi76kR+SgatBKAn2armKEhQrHOxmc0mSxW7/iCc/llBGpvS1IvEJjNIqsU45yQHw=='
            
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls, data = self.get_urls_api3(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.post(urls[page], data=json.dumps(data), timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    _json = res.json()
                    _json['source_url'] = urls[page]
                    raw_data_list.append(json.dumps(_json))

                    # if _json['results']['count'] > 0:
                    #     _json['source_url'] = urls[page]
                    #     raw_data_list.append(json.dumps(_json))
                    # else:
                    #     data['provider'] = 'solr'
                    #     res = self.requester.post(urls[page], data=json.dumps(data), timeout=timeout, headers=headers, cookies=cookies)
                    #     _json = res.json()
                    #     _json['source_url'] = urls[page]
                    #     raw_data_list.append(json.dumps(_json))
                elif res.status_code in [500]:
                    data['provider'] = 'solr'
                    res = self.requester.post(urls[page], data=json.dumps(data), timeout=timeout, headers=headers, cookies=cookies)
                    _json = res.json()
                    _json['source_url'] = urls[page]
                    raw_data_list.append(json.dumps(_json))
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = ['https://api.xxlsports.com/search-api-v2/sites/xxl/search']

        data = {
            "query": search_string,
            "take": 100,
            "skip": 0,
            "sortBy": [
                {
                    "type": "custom",
                    "customName": "relevance"
                }
            ],
            "facets": [],
            "provider": "loop"
        }
            #11/26 - keyword change provider from solr to loop #Change and Update anytime if issue
        # if search_string in ['Vindjakke menn']: # for keywords with different provider in payload
            # data['provider'] = "solr"
        
        return urls, data

    def get_urls_api3(self, search_string):
        # recommended products (https://prnt.sc/lC_b0hiBP3Al)
        if (search_string.lower() ==  'lettvektsjakke menn'):
            urls = [
                f'https://api.xxlsports.com/recommendations-api-v4/sites/xxl-no/recommendations/personalize'
            ]
            data = [{"numResults":16,"key":"personalized-search-no-hits","strategy":"personalized","sessionId":"c79bcc85-1e5e-4f37-abcf-61539ee0bd16","filters":{}}]

        else:

            urls = ['https://api.xxlsports.com/search-api-v3/sites/xxl/search']

            data = {
                "query": search_string,
                "take": 100,
                "skip": 0,
                "sortBy": [
                    {
                        "type": "custom",
                        "customName": "relevance"
                    }
                ],
                "facets": [],
                "provider": "loop"
            }
            
        return urls, data