import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class XxlNoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.xxl.no'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags, source = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx], source)

             # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], source)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx], source)

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break
            
            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        _json = json.loads(raw_data, strict=False)

        if 'results' in _json:
            if 'items' in _json['results']:
                return _json['results']['items'], _json['source_url']

         # for recommended products (https://prnt.sc/lC_b0hiBP3Al)
        elif ('response' in _json and 
            'source_url' in _json and
            len(_json.get('response')) >= 1 and
            'productList' in _json['response'][0]):

            return _json['response'][0]['productList'], _json['source_url']

        else:
            return [], ''


    def get_title(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = _source

            if 'name' in tag:
                value = tag['name']
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        brand = None

        try:
            source = _source

            if 'brand' in tag:
                if 'name' in tag['brand']:
                    value = tag['brand']['name']
                    
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value
    def get_url(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:


            source = _source

            if 'url' in tag:
                value = 'https://www.xxl.no{}'.format(tag['url'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
