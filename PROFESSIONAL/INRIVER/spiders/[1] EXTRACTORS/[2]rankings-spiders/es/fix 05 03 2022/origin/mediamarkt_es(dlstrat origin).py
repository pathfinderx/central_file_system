import json
import re, time
from urllib.parse import urlencode, quote
from bs4 import BeautifulSoup
import cloudscraper

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests


class MediamarktEsDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.old_requester = requester
        self.requester = UnblockerSessionRequests('es')
        self.scraper = cloudscraper.create_scraper(sess=requester.session)

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):

            headers = {
                "accept": "*/*",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                # "apollographql-client-name": "pwa-client",
                # "apollographql-client-version": "6.120.1",
                # "content-type": "application/json",
                # "referer": "https://www.mediamarkt.es/es/search.html?query={}Gaming&t={}".format(quote(search_string), str(time.time()).replace('.', '')[:13]),
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                "x-cacheable": "true",
                # # "x-flow-id": "fed21f2e-987b-4fa4-b3cd-7c9973d59c53",
                # "x-mms-country": "ES",
                # "x-mms-language": "es",
                # "x-mms-salesline": "Media",
                # "x-operation": "SearchV4"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        
        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers)
                # headers['referer'] = 'https://www.mediamarkt.es/es/search.html?query={}&page={}'.format(quote(search_string), page + 1)
                if res.status_code in [200, 201]:
                    result = self.get_result_from_script(res.text)
                    if result[1]:
                        raw_data_list.append(result[0])
                    else:
                        # FOR SUGGESTED PRODUCTS
                        headers = {
                                "accept": "*/*",
                                "accept-encoding": "gzip, deflate",
                                "accept-language": "en-US,en;q=0.9",
                                "apollographql-client-name": "pwa-client",
                                "apollographql-client-version": "1.73.0",
                                "content-type": "application/json",
                                "referer": urls[page],
                                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                                "x-cacheable": "true",
                                "x-flow-id": "54ffc046-70da-42c5-bd8f-b61a589cf62f", # TODO: please update this incase of failure
                                "x-mms-country": "ES",
                                "x-mms-language": "es",
                                "x-mms-salesline": "Media",
                                "x-operation": "GetProductRecommendations"
                            }
                        params = {}
                        params['operationName'] = 'GetProductRecommendations'
                        variables = {
                            "productsToResolve":6,
                            "boxName":"box01_User2Products",
                            "recommendationContext":"noresult-search",
                            "params":f"&searchterm={quote(search_string)}",
                            "sid":"ef778bd3-c042-4238-976e-0aa81e1b856f",
                            "tracking":True,
                            "manualProducts":[]
                        }
                        extensions = {
                            "pwa":{
                                "salesLine":"Media",
                                "country":"ES",
                                "language":"es"},
                            "persistedQuery":{
                                "version":1,
                                "sha256Hash":"46d82c9ad1d6b19a939b1c9fa1f07afd867ecf6437497d5b468f4c727f024857"} # TODO: please update this incase of failure
                        }
                        params['variables'] = json.dumps(variables, ensure_ascii=False)
                        params['extensions'] = json.dumps(extensions, ensure_ascii=False)
                        url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(params))
                        res = self.requester.get(url, timeout=timeout, headers=None)
                        if res.status_code not in [200,201]:
                            res = self.old_requester.get(url, timeout=timeout, headers=headers)
                        if res.status_code in [200, 201]:
                            _json = res.json()
                            _json['source'] = urls[page]
                            raw_data_list.append(json.dumps(_json, ensure_ascii=False))
                            
                            product_ids = [product['productId'] for product in _json['data']['productRecommendations']['products'] if not product['product']]
                            _params = {}
                            _params['operationName'] = 'GetRecoProductsById'
                            #Temporary fix, order of product id's blocking request.
                            # if search_string == 'motorola e':
                                # product_ids = ["1505482","1436170","1504120","1522890","1505278","1424386","1437732"]
                            # if search_string == 'moto g100':
                                # product_ids = ["1505482","1482804","1265597","1461983","1482761","1508345","1469207","1503335","1436170","1504120","1522890","1424386","1437732","1522200","1486910"]

                            variables = {"ids": product_ids}
                            extensions = {
                                "pwa":{"salesLine":"Media","country":"ES","language":"es"},
                                "persistedQuery":{"version":1,
                                    "sha256Hash":"a63f90335c359e538418cbb646c62ee39821a12087bcc1944a7c6c9debcb3ee8"} # TODO: please update this incase of failure
                                }
                            _params['variables'] = json.dumps(variables, ensure_ascii=False, separators=(',', ':'))
                            _params['extensions'] = json.dumps(extensions, ensure_ascii=False, separators=(',', ':'))
                            url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(_params))
                            res = self.requester.get(url, timeout=timeout, headers=None)
                            if res.status_code not in [200,201]:
                                headers['x-operation'] = 'GetRecoProductsById'
                                res = self.old_requester.get(url, timeout=timeout, headers=headers)
                            if res.status_code in [200, 201]:
                                _json = res.json()
                                _json['source'] = urls[page]
                                raw_data_list.append(json.dumps(_json, ensure_ascii=False))
                        break

                    if not self.has_next_page(res.text, page):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        # params = {
        #     'operationName' : 'SearchV4'
        # }

        # variables = {
        #     "hasMarketplace": False,
        #     "page": 1,
        #     "query": search_string
        # }

        # extensions = {
        #     "pwa": {
        #         "salesLine": "Media",
        #         "country": "ES",
        #         "language": "es",
        #         "contentful": True
        #     },
        #     "persistedQuery": {
        #         "version": 1,
        #         "sha256Hash": "5d343bf372026c0b8f37445d7869b8f2736fd17dab4276b55941dc1eb22c47b1"
        #     }
        # }
        q_search_string = search_string.replace(' ','%20')
        for x in range(1, 5):
            # variables['page'] = i
            # params['variables'] = json.dumps(variables, ensure_ascii=False)
            # params['extensions'] = json.dumps(extensions, ensure_ascii=False)
            # url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(params))
            url = f'https://www.mediamarkt.es/es/search.html?q={q_search_string}&ids_only=true&page_size=12&page={x}'
            urls.append(url)

        return urls

    def __has_next_page(self, raw_data):
        _json = json.loads(raw_data, strict=False)

        if 'data' in _json:
            if 'searchV4' in _json['data']:
                if 'products' in _json['data']['searchV4']:
                    if len(_json['data']['searchV4']['products']) >= 12:
                        return True

        return False
    
    def get_result_from_script(self, raw_data):
        result = {}
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            result['source'] = str(tag)
                            result['raw'] = _json['apolloState']['ROOT_QUERY'][keys]['resultItems']
                            has_data = True if result['raw'] else False
                            return json.dumps(result), has_data
                        
        except:
            return result, False

    def has_next_page(self, raw_data, page):
        result = []
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            if page+1 < _json['apolloState']['ROOT_QUERY'][keys]['paging']['pageCount']:
                                return True
                            else:
                                return False
        except:
            return False
    
            

        