import logging
from copy import deepcopy
import json, re

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup


class MediamarktEsWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.es'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        source, tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx], source)

             # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], source)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx], source)

            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1



        return results, last_rank

    def get_product_tags(self, raw_data):
        _json = json.loads(raw_data, strict=False)

        if 'data' in _json:
            if 'searchV4' in _json['data']:
                if 'products' in _json['data']['searchV4'] and 'source' in _json:
                    return _json['source'], _json['data']['searchV4']['products']
            elif 'productRecommendations' in _json['data']:# SUGGESTED PRODUCTS
                if 'products' in _json['data']['productRecommendations'] and 'source' in _json:
                    return _json['source'], [product for product in _json['data']['productRecommendations']['products'] if product['product']]
            elif 'getRecommendationProductsById' in _json['data']:# SUGGESTED PRODUCTS
                if 'source' in _json:
                    return _json['source'], _json['data']['getRecommendationProductsById']
        else:
            return _json['source'], _json['raw']
        return '', []


    def get_title(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = _source
            if 'details' in tag:
                if 'title' in tag['details']:
                    value = tag['details']['title']
            elif 'product' in tag.keys():
                value = tag['product']['title']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = _source
            if 'details' in tag:
                if 'seoUrl' in tag['details']:
                    if tag['details']['seoUrl'].startswith('https'):
                        value = tag['details']['seoUrl']            
                    else:
                        value = 'https://{}{}'.format(self.__class__.WEBSITE, tag['details']['seoUrl'])
            elif 'product' in tag.keys():
                url = tag['product']['url']

                url_pref = 'http://www.mediamarkt.es'

                if url_pref in url:
                    value = url
                else:
                    value = url_pref + url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = _source
            if 'details' in tag:
                if 'manufacturer' in tag['details']:
                    value = tag['details']['manufacturer']
            elif 'product' in tag.keys():
                value = tag['product']['manufacturer']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value
                
        