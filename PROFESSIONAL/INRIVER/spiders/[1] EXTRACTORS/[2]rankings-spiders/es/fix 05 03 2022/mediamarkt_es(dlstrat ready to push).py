import json
import re, time
from urllib.parse import urlencode, quote
from bs4 import BeautifulSoup
import cloudscraper

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from request.unblocker import UnblockerSessionRequests


class MediamarktEsDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.old_requester = requester
        self.requester = UnblockerSessionRequests('es')
        self.scraper = cloudscraper.create_scraper(sess=requester.session)

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):

            headers = {
                "accept": "*/*",
                "accept-encoding": "gzip, deflate",
                "accept-language": "en-US,en;q=0.9",
                # "apollographql-client-name": "pwa-client",
                # "apollographql-client-version": "6.120.1",
                # "content-type": "application/json",
                # "referer": "https://www.mediamarkt.es/es/search.html?query={}Gaming&t={}".format(quote(search_string), str(time.time()).replace('.', '')[:13]),
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                "x-cacheable": "true",
                # # "x-flow-id": "fed21f2e-987b-4fa4-b3cd-7c9973d59c53",
                # "x-mms-country": "ES",
                # "x-mms-language": "es",
                # "x-mms-salesline": "Media",
                # "x-operation": "SearchV4"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')
        
        
        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers)
                # headers['referer'] = 'https://www.mediamarkt.es/es/search.html?query={}&page={}'.format(quote(search_string), page + 1)
                if res.status_code in [200, 201]:
                    pageChecker = self.pageChecker(res.text)
                    result = self.get_result_from_script(res.text)
                    
                    if pageChecker:
                        keyword = search_string.replace(" ", "%20")
                        url = f'https://www.mediamarkt.es/api/v1/graphql?operationName=GetProductRecommendations&variables=%7B%22productsToResolve%22%3A6%2C%22withMarketingInfos%22%3Afalse%2C%22boxName%22%3A%22box01_Searchterm2Product%22%2C%22recommendationContext%22%3A%22search%22%2C%22params%22%3A%22%26searchterm%3D{keyword}%26productlist%3D%22%2C%22sid%22%3A%221b0c3f3a-0819-4f79-9703-f73d1fc3846a%22%2C%22tracking%22%3Atrue%2C%22manualProducts%22%3A%5B%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2266a25e4bb9ebdb06ed1fc99f273c330af56c38e1d6b15b46a4d86edcf5061678%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22ES%22%2C%22language%22%3A%22es%22%7D%7D'

                        headers = {
                            'apollographql-client-name': 'pwa-client',
                            'apollographql-client-version': '1.76.0',
                            'cache-control': 'no-cache',
                            'content-type': 'application/json',
                            'cookie': 'abhomeab=B; __cf_bm=UOBV6OjXlgivgtDSgesholpY2g1umRbGiY4eXc2xprI-1651557169-0-AfXXuJCA2YjPwyZ5Amu+kWpSc7OZcIjIUXRZOfFmwmSjqtBefBvxbwOR4Epg2YWKmzn5S18+MucRXnn0/ugKUIVfynZvEyiQGJLn/pQ7v8086sH9doBYTEJi20diLv2rFk6Zmz2wKxFJDZfTzZjS4blJN1T2X28UYuHRQ13WJd0h; ts_id=1b0c3f3a-0819-4f79-9703-f73d1fc3846a; t_fpd=true; a=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Im8yc2lnbiJ9.eyJzdWIiOiJmZjA1OWQ0OS03OGU0LTRmNDYtYTg4Mi00ZTM1NzliZmUzZDciLCJpc3MiOiJtbXNlIiwiaWF0IjoxNjUxNTU3MTcxLCJleHAiOjE2NTI3NjY3NzEsImF1ZCI6IndlYm1vYmlsZSIsInQiOiJ1IiwibyI6MTExOX0.MuAGq1cRQttMmRxEE9SjHV0GX6U-gmBrP51YpoFkJ67toUQBkrPnYl2CHRdz9UIYjaUEkE2fWS6CF5ifpTdSrT_PsG1nZFUG6sXPYHLKXdlWM0e6NTjApzm_VQ2_LhKnYoZfWLkqOxeUNCkbX6P_wZFONFzJC0TzK6xDaKszsmkgEZ6LAf1C2TRiGQEU7Tgf1KD15I8KwQbm10xAn3lXgpu07pISVkJ5b-T6kNLb9M-ieV4uQRZ-QnVO3sHUEthdZmU4RqEewwayOydnGzg0-oXu1InyFdTn15PQjSc-7BboHGP7vVzz23RhzuCj1YHuPLXbcGFjDyAja8g574K82g; r=ZtiaOw9IfyQJ6Ua/YuD0fL0uZrUAn2/xAiOTmyju/WzmW7LDO6/MN0qmQHhzjxOf; s_id=587af50b-576d-4156-8c26-c3319ffc4acb; MC_PS_SESSION_ID=587af50b-576d-4156-8c26-c3319ffc4acb; p_id=587af50b-576d-4156-8c26-c3319ffc4acb; MC_PS_USER_ID=587af50b-576d-4156-8c26-c3319ffc4acb; __cfruid=7d17d443fe98649022683bf5782ee5ae56f97083-1651557172; pwaconsent=v:1.0~required:1&acf:1,clf:1,cli:1,cxk:1,gtm:1,ocx:1,pbx:1,reb:1|comfort:1&baz:1,cne:1,fix:1,gfa:1,gfc:1,goa:1,gom:1,lob:1,orc:1,prd:1,sen:1,sis:1,spe:1,sst:1,swo:1|marketing:1&asm:1,cri:1,fab:1,fbn:1,gad:1,gam:1,gcm:1,gdv:1,gos:1,gsa:1,gse:1,gst:1,kru:1,lkd:1,msb:1,omp:1,pin:1,trd:1,twt:1,usw:1,wdg:1,yte:1|; _gcl_au=1.1.1089317867.1651557493; lux_uid=165155749500902388; _gid=GA1.2.2134883200.1651557495; _fbp=fb.1.1651557497903.1733814708; _pin_unauth=dWlkPU56aGtabVEzWWpZdE5EY3pNaTAwTTJGbUxUaGpaVEl0TXpReVlqbGpPR013TXpWag; _clck=7xg3r5|1|f15|0; _dc_gtm_UA-122296363-1=1; _clsk=1n7uj9b|1651557973956|3|1|h.clarity.ms/collect; _msbps=92; _ga_7P4TSC4RJ1=GS1.1.1651557495.1.1.1651558012.60; _ga=GA1.2.1b0c3f3a-0819-4f79-9703-f73d1fc3846a; _uetsid=0600ce90caa611ec8c5d2db2925814af; _uetvid=06010ec0caa611ec94c2f1e18674ed80; cto_bundle=XOYuSF9Ocm5IUW94ckxPanVoVFJYODl6cVBxTiUyQnR2JTJCaWZCdTJUd3N2clV0UzEwNVpSTFhTV1BpRzltd0NLV25USUxhdkpkYUxTM0lXWkRCV1FTZVBObzlLdW85T0ZFRGV5RmlVeGxsMkhZTXA4QzRmMEU1Y1hSbmxtUzNvSE5wZ0lvTG54eW5CVXRQaWxSRG5oN052WlJRMk1nJTNEJTNE',
                            'pragma': 'no-cache',
                            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
                            'sec-ch-ua-mobile': '?0',
                            'sec-ch-ua-platform': '"Windows"',
                            'sec-fetch-dest': 'empty',
                            'sec-fetch-mode': 'cors',
                            'sec-fetch-site': 'same-origin',
                            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36',
                            'x-cacheable': 'true',
                            'x-flow-id': '7898ceb9-bb09-468b-9299-0db8844fac98',
                            'x-mms-country': 'ES',
                            'x-mms-language': 'es',
                            'x-mms-salesline': 'Media',
                            'x-operation': 'SearchV4'
                        }

                        res = self.old_requester.get(url, timeout=timeout, headers=headers)

                        if res.status_code in [200, 201]:
                            try:
                                _json = json.loads(res.text)
                                if _json:
                                    raw_data_list.append(res.text)
                                    break
                            except Exception as e:
                                print('dlstrat -> page requesters -> failed to parse json data')

                        # FOR SUGGESTED PRODUCTS
                        else:
                            headers = {
                                "accept": "*/*",
                                "accept-encoding": "gzip, deflate",
                                "accept-language": "en-US,en;q=0.9",
                                "apollographql-client-name": "pwa-client",
                                "apollographql-client-version": "1.73.0",
                                "content-type": "application/json",
                                "referer": urls[page],
                                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36",
                                "x-cacheable": "true",
                                "x-flow-id": "54ffc046-70da-42c5-bd8f-b61a589cf62f", # TODO: please update this incase of failure
                                "x-mms-country": "ES",
                                "x-mms-language": "es",
                                "x-mms-salesline": "Media",
                                "x-operation": "GetProductRecommendations"
                            }
                            params = {}
                            params['operationName'] = 'GetProductRecommendations'
                            variables = {
                                "productsToResolve":6,
                                "boxName":"box01_User2Products",
                                "recommendationContext":"noresult-search",
                                "params":f"&searchterm={quote(search_string)}",
                                "sid":"ef778bd3-c042-4238-976e-0aa81e1b856f",
                                "tracking":True,
                                "manualProducts":[]
                            }
                            extensions = {
                                "pwa":{
                                    "salesLine":"Media",
                                    "country":"ES",
                                    "language":"es"},
                                "persistedQuery":{
                                    "version":1,
                                    "sha256Hash":"46d82c9ad1d6b19a939b1c9fa1f07afd867ecf6437497d5b468f4c727f024857"} # TODO: please update this incase of failure
                            }
                            params['variables'] = json.dumps(variables, ensure_ascii=False)
                            params['extensions'] = json.dumps(extensions, ensure_ascii=False)
                            url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(params))
                            res = self.requester.get(url, timeout=timeout, headers=None)
                            if res.status_code not in [200,201]:
                                res = self.old_requester.get(url, timeout=timeout, headers=headers) 
                            if res.status_code in [200, 201]:
                                _json = res.json()
                                _json['source'] = urls[page]
                                raw_data_list.append(json.dumps(_json, ensure_ascii=False))
                                
                                product_ids = [product['productId'] for product in _json['data']['productRecommendations']['products'] if not product['product']]
                                _params = {}
                                _params['operationName'] = 'GetRecoProductsById'
                                #Temporary fix, order of product id's blocking request.
                                # if search_string == 'motorola e':
                                    # product_ids = ["1505482","1436170","1504120","1522890","1505278","1424386","1437732"]
                                # if search_string == 'moto g100':
                                    # product_ids = ["1505482","1482804","1265597","1461983","1482761","1508345","1469207","1503335","1436170","1504120","1522890","1424386","1437732","1522200","1486910"]

                                variables = {"ids": product_ids}
                                extensions = {
                                    "pwa":{"salesLine":"Media","country":"ES","language":"es"},
                                    "persistedQuery":{"version":1,
                                        "sha256Hash":"a63f90335c359e538418cbb646c62ee39821a12087bcc1944a7c6c9debcb3ee8"} # TODO: please update this incase of failure
                                    }
                                _params['variables'] = json.dumps(variables, ensure_ascii=False, separators=(',', ':'))
                                _params['extensions'] = json.dumps(extensions, ensure_ascii=False, separators=(',', ':'))
                                url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(_params))
                                res = self.requester.get(url, timeout=timeout, headers=None)
                                if res.status_code not in [200,201]:
                                    headers['x-operation'] = 'GetRecoProductsById'
                                    res = self.old_requester.get(url, timeout=timeout, headers=headers)
                                if res.status_code in [200, 201]:
                                    _json = res.json()
                                    _json['source'] = urls[page]
                                    raw_data_list.append(json.dumps(_json, ensure_ascii=False))
                            break

                    elif result[1]:
                        raw_data_list.append(result[0])

                    if not self.has_next_page(res.text, page):
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        urls = []
        # params = {
        #     'operationName' : 'SearchV4'
        # }

        # variables = {
        #     "hasMarketplace": False,
        #     "page": 1,
        #     "query": search_string
        # }

        # extensions = {
        #     "pwa": {
        #         "salesLine": "Media",
        #         "country": "ES",
        #         "language": "es",
        #         "contentful": True
        #     },
        #     "persistedQuery": {
        #         "version": 1,
        #         "sha256Hash": "5d343bf372026c0b8f37445d7869b8f2736fd17dab4276b55941dc1eb22c47b1"
        #     }
        # }
        q_search_string = search_string.replace(' ','%20')
        for x in range(1, 5):
            # variables['page'] = i
            # params['variables'] = json.dumps(variables, ensure_ascii=False)
            # params['extensions'] = json.dumps(extensions, ensure_ascii=False)
            # url = 'https://www.mediamarkt.es/api/v1/graphql?{}'.format(urlencode(params))
            url = f'https://www.mediamarkt.es/es/search.html?q={q_search_string}&ids_only=true&page_size=12&page={x}'
            urls.append(url)

        return urls

    def __has_next_page(self, raw_data):
        _json = json.loads(raw_data, strict=False)

        if 'data' in _json:
            if 'searchV4' in _json['data']:
                if 'products' in _json['data']['searchV4']:
                    if len(_json['data']['searchV4']['products']) >= 12:
                        return True

        return False
    
    def get_result_from_script(self, raw_data):
        result = {}
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            result['source'] = str(tag)
                            result['raw'] = _json['apolloState']['ROOT_QUERY'][keys]['resultItems']
                            has_data = True if result['raw'] else False
                            return json.dumps(result), has_data
                        
        except:
            return result, False

    def has_next_page(self, raw_data, page):
        result = []
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            tag = soup.find('script', text=re.compile('.*window.__PRELOADED_STATE__.=.*'))

            if tag:
                _rgx = re.search(r"({.*})", tag.get_text().strip())
            
                # remove undefined and any object instantation (new ...)
                # from object literal so that it can be qualified as
                # valid json
                json_data = re.sub(r':new[\s\S]+?,', ':null,', _rgx.group(1))
                json_data = re.sub(r':undefined', ':null', json_data)
                
                if _rgx: 
                    source = str(tag)
                    _json = json.loads(json_data)
                    
                    
                    for keys in _json['apolloState']['ROOT_QUERY']:
                        if 'searchV4' in keys or 'categoryV4' in keys:
                            if page+1 < _json['apolloState']['ROOT_QUERY'][keys]['paging']['pageCount']:
                                return True
                            else:
                                return False
        except:
            return False
    
    def pageChecker(self, raw_data):
        try:
            suggestedProductIndicators = ['Nuestras sugerencias de búsqueda para ti', 'Revisa tu entrada para ver si hay errores tipográficos.', 'Prueba con palabras relacionadas para tu término de búsqueda.', 'Revisa nuestras categorías y refina tu búsqueda hasta el producto deseado.']

            for x in suggestedProductIndicators:
                if (raw_data.find(x) > -1):
                    return True
        except Exception as e:
            print('Error at: dlstrat -> pageChecker() with message: ', e)

        