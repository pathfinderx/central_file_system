import logging
import json, re
from copy import deepcopy

from bs4 import BeautifulSoup, Tag

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class OfficeworksComAuWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.officeworks.com.au'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data) -> list:
        try:
            _json = json.loads(raw_data)

            if (_json and
                'results' in _json and
                isinstance(_json['results'], list) and  
                len(_json['results']) > 0 and 
                'hits' in _json['results'][0]):

                return _json['results'][0]['hits']
            return list()

        except Exception as e:
            print('Error at: wsstrat -> get_product_tags() -> with message: ', e)

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(tag, dict):
                if (tag and 'name' in tag):
                    source, value = str(tag), tag.get('name') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(tag, dict):
                if (tag and 'brand' in tag):
                    source, value = str(tag), tag.get('brand') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(tag, dict):
                if (tag and 'seoPath' in tag):
                    base_url = f'https://{self.__class__.WEBSITE}'
                    source, value = str(tag), f"{base_url}{tag.get('seoPath')}"

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value