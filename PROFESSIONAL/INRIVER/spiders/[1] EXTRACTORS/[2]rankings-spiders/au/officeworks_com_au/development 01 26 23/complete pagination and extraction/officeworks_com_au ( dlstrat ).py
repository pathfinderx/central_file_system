import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from bs4 import BeautifulSoup
from typing import Union
import urllib.parse
from request.unblocker import UnblockerSessionRequests


class OfficeworksComAuDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.initial_url = None # initial request url and used as primary url for pagination
        self.initial_data = None # initial html data result storage
        self.unblocker = UnblockerSessionRequests('au')
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # initial data from html results
                q_string = urllib.parse.quote(search_string)
                self.initial_url = f"https://www.officeworks.com.au/shop/officeworks/search?q={q_string}&view=grid&page=1&sortBy=bestmatch"
                res = self.requester.get(self.initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                data = self.get_api_data(search_string, page)

                if data:
                    raw_data_list.append(data)
                    
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = list()
            res = self.get_api_data(search_string)

            if res:
                page_count = self.get_page_count(res, limit)

                url = 'https://k535caawve-2.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20react-instantsearch%205.4.0%3B%20JS%20Helper%202.26.1&x-algolia-application-id=K535CAAWVE&x-algolia-api-key=8a831febe0110932cfa06ff0e2024b4f'
                
                for page in range(0, page_count):
                    urls.append(url)

                return [url for i in range(0, page_count)]
            return urls
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    @staticmethod
    def get_headers():
        pass

    @staticmethod
    def get_payload(page, search_string):
        pass
    
    @staticmethod
    def get_page_count(data, limit) -> int:
        try:
            _json = json.loads(data)
            
            if (_json and
                'results' in _json and
                isinstance(_json['results'], list) and  
                len(_json['results']) > 0 and 
                'nbPages' in _json['results'][0]):
                    
                return _json['results'][0]['nbPages']

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            pass

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)

    def get_api_data(self, search_string: str, page=0) -> Union[str, None]:
        try:
            payload = self.get_api_payload(search_string, page)

            if (payload):
                api_url = "https://k535caawve-2.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20react-instantsearch%205.4.0%3B%20JS%20Helper%202.26.1&x-algolia-application-id=K535CAAWVE&x-algolia-api-key=8a831febe0110932cfa06ff0e2024b4f"

                res = self.requester.post(api_url, timeout=10000, headers=None, cookies=None, data=payload) 

                if res.status_code in [200, 201]:
                    return res.text
            return None

        except Exception as e:
            print('Error at: dlstrat -> get_api_data() -> with message: ', e)

    def get_api_payload(self, search_string: str, page: int) -> Union[str, None]:
        try:
            if (search_string):
                q_string = urllib.parse.quote(search_string)

                param1 = '{"requests":[{"indexName":"prod-product-wc-bestmatch-personal","params":"query='
                param2 = f'{q_string}'
                param3 = f'&hitsPerPage=24&page={page}&analyticsTags=%5B%22search%22%5D&highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&clickAnalytics=true&optionalFilters=%5B%22availState%3AQLD%22%5D&sumOrFiltersScores=true&ruleContexts=%5B%22ANONYMOUS%22%5D&facets=%5B%22materialType%22%2C%22byodDeviceType%22%2C%22suitableOutdoors%22%2C%22MinimumUserHeight%22%2C%22shopByGrade%22%2C%22stackableFurniture%22%2C%22chairAdjLumbar%22%2C%22chairHeadrest%22%2C%22printingTechnologyLabels%22%2C%22greenerChoice%22%2C%22sizeDescription%22%2C%22powerBatteryChargeAmpHours%22%2C%22operatingHand%22%2C%22throwDistance%22%2C%22totalNumberOfLabels%22%2C%22shelfNumberShelves%22%2C%22freeSyncCompatible%22%2C%22curriculumCode%22%2C%22pivotAdjustment%22%2C%22gSyncCompatible%22%2C%22shopBySubject%22%2C%22swivelAdjustment%22%2C%22noiseCancelling%22%2C%22australianMade%22%2C%22tiltAdjustment%22%2C%22selfAdhesive%22%2C%22industryType%22%2C%22maxLoadWeight%22%2C%22bagStyle%22%2C%22connectivity%22%2C%22travelRegion%22%2C%22adjustableHeight%22%2C%22ChargePorts%22%2C%22broadbandGen%22%2C%22rangedOnline%22%2C%22handsetIncludedHandsets%22%2C%22photoCapacityQuantity%22%2C%22carpetThickness%22%2C%22materialFacet%22%2C%22recommendedUse%22%2C%22rangedRetail%22%2C%22commercialGrade%22%2C%22warrantyFacet%22%2C%22bluetoothCompatibility%22%2C%22frameStyle%22%2C%22tsaApproved%22%2C%22refreshRate%22%2C%22diaryLayout%22%2C%22compatibabilityCustomFitAndroid%22%2C%22compatibabilityCustomFitApple%22%2C%22performanceMaxThickness%22%2C%22videoResolution%22%2C%22printingTechnologyPrinters%22%2C%22deskType%22%2C%22maximumPowerRating%22%2C%22deskFeatures%22%2C%22learningSkillsFocus%22%2C%22afrdiRating%22%2C%22numberOfUsb20Ports%22%2C%22performanceHealthMonitoringFunctions%22%2C%22usbFlashLidType%22%2C%22capacityKeys%22%2C%22heightRange%22%2C%22chairAdjBackTilt%22%2C%22operatingSystemEdition%22%2C%22numberOfReamsPerCarton%22%2C%22numberOfPowerPorts%22%2C%22fitsDevice%22%2C%22padSheets%22%2C%22chairAdjSeatDepth%22%2C%22numberOfUsb32Ports%22%2C%222SidedPrinting%22%2C%22boardSizeFacet%22%2C%22printerConnectivityTechnology%22%2C%22performanceBrightness%22%2C%22performanceResolution%22%2C%22processorClockSpeed%22%2C%22numberOfProcessorCores%22%2C%22multipackSize%22%2C%22scannerType%22%2C%22performanceEstimatedCartridgeYieldSheets%22%2C%22connectivityTechnology%22%2C%222sidedScanning%22%2C%22optusPlanType%22%2C%22maxSupportedDocumentSize%22%2C%22earPlacement%22%2C%22deviceConnectivityTechnology%22%2C%22socketType%22%2C%22deviceCaseCompatibility%22%2C%22shelfMaxLoadShelf%22%2C%22flightTime%22%2C%22powerPowerType%22%2C%22rollLength%22%2C%22wallStrengthThickness%22%2C%22energyRating%22%2C%22runTimeHours%22%2C%22staplingCapacity%22%2C%22tipSize%22%2C%22labelsPerSheetRoll%22%2C%22capacityBinder%22%2C%22securityLevel%22%2C%22inputOutputCableType%22%2C%22powerBatteryTechnology%22%2C%22licenceValidityPeriod%22%2C%22deviceLocation%22%2C%22powerSource%22%2C%22learningAgeRange%22%2C%22opticalZoom%22%2C%22caseFeaturesNumberOfCompartments%22%2C%22serviceProvider%22%2C%22chassisType%22%2C%22shelfHeightAdjust%22%2C%22tapeWidth%22%2C%22primaryCameraVideo%22%2C%22topSurfaceDepth%22%2C%22topSurfaceWidth%22%2C%22labellerKeyboardLayout%22%2C%22placementPlacingMounting%22%2C%22colour%22%2C%22ringRingSize%22%2C%22fullSizeInnerDimensions%22%2C%22smartHomeCompatibility%22%2C%22protection%22%2C%22unitsOfMeasure%22%2C%22rulerLength%22%2C%22lightBulbType%22%2C%22sizeNumber%22%2C%22protectionType%22%2C%22wastebinCapacityRange%22%2C%22sharpenerSize%22%2C%22connectivityDisplayConnections%22%2C%22graphicsProcessor%22%2C%22sizeCapacity%22%2C%22stylusPenIncluded%22%2C%22labellingHomeUseFacet%22%2C%22portsTotalNumberOfNetworkingPorts%22%2C%22labellingOfficeUseFacet%22%2C%22brushShape%22%2C%22softwareDistributionMedia%22%2C%22chairAdjChairTilt%22%2C%22numberOfUsb30Ports%22%2C%22surgeSuppression%22%2C%22processorType%22%2C%22processorManufacturer%22%2C%22ramInstalledSize%22%2C%22placementVesaMountCompatibility%22%2C%22performanceApproximateNumberOfImpressions%22%2C%22chairAdjSeatTilt%22%2C%22runTime%22%2C%22connectivityWifiBands%22%2C%22brushhairtype%22%2C%22papersize%22%2C%22ciewhiteness%22%2C%22boardSurfmaterial%22%2C%22microphoneType%22%2C%22numberOfRings%22%2C%22spineSize%22%2C%22dualSimCompatible%22%2C%22chairAdjBackHeight%22%2C%22maxProcessorClockSpeed%22%2C%22performanceShredderCutType%22%2C%22licenceType%22%2C%22byodAge%22%2C%22audioSource%22%2C%22stampInking%22%2C%22forestProductSchemeName%22%2C%22secondaryProcessorType%22%2C%22hardDriveType%22%2C%22portsNumberOfUsbChargePorts%22%2C%22skillLevel%22%2C%22storageHardDriveCapacity%22%2C%22labellingIndustrialUseFacet%22%2C%22restTime%22%2C%22glutenFree%22%2C%22connectivityDisplayConnectionsPanels%22%2C%22rulingType%22%2C%22responseTime%22%2C%22displayResolution%22%2C%22automaticPaperFeed%22%2C%22keyboardCompatibility%22%2C%22connectorType%22%2C%22paperWeightGsm%22%2C%22touchScreen%22%2C%222SidedCopying%22%2C%22baseWheels%22%2C%22iPadGeneration%22%2C%22freeFromAnimal%22%2C%22storageHardDriveCapacityComputingDevices%22%2C%22contentLayout%22%2C%22storageIncludedFlashMemory%22%2C%22supportedMemoryCards%22%2C%22operatingPlatformCompatibility%22%2C%22scannerScanResolution%22%2C%22foldedDimensions%22%2C%22automaticDocumentFeederCapacity%22%2C%22switched%22%2C%22100RecycledProduct%22%2C%22maximumRecommendedDailyUsage%22%2C%22cableLength%22%2C%22numberOfUsb31Ports%22%2C%22displaySize%22%2C%22brand%22%2C%22performancePrintResolution%22%2C%22displayPanelType%22%2C%22envelopeSize%22%2C%22interfaceHardDrive%22%2C%22stapleSize%22%2C%22maximumPunchingCapacity%22%2C%22storageStorageCapacity%22%2C%22bulkbuyOnline%22%2C%22storageInternalMemorySize%22%2C%22chairArmrests%22%2C%22interfaceType%22%2C%22lidIncluded%22%2C%22up1Category%22%2C%22price%22%2C%22catPaths%22%5D&tagFilters="'

                param4 = '},{"indexName":"prod-content-spots","params":"query='
                param5 = f'{q_string}'
                param6 = f'&numericFilters=%5B%22startDate%3C%3D1674723600000%22%2C%22endDate%3E%3D1674723600000%22%5D&page={page}'
                param7 = '&highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&clickAnalytics=true&facets=%5B%5D&tagFilters="}]}'

                payload = f"{param1}{param2}{param3}{param4}{param5}{param6}{param7}"

                return payload if payload else None
            return None
                
        except Exception as e:
            print('Error at: dlstrat -> get_api_payload() -> with message: ', e)