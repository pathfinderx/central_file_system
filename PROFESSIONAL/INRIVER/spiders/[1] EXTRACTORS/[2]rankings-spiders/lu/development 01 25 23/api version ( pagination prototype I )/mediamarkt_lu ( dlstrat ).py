import json, re
from strategies.download.base import DownloadStrategyWithLimit
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, URLPreparationFailureException
from strategies.website.constants import Defaults
from request.unblocker import UnblockerSessionRequests
from bs4 import BeautifulSoup

class MediamarktLuDownloadStrategy(DownloadStrategyWithLimit):
    def __init__(self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('lu')
        self.initial_data = None # initial html data result storage
        self.isGetRequest = False
        
    def download(self, search_string, timeout=100, headers=None, cookies=None,data=None, limit=Defaults.RESULT_LIMIT.value):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            if not self.initial_data: # for this case: api payload requires initial data from html results
                initial_url = f"https://mediamarkt.lu/search?q={search_string}&options[prefix]=last"
                res = self.unblocker.get(initial_url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    self.initial_data = res.text
                
            urls = self.get_urls(search_string, limit)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                payload = self.get_payload(page + 1, search_string) if not self.isGetRequest else None

                if payload:
                    res = self.requester.post(urls[page], timeout=timeout, headers=payload, data=None, cookies=cookies)
                else: 
                    url_source = url = self.get_api_url(search_string, page+1)
                    res = self.requester.get(url, timeout=timeout, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    data = self.get_filtered_json_string(res.text)
                    has_next_page = self.has_next_page(data, limit)
                    raw_data_list.append(data)
                    
                    if not has_next_page:
                        break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, limit) -> list:
        try:
            urls = []
            url = self.get_api_url(search_string)

            if not self.isGetRequest:
                payload = json.dumps(self.get_payload(1, search_string))
                res = self.requester.get(url, timeout=1000, headers=self.get_headers(), data=payload, cookies={})
            else:
                res = self.requester.get(url, timeout=1000, headers=None, cookies=None)

            if res.status_code in [200,201]:
                data = self.get_filtered_json_string(res.text)
                page_count = self.get_page_count(data, limit)
                
                for page in range(0, page_count):
                    urls.append(url)

            return [url for i in range(0, page_count)]
                
        except Exception as e:
            print('Error at: dlstrat -> get_urls() -> with message: ', e)

    def get_filtered_json_string(self, raw_data) -> dict:
        try:
            _json = None
            _rgx_filters = re.search(r'({\"filters[\s\S]+?)\);', raw_data)
            _rgx2_products = re.search(r'({\"products[\s\S]+?)\);', raw_data)

            if _rgx_filters:
                _json = json.loads(_rgx_filters.group(1))

            elif _rgx2_products:
                _json = json.loads(_rgx2_products.group(1))

            return _json
            
        except Exception as e:
            print('Error at: dlstrat -> get_filtered_json_string() -> with message: ', e)

    @staticmethod
    def get_headers():
        return {
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate",
            "accept-language": "en-US,en;q=0.9",
            "adrum": "isAjax:true",
            "content-type": "application/json",
            "origin": "https://www.fastenal.com",
            "pragma": "no-cache",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36"
        }

    @staticmethod
    def get_payload(page, search_string):
        payload =  {
            "attributeFilters": {},
            "page": str(page),
            "pageSize": "48",
            "query": search_string
        }
        return payload
    
    @staticmethod
    def get_page_count(data, limit):
        try:
            if (data and
                'pagination' in data and
                'last_page' in data['pagination']):

                return data['pagination']['last_page']
            return 1

        except Exception as e:
            print('Error at dlstrat: dlstrat -> get_page_count() -> with message: ', e)

    def has_next_page(self, data, limit) -> bool:
        try:
            if (data and 
                'pagination' in data and
                'hasMorePages' in data['pagination']):

                return data['pagination']['hasMorePages']
            return False

        except Exception as e:
            print('Error at: dlstrat -> has_next_page() -> with message: ', e)

    def get_api_url(self, search_string, page=None) -> str:
        try:
            raw_html = self.initial_data
            soup = BeautifulSoup(raw_html, 'lxml')
            callback = 'jQuery360010337055672991902_1674613579115'
            filter_id = None 
            shop = None
            collection = None
            sort_by = None
            q = search_string
            options_prefix = 'last'
            limit = None
            event = None
            operation = 'filter' # default for rankings
            page_indicator = page 

            # extract collection
            collection_rgx = re.search(r'page_id = ([\s\S]+?);', str(soup))
            if collection_rgx:
                collection = collection_rgx.group(1)
                self.global_html_data = raw_html

            # extract sort_by
            global_config_rgx, sort_rgx = re.search(r'GloboFilterConfig = ([\s\S]+?});', str(soup)), None
            if global_config_rgx:
                sort_rgx = re.search(r'sort: \"([\s\S]+?)\",', global_config_rgx.group(1))
                sort_by = sort_rgx.group(1) if sort_rgx else 'relevance'
            else:
                sort_by = 'manual' # default sort_by payload value

            # extract shop
            shop_tag = soup.select_one('meta#th_shop_url') # extract shop
            if (shop_tag and shop_tag.has_attr('content')):
                shop = shop_tag.get('content')

            # extract filter_id
            filter_id_rgx = re.search(r'globo_filters_json = ([\s\S]+?})', str(soup))
            if (filter_id_rgx and collection):
                try:
                    _json = json.loads(filter_id_rgx.group(1))
                    if (_json and collection in _json):
                        filter_id = str(_json.get(collection))
                
                except Exception as e:
                    print('Error at: dlstrat -> set_api_url() -> collection_rgx extraction -> with error: ', e)

            # event manipulation
            event = 'all' if not page else 'loadmore'

            # extract limit
            limit_tag = soup.select_one('[property="og:title"]')
            if (limit_tag and limit_tag.has_attr('content')):
                _rgx = re.search(r'[\d]{2,4}', limit_tag.get('content'))
                
                limit = _rgx.group(0)

            if (filter_id and
                shop and
                sort_by and 
                collection):

                collection = 0 # set collection to 0 as per static requirement for rankings api request
                self.isGetRequest = True
            
                api_url = f"https://filter-eu.globosoftware.net/filter?callback={callback}&filter_id={filter_id}&shop={shop}&collection={collection}&sort_by={sort_by}&q={q}&options%5Bprefix%5D={options_prefix}&limit={limit}&event={event}&operation={operation}"

                return api_url
            return None

        except Exception as e:
            print('Error at: dlstrat -> set_api_url() -> with error: ', e)
            