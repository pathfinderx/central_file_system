from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class AllyourgamesNlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'authority': 'www.addwish.com',
                'pragma': 'no-cache',
                'cache-control': 'no-cache',
                'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
                'dnt': '1',
                'sec-ch-ua-mobile': '?0',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
                'sec-ch-ua-platform': '"Windows"',
                'content-type': 'application/x-www-form-urlencoded',
                'accept': '*/*',
                'origin': 'https://www.allyourgames.nl',
                'sec-fetch-site': 'cross-site',
                'sec-fetch-mode': 'cors',
                'sec-fetch-dest': 'empty',
                'referer': 'https://www.allyourgames.nl/',
                'accept-language': 'en-US,en;q=0.9',
            }

        if not isinstance(cookies, dict):
            cookies = None
        
        raw_data_list = []

        try:
            urls = self.get_urls(search_string)
            data = self.get_body(search_string)

        except Exception:
            raise URLPreparationFailureException(
                'URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = None
                if data:
                    res = self.requester.post(urls[page], timeout=timeout, headers=headers, cookies=cookies, data=data)
                else:
                    res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                    
                status_code = res.status_code

                if status_code in [200, 201]:
                    raw_data_list.append(res.text)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException(
                    'Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException(
                    'Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        urls = [
            f'https://www.addwish.com/api/v1/search/partnerSearch'
        ]

        if search_string in ['spelcomputer']:
            q_search_string = search_string.replace(' ', '+')
            urls = [
                f'https://www.allyourgames.nl/search/{q_search_string}/',
            ]

        return urls

    def get_body(self, query):
        q_search_string = query.replace(' ', '+')
        return f'key=a1c736ed-744b-432c-8ffd-448f24990615&q={q_search_string}&product_count=100&product_start=0&category_count=0&category_start=0&id=17231&return_filters=false&websiteUuid=0f55f57c-a30d-49af-a686-d558a17d32e5&hello_retail_id=616866994655ee47ca8f2815'
