import logging
from copy import deepcopy
import json, re

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup


class BolComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.bol.com'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1



        return results, last_rank

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        products = []
        tags = soup.select('ul.list-view.product-list.js_multiple_basket_buttons_page > li') \
            or soup.select('.product-item__content') \
            or soup.select('.product-item__hit-area-wrapper.hit-area .product-item__content')
        if tags:
            # for tag in tags:
                # filter sponsored tags
                # sponsored = tag.select_one('div[data-test="sponsored-product"]')
                # if sponsored:
                    # continue
                # products.append(tag)

            filtered_products = [products for products in tags if not 'js_sponsored_product' in products.attrs.get('class')] # exclude sponsored products (https://prnt.sc/MqqF6EdrzMxb)
            products.extend(filtered_products)

        return products


    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            
            tag = soup.select_one('a[data-test="product-title"]') or soup.select_one('a[data-test="title"]') or \
                soup.select_one('.product-title.product-title--placeholder') #Suggested tag
                
            if tag:
                source, value = str(tag), tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('a[data-test="product-title"]') or \
                soup.select_one('.product-title.product-title--placeholder') #Suggested tag

            url_pref = 'https://%s' % self.__class__.WEBSITE
            
            if tag and tag.has_attr('href'):
                if url_pref in tag.get('href'):
                    source, value = str(tag), tag.get('href')
                else:
                    source, value = str(tag), url_pref + tag.get('href') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value


    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('[data-test="party-link"]')

            if tag:
                source, value = str(tag), tag.get_text(strip=True)


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

        