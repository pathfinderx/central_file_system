import logging
from copy import deepcopy
import json, re

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from bs4 import BeautifulSoup, Tag


class MediamarktNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

             # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx], raw_data)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx],raw_data)

            results.append(result)
            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank = last_rank + 1



        return results, last_rank

    def get_product_tags(self, raw_data):
        if isinstance(raw_data, list):
            return raw_data

        if isinstance(raw_data, dict):
            if (raw_data and
                'data' in raw_data and
                'categoryV4' in raw_data['data'] and
                'products' in raw_data['data']['categoryV4']):
                return raw_data['data']['categoryV4']['products']
        
        soup = BeautifulSoup(raw_data, 'lxml')

        tags = soup.select('.product-wrapper') or \
            soup.select('#product-sidebar') or soup.select('[data-test="mms-search-srp-productlist-item"]')
        if tags:
            return tags

        return []


    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if tag and isinstance(tag, dict):
                if ('product' in tag and 'title' in tag['product']):
                    source, value = str(tag), tag['product']['title']
                elif ('details' in tag and 'title' in tag['details']):
                    source, value = str(tag), tag['details']['title']

            if value == Defaults.GENERIC_NOT_FOUND.value and isinstance(tag, Tag):
                source = str(tag)
                _tag = tag.select_one('.content h2 a') or \
                    tag.select_one('[itemprop="name"]') or \
                        tag.select_one('.product-name') or tag.select_one('[data-test="product-title"]')

                if _tag:
                    value = _tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag,raw_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            source = str(tag)
            url_pref = 'https://%s' % self.__class__.WEBSITE
            if tag and isinstance(tag, dict):
                if 'product' in tag and 'url' in tag['product']:
                    if url_pref in tag['product']['url']:
                        value = tag['product']['url']
                    else:
                        value = '%s%s' % (url_pref, tag['product']['url'])
                elif 'details' in tag and 'url' in tag['details']:
                    value = f'{url_pref}{tag["details"]["url"]}'

            if value == Defaults.GENERIC_NOT_FOUND.value and isinstance(tag, Tag):
                _tag = tag.select_one('.content h2 a') or \
                    tag.select_one('.model [itemprop="url"]')

                if _tag and _tag.has_attr('href'):
                    if url_pref in _tag.get('href'):
                        value = _tag.get('href')
                    else:
                        value = '%s%s' % (url_pref, _tag.get('href'))

                else:
                    soup = BeautifulSoup(raw_data,'lxml')
                    _tag = soup.select_one('[itemprop="url"]') or soup.select_one('.StyledLinkRouter-sc-1drhx1h-2.cOmqtX')
                    if _tag and _tag.has_attr('href'):
                        if url_pref in _tag.get('href'):
                            value = _tag.get('href')
                        else:
                            value = '%s%s' % (url_pref, _tag.get('href'))
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, tag, raw_data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            try:
                source = str(tag)
                _json = self.get_json(tag.find_previous_sibling())

                if 'brand' in _json:
                    value = _json['brand']
                else:
                    soup = BeautifulSoup(raw_data,'lxml')
                    _tag = soup.select_one('meta[itemprop="brand"]')

                    if _tag and _tag.has_attr('content'):
                        source, value = str(_tag), _tag['content']
            except:
                if isinstance(tag, dict):
                    if (tag and 
                        'details' in tag and
                        'manufacturer' in tag['details']):
                        
                        source, value = str(tag), tag['details']['manufacturer']
                else:
                    _tag = tag.select_one('meta [itemprop="brand"]')

                    if _tag and _tag.has_attr('content'):
                        source, value = str(_tag), _tag['content']
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_json(self, tag):
        _json = {}
        if tag:
            _json = re.search('\{.*\}', tag.get_text().strip())
            _json = json.loads(_json.group()) if _json else {}

        return _json
                
        