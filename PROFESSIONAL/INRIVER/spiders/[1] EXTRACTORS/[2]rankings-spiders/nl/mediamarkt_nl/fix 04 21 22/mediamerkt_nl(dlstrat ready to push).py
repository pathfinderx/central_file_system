import re, json

from random import randint
from urllib.parse import urlparse, quote_plus
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

class MediamarktNlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.json_data = None

    def download(self, search_string, timeout=randint(10,60), headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string, timeout, headers, cookies)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        
        for page in range(0, len(urls)):
            try:

                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies, verify=False)

                if res.status_code in [200, 201]:
                    
                    if self.has_data(res.text) or self.isJsonData(res.text): # PDP CHECKER        
                        if self.json_data:
                            raw_data_list.append(self.json_data)
                        else:
                            raw_data_list.append(res.text)
                            headers = self.get_headers()
                    else:
                        suggested_prod = self.get_suggested_products(search_string,urls[page])
                        if suggested_prod and page == 0:
                            raw_data_list.append(suggested_prod)   
                        break
                    # temporary removal of next page finder. Since it used static declaration for next api url. Discard this if trivial
                    # if not self.__has_next_page(res.text):
                    #     break

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string, timeout, headers, cookies):

        # urls = [
        #     'https://www.mediamarkt.nl/nl/search.html?query=%s&searchProfile=onlineshop&channel=mmnlnl' % quote_plus(search_string)
        # ]

        # urls.extend(self.__get_next_urls(urls[0]))

        urls = [
            'https://www.mediamarkt.nl/nl/search.html?query='+search_string+'&page=1',
            # 'https://www.mediamarkt.nl/nl/search.html?query='+search_string+'&page=2',
            # 'https://www.mediamarkt.nl/nl/search.html?query='+search_string+'&page=3'
        ]
    
        try: # extract wcsId for api url
            res = self.requester.get(urls[0], timeout, headers, cookies)
            if res.status_code in [200, 201]:
                raw_cs = re.search(r'(wcsId[\s\S]+[})])', res.text)
                wcsId = re.search(r'[0-9]+', raw_cs.group(1))

                if wcsId:
                    # list comprehension of url from page 2-3
                    api_url_list = [f'https://www.mediamarkt.nl/api/v1/graphql?operationName=CategoryV4&variables=%7B%22hasMarketplace%22%3Afalse%2C%22maxNumberOfAds%22%3A2%2C%22isRequestSponsoredSearch%22%3Afalse%2C%22isDemonstrationModelAvailabilityActive%22%3Atrue%2C%22withMarketingInfos%22%3Afalse%2C%22filters%22%3A%5B%5D%2C%22wcsId%22%3A%22{wcsId.group(0)}%22%2C%22page%22%3A{x+2}%2C%22sessionId%22%3A%22f63d9436-c4ca-40a7-a40c-065d232e846b%22%2C%22customerId%22%3A%22f63d9436-c4ca-40a7-a40c-065d232e846b%22%2C%22pageType%22%3A%22Category%22%2C%22productFilters%22%3A%5B%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22472d5a118e8b94366a8da09218ca1201ee99a2dae0a9bcc80faf68e724ba7071%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22NL%22%2C%22language%22%3A%22nl%22%2C%22ccr%22%3Atrue%7D%7D' for x in range(2)]

        except Exception as e:
            print(e)
    
        urls.extend(api_url_list)

        return urls

    # def __get_next_urls(self, url):
    #     urls = []
    #     headers = {
    #             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
    #                         'Chrome/57.0.0.12335 Safari/537.36',
    #         }        

    #     res = self.requester.get(url, headers=headers, timeout=1000)
        
    #     if res.status_code in [200, 201]:
    #         soup = BeautifulSoup(res.text, 'lxml') 
    #         tag = soup.select_one('#category > div > ul > li.pagination-next > a')

    #         if tag and tag.has_attr('href'):
    #             url = tag['href'] if tag['href'].startswith('https') else 'https:{}'.format(tag['href'])
    #             if 'www' not in url:
    #                 url = 'https://www.mediamarkt.nl{}'.format(tag['href'])
    #             for i in range(2, 11):
    #                _url = re.sub('page=\d', 'page={}'.format(i), url) 
    #                urls.append(_url)

    #     return urls

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        tag = soup.select_one('#category > div > ul > li.pagination-next > a') or \
            soup.select_one('.StyledButton-sc-140xkaw-1.cUpAbt')

        if tag:
            return True

        return False

    def get_suggested_products(self, search_string, _url):
        search_string = search_string.replace(' ','%2520' )
        params = {
            'operationName': 'GetProductRecommendations',
            'variables': '%7B%22productsToResolve%22%3A6%2C%22boxName%22%3A%22box01_Searchterm2Product%22%2C%22recommendationContext%22%3A%22search%22%2C%22params%22%3A%22%26searchterm%3D'+search_string+'%26productlist%3D%22%2C%22sid%22%3A%22596de0fc-51fc-4f4a-a0d9-0080e25f9d8c%22%2C%22tracking%22%3Atrue%2C%22manualProducts%22%3A%5B%5D%7D',
            'extensions': '%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2246d82c9ad1d6b19a939b1c9fa1f07afd867ecf6437497d5b468f4c727f024857%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22NL%22%2C%22language%22%3A%22nl%22%2C%22ccr%22%3Atrue%7D%7D'
            }
        url_params = '&'.join(['{}={}'.format(k, v) for k, v in params.items()])
        url = 'https://www.mediamarkt.nl/api/v1/graphql?{}'.format(url_params)

        headers = {
            'accept': '*/*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'apollographql-client-name': 'pwa-client',
            'apollographql-client-version': '1.74.0',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'pragma': 'no-cache',
            'referer': _url,
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
            'x-cacheable': 'true',
            'x-flow-id': 'd895c70e-8c36-4bbf-b183-78a322b4cfb1',
            'x-mms-country': 'NL',
            'x-mms-language': 'nl',
            'x-mms-salesline': 'Media',
            'x-operation': 'GetProductRecommendations'
            }
        
        try:
            _products = []
            res = self.requester.get(url, timeout=10, headers=headers)
            if res.status_code in [200, 201]:
                _json =  json.loads(res.text)
                for _prod in _json['data']['productRecommendations']['products']:
                    if not _prod['product']:
                        break
                    _products.append(_prod)

            url = 'https://www.mediamarkt.nl/api/v1/graphql?operationName=GetRecoProductsById&variables=%7B%22ids%22%3A%5B%221476570%22%2C%221714923%22%2C%221593298%22%2C%221664074%22%2C%221677759%22%2C%221703821%22%2C%221718224%22%2C%221611804%22%2C%221700494%22%2C%221613822%22%2C%221690548%22%2C%221656577%22%2C%221721399%22%2C%221700489%22%2C%221633503%22%2C%221672473%22%2C%221715575%22%2C%221505006%22%2C%221543529%22%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22a63f90335c359e538418cbb646c62ee39821a12087bcc1944a7c6c9debcb3ee8%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22NL%22%2C%22language%22%3A%22nl%22%2C%22ccr%22%3Atrue%7D%7D'
            res = self.requester.get(url, timeout=10, headers=headers)
            if res.status_code in [200, 201]:
                _json =  json.loads(res.text)
                for _prod in _json['data']['getRecommendationProductsById']:
                    if not _prod['product']:
                        break
                    _products.append(_prod)
                    
            return _products
        except:
            return False

    def has_data(self, res_text):
        tag = BeautifulSoup(res_text, 'lxml')
        _tag = tag.select_one('.content h2 a') or \
                tag.select_one('[itemprop="name"]') or \
                tag.select_one('.product-name') or tag.select_one('[data-test="product-title"]')
        if _tag:
            return True
        return False

    def isJsonData (self, data):
        try:
            _json = json.loads(data)
            if _json:
                self.json_data = _json
                return True
            else:
                return False
        except Exception as e:
            print('Download Strategy -> isJsonData() -> failure to parse json')

    def get_headers(self):
        headers = {
            # 'accept': '*/*',
            # 'accept-encoding': 'gzip, deflate, br',
            # 'accept-language': 'en-US,en;q=0.9',
            'apollographql-client-name': 'pwa-client',
            'apollographql-client-version': '1.75.0',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'cookie': '__cf_bm=yafXhcf2jHUnGNdoTVxGdbZqk_.YVnvLXjteE8JnTOg-1650528885-0-AT5qxjymhWcV+xMJb3Plbq472Q1hZad6cI9aNpvq52NBZizxxL4qHOzmL/cg5hY1S9isuzt/Jmx4KY3cGJoT8Qc=; ts_id=f63d9436-c4ca-40a7-a40c-065d232e846b; t_fpd=true; _msbps=99; a=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Im8yc2lnbiJ9.eyJzdWIiOiIyZDdjNDliNy0xMWY4LTQxNzctYWMzMC1hYzA5ZWMxYTM0ZTYiLCJpc3MiOiJtbXNlIiwiaWF0IjoxNjUwNTI4ODg2LCJleHAiOjE2NTE3Mzg0ODYsImF1ZCI6IndlYm1vYmlsZSIsInQiOiJ1IiwibyI6NzQyfQ.TKd2UndqB0LpoxS6a-lLRwPpAeozHGg3Q1E4UYBpnSTAybp4WSAEgmLVsoVizeFi7VQ82-VyIy8OYE-5a7lTznwGsCg3HA2mp2htMSZ1_kzXxP-lvHSRWaGpuBowaqdaSx45zq31Kr_AV2K6Imifg-YST-En7GW3XspL5m4Sy9DAK2AoXtVIOnkt7_kP7uqTu-MhrP2GKbVjzl4PJdcUGCIhG4eGKK2iolAYZgdkUn6APcL4TJbmCxEOn_DngBlntVt-R_ocm23QQiZrJcLxT2JbujutqJPQqzAUoOweFthhegrewFgmwRZ6gTmCYkF9V2o66rRWZcGIw7LNGb8c4A; r=3PQeoRFoigMzeiZ7NmGGuf1jrlWUbk4zsmR7L0mmiHyx+yQoDI8MV7F28GKfcsGW; s_id=b92fc37d-4ea6-463d-93f5-b2d53153ef96; MC_PS_SESSION_ID=b92fc37d-4ea6-463d-93f5-b2d53153ef96; p_id=b92fc37d-4ea6-463d-93f5-b2d53153ef96; MC_PS_USER_ID=b92fc37d-4ea6-463d-93f5-b2d53153ef96; __cfruid=439b655dde41cec1fe049cf8f2b715838be4099c-1650528887; pwaconsent=v:1.0~required:1&baz:1,cli:1,gfb:1,gtm:1,ocx:1|comfort:1&bik:1,clf:1,cne:1,fix:1,gfa:1,gfc:1,goa:1,gom:1,grc:1,lob:1,opt:1,orc:1,prd:1,sen:1,spe:1,sst:1,swo:1,yte:1|marketing:1&apn:1,asm:1,cri:1,fab:1,fbn:1,gam:1,gcm:1,gdv:1,gos:1,gse:1,gst:1,htj:1,kru:1,msb:1,pin:1,trd:1,ttd:1,twt:1,usa:1|; _ga=GA1.2.f63d9436-c4ca-40a7-a40c-065d232e846b; _gcl_au=1.1.887371126.1650528891; lux_uid=165052889139630480; _gid=GA1.2.815237078.1650528891; _dc_gtm_UA-34233167-1=1; __gpi=UID=000004f4ed544cad:T=1650528890:RT=1650528890:S=ALNI_MbHPJPHb6X1G9ErnFaAQt-I-HLxhA; __gads=ID=667a8c5948c68b9d:T=1650528890:S=ALNI_MawWz-CkBBsKpeIL7i1RIjnYfWfPw; _fbp=fb.1.1650528891792.836436031; _clck=cckm2r|1|f0t|0; JSESSIONID=0000L9ABFWOeVm3swfAAI15y5q6:1fif0hve9; MC_DEVICE_ID=-1; MC_DEVICE_ID_EXT=-1; WC_SESSION_ESTABLISHED=true; WC_PERSISTENT=PFv%2BmsizKUEnIhtiq%2FWKOUndJJo%3D%0A%3B2022-04-21+10%3A14%3A53.622_1650528893618-123408_10259_-1002%2C-11%2CEUR%2CxRl9tm1IXOL84lRz5TR5ylo3R1zI01t0NKmZpYCg4NjpR7Q1tMJ7oaLJmf5AxOgoH0C1Gx4eEZDahQ8FuKsDYg%3D%3D_10259; WC_AUTHENTICATION_-1002=-1002%2CzXYtwdBkczPrxq2s4y6voINDX%2Fg%3D; WC_ACTIVEPOINTER=-11%2C10259; WC_USERACTIVITY_-1002=-1002%2C10259%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2C7WhZCh1sFtxd%2FzyZnr%2BXcV031G0FQ2eQZtB4te8WQP9ZzzbWDKrLKtE%2BwT0FYAIs4UMSrUSfg%2FLT2ZpKEEWINU1wETHpiUP%2FEOUhSXYuXZDuwGDgSlknHn%2BK%2Fv4dU6NbjjIzuSR7LlGP8ZSVkrE%2Fa8DkySOP0aT4v6K5hY4ak%2Bd3lBbdDJofvssxct9c89M%2F98x8W4zjDJmISJzMk%2F0tUw%3D%3D; WC_GENERIC_ACTIVITYDATA=[28759509935%3Atrue%3Afalse%3A0%3AFHqyVcfefQxIcCCX%2FPmShJGio%2BA%3D][com.ibm.commerce.context.audit.AuditContext|1650528893618-123408][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Store][com.ibm.commerce.context.globalization.GlobalizationContext|-11%26EUR%26-11%26EUR][com.ibm.commerce.catalog.businesscontext.CatalogContext|10001%26null%26false%26false%26false][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.base.BaseContext|10259%26-1002%26-1002%26-1][com.ibm.commerce.context.experiment.ExperimentContext|null][com.ibm.commerce.context.entitlement.EntitlementContext|10018%2610018%26null%26-2000%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]; dtCookie=v_4_srv_3_sn_F6B7E63F275F4F6A59B2195737250DD4_perc_100000_ol_0_mul_1_app-3A0fe7eb5fcee005af_1; _uetsid=fd2a55d0c13a11ec93a0db4fd180dc2c; _uetvid=fd2a7380c13a11ecbd58934d1b61a0e0; cto_bundle=g3RNaF9DSWh1SkFIcWJUYWpQQ3hhc1hqb1FEbTJZY3lZTjkxYmROVUk1Z2l3b0E4NjNzc0YlMkIlMkZMRGxQZzV5V2hrZG53Z3AlMkZrUGduVzU0Mm14WU81M1VwaVMweSUyRlBvWlk0TkFFJTJGOWlBTmlpMEpPd0JLUFp1TjZueXBkT0k0bXJYNHA5JTJGTU5GJTJGTXEwNmZXVmh1SnlLYjZ3TjltQSUzRCUzRA; _clsk=8hy8qa|1650528903374|2|1|f.clarity.ms/collect',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36',
            'x-cacheable': 'true',
            'x-flow-id': 'a3fd531c-8eb5-428a-8dcd-75eb90c98513',
            'x-mms-country': 'NL',
            'x-mms-language': 'nl',
            'x-mms-salesline': 'Media',
            'x-operation': 'CategoryV4'
        }

        return headers