import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BeverNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.bever.nl'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)
        
        last_rank = start_rank
        if tags:
            for idx in range(0, len(tags)):
                result = deepcopy(template)

                # Assign rank
                result["rank"] = str(last_rank)

                # Extract title
                result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

                # Extract Brand
                result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

                # Extract URL
                result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

                results.append(result)

                # Check if limit is hit
                
                if last_rank >= limit:
                    break

                last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        products = []
        try:
            if isinstance(raw_data, dict):
                if (raw_data and 
                    'store' in raw_data and
                    'lister' in raw_data['store'] and
                    'items' in raw_data['store']['lister']):

                    products = raw_data['store']['lister']['items']
            else:
                _json = json.loads(raw_data)

                for i in _json['items']:
                    i['source'] = _json['source']
                    products.append(i)
        except:
            soup = BeautifulSoup(raw_data, 'lxml')
            tags = soup.select('.as-t-product-grid__item')
            if tags:
                for tag in tags:
                    products.append(tag)
        
        return products



    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            try:
                source, value = str(tag), tag['title']
            except:
                tag = tag.select_one('.as-a-text.as-m-product-tile__name')
                if tag:
                    source, value = str(tag), tag.get_text()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No brand for this website
        try:
            try:
                source, value = str(tag), tag['brand']
            except:
                tag = tag.select_one('.as-a-text.as-m-product-tile__brand')
                if tag:
                    source, value = str(tag), tag.get_text()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            try:
                source = str(tag)
                value = 'https://www.bever.nl/p{}.html'.format(tag['seoUrl'])
            except:
                tag = tag.select_one('.as-a-link')
                if tag and tag.has_attr('href'):
                    source, value = str(tag), 'https://www.bever.nl{}'.format(tag.get('href'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
