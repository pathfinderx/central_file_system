
import json, re
from urllib.parse import urlencode

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class BeverNlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):

            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;'
                          'q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fil;q=0.8',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                              '(KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
            }
       
        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:

                    #Temporary fix for search url redirected to pdp
                    #Freely remove this code when keyword('Trousers Women') back to normal
                    if search_string == 'Trousers Women' and _json['itemCount'] == 1:
                        raise DownloadFailureException('Download failed - Unhandled Exception')
                    #end

                    elif search_string in ['Wandelbroek dames','Wandelbroeken dames','wandelbroeken dames','ski jassen heren','skibroek heren']:
                        raw_data_list.append(res.text)

                    else:
                        try:
                            _json = res.json()
                            _json['source'] = urls[page]
                            raw_data_list.append(json.dumps(_json))
                        except Exception as e:
                            print('dlstrat -> pagination loop -> fail to decode json')
                            products = self.page_checker(res.text)
                            raw_data_list.append(products) if products else None

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    
    def get_urls(self, search_string):

        params = self._get_params(search_string)

        urls = [
            'https://www.bever.nl/api/aem/search?{}'.format(urlencode(params))
        ]
        
        if search_string.lower() == 'skibroek dames' or search_string.lower() == 'skibroeken dames':
            urls = [
                'https://www.bever.nl/api/aem/search?filter=categories:c(442;9609;9617)&anaLang=nl&fictiveWebShop=143&locale=nl&market=nl&mainWebShop=bever&platform=public_site&page=0&size=48&segment=a'
            ]

        if search_string in ['Wandelbroek dames','Wandelbroeken dames','wandelbroeken dames','ski jassen heren','skibroek heren']:
            search_string = search_string.replace(' ', '%20')
            urls = [
                'https://www.bever.nl/c/dames/broeken/wandelbroeken.html?q='+search_string+'&sred=1'
            ]

        if search_string in ['Ski jassen heren']:
            search_string = search_string.replace(' ', '%20')
            urls = [
                f'https://www.bever.nl/c/heren/jassen/ski-jassen.html?q={search_string}&sred=1'
            ]
            
        if search_string in ['Skibroek heren']:
            search_string = search_string.replace(' ', '%20')
            urls = [
                 f'https://www.bever.nl/c/heren/broeken/skibroeken.html?q={search_string}&sred=1'
            ]

        return urls

    def _get_params(self, search_string):
        try:
            url = 'https://www.bever.nl/lister.html?q={}'.format(search_string)

            if search_string.lower() == 'wandelschoenen':
                url = 'https://www.bever.nl/c/schoenen/wandelschoenen.html?q={}&sred=1'.format(search_string)

            res = self.requester.get(url)
            params = {
                "anaLang": "nl",
                "locale": "nl",
                "market": "nl",
                "platform": "public_site",
                "query": search_string,
                "page": "0",
                "size": "100"
            }
            if res.status_code:
                soup = BeautifulSoup(res.text, 'lxml')
                tag = soup.select_one('[data-load-zendesk="true"]').parent.next_sibling
                if tag:
                    text = tag.text.split('Config,')[-1].strip()[:-2]
                    _json = json.loads(text)

                    params['mainWebShop'] = _json['mainWebShop']
                    params['fictiveWebShop'] = _json['fictiveWebShop']

                    if 'defaultSearchFilter' in _json['pageInfo'].keys():
                        params['filter'] = _json['pageInfo']['defaultSearchFilter']
                        tag = soup.select_one('script[data-hypernova-key="AEMScenes_ProductLister"]')
                        data = json.loads(tag.text[4:-3])
                        params['segment'] = data['store']['lister']['segment']
                        del params['query']
                    else:
                        params[''] = ''
        except Exception as e:
            raise(e)

        return params

    def page_checker (self, data):
        try:
            soup = BeautifulSoup(data, 'lxml')
            product_as_json_string = soup.select_one('script[type="application/json"][data-hypernova-key="AEMScenes_ProductLister"]').next_element
            if product_as_json_string:
                rgx1 = re.sub(r'(<!--)|(-->)', "", product_as_json_string)
                if rgx1:
                    _json = json.loads(rgx1)
                    return _json if _json else None
            return None
        except Exception as e:
            print('Error in: dlstrat -> page_checker() with message: ', e)