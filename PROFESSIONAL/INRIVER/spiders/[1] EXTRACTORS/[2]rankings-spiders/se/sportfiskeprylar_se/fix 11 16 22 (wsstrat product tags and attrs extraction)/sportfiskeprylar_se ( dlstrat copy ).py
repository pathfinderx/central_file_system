import json
import time
import re

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class SportfiskeprylarSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                    'Accept': 'text/html, */*; q=0.01',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'en-US,en;q=0.9,fil;q=0.8',
                    'Connection': 'keep-alive',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Host': 'www.sportfiskeprylar.se',
                    'Origin': 'https://www.sportfiskeprylar.se',
                    'Referer': 'https://www.sportfiskeprylar.se/shop?funk=gor_sokning&q=garmin', 
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                                '(KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',       
                    'X-Requested-With': 'XMLHttpRequest'
                    }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                for_api_source = ['EKOLOD', 'PLOTTER', 'NAVIGATION', 'BÅT GPS', 'AIS', 'VHF', 'RADAR', 'FISHFINDER', 'GPS', 'KLOCKA','navigation','ekolod']
                if search_string in for_api_source:
                    headers['Referer'] = 'https://www.sportfiskeprylar.se/search/{}'.format(search_string)
                    body = {
                        'QueryString': search_string,
                        'DirectResults_FromIndex': 0,
                        'DirectResults_ToIndex': 25,
                        'RecommendedResults_FromIndex': 0,
                        'RecommendedResults_ToIndex': 9,
                        'funk': 'loop54_search'
                    }
                else:
                    headers['Referer'] = 'https://www.sportfiskeprylar.se/shop?funk=gor_sokning&q={}'.format(search_string)
                    body = {
                        'Sort': 'Relevans',     
                        'Visn': 'search',       
                        'brand_id': '',
                        'campaign_id': '',      
                        'category_id': '',      
                        'filter_params': '{}',  
                        'funk': 'get_filter',   
                        'is_search': '1',       
                        'is_start': '1',        
                        'limits': '60',
                        'offset': str(page * 60),
                        'outlet': '0',
                        'property_value_id': '',
                        'search_list': '',      
                        'sok_term': search_string
                        }
                res = self.requester.post(urls[page], timeout=timeout, data=body, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    result = {
                            'data_source' : None
                        }
                    if 'PT_Wrapper' in res.text: # For Valid results
                        result['data_source'] = 'soup'
                        result['raw_data'] = res.text
                        raw_data_list.append(json.dumps(result)) 
                        
                        if not self.__has_next_page(res.text):
                            break

                    elif self.if_json_result(res.text): # For Valid results (API source)
                        result['data_source'] = 'json'
                        result['raw_data'] = res.text
                        result['api_url'] = urls[page]
                        raw_data_list.append(json.dumps(result)) 
                        break

                    else:
                        url = "https://www.sportfiskeprylar.se/shop?funk=gor_sokning&q=" + search_string
                        headers = {
                            'Accept': '*/*',
                            'Accept-Encoding': 'gzip, deflate, br',
                            'Accept-Language': 'en-US,en;q=0.9',
                            'Sec-Fetch-Dest': 'document',
                            'Sec-Fetch-Mode': 'navigate',
                            'Sec-Fetch-Site': 'same-origin',
                            'Sec-Fetch-User': '?1',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',       
                        }
                        res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                        if res.status_code in [200, 201]:
                            if 'PT_Wrapper' in res.text: # For Suggested Products
                                raw_data_list.append(res.text) 
                        else:
                            raise PaginationFailureException()

                        break # end assuming 1 pager
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        
        urls = ['https://www.sportfiskeprylar.se/shop' for i in range(2)]

        for_api_source = ['EKOLOD', 'PLOTTER', 'NAVIGATION', 'BÅT GPS', 'AIS', 'VHF', 'RADAR', 'FISHFINDER', 'GPS', 'KLOCKA','ekolod','navigation']

        if search_string in for_api_source:
            urls = ['https://www.sportfiskeprylar.se/cgi-bin/ibutik/API.fcgi']

        return urls


    def __has_next_page(self, raw_data):

        soup = BeautifulSoup(raw_data, 'lxml')

        tag = soup.select('.PT_Wrapper_All > .PT_Wrapper')

        if len(tag) < 60:
            return False

        return True

    def if_json_result(self,raw_data):
        try:
            _json = json.loads(raw_data)
            if _json and 'Data' in _json.keys() and 'DirectResults' in _json['Data'] and _json['Data']['DirectResults']:
                return True
        except:
            pass
        return False