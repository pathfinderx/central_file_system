import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class SportfiskeprylarSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.sportfiskeprylar.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break
            
            last_rank = last_rank + 1

        return results, last_rank

    def get_product_tags(self, raw_data):
        raw_json = json.loads(raw_data)
        if raw_json['data_source'] == 'soup':
            soup = BeautifulSoup(raw_json['raw_data'], 'lxml')
            product_tags = soup.select('.search-articles-wrapper .PT_Wrapper.col.span_1_of_4') or \
                soup.select('.PT_Wrapper.col.span_1_of_3')
        else:
            self.source = raw_json['api_url']
            _json = json.loads(raw_json['raw_data'])
            if _json and 'Data' in _json.keys() and 'DirectResults' in _json['Data'] and _json['Data']['DirectResults']:
                product_tags = _json['Data']['DirectResults']

        if product_tags:
            return product_tags
        else: []

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if not isinstance(tag,dict):
                source = str(tag)
                tag = tag.select_one('.PT_Beskr')
                
                if tag:
                    value = tag.get_text().strip()
            else:
                if 'Key' in tag and 'Attributes' in tag['Key'] and 'name' in tag['Key']['Attributes']:
                    source = self.source
                    value = tag['Key']['Attributes']['name'][0]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_prefix = 'https://' + self.__class__.WEBSITE
            if not isinstance(tag,dict):
                source = str(tag)
                tag = tag.select_one('a.hoverLink')
                if tag and tag.has_attr('href'):
                    if url_prefix not in tag.get('href'):
                        value = url_prefix + tag.get('href')
                    else:
                        value = tag.get('href')
            else:
                if 'Key' in tag and 'Attributes' in tag['Key'] and 'url' in tag['Key']['Attributes']:
                    source = self.source
                    link = tag['Key']['Attributes']['url'][0]
                    if url_prefix not in link:
                        value = url_prefix + link
                    else:
                        value =link

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
