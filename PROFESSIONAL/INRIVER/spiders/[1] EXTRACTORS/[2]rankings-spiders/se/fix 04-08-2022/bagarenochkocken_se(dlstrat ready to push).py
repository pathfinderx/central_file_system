from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
    
import json
class BagarenochkockenSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive',
                'Host': 'search.goshopping.dk',
                'Origin': 'https://bagarenochkocken.se',
                'Pragma': 'no-cache',
                'Referer': 'https://bagarenochkocken.se/',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'cross-site',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException(
                'URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code
                if status_code in [200, 201]:
                    try:
                        if self.check_products(res.text):
                            raw_data_list.append(res.text)
                    except:
                        # For unit tests purposes only
                        if res.text == 'Success':
                            raw_data_list.append(res.text)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException(
                    'Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException(
                    'Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://search.goshopping.dk/search/kitchen_sv?term={q_search_string}', # temporary: 50+ items per page. Reactivate the urls below if pagination returns to original form
            # f'https://search.goshopping.dk/search/kitchen_sv?sorting=undefined%7Cundefined&page=2&from=&to=&term={q_search_string}&userId=',
            # f'https://search.goshopping.dk/search/kitchen_sv?sorting=undefined%7Cundefined&page=3&from=&to=&term={q_search_string}&userId=',
            # f'https://search.goshopping.dk/search/kitchen_sv?sorting=undefined%7Cundefined&page=4&from=&to=&term={q_search_string}&userId='
        ]

        return urls
    
    @staticmethod
    def check_products(raw_data):
        json_data = json.loads(raw_data)
        products = json_data.get("Products", [])

        return len(products) > 0
