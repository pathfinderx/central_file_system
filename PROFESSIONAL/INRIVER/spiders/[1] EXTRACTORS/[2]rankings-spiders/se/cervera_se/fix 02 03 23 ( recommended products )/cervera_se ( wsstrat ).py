import logging
import json
from copy import deepcopy

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class CerveraSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.cervera.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data) -> list:
        try: 
            json_data = json.loads(raw_data)
            if ('searchResultZone' in json_data and
                len(json_data['searchResultZone']) > 2):

                return json_data['searchResultZone'][3]['products']

            elif ('recommendations' in json_data and
                'searchpage-nosto-2' in json_data['recommendations']):
                
                return json_data['recommendations']['searchpage-nosto-2']['products']

            else:
                result = self.downloader.get_suggested_products()
                recommended_products = list()

                _json = json.loads(result) if result else None
                
                if (_json and
                    'recommendations' in _json):

                    recommendations_list = list(_json.get('recommendations').values()) # convert arbitrary keys to list
                    recommended_products = [y for x in recommendations_list for y in x['products'] if 'products' in x]

                return recommended_products if recommended_products else []
                
        except Exception as e:
            print('Error at: wsstrat -> get_product_tags() -> with message: ', e)
            return json_data.get("results",{}).get("items",[])

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'attributes' in tag:
                attributes = tag.get("attributes")
                if 'se_title' in attributes:
                        source = str(attributes)
                        value = attributes['se_title'][0]
                else:
                    for attr in attributes:
                        if attr.get("name") == "Name":
                            source = str(self.downloader.get_urls('')[0])
                            value = attr.get("values",[])[0]
                            break
            elif 'name' in tag:
                source = str(tag)
                value = tag['name']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'attributes' in tag:
                attributes = tag.get("attributes")
                if 'manufacturer' in attributes:
                        source = str(attributes)
                        value = attributes['manufacturer'][0]
                else:
                    for attr in attributes:
                        if attr.get("name") == "ManufacturerName":
                            source = str(self.downloader.get_urls('')[0])
                            value= attr.get("values",[])[0]
                            break
            elif 'brand' in tag:
                source = str(tag)
                value = tag['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_pref = 'https://www.cervera.se/produkt/'
            if 'attributes' in tag:
                attributes = tag.get("attributes")
                if 'se_title' in attributes:
                    source = str(attributes)
                    value = attributes['se_url'][0]
                else:
                    for attr in attributes:
                        if attr.get("name") == "UniqueURL":
                            source = str(self.downloader.get_urls('')[0])
                            value = url_pref + attr.get("values",[])[0]
                            break
            elif 'url' in tag:
                source = str(tag)
                value = tag['url']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
