import json
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
import urllib.parse, re
from uuid import uuid4
from bs4 import BeautifulSoup
from typing import Union

class CerveraSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.preserved_search_string = None
        
    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)
        self.preserved_search_string = search_string

        if not isinstance(headers, dict):
            headers = {
                'accept': 'Accept:application/json, text/plain, */*',
                "api-version":"V3",
                'accept-language': 'en-US,en;q=0.9',
                'Origin':'https://www.cervera.se',
                'Referer':'https://www.cervera.se/',
                "user-id":"111.119.187.52_QLFKVGMNZQ",
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "\
                    "(KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'
            }
       
        if not isinstance(cookies, dict):
            cookies = None
        
        if not isinstance(data, dict):
            # data = json.dumps({
            #     "query":str(search_string),
            #     "resultsOptions":{
            #         "sortBy":[{"type":"relevance"}],
            #         "take":120,"skip":0,
            #         "facets":[
            #             {"name":"ManufacturerName",
            #             "attributeName":"ManufacturerName",
            #             "type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}],
            #             "selected":[]},
            #             {"name":"StormName",
            #             "attributeName":"StormName",
            #             "type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}],
            #             "selected":[]},{"name":"Färg",
            #             "attributeName":"Färg","type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}],
            #             "selected":[]},
            #             {"name":"Designer",
            #             "attributeName":"Designer",
            #             "type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}]},
            #             {"name":"Material",
            #             "attributeName":"Material","type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}],"selected":[]},
            #             {"name":"Diameter",
            #             "attributeName":"Diameter","type":"distinct",
            #             "sortBy":[{"type":"item","order":"asc"}],
            #             "selected":[]},
            #             {"name":"PriceSaleExclVat",
            #             "attributeName":"PriceSaleExclVat",
            #             "type":"range"}
            #         ]
            #     }
            # })

            # if search_string not in ['termoskanna bäst i test','termoskopp']:
            data = None
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36'
            }


        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                if data is None:
                    res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)
                else:
                    res = self.requester.post(urls[page], timeout=timeout, headers=headers, cookies=cookies, data=data)
                status_code = res.status_code

                if status_code in [200, 201]:
                    if 'Åh nej! Här var det tomt' in res.text:
                        raise DownloadFailureException('Download failed - Unhandled Exception')
                    else:
                        raw_data_list.append(res.text)
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        #temp bypass
        if search_string == 'drinksglas':
            search_string = 'dricksglas'
        urls = [
            # 'https://cervera.54proxy.com/search',
            f'https://w458e3c2c.api.esales.apptus.cloud/api/v2/panels/search-desktop/search-as-you-type?locale=sv-SE&filter=(market%3A%27SE%27%20AND%20(flag_id%3A%27300%27%20AND%20flag_id%3A%276736%27%20AND%20flag_id%3A%274177%27))&search_phrase={search_string}&root_category=se_category_code%3A%27root%27&search_hits_first=1&search_hits_last=40&facets=&product_sort_by=relevance%20desc&notify=true&search_attributes=product%5Bse_category%2Cse_color%2Cse_colors%2Cse_description%2Cdesigner%2Cmanufacturer%2Cse_material%2Cproduct_id%2Cproduct_key%2Cse_title%5D%2Ccategory%5Bdisplay_name%5D&fixed_facets=in_stock%2Cmanufacturer_value%2Cse_category%2Cselling_price&dynamic_facets=designer%2Cdiameter%2Cse_color%2Cse_material&esales.market=SE&esales.customerKey=1412eb5e-86a4-4cca-8bf6-5f71ca4927ef&esales.sessionKey=a255e46b-b446-4a53-93ef-99c36cd599ac'
        ]

        if search_string == 'grytset':
            urls = [
                f'https://w458e3c2c.api.esales.apptus.cloud/api/v2/panels/search-desktop/search-as-you-type?locale=sv-SE&facets=&product_sort_by=relevance%20desc&max_facets=10&fixed_facets=in_stock%2Cmanufacturer_value%2Cse_category%2Cselling_price&dynamic_facets=designer%2Cdiameter%2Cse_color%2Cse_material&search_phrase={search_string}&notify=false&search_hits_first=1&search_hits_last=40&filter=(market%3A%27SE%27%20AND%20(flag_id%3A%276736%27%20AND%20flag_id%3A%274177%27))&root_category=se_category_code%3A%27root%27&search_attributes=product%5Bse_category%2Cse_color%2Cse_colors%2Cse_description%2Cse_designer%2Cse_material%2Cse_title%2Cmanufacturer%2Cproduct_id%2Cproduct_key%5D%2Ccategory%5Bdisplay_name%5D&esales.market=SE&esales.customerKey=c9d434a6-e93a-4cca-a02c-a977cc362841&esales.sessionKey=9f1f26d6-b835-4eac-aacb-7da16830b18a'
            ]
        
        # For Suggested products -- uncomment or remove keywords when site returns to normal results
        if search_string in ['termoskanna bäst i test','termoskopp']:

            # default message dictionary for termoskanna bäst i test
            message_dict = {"elements":["searchpage-nosto-2","searchpage-nosto-1"],"tags":[],"custom_fields":[],"categories":[],"response_mode":"JSON_ORIGINAL","url":"https://www.cervera.se/search?query=termoskanna+b%C3%A4st+i+test","events":[["is","termoskanna bäst i test"]],"page_type":"search","cart":[],"restore_link":'null'}
            
            if search_string == 'termoskopp':
                message_dict = {"elements":["searchpage-nosto-2","searchpage-nosto-1"],"tags":[],"custom_fields":[],"categories":[],"response_mode":"JSON_ORIGINAL","url":"https://www.cervera.se/search?query=termoskopp","events":[["is",""]],"page_type":"search","cart":[],"restore_link":'null'}
            
            params = {
                'c': '626243c0b4fd7c180cd61608',
                'm': 'ssfgvcft',
                'message': json.dumps(message_dict)
            }
            urls = [
                'https://connect.nosto.com/ev1?'+urllib.parse.urlencode(params)
            ]

        if search_string in ['kaffekanne']:
            params = {
                "locale": "sv-SE",
                "filter": "(market:'SE' AND (flag_id:'300' AND flag_id:'6736' AND flag_id:'4177'))",
                "search_phrase": "kaffekvarn",
                "root_category": "se_category_code:'root'",
                "search_hits_first": "1",
                "search_hits_last": "40",
                "facets": "",
                "product_sort_by": "relevance desc",
                "notify": "true",
                "search_attributes": "product[se_category,se_color,se_colors,se_description,designer,manufacturer,se_material,product_id,product_key,se_title],category[display_name]",
                "fixed_facets": "in_stock,manufacturer_value,se_category,selling_price",
                "dynamic_facets": "designer,diameter,se_color,se_material",
                "esales.market": "SE",
                "esales.customerKey": "66ed3b56-bbbb-49f1-b95d-83a753b377c6",
                "esales.sessionKey": "cc82d480-861c-41bc-98d1-fffb21a717b4"
            }
            
            urls = [
                'https://w458e3c2c.api.esales.apptus.cloud/api/v2/panels/search-desktop/search-as-you-type?{}'.format(urllib.parse.urlencode(params))
            ]
        return urls

    def get_elements_from_initial_data(self) -> Union[None, list]:
        try:
            script_tag, _json = None, None
            elements = list()

            if self.preserved_search_string:
                query_string =  self.preserved_search_string.replace(' ', '+')
                initial_url = f"https://www.cervera.se/search?query={query_string}"

                res = self.requester.get(initial_url, timeout=10000, headers=None, cookies=None)

            if res.status_code in [200, 201]:
                soup = BeautifulSoup(res.text, 'lxml')
                script_tag = soup.select_one('script[id="__NEXT_DATA__"]')
            else:
                return None

            if script_tag:
                _json = json.loads(script_tag.get_text(strip=True))

            if (_json and
                'props' in _json and
                'pageProps' in _json['props'] and
                'searchCmsData' in _json['props']['pageProps'] and
                'noResultBlock' in _json['props']['pageProps']['searchCmsData'] and
                isinstance(_json['props']['pageProps']['searchCmsData']['noResultBlock'], list)):

                element_container = _json['props']['pageProps']['searchCmsData']['noResultBlock']

                for x in element_container:
                    if ('nostoId' in x):
                        elements.append(x['nostoId'])

            return elements if elements else None

        except Exception as e:
            print('Error at: dlstrat -> get_api_data -> with message: ', e) 

    def get_suggested_products(self, timeout=10, headers=None, cookies=None) -> str:
        try:
            elements = self.get_elements_from_initial_data()
            api_url = None

            if (elements and self.preserved_search_string):
                query_string =  self.preserved_search_string.replace(' ', '+')
                initial_url = f"https://www.cervera.se/search?query={query_string}"
                converted_url = urllib.parse.quote(initial_url)
                converted_elements = urllib.parse.quote(str(elements).replace(' ', '').replace("'", '"'))
                
                if (converted_url and converted_elements):
                    api_url = f"https://connect.nosto.com/ev1?c=63dc67b1b4660e73a028a783&m=ssfgvcft&message=%7B%22elements%22%3A{converted_elements}%2C%22tags%22%3A%5B%5D%2C%22custom_fields%22%3A%5B%5D%2C%22categories%22%3A%5B%5D%2C%22response_mode%22%3A%22JSON_ORIGINAL%22%2C%22url%22%3A%22{converted_url}%22%2C%22events%22%3A%5B%5B%22is%22%2C%22vannkoker%22%5D%5D%2C%22page_type%22%3A%22search%22%2C%22cart%22%3A%5B%5D%2C%22restore_link%22%3Anull%7D"

            if api_url:
                res = self.requester.get(api_url, timeout=None, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    return res.text

        except Exception as e:
            print('Error at: dlstrat -> get_api_data -> with message: ', e)