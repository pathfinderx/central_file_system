import json

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class AptekageminiPlDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
                "content-type": "application/x-www-form-urlencoded",
                "Accept": "*/*",
                "Origin": "https://www.aptekagemini.pl",
                "Sec-Fetch-Site": "cross-site",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Dest": "empty",
            }

        if not isinstance(cookies, dict):
            cookies = None

        if data is None:
            data = self.get_body(search_string)
        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException(
                'URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                if data is None:
                    res = self.requester.get(
                        urls[page], timeout=timeout, headers=headers, cookies=cookies)
                else:
                    res = self.requester.post(
                        urls[page], timeout=timeout, headers=headers, cookies=cookies, data=data.encode('utf-8'))
                status_code = res.status_code

                if status_code in [200, 201]:
                    if self.has_products(res.text):
                        raw_data_list.append({'data':res.text, 'source': urls[page]})

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException(
                    'Pagination failed - Page {}'.format(str(page+1)))

            except Exception:
                raise DownloadFailureException(
                    'Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://nnwykhz90y-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(4.8.5)%3B%20Browser%20(lite)%3B%20instantsearch.js%20(4.14.1)%3B%20Vue%20(2.6.12)%3B%20Vue%20InstantSearch%20(3.4.3)%3B%20JS%20Helper%20(3.4.4)&x-algolia-api-key=MjI0OWM3NWUwN2VhZDk0YTIyZTE1MzQ1Y2M3MmFlYTE2Yzg3YzJmYjY1YzhjMDVmNTJlODY1YWEwNzMxNDNkNWZpbHRlcnM9c3Rhbl96ZWQlM0QxK09SK3N0YW5femVkJTNENStPUitzdGFuX3plZCUzRDI%3D&x-algolia-application-id=NNWYKHZ90Y'
        ]

        return urls

    def get_body(self, search_string):
        q_search_string = search_string.replace(' ', '+')
        return '{"requests":[{"indexName":"prod_SPRYKER","params":"hitsPerPage=100&clickAnalytics=true&userToken=1039039601-1626799678&analyticsTags=%5B%22ai%22%5D&query='\
            + q_search_string\
            + '&getRankingInfo=true&analytics=true&maxValuesPerFacet=100&highlightPreTag=__ais-highlight__&highlightPostTag=__%2Fais-highlight__&page=0&facets=%5B%22labels.name%22%2C%22brand%22%2C%22producer%22%2C%22price%22%2C%22attributes.type%22%2C%22attributes.material%22%2C%22attributes.effect%22%2C%22attributes.form%22%2C%22attributes.taste%22%2C%22attributes.application%22%2C%22attributes.skin-type%22%2C%22attributes.use-time%22%2C%22attributes.problem%22%2C%22attributes.key-ingredient%22%2C%22attributes.hair-type%22%2C%22attributes.colour%22%2C%22attributes.sex%22%2C%22attributes.shape%22%2C%22attributes.size%22%2C%22attributes.special-feature%22%2C%22attributes.age%22%2C%22attributes.body-part%22%2C%22attributes.spf%22%2C%22attributes.registration%22%2C%22attributes.trends%22%2C%22attributes.optical-power%22%2C%22hierarchicalCategories.lvl0%22%5D&tagFilters="}]}'

    @staticmethod
    def has_products(raw_data):
        has_products = False

        try:
            data = json.loads(raw_data)
            if data and data['results']:
                has_products = len(data['results'][0]['hits']) > 0
        except Exception as e:
            if raw_data and not raw_data.startswith('{'):
                # Most likely caused by mock requests
                has_products = True
            else:
                print(f'An error has occurred while parsing response: {e}')

        return has_products
