import logging, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ZooSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.zoo.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)
        self.isBrandFounded, self.founded_brand, self.title = False, None, None

        # brand list for company: M00 (Royal Canin) -> query: (SELECT * FROM rcms_brands rb WHERE is_active = '1')
        # update the brand_list variable if any changes in db. Must follow the active brand list
        self.brand_list = ["Acana","Brit Care","Canagan","Eukanuba","Granngården","Hill's","Hills","Nutrima","Orijen","Pure Natural","Purina","Royal Canin"]

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()

        last_rank = start_rank
        
        soup = BeautifulSoup(raw_data, "html.parser")
        tags = self.get_product_tags(soup)
        
        for tag in tags:
            result = deepcopy(template)
            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tag)

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tag)

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tag)

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break
            last_rank += 1

        return results, last_rank

    def get_product_tags(self, data):
        # tags = data['product_data']
        tags = data.find('ul', {'class': 'product-items'})
        if tags:
            products = tags.select('li')
            return products

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            title, merged_title = tag.select('div.product-item-name'), str()
            merged_title = title[1].text.strip().replace("\n", "") if title else None

            if merged_title:
                source, value = str(tag), merged_title 
                
                # search if brand(from self.brand_list) was found in title
                self.title = merged_title
                for declared_brand in self.brand_list:
                    if declared_brand in merged_title:
                        self.isBrandFounded, self.founded_brand = True, declared_brand # if found then use that brand for get_brand()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # if brand is found in title then use that brand(from self.brand_list)
            if self.isBrandFounded and self.founded_brand:
                source, value = str(tag), self.founded_brand

            else:
                # if brand is not found in title
                brand_tag = tag.find('div', {'class': 'product-item-brand-inf6'})
                if brand_tag:
                    brand_text = brand_tag.select_one('a').text.strip().replace("\n", "")
                    brand_split = brand_text.replace("\n", "").split()

                    # if the scraped brand matched any values in self.brand_list
                    if any(x in brand_text for x in self.brand_list): # iterate brand_list to check if exists in scraped brand
                       for index, value in enumerate(self.brand_list): 
                           if (value in brand_text):
                               source, value = str(brand_tag), value

                    # if the whole word of scraped brand was found in title but not in brand_list
                    elif brand_text in self.title:
                        source, value = str(brand_tag), brand_text

                    else: # if first word in brand has a match inside the title
                        for letter in self.title.split():
                            if (letter == brand_split[0]):
                                source, value = str(brand_tag), brand_split[0]
                                break

                        # if any occurence of first word of brand in the title is not found
                        # if any(x in brand_split[0] for x in self.title.split()):
                        #     source, value = str(tag), brand_text

            # Don't hesitate to remove these lines of below
            # if something wen't wrong with the other keywords
            if '.' in value:
                source = Defaults.GENERIC_NOT_FOUND.value
                value = Defaults.GENERIC_NOT_FOUND.value            

            self.isBrandFounded, self.founded_brand, self.title = False, None, None # return to default state for next product

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url = tag.find('div', {'class': 'product-item-brand-inf6'})
            if url:
                url_tag = url.select_one('a')
                source, value = str(url_tag), url_tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
