import logging
from copy import deepcopy
import json

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class CykloteketSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.cykloteket.se'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data, start_rank, limit):
        assert isinstance(start_rank, int)
        assert isinstance(limit, int)

        results = []
        template = get_result_base_template()
        tags = self.get_product_tags(raw_data)

        last_rank = start_rank

        for idx in range(0, len(tags)):
            result = deepcopy(template)

            # Assign rank
            result["rank"] = str(last_rank)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(tags[idx])

            # Extract Brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(tags[idx])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(tags[idx])

            results.append(result)

            # Check if limit is hit
            if last_rank >= limit:
                break

            last_rank += 1

        return results, last_rank

    def get_product_tags(self, raw_data):

        try:
            json_data = json.loads(raw_data)
            soup = BeautifulSoup(json_data['result'], "lxml")
            tags = soup.select('article')

            if tags:
                return tags
            else:
                return []
        except Exception as e:
            self.logger.exception(e)

            soup = BeautifulSoup(raw_data.strip(), 'lxml')
            tag = soup.find_all('div', class_="card-body h-100 py-2 px-3 d-flex justify-content-between flex-column flex-grow-3")
            if tag:
                return tag
            else:
                return []
                

    def get_title(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_title = tag.select_one('h3.card-title')
            if tag_title:
                source, value = str(tag_title), tag_title.text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No brand for this website
        return source, value

    def get_url(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag_url = tag.select_one('a')
            if tag_url and tag_url.has_attr('href'):
                source = str(tag_url)
                value = tag_url.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value
