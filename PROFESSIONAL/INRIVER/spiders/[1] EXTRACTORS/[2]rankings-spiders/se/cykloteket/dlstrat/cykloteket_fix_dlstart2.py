from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException
from bs4 import BeautifulSoup
import json
class CykloteketSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=100, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = self.get_headers()

        if not isinstance(cookies, dict):
            cookies = None
        body_data = self.set_body_params(search_string)

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except Exception:
            raise URLPreparationFailureException(
                'URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = None
                if body_data:
                    res = self.requester.post(
                        urls[page], data=body_data, timeout=timeout, headers=headers, cookies=cookies)
                else:
                    res = self.requester.get(
                        urls[page], timeout=timeout, headers=headers, cookies=cookies)
                status_code = res.status_code

                if status_code in [200, 201]:
                    raw_data_list.append(res.text)

                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException(
                    'Pagination failed - Page {}'.format(str(page+1)))

            except Exception as e:
                print(e)
                raise DownloadFailureException(
                    'Download failed - Unhandled Exception')

        return raw_data_list

    # Private methods
    @staticmethod
    def get_urls(search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [
            f'https://www.addwish.com/api/v1/search/partnerSearch'
        ]

        if search_string == 'CYKELTRAINER':
            search_string = search_string.lower()
            urls = [
                f'https://www.cykloteket.se/cykeltillbehor/{search_string}s'
            ]

        return urls

    def set_body_params(self, serach_string):
        return {
            'key': '6ada9ea9-7355-46fd-8bd7-2c0262786150',
            'q': serach_string,
            'product_count': 100,
            'product_start': 0,
            'category_count': 0,
            'category_start': 0,
            'id': 17570,
            'return_filters': False,
            'websiteUuid': '0aa1b5db-8ede-42cb-b6c0-18c50e9c1cdc',
            'hello_retail_id': '616866994655ee47ca8f2815',
        }

    def get_headers(self):
        return {
            "authority": "www.addwish.com",
            "pragma": "no-cache",
            "cache-control": "no-cache",
            "sec-ch-ua": '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99""',
            "sec-ch-ua-mobile": "?0",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36",
            "sec-ch-ua-platform": '"Windows"',
            "content-type": "application/x-www-form-urlencoded",
            "accept": "*/*",
            # "origin:https": "//www.cykloteket.se",
            "sec-fetch-site": "cross-site",
            "sec-fetch-mode": "cors",
            "sec-fetch-dest": "empty",
            # "referer:https": "//www.cykloteket.se/",
            "accept-language": "en-US,en;q=0.9",
        }