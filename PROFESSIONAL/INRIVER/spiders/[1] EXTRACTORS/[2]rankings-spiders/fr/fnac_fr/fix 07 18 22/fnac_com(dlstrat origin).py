import urllib, json, requests
import html
import cloudscraper
from urllib.parse import quote_plus
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

class FnacComDownloadStrategy(DownloadStrategy):

    LIMIT = 25 #Reduced limit to lessen pages being downloaded

    def __init__(self, requester):
        self.requester = requester

        # shared state across method calls, provides isolation
        self.__state = None
        self.autoRerunCounter = 0 # attempts counter to prevent auto rerun infinite loop incase if none of the requests works
        self.autoRerunMaxAttempts = 10 # maximum tolerable recursion

    def download(self, search_string, timeout=10000, headers=None, cookies=None, data=None):
        # NOTE: Reset the state on every download
        # every download is different state of the instance
        self.__state = {}

        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.5",
                "Cache-Control": "max-age=0",
                "Connection": "keep-alive",
                "Host": "www.fnac.com",
                "Sec-Fetch-Dest":"document",
                "Sec-Fetch-Mode":"navigate",
                "Sec-Fetch-Site": "none",
                "Sec-Fetch-User":"?1",
                "TE": "trailer",
                "Upgrade-Insecure-Requests":"1",
                "User-Agent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0",
                # "Cookie":"akavpau_FRPRD_FNACCOM=1647867608~id=ff659ccda28c26e2746df3c4b06c7487; QueueITAccepted-SDFrts345E-V3_frprdfnaccom=EventId%3Dfrprdfnaccom%26QueueId%3D5a2ac48f-2f6f-4f9c-9a13-393c729302b0%26RedirectType%3Dsafetynet%26IssueTime%3D1647867296%26Hash%3D50f7cf6b75699ce3d077c86e0802ee6e73426fefecf28f04b6a40086c9305f15; datadome=oi9b07T4Z.SkAfuQUDeeDYb5r2EaA~2CWRS3QIUjz6VAktaNwjqYRSkjQ.GipL.XWReNnHS9.9UNOV99uQz4PgPaRRKC6LFf~GwJkMiyegs-4PpxRChM7RjutnsQlKf; ORGN=FnacAff; OrderInSession=0; UID=0f89abaf7-33cb-434f-9dc6-d…47867279463; OptanonAlertBoxClosed=2022-03-21T12:54:41.266Z; s_sq=%5B%5BB%5D%5D; kvn=Va; fd_vls_address=None; _gcl_au=1.1.244554151.1647867283; sto__session=1647867283553; sto__count=2; sto__vuid=0f89abaf7-33cb-434f-9dc6-d639f9b6a152; rlvt_tmpId=cl10pmwtk000024a1oexayrwc; SDMTemplateSearch=list; ItemPerPagelistTemplateSearch=20; s_vi=[CS]v1|311C3AD0BBA8C41D-40001C3D276581BF[CE]; LSI=99=AAB#WBQNC#A0b-J|B; tc_ut_uuid=2022032120550211252225428; tc_ut_suid=11252225428; kameleoonVisitorCode=_js_u2nwcq06t2ynuaw2"
            }

        if not isinstance(cookies, dict):
            cookies = None

        self.__state['search_string'] = search_string
        self.__state['raw_data'] = []
        self.__state['num_products'] = 0
        self.__state['page'] = 1
        self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = None
        while True:
            url = self.__generate_url()
        
            try:
                self.autoRerunCounter += 1
                if self.autoRerunCounter == self.autoRerunMaxAttempts:
                    break
                else:
                    pass

                body = self.__generate_url_body()

                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(body))

                if response.status_code in [200, 201]:
                    print('I just entered the self.requester')
                    soup = None
                    try:
                        soup = self.process_json(response.text)
                    except:
                        # the server sometimes return XML format
                        soup = self.process_xml(response.text)

                    self.__state['bs'] = soup
                    self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                    self.__state['num_products'] += self.__state['current_num_products']
                    self.stateCopy = self.__state # modifiable self.__state object in console (testing purposes)

                    if not self.__has_next_page():
                        break

                    self.__state['page'] += 1

                elif response.status_code in [403]:
                    response = requests.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(body))

                    if response.status_code in [200, 201]:
                        print('I just entered the normal requests')
                        soup = None
                        try:
                            soup = self.process_json(response.text)
                        except:
                            # the server sometimes return XML format
                            soup = self.process_xml(response.text)

                        self.__state['bs'] = soup
                        self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                        self.__state['num_products'] += self.__state['current_num_products']
                        self.stateCopy = self.__state # modifiable self.__state object in console (testing purposes)

                        if not self.__has_next_page():
                            break

                        self.__state['page'] += 1
                    elif response.status_code in [403]:
                        print('I just entered the cloudscraper requests')
                        session = requests.session()
                        scraper = cloudscraper.create_scraper(sess=session)
                        res = scraper.post(url, data=json.dumps(body))

                        if res.status_code in [200, 201]:
                            print('I just entered the normal requests')
                            soup = None
                            try:
                                soup = self.process_json(res.text)
                            except:
                                # the server sometimes return XML format
                                soup = self.process_xml(res.text)
                                
                            self.__state['bs'] = soup
                            self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                            self.__state['num_products'] += self.__state['current_num_products']
                            self.stateCopy = self.__state # modifiable self.__state object in console (testing purposes)

                            if not self.__has_next_page():
                                break

                            self.__state['page'] += 1
                    else:
                        raise PaginationFailureException('Auto rerun is coming. . . . .')
                else: 
                    raise PaginationFailureException('Auto rerun is coming. . . . .')

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(self.__state['page']+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return self.__state['raw_data']

    def __generate_url(self):
        return 'https://www.fnac.com/searchapi/tools/updateproductslist'

    def __generate_url_body(self):
        body = {
            'data':{
                'environment': {
                    'url': 'https://www.fnac.com/SearchResult/ResultList.aspx?PageIndex={page}&Search={search_string}&sft=1&sl'.format(page=self.__state['page'], search_string=quote_plus(self.__state['search_string'])),
                    'query': {
                        'text': self.__state['search_string'],
                        'type': 'Standard',
                        'disabled_keywords': 0,
                        'search_debug': 0,
                        'first_time': True,
                        'versionArbo': '1.0.3.1',
                        'nb_element_per_page': 20,
                        'page_index': str(self.__state['page']),
                        'sort_option': 0,
                        'sort_direction': 0,
                        'new_facet': 'True',
                        'double_facet': 'True',
                        'id': 0,
                        'multibuy_data': {}
                    }
                },
                'context': {
                    'view_mode': {
                        'current': 'list',
                        'is_multibuy': 'false'
                    }
                }
            },
            'update': {
                'request': {
                    'type': 'tools/updateproductslist',
                    'value': str(self.__state['page'])
                }
            }
        }

        return body

    def __has_next_page(self):
        if 'num_products' in self.__state and 'TOTAL_PRODUCT_COUNT_FOUND' in self.__state:
            
            if self.__state['num_products'] >= FnacComDownloadStrategy.LIMIT or self.__state['num_products'] >= self.__state['TOTAL_PRODUCT_COUNT_FOUND'] or (self.__state['current_num_products'] < 20 and self.__state['page'] == 2):
                return False

        return True

    def __count_products(self, tag):
        products = tag.select('div.Article-item.js-Search-hashLinkId')
        return len(products)

    def get_total_count(self, json_data):
        return json_data['data']['environment']['nb_results_number']


    def parse_xml(self, raw_data):
        soup = BeautifulSoup(raw_data, 'xml')
        html_tag = soup.select_one('html')
        if html_tag:
           html_text =  html.unescape(html_tag.text)
           return html_text
        return None

    def process_xml(self, response_text):
        raw = self.parse_xml(response_text)
        self.__state['raw_data'].append(raw)
        soup = BeautifulSoup(raw, 'lxml')

        if not self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
            xml = BeautifulSoup(response_text, 'xml')
            tag = xml.select_one('nb_results_number')
            if tag:
               self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = int(tag.text)
               
        return soup

    def process_json(self, response_text):
        json_data = json.loads(response_text)
        self.__state['raw_data'].append(json_data['data']['content']['body']['html'])

        if not self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
            self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = self.get_total_count(json_data)

        soup = BeautifulSoup(json_data['data']['content']['body']['html'], 'lxml')
        return soup