import urllib, json, requests
import cloudscraper
from urllib.parse import quote_plus
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException

class FnacComDownloadStrategy(DownloadStrategy):

    LIMIT = 100

    def __init__(self, requester):
        self.requester = requester

        # shared state across method calls, provides isolation
        self.__state = None

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        # NOTE: Reset the state on every download
        # every download is different state of the instance
        self.__state = {}

        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'content-type': 'application/json',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest'
            }

        if not isinstance(cookies, dict):
            cookies = None

        self.__state['search_string'] = search_string
        self.__state['raw_data'] = []
        self.__state['num_products'] = 0
        self.__state['page'] = 1
        self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = None

        while True:
            url = self.__generate_url()
            try:
                body = self.__generate_url_body()
                response = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(body))

                if response.status_code in [200, 201]:
                    print('I just entered the self.requester')
                    json_data = json.loads(response.text)
                    self.__state['raw_data'].append(json_data['data']['content']['body']['html'])

                    if not self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
                        self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = self.__get_total_count(json_data)

                    self.__state['bs'] = BeautifulSoup(json_data['data']['content']['body']['html'], 'lxml')
                    self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                    self.__state['num_products'] += self.__state['current_num_products']

                    if not self.__has_next_page():
                        break

                    self.__state['page'] += 1

                elif response.status_code in [403]: # patch 
                    search_string = self.__state['search_string'].replace(" ", "+")
                    url = f'https://www.fnac.com/SearchResult/ResultList.aspx?PageIndex=2&Search={search_string}&sft=1&sl'
                    res = requests.get(url, timeout=timeout, headers=headers, cookies=cookies)

                    print(res.status_code)

                elif response.status_code in [403]:
                    response = requests.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(body))

                    if response.status_code in [200, 201]:
                        print('I just entered the normal requests')
                        json_data = json.loads(response.text)
                        self.__state['raw_data'].append(json_data['data']['content']['body']['html'])

                        if not self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
                            self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = self.__get_total_count(json_data)

                        self.__state['bs'] = BeautifulSoup(json_data['data']['content']['body']['html'], 'lxml')
                        self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                        self.__state['num_products'] += self.__state['current_num_products']

                        if not self.__has_next_page():
                            break

                        self.__state['page'] += 1
                    elif response.status_code in [403]:
                        print('I just entered the cloudscraper requests')
                        session = requests.session()
                        scraper = cloudscraper.create_scraper(sess=session)
                        res = scraper.post(url, data=json.dumps(body))

                        if response.status_code in [200, 201]:
                            print('I just entered the normal requests')
                            json_data = json.loads(response.text)
                            self.__state['raw_data'].append(json_data['data']['content']['body']['html'])

                            if not self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
                                self.__state['TOTAL_PRODUCT_COUNT_FOUND'] = self.__get_total_count(json_data)

                            self.__state['bs'] = BeautifulSoup(json_data['data']['content']['body']['html'], 'lxml')
                            self.__state['current_num_products'] = self.__count_products(self.__state['bs'])
                            self.__state['num_products'] += self.__state['current_num_products']

                            if not self.__has_next_page():
                                break

                            self.__state['page'] += 1
                    else:
                        raise PaginationFailureException('Auto rerun is coming. . . . .')
                else:
                    raise PaginationFailureException('Auto rerun is coming. . . . .')

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(self.__state['page']+1)))

            except:
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return self.__state['raw_data']

    def __generate_url(self):
        return 'https://www.fnac.com/searchapi/tools/updateproductslist'

    def __generate_url_body(self):
        body = {
            'data':{
                'environment': {
                    'url': 'https://www.fnac.com/SearchResult/ResultList.aspx?PageIndex={page}&Search={search_string}&sft=1&sl'.format(page=self.__state['page'], search_string=quote_plus(self.__state['search_string'])),
                    'query': {
                        'text': self.__state['search_string'],
                        'type': 'Standard',
                        'disabled_keywords': 0,
                        'search_debug': 0,
                        'first_time': True,
                        'versionArbo': '1.0.3.1',
                        'nb_element_per_page': 20,
                        'page_index': str(self.__state['page']),
                        'sort_option': 0,
                        'sort_direction': 0,
                        'new_facet': 'True',
                        'double_facet': 'True',
                        'id': 0,
                        'multibuy_data': {}
                    }
                },
                'context': {
                    'view_mode': {
                        'current': 'list',
                        'is_multibuy': 'false'
                    }
                }
            },
            'update': {
                'request': {
                    'type': 'tools/updateproductslist',
                    'value': str(self.__state['page'])
                }
            }
        }

        return body

    def __has_next_page(self):
        if self.__state['num_products'] >= FnacComDownloadStrategy.LIMIT or self.__state['num_products'] >= self.__state['TOTAL_PRODUCT_COUNT_FOUND']:
            return False

        return True

    def __count_products(self, tag):
        products = tag.select('div.Article-item.js-Search-hashLinkId')
        return len(products)

    def __get_total_count(self, json_data):
        return json_data['data']['environment']['nb_results_number']