import json
import re

from bs4 import BeautifulSoup
from urllib.parse import quote
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException, PaginationFailureException, \
    URLPreparationFailureException


class BoulangerComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, search_string, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(search_string, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data_list = []

        try:
            urls = self.get_urls(search_string)

        except:
            raise URLPreparationFailureException('URL preparation failed - Unhandled Exception')

        for page in range(0, len(urls)):
            try:
                res = self.requester.get(urls[page], timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code in [200, 201]:
                    # if 'redirection' in res.url:
                    #     url = res.url.split('#')[0]+'&viewSize=100'
                    #     res = self.requester.get(url)
                        
                    raw_data_list.append(res.text)
                    
                else:
                    raise PaginationFailureException()

            except PaginationFailureException:
                raise PaginationFailureException('Pagination failed - Page {}'.format(str(page+1)))

            except Exception as e:
                print(e)
                raise DownloadFailureException('Download failed - Unhandled Exception')

        return raw_data_list

    def get_urls(self, search_string):
        q_search_string = search_string.replace(' ', '+')
        urls = [f'https://www.boulanger.com/resultats?tr={q_search_string}&viewSize=100']
        
        if 'enceinte connectée' in search_string:
            urls = ['https://www.boulanger.com/c/assistant-vocal#tr=enceinte%20connect%E9e_redirection']
        if 'CLAVIER MÉCANIQUE'.lower() in search_string.lower():
            urls = ['https://www.boulanger.com/c/nav-filtre/clavier-pave-numerique?facettes_clavier_____type_de_clavier~me9canique#tr=CLAVIER%20MECANIQUE_redirection']

        if 'écouteurs' in search_string:
            q_search_string = search_string.replace('é', 'e')
            urls = [f'https://www.boulanger.com/resultats?tr={q_search_string}&viewSize=100']
        
        if 'Souris PC' in search_string:
            urls = [f'https://www.boulanger.com/c/souris#tr=SOURIS%20PC_redirection']
        
        if 'Chaise de bureau' in search_string:
            urls = [f'https://www.boulanger.com/c/fauteuil-gamer#tr=Chaise%20de%20bureau_redirection']

        if 'Clavier'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/clavier-pave-numerique#tr=clavier_redirection']

        if 'Manette Xbox'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/accessoire-xbox-one/caracteristiques_generales_____type_d_accessoire_pour_facette~manette#tr=manette+xbox_redirection']
            
        if 'casque sans fil'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/casque-bluetooth#tr=casque+sans+fil_redirection']
            
        if 'enceinte bluetooth'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/enceinte-portable/aggregationType_type_d_enceinte_____concatener_oui~AND/type_d_enceinte_____concatener_oui~enceinte20bluetooth#tr=enceinte%20bluetooth_redirection']

        if 'enceinte bluetooth portable'.lower() == search_string.lower():
           urls = [f'https://www.boulanger.com/c/enceinte-portable/aggregationType_type_d_enceinte_____concatener_oui~AND/type_d_enceinte_____concatener_oui~enceinte20bluetooth#tr=enceinte%20bluetooth%20portable_redirection']
        
        if 'enceinte sans fil'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/enceinte-portable#tr=enceinte+sans+fil_redirection']
        
        if 'mini enceinte bluetooth'.lower() == search_string.lower():
            urls = [f'https://www.boulanger.com/c/enceinte-portable/aggregationType_type_d_enceinte_____concatener_oui~AND/type_d_enceinte_____concatener_oui~enceinte20bluetooth#tr=mini%20enceinte%20bluetooth_redirection']
        
        return urls