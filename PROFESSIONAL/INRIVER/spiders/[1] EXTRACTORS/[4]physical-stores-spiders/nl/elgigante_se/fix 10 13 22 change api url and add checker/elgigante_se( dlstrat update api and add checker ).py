import urllib
import json, re

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class ElgigantenSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self._url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None, productstore=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}

        if not isinstance(cookies, dict):
            cookies = None

        if not self._url:
            self._url = url

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [307]: # temporary fix, allow 302 redirect
                        is_redirect = True
                        status_code = item.status_code
                        break
                
                if productstore:
                    if res.text:
                        return res.text

                if not is_redirect:
                    if 'meta property="og:type" content="product"' in res.text or 'class="pdp__head"' in res.text: # PDP Checker
                        return res.text
                    else:
                        raise DownloadFailureException()

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def download_all_store_api(self):
        # stores = []
        # _store_name = []
        # url = 'https://www.elgiganten.se/cxorchestrator/dk/api?operationName=getAllStores&variables=%7B%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2276e4a18bbf360e52936a539a46bd4920b71af0bb69dfc553463d9c81fab27e08%22%7D%7D'
        # url = 'https://www.elgiganten.se/cxorchestrator/se/api?appMode=b2c&user=anonymous&operationName=getAllStores&variables=%7B%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2276e4a18bbf360e52936a539a46bd4920b71af0bb69dfc553463d9c81fab27e08%22%7D%7D'
        url = 'https://www.elgiganten.se/cxorchestrator/se/api?getAllStores&appMode=b2c&user=anonymous&operationName=getAllStores&variables=%7B%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2217a1e542677391a9fbae9df196cb565ec643d1a55cd42943feeca916a242660f%22%7D%7D'
        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'authorization': '',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'referer': self._url,
            'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'x-app-checkout': 'false',
            'x-app-mode': 'b2bExVat',
            'x-authorization': '',
            'x-instana-t': '550b7dc447679f53',
            'x-is-anonymous': 'true',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                            ' Chrome/79.0.3945.130 Safari/537.36'
            }
        cookies = self._get_cookies()
        _json = []
        raw_data = None
        try:
            res = self.requester.get(url, timeout=10, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.text
                
            _json, allStores = json.loads(raw_data), None

            if ('data' in _json and
                'allStores' in _json['data']):
                allStores = _json['data']['allStores']
            
            # for _list in _json:
            #     if not stores:
            #         stores.append(_list)
            #         _store_name.append(_list['name']) #list all store to use it to filter after and prevent duplicate data
            #     elif _list['name'] not in _store_name:
            #         stores.append(_list)
            #         _store_name.append(_list['name']) #list all store to use it to filter after and prevent duplicate data

            # _json = list(dict.fromkeys(_json.items()))
        except Exception as e:
            pass
        
        return allStores if allStores else None

    def download_product_stores_api(self, _sku):
        articleNumber = None
        pattern = re.compile(r'\d{6}', re.IGNORECASE)
        result = pattern.findall(self._url)
        if result and not articleNumber:
            articleNumber = result[0]
            
        pattern = re.compile(r'\d{5}', re.IGNORECASE)
        result = pattern.findall(self._url)
        if result and not articleNumber:
            articleNumber = result[0]

        if not articleNumber:
            articleNumber = _sku
            
        url = 'https://www.elgiganten.se/cxorchestrator/se/api?appMode=b2c&user=anonymous&operationName=getStoresByLocationWithProduct&variables=%7B%22articleNumber%22%3A%22'+articleNumber+'%22%2C%22latitude%22%3A59.128161%2C%22longitude%22%3A17.643501%2C%22thresholdKm%22%3A-1%2C%22myStoreId%22%3Anull%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%226a2d3941da4ceb0d6e7bb1254fa3ad1c0e3668ffb333df9b194b19f7436047af%22%7D%7D'
        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'authorization': '',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'referer': self._url,
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'x-app-checkout': 'false',
            'x-app-mode': 'b2c',
            'x-authorization': '',
            'x-instana-t': 'ce4e825f40bbd000',
            'x-is-anonymous': 'true',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                            ' Chrome/79.0.3945.130 Safari/537.36'
            }
        cookies = self._get_cookies()
        _json = []
        store = []
        _delivry = []
        raw_data = None
        try:
            res = self.requester.get(url, timeout=10, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.text
            _json = json.loads(raw_data)
            _json = _json['data']['storesByLocationWithProduct']
            
            store = [_store['store'] for _store in _json['closeByStores'] if _store['inStock']]
            if _json['primaryStore']['inStock']:
                store.append(_json['primaryStore']['store'])

            # IF deliveryArticleNumber ARE NULL, EXTERNAL REASON ARE NOT FOUND
            _delivery = [_store for _store in _json['closeByStores']]
            _delivery.append(_json['primaryStore'])
            _delivry = [_dlvry['store']['id'] for _dlvry in _delivery if _dlvry['deliveryArticleNumber']]

        except:
            pass
        
        return store, _delivry

    def _get_cookies(self):
        cookie = None
        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.56'}
        response = self.requester.get('https://www.elgiganten.se/', headers=headers, timeout=10)
        cookie = response.cookies.get_dict()
        return cookie

    def download_all_stores(self, url='https://www.elgiganten.se/INTERSHOP/web/WFS/store-elgigantenSE-Site/sv_SE/-/SEK/ViewStoreFinder-Start'):
        raw_data = self.download(url, productstore=True)

        return raw_data

    def extra_download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = None

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect:
                    return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')