from pprint import pprint
from copy import deepcopy
import urllib3
import json
from dotenv import load_dotenv
from request import CrawleraSessionRequests, LuminatiSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
if __name__ == '__main__':
    website = 'www.xxl.no'
    country_code = 'no'
    requester = LuminatiSessionRequests(country_code)  # Init requester
    factory = StrategyFactory()
    dl_strategy = factory.get_download_strategy(website, country_code=country_code)(requester)  # Init download strategy
    ws_strategy = factory.get_website_strategy(website, country_code=country_code)(dl_strategy)  # Init website strategy
    # Get raw data
    url = 'https://www.xxl.no/garmin-acc-fenix-7s-20mm-quickfit-black-silicone-band-klokkereim-svart/p/1209594_1_style'
    raw_data = dl_strategy.download(url)
    # raw_data = dl_strategy.download('https://www.sportfiskeprylar.se/sv/artiklar/garmin-echomap-uhd-62cv-med-givare-gt24-tm.html')
    # Get Proxy IP info
    pprint(dl_strategy.requester.proxy_info)
    # Get extracted data
    res = ws_strategy.execute(raw_data)
    # pprint(res)
    deepcopy(res)
    with open('../extract-transform-physical-stores/et_test.json', 'w', encoding='utf-8') as f:
            json.dump(res, f, ensure_ascii=False, indent=2)