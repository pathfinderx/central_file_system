from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json, re
from urllib.parse import urlencode, quote

class ElgigantenDkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self._url = None

    def download(self, url, timeout=10, headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                              ' Chrome/79.0.3945.130 Safari/537.36'
            }

        if not self._url:
            self._url = url

        if not isinstance(cookies, dict):
            cookies = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                return res.text

            raise DownloadFailureException('Download failed - Status Code {}'.format(res.status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')

    def download_all_store_api(self):
        url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?operationName=getAllStores&variables=%7B%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%2217a1e542677391a9fbae9df196cb565ec643d1a55cd42943feeca916a242660f%22%7D%7D'
        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'authorization': '',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'referer': self._url,
            'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'x-app-checkout': 'false',
            'x-app-mode': 'b2bExVat',
            'x-authorization': '',
            'x-instana-t': '550b7dc447679f53',
            'x-is-anonymous': 'true',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                            ' Chrome/79.0.3945.130 Safari/537.36'
            }
        cookies = self._get_cookies()
        _json = []
        raw_data = None
        try:
            res = self.requester.get(url, timeout=10, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.text
            _json = json.loads(raw_data)
            _json = _json['data']['allStores']
        except:
            pass
        
        return _json

    def download_product_stores_api(self, _sku):
        try:
            articleNumber = None
            pattern = re.compile(r'\d{6}', re.IGNORECASE)
            result = pattern.findall(self._url)
            if result and not articleNumber:
                articleNumber = result[0]
                
            pattern = re.compile(r'\d{5}', re.IGNORECASE)
            result = pattern.findall(self._url)
            if result and not articleNumber:
                articleNumber = result[0]

            if not articleNumber:
                articleNumber = _sku

            filtered_sku = re.search(r'\d+', _sku).group(0)
            articleNumber = filtered_sku if filtered_sku else _sku
            
            payload = {
                'appMode': 'b2c',
                'user': 'anonymous',
                'operationName': 'getStoresByLocationWithProduct',
                'variables': '{"articleNumber":'+ json.dumps(articleNumber) +',"latitude":null,"longitude":null,"myStoreId":"3070"}',
                'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"ee66aa5be34447d95dfc73bea5f8de9ad2a0754c674b95f0a12544b1a36eeea2"}}'
            }

            queries = urlencode(payload, quote_via=quote)
            url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(queries)

            headers = {
                'accept': 'application/json, text/plain, */*',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-US,en;q=0.9',
                'authorization': '',
                'cache-control': 'no-cache',
                'pragma': 'no-cache',
                'referer': self._url,
                'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'x-app-checkout': 'false',
                'x-app-mode': 'b2bExVat',
                'x-authorization': '',
                'x-instana-t': '550b7dc447679f53',
                'x-is-anonymous': 'true',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                                ' Chrome/79.0.3945.130 Safari/537.36'
                }
            cookies = self._get_cookies()
            _json = []
            store = []
            _delivry = []
            raw_data = None
            
            res = self.requester.get(url, timeout=10, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.text
            _json = json.loads(raw_data)
            _json = _json['data']['storesByLocationWithProduct']
            
            store = [_store['store'] for _store in _json['closeByStores'] if _store['inStock']]
            if _json['primaryStore']['inStock']:
                store.append(_json['primaryStore']['store'])

            # IF deliveryArticleNumber ARE NULL, EXTERNAL REASON ARE NOT FOUND
            _delivery = [_store for _store in _json['closeByStores']]
            _delivery.append(_json['primaryStore'])
            _delivry = [_dlvry['store']['id'] for _dlvry in _delivery if _dlvry['deliveryArticleNumber']]

        except:
            pass
        
        return store, _delivry

    def _get_cookies(self):
        cookie = None
        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.56'}
        response = self.requester.get('https://www.elgiganten.dk/', headers=headers, timeout=10)
        cookie = response.cookies.get_dict()
        return cookie