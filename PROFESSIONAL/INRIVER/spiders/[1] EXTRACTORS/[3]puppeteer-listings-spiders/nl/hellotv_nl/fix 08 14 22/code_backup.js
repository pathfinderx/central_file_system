let result = [];
let productElements = document.querySelectorAll('.css-1id9gl1 .css-1fxj02p');
if(productElements){
    for(let index = 0; index < productElements.length; index++){
        let template = JSON.parse(JSON.stringify(baseTemplate));
        let element = productElements[index];

        template.rank = startRank.toString();
        template.page_number = startPage.toString();

        // get price
        try{
            let priceElement = element.querySelector('.e2uxywt2.ezb64sj0');

            if(priceElement){
                template.price.value = priceElement.textContent.trim();
                template.price.source = priceElement.outerHTML;
            }
        }catch(error){
            template.price.value = FailureMessages.PRICE_EXTRACTION;
            template.price.source = FailureMessages.GENERIC_NOT_FOUND;
        }

        // get currency
        template.currency.value = 'EUR';
        
        //no price per unit

        // get promo


        // get url
        try{
            let urlElement = element.querySelector('a.css-1tf00du');

            if (urlElement && urlElement.hasAttribute('href')){
                template.url.value = 'https://www.hellotv.nl' + urlElement.getAttribute('href');
                template.url.source = urlElement.outerHTML;
            }
        }catch(error){
            template.url.value = FailureMessages.URL_EXTRACTION;
            template.url.source = FailureMessages.GENERIC_NOT_FOUND;
        }

        // get brand

        // get title
        try{
            let titleElement = element.querySelector('header h3')

            if(titleElement){
                template.title.value = titleElement.innerText.trim();
                template.title.source = titleElement.outerHTML;
            }
        }catch(error){
            template.title.value = FailureMessages.TITLE_EXTRACTION;
            template.title.source = FailureMessages.GENERIC_NOT_FOUND;
        }

        // get description

        // get rating


        // no availability
        template.availability.value = true


        // get image urls
        try{
            let imgElement = element.querySelector('img');
            if(imgElement && imgElement.hasAttribute('src')){
                let url = imgElement.getAttribute('src');
                if (url){
                    template.image_urls.value.push(url);
                }
                
                template.image_urls.source = element.outerHTML;
            }
        }catch(error){
            template.image_urls.value = FailureMessages.IMAGE_URLS_EXTRACTION;
            template.image_urls.source = FailureMessages.GENERIC_NOT_FOUND;
        }


        result.push(template);
        startRank++;
    }
}