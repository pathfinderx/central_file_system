import { DocumentStatus } from "./services/constants.js";
import { BlobStorage } from './services/blob.js';
import template from './strategies/static/result_base_template.js';
import { StrategyFactory } from './strategies/factory.js';
import { ProxyConnection } from "./models.js";
import { tryCatchPromiseWrapper } from "./utils.js";
import dotenv from 'dotenv'
import fs from 'fs';
dotenv.config()
import sample from 'amqplib/callback_api.js';
function formatReadableResult(result){
    for(let index = 0; index < result.length; index++){
        let item = result[index];
        console.log('-----------------');
        console.log('Rank: ' + item.rank);
        console.log('Page: ' + item.page_number);
        console.log('Title: ' + item.title.value);
        console.log('Price: ' + item.price.value);
        console.log('Currency: ' + item.currency.value);
        console.log('Promo: ' + item.promo.price.value + ', ' + item.promo.description.value + ', ' + item.promo.currency.value);
        console.log('Url: ' + item.url.value);
        console.log('Brand: ' + item.brand.value);
        console.log('Description: ' + item.description.value);
        console.log('Rating: ' + item.rating.score.value);
        console.log('Reviews: ' + item.rating.reviews.value);
        console.log('Availability: ' + item.availability.value);
        console.log('Brand: ' + item.brand.value);
        console.log('Image urls: ' + String(item.image_urls.value))
        console.log('-----------------');
    }
}
async function main(){
    const countryCode = 'ro';
    const website = 'pcgarage.ro'
    const url = 'https://www.pcgarage.ro/gamepad/'
    const browserEndpoint = process.env.BROWSER_ENDPOINT
    const timeout = process.env.BROWSER_TIMEOUT
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36'
    const isHeadless = false;
    const useLocalBrowser = true;
    console.log('[*] Init proxy')
    var proxyConnection = new ProxyConnection(process.env.LUMINATI_SERVER, process.env.LUMINATI_USER + countryCode.toLowerCase(), process.env.LUMINATI_PWD)
    const factory = new StrategyFactory();
    const context = await factory.getWebsiteStrategy(countryCode.toLowerCase(), website);
    if (!context) {
        console.error('[x] Strategy identification failed')
    }
    else {
        const strategy = new context(browserEndpoint, isHeadless, proxyConnection);
        let [strategyError, strategyResult] = await tryCatchPromiseWrapper(strategy.execute(url, userAgent, timeout, useLocalBrowser, 1, 1, 100));
        if(strategyError){
            console.error(`[x] Strategy execution failed: ${strategyError}`);
        }else{
            formatReadableResult(strategyResult.result);
            fs.writeFileSync('../extract-transform-listings/et_test.json', JSON.stringify(strategyResult.result), function(error){
            });
        }
    }
}
main()