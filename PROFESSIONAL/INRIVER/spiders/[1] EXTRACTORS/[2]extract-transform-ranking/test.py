import json
from transformers import Transformer
from post_transformers import PostTransformer
from builder import ETPayloadBuilder
from validators import RunningmanETDataValidator
from pprint import pprint
def transform(retailer, country_code, data):
    transformer = Transformer(retailer, country_code=country_code)
    post_transformer = PostTransformer(retailer, country_code=country_code)
    payload_builder = ETPayloadBuilder(transformer, post_transformer)
    # raises an exception
    transformed_data = payload_builder.build(data)
    return transformed_data
if __name__ == "__main__":
    with open('sample_batch.json', encoding="utf-8") as f:
        data = json.load(f)
        retailer = 'www.zoo.se'
        country_code = 'se'
        warnings = []
        for outer in data:
            for inner in outer:
                transformed_data = transform(retailer, country_code, inner)
                print(transformed_data['title'], ' | ', transformed_data['url'], ' | ', transformed_data['brand'])
                rm_et_data_validator = RunningmanETDataValidator(transformed_data)
                warning_fields_mapping = rm_et_data_validator.get_warning_fields_mapping()
                if warning_fields_mapping:
                    warnings.append({
                        'rank': transformed_data['rank'],
                        'warnings': warning_fields_mapping
                    })
        pprint(warnings)