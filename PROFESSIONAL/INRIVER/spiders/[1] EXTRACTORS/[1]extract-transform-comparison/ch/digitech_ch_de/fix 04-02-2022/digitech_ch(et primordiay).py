import re
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException
from constants import ETDefaults

class DigitecChDeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'http://schema.org/InStock',
            'http://schema.org/instock',
            'in den warenkorb'
        ])
        #self.outofstock_values.extend([])

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value
            
        else:
            # NOTE: regex match for this format "Aktuell nicht lieferbar und kein Liefertermin vorhanden"
            # just match "Aktuell nicht lieferbar" which means out of stock
            pattern = re.compile(r'(Aktuell nicht lieferbar)', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.OUT_OF_STOCK.value
        
        return Availability.NOT_FOUND.value

    def transform_rating(self, value, max_value):
        try:
            value = round(float(value), 1)
            return value
        except Exception:
            raise TransformationFailureException('rating transformation failed.')
    
    def transform_price(self, value):
        try:
            if value == '':
                return ETDefaults.NOT_FOUND.value
            if 'statt' not in value:
                value = re.sub('[^0-9.]', '', value)
                return super().transform_price(value)
            elif value == '':
                return ETDefaults.NOT_FOUND.value
            else:
                value = value.split('statt')[0]
                return value
        except Exception:
            raise TransformationFailureException('Price transformation failed.') 

    def transform_title(self, value):
        try:
            if value == '':
                return ETDefaults.NOT_FOUND.value
            if ' - digitec' in value:
                value = value.replace(' - digitec','')
                return super().transform_title(value)
            else:
                return super().transform_title(value)
        except Exception:
            raise TransformationFailureException('Title transformation failed.') 

 