
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from constants import ETDefaults
import math


class ShopeePhTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

    def transform_price(self, value):
        try:
            trailing_zeros = len(value) - len(value.rstrip('0'))
            value = int(value)/(10**5)
            return super().transform_price(str(value))
        except Exception:
            raise TransformationFailureException('Price transformation failed.')

    def transform_availability(self, value):
        try:
            return Availability.IN_STOCK.value if int(value) > 0 else  Availability.OUT_OF_STOCK.value
        except Exception:
            raise TransformationFailureException('Availability transformation failed')

    def transform_rating(self, value, max_value):
        try:

            max_value = max_value.replace(',', '')
            value = value.replace(',', '')
            factor = 10
            value = (float(value) / float(max_value)) * Defaults.MAX_RATING.value
            return math.ceil(value * factor) / factor

        except Exception:
            raise TransformationFailureException('Rating transformation failed')        
    
    def transform_brand(self, value):
        try:
            if value.lower() in ['none', 'no brand']:
                return ETDefaults.NOT_FOUND.value
            else:
                return value.strip()
        except Exception:
            raise TransformationFailureException('Brand transformation failed')
