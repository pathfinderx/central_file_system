import re, html
from transformers.base import TransformationStrategy
from transformers.constants import Availability


class HjertmansSeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'finnsilager',
            'lagervara',
            'finnsilager((lågtlagersaldo))', # Instock (low stock)
            'väljbutik...',
            'hittaibutik!', # Find in Store
            'ilager'
        ])
        self.outofstock_values.extend([
            'beställningsvara',
            'ejilager',
            'tillfälligtslut,viväntarpåleverans'
        ])

    def transform_price(self, value):
        value = value.replace('\xa0', '')
        value = re.sub(r'[^0-9\.]', '', value)
        return round(float(value), 2)

    def transform_availability(self, value):
        availability = ''.join(value.split()).lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        pattern = re.compile('\d*stilager', re.IGNORECASE)
        results = pattern.findall(availability)

        if results:
            return Availability.IN_STOCK.value

        return Availability.NOT_FOUND.value

    def transform_description(self, value):
        value = re.sub('[\n\xa0]', '', value)
        value = re.sub(r'\s+', ' ', value)
        value = html.unescape(value)
        return value