import re
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException


class FnacEsTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'http://schema.org/instock',
            'f-otherOffers-stock--available',
            'f-otheroffers-stock--available',
            'en stock en ligne',
            'téléchargement immédiat',
            'en stock en fnac.es',
            'en stock fnac.es',
            'Bajo pedido',
            'bajo pedido',
            'descarga inmediata',
            '103',
            'en stock ver ofertas »',
            'en stock'
        ])
        self.outofstock_values.extend([
            'http://schema.org/outofstock',
            'http://schema.org/limitedavailability',
            'preorder',
            'pre order',
            'artículo no disponible en web',
            # from tc_vars json - product_stock key
            'no',
            '115',
            '0',
            'stock épuisé',
            'agotado' # 'Agotado' is Out of stock for these urls 
            #https://www.fnac.es/Microsoft-Surface-Book-3-i7-15-512GB-Plata-Ordenador-portatil-PC-Portatil/a7387624#omnsearchpos=1
            #https://www.fnac.es/Microsoft-Surface-Book-3-i7-13-5-256GB-Plata-Ordenador-portatil-PC-Portatil/a7387621#omnsearchpos=3
            #https://www.fnac.es/Microsoft-Surface-Go-2-10-5-128GB-LTE-Plata-Tablet-Tablet/a7387576#omnsearchpos=4
        ])
    
    def transform_price(self, value):
        try:
            # value = re.sub(r"[^0-9.]", "", value.replace('.', '').replace(',', '.'))
            try:
                value = re.sub(r"[^0-9.,]", "", value)
                if len(value) >= 3:
                    if value[-3] == ',' or value[-4] == '.':
                        value = re.sub(r"[^0-9.,]", "", value.replace('.', '').replace(',', '.'))
                else:
                    value = re.sub(r"[^0-9.]", "", value)
                    preserved_value = value.replace(',', '.').replace('€', '') # preserve the variable 'value' for ambiguity prevention during testing phase
                    rounded_value = self.round_half_up(float(preserved_value), decimal=2) # first arg static, 2nd arg is keyword arg: adjust to target decimal place
                    value = float("{0:.2f}".format(rounded_value)) # replace formatting e.g {0:.2f} format to hundredths place
            except:
                value = re.sub(r"[^0-9.]", "", value)

            # return super().transform_price(value)
            value = round(float(value),2)
            return value
        except Exception:
            raise TransformationFailureException('Price transformation failed.')

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value
        else:
            # NOTE: match "Expédié habituellement sous 2 à 3 semaines" format
            # just match "plus que 1 en stock" wc means "Usually ships within 2 to 3 weeks"
            # and consider as In stock
            pattern = re.compile(r'(Expédié habituellement sous)', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'(plus que [\d]+ en stock)', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.IN_STOCK.value
            
            pattern = re.compile(r'sous réserve de disponibilité', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value
            
            pattern = re.compile(r'(Bajo pedido, expedición [\d]+-[\d]+ días)', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.OUT_OF_STOCK.value
            
            pattern = re.compile(r'(Sólo queda.* [\d]+ en stock)', re.IGNORECASE) # Only 1 left in stock
            results = pattern.findall(availability)
            
            if results:
                return Availability.IN_STOCK.value
            
        return Availability.NOT_FOUND.value