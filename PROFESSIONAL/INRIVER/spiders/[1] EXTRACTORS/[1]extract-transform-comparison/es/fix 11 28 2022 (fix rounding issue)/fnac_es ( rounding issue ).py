import re
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException
import math, copy

class FnacEsTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'http://schema.org/instock',
            'f-otherOffers-stock--available',
            'f-otheroffers-stock--available',
            'en stock en ligne',
            'téléchargement immédiat',
            'en stock en fnac.es',
            'en stock fnac.es',
            'Bajo pedido',
            'bajo pedido',
            'descarga inmediata',
            '103',
            'en stock ver ofertas »',
            'en stock'
        ])
        self.outofstock_values.extend([
            'http://schema.org/outofstock',
            'http://schema.org/limitedavailability',
            'preorder',
            'pre order',
            'artículo no disponible en web',
            # from tc_vars json - product_stock key
            'no',
            '115',
            '0',
            'stock épuisé',
            'agotado' # 'Agotado' is Out of stock for these urls 
            #https://www.fnac.es/Microsoft-Surface-Book-3-i7-15-512GB-Plata-Ordenador-portatil-PC-Portatil/a7387624#omnsearchpos=1
            #https://www.fnac.es/Microsoft-Surface-Book-3-i7-13-5-256GB-Plata-Ordenador-portatil-PC-Portatil/a7387621#omnsearchpos=3
            #https://www.fnac.es/Microsoft-Surface-Go-2-10-5-128GB-LTE-Plata-Tablet-Tablet/a7387576#omnsearchpos=4
        ])
    
    def transform_price(self, value):
        try:
            # value = re.sub(r"[^0-9.]", "", value.replace('.', '').replace(',', '.'))
            try:
                origin_value = copy.deepcopy(value) # value preservation

                value = re.sub(r"[^0-9.,]", "", value)
                if len(value) >= 3:
                    if value[-3] == ',' or value[-4] == '.':
                        value = re.sub(r"[^0-9.,]", "", value.replace('.', '').replace(',', '.'))
                
                if (value.isdigit() and
                    origin_value and 
                    not origin_value.isdigit()): # recheck condition if value is not float and origin value is float > means incorrect https://prnt.sc/q-6pLm-ELCTm

                    replaced_value = origin_value.replace(',', '.').replace('€', '')
                    rounded_value = self.round_half_up(float(replaced_value), decimal=2) # add condition in the future if pdp price decimal value changes from Thousandths to Hundredths
                    value = float("{0:.2f}".format(rounded_value))  # format to Hundredths place if given pdp decimal format of Thousandths

            except:
                value = re.sub(r"[^0-9.]", "", value)

            # return super().transform_price(value)
            value = round(float(value),2)
            return value
        except Exception:
            raise TransformationFailureException('Price transformation failed.')

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value
        else:
            # NOTE: match "Expédié habituellement sous 2 à 3 semaines" format
            # just match "plus que 1 en stock" wc means "Usually ships within 2 to 3 weeks"
            # and consider as In stock
            pattern = re.compile(r'(Expédié habituellement sous)', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'(plus que [\d]+ en stock)', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.IN_STOCK.value
            
            pattern = re.compile(r'sous réserve de disponibilité', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value
            
            pattern = re.compile(r'(Bajo pedido, expedición [\d]+-[\d]+ días)', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.OUT_OF_STOCK.value
            
            pattern = re.compile(r'(Sólo queda.* [\d]+ en stock)', re.IGNORECASE) # Only 1 left in stock
            results = pattern.findall(availability)
            
            if results:
                return Availability.IN_STOCK.value
            
        return Availability.NOT_FOUND.value

    def round_half_up(self, num, **kwargs): # adjust the decimal argument for the targetted decimal place 
        tens,half = 10.0, 0.5
        try:
            if kwargs['decimal'] in [0]:
                return math.floor(num + half)

            arg1 = math.floor(num * math.pow(tens, kwargs['decimal']) + half)  # mathematical fomula to resolve half_down issue
            arg2 = math.pow(tens, kwargs['decimal']) # target decimal place

            return arg1 / arg2 if (arg1 and arg2) else Defaults.GENERIC_NOT_FOUND.value
        except Exception as e:
            print('Error in: half_up() -> with message: ', e)