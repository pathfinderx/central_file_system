import re
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from constants import ETDefaults

class ShoptimeComBrTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()
        
        self.instock_values.extend([
            'comprar', 
            "calcular frete e prazo"
        ])
        self.outofstock_values.extend([
            "http://schema.org/outofstock",
            "encontre a loja mais proxima", 
            "loja parceira perto de você",
            'me avise quando chegar',
            'produto sem estoque'
        ])
        self.currentAvailability = []

    def transform_price(self, value):
        value = re.sub('[^0-9.]', '', value.replace('.','').replace(',','.')) if ',' in value else re.sub('[^0-9.]', '', value)
        #value = re.sub('[^0-9.]', '', value) #temporary fix
        return super().transform_price(value)

    def transform_rating(self, value, max_value):
        value = str(round(float(value), 1))
        return super().transform_rating(value, max_value)

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            self.currentAvailability = Availability.OUT_OF_STOCK.value
            return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value

    def transform_reviews(self, value):
        value = re.sub(r'[^0-9]', '', value)
        return super().transform_reviews(value)
