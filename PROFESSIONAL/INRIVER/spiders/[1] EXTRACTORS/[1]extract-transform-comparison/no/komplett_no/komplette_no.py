import html,re
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from transformers.exceptions import TransformationFailureException
from constants import ETDefaults

class KomplettNoTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super(TransformationStrategy, self).__init__()

        self.instock_values = [
            'http://schema.org/instock',
        ]
        self.outofstock_values = [
            'http://schema.org/outofstock',
            'http://schema.org/instoreonly',
            'http://schema.org/discontinued'
        ]

    def transform_availability(self, value):
        availability = value.lower() #''.join(value.split()).lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value
        else:
            # NOTE: match "Ikke på lager. 34 stk. bekreftet sendingsklare 23.apr..2021" format
            # just match "Ikke på lager. 34 stk. bekreftet sendingsklare 23.apr..2021"
            # and consider as out of stock
            pattern = re.compile(r'Ikke på lager. [0-9]+ stk. bekreftet sendingsklare [\s\S]+', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'Ikke på lager', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.OUT_OF_STOCK.value

            # NOTE: match "Ikke på lager. 34 stk. bekreftet sendingsklare 23.apr..2021" format
            # just match "Ikke på lager. 34 stk. bekreftet sendingsklare 23.apr..2021"
            # and consider as out of stock
            pattern = re.compile(r'Bestillingsvare. Bestilles ved ordre. Leveringstid [\s\S]+ uker.', re.IGNORECASE)
            results = pattern.findall(availability)
            
            if results:
                return Availability.IN_STOCK.value
                

        return Availability.NOT_FOUND.value
    
    def transform_description(self, value):
        if not value.strip():
            return ETDefaults.NOT_FOUND.value
        else:
            value = html.unescape(value)
            return super().transform_description(value)

    def transform_price(self, value):
        if value and float(value) == 0:
            return ETDefaults.NOT_FOUND.value
        return value