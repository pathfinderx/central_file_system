import json
import logging
import re
import requests
from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class CyberportDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.cyberport.de'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")


        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency -no proper tag for currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

         # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand  -no proper idicator for brand
        # result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result
   
    # GET PRICE
    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one("div.price")
            if tag:
                source = str(tag)
                value = re.sub("[^0-9,]", "", tag.get_text())

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    # GET CURRENCY
    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('script',{'id':'dataLayerID'})
            if tag:
                source = str(tag)
                value = tag.get_text().split("productCurrency: '")[1].split("',")[0]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    #GET AVAILABILITY
    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = None
            tag = soup.find('div',{'class':'tooltipParent tooltipAvaibilityParent'})
            if tag:
                source = str(tag)
                value =  tag.get_text().strip()

                if value == '':
                    tag = soup.select_one('div.addToCart.exclusiveBox span.headline')
                    if tag:
                        source = str(tag)
                        value =  tag.get_text().strip()

            if tag == None:
                tag = soup.find('div',{'class':'head heading-level5 available'})
                if tag:
                    source = str(tag)
                    value =  tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    #GET RATINGS
    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('section',{'class':'keybox'})
            if tag and tag.has_attr('data-productinfofordatalayer'):
                data = tag.get('data-productinfofordatalayer')
                _json = json.loads(data)

                # Reviews
                if _json and 'productRatingCount' in _json.keys() and _json['productRatingCount']:
                    reviews_source = str(tag)
                    reviews_value = str(_json['productRatingCount'])
                # Ratings
                if _json and 'productAvgRating' in _json.keys() and _json['productAvgRating']:
                    score_source = str(tag)
                    score_value = str(_json['productAvgRating'])

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    # GET TITLE
    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('meta',{'property':'og:title'})
            if tag and tag.has_attr('content'):
                source = str(tag)
                value = tag.get('content')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    #GET BRAND
    # def get_brand(self, soup):
    #     source = Defaults.GENERIC_NOT_FOUND.value
    #     value = Defaults.GENERIC_NOT_FOUND.value

    #     try:
    #         script_tag = soup.find('script',text=re.compile(".bbmx-product-variations',"))            
    #         if script_tag:
    #             tag = script_tag.get_text().split(".bbmx-product-variations', ")[1].split(", 'es-MX',")[0]
    #             if tag:
    #                 _json = json.loads(tag)
    #                 if _json and 'itemData' in _json.keys():
    #                     source = str(script_tag)
    #                     value = _json['itemData']['brand']
            
    #     except Exception as e:
    #         self.logger.exception(e)
    #         value = FailureMessages.BRAND_EXTRACTION.value

    #     return source, value        

    #EXTRACT DESCRIPTION
    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('div',{'class':'descriptionText'})
            if tag:
                source = str(tag)
                value = ' '.join(tag.stripped_strings)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    #GET SPECS
    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find('table',{'class':'tblProductDataSheet'})
            if tag:
                source = str(tag)
                value = self._get_initial_specs(tag.select('tr'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source, value

    #GET IMAGE URLS
    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.find('div',{'class':'thumbnails-container'})
            if tag:
                tags = tag.select('a')
                if tags:
                    source = str(tag)
                    value = ['https:'+img.get('href') for img in tags if img.has_attr('href')]
            if not tag:
                tag = soup.select('.productImage img')
                if tag:
                    source = str(tag)
                    for img in tag:
                        https = 'https:'
                        value.append(https + img.get('data-src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #get Delivery
    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('span',{'class':'tooltipAppend available'})
            if tag:
                value = tag.get_text().strip()
                source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source, value

    @staticmethod
    def _get_initial_specs(tags):
        result = {}
       
        for tag in tags:
            try:
                key = tag.find('td',{'class':'detailTitle'})
                if key:
                    key = key.get_text().strip()
                value = tag.find('div',{'class':'article'})
                if value:
                    value = ' '.join(value.stripped_strings)
                result[f'{key}'] = value
            except:
                continue

        return result
