import re
from transformers.base import TransformationStrategy
from transformers.exceptions import TransformationFailureException
from transformers.constants import Availability, Defaults
from constants import ETDefaults

class AlternateDeTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'Auf Lager',
            'auf lager',
            'Ware neu eingetroffen, in Kürze versandfertig',
            'ware neu eingetroffen, in kürze versandfertig'
        ])
        
        self.outofstock_values.extend([
            'liefertermin in klärung',
            'bereits verkauft',
            'derzeit sind alle artikel reserviert'
        ])

    def transform_price(self, value):
        try:
            # value = value.split(' ')[1].replace(',', '.')
            value = re.sub(r"[^0-9.]", "", value.replace('.','').replace(',','.'))
            return round(float(value), 2)

        except Exception:
            raise TransformationFailureException('Price transformation failed')

    def transform_rating(self, value, max_value):
        try:
            value = value.split(' ')[0]
            # value = (float(value) / float(max_value)) * Defaults.MAX_RATING.value
            if value[-1] == '5':
                value = round(float(value) +.01,1)
            else:
                value = float("{0:.1f}".format(float(value))) # 1 decimal place (https://prnt.sc/GNjgjQGJ5YY8)
            return value
            
        except Exception:
            raise TransformationFailureException('Rating transformation failed')

    def transform_reviews(self, value):
        try:
            if value:
                pattern = re.compile(r'([\d]+)')
                results = pattern.findall(value)
                return int(results[0])
            else:
                return ETDefaults.NOT_FOUND.value
                
        except Exception:
            raise TransformationFailureException('Reviews transformation failed')
    
    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value
        
        elif 'vorbestellen' in availability:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        else:
            pattern = re.compile(r'Lieferbar in ([\d]+) Tagen', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                days = int(results[0])
                if days <= 30:
                    return Availability.IN_STOCK.value
                else:
                    return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'Auf Lager\([\d]+ Stück verfügbar\)', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.IN_STOCK.value

            pattern = re.compile(r'Artikel kann derzeit nicht gekauft werden', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'Lieferbar in ([\d]+) Tag', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                days = int(results[0])
                if days <= 30:
                    return Availability.IN_STOCK.value
                else:
                    return Availability.OUT_OF_STOCK.value

            pattern = re.compile(r'Ware neu eingetroffen, in Kürze versandfertig\([\d]+ Stück verfügbar\)', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value
