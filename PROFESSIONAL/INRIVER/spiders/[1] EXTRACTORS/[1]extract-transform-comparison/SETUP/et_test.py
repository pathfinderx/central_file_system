import json
from pprint import pprint

from transformers import Transformer
from post_transformers import PostTransformer
from builder import ETPayloadBuilder
from validators import RunningmanETDataValidator

def transform(retailer, country_code, data, company_code='E00'):

    transformer = Transformer(retailer, country_code=country_code, company_code=company_code)
    post_transformer = PostTransformer(retailer, country_code=country_code, company_code=company_code)

    payload_builder = ETPayloadBuilder(transformer, post_transformer)
    
    # raises an exception
    transformed_data = payload_builder.build(data)

    return transformed_data

if __name__ == "__main__":

    retailer = 'www.utomhusliv.se'
    country_code = 'se'
    company_code = 'a00'
    
    with open('sample.json', 'r') as f:
        data = json.load(f)
        # print(data['buybox']['availability'])
        # print(data['price']['value'])
        # print(data['availability']['value'])
        # print(data['rating']['score']['value'])
        # print(data['rating']['reviews']['value'])
        transformed_data = transform(retailer, country_code, data, company_code=company_code)

        # for item in data['third_parties']:
        #     print(item['rating']['score']['value'])
        #     print(item['rating']['reviews']['value'])

        rm_et_data_validator = RunningmanETDataValidator(transformed_data)
        warning_fields_mapping = rm_et_data_validator.get_warning_fields_mapping()

        print()
        pprint(transformed_data)
        print()
        print(warning_fields_mapping)
