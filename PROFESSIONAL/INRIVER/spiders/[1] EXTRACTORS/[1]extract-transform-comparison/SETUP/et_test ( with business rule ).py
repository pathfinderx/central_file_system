import json
import os
from pprint import pprint
from transformers import Transformer
from post_transformers import PostTransformer
from builder import ETPayloadBuilder
from validators import RunningmanETDataValidator
from clients.rm import RunningManPublisher
from dotenv import load_dotenv
load_dotenv()

def transform(website, country_code, company, data):
    transformer = Transformer(website, country_code=country_code, company_code=company)
    post_transformer = PostTransformer(website, country_code=country_code, company_code=company)
    payload_builder = ETPayloadBuilder(transformer, post_transformer)
    business_rule =[]
    transformed_data = payload_builder.build(data, business_rule=business_rule)
    return transformed_data

# def test_builder(website, country_code, company, data):
#     transformer = Transformer(website, country_code=country_code)
#     post_transformer = PostTransformer(website, country_code=country_code, company_code=company)
#     payload_builder = ETPayloadBuilder(transformer, post_transformer)
#     with open('sample.json', encoding="utf-8") as sample_json:
#         scrape_data = json.load(sample_json)
#     business_rule = []
#     pprint(payload_builder.build(scrape_data, business_rule=business_rule))


if __name__ == '__main__':
    with open('et_test.json', encoding="utf-8") as f:
        data = json.load(f)       
        company_code = ''
        website = 'www.heureka.cz'
        country = 'cz'
        # print(website)
        transformed_data = transform(website, country, company_code, data)
        
        pprint(transformed_data)
        # rm_et_data_validator = RunningmanETDataValidator(transformed_data)
        # warning_fields_mapping = rm_et_data_validator.get_warning_fields_mapping()
        # print(warning_fields_mapping)