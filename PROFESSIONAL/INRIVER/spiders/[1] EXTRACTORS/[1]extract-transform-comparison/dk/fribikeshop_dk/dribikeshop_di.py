import re
from constants import ETDefaults
from transformers.base import TransformationStrategy
from transformers.constants import Availability   
from validators.constants import RawDataDefaults, ScrapeDataFailureMessages

class FribikeshopDkTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super(TransformationStrategy, self).__init__()

        self.instock_values = [
            'pålager',
            'fåstk.',
            'InStock',
            'instock'
        ]
        self.outofstock_values = [
            'ikkepålager',
            'mulighedforclick&collect',
            'OutOfStock',
            'outofstock'
        ]
        self.currentAvailability = []
    
    def transform_price(self, value):
        value = re.sub('[^0-9]', '', value)
        return super().transform_price(value)

    def transform_availability(self, value):
        availability = ''.join(value.split()).lower()

        if availability in self.instock_values:
            self.currentAvailability = Availability.IN_STOCK.value # used to access for tranform_delivery
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            self.currentAvailability = Availability.OUT_OF_STOCK.value # used to access for tranform_delivery
            return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value

    def transform_delivery(self, value):
        
        palager = 'INSTOCK'
        click_and_collect = 'STOREONLY'
        ikke_pa_lager = ['SOLDOUT', 'INCOMING', 'UNKNOWN', 'BACKORDER']
        fa_stk = 'LOWSTOCK'

        if value == RawDataDefaults.NOT_FOUND.value:
            new_value = ETDefaults.NOT_FOUND.value
        elif value == ScrapeDataFailureMessages.DELIVERY_EXTRACTION.value:
            new_value = ETDefaults.CRAWL_FAILURE.value
        else:
            try:
                if self.currentAvailability == Availability.OUT_OF_STOCK.value:
                    new_value = ETDefaults.NOT_FOUND.value
                elif value in palager:
                    new_value = 'På lager'
                elif value in click_and_collect:
                    new_value = 'Click&Collect'
                elif value in ikke_pa_lager:
                    new_value = 'Ikke på lager'
                elif value in fa_stk:
                    new_value = 'Få stk'
            except Exception:
                new_value = ETDefaults.TRANSFORMATION_FAILURE.value
        return new_value
