import re, unicodedata
import html
from transformers.base import TransformationStrategy
from transformers.constants import Availability
from transformers.exceptions import TransformationFailureException
from constants import ETDefaults
import copy

class MagasinDkTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()
        self.instock_values.extend([
            'tilføj til kurv'
        ])

        self.outofstock_values.extend([
            'ikke tilgængelig',
            'skriv mig op'
        ])

    def transform_shipping(self, value):
        value = self.replace_text(value)
        return value
    
    def transform_description(self, value):
        preserved_value = copy.deepcopy(value)

        replaced_value = self.replace_text(preserved_value)
        stripped_value = re.sub(r'<.*?>', ' ', replaced_value)

        removed_succeding_spaces = re.sub('[\s]{2}', " ", stripped_value)
        value = html.unescape(removed_succeding_spaces).strip()

        return value

    def transform_title(self, value):
        value = self.replace_text(value)
        return value    
    
    def transform_brand(self, value):
        value = self.replace_text(value)
        return value
      
    def replace_text(self, text):
        text = html.unescape(text)
        value = text.replace('Ã¥', 'å').replace('Ã©', 'é').replace('Ã¸', 'ø').replace('Ã­', 'í').replace('Â', '').replace('Ã¦', 'æ').replace('Ã˜', 'Ø').replace('Ã´', 'ô').replace('Ã¸', 'ø')
        return value

    def transform_price(self, value):
        try:
            if value == "None":
                return ETDefaults.NOT_FOUND.value
            return value

        except Exception:
            raise TransformationFailureException('Price transformation failed')


    def transform_availability(self, value):
        try:
            availability = value.lower()

            if availability in self.instock_values:
                return Availability.IN_STOCK.value

            elif availability in self.outofstock_values:
                return Availability.OUT_OF_STOCK.value

            return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')