import re
from constants import ETDefaults
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException


class KomplettDkTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

    def transform_specifications(self, value):
        specs = dict()
        for key, val in value.items():
            new_key = key.encode('ascii', 'ignore').decode('utf8')
            new_val = val.encode('ascii', 'ignore').decode('utf8')
            specs[new_key] = new_val
        return specs

    def transform_price(self, value):
        if value and float(value) == 0:
            return ETDefaults.NOT_FOUND.value
        return value