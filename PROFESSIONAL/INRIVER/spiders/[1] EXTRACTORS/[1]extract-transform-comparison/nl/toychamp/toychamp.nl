import re
import unicodedata
from transformers.base import TransformationStrategy
from transformers.constants import Availability

class ToychampNlTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()     

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value
        else:
            pattern = re.compile(r'binnen [0-9]+ werkdagen in huis', re.IGNORECASE)
            results = pattern.findall(availability)

            if results:
                return Availability.IN_STOCK.value

        return Availability.NOT_FOUND.value