import re, html
import unicodedata
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from transformers.exceptions import TransformationFailureException

class BccNlTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'Op voorraad',
            'op voorraad',
            'online op voorraad',
            'in winkelwagen',
        ])

        self.outofstock_values.extend([
            'dit product is tijdelijk niet leverbaar'
        ])
    
    def transform_title(self, value):
        value = html.unescape(value)
        return super().transform_title(value)
    
    def transform_description(self, value):
        value = html.unescape(value)
        return super().transform_description(value)

    def transform_availability(self, value):
        availability = value.lower()

        if availability in self.instock_values:
            return Availability.IN_STOCK.value

        elif availability in self.outofstock_values:
            return Availability.OUT_OF_STOCK.value

        return Availability.NOT_FOUND.value
    