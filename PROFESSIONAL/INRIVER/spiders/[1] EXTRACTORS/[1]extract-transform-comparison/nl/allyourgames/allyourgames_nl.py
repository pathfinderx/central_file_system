import re
from transformers.base import TransformationStrategy
from transformers.exceptions import TransformationFailureException
from transformers.constants import Availability

class AllyourgamesNlTransformationStrategy(TransformationStrategy):
    def __init__(self):
        super(TransformationStrategy, self).__init__()
        
    def transform_price(self, value):
        value = re.sub('[^0-9.]', '', value)
        return super().transform_price(str(value))