import re

from transformers.exceptions import TransformationFailureException
from transformers.base import TransformationStrategy
from transformers.constants import Availability, Defaults
from constants import ETDefaults
from validators.constants import RawDataDefaults, ScrapeDataFailureMessages


class PlattetvNlTransformationStrategy(TransformationStrategy):

    def __init__(self):
        super().__init__()

        self.instock_values.extend([
            'in winkelwagen',
            'Vraag naar levertijd',
            'reserveer vrijblijvend',
            'Reserveer vrijblijvend',
            'IN_STOCK'
        ])

        self.outofstock_values.extend([
            'OUT_STOCK'
        ])

    def transform_price(self, value):
        try:
            value = re.sub(r"[^0-9.]", "", value.replace('.', '').replace(",", "."))
            # value = value[:-2]
            return round(float(value), 2)

        except Exception:
            raise TransformationFailureException('Price transformation failed')

    def transform_availability(self, value):
        try:
            value = value.strip()

            if value in self.instock_values:
                return Availability.IN_STOCK.value

            elif value in self.outofstock_values:
                return Availability.OUT_OF_STOCK.value

            else:
                # NOTE: regex match for this format "Voor 21:00 besteld, morgen in huis"
                # just match "besteld, morgen in huis" which means "ordered, delivered tomorrow"
                # and consider as in stock
                pattern = re.compile(r'(besteld, morgen in huis)', re.IGNORECASE)
                results = pattern.findall(value)

                if results:
                    return Availability.IN_STOCK.value

            return Availability.NOT_FOUND.value

        except Exception:
            raise TransformationFailureException('Availability transformation failed')

    def transform_rating(self, value, max_value):
        try:
            value = float(value) / 2
            return round(value, 1)
        except Exception as e:
            raise TransformationFailureException('ratings transformation failed.')

    def transform_specifications(self, value):
        specs = dict()
        for key, val in value.items():
            new_key = key
            if key in ['SoortTV','TypeHDR','SmartTV','Digitaaloptischeaansluiting','Digitaalcoaxaleaansluiting','CIgeschikt','Afmetingeninclvoet','Gewichtinclvoet','Afmetingenexclvoet','gewichtexclvoet','MeegeleverdeAccessoires']:
                new_key = key.replace('SoortTV','Soort TV').replace('TypeHDR','Type HDR').replace('SmartTV','Smart TV').replace('Digitaaloptischeaansluiting','Digitaal optische aansluiting').replace('Digitaalcoaxaleaansluiting','Digitaal coaxale aansluiting').replace('CIgeschikt','CI+ geschikt').replace('Afmetingeninclvoet','Afmetingen incl. voet').replace('Gewichtinclvoet','Gewicht incl. voet').replace('Afmetingenexclvoet','Afmetingen excl. voet').replace('gewichtexclvoet','gewicht excl. voet').replace('MeegeleverdeAccessoires','Meegeleverde Accessoires')
            new_val = val
            specs[new_key] = new_val

        return specs