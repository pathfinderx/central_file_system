import re, json
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class ParisClDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Accept': 'image/avif,image/webp,image/apng,image/*,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.9',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=None, cookies=None)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res