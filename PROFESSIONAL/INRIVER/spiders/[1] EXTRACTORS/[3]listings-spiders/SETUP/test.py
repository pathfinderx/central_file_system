from pprint import pprint
import json
import urllib3
from dotenv import load_dotenv
from request import LuminatiSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
 
if __name__ == '__main__':
    try:
        # website = 'www.orange.ro'
        # country_code = 'ro'
        # url = 'https://www.orange.ro/magazin-online/telefoane?order=ASC&sort=TopSales&from=0&size=36&filter=Cuabonament1,Instoc,5G'

        website = 'www.dreamland.be/e/nl/dl'
        country_code = 'be'
        url = 'https://www.dreamland.be/e/nl/dl/nintendo-switch?#facet:&productBeginIndex:0&facetLimit:&orderBy:8&pageView:grid&minPrice:&maxPrice:&pageSize:&'

        requester = LuminatiSessionRequests(country_code)  # Init requester
        factory = StrategyFactory()
        dl_strategy = factory.get_download_strategy(website, country_code)(requester)  # Init download strategy
        ws_strategy = factory.get_website_strategy(website, country_code)(dl_strategy)  # Init website strategy
        # Get extracted data 
        # res = ws_strategy.execute(url, postal_code='41700')   
        res = ws_strategy.execute(url, max_results=25)
        # res = ws_strategy.execute(url)
        #pprint(res['products'][-1][-1]['rank'])
        
        all_products = []
        # for earch result just extend the list
        for product_list in res['products']:
            all_products.extend(product_list)
        with open('../extract-transform-listings/sample-l-all.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(all_products, indent=4,))
                # pprint(res)
        # Get Proxy IP info
        pprint(ws_strategy.download_strategy.requester.proxy_info)
    except Exception as e:
        print(str(e))