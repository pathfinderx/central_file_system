import logging, urllib, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MagasinetNoWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.magasinet.no'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)

        # NOTE: in this context, either _method/variable or __method/variable are considered private
        # its just a matter of programming style since im used to
        # _ protected method/variable
        # __ private method/variable

        # NOTE: we use one variable to store the instance state
        # the reason for this one is just a matter of programming style since
        # prior implementations does not really utilize instance state and I
        # dont want also to clutter the object w many instance variables vs prior implementations
        self.__state = {}

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            self.__state = {}

            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # initialize some values on the instance state
            self.__state.update({
                'url': url, 'page': page, 'start_rank': start_rank,
                'postal_code': postal_code, 'max_results': max_results, 'request_params': request_params,
                'headers': None, 'timeout': 10, 'cookies': None
            })

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    self.__state['timeout'] = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    self.__state['headers'] = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    self.__state['cookies'] = request_params['cookies']

            # current_product_count - number of product per iteration/page
            # total_product_count - total number of products so far
            # TOTAL_PRODUCT_COUNT - total number of products found in a certain category. Some websites provides this and some not. This is optional
            self.__state.update({
                'last_response': None, 'is_error': False, 'products': [],
                'current_product_count': 0, 'total_product_count': 0, 'TOTAL_PRODUCT_COUNT': 0,
            })

            has_url_params = self.__has_url_params(self.__state['url'])

            # variable that holds query params for page 2 and so on...
            next_page_params = {}

            # Website updated -- move fetching of data to API
            if self.__state['page'] < 1: self.__state['page'] = 1  # starts with 1
            cat_id = None
            cat_id = self.download_strategy.init_download(url)
            if cat_id:
                self.api_url = 'https://www.magasinet.no/graphql?query=query+category%28%24id%3AInt%21%24pageSize%3AInt%21%24currentPage%3AInt%21%24filters%3AProductAttributeFilterInput%21%24sort%3AProductAttributeSortInput%29%7Bcategory%28id%3A%24id%29%7Bid+description+name+product_count+meta_title+meta_keywords+meta_description+display_category_boxes_instead_of_products+cms_block%7Bcontent+__typename%7Dchildren%7Bid+name+url_path+url_suffix+image+__typename%7D__typename%7Dproducts%28pageSize%3A%24pageSize+currentPage%3A%24currentPage+filter%3A%24filters+sort%3A%24sort%29%7Bitems%7B__typename+productBrand%7Bname+__typename%7Ddescription%7Bhtml+__typename%7Did+media_gallery_entries%7Bid+label+position+disabled+file+__typename%7DproductLabel%7Bcustom+new+discount%7Bstatus+percent+percent_up_to+__typename%7D__typename%7Dmeta_title+meta_keyword+meta_description+name+price_range%7B...priceInfo+__typename%7Dshort_description%7Bhtml+__typename%7Dsku+small_image%7Burl+__typename%7Dstock_status+url_key+url_suffix+...on+ConfigurableProduct%7Bconfigurable_options%7Battribute_code+attribute_id+id+label+values%7Bdefault_label+label+store_label+use_default_value+value_index+swatch_data%7B...on+ImageSwatchData%7Bthumbnail+__typename%7Dvalue+__typename%7D__typename%7D__typename%7Dvariants%7Battributes%7Bcode+value_index+__typename%7Dproduct%7Bid+media_gallery_entries%7Bid+disabled+file+label+position+__typename%7Dsku+stock_status+__typename%7D__typename%7D__typename%7D%7Dpage_info%7Btotal_pages+__typename%7Daggregations%7Blabel+count+attribute_code+is_search_visible+options%7Blabel+value+count+__typename%7D__typename%7Dtotal_count+__typename%7D%7Dfragment+priceInfo+on+PriceRange%7Bminimum_price%7Bdiscount%7Bamount_off+percent_off+__typename%7Dfinal_price%7Bcurrency+value+__typename%7Dregular_price%7Bcurrency+value+__typename%7Dfull_price%7Bcurrency+value+__typename%7D__typename%7D__typename%7D&operationName=category&variables=%7B%22currentPage%22%3A1%2C%22id%22%3A'+str(cat_id)+'%2C%22filters%22%3A%7B\
                        %22category_id%22%3A%7B%22eq%22%3A%22'+str(cat_id)+'%22%7D%7D%2C%22pageSize%22%3A24%2C%22sort%22%3A%7B%7D%7D'
            else:
                raise


            while True:
                try:

                    print("On page: %d" % self.__state['page'])

                    if self.__state['page'] == 1:
                        url = self.api_url
                    else:
                        self.api_url = 'https://www.magasinet.no/graphql?query=query+category%28%24id%3AInt%21%24pageSize%3AInt%21%24currentPage%3AInt%21%24filters%3AProductAttributeFilterInput%21%24sort%3AProductAttributeSortInput%29%7Bcategory%28id%3A%24id%29%7Bid+description+name+product_count+meta_title+meta_keywords+meta_description+display_category_boxes_instead_of_products+cms_block%7Bcontent+__typename%7Dchildren%7Bid+name+url_path+url_suffix+image+__typename%7D__typename%7Dproducts%28pageSize%3A%24pageSize+currentPage%3A%24currentPage+filter%3A%24filters+sort%3A%24sort%29%7Bitems%7B__typename+productBrand%7Bname+__typename%7Ddescription%7Bhtml+__typename%7Did+media_gallery_entries%7Bid+label+position+disabled+file+__typename%7DproductLabel%7Bcustom+new+discount%7Bstatus+percent+percent_up_to+__typename%7D__typename%7Dmeta_title+meta_keyword+meta_description+name+price_range%7B...priceInfo+__typename%7Dshort_description%7Bhtml+__typename%7Dsku+small_image%7Burl+__typename%7Dstock_status+url_key+url_suffix+...on+ConfigurableProduct%7Bconfigurable_options%7Battribute_code+attribute_id+id+label+values%7Bdefault_label+label+store_label+use_default_value+value_index+swatch_data%7B...on+ImageSwatchData%7Bthumbnail+__typename%7Dvalue+__typename%7D__typename%7D__typename%7Dvariants%7Battributes%7Bcode+value_index+__typename%7Dproduct%7Bid+media_gallery_entries%7Bid+disabled+file+label+position+__typename%7Dsku+stock_status+__typename%7D__typename%7D__typename%7D%7Dpage_info%7Btotal_pages+__typename%7Daggregations%7Blabel+count+attribute_code+is_search_visible+options%7Blabel+value+count+__typename%7D__typename%7Dtotal_count+__typename%7D%7Dfragment+priceInfo+on+PriceRange%7Bminimum_price%7Bdiscount%7Bamount_off+percent_off+__typename%7Dfinal_price%7Bcurrency+value+__typename%7Dregular_price%7Bcurrency+value+__typename%7Dfull_price%7Bcurrency+value+__typename%7D__typename%7D__typename%7D&operationName=category&variables=%7B%22currentPage%22%3A1%2C%22id%22%3A'+str(self.__state['page'])+'%2C%22id%22%3A'+str(cat_id)+'%2C%22filters%22%3A%7B\
                        %22category_id%22%3A%7B%22eq%22%3A%22'+str(cat_id)+'%22%7D%7D%2C%22pageSize%22%3A24%2C%22sort%22%3A%7B%7D%7D'
                        url = self.api_url#'{}?p={}'.format(self.__state['url'], self.__state['page'])

                    raw_data, self.__state['last_response'] = self.download_strategy.download(url)

                    if not raw_data: break

                    data_json = json.loads(raw_data)
                    self.__state['bs'] = data_json['data']['products'] #BeautifulSoup(raw_data, 'lxml')
                    product_count_tag = len(data_json['data']['products']['items'])#self.__state['bs'].select_one('div.amount')
                    if product_count_tag:
                        # Get total product count
                        count = data_json['data']['products']['total_count'] #[int(s) for s in product_count_tag.text.split() if s.isdigit()]
                        self.__state['max_results'] = count #count[0]

                    products = self.get_products(self.__state['bs'])

                    # truncate the products according to the max_results
                    products = self.truncate_products(products)
                    products = self._extract(products, page=self.__state['page'], start_rank=self.__state['start_rank'])

                    self.__state['products'].append(products)
                    self.__state['current_product_count'] = len(products)
                    self.__state['total_product_count'] += self.__state['current_product_count']

                    if not self.has_next():
                        break

                    # get the next page params
                    # next_page_params = self.__get_next_page_params(self.__state['bs'])

                    self.__state['page'] += 1
                    self.__state['start_rank'] += self.__state['current_product_count']
                except DownloadFailureException:
                    self.__state['is_error'] = True
                    break
                except Exception:
                    raise

            return dict(
                is_error=self.__state['is_error'],
                last_page=self.__state['page'],
                last_response=self.__state['last_response'],
                products=self.__state['products']
            )

        except Exception:
            raise

    def get_products(self, tag):
        products = []

        # get products from list
        products = tag['items'] #product_tags = tag.select('li.item > div.inner-item')

        # for tag in product_tags:
        #     # get each product information
        #     product_tag = tag.select_one('h2 a')
        #     product_url = product_tag.get('href')
        #     data, response = self.download_strategy.download_api(product_url)
        #     data['url_source'] = product_tag
        #     data['url_value'] = product_url
        #     products.append(data)

        return products

    def get_price(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            product_price_tag = data['price_range']['minimum_price']['final_price']['value'] #data['product'].select_one('span.regular-price')

            if product_price_tag:
                # value = product_price_tag.text.strip().replace('\xa0', ' ')
                # source = str(product_price_tag).replace('\xa0', ' ')
                source, value = self.api_url, str(product_price_tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'NOK'
        return source, value

    def get_price_per_unit(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'NOK'  # Default currency
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, data, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'NOK'  # Default currency

        try:
            pass
            # full_price_tag = data['product'].select_one('div.full-price > span.price')

            # if full_price_tag:
            #     promo_price_value = price_value
            #     description_value = 'From ' + str(full_price_tag.text.strip().replace('\xa0', ' '))
            #     source = str(full_price_tag).replace('\xa0', ' ')

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # value = data['url_value']
            # source = str(data['url_source'])
            source, value = self.api_url, 'https://www.magasinet.no/'+data['url_key']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            brand_source = data['productBrand']#data['product'].select_one('span.brand')
            if brand_source:
                # value = brand_source.text
                # source = str(brand_source)
                source, value = self.api_url, brand_source['name']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            product_title = data['name']#data['product'].select_one('span.name')
            if product_title:
                # value = product_title.text
                # source = str(product_title)
                source, value = self.api_url, product_title
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # try:
        #     description_tag = data['product'].select_one('span.description')
        #     if description_tag:
        #         description_str = re.sub("<\/?\w+>", "", description_tag.text)
        #         value = description_str.strip()
        #         source = str(description_tag)
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = '5'  # default max stars

        # try:
        #     if 'rating_score_val' in data:
        #         value = str(data['rating_score_val'])
        #         source = data['rating_score_src'] if 'rating_score_src' in data else data
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_rating_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # try:
        #     if 'review_count_val' in data:
        #         value = str(data['review_count_val'])
        #         source = data['review_count_src'] if 'review_count_src' in data else data
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            availability = data['stock_status']#data['product'].select_one('p.availability > span.value')
            if availability:
            # value = availability.text
            # source = str(availability)
                source, value = self.api_url, availability
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            image_tags = data['small_image']['url']#data['product'].select('div#photozoom__product-images a')

            # for tag in image_tags:
            #     image_url = tag.get('href')
            #     value.append(image_url)
            if image_tags:
                source = self.api_url
                value.append(image_tags)

            source = str(image_tags)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################

    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def __has_url_params(self, url):
        parsed_url = urllib.parse.urlparse(url)
        if parsed_url.query:
            return True
        return False

    def __get_next_page_params(self, tag):
        next_tag = tag.select_one('li#san_pagingBottomNext button')
        return json.loads(next_tag['data-page'])

    def truncate_products(self, products):
        if self.__state['max_results'] is None:
            return products

        remaining_length = self.__state['max_results'] - self.__state['total_product_count']
        # truncate products by slicing
        return products[0:remaining_length]

    def has_next(self):
        if self.__state['max_results'] is not None and \
                self.__state['total_product_count'] >= self.__state['max_results']:
            return False

        if self.__state['current_product_count'] <= 0:
            return False

        return True

    def _extract(self, products, page, start_rank):

        template = get_result_base_template()
        results = []

        for product in products:
            result = deepcopy(template)

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(self.__state['bs'])

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
            result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
            result["price_per_unit"]['currency']["source"], \
            result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(product)

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
            result["promo"]['price']["value"], \
            result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_rating_score(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(product)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results
