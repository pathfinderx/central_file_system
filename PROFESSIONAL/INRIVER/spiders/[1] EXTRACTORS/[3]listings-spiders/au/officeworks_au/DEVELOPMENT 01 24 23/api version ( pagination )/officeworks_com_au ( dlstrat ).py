import re, json, urllib.parse
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from typing import Union

class OfficeworksComAuDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Accept': 'image/avif,image/webp,image/apng,image/*,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.9',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=None, cookies=None)

            if res.status_code in [200, 201]:
                if self.hasProductsOnPage(res.text):
                    raw_data = res.content.decode('utf-8')
                else:
                    res = self.get_api_data(res.text)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def hasProductsOnPage(self, raw_data) -> bool:
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            products = soup.select('.sc-bdVaJa.Tile-iqbpf7-0.PLLTo') or \
                soup.select('[data-ref="product-tile"]')

            return True if (isinstance(products, list) and len(products) > 0) else False

        except Exception as e:
            print('Error at: dlstrat -> hasProductsOnPage() -> with message: ', e)

    def get_api_products_availability(self, sku_list):
        try:
            if sku_list:
                skus = '|'.join(sku_list)
                postal_code = 3000 # temporary static postal code
                api_url = f"https://www.officeworks.com.au/catalogue-app/api/availabilities/summary/store/W320/postcode/{postal_code}?partNumbers={skus}"

                res = self.requester.get(api_url, timeout=10000, headers=None, cookies=None)

                if res.status_code in [200, 201]:
                    return res.text

        except Exception as e:
            print('Error at: dlstrat -> get_api_products_availability -> with message: ', e)

    def get_api_data(self, raw_data) -> Union[str, None]:
        try:
            payload = self.get_api_payload(raw_data)
            api_url = "https://k535caawve-2.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(3.35.1)%3B%20Browser%20(lite)%3B%20react-instantsearch%205.4.0%3B%20JS%20Helper%202.26.1&x-algolia-application-id=K535CAAWVE&x-algolia-api-key=8a831febe0110932cfa06ff0e2024b4f"

            res = self.requester.post(api_url, timeout=10000, headers=None, cookies=None, data=payload) 

            if res.status_code in [200, 201]:
                return res
            return None

        except Exception as e:
            print('Error at: dlstrat -> get_api_data() -> with message: ', e)

    def get_api_payload(self, raw_data) -> Union[str, None]:
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            script_tag, _json = soup.select_one('script[type="application/ld+json"]'), None
            _json = json.loads(script_tag.get_text(strip=True)) if script_tag else None

            if (_json and
                'itemListElement' in _json and
                isinstance(_json['itemListElement'], list)):

                category_options_list, filters, rule_contexts = list(), None, None

                # category extraction from json converted html data as: category_options_list
                for x in _json['itemListElement']:
                    if (isinstance(x, dict) and 'item' in x):
                        for y in x['item']:
                            if (y in ['@id']):
                                category_options_list.append(x['item'][y])
              
                # category specific selection from category_options_list as filters
                if (category_options_list):
                    max_len_category = max(category_options_list, key=len) # longest content = complete category
                    if max_len_category:
                        filters = max_len_category.split('officeworks/c/')[-1]

                    filters = urllib.parse.quote_plus(filters)

                # category specific selection from category_options_list as rule_contexts
                if (category_options_list):
                    pre_rule_contexts = list()
                    arg1_list, arg1 = list(), str()
                    
                    for x in category_options_list:
                        arg1_list.append(f"{x.split('/')[-1]}")

                    arg1 = '_'.join(arg1_list) # join by underscore: https://prnt.sc/Xmc0KJHLuyHt
                    pre_rule_contexts.append(arg1) # pre append to make it first from the list order
                    
                    for x in category_options_list:
                        pre_rule_contexts.append(f"{x.split('/')[-1]}") # append for the rest 

                    pre_rule_contexts.append('ANONYMOUS') # append the 'ANONYMOUS' as last index

                    rule_contexts = str(pre_rule_contexts).replace(r' ', '') # remove space to exclude from url parsing
                    rule_contexts = urllib.parse.quote_plus(rule_contexts) # convert to url encoded string

                args = {
                    'arg_filters': filters,
                    'arg_rule_contexts': rule_contexts
                }
                
                # add to form_data
                form_start = '{"requests":[{"indexName":"prod-product-wc-bestmatch-personal","params":"query=&'
                form_data = """hitsPerPage=24&page=0&analyticsTags=%5B%22browse%22%5D&highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&clickAnalytics=true&optionalFilters=%5B%22availState%3AVIC%22%5D&sumOrFiltersScores=true&filters=(categorySeoPaths%3A%22{arg_filters}%22)&ruleContexts={arg_rule_contexts}&facets=%5B%22materialType%22%2C%22byodDeviceType%22%2C%22suitableOutdoors%22%2C%22MinimumUserHeight%22%2C%22shopByGrade%22%2C%22stackableFurniture%22%2C%22chairAdjLumbar%22%2C%22printingTechnologyLabels%22%2C%22chairHeadrest%22%2C%22greenerChoice%22%2C%22sizeDescription%22%2C%22powerBatteryChargeAmpHours%22%2C%22operatingHand%22%2C%22throwDistance%22%2C%22totalNumberOfLabels%22%2C%22shelfNumberShelves%22%2C%22freeSyncCompatible%22%2C%22curriculumCode%22%2C%22pivotAdjustment%22%2C%22gSyncCompatible%22%2C%22shopBySubject%22%2C%22swivelAdjustment%22%2C%22noiseCancelling%22%2C%22australianMade%22%2C%22tiltAdjustment%22%2C%22selfAdhesive%22%2C%22industryType%22%2C%22maxLoadWeight%22%2C%22bagStyle%22%2C%22connectivity%22%2C%22travelRegion%22%2C%22adjustableHeight%22%2C%22ChargePorts%22%2C%22broadbandGen%22%2C%22rangedOnline%22%2C%22handsetIncludedHandsets%22%2C%22photoCapacityQuantity%22%2C%22carpetThickness%22%2C%22materialFacet%22%2C%22recommendedUse%22%2C%22rangedRetail%22%2C%22commercialGrade%22%2C%22warrantyFacet%22%2C%22bluetoothCompatibility%22%2C%22frameStyle%22%2C%22tsaApproved%22%2C%22refreshRate%22%2C%22diaryLayout%22%2C%22compatibabilityCustomFitAndroid%22%2C%22compatibabilityCustomFitApple%22%2C%22performanceMaxThickness%22%2C%22videoResolution%22%2C%22printingTechnologyPrinters%22%2C%22deskType%22%2C%22maximumPowerRating%22%2C%22deskFeatures%22%2C%22learningSkillsFocus%22%2C%22afrdiRating%22%2C%22numberOfUsb20Ports%22%2C%22performanceHealthMonitoringFunctions%22%2C%22usbFlashLidType%22%2C%22capacityKeys%22%2C%22heightRange%22%2C%22chairAdjBackTilt%22%2C%22operatingSystemEdition%22%2C%22numberOfReamsPerCarton%22%2C%22numberOfPowerPorts%22%2C%22fitsDevice%22%2C%22padSheets%22%2C%22chairAdjSeatDepth%22%2C%22numberOfUsb32Ports%22%2C%222SidedPrinting%22%2C%22boardSizeFacet%22%2C%22printerConnectivityTechnology%22%2C%22performanceBrightness%22%2C%22performanceResolution%22%2C%22processorClockSpeed%22%2C%22numberOfProcessorCores%22%2C%22multipackSize%22%2C%22scannerType%22%2C%22performanceEstimatedCartridgeYieldSheets%22%2C%22connectivityTechnology%22%2C%222sidedScanning%22%2C%22optusPlanType%22%2C%22maxSupportedDocumentSize%22%2C%22earPlacement%22%2C%22deviceConnectivityTechnology%22%2C%22socketType%22%2C%22deviceCaseCompatibility%22%2C%22shelfMaxLoadShelf%22%2C%22flightTime%22%2C%22powerPowerType%22%2C%22rollLength%22%2C%22wallStrengthThickness%22%2C%22energyRating%22%2C%22runTimeHours%22%2C%22staplingCapacity%22%2C%22tipSize%22%2C%22labelsPerSheetRoll%22%2C%22capacityBinder%22%2C%22securityLevel%22%2C%22inputOutputCableType%22%2C%22powerBatteryTechnology%22%2C%22licenceValidityPeriod%22%2C%22deviceLocation%22%2C%22powerSource%22%2C%22learningAgeRange%22%2C%22opticalZoom%22%2C%22caseFeaturesNumberOfCompartments%22%2C%22serviceProvider%22%2C%22chassisType%22%2C%22shelfHeightAdjust%22%2C%22tapeWidth%22%2C%22primaryCameraVideo%22%2C%22topSurfaceDepth%22%2C%22topSurfaceWidth%22%2C%22labellerKeyboardLayout%22%2C%22placementPlacingMounting%22%2C%22colour%22%2C%22ringRingSize%22%2C%22fullSizeInnerDimensions%22%2C%22smartHomeCompatibility%22%2C%22protection%22%2C%22unitsOfMeasure%22%2C%22rulerLength%22%2C%22lightBulbType%22%2C%22sizeNumber%22%2C%22protectionType%22%2C%22wastebinCapacityRange%22%2C%22sharpenerSize%22%2C%22connectivityDisplayConnections%22%2C%22graphicsProcessor%22%2C%22sizeCapacity%22%2C%22stylusPenIncluded%22%2C%22labellingHomeUseFacet%22%2C%22portsTotalNumberOfNetworkingPorts%22%2C%22labellingOfficeUseFacet%22%2C%22brushShape%22%2C%22softwareDistributionMedia%22%2C%22chairAdjChairTilt%22%2C%22numberOfUsb30Ports%22%2C%22surgeSuppression%22%2C%22processorType%22%2C%22processorManufacturer%22%2C%22ramInstalledSize%22%2C%22placementVesaMountCompatibility%22%2C%22performanceApproximateNumberOfImpressions%22%2C%22chairAdjSeatTilt%22%2C%22runTime%22%2C%22connectivityWifiBands%22%2C%22brushhairtype%22%2C%22papersize%22%2C%22ciewhiteness%22%2C%22boardSurfmaterial%22%2C%22microphoneType%22%2C%22numberOfRings%22%2C%22spineSize%22%2C%22dualSimCompatible%22%2C%22chairAdjBackHeight%22%2C%22maxProcessorClockSpeed%22%2C%22performanceShredderCutType%22%2C%22licenceType%22%2C%22byodAge%22%2C%22audioSource%22%2C%22stampInking%22%2C%22forestProductSchemeName%22%2C%22secondaryProcessorType%22%2C%22hardDriveType%22%2C%22portsNumberOfUsbChargePorts%22%2C%22skillLevel%22%2C%22storageHardDriveCapacity%22%2C%22labellingIndustrialUseFacet%22%2C%22restTime%22%2C%22glutenFree%22%2C%22connectivityDisplayConnectionsPanels%22%2C%22rulingType%22%2C%22responseTime%22%2C%22displayResolution%22%2C%22automaticPaperFeed%22%2C%22keyboardCompatibility%22%2C%22connectorType%22%2C%22paperWeightGsm%22%2C%22touchScreen%22%2C%222SidedCopying%22%2C%22baseWheels%22%2C%22iPadGeneration%22%2C%22freeFromAnimal%22%2C%22storageHardDriveCapacityComputingDevices%22%2C%22contentLayout%22%2C%22storageIncludedFlashMemory%22%2C%22supportedMemoryCards%22%2C%22operatingPlatformCompatibility%22%2C%22scannerScanResolution%22%2C%22foldedDimensions%22%2C%22automaticDocumentFeederCapacity%22%2C%22switched%22%2C%22100RecycledProduct%22%2C%22maximumRecommendedDailyUsage%22%2C%22cableLength%22%2C%22numberOfUsb31Ports%22%2C%22displaySize%22%2C%22brand%22%2C%22performancePrintResolution%22%2C%22displayPanelType%22%2C%22envelopeSize%22%2C%22interfaceHardDrive%22%2C%22stapleSize%22%2C%22maximumPunchingCapacity%22%2C%22storageStorageCapacity%22%2C%22bulkbuyOnline%22%2C%22storageInternalMemorySize%22%2C%22chairArmrests%22%2C%22interfaceType%22%2C%22lidIncluded%22%2C%22up1Category%22%2C%22price%22%2C%22catPaths%22%5D&tagFilters=""".format(**args)
                form_end = '"}]}'

                payload = f"{form_start}{form_data}{form_end}"

                return payload if payload else None
            return None
                
        except Exception as e:
            print('Error at: dlstrat -> get_api_payload() -> with message: ', e)
