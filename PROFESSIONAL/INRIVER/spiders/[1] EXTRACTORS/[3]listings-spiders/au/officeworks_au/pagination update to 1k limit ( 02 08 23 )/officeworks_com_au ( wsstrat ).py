import logging
import json, re 
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

from typing import Union

class OfficeworksComAuWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.officeworks.com.au'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.pagination_url = list() 
        self.pagination_counter = -1
        self.preserved_url = None
        self.product_availabilities = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)
            self.preserved_url = url

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            next_url = ''

            while True:
                try:
                    """ url alternator """
                    if page == 0:
                        page += 1
                        next_url = url

                    else:
                        next_url = next_url

                    """ Downloader """
                    print("On page: %d" % page)
                    print(next_url)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break
                    
                    """ Product Extraction """
                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    """ Pagination loader and breaker"""
                    if not self.pagination_url: # to prevent infinite extraction of url
                        self.set_next_urls(raw_data) # load the urls
                    
                    self.pagination_counter += 1 # modified pagination counter
                    
                    if (self.pagination_counter <= len(self.pagination_url) - 1):
                        next_url = self.pagination_url[self.pagination_counter]
                    elif (self.pagination_counter >= len(self.pagination_url) - 1):
                        break

                    if not next_url:
                        break

                    """ Product count increment and limiter """
                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def set_next_urls(self, raw_data):
        try:
            _json = json.loads(raw_data)
            page_limit = None
            
            if (_json 
                and 'results' in _json 
                and isinstance(_json['results'], list) 
                and 'nbPages' in _json['results'][0]):

                page_limit = _json['results'][0]['nbPages']

            if page_limit:
                for x in range(2, page_limit+1):
                    self.pagination_url.append(f"{self.preserved_url}?page={x}")
                    
        except Exception as e:
            print('Error at: wsstrat -> set_next_urls() -> with message: ', e) 

    def get_products(self, soup) -> Union[list, dict, None]:
        try:
            products = soup.select('.product-item')

            if products:
                return products
            else:
                _json = json.loads(soup.get_text(strip=True))
                if (_json and
                    'results' in _json and
                    isinstance(_json['results'], list) and
                    'hits' in _json['results'][0]):

                    return _json['results'][0]['hits']
            return None

        except Exception as e:
            print('Error at: wsstrat -> get_products() -> with message: ', e) 

    def set_product_availabilities(self, product_tags):
        try:
            sku_list = list()

            for x in product_tags:
                if ('sku' in x):
                    sku_list.append(x.get('sku'))

            requested_data = self.download_strategy.get_api_products_availability(sku_list)

            if requested_data:
                try:
                    _json = json.loads(requested_data)
                    self.product_availabilities = _json

                except Exception as e:
                    print('Error at: wsstrat -> set_product_availabilities() -> product availability json decode -> with message: ', e)

        except Exception as e:
            print('Error at: wsstrat -> set_product_availabilities() -> with message: ', e)  

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []
        
        product_tags = self.get_products(soup)
        self.set_product_availabilities(product_tags)

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency()

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('price' in product_tag):
                    source, value = str(product_tag), product_tag.get('price')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'USD'  # Default value
        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'USD'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'USD'  # Default value for this site
        price_value = Defaults.GENERIC_NOT_FOUND.value
        return source, description_value, price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('seoPath' in product_tag):
                    base_url = f'https://{self.__class__.WEBSITE}'
                    source, value = str(product_tag),   f"{base_url}{product_tag.get('seoPath')}" 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('brand' in product_tag):
                    source, value = str(product_tag), product_tag.get('brand') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('name' in product_tag):
                    source, value = str(product_tag), product_tag.get('name') 
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            if isinstance(product_tag, dict):
                if ('descriptionShort' in product_tag):
                    source, value = str(product_tag), product_tag.get('descriptionShort') 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            if isinstance(product_tag, dict):
                if ('rating' in product_tag):
                    source, value = str(product_tag), product_tag.get('rating') 

        except Exception as e:
            print('Error at: wsstrat -> get_ratings() -> with message: ', e)

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('reviewCount' in product_tag):
                    source, value = str(product_tag), product_tag.get('reviewCount') 

        except Exception as e:
            print('Error at: wsstrat -> get_reviews() -> with message: ', e)

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                product_sku = None
                if (product_tag and 'sku' in product_tag):
                    product_sku = product_tag.get('sku') 

                if (product_sku and self.product_availabilities):
                    for x in self.product_availabilities:
                        if ('sku' in x and 
                            'deliveryType' in x and
                            product_sku == x.get('sku')):

                            source, value = str(product_tag), x.get('deliveryType')
                            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if isinstance(product_tag, dict):
                if (product_tag and
                    'image' in product_tag):
                    
                    source, value = str(product_tag), product_tag.get('image')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value