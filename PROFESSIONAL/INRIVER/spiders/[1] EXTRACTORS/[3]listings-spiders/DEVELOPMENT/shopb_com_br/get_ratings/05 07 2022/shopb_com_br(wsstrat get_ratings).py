import logging, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException 
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ShopbComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.shopb.com.br'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_page = None
        self.url = None
        self.main_soup = None
        self.ratings_tag = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)
            self.url = url

            if max_results:
                assert isinstance(max_results, int)

            timeout = 10
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36',
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            while True:
                try:
                    if page == 0:
                        page += 1
                        next_url = url

                    else:
                        if self.next_page:
                            next_url = self.next_page
                        else: 
                            break

                    print("On page: %d" % page)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    # request for ratings value first
                    if raw_data:
                        self.get_ratings_values(raw_data)

                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise
    
    def find_next_page(self, soup):
        tag = soup.select('div.pagination > ul > li > a[rel="nofollow"]')

        if tag:
            if len(tag) > 0 and tag[0].has_attr('href'):
                self.next_page = self.url + tag[0].get('href')
            else:
                self.next_page = None
        else:
            self.next_page = None

    def get_product_tags(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        product_tags = soup.select_one('div#listagemProdutos > ul').select('li[class="span3"]')
        return product_tags if product_tags else None

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []

        self.find_next_page(soup)
        
        product_tags = self.get_product_tags(raw_data)

        if not product_tags:
            return results

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank :
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('strong[data-sell-price]')
            if tag:
                if tag.has_attr('data-sell-price'):
                    source, value = str(tag), tag.get('data-sell-price')
                else:
                    source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'  # Default value | Currency not Found
        return source, value

    def get_price_per_unit(self, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency 
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_value, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency

        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('a.produto-sobrepor') or product_tag.select_one('a')

            if tag and tag.has_attr('href'):
                source, value = str(tag), tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('a.produto-sobrepor')
            if tag and tag.has_attr('title'):
                source, value = str(tag), tag.get('title').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            tag = product_tag.select_one('div.listagem-item')
            if tag:
                attributes, product_id = [x for x in tag.attrs.get('class') if x.find('prod-id') > -1], None
                product_id = re.search(r'([\d]+)', attributes[0]).group(0) if attributes else None

                if product_id:
                    star_ratings_data = str()
                    for x in self.ratings_tag:
                        if (x['productId'].replace("'",'') == product_id and 'data' in x):
                            star_ratings_data = x.get('data')

                    if star_ratings_data:
                        star_ratings_soup = BeautifulSoup(star_ratings_data, 'lxml')
                        fa_star, rating_score = star_ratings_soup.select('i.fa.fa-star.yv-star-color'), 0
                        rating_score_list = [x + 1 for x in range(len(fa_star))] if fa_star else 0
                        value = str(rating_score_list[-1]) if rating_score_list else '0'
                        source = str(tag) if value else Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass # As observed, reviews: javascript rendered
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        tag = product_tag.select_one('div.acoes-produto.hidden-phone > a') or product_tag.select_one('div.bandeiras-produto')
        if tag:
            source, value = str(product_tag), tag.get_text().strip()
        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = product_tag.select_one('img')
            if tag and tag.has_attr('src'):
                source = str(product_tag)
                value.append(tag.get('src'))

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def get_ratings_values(self, raw_data):
        try:
            assert isinstance(raw_data, str)
            self.main_soup = BeautifulSoup(raw_data, 'lxml')
                
            res, _json = self.download_strategy.get_ratings(self.main_soup), dict()
            if res:
                _json = json.loads(res) if res else None

                if _json and 'html' in _json:
                    self.ratings_tag = json.loads(_json.get('html'))
        except Exception as e:
            print('Error in: wsstrat -> get_ratings_values() -> with message: ', e)