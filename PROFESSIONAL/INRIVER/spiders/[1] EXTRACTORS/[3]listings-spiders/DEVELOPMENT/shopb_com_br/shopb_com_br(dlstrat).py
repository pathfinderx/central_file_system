import re
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ShopbComBrDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'cookie': '_ga=GA1.3.838762533.1651652424; _gid=GA1.3.1120804489.1651652424; _gcl_au=1.1.1399748387.1651652424; _fbp=fb.2.1651652426190.316313348; _hjSessionUser_2945039=eyJpZCI6ImQ4YmJmZmUwLTM3Y2UtNWRmOC05NDc5LWQ5ZDM4ZGQ4ZTU0NCIsImNyZWF0ZWQiOjE2NTE2NTI0MjY4NDQsImV4aXN0aW5nIjp0cnVlfQ==; owa_v=cdh%3D%3Ea5719cf8%7C%7C%7Cvid%3D%3E1651652425051527806%7C%7C%7Cfsts%3D%3E1651652425%7C%7C%7Cdsfs%3D%3E0%7C%7C%7Cnps%3D%3E2; owa_s=cdh%3D%3Ea5719cf8%7C%7C%7Clast_req%3D%3E1651668002%7C%7C%7Csid%3D%3E1651666838604824944%7C%7C%7Cdsps%3D%3E0%7C%7C%7Creferer%3D%3E%28none%29%7C%7C%7Cmedium%3D%3Edirect%7C%7C%7Csource%3D%3E%28none%29%7C%7C%7Csearch_terms%3D%3E%28none%29',
                'pragma': 'no-cache',
                'referer': 'https://www.shopb.com.br/cards-especiais',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res