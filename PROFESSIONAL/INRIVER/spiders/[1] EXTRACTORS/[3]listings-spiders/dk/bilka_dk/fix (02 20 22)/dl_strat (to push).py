import logging, urllib, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class BilkaDkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.bilka.dk'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.__state = {}

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            self.__state = {}
            
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # initialize some values on the instance state
            self.__state.update({
                'url': url, 'page': page, 'start_rank': start_rank,
                'postal_code': postal_code, 'max_results': max_results, 'request_params': request_params,
                'headers': None, 'timeout': 10, 'cookies': None
            })

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    self.__state['timeout'] = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    self.__state['headers'] = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    self.__state['cookies'] = request_params['cookies']

            # current_product_count - number of product per iteration/page
            # total_product_count - total number of products so far
            # TOTAL_PRODUCT_COUNT - total number of products found in a certain category. Some websites provides this and some not. This is optional
            self.__state.update({
                'last_response': None, 'is_error': False, 'products': [],
                'current_product_count': 0, 'total_product_count': 0, 'TOTAL_PRODUCT_COUNT': 0,
            })

            if self.__state['page'] < 1: self.__state['page'] = 1 # starts with 1
            while True:
                try:

                    print("On page: %d" % self.__state['page'])

                    page_url = url
                    if self.__state['page'] == 1:
                        raw_data, self.__state['last_response'] = self.download_strategy.download(page_url)
                        if not raw_data: break
                        data = BeautifulSoup(raw_data, 'lxml')
                        page_url = self.__get_api_url(data)
                    else:
                        page_url = self.__get_page_url(self.__state['category'], self.__state['page'])

                    self.__state['source'] = page_url
                    raw_data, self.__state['last_response'] = self.download_strategy.download(page_url)
                    if not raw_data: break
                    self.__state['bs'] = json.loads(raw_data)
                    
                    products = self.get_products(self.__state['bs'])

                    # truncate the products according to the max_results
                    products = self.truncate_products(products)
                    products = self._extract(products, page=self.__state['page'], start_rank=self.__state['start_rank'])

                    self.__state['products'].append(products)
                    self.__state['total_product_count'] += self.__state['current_product_count']

                    if not self.has_next():
                        break

                    self.__state['page'] += 1
                    self.__state['start_rank'] = self.__state['current_product_count']+1

                except DownloadFailureException:
                    self.__state['is_error'] = True
                    break
                except Exception:
                    raise
            
            return dict(
                is_error=self.__state['is_error'],
                last_page=self.__state['page'],
                last_response=self.__state['last_response'],
                products=self.__state['products']
            )

        except Exception:
            raise


    def get_products(self, data):
        products = []
        products = data['products']['documents']

        return products

    def get_price(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            price = data['display_sales_price']
            if price:
                price_value = price
                source, value = str(self.__state['source']), str(price_value)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'DKK'
        return source, value

    def get_price_per_unit(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'DKK'  # Default currency
        # not available
        
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, data, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'DKK'  # Default currency

        try:
            old_price_value = data['display_list_price']
            if old_price_value and old_price_value != price_value:
                promo_price_value = price_value
                description_value = 'From ' + str(old_price_value)
                source = str(self.__state['source'])

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url = data['product_url']
            value = f'https://www.bilka.dk{url}'
            source = str(self.__state['source'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            brand = data['brand']
            if brand:
                value = brand
                source = str(self.__state['source'])

        except KeyError:
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value
        
        return source, value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            value = data['name']
            source = str(self.__state['source'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            description = BeautifulSoup(data['description'], 'lxml')
            if description:
                desc_value = " ".join(description.stripped_strings)
                source, value = str(self.__state['source']), desc_value
                
        except KeyError:
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = '5' # default max stars

        return source, value, max_value

    def get_rating_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
       
        try:
            stocks_online = data['stock_count_online']
            availability = 'Ikke på lager online'
            if stocks_online >= 100:
                availability = 'På lager online (100+)'
            if stocks_online >= 50 and stocks_online < 100:
                availability = 'På lager online (50+)'
            if stocks_online >= 25 and stocks_online < 50:
                availability = 'På lager online (25+)'
            if stocks_online >= 5 and stocks_online < 25:
                availability = 'På lager online (5+)'
            if stocks_online > 0 and stocks_online < 5:
                availability = 'Fa på lager online'
                
            value = availability
            source = str(self.__state['source'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            image_primary = data['image_primary']
            if image_primary:
                source = str(self.__state['source'])
                value = [image_primary]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################


    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def __has_reached_limit(self):
        if self.__state['max_results'] is not None and self.__state['current_product_count'] >= self.__state['max_results']:
            return True
        return False

    def __get_api_url(self, data):
        pattern = re.compile(r'return')
        script = data.find("script", text=pattern)
        tag = re.findall('\(.*?\)',str(script))[1] if 'cfh_nodes' in re.findall('\(.*?\)',str(script))[1] else re.findall('\(.*?\)',str(script))[3] #temporary fix
        matches = tag.split('","') 
        for item in matches:
            if 'cfh_nodes' in item:
                self.__state['category'] = item
                url = self.__get_page_url(item, self.__state['page'])
                break
            
        return url
    def __get_page_url(self, category, page_number):
        # url = f'https://api.sallinggroup.com/v1/ecommerce/bilka/search/sp?'\
                # f'p={page_number}&pl={category}&apiKey=d889a569-6548-4a11-b447-c653730f1747'

        url = f'https://api.sallinggroup.com/v1/ecommerce/bilka/search/sp?'\
                f'pl={category}&apiKey=d889a569-6548-4a11-b447-c653730f1747'

        return url
    
    def truncate_products(self, products):
        if self.__state['max_results'] is None:
            return products
            
        remaining_length = self.__state['max_results'] - self.__state['total_product_count']
        # truncate products by slicing
        return products[0:remaining_length]

    def has_next(self):
        if self.__has_reached_limit():
            return False

        if self.__state['current_product_count'] <= 0:
            return False


        number_of_pages = self.__state['bs']['products']['pagination']['totalNumberOfPages']
        if not number_of_pages or number_of_pages <= self.__state['page']:
            return False

        return True

    def _extract(self, products, page, start_rank):
        
        template = get_result_base_template()
        results = []

        for product in products:
            result = deepcopy(template)

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(self.__state['bs'])

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(product)

            # # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_rating_score(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(product)

            # # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results
