import logging
import json, re, html
from urllib.parse import urlparse
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class VodafoneCoUkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.vodafone.co.uk'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            self.url = next_url = url
            

            while True:
                try:
                    # One page only
                    if page == 0:
                        page += 1
                    else:
                        break

                    print("On page: %d" % page)

                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def __extract_json(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')
        data = []
        brand = None
        tag = soup.find(
            'script',
            text = re.compile(r'var\s_hydratedData.*')
        )

        if tag:
            try:
                text = html.unescape(tag.get_text()).split(';')[0].split(' = ')[-1]
                _json = json.loads(text, strict=False)
            except:
                _rgx = re.search(r'=.({.*});\n', tag.get_text().strip())
                _json = json.loads(_rgx.group(1), strict=False)

            if 'deviceList' in _json:
                if 'filters' in _json['deviceList']:
                    for i in _json['deviceList']['filters']:
                        if 'url' in i:
                            if i['url'].lower() == urlparse(self.url).path:
                                if 'name' in i:
                                    brand = i['name']


                if 'devices' in _json['deviceList']:
                    if brand:
                        for i in _json['deviceList']['devices']:
                            if 'make' in i:
                                if brand.lower() == i['make']:
                                    data.append(i)
                    else:
                        data = _json['deviceList']['devices']
            
            if 'handsetList' in _json:
                data = _json['handsetList']['deviceGroups']
        else:
            tag = soup.find('script', text= re.compile(r'window.__STATE'))
            if tag:
                # UI # 2
                # Sample URL : https://www.vodafone.co.uk/mobile/phones/pay-monthly-contracts?icmp=uk~1_consumer~topnav~1_shop~1_phones_&_tablets~1_pay_monthly_phones&linkpos=topnav~1~1~1~1
                # Sample UI : https://prnt.sc/wlh14i
                _rgx = re.search(r'window.__STATE = (\{.*\});', tag.get_text().strip())
                if _rgx:
                    _json = json.loads(_rgx.group(1))
                    if 'HandsetListPageSSRWrapped' in _json['_dataFromServerRender']:
                        data = _json['_dataFromServerRender']['HandsetListPageSSRWrapped']['initialData']['listData']['deviceGroups']
                    else:
                        # UI # 3
                        # Sample URL : https://www.vodafone.co.uk/mobile/5g-phones?icmp=uk~1_consumer~topnav~2_why_vodafone~2_5g~3_5g_phones_&_devices&linkpos=topnav~1~2~2~3
                        # Sample UI : https://prnt.sc/wlh0iv
                        _json_product = None
                        for page_content in _json['_dataFromServerRender']['Page']['pageContent']:
                            if 'variant' in _json['_dataFromServerRender']['Page']['pageContent'][page_content]:
                                if 'product' in _json['_dataFromServerRender']['Page']['pageContent'][page_content]['variant']:
                                    _json_product = _json['_dataFromServerRender']['Page']['pageContent'][page_content]
                                    break
                        if _json_product:
                            for products in _json_product['content']:
                                data.append(_json_product['content'][products])

        return data, str(tag)

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        data = self.__extract_json(raw_data)
        template = get_result_base_template()
        results = []
        product_tags = None

        if data[0]:
            product_tags, source = data
        else:
            # UI 4
            # Sample URL : https://www.vodafone.co.uk/business/business-mobile-phones/5g
            # Sample UI : https://prnt.sc/wllckc
            product_tags = soup.select('.accessory__content')
            source = None

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue            

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag, source)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag, source)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, source, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag, source)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag, source)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag, source)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag, source)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag, source)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag, source)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag, source)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag, source)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _source:
                data, source = product_tag, _source
                if 'upfrontPrice' in data:
                    value = str(data['upfrontPrice'])
                elif 'oneOffPrice' in data:
                    # UI 2
                    value = str(data['oneOffPrice']['gross']['value'])
            # UI 3 and 4 No Price
            else:
                tag = product_tag.select_one('.accessory__header.container-lv5 h3')
                if tag:
                    source, value = str(tag),tag.get_text().strip()


        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR' # Default value for this site

        try:
            if _source:
                data, source = product_tag, _source
                if 'oneOffPrice' in data:
                    # UI 2
                    value = str(data['oneOffPrice']['gross']['uom'])
            # UI 3 and 4 No Price Currency
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, _source, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site

        try:
            if _source:
                data, source = product_tag, _source
                
                if 'primaryPromotionName' in data:
                    # UI 2
                    description_value = data['primaryPromotionName']
                    promo_price_value = price_value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _source:
                data, source = product_tag, _source
                
                if '_links' in data:
                    # UI 2
                    url = self.url.split('?')[0]
                    if 'get-device-group-journey' in data['_links']:
                        value = '{}/{}/{}'.format(url, data['_links']['get-device-group-journey']['parameters']['make'], data['_links']['get-device-group-journey']['parameters']['model'])
                    elif 'create-device-group-journey' in data['_links']:
                        value = '{}/{}/{}'.format(url, data['_links']['create-device-group-journey']['parameters']['make'], data['_links']['create-device-group-journey']['parameters']['model'])
                elif 'model' in data:
                    # UI 1
                    value = '{}/{}'.format(self.url, data['model'])
                elif 'cta1URL' in data:
                    # UI 3
                    value = data['cta1URL']
            else:
                # UI 4
                tag = product_tag.select_one('.accessory__link a')
                if tag and tag.has_attr('href'):
                    source = str(tag)
                    value = tag.get('href')
                else:
                    tag = product_tag.select_one('.button.button--primary')
                    if tag and tag.has_attr('href'):
                        source = str(tag)
                        value = tag.get('href')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _source:
                data, source = product_tag, _source
                
                if 'make' in data:
                    # UI 1
                    value = data['make']
                elif '_links' in data:
                    # UI 2
                    if 'get-device-group-journey' in data['_links']:
                        value = data['_links']['get-device-group-journey']['parameters']['make']
                    elif 'create-device-group-journey' in data['_links']:
                        value = data['_links']['create-device-group-journey']['parameters']['make']

                elif 'mainCopy' in data:
                    # UI 3
                    brand_soup = BeautifulSoup(data['mainCopy'], 'lxml')
                    brand_tag = brand_soup.select_one('strong')
                    if brand_tag:
                        source = str(brand_tag)
                        value = brand_tag.get_text().strip()
            else:
                tag = product_tag.select_one('.accessory__brand')
                if tag:
                    source, value = str(tag),tag.get_text().strip().split(' ')[0]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _source:
                data, source = product_tag, _source
                
                if 'cta1Copy' in data:
                    # UI 3, name is incorrect in ui3
                    value = data['cta1Copy']
                elif 'name' in data:
                    # UI 1 and UI 2
                    value = data['name']
            else:
                # UI 4
                tag = product_tag.select_one('.accessory__brand')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            description = ''
            if _source:
                data, source = product_tag, _source
                
                if 'features' in data:
                    # UI 2
                    for feature in data['features']:
                        description += '{}:{}\n'.format(feature, data['features'][feature])

                    value = description

                elif 'mainCopy' in data:
                    # UI 3
                    desc_soup = BeautifulSoup(data['mainCopy'], 'lxml')
                    desc_tag = desc_soup.select_one('p')
                    if desc_tag:
                        source = str(desc_soup)
                        value = " ".join(desc_tag.stripped_strings)
            else:
                tag = product_tag.select_one('.accessory__description')
                if tag:
                    source = str(tag)
                    value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            if _source:
                data, source = product_tag, _source

                if 'rating' in data:
                    # UI 2
                    if 'na' not in data['rating']:
                        value = str(data['rating'])
                # UI 3 No ratings

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if _source:
                data, source = product_tag, _source

                if 'reviews' in data:
                    # UI 2
                    if data['reviews']:
                        value = str(data['reviews'])
                # UI 3 No ratings

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #No product availability indicated
            pass
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag, _source):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if _source:
                data, source = product_tag, _source
                
                if 'image' in data:
                    # UI 1 and UI 2
                    if 'src' in data['image']:
                        value = [data['image']['src']]
                elif 'primaryImg' in data:
                    key = list(data['primaryImg'].keys())[0]  # assuming 1 dict only
                    value = [ data['primaryImg'][key]['desktopImage_bloblink_'] ]
            else:
                # UI 4
                tag = product_tag.select_one('.accessory__image-container img')
                if tag:
                    source = str(tag)
                    img_url = None
                    if tag.has_attr('data-srcset'):
                        img_url = tag.get('data-srcset')
                    elif tag.has_attr('src'):
                        img_url = tag.get('src')

                    if img_url:
                        # value = f'https:{img_url}' if 'http' not in img_url else img_url
                        value.append(f'https:{img_url}') if 'http' not in img_url else img_url
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value