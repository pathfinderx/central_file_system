from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json
import cloudscraper

class TelekomSkDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)
        self.url = None
        self.scraper = cloudscraper.create_scraper()

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        self.url = url
        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None
        
        try:
            res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_api_data (self, postal_code=None, timeout=10, headers=None, cookies=None, data=None, **kwargs):
          
        if not isinstance(headers, dict):
            # default common headers
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'caller': 'oneshop-ui',
            'channel': 'OneShop',
            'content-language': 'sk',
            'content-length': '243',
            'content-type': 'application/json',
            'cookie': '_gcl_au=1.1.1170166057.1643698805; _anonymise=GA1.2.2138851551.1643698805; _anonymise_gid=GA1.2.52210763.1643698805; _lb=7059632618221898000; cd_user_id=17eb4156c5ca5-00185f15bee7ca-f791539-1fa400-17eb4156c5d228; purpose_cookie=1; receiver_cookie=; CART_SESSION=03cc5f39-9e47-4d1c-ab53-ec29bda4a610; ONESHOP_SESSION=ZGY5Mzg3NDYtYzNhMS00Zjc3LTgyNzAtNjNiZjM2MjJlNzdj; cmbShow=open',
            'country': 'sk',
            'language': 'sk',
            'origin': 'https://eshop.telekom.sk',
            'pragma': 'no-cache',
            # 'referer': 'https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806&bp=acquisition',
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'servicename': 'productOfferings',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
            'x-request-config-created-at': '1643686073059',
            'x-request-country': 'sk',
            'x-request-customer-segment': 'B2C',
            'x-request-status': 'AUTHORIZED',
            'x-request-tracking-id': 'a67a51a7-3cc5-4ff7-a936-176857cd6d22',
            'x-request-unique-identifier': 'db795af9-a224-4258-bac9-c3813c1b359c',

        }

        if kwargs['state'] == 'FIRSTPAGE':
            payload = {
                "channel":"OneShop",
                "page":0,
                "itemPerPage":25,
                "filterCharacteristics":[],
                "categories":[
                    {"id":"all-devices","characteristics":[
                        {"key":"eligibleMSISDN","value":""},
                        {"key":"preferredBusinessInteraction","value":"acquisition"}
                        ]
                    }
                ],
                "conditions":[]
            }    

        elif kwargs['state'] == 'SECONDPAGE' and \
            self.url in ['https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806&currentPage=2&tariffId=RP1140&itemPerPage=16']:

            payload = {
                "channel":"OneShop",
                "page":1,
                "itemPerPage":16,
                "filterCharacteristics":[],
                "categories":[{
                    "id":"all-devices",
                    "characteristics":[
                        {"key":"eligibleMSISDN","value":""},
                        {"key":"preferredBusinessInteraction","value":"acquisition"},
                        {"key":"selectedTariff","value":"RP1143"},
                        {"key":"selectedProductOfferingTerm","value":"agreement24"}
                    ]
                }],
                "conditions":[]
            }

        url = f'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing'
        
        try:
            res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(payload))

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, url