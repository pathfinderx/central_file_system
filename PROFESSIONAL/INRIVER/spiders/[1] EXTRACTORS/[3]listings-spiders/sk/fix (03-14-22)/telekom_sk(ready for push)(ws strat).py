import logging
from math import prod
import re
import json
from copy import deepcopy
from bs4 import BeautifulSoup, Tag, NavigableString, ResultSet
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class TelekomSkWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.telekom.sk'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_page = None
        self.base_url = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = None
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            raw_data = None

            while True:
                try:
                    if page == 0:
                        page += 1
                        #Redirect to page 2
                        next_url = url + '&currentPage=2&tariffId=RP1140&itemPerPage=16'

                    else:
                        #Page 2 wil scrap product form page 1 to page 2, 16 product per page
                        if self.next_page:
                            next_url = self.next_page
                        else:
                            break

                    print("On page: %d" % page)
                    print(next_url)
                    if self.next_page: # second page requests for api data (https://prnt.sc/dl7fVqnyxT0A); therefore, special api request is needed
                        raw_data, last_response = self.download_strategy.get_api_data(postal_code=postal_code, timeout=timeout, headers=headers, cookies=cookies, data=None, state='SECONDPAGE')
                    else:
                        raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code, timeout=timeout, headers=headers, cookies=cookies, data=None)
                        
                    if not raw_data:
                        break

                    products.append(self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results))


                    if not next_url:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def find_next_page (self, soup):
        tag = soup.select_one('#osAppInnerContainer > main > section > div > div > div.sc-pDboM.eLzJMA.pagination.showmorePagination')
        if tag:
            self.next_page = 'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing' # this url utilized POST method to get json data
        else:
            self.next_page = None
    
    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []
        # idx = 0
        
        self.find_next_page(soup)
        product_tags = self.product_data_checker (soup, raw_data)

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency()

            # Extract price per unit
            # result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
            #     result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
            #     result["price_per_unit"]['currency']["source"], \
            #     result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            # result["promo"]['description']["source"], result["promo"]['description']["value"], \
            #     result["promo"]['price']["value"], \
            #     result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["source"],
            #                                                           result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            #price uncertain
            value = '0'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'  # Default value
        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site

        try:
            pass
        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # this can still be utilized in the future
            # prod_id = json_list['variant']['productId'] # 
            # tag = product_tag.select_one('.primaryLink')

            # if tag and tag.has_attr('href'):
            #     source = str(tag)
            #     if '?variantId' in tag['href']:
            #         url = tag.attrs.get('href').split('?variantId')
            #         _url = url[0] + '/product/' + prod_id + '?variantId' + url[1]
            #         value = str('https://eshop.telekom.sk' + _url)
            #     else:
            #         url = tag['href'].split('?')
            #         _url = url[0] + '/product/' + prod_id + '?' + url[1]
            #         value = str('https://eshop.telekom.sk' + _url)

            if product_tag and isinstance (product_tag, dict):
                if 'variant' in product_tag and \
                    'productId' in product_tag['variant'] and \
                    'categoryId' in product_tag['variant']:
                    
                    url = f'https://eshop.telekom.sk/category/{product_tag["variant"]["categoryId"]}/list/product_listing/product/{product_tag["variant"]["productId"]}'
                    source, value = str(product_tag), url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if product_tag and isinstance(product_tag, dict):
                if 'variant' in product_tag and 'brand' in product_tag['variant']:
                  source, value = str(product_tag), product_tag['variant']['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if product_tag and isinstance(product_tag, Tag):
                tag = product_tag.select_one('.sc-fznLPX.gwVRlu.dt_title.productTitle')
                if tag:
                    source, value = str(tag), tag.get_text().strip()

            elif product_tag and isinstance(product_tag, dict):
                if 'variant' in product_tag and 'name' in product_tag['variant']:
                  source, value = str(product_tag), product_tag['variant']['name']
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value
        
    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if product_tag and isinstance(product_tag, dict):
                if 'variant' in product_tag and \
                    'unavailabilityReasonCodes' in product_tag['variant'] and \
                    len(product_tag['variant']['unavailabilityReasonCodes']) > 0:

                    source, value = str(product_tag), product_tag['variant']['unavailabilityReasonCodes'][0]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if product_tag and isinstance(product_tag, dict):
                if 'variant' in product_tag and \
                    'attachments' in product_tag['variant'] and \
                    'thumbnail' in product_tag['variant']['attachments']:

                    source = str(product_tag)
                    for x in product_tag['variant']['attachments']['thumbnail']:
                        value.append(x.get('url'))
                        break

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def convert_json_string (self, data): # convert to json if raw_data is already a json string
        try:
            json_data = json.loads(data)
            return json_data if json_data else None
        except Exception as e:
            print('Raw data is not a valid json')
            return None

    def _get_prod_list(self, raw_data):
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            scrp_tag = soup.find('script', text=re.compile('window\.\__INITIAL\_STATE\__'))
            if scrp_tag:
                _rgx = re.search(r'window\.\__INITIAL\_STATE\__.=.({.*})', scrp_tag.get_text(strip=True))
                if _rgx:
                    product_data = _rgx.group(1)
                    _json = json.loads(product_data)

                    return _json['productList']['data']

        except Exception as e:
            print(e)
            return None

    def product_data_checker (self, soup, raw_data):
        try:
            product_tags = []
            if raw_data:
                json_data = self.convert_json_string (raw_data)
                if json_data and 'data' in json_data:
                    product_tags = json_data.get('data')
                else:
                    _json = self._get_prod_list(raw_data)

                    if _json:
                        product_tags = _json
                    elif not product_tags:
                        api_response =  self.download_strategy.get_api_data (state='FIRSTPAGE')

                        if api_response and api_response[0] and api_response[1]:
                            _json = json.loads(api_response[0])
                            self.base_url = api_response[1]

                            if _json and 'data' in _json:
                                product_tags = _json.get('data')

            # if soup and raw_data:
            #     product_tags = soup.select('.productListContainer [data-cy="product-column"]')

            #     if product_tags:
            #         return product_tags

            #     elif not product_tags:
            #         api_response =  self.download_strategy.get_api_data ()

            #         if api_response and api_response[0] and api_response[1]:
            #             _json = json.loads(api_response[0])
            #             self.base_url = api_response[1]

            #             if _json and 'data' in _json:
            #                 return _json.get('data')
            #     else:
            #         _json = self._get_prod_list(raw_data)
            #         if _json:
            #             return _json
            return product_tags
        except Exception as e:
            self.logger.exception(e)
            return None
