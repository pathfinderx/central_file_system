from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json
from bs4 import BeautifulSoup

class TelekomSkDownloadStrategy(DownloadStrategy):

    def download(self, url, page, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        
        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None
        
        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                # raw_data = res.text

                # for history in res.history:
                #     if history.status_code in [302, 301]:
                #         raw_data = None
                #         break

                # not future proof fix! 
                # resorted to api because normal requester provides obfuscated and incomplete data causing it have -1 values though actual data count is correct
                raw_data = self.get_api_data(url, res.text, page, timeout=None, headers=None, cookies=None) 

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_api_data (self, url, res_data, page, timeout=None, headers=None, cookies=None):
          
        if not isinstance(headers, dict):
            # default common headers
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'caller': 'oneshop-ui',
            'channel': 'OneShop',
            'content-language': 'sk',
            'content-length': '243',
            'content-type': 'application/json',
            'cookie': '_gcl_au=1.1.1170166057.1643698805; _anonymise=GA1.2.2138851551.1643698805; _anonymise_gid=GA1.2.52210763.1643698805; _lb=7059632618221898000; cd_user_id=17eb4156c5ca5-00185f15bee7ca-f791539-1fa400-17eb4156c5d228; purpose_cookie=1; receiver_cookie=; CART_SESSION=03cc5f39-9e47-4d1c-ab53-ec29bda4a610; ONESHOP_SESSION=ZGY5Mzg3NDYtYzNhMS00Zjc3LTgyNzAtNjNiZjM2MjJlNzdj; cmbShow=open',
            'country': 'sk',
            'language': 'sk',
            'origin': 'https://eshop.telekom.sk',
            'pragma': 'no-cache',
            # 'referer': 'https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806&bp=acquisition',
            'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'servicename': 'productOfferings',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
            'x-request-config-created-at': '1643686073059',
            'x-request-country': 'sk',
            'x-request-customer-segment': 'B2C',
            'x-request-status': 'AUTHORIZED',
            'x-request-tracking-id': 'a67a51a7-3cc5-4ff7-a936-176857cd6d22',
            'x-request-unique-identifier': 'db795af9-a224-4258-bac9-c3813c1b359c',

        }

        
        # if self.hasNextPage(res_data):
        #     loopLength = 2
        # else:
        #     loopLength = 1

        # if url and kwargs['hasNextPage']:
        #     urls = [
        #         f'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing',
        #         f'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing'
        #     ]
           
        # else:
        #     urls = f'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing'
    
        try:
            payload = self.getPayload(url, page)
            url = f'https://eshop.telekom.sk/api/eshop/bff-sk/productOfferings/listing'

            res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(payload))

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data

    def getPayload (self, url, page):
        payload = dict()
        page = page - 1

        if url in ['https://eshop.telekom.sk/category/all-devices/list/product_listing/product/iPhone-13?variantId=RP1140&agreementId=agreement24&bp=acquisition&variantId=HW3209&filter%5B%5D=filter.device_category%5B%5D%3D5g&deviceCategoryName=']:
            payload = {
                "channel":"OneShop",
                "page": page,
                "itemPerPage":32,
                "filterCharacteristics":[{"id":"device_category",
                "characteristicValues":["5g"]}],
                "categories":[{"id":"all-devices",
                "characteristics":[
                    {"key":"eligibleMSISDN","value":""},
                    {"key":"preferredBusinessInteraction","value":"acquisition"},
                    {"key":"selectedTariff","value":"RP1140"},
                    {"key":"selectedProductOfferingTerm","value":"agreement24"}]}],
                "conditions":[]}

        elif url in ['https://eshop.telekom.sk/category/all-devices/list/product_listing?_ga=2.143374837.1517353058.1614774580-1983963565.1595229806']:
            payload = {
                "channel":"OneShop",
                "page":page,
                "itemPerPage":16,
                "filterCharacteristics":[],
                "categories":[{"id":"all-devices",
                "characteristics":[
                    {"key":"eligibleMSISDN","value":""},
                    {"key":"preferredBusinessInteraction","value":"acquisition"},
                    {"key":"selectedTariff","value":"RP1143"},
                    {"key":"selectedProductOfferingTerm","value":"agreement24"}]}],
                "conditions":[]}

        return payload if payload else None