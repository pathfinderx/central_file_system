from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import cloudscraper
from random import randint
from request.unblocker import UnblockerSessionRequests
class FrFnacBeDownloadStrategy(DownloadStrategy):
    def __init__ (self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
        self.unblocker = UnblockerSessionRequests('fr')

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'pragma': 'no-cache',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        url = self.check_url(url)
        
        try:
            res = self.unblocker.get(url, timeout=randint(60,120), headers=None, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text
                
                if 'Voir tout' in res.text: # PDP CHECKER
                    raise DownloadFailureException()

                # for history in res.history:
                #     if history.status_code in [302, 301]:
                #         raw_data = None
                #         break

            elif res.status_code in [400,403]:

                cookies = self.get_cookies(url)
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

                if res.status_code == 200:
                    raw_data = res.text

                    if 'Voir tout' in res.text: # PDP CHECKER
                        raise DownloadFailureException()

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def check_url (self, url):
        if url == 'https://www.fr.fnac.be/n470487/Switch/Consoles-Switch':
            return f'https://www.fr.fnac.be/n470487/Nintendo-Switch/Consoles-Switch'
        else:
            return url

    def get_cookies (self, url):
        res = self.scraper.get(url, proxies = self.requester.session.proxies)
        cookies = res.cookies.get_dict()
        if cookies:
            cookies['pv'] = 'list'
        return cookies