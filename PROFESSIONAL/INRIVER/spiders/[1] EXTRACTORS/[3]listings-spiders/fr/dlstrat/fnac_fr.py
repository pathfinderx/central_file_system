import cloudscraper
import requests
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests

class FnacComDownloadStrategy(DownloadStrategy):
    
    def __init__(self, requester):
        super().__init__(requester)
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('fr')

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        session = requests.session()
        self.scraper = cloudscraper.create_scraper(sess=session)

        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.unblocker.get(url, timeout=timeout, headers=headers, cookies=cookies)
            print(url, res.status_code)

            if res.status_code not in [200, 201]:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                print('Re-trigger requests using Luminati', res.status_code)

            # if res.status_code == 403:
            #     res = requests.post(url, headers=headers, timeout=10)
            #     # res = self.scraper.get(url)
                
            #     if res.status_code in [200,201]:
            #         raw_data = res.text

            #     elif res.status_code in [403]:
            #         # res = requests.get(url, headers=headers, timeout=10)
            #         res = self.scraper.post(url)

            print(url)
            # Download successful
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res