from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class OlssonsfiskeSeDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-GB,en;q=0.9,en-US;q=0.8',
                'cache-control': 'max-age=0',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def download_api (self, url, payload):
        headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'content-length': '220',
            'content-type': 'application/json',
            'origin': 'https://www.olssonsfiske.se',
            'pragma': 'no-cache',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'cross-site',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
        }

        raw_data = None

        try:
            res = self.requester.post(url, timeout=10000, headers=headers, data=json.dumps(payload))

            # Download successful
            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data