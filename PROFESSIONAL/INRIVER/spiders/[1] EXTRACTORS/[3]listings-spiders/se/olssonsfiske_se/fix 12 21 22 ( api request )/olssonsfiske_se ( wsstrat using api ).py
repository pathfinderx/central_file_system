import logging
import re, json, requests

from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class OlssonsfiskeSeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.olssonsfiske.se'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_url = None
    

    def execute(self, url, page=1, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            next_url = ''

            # used as sample url : https://www.olssonsfiske.se/fiskeredskap/forvaring
            while True:
                try:               
                    if page == 0:
                        page += 1
                        next_url = url
                    else:
                        next_url = url + "?page=" + str(page)
                    
                    print("On page: %d" % page)
                    print(next_url)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    products.append(self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results))


                    if not next_url:
                        break


                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        if self.no_next_page(raw_data):
                            break
                        else:
                            pass

                        page += 1
                        continue
                        
                    else:
                        break

                    

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []

        # # check if has next page
        # next_page_url = soup.select_one('.is-nextLink')
       
        # if next_page_url and next_page_url.has_attr('href'):
        #     self.next_url = next_page_url['href']

        product_tags = soup.select('div.product-card-inner')
        product_tags = self.get_products(soup)

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency()

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1
            
            # Append to results list
            # print(result)
            results.append(result)
        
        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('.product-card-detail .new-price')
        
            if tag:
                source, value = str(tag), tag.get_text(strip=True).replace("\xa0", " ")

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'SEK'  # Default value
        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'SEK'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'SEK'  # Default value for this site
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            new_price_tag = product_tag.select_one('.product-card-detail .new-price')
            old_price_tag = product_tag.select_one('.product-card-detail .old-price')
           

            if new_price_tag and old_price_tag:
                data_source = str(new_price_tag) + ":" + str(old_price_tag)
                promo_price_value = new_price_tag.get_text(strip=True)
                old_price_value = old_price_tag.get_text(strip=True)

                if len(promo_price_value) > 0:
                    source, description_value = data_source, 'From {} to {}'.format(old_price_value, promo_price_value)
            
        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('a')
            
            if tag and tag.has_attr('href'):
                source, value = str(tag), 'https://{}{}'.format(self.__class__.WEBSITE, tag['href'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('.product-card-detail h3')

            if tag:
                temp = tag.decompose()
                tag = product_tag.select_one('h3')
                source, value = str(tag), tag.get_text(strip=True)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('.product-card-detail')

            if tag:
                for _script in tag.select('script'):
                    _script.decompose()
                for _style in tag.select('style'):
                    _style.decompose()

                new_tag = tag.select_one('div > p')
                source, value = str(tag), new_tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = product_tag.select('picture img')
            
            if len(tags):
                source = str(tags) 
                for i in tags:
                    if i.has_attr('src'):
                        img = i['src']
                        if img.startswith('http'):
                            value.append(img)
                        else:
                            value.append("https:" + img)
                    elif i.has_attr('data-src'):
                        img = i['data-src']
                        if img.startswith('http'):
                            value.append(img)
                        else:
                            value.append("https:" + img)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    @staticmethod
    def no_next_page(text):
        bs = BeautifulSoup(text, 'lxml')
        tag = bs.find("a", text=re.compile('Nästa'))
        
        if tag and tag.has_attr('data-disabled'):
            return True
        elif tag and tag.has_attr('href'):
            return False
        else:
            return True

    def get_products(self, soup):
        try:
            api_payload, res = None, None
            category_url = self.get_category_url(soup) # extract category_url

            # set api payload
            if (category_url):
                api_payload = self.set_requester_prerequisite(category_url)

            # get api data using api payload
            if (api_payload and len(api_payload) > 0):
                res = self.download_strategy.download_api(api_payload[0], api_payload[1])

            # extract products
            if (res):
                _json = json.loads(res)
                return _json.get('items') if (_json and 'items' in _json) else None

            return None # default return statement if no products extracted from api request

        except Exception as e:
            print('Error at: wsstrat -> get_products() -> with message: ', e)

    def get_category_url (self, soup):
        try:
            raw_category_url, final_category_url = None, None
            product_url_tag = soup.select_one('meta[property="og:url"]')

            # extract category url
            if (product_url_tag and 
                product_url_tag.has_attr('content')): 
                
                raw_category_url = product_url_tag.get('content').split('olssonsfiske.se/')

            # initialize final_category_url
            if (raw_category_url and len(raw_category_url) > 0): 
                final_category_url = raw_category_url[-1]

            return final_category_url if final_category_url else None

        except Exception as e:
            print('Error at: wsstrat -> get_category_url() -> with message: ', e)
        
    def set_requester_prerequisite(self, final_category_url):
        try:
            api_key = '5324e64f-e26c-4395-8e55-179ed012aaba'
            request_url = f"https://api.findify.io/v4/{api_key}/smart-collection/{final_category_url}"
        
            payload = {
                "user":{
                    "uid":"WvCQXq6G87qwREr1",
                    "sid":"yzdnz4XLRm3pmWuJ",
                    "persist":True,
                    "exist":True
                },
                "t_client":1671581455020,
                "key":api_key,
                "limit":25,
                "offset":0,
                "slot":final_category_url
            }

            return request_url, payload

        except Exception as e:
            print('Error at: wsstrat -> set_requester_prerequisite() -> with message: ', e)