from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import cloudscraper
from request.unblocker import UnblockerSessionRequests
from bs4 import BeautifulSoup
import json

class ElgigantenSeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)
        self.scraper = cloudscraper.create_scraper()
        self.unblocker = UnblockerSessionRequests('se')

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None
        self.res_headers = None
        cookies = self.get_cookies(url)
        try:
            # res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=None)
            res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)
            print(url)
            # Download successful
            if res.status_code == 200:
                raw_data = res.text
                self.res_headers = res.headers
        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_cookies (self, url):
        res = self.scraper.get(url, proxies = self.requester.session.proxies)
        cookies = res.cookies.get_dict()
        if cookies:
            cookies['pv'] = 'list'
        return cookies

    def get_api_request (self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml')

        script_list = soup.select_one('script[type="application/ld+json"]')
        if script_list:
            try:
                _json_product_taxonomy = json.loads(script_list.get_text())
                if (_json_product_taxonomy and 
                    'itemListElement' in _json_product_taxonomy):

                    [x.get('name') for x in _json_product_taxonomy.get('itemListElement')]

            except Exception as e:
                print('Error at: dlstrat -> with message: ', e)

        pass


    # page: 
    # 1
    # filter: 
    # ArticleAssignments.AssignmentSystem.Product_Taxonomy:Datorer & Kontor/Datortillbehör/Mus & Tangentbord/Datormus
    # sid: 
    # 3fueEZ6SbvAeK1VsOVzeot4xLgezSC
    # log: 
    # cms
    # format: 
    # json

    # page: 
    # 1
    # filter: 
    # ArticleAssignments.AssignmentSystem.Product_Taxonomy:Sport & Fritid/Sport & Fitness/Aktivitetsarmband
    # sid: 
    # 3fueEZ6SbvAeK1VsOVzeot4xLgezSC
    # log: 
    # cms
    # format: 
    # json