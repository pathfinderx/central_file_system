import logging, urllib, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class DreamlandBeENlDlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.dreamland.be/e/nl/dl'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)

        # NOTE: in this context, either _method/variable or __method/variable are considered private
        # its just a matter of programming style since im used to
        # _ protected method/variable
        # __ private method/variable

        # NOTE: we use one variable to store the instance state
        # the reason for this one is just a matter of programming style since
        # prior implementations does not really utilize instance state and I
        # dont want also to clutter the object w many instance variables vs prior implementations
        self.__state = {}

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            self.__state = {}

            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # initialize some values on the instance state
            self.__state.update({
                'url': url, 'page': page, 'start_rank': start_rank,
                'postal_code': postal_code, 'max_results': max_results, 'request_params': request_params,
                'headers': None, 'timeout': 10, 'cookies': None
            })

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    self.__state['timeout'] = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    self.__state['headers'] = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    self.__state['cookies'] = request_params['cookies']

            # current_product_count - number of product per iteration/page total_product_count - total number of
            # products so far TOTAL_PRODUCT_COUNT - total number of products found in a certain category. Some
            # websites provides this and some not. This is optional
            self.__state.update({
                'last_response': None, 'is_error': False, 'products': [],
                'current_product_count': 0, 'total_product_count': 0, 'TOTAL_PRODUCT_COUNT': 0,
            })

            next_page_params = {}
            if self.__state['page'] < 1: self.__state['page'] = 1  # starts with 1
            while True:
                try:

                    print("On page: %d" % self.__state['page'])

                    if self.__state['page'] == 1:
                        page_url = url
                    else:
                        page_url = next_page_params

                    raw_data, self.__state['last_response'] = self.download_strategy.download(page_url)

                    if not raw_data: break

                    self.__state['bs'] = BeautifulSoup(raw_data, 'lxml')

                    products = self.get_products(self.__state['bs'])

                    # truncate the products according to the max_results
                    products = self.truncate_products(products)
                    products = self._extract(products, page=self.__state['page'], start_rank=self.__state['start_rank'])

                    self.__state['products'].append(products)
                    self.__state['current_product_count'] = len(products)
                    self.__state['total_product_count'] += self.__state['current_product_count']

                    if not self.has_next():
                        break

                    next_page_params = self._get_next_page_params(page_url)

                    self.__state['page'] += 1
                    self.__state['start_rank'] += self.__state['current_product_count']
                except DownloadFailureException:
                    self.__state['is_error'] = True
                    break
                except Exception:
                    raise

            return dict(
                is_error=self.__state['is_error'],
                last_page=self.__state['page'],
                last_response=self.__state['last_response'],
                products=self.__state['products']
            )

        except Exception:
            raise

    def get_products(self, tag):
        products = tag.select('div[class=product_listing_container] ul li')
        
        return products

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div[class=product_listing_container] ul li div[class*="mini"]')
            
            source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'
        return source, value

    def get_price_per_unit(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default currency

        # Website has no price per unit on product card
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, soup, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default currency

        try:
            tag = soup.select_one('div[class*="product_price"] span[class*="price old"] span[class="value"]')

            if tag and tag.text.strip():
                description_value = ' '.join(tag.stripped_strings)
                promo_price_value = price_value
                source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            url_tag = soup.select('div.product_info > a')
            tag = soup.select_one('div[class=product_listing_container] ul li div[class*="product_info"] a:not([data-tms-cartchange])')

            if url_tag:
                for x in url_tag:
                    if (x.has_attr('href') and re.search(r'https://www.dreamland.be', x.get('href'))):
                        value = (x.get('href'))
                source = str(url_tag) if value else Defaults.GENERIC_NOT_FOUND.value

            elif soup:
                source, value = str(tag), tag['href'].strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value


    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Website has no brand on product card
        return source, value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div[class=product_listing_container] ul li div[class*="product"]')
            if soup:
                source, value = str(tag), tag['base-product-name'].strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value


    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Website has no description on product card
        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = '5'  # default max stars

        # Rating score is fetch via API
        return source, value, max_value

    def get_rating_reviews(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Rating score is fetch via API
        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = data.select_one('ul li div[class*="product_info"] div[class*="NFAVAILABILITYFLAG"]')
            
            if tag:
                source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value


    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tags = soup.select('ul li div[class*="product_info"] img')
            
            # Unable to retrieve `src` attribute,might be from API or JS
            if tags and len(tags):
                source = str(tags)
                value = [i.get('data-dojo-props') for i in tags if i.get('data-dojo-props')]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value


    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################

    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def truncate_products(self, products):
        if self.__state['max_results'] is None:
            return products

        remaining_length = self.__state['max_results'] - self.__state['total_product_count']
        # truncate products by slicing
        return products[0:remaining_length]

    def has_next(self):
        if self.__state['max_results'] is not None and\
                self.__state['total_product_count'] >= self.__state['max_results']:
            return False

        if self.__state['current_product_count'] <= 0:
            return False

        pagination = self.__state['bs'].find('div', {'class':'paging_controls'})
        if pagination:
            is_next_arrow = pagination.find('a', {'class': 'right_arrow invisible'})
            if is_next_arrow:
                return False

        return True

    def _get_next_page_params(self, url):
        if url:
            pattern = re.compile(r'BeginIndex:([0-9]+)')
            index = pattern.search(url)
            new_index = int(index.group(1)) + 48
            url = re.sub(pattern, 'BeginIndex:' + str(new_index), url)
        return url

    def _extract(self, products, page, start_rank):

        template = get_result_base_template()
        results = []

        for product in products:
            result = deepcopy(template)

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(self.__state['bs'])

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
            result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
            result["price_per_unit"]['currency']["source"], \
            result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(product)

            # # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
            result["promo"]['price']["value"], \
            result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"] = self.get_rating_score(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(
                product)

            # # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results
