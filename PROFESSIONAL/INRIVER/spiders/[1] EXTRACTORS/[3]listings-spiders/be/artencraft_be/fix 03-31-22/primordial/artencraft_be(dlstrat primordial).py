import time
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class ArtencraftBeDownloadStrategy(DownloadStrategy):

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):

        if not isinstance(headers, dict):
            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9",
                "Sec-Fetch-Dest": "document",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-Site": "same-origin",
                "Sec-Fetch-User": "?1",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.56",
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(
                url, timeout=timeout, headers=headers, cookies=cookies)
            print('Status code:', res.status_code)
            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException(
                'Download failed - Unhandled exception')

        # Added a 5 second sleep timer to not spam the website.
        # Website blocks your IP if you cannot prove if you're a robot.
        time.sleep(5)
        return raw_data, res

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################

    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################
