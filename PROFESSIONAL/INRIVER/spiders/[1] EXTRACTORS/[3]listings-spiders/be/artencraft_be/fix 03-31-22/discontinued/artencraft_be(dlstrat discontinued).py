import time
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class ArtencraftBeDownloadStrategy(DownloadStrategy):

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):

        if not isinstance(headers, dict):
            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9",
                "Sec-Fetch-Dest": "document",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-Site": "same-origin",
                "Sec-Fetch-User": "?1",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.56",
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            print('Status code:', res.status_code)
            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException(
                'Download failed - Unhandled exception')

        # Added a 5 second sleep timer to not spam the website.
        # Website blocks your IP if you cannot prove if you're a robot.
        time.sleep(5)
        return raw_data, res

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################

    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def extra_download (self, url, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Accept': 'text/html, */*; q=0.01',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'same-origin',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
                'cookies':'ac_PHPSESSID=683bcc62aad4ce25be0bd0a91e9966a6; anc_analytics_id=leYVgKctXq78El0B2ML5nU2WAOg8B01y; BVBRANDID=e0537813-6efa-4f7f-9872-efe4e615acf6; _gid=GA1.2.500242636.1648691291; aodpuid=A825894235-1648691291; ac_l=nl; _hjSessionUser_2190032=eyJpZCI6ImVjZjMwOGVkLTdjYjYtNTRlZi04N2NmLWRiZjgzN2E4YzRhNyIsImNyZWF0ZWQiOjE2NDg2OTEyOTExNDcsImV4aXN0aW5nIjp0cnVlfQ==; _hjAbsoluteSessionInProgress=0; BVBRANDSID=c24c8133-e52e-467b-89f0-465aac562b5a; _hjIncludedInSessionSample=1; _hjSession_2190032=eyJpZCI6IjU5MDUxYzM0LTIxODQtNDZlOC1iZTkxLTA0ODlmMDI5NjdjZCIsImNyZWF0ZWQiOjE2NDg2OTg3NTY5NzMsImluU2FtcGxlIjp0cnVlfQ==; _hjIncludedInPageviewSample=1; _ga=GA1.1.531746688.1648691291; aodpdata=A825894235-1648691291-1695354764; aodp-_ga=GA1.1.531746688.1648691291-aodpsecure-1695354764; _ga_01FGMFQWD9=GS1.1.1648698754.2.1.1648698871.0; aodpsid=227613054-1648700671459'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            # res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)
            print('Status code:', res.status_code)
            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException(
                'Download failed - Unhandled exception')

        # Added a 5 second sleep timer to not spam the website.
        # Website blocks your IP if you cannot prove if you're a robot.
        time.sleep(5)
        return raw_data, res