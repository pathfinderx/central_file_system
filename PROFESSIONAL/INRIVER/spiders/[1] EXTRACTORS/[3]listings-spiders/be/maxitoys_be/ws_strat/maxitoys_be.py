import logging
import re
import json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class MaxitoysBeWebsiteStrategy(WebsiteStrategy):
	WEBSITE = 'www.maxitoys.be'

	def __init__(self, download_strategy):
		super().__init__(download_strategy=download_strategy)
		self.logger = logging.getLogger(__name__)
		self.next_page = None
		self.url = None

	def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
		try:
			assert isinstance(url, str)
			assert isinstance(page, int)
			assert isinstance(start_rank, int)

			if max_results:
				assert isinstance(max_results, int)

			# Init request parameters
			timeout = 10
			headers = {
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
				'x-api-source': 'pc',
				'x-requested-with': 'XMLHttpRequest',
				'x-shopee-language': 'en'
			}
			cookies = None

			if request_params and isinstance(request_params, dict):
				if 'timeout' in request_params and isinstance(request_params['timeout'], int):
					timeout = request_params['timeout']

				if 'headers' in request_params and isinstance(request_params['headers'], dict):
					headers = request_params['headers']

				if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
					cookies = request_params['cookies']

			last_response = None
			is_error = False
			products = list()

			while True: # products has values and total_products not reached max resuts max_results
				try:
					if page == 0: # initial start
						page += 1
						next_url = url
						self.url = url # constructor variable as global variable to be access for set_next_page

					else: # next page
						if self.next_page:
							next_url = self.next_page
						else: 
							break

					# new param in download(page)
					raw_data, last_response = self.download_strategy.download(next_url, page, postal_code=postal_code,timeout=timeout, headers=headers,cookies=cookies, data=None)

					if not raw_data:
						break

					product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)

					if product:
						products.append(product)
					else:
						break

					if products:
						total_products = sum([len(page_products) for page_products in products])

						if max_results and total_products >= max_results: # limiter and breaker
							break

						start_rank = total_products + 1

						page += 1
						continue

					else:
						break

				except DownloadFailureException:
					is_error = True
					break

				except Exception:
					raise

			return dict(
				is_error=is_error,
				last_page=page,
				last_response=last_response,
				products=products
			)

		except Exception:
			raise
	
	# def set_next_page (self, soup, page):
	# 	self.next_page = self.url

	def _extract(self, raw_data, page, start_rank, max_results):
		soup = BeautifulSoup(raw_data, 'lxml')
		template = get_result_base_template()
		results = []

		# self.set_next_page(soup, page)
		
		product_tags = soup.find_all('div', class_="article__tile relative font-raleway min-h-104 hover:shadow-lg lg:min-h-108 border")

		if not product_tags:
			return results

		for product_tag in product_tags: 
			result = deepcopy(template)

			if max_results and max_results < start_rank :
				continue

			result["page_number"] = str(page)
			result["rank"] = str(start_rank)

			# Extract price
			result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

			# Extract currency
			result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

			# Extract price per unit
			result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
				result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
				result["price_per_unit"]['currency']["source"], \
				result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

			# Extract promo
			result["promo"]["source"], result["promo"]['description']["value"], \
				result["promo"]['price']["value"], \
				result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

			# Extract URL
			result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

			# Extract brand
			result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

			# Extract title
			result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

			# Extract description
			result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

			# Extract rating - score
			result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
				result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

			# Extract rating - reviews
			result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

			# Extract availability
			result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

			# Extract image URLS
			result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

			start_rank += 1

			# Append to results list
			results.append(result)

		return results

	def get_price(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag:
				tag = product_tag.find('span', class_="font-bold mt-2 text-xl")
				if tag:
					source, value = str(product_tag), tag.get_text().strip()
					
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value

		return source, value

	def get_currency(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = 'PHP'  # Default value | Currency not Found
		return source, value

	def get_price_per_unit(self, currency):
		source = Defaults.GENERIC_NOT_FOUND.value
		unit_value = Defaults.GENERIC_NOT_FOUND.value
		price_value = Defaults.GENERIC_NOT_FOUND.value
		currency_value = currency 
		return source, unit_value, source, price_value, source, currency_value

	def get_promo(self, product_tag, price_value, currency):
		source = Defaults.GENERIC_NOT_FOUND.value
		description_value = Defaults.GENERIC_NOT_FOUND.value
		promo_price_value = Defaults.GENERIC_NOT_FOUND.value
		currency_value = currency

		try:
			old_price = ""
			discount = ""
			description = ""
			
			if product_tag:
				oldPriceTag = product_tag.find('span', class_='price__old text-mt-grey text-sm font-medium ml-1 line-through')
				if oldPriceTag and oldPriceTag.get_text():
					old_price = oldPriceTag.get_text().strip()

				discountTag = product_tag.find('span', class_='price__discount text-white bg-mt-red p-1 text-xs font-bold rounded')
				if discountTag:
					discount = discountTag.get_text().strip()
											
				if old_price and discount:
					description = f"From {old_price} {discount}"
				elif old_price and not discount:
					description = f"From {old_price}"

				if description:
					source = str(product_tag)
					description_value = description
					promo_price_value = old_price

		except Exception as e:
			self.logger.exception(e)
			description_value = FailureMessages.PROMO_EXTRACTION.value

		return source, description_value, promo_price_value, currency_value

	def get_url(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag:
				tag = product_tag.find('a')
				if tag and tag.get('href'): 
					temp = tag.get('href').replace('../', "")
					url = f'https://www.maxitoys.be/{temp}'
					source, value = str(product_tag), url

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.URL_EXTRACTION.value

		return source, value

	def get_brand(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag:
				tag = product_tag.find('a')
				if tag:
					title = tag.find('span', class_="text-sm font-bold block")
					source, value = str(product_tag), title.get_text()

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.BRAND_EXTRACTION.value

		return source, value # no brand found in product display

	def get_title(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag:
				tag = product_tag.find('span', class_="descente__article__libelle")
				if tag:
					source, value = str(product_tag), tag.get_text()

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_description(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			pass # no description in product display

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.DESCRIPTION_EXTRACTION.value

		return source, value

	def get_ratings(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value
		max_value = "5"  # For this site, max rating is 5 stars

		try:
			pass # no ratings in product display

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.RATING_EXTRACTION.value

		return source, value, max_value

	def get_reviews(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			pass # no reviews in product display

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.REVIEWS_EXTRACTION.value

		return source, value

	def get_availability(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			pass # no product availability found in product display

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.AVAILABILITY_EXTRACTION.value

		return source, value

	def get_image_urls(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = []  # image_urls data type must be list

		try:
			tag = product_tag.find('img')
			if tag: # add hasattr, checker
				source =str(product_tag),
				value.append(tag.get('src'))
			
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.IMAGE_URLS_EXTRACTION.value

		return source, value