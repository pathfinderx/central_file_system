import random
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class MaxitoysBeDownloadStrategy(DownloadStrategy):
	def download(self, url, page, postal_code=None, timeout=randint(60, 100), headers=None, cookies=None, data=None):
		# if url:
		#     temp = url.split('.htm')
		#     url = temp[-2]

		if not isinstance(headers, dict):
			headers = { 
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
				'x-api-source': 'pc',
				'x-requested-with': 'XMLHttpRequest',
				'x-shopee-language': 'en'
			}

		if not isinstance(cookies, dict):
			cookies = None

		raw_data = None


		try:
			res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

			if res.status_code == 200:
				raw_data = res.text

		except Exception:
			raise DownloadFailureException('Download failed - Unhandled exception')

		return raw_data, res