import re, json
import logging
from random import randint
from urllib.parse import urlencode
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class MagazineluizaComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.magazineluiza.com.br'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_url = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = randint(60, 100)
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            next_url = None

            # sample url: https://www.magazineluiza.com.br/lg-k61/celulares-e-smartphones/s/te/lk61
            # Pagination logic
    
            while True:
                try:
                    if page == 0:
                        page += 1
                        next_url = self.__get_next_url(url, page)
                        
                    print(next_url)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    products.append(self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results))


                    next_url = self.__get_next_url(url, page + 1)
                    if not self.__has_next_page(raw_data):
                        break

                    if products:
                        # print(products)
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def __get_next_url(self, url, page):
        
        if url.endswith('/'):
            url = url[:-1]

        params = {
            'page': page
        }

        url = '{}?{}'.format(url, urlencode(params))

        return url

    def __has_next_page(self, raw_data):
        soup = BeautifulSoup(raw_data, 'lxml') 
        product_tags = soup.select('[name="linkToProduct"] [type="application/ld+json"]')

        if len(product_tags) >= 24:
            return True
        else:
            if len(soup.select('ul[data-css-iabqk5] [name="linkToProduct"]')) >= 23:
                return True

        return False

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []

        product_tags = self.get_product_tags(soup)

        if not product_tags:
            return results

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)
        
        return results

    def get_product_tags(self, soup):
        product_tags = soup.select('[name="linkToProduct"] [type="application/ld+json"]') or soup.select('ul[data-css-iabqk5] [name="linkToProduct"]')
        if product_tags:
            return product_tags
        else:
            script_list = soup.select('script[data-testid="jsonld-script"]')
            if script_list:
                product_data, product_list = None, list()
                for index, value in enumerate(script_list):
                    product_list_indicator = value.next_element.find('graph') # temporary indicator, this product list indicator could change anytime
                    product_data = (script_list[index]) if product_list_indicator not in [-1] else None 
                if product_data:
                    try:
                        _json = json.loads(product_data.text)
                        if _json and '@graph' in _json:
                            product_list = [x for x in _json.get('@graph')] # returns list of products in a form of dictionary
                    except Exception as e:
                        print('at: get_product_tags(), json parse error')
                        return None
                return product_list if product_list else None 

        # TRY THIS SELECTOR (if neither of the extrators above work)
        # product_list = soup.select_one('div[data-testid="mod-productlist"]').select('ul[data-testid="list"] > li') 
            
    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if 'offers' in product_tag and 'price' in product_tag['offers']:
                    source, value = str(product_tag), product_tag['offers']['price']
            else:
                _json = None
                source = str(product_tag)
                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)
                if _json:     
                    if 'offers' in _json:
                        if 'lowPrice' in _json['offers']:
                            value = str(_json['offers']['lowPrice'])
                else:
                    tag = product_tag.select_one('[data-css-lz0zr]')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'  # Default value
        
        try:
            if isinstance(product_tag, dict):
                if 'offers' in product_tag and 'priceCurrency' in product_tag['offers']:
                    source, value = str(product_tag), str(product_tag['offers']['priceCurrency'])
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)

                if _json:
                    if 'offers' in _json:
                        if 'priceCurrency' in _json['offers']:
                            value = str(_json['offers']['priceCurrency'])
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'BRL'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'BRL'  # Default value for this site
        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if 'offers' in product_tag and 'url' in product_tag['offers']:
                    source, value = str(product_tag), product_tag['offers']['url']
            else:
                tag = product_tag.parent

                if tag and tag.has_attr('href'):
                    source, value = str(tag), tag['href']   
                else:
                    source, value = str(product_tag), product_tag['href']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if 'brand' in product_tag:
                    source, value = str(product_tag), product_tag.get('brand')
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)
                if _json:
                    if 'brand' in _json:
                        if 'name' in _json['brand']:
                            value = str(_json['brand']['name'])
                
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if 'name' in product_tag:
                    source, value = str(product_tag), product_tag.get('name')
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)

                if _json:
                    if 'name' in _json:
                        value = str(_json['name'])
                else:
                    tag = product_tag.select_one('div h3')
                    if tag:
                        source = str(tag)
                        value = tag.get('title').strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        if isinstance(product_tag, dict):
            if 'description' in product_tag:
                source, value = str(product_tag), product_tag.get('description')
            
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            if isinstance(product_tag, dict):
                if 'aggregateRating' in product_tag and 'ratingValue' in product_tag['aggregateRating']:
                    source, value = str(product_tag), product_tag['aggregateRating']['ratingValue']
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)
                if _json:
                    if 'aggregateRating' in _json:
                        if 'ratingValue' in _json['aggregateRating']:
                            value = str(_json['aggregateRating']['ratingValue'])
                else:
                    tag = product_tag.select_one('[data-css-1b8i6hi]')
                    if tag and tag.has_attr('style'):
                        source = str(tag)
                        value = tag.get('style')
                        max_value = '100'

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if 'aggregateRating' in product_tag and 'ratingCount' in product_tag['aggregateRating']:
                    source, value = str(product_tag), product_tag['aggregateRating']['ratingCount']
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)

                if _json:
                    if 'aggregateRating' in _json:
                        if 'ratingCount' in _json['aggregateRating']:
                            value = str(_json['aggregateRating']['ratingCount'])
                else:
                    tag = product_tag.select_one('[data-css-1elmqex]')
                    if tag:
                        source = str(tag)
                        value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        if isinstance(product_tag, dict):
            if 'offers' in product_tag and 'availability' in product_tag['offers']:
                source, value = str(product_tag), product_tag['offers']['availability']

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if isinstance(product_tag, dict):
                if 'image' in product_tag:
                    source, value = str(product_tag), product_tag['image']
            else:
                _json = None
                source = str(product_tag)

                try:
                    _json = json.loads(product_tag.text, strict=False)
                except:
                    rgx = re.search(r'"name"\s*:\s*"(.*)"', product_tag.text)
                    if rgx:
                        text = product_tag.text.replace(rgx.group(1), rgx.group(1).replace('"', "'"))
                        _json = json.loads(text)
                
                if _json:
                    if 'image' in _json:
                        value = _json['image']
                else:
                    tag = product_tag.select_one('[data-css-1p0ua3b]')
                    if tag:
                        source = str(tag)
                        value = [tag.get('src')]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
