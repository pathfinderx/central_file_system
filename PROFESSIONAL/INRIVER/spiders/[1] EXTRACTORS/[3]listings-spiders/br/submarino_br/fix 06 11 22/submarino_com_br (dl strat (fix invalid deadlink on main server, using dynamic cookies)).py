from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import cloudscraper

class SubmarinoComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
        
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-US,en;q=0.9',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'
            }

        raw_data = None

        try:
            if data:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            else:
                cookies = self.get_cookies(url)
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
                if res.status_code == 200:
                    raw_data = res.text

                else:
                    res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_cookies (self, url):
        res = self.scraper.get(url, proxies = self.requester.session.proxies)
        cookies = res.cookies.get_dict()
        if cookies:
            cookies['pv'] = 'list'
        return cookies