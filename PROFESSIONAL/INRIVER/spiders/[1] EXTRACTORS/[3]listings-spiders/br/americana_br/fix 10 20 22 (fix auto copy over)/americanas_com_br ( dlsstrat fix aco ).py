from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import cloudscraper

class AmericanasComBrDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper()
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36',
            }

        raw_data = None
        cookies = self.get_cookies(url)

        try:
            if data:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            else:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            if res.status_code in [401, 403]:
                res = self.scraper.get(url, timeout=timeout, headers=None, cookies=cookies, data=data)
            # Download successful
            if res.status_code in [200, 201, 202]:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_cookies (self, url):
        res = self.scraper.get(url, proxies = self.requester.session.proxies)
        cookies = res.cookies.get_dict()
        if cookies:
            cookies['pv'] = 'list'
        return cookies