import logging
import re
import json
import urllib
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class AmericanasComBrWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.americanas.com.br'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_page = False

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36',
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            # Pagination logic 
            # https://www.americanas.com.br/categoria/games/playstation-4/console-ps4
            # https://www.americanas.com.br/categoria/games/playstation-4/jogos-ps4?ordenacao=topSelling&origem=blanca

            while True:
                try:
                    
                    if page == 0:
                        page += 1
                        next_url = url+'limit=24&offset=0' if '?' in url else url+'?limit=24&offset=0'

                    else:
                        if self.next_page:
                            next_url = self.next_page
                        else:
                            break

                    print("On page: %d" % page)
                    print(next_url)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise
    
    def _find_next_page(self, soup):
        # find next page
        tag = soup.select_one('.pagination-product-grid li a [aria-label="Next"]')
        if tag:
            self.next_page = 'https://{}{}'.format(self.__class__.WEBSITE, tag.parent.get('href'))
        else:
            tag = soup.select_one('[property="al:web:url"]')
            if tag and tag.has_attr('content'):
                url = tag['content']
                if 'limit=24&offset=0' in url: # to stop in second page
                    self.next_page = url.replace('offset=0','offset=24')
                else:
                    self.next_page = None
            else:
                self.next_page = None

    def get_products(self, soup):
        try:
            product_tags = soup.find_all("div",{"class":"col__StyledCol-sc-1snw5v3-0 epVkvq src__ColGridItem-sc-122lblh-0 bvfSKS"}) or \
                soup.select('[class="col__StyledCol-sc-1snw5v3-0 jGlQWu src__ColGridItem-sc-122lblh-1 cJnBan"]')
            product_tags = None

            if product_tags:
                return product_tags
            else:
                script_list,  = soup.select('script'), 
                script_data, _rgx, _json = None, None, None

                for i,v in enumerate(script_list): # iterate over script_list and find __APOLLO_STATE__
                    if (str(v).find('__APOLLO_STATE__ ') > -1):
                        script_data = script_list[i] 
                        break

                _rgx = re.search(r'__APOLLO_STATE__ = ([\s\S]+)', script_data.get_text()).group(1) if script_data else None
                _json = json.loads(_rgx)

                # target_product_key = [k for k,v in _json.get('ROOT_QUERY').items() if k.find('search({"limit"') > -1] # shortcut list comprehension
                target_product_key = None
                if (_json and 'ROOT_QUERY' in _json): 

                    for k,v in _json.get('ROOT_QUERY').items(): 
                        if (k.find('search({"limit"') > -1): # find the target long key that contains the products
                            target_product_key = k

                    if (target_product_key and
                        target_product_key in _json['ROOT_QUERY'] and
                        'products' in _json['ROOT_QUERY'][target_product_key]):

                        return _json['ROOT_QUERY'][target_product_key]['products']
            
        except Exception as e:
            print ()

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []

        self._find_next_page(soup)
        # product_ids = []
        # tag = soup.find('script', text=re.compile(r'window.__PRELOADED_STATE__'))
        # if tag:
        #     _rgx = re.search(r'window.__PRELOADED_STATE__ = "(.*)";', tag.get_text().strip())
        #     if _rgx:
        #         self._json = json.loads(urllib.parse.unquote(_rgx.group(1)))
        #         for product in self._json['productGrid']['items']:
        #             product_ids.append(product['id'])
                
        # product_tags = soup.select("div.product-grid-item")
        product_tags = self.get_products(soup)

        if not product_tags:
            return results

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank :
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('product').get('offers({"promoted":true})').get('result')[0].get('salesPrice')
            else:
                tag = product_tag.select_one('span.src__Text-sc-154pg0p-0.price__PromotionalPrice-sc-h6xgft-1') or \
                    product_tag.select_one('span.src__Text-sc-154pg0p-0.src__PromotionalPrice-sc-1k0ejj6-8.gxxqGt')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'BRL'  # Default value | Currency not Found
        return source, value

    def get_price_per_unit(self, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency 
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_value, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency
        
        try:
            promo_value = 0.0
            promo_desc = ""
            src = []
            
            tag = product_tag.find('span',{'class':'src__Text-sc-154pg0p-0 src__Price-sc-1k0ejj6-7 dvVMTs'})

            if tag:
                src.append(str(tag))
                promo_value = tag.get_text().strip()
                
            if promo_desc and promo_value:
                promo_desc = "From %s (%s)" % (promo_value, promo_desc)
            elif promo_value and not promo_desc:
                promo_desc = "From %s" % promo_value

            if promo_desc:
                source = " + ".join(src)
                description_value = promo_desc
                promo_price_value = price_value

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                product_id = product_tag.get('product').get('id')
                request_url = f'https://www.americanas.com.br/produto/{product_id}'
                source, value = str(product_tag), request_url
            else:
                tag = product_tag.select_one('a')

                if tag and tag.has_attr('href'):
                    source = str(tag)
                    url = tag.get('href')
                    value = 'https://{}{}'.format(self.__class__.WEBSITE, url) if 'http' not in url else url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('product').get('name')
            else:
                tag = product_tag.select_one('[class="product-name__Name-sc-1shovj0-0 gUjFDF"]') or \
                    product_tag.select_one('h3.src__Name-sc-1k0ejj6-4.isrBEm')

                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            # if product_id in self._json['products']['refs']:
            #     _json = self._json['products']['refs'][product_id]

            #     if 'rating' in _json.keys():
            #         source = str(product_tag)
            #         if 'average' in _json['rating']:
            #             value = str(_json['rating']['average'])
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('product').get('rating').get('average')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value


        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # if product_id in self._json['products']['refs']:
            #     _json = self._json['products']['refs'][product_id]
                
            #     if 'rating' in _json.keys():
            #         source = str(product_tag)
            #         value = str(_json['rating']['reviews'])
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('product').get('rating').get('reviews')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # if product_id in self._json['products']['refs']:
            #     source = str(product_tag)

            #     _json = self._json['products']['refs'][product_id]
            #     value = str(_json['offers'][0]['availability']['_embedded']['stock']['quantity'])
            pass
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if isinstance(product_tag, dict):
                source = str(product_tag)
                value.append(product_tag.get('product').get('images({"limit":1})')[0].get('big'))
            else:
                tag = product_tag.select_one('picture.src__Picture-xr9q25-2.jaokHu img')
                if tag and tag.has_attr('src'):
                    value.append(tag['src'])
                    source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value