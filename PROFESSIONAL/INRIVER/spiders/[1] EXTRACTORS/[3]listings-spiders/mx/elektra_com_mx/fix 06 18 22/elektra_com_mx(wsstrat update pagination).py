import base64
import logging, urllib, re, json
from copy import deepcopy
from urllib.parse import urlparse

from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class ElektraComMxWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.elektra.com.mx'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)

        # NOTE: in this context, either _method/variable or __method/variable are considered private
        # its just a matter of programming style since im used to
        # _ protected method/variable
        # __ private method/variable

        # NOTE: we use one variable to store the instance state
        # the reason for this one is just a matter of programming style since
        # prior implementations does not really utilize instance state and I
        # dont want also to clutter the object w many instance variables vs prior implementations
        self.__state = {}

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            self.__state = {}

            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # initialize some values on the instance state
            self.__state.update({
                'url': url, 'page': page, 'start_rank': start_rank,
                'postal_code': postal_code, 'max_results': max_results, 'request_params': request_params,
                'headers': None, 'timeout': 10, 'cookies': None
            })

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    self.__state['timeout'] = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    self.__state['headers'] = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    self.__state['cookies'] = request_params['cookies']

            # current_product_count - number of product per iteration/page total_product_count - total number of
            # products so far TOTAL_PRODUCT_COUNT - total number of products found in a certain category. Some
            # swebsites provides this and some not. This is optional
            self.__state.update({
                'last_response': None, 'is_error': False, 'products': [],
                'current_product_count': 0, 'total_product_count': 0, 'TOTAL_PRODUCT_COUNT': 0,
            })

            if self.__state['page'] < 1: self.__state['page'] = 1  # starts with 1
            while True:
                try:

                    print("On page: %d" % self.__state['page'])

                    # Construct URL
                    if self.__state['page'] == 1:
                        api_url = self.construct_url_query(url, self.__state['current_product_count'], 20)
                    else:
                        api_url = self.construct_url_query(url,
                                                           self.__state['total_product_count'],
                                                           self.__state['total_product_count'] + 20)

                    raw_data, self.__state['last_response'] = self.download_strategy.download(api_url)

                    if not raw_data: break

                    self.__state['bs'] = json.loads(raw_data)

                    products = self.get_products(self.__state['bs'])

                    # truncate the products according to the max_results
                    products = self.truncate_products(products)
                    products = self._extract(products, page=self.__state['page'], start_rank=self.__state['start_rank'])

                    self.__state['products'].append(products)
                    self.__state['current_product_count'] = len(products)
                    self.__state['total_product_count'] += self.__state['current_product_count']

                    if not self.has_next(url, self.__state['page']):
                        break

                    self.__state['page'] += 1
                    self.__state['start_rank'] += self.__state['current_product_count']
                except DownloadFailureException:
                    self.__state['is_error'] = True
                    break
                except Exception:
                    raise

            return dict(
                is_error=self.__state['is_error'],
                last_page=self.__state['page'],
                last_response=self.__state['last_response'],
                products=self.__state['products']
            )

        except Exception:
            raise

    def get_products(self, tag):
        products = tag['data']['productSearch']['products']
        return products

    def get_price(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'priceRange' in data and 'sellingPrice' in data['priceRange']:
                if data['priceRange']['sellingPrice']['lowPrice']:
                    source, value = self.__state['last_response'].url, str(data['priceRange']['sellingPrice']['lowPrice'])
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'MXN'
        return source, value

    def get_price_per_unit(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'MXN'  # Default currency
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, data, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'MXN'  # Default currency

        try:
            if 'priceRange' in data and 'listPrice' in data['priceRange']:
                if data['priceRange']['listPrice']['lowPrice']:
                    if float(data['priceRange']['listPrice']['lowPrice']) > float(price_value):
                        description_value = 'From ' + str(data['priceRange']['listPrice']['lowPrice'])
                        promo_price_value = price_value
                        source = self.__state['last_response'].url

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'linkText' in data:
                source, value = self.__state['last_response'].url, 'https://www.elektra.com.mx/' + data['linkText'] + '/p'
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'brand' in data:
                source, value = self.__state['last_response'].url, data['brand']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'productName' in data:
                source, value = self.__state['last_response'].url, data['productName']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'description' in data:
                source, value = self.__state['last_response'].url, data['description']
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = '5'  # default max stars

        return source, value, max_value

    def get_rating_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if 'items' in data and len(data['items']):
                sellers = []
                for item in data['items']:
                    if 'sellers' in item:
                        sellers.extend(item['sellers'])

                if len(sellers):
                    has_stocks = any(i['commertialOffer']['AvailableQuantity'] > 0 for i in sellers if 'commertialOffer' in i)
                    value = 'IN-STOCK' if has_stocks else 'OUT-STOCK'
                    source = self.__state['last_response'].url
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            if 'items' in data and len(data['items']):
                img_list = []
                for item in data['items']:
                    if 'images' in item:
                        img_list.extend(item['images'])

                if len(img_list):
                    source = self.__state['last_response'].url
                    value = [i['imageUrl'] for i in img_list if 'imageUrl' in i]
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################

    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def construct_url_query(self, url, offset, max):

        base_url = 'https://www.elektra.com.mx/_v/segment/graphql/v1'
        binding_id = '96379d85-464b-41f2-9a08-033083a95336'
        sha_hash = 'ced4eca9358c9f47adcc31ea986637aee6be81d70f7a434c6420daf7219b6d39' #defaulted if sha hash is not found in page source -- this value always changes

        selected_facets = None
        temp_response,temp_code = self.download_strategy.download(url)
        if temp_response:
            temp_soup = BeautifulSoup(temp_response,'lxml')
            temp_tag = temp_soup.find('script',text = re.compile('selectedFacets'))
            if temp_tag:
                _json = json.loads(temp_tag.text.strip())
                if _json and 'ROOT_QUERY' in _json.keys() and _json['ROOT_QUERY']:
                    facet_dict = None
                    sha_hash_dict = None
                    for _key in _json['ROOT_QUERY']:
                        if 'typename' in _json['ROOT_QUERY'][_key] and 'ProductSearch' in _json['ROOT_QUERY'][_key]['typename']:
                            sha_hash_dict = _json['ROOT_QUERY'][_key]
                            #facet_dict = _json['ROOT_QUERY'][_key]
                        if 'typename' in _json['ROOT_QUERY'][_key] and ('Facets' in _json['ROOT_QUERY'][_key]['typename'] or 'SearchMetadata' in _json['ROOT_QUERY'][_key]['typename']):
                            facet_dict = _json['ROOT_QUERY'][_key]
                            
                        

                    if facet_dict and 'id' in facet_dict.keys():
                        _id = facet_dict['id']
                        _rgx = re.search(r'"selectedFacets":(\[.*\])\}\)@context',_id)
                        if _rgx:
                            selected_facets = _rgx.group(1)
                        else:
                            selected_facets = None
                            
                        query_rgx = re.search(r'"query":"([\w/-]+)"',_id)
                        if query_rgx:
                            query_val = query_rgx.group(1)
                        else:
                            query_val = 'telefonia/celulares-por-compania' # from old method

                    if sha_hash_dict and 'id' in sha_hash_dict.keys():
                        _id = sha_hash_dict['id']
                        hash_rgx = re.search(r'@runtimeMeta\(\{"hash":"(.*)"',_id)
                        if hash_rgx:
                            sha_hash = hash_rgx.group(1)
                

        if not selected_facets:
            # Encode variables parameter
            parsed_url = urlparse(url)
            
            facets = parsed_url.path.split('/')
            selected_facets = json.dumps([{"key":"c","value":f"{i}"} for i in facets if i.strip()])
        data = '{"hideUnavailableItems":false,"skusFilter":"ALL","simulationBehavior":"skip",' \
               '"installmentCriteria":"MAX_WITHOUT_INTEREST","productOriginVtex":true,"map":"c,c",' \
               f'"query":"{query_val}","orderBy":"","from":{offset},"to":{max},' \
               f'"selectedFacets":{selected_facets},' \
               '"facetsBehavior":"Static","categoryTreeBehavior":"default","withFacets":false}'

        data_bytes = data.encode("ascii")
        base64_bytes = base64.b64encode(data_bytes)
        base64_str = base64_bytes.decode("ascii").strip('=')

        api_url = f'{base_url}?workspace=master&maxAge=short&appsEtag=remove' \
                  f'&domain=store&locale=es-MX&__bindingId={binding_id}&operationName' \
                  '=productSearchV3&variables=%7B%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C' \
                  f'%22sha256Hash%22%3A%22{sha_hash}%22%2C' \
                  '%22sender%22%3A%22vtex.store-resources%400.x%22%2C%22provider%22%3A%22vtex.search-graphql%400.x%22' \
                  f'%7D%2C%22variables%22%3A%22{base64_str}%3D%3D%22%7D'

        return api_url

    def truncate_products(self, products):
        if self.__state['max_results'] is None:
            return products

        remaining_length = self.__state['max_results'] - self.__state['total_product_count']
        # truncate products by slicing
        return products[0:remaining_length]

    def has_next(self, url, page):
        if self.__state['max_results'] is not None and \
                self.__state['total_product_count'] >= self.__state['max_results']:
            return False

        if self.__state['current_product_count'] <= 0:
            return False

        # check for next page button
        raw_data, res = self.download_strategy.download(url+'?page='+str(page))
        if raw_data:
            soup = BeautifulSoup(raw_data, 'lxml')

            nxt_pag_btn_tag = soup.select_one('.vtex-button.bw1.ba.fw5.v-mid.relative.pa0.lh-solid.br2.min-h-small')

            # Every page contains 20 products so it is expected that it reaches maximum product count at 2nd pages
            if (self.__state['page'] == 2 and self.__state['start_rank'] > 20):
                return False

            if not nxt_pag_btn_tag:
                return False

        return True

    def _extract(self, products, page, start_rank):

        template = get_result_base_template()
        results = []

        for product in products:
            result = deepcopy(template)

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(self.__state['bs'])

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
            result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
            result["price_per_unit"]['currency']["source"], \
            result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(product)

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
            result["promo"]['price']["value"], \
            result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"] = self.get_rating_score(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(
                product)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results
