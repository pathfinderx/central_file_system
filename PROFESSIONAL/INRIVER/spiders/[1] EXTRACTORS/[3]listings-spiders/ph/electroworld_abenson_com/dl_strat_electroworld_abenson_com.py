import random
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ElectroworldAbensonComDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=randint(60, 100), headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = { 
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res