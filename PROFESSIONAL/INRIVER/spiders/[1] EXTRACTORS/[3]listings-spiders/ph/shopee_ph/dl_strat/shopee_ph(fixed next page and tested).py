import random
from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class ShopeePhDownloadStrategy(DownloadStrategy):
	def download(self, url, page, postal_code=None, timeout=randint(60, 100), headers=None, cookies=None, data=None):
		category_id = ''
		newest = ''
		if page and (page == 1):
			temp = url.split('.')
			category_id = temp[-1]
			newest = '0'
		elif page and (page > 1): 
			temp = url.split('.')
			category_id = temp[-1]
			newest = '60'
			
		api_url = f"https://shopee.ph/api/v4/search/search_items?by=relevancy&limit=60&match_id={category_id}&newest={newest}&order=desc&page_type=search&scenario=PAGE_OTHERS&version=2"

		if not isinstance(headers, dict):
			headers = { 
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
				'x-api-source': 'pc',
				'x-requested-with': 'XMLHttpRequest',
				'x-shopee-language': 'en'
			}

		if not isinstance(cookies, dict):
			cookies = None

		raw_data = None


		try:
			res = self.requester.get(api_url, timeout=timeout, headers=headers, cookies=cookies)

			# Download successful
			if res.status_code == 200:
				raw_data = res.text

		except Exception:
			raise DownloadFailureException('Download failed - Unhandled exception')

		return raw_data, res