from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

import cloudscraper
from request.response import ResponseWrapper
from request.unblocker import UnblockerSessionRequests

class GraingerComDownloadStrategy(DownloadStrategy):
    def __init__ (self, requester):
        self.requester = requester
        self.unblocker_requester = UnblockerSessionRequests('us')
        self.scraper = cloudscraper.create_scraper()
        self.scraper.proxies = requester.proxies

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):        
        if not isinstance(headers, dict):
            
            # As observed, while using x-dtpc cookie, it will return json data while its counterpart returns an incomplete xml DOM by utizilizing the same url request. Note that this gets blocked sometimes
            # if this request is blocked, dont use unblocker because it will request for an incomplete xml DOM (unless if you can modify the request headers of unblocker)
                # change the headers instead (especially, cookie, and x-dtpc)
            headers = {
                'accept': 'application/json',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'cache-control': 'no-cache',
                'cookie': '__wwgmui=e5250c54-6da8-41b5-b651-ae8deb86d430; JSESSIONID=23DEEC1C93369CB9DCE82E7910450F9E.f238b700; O=1p; insightEligible=false; qc=0; btps=false; sitetype=full; AB1=A; AB2=B; AB4=B; DS1=A; DS2=A; geo=94102|SANFRANCISCO|CA|US; country=US; TLTSID=03647FB84860B5A844DF33FFFA2341F9; gws_lcp=1; signin=B; ak_bmsc=3237A2AB8A0AE1F0C2EEA8C27A10B4FD~000000000000000000000000000000~YAAQpVw6F+h/erd+AQAA5Os5wA6HlWmWbP6hqIaisGL23FtXAH0s6Qz1ZzcNpNaxKnehOeMJI291tshUF70xiMv8MLHkPO8ZKE/8DYc2Y+tYwdR5wgpcUuZDE+Xixq58w1T+PXZWdBjcPyzRzoDkfPcAzvCV7cPIZc5/HNTM5Bopuo2GCBVQpbWmHm5KhO54Du2dHupp5YKI1N2F+hlTs2sl5w9ddgNj2slt0nPyltizqiNQKQawvpjTxJKTicIBUi4y6M6ewa/BzENiaAiysXq6efyjtWVdKCnpN/2eE/IoI72MMttWaNyhGm5uRzaQuov9rKjoAy2UYNDRyEBnP9dx1atYoayeMCyT1+ZN8tYre6zUq5QfCaXTcYz5yqgr+xmy0mcbkvMACRJhQuQ=; bm_sz=9DFAC382C881C9A74A4D37CD937CB9B5~YAAQpVw6F+l/erd+AQAA5Os5wA4TKQa65eMsInx9C35E8XxOm72Q0TEpKG7YMgCs09QjSs/wBLy0riAbl4T+ANnWeKUY14llT8YSPT5IRo8Wao1i46gDvtLwZTpS0sytiYnTFDo8SUJqfppIAjNuMcxJ/AqcYPqQTbMqkwRGZm1i+y8aJlHlOrXuQFHQp3w9kkF/+P0vXXuoFl+spIF/K4ZJtG+4qOVrXMjCBDyKhBWMoYafQvVsa4Hj8VPqc0/WYK0e46XaBkiKB6jJZkNoxzXzcpLl76tpmWpzJiXpCyemFAXIqQ==~3290672~3290418; _abck=4D8BB9950918B41FF3D48CE98FB1733C~0~YAAQpVw6FzeAerd+AQAACQA6wAdWvaUAQmiec/Je9y71txwAl/jfaiu+BWUWVYglPTPRyvbLX9AdiFQTGDuY+LbRFx9r2d/AVlz0vzc/6ie+VTsTbGbsEUjFVU7A+HpVfALv+lgYIMdPkhz8XukSFhs6Gukts4u8EE1d4WGPntkIbbAC2JoWfR0P8ZfGrDBctgJvSra9YNzPPov5Hm08eg5CiBtMvTN1NprlDTZe4Mw3eOUQP4e4OWarcpNsJrH6+LMrHiD2AyrwHdAA9+eW7F/tjHSMvIykwejNT96MeNmtOMGhK6rtsrMpsIDvuO76z0RqNq1MLxQhIO5LZLdoP/CZOih393+vcjOsfjmshvSmQhf60Z5fgt48u/mMh3+bEKS6RmvNXSvOLukxJAKLwCQfV0/JwR+h+y8=~-1~||1-VVZQqQScEE-1-10-1000-2||~1643906074; at_check=true; compare=; AMCVS_FC80403D53C3ED6C0A490D4C%40AdobeOrg=1; s_ecid=MCMID%7C89380793041874892763883560817318033424; AMCV_FC80403D53C3ED6C0A490D4C%40AdobeOrg=-2121179033%7CMCIDTS%7C19027%7CMCMID%7C89380793041874892763883560817318033424%7CMCAAMLH-1644507341%7C9%7CMCAAMB-1644507341%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1643909742s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.3.0; AC=T; ttc=1643902543224; s_cc=true; Monetate=seg%3D1139434; aam_uuid=89665827410793773133855346569694877261; _gcl_au=1.1.474947562.1643902545; _gid=GA1.2.2125318029.1643902550; _clck=henmq4|1|eyo|0; _fbp=fb.1.1643902557699.165465035; maId={"cid":"52eed90414528939807cab2cd5bdf918","sid":"95eb50bc-8bb8-46b7-8358-b0e565c106c4","isSidSaved":true,"sessionStart":"2022-02-03T15:35:49.000Z"}; s_fid=1E2EE3422791BACB-159FEC0342823510; CIP=64.225.19.19; gpv_v10=grainger%2Fcategory%2Ftools%2Fpower%20tools%2Fcordless%20tool%20batteries%20%26%20power%20accessories%2Fcordless%20tool%20batteries; bm_sv=B47D5307707FB3A91FC173001F2D8F59~hULD0UdjX4qGMXUo00dlSAjVxAS1gf+XvYlpgRA9C5XrBnGHofrzG5WRpndmjqKahFhTjcKWgPs1fb0X4KtqM+WrsMgN7U9fZUsPR9VAFRQMnzBrQfTEJ4pubTf1CaNeR3lLoOnGn6OtLjIc8N+KGKkgq9os70Wc91Tl3VfBlQk=; mbox=session#33249379be234384b087c534460b27a2#1643904400|PC#33249379be234384b087c534460b27a2.35_0#1707147722; OptanonConsent=isIABGlobal=false&datestamp=Thu+Feb+03+2022+23%3A42%3A03+GMT%2B0800+(China+Standard+Time)&version=6.10.0&hosts=&consentId=7aa04176-6c64-4261-8bb3-2d6817e7ca0d&interactionCount=1&landingPath=NotLandingPage&groups=BG9%3A1%2CC0007%3A1%2CBG10%3A1%2CC0003%3A1%2CC0001%3A1%2CC0002%3A1&AwaitingReconsent=false; RT="z=1&dm=grainger.com&si=e5ec5213-1eb8-477a-b484-8e11e088df7b&ss=kz754czg&sl=8&tt=ll7&bcn=%2F%2F17de4c16.akstat.io%2F&obo=1&ld=8heg"; _ga_94DBLXKMHK=GS1.1.1643902544.1.1.1643902922.0; _uetsid=f6702e70850611ec81144bcd8ba19ec1; _uetvid=f6707390850611ecb7dee7d5c1d8eafd; _ga=GA1.2.29054988.1643902547; _gat_gtag_UA_148736896_1=1; _clsk=6fww9c|1643902934716|5|0|i.clarity.ms/collect; s_nr30=1643902942929-New; s_pers=%20s_vnum%3D1646064000246%2526vn%253D1%7C1646064000246%3B%20s_invisit%3Dtrue%7C1643904742930%3B; s_sess=%20hbx_lt%3Dgrainger%252Fcategory%252Ftools%252Fpower%2520tools%252Fcordless%2520tool%2520batter%252Fcordless%2520tool%2520batter%255E%255EList%255E%255Egrainger%252Fcategory%252Ftools%252Fpower%2520tools%252Fcordless%2520tool%2520batter%252Fcordless%2520tool%2520batter%2520%257C%2520List%255E%255E%3B; pv=list',
                'pragma': 'no-cache',
                'referer': 'https://www.grainger.com/category/tools/power-tools/cordless-tool-batteries-power-accessories/cordless-tool-batteries?categoryIndex=1',
                'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
                'x-dtpc': '$502920220_939h8vVPVMFALBDHAACAKPUMFEHOCVIUFWGMUR-0'
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res