from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

import cloudscraper
from request.response import ResponseWrapper
from request.unblocker import UnblockerSessionRequests

class GraingerComDownloadStrategy(DownloadStrategy):
	def __init__ (self, requester):
		self.requester = requester
		self.unblocker_requester = UnblockerSessionRequests('us')
		self.scraper = cloudscraper.create_scraper()
		self.scraper.proxies = requester.proxies

	def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):        
		if not isinstance(headers, dict):
			
			# As observed, x-dtpc cookie requests for json product data while its counterpart returns an incomplete xml DOM by utizilizing the same url request without x-dtpc cookie. Note that this gets blocked sometimes
			# if this request is blocked, dont use unblocker because it will request for an incomplete xml DOM (unless if you can modify the request headers of unblocker)
			headers = {
				'accept': 'application/json',
				'accept-encoding': 'gzip, deflate, br',
				'accept-language': 'en-US,en;q=0.9',
				'cache-control': 'no-cache',
				'cookie': 'ttc=1643634517850; btps=false; sitetype=full; AB1=A; AB2=B; AB4=B; DS1=B; DS2=B; geo=10001|NEWYORK|NY|US; country=US; TLTSID=89404FC17F4CFF7271FE88C3D64E20CE; gws_lcp=1; ak_bmsc=CC0A5B4006B7A1ECD52CB40CFB9173A5~000000000000000000000000000000~YAAQrJEvF8RdCaJ+AQAAW2RAsA6xtJhiSWi5sw6fCLJvf84imY1aFNSWWJnoSOfJkbrngjW0khdPTU9mz3G0kfPrxcYOBJFqCIoafvLJ/q7VtyxiZ27IdHyNZNA5xHcqadFlkRRTCo6Fx0/ylX7DvLhujJX4u4aU9yih8zzzrF0UCRpUXdKSeuWk/GaSL/BYHI6Ccf/uX7MGFEWjxRNPTHgK8YQqb5p3WnPN3RXQ6sChSweGcMbArOm9/e8VdWR5g58NiHBhKnxzHHlZL97GF4KVkEcLQ8LVCsfpaKPvy4fxI/SQKANHX6YouEebxzqlGvahraqyqeQAFfkF2ynTOc8SSvduY49JmIFb8FbUGs/7s3dGrJWqYG6LiEoUORpOwai9E5Pnfej5o+dDOLY=; bm_sz=D0C761625BFCBC2F86586F9F8A3C8B31~YAAQrJEvF8VdCaJ+AQAAW2RAsA5zA/nLyns1JTNuqGNIPgOxB0JSxtwEbkkpZZfOIldV1eKWTjOkEXESjmTfKhrnf/GdQufFN/vhRGJIr3trFI8DZUQUxkdUPD1CPgL6Wk/sbGZt4RLoBVkxEKbblwn8G1qqba2roO+70HdTjQegyAugzGJAG5WiO8N5mAcsDnE4fg3ewZp9nMyuWJdoEV2x3mi7CXT18+ZJCXS3U9ByCyVhCg6XLGMYksGiXeuX1SEhyLaID4lmJGb4VmDoegWYPOLPDWynoxh/a4VpzASV8BOrFg==~3622211~3753265; _ga_94DBLXKMHK=GS1.1.1643632316.16.1.1643634519.0; __wwgmui=2e33db29-93ff-457b-af29-5bdd41d8ffd0; JSESSIONID=E205E79D798375835657385041436067.5fdf0745; O=1p; insightEligible=false; qc=0; _abck=ABFB8E6B46F08B9218B797FD7D5D2BF1~0~YAAQl5EvF9LYmIJ+AQAAwh5BsAeZZK9GIg70AQ8pYQkPkK+GZBtj0ae/0g+eioI4c1UL8ouqTyIbSRImapuwD1Qp3zX8etFr8eiWLrF1HBUvsDErtMBPUELogr4zb+epnqi6uZi94Vji0n6YWAL9ALPwqZriKdB7fpHW8p1NeLflEW2dwjXgm0qhgUDO6X7zkpWhN54JoaMKDWHKHLRXYQ+pPDT+TAjYNd4kcVKGD3SP83zxlKrh0ISTBlarX99PTYUcUYvtTTWh02agHqqyRkXMd//Sb4IZMzE4C80cmKoOGK21tJ8Lz680kz8151U7soo9ChnjyzwL/6OHPAiWZF7BWbSsalDk380WXyjiy+PXAMBz49PvTk5FZ/+N3LAB1CyChsc62ATtS6U+46ExKzJ7E33bjQvCFj4=~-1~-1~1643638021; compare=; CIP=167.172.20.129; bm_sv=EEB470FC8CAB618489526F5AAA3C37B8~dKgvZPyI+L0/gND2MIt9L5umEOQl8lfzFeCfDTROUwjl1NAaoVH/cZNFtKLC52GzQ4S1HjzOEqnVTBP3iyo2CDHI60wOAlafc5v+eINXJrtUZqXa/thnXrkd6Zv+AaWy9fEPLtiDQvgZtOJsAhP+4NHvD4BdrjFAzgVjMtKo2/w=; AMCVS_FC80403D53C3ED6C0A490D4C%40AdobeOrg=1; at_check=true; s_ecid=MCMID%7C13491677739190621474080501021446779327; AMCV_FC80403D53C3ED6C0A490D4C%40AdobeOrg=-2121179033%7CMCIDTS%7C19024%7CMCMID%7C13491677739190621474080501021446779327%7CMCAAMLH-1644239378%7C7%7CMCAAMB-1644239378%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1643641781s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.3.0; mbox=session#aac15b9c0d9b4ac48abc9e7d186024d9#1643636440|PC#aac15b9c0d9b4ac48abc9e7d186024d9.34_0#1706879382; AC=T; gpv_v10=grainger%2Fcategory%2Fcleaning%20and%20janitorial%2Ffloor%20cleaning%20machines%2Fcarpet%20%26%20floor%20sweepers; s_cc=true; RT="z=1&dm=grainger.com&si=a6546ad0-4e96-4918-a265-c397a500c859&ss=kz2par32&sl=2&tt=1ajy&bcn=%2F%2F684d0d4a.akstat.io%2F&ld=alsg"; Monetate=seg%3D1139434; aam_uuid=13592165948637365424090683210315784162; OptanonConsent=isIABGlobal=false&datestamp=Mon+Jan+31+2022+21%3A09%3A45+GMT%2B0800+(China+Standard+Time)&version=6.10.0&hosts=&consentId=8ff84e4a-a453-4e15-b4b2-5a0198196805&interactionCount=0&landingPath=https%3A%2F%2Fwww.grainger.com%2Fcategory%2Fcleaning-and-janitorial%2Ffloor-cleaning-machines%2Fcarpet-floor-sweepers%3FcategoryIndex%3D1&groups=BG9%3A1%2CC0007%3A1%2CBG10%3A1%2CC0003%3A1%2CC0001%3A1%2CC0002%3A1; s_nr30=1643634596126-New; s_pers=%20s_vnum%3D1646226517850%2526vn%253D1%7C1646226517850%3B%20s_invisit%3Dtrue%7C1643636396126%3B; s_sess=%20hbx_lt%3Dgrainger%252Fcategory%252Fcleaning%2520and%2520janitor%252Ffloor%2520cleaning%2520machi%252Fcarpet%2520%2526%2520floor%2520sweep%255E%255EList%255E%255Egrainger%252Fcategory%252Fcleaning%2520and%2520janitor%252Ffloor%2520cleaning%2520machi%252Fcarpet%2520%2526%2520floor%2520sweep%2520%257C%2520List%255E%255E%3B; pv=list',
				'pragma': 'no-cache',
				'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
				'sec-ch-ua-mobile': '?0',
				'sec-ch-ua-platform': '"Windows"',
				'sec-fetch-dest': 'empty',
				'sec-fetch-mode': 'cors',
				'sec-fetch-site': 'same-origin',
				'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
				'x-dtpc': '$234579115_743h9vMOQMNPLJUBNQDHOTBBRLMKGUFRRGFVKF-0', 
			}

		if not isinstance(cookies, dict):
			cookies = None

		raw_data = None

		try:
			res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

			# Download successful
			if res.status_code == 200:
				raw_data = res.text

				for history in res.history:
					if history.status_code in [302, 301]:
						raw_data = None
						break

		except Exception as e:
			raise DownloadFailureException('Download failed - Unhandled exception')

		return raw_data, res