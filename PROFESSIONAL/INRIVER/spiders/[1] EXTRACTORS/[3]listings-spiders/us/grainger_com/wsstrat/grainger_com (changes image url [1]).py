import logging
from math import prod
import re
import json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class GraingerComWebsiteStrategy(WebsiteStrategy):
	WEBSITE = 'www.grainger.com'

	def __init__(self, download_strategy):
		super().__init__(download_strategy=download_strategy)
		self.logger = logging.getLogger(__name__)
		self.next_page = None

	def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
		try:
			assert isinstance(url, str)
			assert isinstance(page, int)
			assert isinstance(start_rank, int)

			if max_results:
				assert isinstance(max_results, int)

			# Init request parameters
			timeout = 10
			headers = None
			cookies = None

			if request_params and isinstance(request_params, dict):
				if 'timeout' in request_params and isinstance(request_params['timeout'], int):
					timeout = request_params['timeout']

				if 'headers' in request_params and isinstance(request_params['headers'], dict):
					headers = request_params['headers']

				if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
					cookies = request_params['cookies']

			last_response = None
			is_error = False
			products = list()

			while True:
				try:
					if page == 0:
						page += 1
						next_url = url
					else:
						if self.next_page:
							next_url = self.next_page
						else: 
							break

					print("On page: %d" % page)
					raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
																			  timeout=timeout, headers=headers,
																			  cookies=cookies, data=None)

					if not raw_data:
						break

					product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
					if product:
						products.append(product)
					else:
						break

					if products:
						total_products = sum([len(page_products) for page_products in products])

						if max_results and total_products >= max_results:
							break

						start_rank = total_products + 1

						page += 1
						continue

					else:
						break

				except DownloadFailureException:
					is_error = True
					break

				except Exception:
					raise

			return dict(
				is_error=is_error,
				last_page=page,
				last_response=last_response,
				products=products
			)

		except Exception:
			raise
	
	def _find_next_page(self, soup):
		# find next page
		pass

	def get_products(self, raw_data):
		try:
			if 'seo' in raw_data: # content checker. 'seo' indicates x-dtpc unique response as json. However, it might get blocked sometimes
				_json = json.loads(raw_data)
				if _json and 'view' in _json and \
					'data' in _json['view'] and \
					'searchResultData' in _json['view']['data'] and \
					'products' in _json['view']['data']['searchResultData']:

					return _json['view']['data']['searchResultData']['products']
					
		except Exception as e:
			self.logger.exception(e)
			return None


	def _extract(self, raw_data, page, start_rank, max_results):
		soup = BeautifulSoup(raw_data, 'lxml')
		template = get_result_base_template()
		results = []

		product_tags = self.get_products(raw_data)

		if not product_tags:
			return results
		
		for product_tag in product_tags:
			result = deepcopy(template)
			
			if max_results and max_results < start_rank :
				continue

			result["page_number"] = str(page)
			result["rank"] = str(start_rank)

			# Extract price
			result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

			# Extract currency
			result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

			# Extract price per unit
			result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
				result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
				result["price_per_unit"]['currency']["source"], \
				result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

			# Extract promo
			result["promo"]["source"], result["promo"]['description']["value"], \
				result["promo"]['price']["value"], \
				result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

			# Extract URL
			result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

			# Extract brand
			result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

			# Extract title
			result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

			# Extract description
			result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

			# Extract rating - score
			result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
				result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

			# Extract rating - reviews
			result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

			# Extract availability
			result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

			# Extract image URLS
			result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

			start_rank += 1

			# Append to results list
			results.append(result)

		return results

	def get_price(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and 'sellPrice' in product_tag['priceDataMap']:
					source, value = str(product_tag), product_tag['priceDataMap']['sellPrice']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value

		return source, value

	def get_currency(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = 'USD' # default value
		
		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and 'priceCurrencyIso' in product_tag['priceDataMap']:
					source, value = str(product_tag), product_tag['priceDataMap']['priceCurrencyIso']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value

		return source, value

	def get_price_per_unit(self, currency):
		source = Defaults.GENERIC_NOT_FOUND.value
		unit_value = Defaults.GENERIC_NOT_FOUND.value
		price_value = Defaults.GENERIC_NOT_FOUND.value
		currency_value = currency 
		return source, unit_value, source, price_value, source, currency_value

	def get_promo(self, product_tag, price_value, currency):
		source = Defaults.GENERIC_NOT_FOUND.value
		description_value = Defaults.GENERIC_NOT_FOUND.value
		promo_price_value = Defaults.GENERIC_NOT_FOUND.value
		currency_value = currency
		
		# promo not available

		return source, description_value, promo_price_value, currency_value

	def get_url(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and 'productDetailUrl' in product_tag['priceDataMap']:
					raw_url = product_tag['priceDataMap']['productDetailUrl']
					url = f'www.grainger.com{raw_url}'
					source, value = str(product_tag), url

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.PRICE_EXTRACTION.value

		return source, value

	def get_brand(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and 'brand' in product_tag['priceDataMap']:
					source, value = str(product_tag),  product_tag['priceDataMap']['brand']

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.BRAND_EXTRACTION.value

		return source, value

	def get_title(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'name' in product_tag:
					source, value = str(product_tag),  product_tag['name']
					
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_description(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'truncatedLongDescription' in product_tag:
					source, value = str(product_tag),  product_tag['truncatedLongDescription']
					
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.TITLE_EXTRACTION.value

		return source, value

	def get_ratings(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value
		max_value = "5"  # For this site, max rating is 5 stars

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'averageRating' in product_tag:
					source, value = str(product_tag),  str(product_tag['averageRating'])
					
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.RATING_EXTRACTION.value

		return source, value, max_value

	def get_reviews(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'numberOfReviews' in product_tag:
					source, value = str(product_tag),  str(product_tag['numberOfReviews'])

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.REVIEWS_EXTRACTION.value

		return source, value

	def get_availability(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = Defaults.GENERIC_NOT_FOUND.value

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and \
					'product' in product_tag['priceDataMap'] and \
					'stockStatus'in product_tag['priceDataMap']['product']:
					source, value = str(product_tag),  product_tag['priceDataMap']['product']['stockStatus']
			
		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.AVAILABILITY_EXTRACTION.value

		return source, value

	def get_image_urls(self, product_tag):
		source = Defaults.GENERIC_NOT_FOUND.value
		value = []  # image_urls data type must be list

		try:
			if product_tag and isinstance(product_tag, dict):
				if 'priceDataMap' in product_tag and \
					'product' in product_tag['priceDataMap'] and \
					'imageUrl'in product_tag['priceDataMap']['product']:
					source = str(product_tag)
					value.append(product_tag['priceDataMap']['product']['imageUrl'])

		except Exception as e:
			self.logger.exception(e)
			value = FailureMessages.IMAGE_URLS_EXTRACTION.value

		return source, value
