import logging
import re
import json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template

class GraingerComWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.grainger.com'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_page = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            while True:
                try:
                    if page == 0:
                        page += 1
                        next_url = url
                    else:
                        if self.next_page:
                            next_url = self.next_page
                        else: 
                            break

                    print("On page: %d" % page)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise
    
    def _find_next_page(self, soup):
        # find next page
        tag = soup.select_one('.show-more button')
        if tag and tag.has_attr('data-url'):
            self.next_page = tag.get('data-url')
        else:
            self.next_page = None

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []

        product_tags = soup.select(".product-grid-tile-wrapper")

        if not product_tags:
            return results
        
        product_id_tags = soup.select('.product.grid-tile')
        product_ids = [products.get('data-pid') for products in product_id_tags if products.get('data-pid')]
        ratings_url = "https://api.bazaarvoice.com/data/statistics.json?" \
            "apiversion=5.4&passkey=ca0SPanXcxTi6Os49LTaXK2PuXoCok57Y7dzJY0FfuxDs&stats=Reviews&filter=ContentLocale:en_US&" \
            "filter=ProductId:%s" % ",".join(product_ids)
            
        ratings_raw = self.download_strategy.download_ratings(ratings_url)
        ratings_json = {review["ProductStatistics"]["ProductId"]:review["ProductStatistics"]["ReviewStatistics"] for review in json.loads(ratings_raw)["Results"]}

        for product_tag in product_tags:
            result = deepcopy(template)
            
            tag = product_tag.select_one('.product.grid-tile')
            if tag and not tag.has_attr('data-pid'):
                continue

            if max_results and max_results < start_rank :
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag, ratings_json, ratings_url)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag, ratings_json, ratings_url)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            price = 0.00
            tag = product_tag.select_one('div.price span.sales .value')

            if tag and tag.has_attr('content'):
                source = str(tag)
                price = tag.get('content')

            if price:
                value = price

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'USD'  # Default value | Currency not Found
        return source, value

    def get_price_per_unit(self, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency 
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_value, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency
        
        try:
            promo = 0.0
            tag = product_tag.select_one('div.price span.strike-through .value')
            if tag:
                promo = tag.get_text().strip()
            
            if promo and promo != price_value:
                source = str(tag)
                description_value = "From %s" % promo
                promo_price_value = price_value

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one(".pdp-link a.link") 

            if tag:
                source = str(tag)
                url = tag.get('href')

                if not 'http' in url:
                    value = "https://%s%s" % (self.__class__.WEBSITE, url)
                else:
                    value = url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one(".pdp-link a.link") 
            if tag and tag.has_attr('data-gtmdata'):
                _json = json.loads(tag.get('data-gtmdata'))

                if 'brand' in _json['productInfo'].keys():
                    source = str(tag)
                    value = _json['productInfo']['brand']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one(".pdp-link a.link")

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_ratings(self, product_tag, ratings_json, api_url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            score = None
            tag = product_tag.select_one('.product.grid-tile')
            if tag and tag.has_attr('data-pid'):
                prod_id = tag.get('data-pid')
                if prod_id in ratings_json.keys():
                    score = ratings_json[prod_id]["AverageOverallRating"]
                    
            if score:
                source = api_url
                value = score

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag, ratings_json, api_url):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            reviews = None
            tag = product_tag.select_one('.product.grid-tile')
            if tag and tag.has_attr('data-pid'):
                prod_id = tag.get('data-pid')
                if prod_id in ratings_json.keys():
                    reviews = ratings_json[prod_id]["TotalReviewCount"]
                    
            if reviews:
                source = api_url
                value = reviews

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one(".pdp-link a.link") 

            if tag and tag.has_attr('data-gtmdata'):
                _json = json.loads(tag.get('data-gtmdata'))
                
                if 'availability' in _json['productInfo'].keys():
                    source = str(tag)
                    value = _json['productInfo']['availability']

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            src = None
            img_urls = []
            tag = product_tag.select_one('.image-container img.tile-image')

            if tag:
                if tag.has_attr('src'):
                    img_src = tag.get('src')
                    if '?$grid$' in img_src:
                        img_src = img_src.replace('?$grid$', '')
                    img_urls.append(img_src)

            if len(img_urls):
                source = src
                value = img_urls

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value
