from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class FergusonComDownloadStrategy(DownloadStrategy):

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None
    
        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def modify_url (self, url):
            # url = f'https://www.ferguson.com/category/bathroom-plumbing/bathroom-faucets/bathroom-sink-faucets/_/N-zbq4i3Zzbq5ebZzbq7ix?ADA_Compliant_fr=some&Application_fr=some&Bathroom_Faucet_Type_fr=some&CEC_Compliant_fr=some&Center_Size_fr=some&Collection_fr=some&Color_Finish_Category_fr=some&Deck_Plate_Included_fr=some&Faucet_Mount_fr=some&Flow_Rate_fr=some&Handle_Type_fr=some&Nrpp=30&Number_of_Handles_fr=some&Number_of_Holes_fr=some&WaterSense_Labeled_fr=some&_=1643779825700&brand_fr=some&ds=list&sr=everywhere'

            next_result = 20
            first_split = url.split('.com')
            second_split = first_split[1].split('?')
            category = second_split[0]
            
            # https://www.ferguson.com/cartridges/main/plpRedesign/ajaxPlpRightItems.jsp?/category/bathroom-plumbing/bathroom-faucets/bathroom-sink-faucets/_/N-zbq4i3Zzbq5ebZzbq7ix&No=40&groupBound=1_5&sr=everywhere&_=1643792701665

            url = f'https://www.ferguson.com/cartridges/main/plpRedesign/ajaxPlpRightItems.jsp?{category}&No={next_result}&groupBound=1_5&sr=everywhere'

            return url
