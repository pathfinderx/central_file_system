from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
import json

class FergusonComDownloadStrategy(DownloadStrategy):

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;"
                    "q=0.8,application/signed-exchange;v=b3;q=0.9",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" 
                    "Chrome/88.0.4324.146 Safari/537.36"
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None
    
        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def modify_url (self, url):
            next_result = 20
            first_split = url.split('.com')
            second_split = first_split[1].split('?')
            category = second_split[0]

            url = f'https://www.ferguson.com/cartridges/main/plpRedesign/ajaxPlpRightItems.jsp?{category}&No={next_result}&groupBound=1_5&sr=everywhere'

            return url
