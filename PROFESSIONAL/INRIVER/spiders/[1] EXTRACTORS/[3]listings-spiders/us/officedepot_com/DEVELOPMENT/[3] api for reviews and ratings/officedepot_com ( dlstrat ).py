import re, json
from bs4 import BeautifulSoup
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class OfficedepotComDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Accept': 'image/avif,image/webp,image/apng,image/*,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.9',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, data=json.dumps(data), timeout=timeout, headers=headers)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_reviews_and_ratings(self, skus, timeout=10000, headers=None, cookies=None, data=None):
        try:
            api_url = self.construct_sku_api_url(skus)

            if not data:
                res = self.requester.get(api_url, timeout=timeout, headers=headers, cookies=cookies)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - get_reviews_and_ratings()')

        return raw_data, res

    def construct_sku_api_url(self, skus) -> list:
        api_version = 5.4
        pass_key = 'caob5V3OJhI8lZDGWAWVDrPTHzXvME8y9qobBOFBZnfhQ'
        stats = 'Reviews'
        _filter = 'ContentLocale:en_US'
        api_url = f"https://api.bazaarvoice.com/data/statistics.json?apiversion={api_version}&passkey={pass_key}&stats=Reviews&filter=ContentLocale:en_US&filter=ProductId:{[f'{x}' for x in skus]}"
        _rgx = None
        
        _rgx = re.sub(r" |\[|\]|'", '', api_url)
        
        return _rgx if _rgx else None