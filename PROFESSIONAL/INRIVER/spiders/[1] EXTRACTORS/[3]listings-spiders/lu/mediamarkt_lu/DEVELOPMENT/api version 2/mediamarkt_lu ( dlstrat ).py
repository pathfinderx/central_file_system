import re, json

from bs4 import BeautifulSoup

from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests


class MediamarktLuDownloadStrategy(DownloadStrategy):
    def __init__ (self, requester):
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('lu')
        self.global_html_data = None

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'Accept': 'image/avif,image/webp,image/apng,image/*,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.9',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        try:
            if data:
                res = self.requester.post(url, data=json.dumps(data), timeout=timeout, headers=headers)
            else:
                res = self.unblocker.get(url, timeout=10000)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def get_api_data(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None, page=1) -> str:
        try:
            raw_html = self.download(url) # download raw data from priomordial url

            if (raw_html and 
                len(raw_html) > 1 and 
                raw_html[1].status_code in [200, 201]):

                api_url = self.get_api_url(raw_html[0], page)
                headers = self.get_request_headers()

                res = self.requester.get(api_url, timeout=timeout, headers=headers, cookies=None)

            if res.status_code in [200, 201]:
                raw_data = res.content.decode('utf-8')

                return raw_data, res if res else None
            return None

        except Exception as e:
            print('Error at: dlstrat -> get_api_data() -> with message: ', e)

    def get_api_url(self, raw_html, page) -> str:
        try:
            soup = BeautifulSoup(raw_html, 'lxml')
            callback = 'jQuery360046052293973605063_1674188317350'
            filter_id = None 
            shop = None
            collection = None # var page_id = 279165468867;
            sort_by = 'manual'
            limit = '12'
            event = 'all' if page == 1 else 'loadmore'
            page_indicator = page
            # options = '1674188317351'

            collection_rgx = re.search(r'page_id = ([\s\S]+?);', str(soup)) # extract collection
            if collection_rgx:
                collection = collection_rgx.group(1)
                self.global_html_data = raw_html

            shop_tag = soup.select_one('meta#th_shop_url') # extract shop
            if (shop_tag and shop_tag.has_attr('content')):
                shop = shop_tag.get('content')

            filter_id_rgx = re.search(r'globo_filters_json = ([\s\S]+?})', str(soup)) # extract filter_id
            if (filter_id_rgx and collection):
                try:
                    _json = json.loads(filter_id_rgx.group(1))
                    if (_json and collection in _json):
                        filter_id = str(_json.get(collection))

                except Exception as e:
                    print('Error at: dlstrat -> set_api_url() -> collection_rgx extraction -> with error: ', e)

            if (filter_id and
                shop and
                collection):
            
                api_url = f"https://filter-eu.globosoftware.net/filter?callback={callback}&filter_id={filter_id}&shop={shop}&collection={collection}&sort_by={sort_by}&limit={limit}&event={event}"

                if (int(page_indicator) > 1):
                    api_url = f"https://filter-eu.globosoftware.net/filter?callback={callback}&filter_id={filter_id}&shop={shop}&collection={collection}&sort_by={sort_by}&limit={limit}&event={event}&page={page_indicator}"

                return api_url
            return None
    
        except Exception as e:
            print('Error at: dlstrat -> set_api_url() -> with error: ', e)

    def get_request_headers(self) -> dict:
        headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9,fil;q=0.8,es;q=0.7',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Cookie': 'XSRF-TOKEN=eyJpdiI6IlwveUx6WUFKemVTZHBrcEJPUzJXWUxBPT0iLCJ2YWx1ZSI6IjJjSnRTdDFwNW9aUVRMTk5VaytCQk1Sa3NzZTkzd01zWGZEeXFcL0JpQWxJSElkZzA5Znd6bTd0U3ZHODZGOTFzIiwibWFjIjoiODRhMWM5MjIwOTYwYmNlNzUxYWUxNzkyMzUwZDQyNzQzYmMxNGYzY2NkMGQxYTE3ZGVjNDJhZmE0YzllNmMxNCJ9; smart_product_filter_search_session=eyJpdiI6IkVTTndmS3ExM1pVZXM5YnRmT29FXC9BPT0iLCJ2YWx1ZSI6ImQ5VGZnWXVsSW9YcHdXRXM2eDk3UU1MUURheWlQYmpnMWRLeVFRTENXbDczTnd6aW5hSTdGMzB4VFdnc0lKdTciLCJtYWMiOiJmMTgyNzE3ZDkzNTViZGZkYzhkNTAyNDNiOGViZTMwYmE0ZTFmNzY1ZGUxNTBhMjUyMjhlZjllNDI2YjkzNmRmIn0%3D',
            'Host': 'filter-eu.globosoftware.net',
            'Pragma': 'no-cache',
            'Referer': 'https://mediamarkt.lu/',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'Sec-Fetch-Dest': 'script',
            'Sec-Fetch-Mode': 'no-cors',
            'Sec-Fetch-Site': 'cross-site',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
        }

        return headers