import logging
import json, re 
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MediamarktLuWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.lu'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.pagination_url = list()
        self.pagination_counter = -1
        self.preserved_url = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)
            self.preserved_url = url

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()
            next_url = ''

            while True:
                try:
                    """ url alternator """
                    if page == 0:
                        page += 1
                        next_url = url

                    else:
                        next_url = next_url

                    """ Downloader """
                    print("On page: %d" % page)
                    print(next_url)
                    raw_data, last_response = self.download_strategy.get_api_data(next_url, 
                                                                            postal_code=postal_code,
                                                                            timeout=timeout, 
                                                                            headers=headers,
                                                                            cookies=cookies, 
                                                                            data=None, 
                                                                            page=page)

                    if not raw_data:
                        break
                    
                    """ Product Extraction """
                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    """ Pagination loader and breaker"""
                    if not self.pagination_url: # if pagination url is empty
                        self.load_next_urls(url) # load the urls
                    
                    self.pagination_counter += 1
                    
                    if (self.pagination_counter <= len(self.pagination_url) - 1):
                        next_url = self.pagination_url[self.pagination_counter]
                    elif (self.pagination_counter >= len(self.pagination_url) - 1):
                        break

                    if not next_url:
                        break

                    """ Product count increment and limiter"""
                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def load_next_urls(self, url):
        self.pagination_url = [
            f"{url}?page=2&limit=12",
            f"{url}?page=3&limit=12"
        ]

    def get_product(self, soup) -> list:
        try:
            product_tag, _json = soup.select('.product-card.product-card--list'), None

            if product_tag:
                return product_tag if (product_tag) else None # returns list
            else:
                _rgx_filters = re.search(r'({\"filters[\s\S]+?)\);', soup.get_text(strip=True))
                _rgx2_products = re.search(r'({\"products[\s\S]+?)\);', soup.get_text(strip=True))

                if _rgx_filters:
                    _json = json.loads(_rgx_filters.group(1))

                elif _rgx2_products:
                    _json = json.loads(_rgx2_products.group(1))

                return _json.get('products') if (_json and 'products' in _json) else None

        except Exception as e:
            print('Error at: wsstrat -> get_product() -> with message: ', e)

    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []
        
        product_tags = self.get_product(soup)

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency()

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if (product_tag and 
                    'variants' in product_tag and
                    len(product_tag['variants']) > 0 and
                    'price' in product_tag['variants'][0]):

                    source, value = str(product_tag), product_tag['variants'][0]['price']
            else:
                tag = product_tag.select_one('.price-item.price-item--sale')

                if (tag):
                    source, value = str(tag), tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'  # Default value
        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site
        price_value = Defaults.GENERIC_NOT_FOUND.value
        return source, description_value, price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            raw_html = self.download_strategy.global_html_data
            soup, collection = None, None
            soup = BeautifulSoup(raw_html, 'lxml') if raw_html else None
            url = None

            if isinstance(product_tag, dict):
                collection_rgx = re.search(r'page_id = ([\s\S]+?);', str(soup))
                if collection_rgx:
                    collection = collection_rgx.group(1)

                if (collection):
                    segment1 = [x.get('handle') for x in product_tag.get('collections') if x.get('id') == int(collection)]
                    segment2 = product_tag.get('handle')
                    url = f"https://mediamarkt.lu/collections/{segment1[0]}/products/{segment2}"

                if url:
                    source, value = str(product_tag), url

            else:
                tag = product_tag.select_one('.c-product-card__action a') or \
                    product_tag.select_one('a.full-width-link')

                if (tag and tag.has_attr('href')):
                    base_url = f"https://{self.__class__.WEBSITE}"
                    value = f"{base_url}/{tag.get('href')}"

                    if value:
                        source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('vendor' in product_tag):
                    source, value = str(product_tag), product_tag['vendor']

            else:
                tag = product_tag.select_one('.list-view-item__vendor')

                if tag:
                    source, value = str(tag), tag.get_text(strip=True)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('title' in product_tag):
                    source, value = str(product_tag), product_tag['title']
            else:
                tag = product_tag.select_one('.product-card__title')

                if tag:
                    source, value = str(tag), tag.get_text(strip=True)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        try:
            pass # no description

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        # No Ratings Indicated in site

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # No Ratings Indicated in site

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                if ('available' in product_tag):
                    source, value = str(product_tag), str(product_tag['available'])
            else:
                tag = product_tag.select_one('span.price__badge.price__badge--sold-out')
                
                if tag:
                    source, value = str(tag), tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if isinstance(product_tag, dict):
                if ('image' in product_tag and 
                    'src' in product_tag['image']):
                    value.append(product_tag['image']['src'])
                if value:
                    source = str(product_tag)
            else:
                tag = product_tag.select_one('img')
                
                if (tag and tag.has_attr('src')):
                    source = str(tag)
                    value.append(tag['src'])

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value