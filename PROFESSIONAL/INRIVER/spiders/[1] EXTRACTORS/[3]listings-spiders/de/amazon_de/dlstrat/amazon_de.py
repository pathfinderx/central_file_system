from request.unblocker import UnblockerSessionRequests
import json
import cloudscraper


url = 'https://www.amazon.de/b?node=361589011&ref=lp_570278_nr_n_5'
res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies)
res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies, data=json.dumps(payload))
res = self.unblocker.get(url, timeout=timeout, headers=headers, cookies=cookies)

headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9',
    'cache-control': 'no-cache',
    'cookie': 'session-id=260-4084363-6646564; i18n-prefs=EUR; ubid-acbde=257-6927598-9128616; session-token=bTEMucdTWMq2GtoEzR+zofqsC8SR1vM9zOoudxZdGjFrBbOT+cn5yJvrRAfDPRCiZBFvCz6Q+NmjUhMo/7y7bwpbtSBY2W1WOycdxuE//aakod4PNN81SMQU3ROVaKQQuOJmjrzhK/pqSd9PBRxfZVpxwu9a39btIpM3HcWGmqqDzuX57mqruqu4HZ1HC5x+; session-id-time=2082754801l; csm-hit=tb:2GMYA03FSW6M0VF00DK3+s-PA301H4MQWXPYRHRRERB|1642897729140&t:1642897729140&adb:adblk_no',
    'downlink': '5.4',
    'ect': '4g',
    'pragma': 'no-cache',
    'rtt': '150',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
}

payload = {
    'node': '361589011',
    'ref': 'lp_570278_nr_n_5'
}