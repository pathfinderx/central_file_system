from itertools import product
import logging
import json, re
from math import prod 
from copy import deepcopy
from tkinter import E
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class MediamarktDeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.mediamarkt.de'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.isApiUrl = False
        self.pim_code = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/57.0.0.12335 Safari/537.36',
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            next_url = url
            raw_data = None # declared as higher variable scope

            while True:
                try:
                    if page == 0:
                        page += 1

                    if (page >= 1 and self.isApiUrl): # page 2 and url is api (this method prevents using self.download_strategy.download() -> which uses unblocker that wont work with api request)
                        raw_data = self.download_strategy.api_downloader(request_url, timeout=10, headers=None, cookies=None, data=None)

                        if raw_data:
                            try:
                                _json = json.loads(raw_data)
                                raw_data = _json if _json else None
                            except Exception as e:
                                print('Error at: wsstrat-> 2nd page data extraction using self.download_strategy.api_downloader -> with message: ', e)
                    else: 
                        print("On page: %d" % page)
                        print(next_url)
                        raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)
                    if not raw_data:
                        break
                    
                    if (url in ['https://www.mediamarkt.de/de/category/_bluetooth-lautsprecher-522549.html']): # page already incremented for next url reques
                        self.isApiUrl = True
                        request_url = self._get_url_parameters(url, raw_data, page)
                        next_url = request_url if request_url else None
                    else:
                        next_url = self.__get_next_url(raw_data, page, url)

                    products.append(self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results))

                    if not next_url:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise

    def get_products(self, raw_data):
        products, soup = None, None
        if isinstance(raw_data, str):
            soup = BeautifulSoup(raw_data, 'lxml')
            products = soup.find_all('div', {'class': re.compile('.*ProductFlexBox__StyledListItem.*')}) or soup.select('[data-test="mms-search-srp-productlist-item"]')
        elif isinstance(raw_data, dict):
            _json = raw_data
            if (_json and 
                'data' in _json and 
                'categoryV4' in _json['data'] and
                'products' in _json['data']['categoryV4']):

                return _json['data']['categoryV4']['products']
        if products:
            return products

    def _extract(self, raw_data, page, start_rank, max_results):
        template = get_result_base_template()
        results = []

        products = self.get_products(raw_data)

        for product in products:
            result = deepcopy(template)

            if max_results and max_results < start_rank:
                continue
            
            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency()

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit()

            # Extract promo
            result["promo"]['description']["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["source"],
                                                                      result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                source, value = str(product), product.get('productAggregate').get('price').get('price')
            else:
                tag = product.find('div', {'class':re.compile('.*StrikeThrough__StyledStrikeThrough.*')})
                if tag:
                    tag = product.find_all('div', {'class':re.compile('.*UnbrandedPriceDisplay__StyledUnbrandedPriceDisplayWrapper.*')})
                    if tag:
                        tag = tag[-1]
                        source, value = str(tag), ' '.join(tag.stripped_strings)
                    else:
                        tag = product.select_one('[data-test="product-price"]')
                        if tag:
                            tag = tag.select('[display=flex]')
                            if tag:
                                tag = tag[1].find_all('div', {'class':re.compile('.*UnbrandedPricestyled__StyledUnbrandedPriceDisplayWrapper.*')})

                                if tag:
                                    tag = tag[-1]
                                    source, value = str(tag), ' '.join(tag.stripped_strings)

                else:
                    tag = product.select_one('div[data-test="product-price"]')
                    if tag:
                        price_tag = tag.select('div.UnbrandedPriceDisplay__StyledUnbrandedPriceDisplayWrapper-sc-1pmc1sr-0.dLHvFz') or\
                                    tag.select('div.UnbrandedPriceDisplay__StyledUnbrandedPriceDisplayWrapper-sc-1pmc1sr-0') or\
                                    tag.find_all('div', {'class':re.compile('.*UnbrandedPricestyled__StyledUnbrandedPriceDisplayWrapper.*')}) or\
                                    tag.find_all('div',{'class': re.compile(r'StyledUnbrandedPriceDisplayWrapper-.*')})
                        if price_tag:
                            # for double price
                            if price_tag[-1].find('span',{'class':re.compile(r'ScreenreaderTextSpan-sc-.*')}):
                                price_tag[-1].find('span',{'class':re.compile(r'ScreenreaderTextSpan-sc-.*')}).extract()
                            source = str(tag)
                            value = price_tag[-1].get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'

        try:
            pass

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value        

        return source, value

    def get_price_per_unit(self):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product, price_source, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default value for this site

        try:
            if isinstance(product, dict):
                source, value = str(product), product.get('productAggregate').get('price').get('promotions')[1].get('strikePrice')
            else:
                tag = product.find('div', {'class':re.compile('.*StrikeThrough__StyledStrikeThrough.*')})
                if tag:
                    old_price = re.sub('[^0-9\.]', '', tag.text)
                    if old_price:
                        source, description_value = str(tag), 'From {}'.format(old_price)

                    else:
                        price_value = Defaults.GENERIC_NOT_FOUND.value

                else:
                    price_value = Defaults.GENERIC_NOT_FOUND.value
        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, price_value, currency_value

    def get_url(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                url = 'www.mediamarkt.de' + product.get('productAggregate').get('product').get('url')
                source, value = str(product), url
            else:
                tag = product.find('a', {'class':re.compile('.*Linkstyled__StyledLink.*')}) or product.find('a',{'class':re.compile(r'StyledLinkRouter-.*')})

                if tag and tag.has_attr('href'):
                    source, value = str(tag), 'https://{}{}'.format(self.__class__.WEBSITE, tag['href']) 

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                source, value = str(product), product.get('productAggregate').get('product').get('manufacturer')
            else:
                tag = product.select_one('[data-test="product-manufacturer"]')

                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                source, value = str(product), product.get('productAggregate').get('product').get('title')
            else:
                tag = product.select_one('[data-test="product-title"]')
                
                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                soup = BeautifulSoup(product.get('productAggregate').get('product').get('description'), 'lxml')
                source, value = str(product), ' '.join(soup.stripped_strings)
            else:
                tag = product.select_one('[data-test="feature-list"]')

                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        try:
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('productAggregate').get('rating').get('average')
            else:
                tag = product_tag.select_one('[data-test="mms-customer-rating"]')
                if tag:
                    if tag.parent.has_attr('aria-label'):
                        source = str(tag.parent)
                        value = tag.parent.get('aria-label')

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product_tag, dict):
                source, value = str(product_tag), product_tag.get('productAggregate').get('rating').get('count')
            else:
                tag = product_tag.select_one('[data-test="mms-customer-rating"] span')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            if isinstance(product, dict):
                source, value = str(product), product.get('productAggregate').get('availability').get('delivery').get('availabilityType')
            else:
                tag = product.find('div', {'class':re.compile('.*Availabilitystyled__StyledAvailabilityHeadingWrapper.*')})
                
                if tag:
                    source, value = str(tag), ' '.join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            if isinstance(product, dict):
                titleImageId = product.get('productAggregate').get('product').get('titleImageId')
                image_url = f'https://assets.mmsrg.com/isr/166325/c1/-/{titleImageId}/mobile_200_200_png'
                source = str(product)
                value.append(image_url)
            else:
                tags = product.select('[data-test="product-image"] img')

                if tags:
                    source = str(tags)
                    value = [i['src'] for i in tags if i.has_attr('src')] if tags else []              

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def __get_next_url(self, raw_data, page, url):
        soup = BeautifulSoup(raw_data, 'lxml')
        
        products = soup.find_all('div', {'class': re.compile('.*ProductFlexBox__StyledListItem.*')}) or soup.select('[data-test="mms-search-srp-productlist-item"]')

        if len(products) < 12:
            return None
        else:
            if 'page=' not in url and '?' in url:
                return '{}&page={}'.format(url, page+1)
            elif 'page=' in url and '?' in url:
                return url.replace('page=','page='+str(page+1))
            else:
                return '{}?page={}'.format(url, page+1) 

    def _get_url_parameters (self, url, raw_data, page):
        if isinstance(raw_data, str):
            self.pim_code  = re.search(r'pim\_code=([\s\S]+?)\&amp', raw_data).group(1)
        
            if self.pim_code:
                request_url = f'https://www.mediamarkt.de/api/v1/graphql?operationName=CategoryV4&variables=%7B%22hasMarketplace%22%3Atrue%2C%22maxNumberOfAds%22%3A2%2C%22isRequestSponsoredSearch%22%3Afalse%2C%22isDemonstrationModelAvailabilityActive%22%3Afalse%2C%22withMarketingInfos%22%3Afalse%2C%22filters%22%3A%5B%5D%2C%22pimCode%22%3A%22{self.pim_code}%22%2C%22page%22%3A{page+1}%2C%22experiment%22%3A%22mp%22%2C%22sessionId%22%3A%2208d3b896-366a-449f-b738-b948f78ea23d%22%2C%22customerId%22%3A%2208d3b896-366a-449f-b738-b948f78ea23d%22%2C%22pageType%22%3A%22Category%22%2C%22productFilters%22%3A%5B%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%226901f75d0379900c5917d2a6cf77ead512ac14b895ca7d73fddf38c7338dd24a%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22DE%22%2C%22language%22%3A%22de%22%2C%22globalLoyaltyEnrollment%22%3Atrue%2C%22globalLoyaltyProgram%22%3Atrue%2C%22fifaUserCreation%22%3Atrue%7D%7D'
                return request_url
        elif isinstance(raw_data, dict):
            if self.pim_code:
                request_url = f'https://www.mediamarkt.de/api/v1/graphql?operationName=CategoryV4&variables=%7B%22hasMarketplace%22%3Atrue%2C%22maxNumberOfAds%22%3A2%2C%22isRequestSponsoredSearch%22%3Afalse%2C%22isDemonstrationModelAvailabilityActive%22%3Afalse%2C%22withMarketingInfos%22%3Afalse%2C%22filters%22%3A%5B%5D%2C%22pimCode%22%3A%22{self.pim_code}%22%2C%22page%22%3A{page+1}%2C%22experiment%22%3A%22mp%22%2C%22sessionId%22%3A%2208d3b896-366a-449f-b738-b948f78ea23d%22%2C%22customerId%22%3A%2208d3b896-366a-449f-b738-b948f78ea23d%22%2C%22pageType%22%3A%22Category%22%2C%22productFilters%22%3A%5B%5D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%226901f75d0379900c5917d2a6cf77ead512ac14b895ca7d73fddf38c7338dd24a%22%7D%2C%22pwa%22%3A%7B%22salesLine%22%3A%22Media%22%2C%22country%22%3A%22DE%22%2C%22language%22%3A%22de%22%2C%22globalLoyaltyEnrollment%22%3Atrue%2C%22globalLoyaltyProgram%22%3Atrue%2C%22fifaUserCreation%22%3Atrue%7D%7D'
                return request_url

        else:
            return None