import json, re, time
import random
import cloudscraper
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests


class MediamarktDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)
        self.luminati = requester
        self.requester = UnblockerSessionRequests('de')
        self.cookies = None

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        self.scraper = cloudscraper.create_scraper(
            interpreter='native'
        )
        # self.scraper.proxies = self.requester.proxies

        random_agents = ['user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36']
        if not isinstance(headers, dict):
            headers = {
                'User-Agent': random.choice(random_agents)
            }

        if not isinstance(cookies, dict):
            cookies = None

        # if not self.cookies:
        #     self.cookies = self.get_cookies(headers=headers, timeout=timeout)

        raw_data = None

        try:
            # res = self.requester.get(url, timeout=timeout, headers=headers, cookies=self.cookies)
            res = self.luminati.get(url, timeout=timeout, headers=None, cookies=None)

            if res.status_code not in [200, 201]:
                res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=self.cookies)

            if res.status_code not in [200, 201]:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=self.cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                # for history in res.history:
                #     if history.status_code in [302, 301]:
                #         raw_data = None
                #         break
            # else:
            #     time.sleep(2)
            #     # Request using cloudscraper without proxy
            #     res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=self.cookies, proxies = self.requester.session.proxies)

            #     # Download successful
            #     if res.status_code == 200:
            #         raw_data = res.text

            #         for history in res.history:
            #             if history.status_code in [302, 301]:
            #                 raw_data = None
            #                 break
                
            #     if res.status_code == 403:
            #         res = self.scraper.get(url)
            #         if res.status_code == 200:
            #             raw_data = res.text

            #             for history in res.history:
            #                 if history.status_code in [302, 301]:
            #                     raw_data = None
            #                     break

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res
    
    def get_cookies(self, headers, timeout):
        cookies = None
        url = 'https://www.mediamarkt.de/'

        res = self.requester.get(url, timeout=timeout, headers=headers)
        cookies = res.cookies
        print(cookies.get_dict())
        url = 'https://www.mediamarkt.de/public/setCookie/mc-gdpr-cookie/%7Cm%3D1%7Cf%3D1%7Cc%3D1%7Ca%3D1%7C'
        res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
        print(res.cookies.get_dict())
        cookies.update(res.cookies)
        print(cookies.get_dict())

        return cookies