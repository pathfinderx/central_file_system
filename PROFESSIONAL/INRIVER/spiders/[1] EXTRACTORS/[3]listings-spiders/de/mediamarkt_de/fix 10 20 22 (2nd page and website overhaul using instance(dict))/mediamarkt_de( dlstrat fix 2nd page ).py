import json, re, time
import random
import cloudscraper
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from request.unblocker import UnblockerSessionRequests


class MediamarktDeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        super().__init__(requester)
        self.requester = requester
        self.unblocker = UnblockerSessionRequests('de')
        self.cookies = None

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        self.scraper = cloudscraper.create_scraper(
            interpreter='native'
        )
        # self.scraper.proxies = self.requester.proxies

        random_agents = ['user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36']
        if not isinstance(headers, dict):
            headers = {
                'User-Agent': random.choice(random_agents)
            }

        if not isinstance(cookies, dict):
            cookies = None

        # if not self.cookies:
        #     self.cookies = self.get_cookies(headers=headers, timeout=timeout)

        raw_data = None

        try:
            # res = self.requester.get(url, timeout=timeout, headers=headers, cookies=self.cookies)
            res = self.unblocker.get(url, timeout=timeout, headers=None, cookies=None)

            if res.status_code not in [200, 201]: 
                res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=self.cookies)

            if res.status_code not in [200, 201]:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=self.cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                # for history in res.history:
                #     if history.status_code in [302, 301]:
                #         raw_data = None
                #         break
            # else:
            #     time.sleep(2)
            #     # Request using cloudscraper without proxy
            #     res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=self.cookies, proxies = self.requester.session.proxies)

            #     # Download successful
            #     if res.status_code == 200:
            #         raw_data = res.text

            #         for history in res.history:
            #             if history.status_code in [302, 301]:
            #                 raw_data = None
            #                 break
                
            #     if res.status_code == 403:
            #         res = self.scraper.get(url)
            #         if res.status_code == 200:
            #             raw_data = res.text

            #             for history in res.history:
            #                 if history.status_code in [302, 301]:
            #                     raw_data = None
            #                     break

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    def api_downloader(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        try:
            headers = self.get_api_headers()
            if headers:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=None)

                if res.status_code in [200, 201]:
                    return res.text
                else:
                    return None

        except Exception as e:
            print('Error at dlstrat: -> api_downloader() -> with message: ', e)

    def get_api_headers(self):
        headers = {
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9',
            'apollographql-client-name': 'pwa-client',
            'apollographql-client-version': '1.79.0',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'cookie': 'optid=7b71c4a6-c4b2-4371-a81d-45cd931b1a73; ts_id=08d3b896-366a-449f-b738-b948f78ea23d; t_fpd=true; __cfruid=bdeee92c78259c89bc9ff29adf6b3ad3057ceec5-1666244800; a=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Im8yc2lnbiJ9.eyJzdWIiOiIxYjE3MDQyYy0xNTU0LTRlZDYtYjk2My1jOGFmZWQzMjA4ZGIiLCJpc3MiOiJtbXNlIiwiaWF0IjoxNjY2MjQ0ODAwLCJleHAiOjE2Njc0NTQ0MDAsImF1ZCI6IndlYm1vYmlsZSIsInQiOiJ1IiwibyI6MTEyNX0.Li02Z221_5KEfpdhcRJPZ14PI8NSLZTZTF-7B8cphqTIINJEZXnM39gFx1EUgKvEhLaY2L_Nc2XIyn8DZzSN2S95zsu4GghQAIIuXWdxa0qT-eDi2dHo988I_XgzVGfVtA9NKXYp9npCqXNU_3AdjRfEIUZXDFTVRhhltuAvAli2AnYTFkR8b4Yfw_0PUqOG31H05ElXKiOLJ8nIgzD19mQifUuSNJ_kAhPYwAAxNNB-GZat25BR8MMniRlA4BFGm31UHk4Bum9p8Z2XnqBk2D8qOodqRp3uHtdW4ClGWAaNRpUHle-f3y1ZDJ2SgvKsIKOBKZxnHBHLGKNtq_sFFw; r=rYJJf5/2+xu8FwUEbSC3QHg2O9heiYqL3nY/GDKSKG8OX1p56uQzj+qzFJGx+w7I; s_id=9af8e17c-ea01-4db8-977c-c8025165d48d; MC_PS_SESSION_ID=9af8e17c-ea01-4db8-977c-c8025165d48d; p_id=9af8e17c-ea01-4db8-977c-c8025165d48d; MC_PS_USER_ID=9af8e17c-ea01-4db8-977c-c8025165d48d; pwaconsent=v:1.0~required:1&clf:1,cli:1,gfb:1,gtm:1,jot:1,ocx:1|comfort:1&baz:1,cne:1,fix:1,gfa:1,gfc:1,goa:1,gom:1,grc:1,grv:1,lib:1,lob:1,opt:1,orc:1,ore:1,sen:1,sis:1,spe:1,sst:1,swo:1,twi:1,usw:1,usz:1,yte:1|marketing:1&asm:1,cad:1,cri:1,eam:1,fab:1,fbn:1,fcm:1,gad:1,gam:1,gcm:1,gdv:1,gos:1,gse:1,gst:1,msb:1,omp:1,pin:1,ttd:1,twt:1|; _gcl_au=1.1.1515901683.1666244803; _gid=GA1.2.1455390401.1666244803; lux_uid=166624480292371051; _fbp=fb.1.1666244803522.299477138; _clck=1rwxg3v|1|f5v|0; _pin_unauth=dWlkPU5XWmtZV1U1TXpFdE5XTTVNQzAwTkdKbUxXSXpOMkV0TjJWaFkyTTBNREU1T1RJeQ; _msbps=98; _ga=GA1.2.08d3b896-366a-449f-b738-b948f78ea23d; _ga_WK3CRFV96J=GS1.1.1666244802.1.1.1666244811.51.0.0; _uetsid=93776180503a11ed801c27e0667ba79e; _uetvid=93777e40503a11eda3d9b9854afbab61; __cf_bm=oPgzWg.uDlPl1h6byOYsgGJPYwmX.akZD4GQbiCTggc-1666244935-0-Ad0yyJSOlirJ0DJXuCNzeQDZfgy0np4gmLjyc1MGIkwOpSrB79wb4GRv41Zoi+xq+43+atbGr017zt/mKgXhnUg5Rumpi+ZAN1llJ9Z/BcFj; _dc_gtm_UA-36693629-1=1',
            'pragma': 'no-cache',
            'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'x-cacheable': 'true',
            'x-flow-id': 'e8f8d465-0f37-42b1-a932-1cf1ca787122',
            'x-mms-country': 'DE',
            'x-mms-language': 'de',
            'x-mms-salesline': 'Media',
            'x-operation': 'CategoryV4'
        }

        return headers
    
    def get_cookies(self, headers, timeout):
        cookies = None
        url = 'https://www.mediamarkt.de/'

        res = self.unblocker.get(url, timeout=timeout, headers=headers)
        cookies = res.cookies
        print(cookies.get_dict())
        url = 'https://www.mediamarkt.de/public/setCookie/mc-gdpr-cookie/%7Cm%3D1%7Cf%3D1%7Cc%3D1%7Ca%3D1%7C'
        res = self.unblocker.get(url, timeout=timeout, headers=headers, cookies=cookies)
        print(res.cookies.get_dict())
        cookies.update(res.cookies)
        print(cookies.get_dict())

        return cookies