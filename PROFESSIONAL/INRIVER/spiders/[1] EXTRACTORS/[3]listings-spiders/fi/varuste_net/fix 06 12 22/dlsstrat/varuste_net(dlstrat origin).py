from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class VarusteNetDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'
            }

        raw_data = None

        try:
            if data:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            else:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            print(res.url)
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res
