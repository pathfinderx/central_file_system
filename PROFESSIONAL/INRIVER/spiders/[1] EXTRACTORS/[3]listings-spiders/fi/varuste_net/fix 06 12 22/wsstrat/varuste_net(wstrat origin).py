from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class VarusteNetDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'
            }

        raw_data = None

        try:
            if data:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            else:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            print(res.url)
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res
aimport logging
import re
import json
import urllib
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class VarusteNetWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.varuste.net'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self.next_page = None

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # Init request parameters
            timeout = 10
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }
            cookies = None

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            # Pagination logic 
            # https://varuste.net/c3369/kaikuluotaimet-gps-t-karttaplotterit 
            # https://varuste.net/c9719/py%C3%B6r%C3%A4ily-gps-t 


            while True:
                try:

                    if page == 0:
                        page += 1
                        next_url = url
                    else:
                        # TODO: Pagination not found
                        break

                    print("On page: %d" % page)
                    raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)

                    if not raw_data:
                        break

                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    if products:
                        total_products = sum([len(page_products) for page_products in products])

                        if max_results and total_products >= max_results:
                            break

                        start_rank = total_products + 1

                        page += 1
                        continue

                    else:
                        break

                except DownloadFailureException:
                    is_error = True
                    break

                except Exception:
                    raise

            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )

        except Exception:
            raise
        
    def _extract(self, raw_data, page, start_rank, max_results):
        soup = BeautifulSoup(raw_data, 'lxml')
        template = get_result_base_template()
        results = []
        
        tag = soup.select_one('div#tuotelistaus_div .grid')
        if tag:
            product_tags = tag.select('.item') #soup.select_one('div.tuotelistaus_class').select('.tuoterivi_div')
        
        if product_tags:
            temp_prod_tag = []
            for item in product_tags:
                if not item.select_one('[alt="Tarvikkeet"]'):
                    temp_prod_tag.append(item)
            if temp_prod_tag:
                product_tags = temp_prod_tag

        if not product_tags:
            return results

        for product_tag in product_tags:
            result = deepcopy(template)

            if max_results and max_results < start_rank :
                continue

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product_tag)

            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(product_tag)

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])

            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product_tag, result["price"]["value"], result["currency"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product_tag)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product_tag)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product_tag)

            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product_tag)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(product_tag)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(product_tag)

            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product_tag)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product_tag)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results

    def get_price(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('small')
            if tag:
                source = str(product_tag)
                value = tag.previous_sibling.previous_sibling.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR' # Defaulted
        return source, value

    def get_price_per_unit(self, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency 
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, product_tag, price_value, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency
        return source, description_value, promo_price_value, currency_value

    def get_url(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('a.ajaxlinkki')

            if tag:
                source = str(tag)
                url = tag.get('href')
                if 'http' not in url:
                    url = 'https://{}{}'.format('varuste.net', url)
                value = url

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_title(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('b a.ajaxlinkki') or product_tag.select_one('.rivi_paalinkki a.ajaxlinkki')

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        return source, value

    def get_ratings(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "100"  # For this site, max rating is 5 stars

        # try:
        #     tag = product_tag.select('span.star-icon')

        #     if tag:
        #         source = str(tag[-1])
        #         value = tag[-1].get('class')[-1]

        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_reviews(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = product_tag.select_one('span a.linkki.ajaxlinkki')

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            tag = product_tag.select_one('.varastossa')

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_image_urls(self, product_tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            src = None
            img_urls = []
            img_src = None
            tag = product_tag.select_one('a.ajaxlinkki img')

            if tag.has_attr('src'):
                src = str(tag)
                img_src = tag.get('src')

            if img_src:
                if 'http' not in img_src:
                    img_src = 'https://{}/{}'.format('varuste.net', img_src) 
                img_urls.append(img_src)

            if len(img_urls):
                source = src
                value = img_urls
            
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value