from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException

class VarusteNetDownloadStrategy(DownloadStrategy):
    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'cookie': 'maaid=ZA; valuutta=ZAR; shurikenscript=332; _ga=GA1.2.2007814667.1654998690; _gid=GA1.2.1840704354.1654998690; _fbp=fb.1.1654998694492.280814883',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'
        }

        raw_data = None

        try:
            if data:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)
            else:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            print(res.url)
            if res.status_code == 200:
                raw_data = res.text

        except Exception:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res
