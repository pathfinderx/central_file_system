import json
import logging
import re
from select import select

from bs4 import BeautifulSoup

from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class VarusteNetWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.varuste.net'

    def __init__(self, downloader):
        self.downloader = downloader
        self.logger = logging.getLogger(__name__)

    def execute(self, raw_data):
        result = get_result_base_template()
        soup = BeautifulSoup(raw_data, "lxml")

        # Extract price
        result["price"]["source"], result["price"]["value"] = self.get_price(soup)

        # Extract currency
        result["currency"]["source"], result["currency"]["value"] = self.get_currency(soup)

        # Extract availability
        result["availability"]["source"], result["availability"]["value"] = self.get_availability(soup)

        # Extract ratings (score and reviews)
        result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
            result["rating"]["score"]["max_value"], result["rating"]["reviews"]["source"], \
            result["rating"]["reviews"]["value"] = self.get_rating(soup)

        # Extract title
        result["title"]["source"], result["title"]["value"] = self.get_title(soup)

        # Extract brand
        result["brand"]["source"], result["brand"]["value"] = self.get_brand(soup)   

        # Extract description
        result["description"]["source"], result["description"]["value"] = self.get_description(soup)

        # Extract specifications - No table for specs
        result["specifications"]["source"], result["specifications"]["value"] = self.get_specifications(soup)

        # Extract image urls
        result["image_urls"]["source"], result["image_urls"]["value"] = self._get_image_urls(soup)

        # Extract video urls
        result["video_urls"]["source"], result["video_urls"]["value"] = self._get_video_urls(soup)

        # Extract delivery
        result["delivery"]["source"], result["delivery"]["value"] = self._get_delivery(soup)

        return result


    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="price")

            if tag:
                source = str(tag)
                value = tag.get('content')
            else:
                tag = soup.select_one('.hintateksti')
                if tag:
                    source = str(tag)
                    value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="priceCurrency")

            if tag:
                source = str(tag)
                value = tag["content"]

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="availability")

            if tag:
                source = str(tag)
                value = tag["href"].strip()
            else:
                tag = soup.select_one('[name="_tuoteid"]')
                if tag and tag.has_attr('value'):
                    temp_tag = tag.parent
                    id_name = '#vapaana{} .varastossa'.format(tag['value'])
                    tag = soup.select_one(id_name)
                    if tag:
                        source, value = str(tag), ' '.join(tag.stripped_strings)
                    else:
                        tag = temp_tag.find('input',type = 'submit').next_sibling.next_sibling
                        if tag.name == 'small':
                            source, value = str(tag), tag.get_text().lower()
                        else:
                            content_id = soup.find("input", {"name":"_tuoteid"})
                            if content_id:
                                selector_id = f"#vapaana{content_id['value']} > table > tr:nth-child(2)"
                                tag = soup.select_one(selector_id)
                                if tag:
                                    source, value = str(tag), tag.get_text().lower()

            if value == 'NOT_FOUND':
                tag = soup.select_one('input[name="ostoskoriin"]')
                if tag and tag.has_attr('disabled'):
                    value = 'http://schema.org/outofstock'
                    source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, value

    def get_rating(self, soup):
        score_source = Defaults.GENERIC_NOT_FOUND.value
        score_value = Defaults.GENERIC_NOT_FOUND.value
        score_max_value = "5"  # For this site, max rating is 5 stars
        reviews_source = Defaults.GENERIC_NOT_FOUND.value
        reviews_value = Defaults.GENERIC_NOT_FOUND.value

        try:
            # Reviews
            tag = soup.find(itemprop="ratingCount")

            if tag and tag.has_attr('content'):
                reviews_source = str(tag)
                content = tag["content"]

                if content:
                    reviews_value = content

            # Ratings
            tag = soup.find(itemprop="ratingValue")

            if tag and tag.has_attr('content'):
                score_source = str(tag)
                content = tag["content"]

                if content:
                    score_value = content

        except Exception as e:
            self.logger.exception(e)
            score_value = FailureMessages.RATING_EXTRACTION.value
            reviews_value = FailureMessages.REVIEWS_EXTRACTION.value

        return score_source, score_value, score_max_value, reviews_source, reviews_value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('span',{'itemprop':'model'})

            if tag:
                source = str(tag)
                value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find('span', {'itemprop': 'brand'})

            if tag:
                source = str(tag)
                value = tag.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.find(itemprop="description")

            if tag:
                source = str(tag)
                value = " ".join(tag.stripped_strings)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def _get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list

        try:
            tag = soup.select('.thumbnail_container a')

            if tag:
                source = str(tag)
                for anchor in tag:
                    # if anchor.has_attr('href'):
                    #     image_url = anchor.get('href')
                    #     if "varuste.net" not in image_url:
                    #         image_url = "https://varuste.net" + image_url
                    #     value.append(image_url)
                        
                    if anchor.img:
                        img = anchor.img
                        if img.has_attr('src'):
                            image_url = img.get('src')
                            if "varuste.net" not in image_url:
                                image_url = "https://varuste.net" + image_url
                                if "/160/" in image_url:
                                    image_url = image_url.replace("/160/", "/600/")
                            value.append(image_url)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    def _get_video_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # video_urls data type must be list

        try:
            src = None
            tags = soup.select('iframe')
            if tags:
                source = str(tags)
                for tag in tags:
                    src = tag.get("src")
                    value.append(src)

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.VIDEO_URLS_EXTRACTION.value

        return source, value

    def get_specifications(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = {}  # specification data type must be dict

        try:
            tag = soup.find(itemprop="description")

            if tag:
                row_tag = tag.select('table tbody tr')
                if row_tag:
                    source = str(tag)
                    for rows in row_tag:
                        columns = rows.select('td')
                        if columns and len(columns) == 2:
                            value[columns[0].text.strip()] = columns[1].text.strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.SPECIFICATIONS_EXTRACTION.value

        return source,value

    def _get_delivery(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = ''  # specification data type must be dict

        try:

            prod_id = ''
            tag = soup.select_one('#_omin_1')
            if tag and tag.has_attr('value'):
                prod_id = tag['value']
            else:
                tag = soup.find("input", {"name":"_tuoteid"})
                prod_id = tag['value']

            selector = '.ominaisuusvapaana_{}'.format(prod_id)

            tag = soup.select_one(selector)
            if tag:
                value = tag.get_text().strip()
                source = str(tag)

            if value == '':
                prod_id = ''
                tag = soup.select_one('[name="_tuoteid"]')
                if tag and tag.has_attr('value'):
                    prod_id = tag['value']
                    
                selector = '#vapaana{}'.format(prod_id)

                tag, temp_text = soup.select_one(selector), None

                if tag:
                    if 'Väri:' in ' '.join(tag.stripped_strings) or 'Colour:' in ' '.join(tag.stripped_strings): 
                        if tag.select_one('span:not([itemprop="mpn"]):not([itemprop="gtin"])'):
                            [i.decompose() for i in tag.select('table')]
                            [i.decompose() for i in tag.select('script')]
                            tag = tag.select_one('span:not([itemprop="mpn"]):not([itemprop="gtin"])')
                        else:
                            ref_number = None
                            selected_radio_tag = soup.select('.radio_wrapper input[checked=""]')
                            if selected_radio_tag:
                                radio_wrapper_tag = selected_radio_tag[-1].parent
                                ref_number_tag = radio_wrapper_tag.select_one('[style="font-size:smaller;"]')
                                if ref_number_tag:
                                    ref_number = ref_number_tag['class'][0]

                            if ref_number:
                                script_tag = soup.find('script',text = re.compile(ref_number))
                                if script_tag:
                                    rgx = re.search(r"\$\(\'."+ref_number+"\'\).html\(\"(.*)\"\);",script_tag.text.strip())
                                    if rgx:
                                        tag = BeautifulSoup(rgx.group(1).split(');$')[0],'lxml')
                            else:
                                tags = soup.select('.tuotesivu_content')
                                if tags:
                                    for item in tags:
                                        if item.contents[1].text == 'Saatavuus':
                                        
                                            if (re.search(r'Saatavuus', item.get_text()) and re.search(r'Varastotilanne ja saapuvat', item.get_text())):
                                                rgx = re.search(r'Saatavuus([\s\S]+?)Varastotilanne ja saapuvat', item.get_text())
                                                source, value = str(item), rgx.group(1).strip()
                                            break
                                    
                                if value == '':
                                    value = Defaults.GENERIC_NOT_FOUND.value
                    else:
                        temp_text = tag.text.strip()
                        [i.decompose() for i in tag.select('table')]
                        [i.decompose() for i in tag.select('script')]
                        [i.decompose() for i in tag.select('[style]:not([style="white-space: nowrap;"])')]
                        [i.decompose() for i in tag.select('table')]
                        [i.decompose() for i in tag.select('div strong')]
                        #[i.decompose() for i in tag.select('[style]')]
                        
                    if (temp_text and (tag and '.html("<span class=' in tag.get_text().strip() or tag.text.strip() == '')):
                        if '.html("<span class=' in temp_text:
                            if tag.text.strip() == '':
                                _text = temp_text
                            else:
                                _text = tag.get_text().strip()
                            value = _text.split('.html("<span class=')[1].split('\\">')[1].split('<\\')[0]
                            source = str(tag)
                        
                    if value == '':
                        tags = soup.select('.tuotesivu_content')
                        if tags:
                            for item in tags:
                                if item.contents[1].text in ['Saatavuus', 'Availability']:
                                    if str(item.contents[3]).strip() in ['Tuote ei ole myynnissä tällä hetkellä.', 'Tuotetta ei voi lisätä ostoskoriin, koska se ei ole myynnissä tällä hetkellä tai muusta syystä johtuen.']:
                                        source,value = str(item),str(item.contents[3]).strip()
                                        break
                                    else:
                                        matches = ['product could not be added to the shopping cart']
                                        if re.search('|'.join(matches), str(item.contents[3]).strip() ):
                                            source,value = str(item),str(item.contents[3]).strip()
                                            break
                                if (re.search(r'Saatavuus', item.get_text()) and re.search(r'Varastotilanne ja saapuvat', item.get_text())):
                                    rgx = re.search(r'Saatavuus([\s\S]+?)Varastotilanne ja saapuvat', item.get_text())
                                    source, value = str(item), rgx.group(1).strip()
                            
                    if value == '':
                        source, value = str(tag), ' '.join(tag.stripped_strings)

                # temporary bypass as request from validation team
                valid_delivery_container, final_delivery = ['Toimitusaika arviolta', 'Varastossa', 'Available in stock'], None
                if value:
                    for x in valid_delivery_container:
                        if (re.search(x, value)):
                            final_delivery = value
                            break

                    if (not final_delivery):
                        source = value = Defaults.GENERIC_NOT_FOUND.value

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DELIVERY_EXTRACTION.value

        return source,value        