from random import randint
from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class VarusteNetDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester

    def download(self, url, timeout=randint(60, 100), headers=None, cookies=None, data=None):
        assert isinstance(url, str)
        assert isinstance(timeout, int)

        if not isinstance(headers, dict):
            headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9',
                'sec-fetch-dest': 'document',
                'sec-fetch-mode': 'navigate',
                'sec-fetch-site': 'none',
                'sec-fetch-user': '?1',
                'upgrade-insecure-requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36 Avast/69.0.792.82'}

        if not isinstance(cookies, dict):
            cookies = None

        try:
            if not data:
                res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
            else:
                res = self.requester.post(url, timeout=timeout, headers=headers, cookies=cookies, data=data)

            status_code = res.status_code

            if status_code in [200, 201]:
                is_redirect = False

                for item in res.history:
                    if item.status_code in [302, 307]:
                        is_redirect = True
                        status_code = item.status_code
                        break

                if not is_redirect and 'meta property="og:type" content="product"' in res.text: #PDP checker
                    return res.text
                   
            raise DownloadFailureException('Download failed - Status Code {}'.format(status_code))

        except:
            raise DownloadFailureException('Download failed - Unhandled Exception')
