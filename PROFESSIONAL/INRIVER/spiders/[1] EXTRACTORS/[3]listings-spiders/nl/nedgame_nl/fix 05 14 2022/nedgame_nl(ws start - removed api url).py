import logging, urllib, re, json
from copy import deepcopy
from bs4 import BeautifulSoup
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template


class NedgameNlWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.nedgame.nl'

    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)

        # NOTE: in this context, either _method/variable or __method/variable are considered private
        # its just a matter of programming style since im used to
        # _ protected method/variable
        # __ private method/variable

        # NOTE: we use one variable to store the instance state
        # the reason for this one is just a matter of programming style since
        # prior implementations does not really utilize instance state and I
        # dont want also to clutter the object w many instance variables vs prior implementations
        self.__state = {}

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        try:
            self.__state = {}
            
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)

            if max_results:
                assert isinstance(max_results, int)

            # initialize some values on the instance state
            self.__state.update({
                'url': url, 'page': page, 'start_rank': start_rank,
                'postal_code': postal_code, 'max_results': max_results, 'request_params': request_params,
                'headers': None, 'timeout': 10, 'cookies': None
            })

            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    self.__state['timeout'] = request_params['timeout']

                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    self.__state['headers'] = request_params['headers']

                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    self.__state['cookies'] = request_params['cookies']

            # current_product_count - number of product per iteration/page
            # total_product_count - total number of products so far
            # TOTAL_PRODUCT_COUNT - total number of products found in a certain category. Some websites provides this and some not. This is optional
            self.__state.update({
                'last_response': None, 'is_error': False, 'products': [],
                'current_product_count': 0, 'total_product_count': 0, 'TOTAL_PRODUCT_COUNT': 0,
            })

            has_url_params = self.__has_url_params(self.__state['url'])

            # variable that holds query params for page 2 and so on...
            next_page_params = {}

            self.query_params = {
                'url': url,
                'outputtype': 'results'
            }

            # api_url = 'https://www.nedgame.nl/include/ajax/ajax.Zoeken.php?'+urllib.parse.urlencode(self.query_params)
            # url = api_url

            if self.__state['page'] < 1: self.__state['page'] = 1 # starts with 1
            while True:
                try:

                    print("On page: %d" % self.__state['page'])

                    if self.__state['page'] == 1:
                        url = url
                    else:
                        url =next_page_params

                    raw_data, self.__state['last_response'] = self.download_strategy.download(url)
                    
                    if not raw_data: break
                    
                    self.__state['bs'] = BeautifulSoup(raw_data, 'lxml')
                        
                    products = self.get_products(self.__state['bs'])

                    # truncate the products according to the max_results
                    products = self.truncate_products(products)
                    products = self._extract(products, page=self.__state['page'], start_rank=self.__state['start_rank'])

                    self.__state['products'].append(products)
                    self.__state['current_product_count'] = len(products)
                    self.__state['total_product_count'] += self.__state['current_product_count']

                    if not self.has_next():
                        break

                    # get the next page params
                    next_page_params = self.__get_next_page_params(self.__state['bs'])

                    self.__state['page'] += 1
                    self.__state['start_rank'] += self.__state['current_product_count']
                except DownloadFailureException:
                    self.__state['is_error'] = True
                    break
                except Exception:
                    raise

            return dict(
                is_error=self.__state['is_error'],
                last_page=self.__state['page'],
                last_response=self.__state['last_response'],
                products=self.__state['products']
            )

        except Exception:
            raise


    def get_products(self, tag):
        products = []
        products = tag.select('table.productTable tbody tr') or tag.select('div.productOverzicht > div.productShopHeader')
        if products:
            return products

    def get_price(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.currentprice')
            if tag:
                value = tag.get_text().strip()
                source = str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value

        return source, value

    def get_currency(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'
        return source, value

    def get_price_per_unit(self, tag):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default currency
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, soup, price_value):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = 'EUR'  # Default currency

        try:
            tag = soup.select_one('div.oldprice')
            if tag and tag.text:
                promo_price_value = price_value
                description_value = 'From ' + tag.get_text().strip()
                source = str(tag)

        except Exception as e:
            self.logger.exception(e)
            description_value = FailureMessages.PROMO_EXTRACTION.value

        return source, description_value, promo_price_value, currency_value

    def get_url(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('div.titlewrapper a')
            if tag:
                value = tag.get('href')
                source = str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value

        return source, value

    def get_brand(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.productinfo')
            if tag:
                value = tag.text.strip().split('Uitgever:')[-1]
                source = str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value

        return source, value

    def get_title(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('td.title h3')
            tag2 = soup.select_one('a.productTitleLink > h3')

            if tag:
                value = tag.text.strip().split('Uitgever:')[-1]
                source = str(tag)
            elif tag2:
                source, value = str(tag), tag2.get_text().strip()

        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value

        return source, value

    def get_description(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        try:
            tag = soup.select_one('span.productinfo')
            if tag:
                value = tag.text.strip()
                source = str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value

        return source, value

    def get_rating_score(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = '5' # default max stars

        #  Not Available
        # try:
        #     if 'customerReviews' in data['product']:
        #         value = str(data['product']['customerReviews']['averageRatingAsIconFont'])
        #         source = str(data)
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.RATING_EXTRACTION.value

        return source, value, max_value

    def get_rating_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Not Available
        # try:
        #     if 'customerReviews' in data['product']:
        #         value = str(data['product']['customerReviews']['numberOfReviews'])
        #         source = str(data)
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.REVIEWS_EXTRACTION.value

        return source, value

    def get_availability(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value

        # Set availability to 1 as per validator

        # try:
        #     tag = soup.select_one('div.delivertime')
        #     if tag:
        #         value = tag.get_text().strip()
        #         source = str(tag)
        # except Exception as e:
        #     self.logger.exception(e)
        #     value = FailureMessages.AVAILABILITY_EXTRACTION.value

        return source, json.dumps(True)

    def get_image_urls(self, soup):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        try:
            tag = soup.select_one('td.image img') or soup.select_one('img')
            if tag and tag.has_attr('src'):
                value.append(tag.get('src'))
                source = str(tag)
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value

        return source, value

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################


    #################################################################################
    # NOTE: NOT PART OF THE INTERFACE/ABC BUT NECESSARY. CONSIDERED AS HELPER METHODS
    #################################################################################

    def __has_url_params(self, url):
        parsed_url = urllib.parse.urlparse(url)
        if parsed_url.query:
            return True
        return False

    def __get_next_page_params(self, tag):
        next_tag = tag.select_one('div.dataTables_paginate.paging_full_numbers span:last-child a')
        if '»' in next_tag and self.__state['page'] == 1:
            self.query_params['url'] += '&pagina=2' # temporarily defaulted -- 24 in each page
            return 'https://www.nedgame.nl/include/ajax/ajax.Zoeken.php?'+urllib.parse.urlencode(self.query_params)
            # return next_tag.get('href')
        else:
            return None

    def truncate_products(self, products):
        if self.__state['max_results'] is None:
            return products
            
        remaining_length = self.__state['max_results'] - self.__state['total_product_count']
        # truncate products by slicing
        return products[0:remaining_length]

    def has_next(self):
        if self.__state['max_results'] is not None and self.__state['total_product_count'] >= self.__state['max_results']:
            return False

        if self.__state['current_product_count'] <= 0:
            return False

        next_pagination_tag = self.__state['bs'].select_one('div.dataTables_paginate.paging_full_numbers span:last-child a')
        if next_pagination_tag and '»' not in next_pagination_tag.text:
            return False
        elif not next_pagination_tag:
            return False

        return True

    def _extract(self, products, page, start_rank):
        
        template = get_result_base_template()
        results = []

        for product in products:
            result = deepcopy(template)

            result["page_number"] = str(page)
            result["rank"] = str(start_rank)

            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(product)

            # # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(self.__state['bs'])

            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(product)

            # # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(product, result["price"]["value"])

            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(product)

            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(product)

            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(product)

            # # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(product)

            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_rating_score(product)

            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_rating_reviews(product)

            # # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(product)

            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(product)

            start_rank += 1

            # Append to results list
            results.append(result)

        return results
