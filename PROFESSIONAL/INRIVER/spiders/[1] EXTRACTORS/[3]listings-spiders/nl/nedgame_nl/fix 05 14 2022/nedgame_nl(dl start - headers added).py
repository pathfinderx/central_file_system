from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException


class NedgameNlDownloadStrategy(DownloadStrategy):

    ###################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC STARTS HERE
    ###################################################

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        
        if not isinstance(headers, dict):
            headers = None
        if not isinstance(cookies, dict):
            cookies = None

        raw_data = None

        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            # 'accept-encoding': 'gzip, deflate, br',
            # 'accept-language': 'en-US,en;q=0.9',
            'cache-control': 'no-cache',
            'cookie': '__cflb=02DiuEaHK5vYCFfM817tZv9xUyffZsATVqwEG7v585j2L; PHPSESSID=c474c01jcvibaq3jc8usa48f8h; _gid=GA1.2.612704678.1652499272; _gaexp=GAX1.2.Z70C8NnTRW-xuqSfbQXiHQ.19216.1; veuid=87a9a2f3-9470-4668-8388-e8c3e6310c73_45f04bbd18af8775643c6fb414b77631f1b439d7; _clck=1c7yzbz|1|f1g|0; _ga=GA1.1.795317135.1652499272; _ga_TQVTWKJ642=GS1.1.1652499272.1.1.1652499301.31; _clsk=y0hghk|1652501325420|1|1|d.clarity.ms/collect; ClosedLoginSimulator=1652501589160',
            'pragma': 'no-cache',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="101", "Google Chrome";v="101"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'none',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36'
        }
        
        try:
            res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)

            # Download successful
            if res.status_code == 200:
                raw_data = res.text

                for history in res.history:
                    if history.status_code in [302, 301]:
                        raw_data = None
                        break

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')

        return raw_data, res

    #################################################
    # NOTE: IMPLEMENTATION OF INTERFACE/ABC ENDS HERE
    #################################################
