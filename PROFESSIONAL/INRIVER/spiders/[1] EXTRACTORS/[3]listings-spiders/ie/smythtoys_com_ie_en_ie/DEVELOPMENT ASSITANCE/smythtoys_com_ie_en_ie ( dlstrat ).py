from strategies.download.base import DownloadStrategy
from strategies.download.exceptions import DownloadFailureException
from urllib.parse import urlencode, quote
from request.unblocker import UnblockerSessionRequests
import json, re
import cloudscraper, requests

class SmythstoysComIeEnIeDownloadStrategy(DownloadStrategy):
    def __init__(self, requester):
        self.requester = requester
        self.scraper = cloudscraper.create_scraper(sess=requester.session)
        self.unblocker = UnblockerSessionRequests('ie')
        self.public_cookies = None

    def download(self, url, postal_code=None, timeout=10, headers=None, cookies=None, data=None):
        if not isinstance(headers, dict):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            }

        # random_cookies = [
        #     'visid_incap_2483049=Gpfpf7EvTaiG7z3CPCYm3nss0mMAAAAAQUIPAAAAAACZswYylMDD3YhOtZ3MdEHW; incap_ses_1350_2483049=acN2Tev3ajb49/Wdciq8Em282GMAAAAA1BSI1b8sjipBEB5Cf/v7hQ==; nlbi_2483049=NiiwWfUObmOQT9PGYsb6kQAAAABLOhiROcWZQTHoCoicqzGP; reese84=3:m/RHP66zNqACGmriyfryUA==:ZpjNlIXSigPADuUqf9X+Wb0sItHVuzivxS+g2iwSN7ibhBHtjsyEFsOaGreNmeNrUGs56v7FytHHyDKYS13RNmqUxXucBkvPv+WOjsvkyo87zOiBJ0aAfeXKiA2NM34xbthJIAUXUkDLwfpkijClWYdOpMkR5oj9tlwbhHqGTU9QXqQivJf9/uPyJa9A0WsBkUCpO8dmka9p1GPpvL92NSqAE8gW9j9hT9T+/+TT2smKaC4W0iLOgVgWt43alXyOwwcgVEKs3YfUhkUPcZ+bW0Kbu38hcfC83CrvVtUWCquSD0tWE34Ellv/Ggqwjiktd/uGi3yitsyt/s6NiFWUJu/NmM1wQfT3TmMpIprIoIW33NfkYptnggvxPkW4R86ZL5gWvrzEO5r+g1WmHxwHgqausEhOCEYzLFBYeD1VVfxT3dd0xvB76nS58ay3GF8u10zjIdRxog3qcQoUDTPjCg==:FTK6Q1dzBw69VpwYjXmaFafu79Wvqu88a26VlhaKcV0=',
        #     'visid_incap_2483049=Gpfpf7EvTaiG7z3CPCYm3nss0mMAAAAAQUIPAAAAAACZswYylMDD3YhOtZ3MdEHW; incap_ses_1636_2483049=zTw0an/tqmQ1UdSsbj20FgKd2WMAAAAAPwS6QDkXUaLlRCMsJa8N+A==; reese84=3:BubvMzijsnXEmAgTVIHO+Q==:V1cavTEhKoVNRtj0qp3re7uLTYlcsk5fYSmSHPDPvryD0sA2dzqTs38GoBuozkDiYBsVcLKbMRIuRDGa8LWToVNK8P3QAG0w3Yg4VBXemDNoSvgNP5yHh52feq+iF0fXJBXQvtFsrNqoU+8qVjfO9sa1bVKTTyNkJ737o3gORqPpv9DaYJqf9FDxEhgw18uAJ0L9LJzDP/7rfLFCRB+2Wrd0bdH4ajDr/oXKWeAf5m5qNxlDRN0nYR/HBR+aM0kiDvdHODlRZ8nYZ0JJGTuDnVXgpfsOkG00eH6ZeEfh0KaOdZXDNQ2pwC/FBkjIkYH/pekj868UwsYUWQaJ+7RQBCCg7sygC6i/tWgA2AA0b6KOCA5qXks+s+x8hBzPFOPQaNv9et5q9kqa7FMhYBK5v/yNuou/lkcXE6rVWm6zuBDQ5HNpUWa3hb4qDsM5gobrs5bdOvtTJOEs8VaHbXj0pQ==:bX+6zwWETbEBmPSe16E3UFneXT8265nUidcDfuNZlVM=; nlbi_2483049=SMbFQ9aBCGfwdxtAYsb6kQAAAAADoXbTtpLidqcFm79ap/IJ', 
        # ]
        # headers['cookie'] = random.choice(random_cookies)
        # self.unblocker.session.headers = headers

        if not isinstance(cookies, dict):
            # cookies = self._get_cookies()
            cookies = None
        raw_data = None

        try:
            res = self.unblocker.get(url, timeout=10000, headers=None, cookies=None)
            # Download successful
            if res.status_code in [200, 201]:
                cookies_res = requests.get(url, timeout=timeout)

                if cookies_res.status_code in [200, 201, 403, 501]:
                    self.public_cookies = cookies_res.cookies.get_dict()

                raw_data = res.text

        except Exception as e:
            raise DownloadFailureException('Download failed - Unhandled exception')
        return raw_data, res
        
    def get_api_data(self, url, timeout=10, headers=None, cookies=None, page=1):
        try:
            api_url = f'{url}/load-more?q=%3AieBestsellerRating%3AproductVisible%3Atrue&page={page}'
            static_headers = self.get_static_headers()
            _json = None

            res = self.requester.get(api_url, timeout=10000, headers=static_headers)    

            if res.status_code in [200, 201]:
                _json = json.loads(res.text)
                return _json if _json else None

        except Exception as e:
            print('Error at: dlstrat -> get_api_data() -> with message: ', e)

    def _get_cookies(self):
        cookie = None
        headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
            }
        response = self.unblocker.get('https://www.smythstoys.com/ie/en-ie/', timeout=10000, headers=headers, cookies=cookie)
        cookie = response.cookies.get_dict()
        return cookie

    def get_static_headers(self) -> dict:
        try:
            if self.public_cookies:
                headers = {
                    'accept': '*/*',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'en-US,en;q=0.9,fil;q=0.8,es;q=0.7',
                    'cache-control': 'no-cache',
                    'content-type': 'application/json; charset=utf-8',
                    'cookie': 'visid_incap_2483049=0lTn8GNSQhSNgcy9FBB2ZX6x2WMAAAAAQUIPAAAAAADxa9DceGkwFwYWDOzkXeJx; nlbi_2483049=SAaxANGVwR8PMkI5Ysb6kQAAAADVH0/2JrF9lwNklXzM4AnT; incap_ses_1380_2483049=FpyuaPlbGDb/2ReSTr8mE3my2WMAAAAALf6eih3B9JMNenpSms/W8g==; smyths_gtm_CYBERSOURCE=false; smyths_gtm_KLARNA=false; smyths_gtm_RAKUTEN=false; smyths_gtm_FLIXMEDIA=false; smyths_gtm_WEBCOLLAGE=false; smyths_gtm_FRESHCHAT=false; smyths_gtm_YOUTUBE=false; smyths_gtm_SITEVISIT=false; smyths_gtm_LOCATION=false; smyths_gtm_ALGONOMY=false; uk-anonymous-consents=%5B%7B%22templateCode%22%3A%22Analytics%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Essential%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Functional%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Targeting%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%5D; uk_rcs=eF5jYSlN9jA2T00yMzax0E0yTUvVNTG0SNZNTTZN0rUwMTO0MLI0TTNLTOLKLSvJTBEwtDQz1AVCAJ16Dnk; _gcl_au=1.1.1153484569.1675211422; _gid=GA1.2.1982156503.1675211423; _fbp=fb.1.1675211424087.321307026; uid=ie; _ga_EGQRHEPF85=GS1.1.1675211422.1.0.1675211424.58.0.0; smyths_gtm_SYNDIGO=false; ie-anonymous-consents=%5B%7B%22templateCode%22%3A%22Analytics%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Essential%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Functional%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%2C%7B%22templateCode%22%3A%22Targeting%22%2C%22templateVersion%22%3A0%2C%22consentState%22%3A%22GIVEN%22%7D%5D; smyths_gtm_HOTJAR=true; smyths_gtm_GA=true; smyths_gtm_BAZAARVOICE=true; smyths_gtm_LEGOZOOVU=true; smyths_gtm_GOOGLEADWORDS=true; smyths_gtm_FACEBOOK=true; _hjSessionUser_152733=eyJpZCI6ImIzYzdiMjYzLTI4MmYtNTJiNy04Y2I5LTIzNzE1Y2MzMmM3ZSIsImNyZWF0ZWQiOjE2NzUyMTE0MzAxNjQsImV4aXN0aW5nIjp0cnVlfQ==; incap_ses_1387_2483049=VPOMJT266FwpsXUfsJ0/E6my2WMAAAAA65UVIPO0lJf5OMP92GwDEg==; incap_ses_1138_2483049=WcsZJnZQfxuzgB4Kyv3KD5672WMAAAAAUyNgHu+w0RvsyWDgpKgBAQ==; incap_ses_1635_2483049=AWT5JtpJVjFAzqwG8K+wFlXU2WMAAAAAwfTaS/9aE/oUcB8vwevxvA==; incap_ses_197_2483049=aV7SCw8MuSvaLgutuuK7ArLV2WMAAAAAKPsM3ByuK/s5gTRYzHe3uw==; incap_ses_374_2483049=5eP1PceRSSaR497407cwBQbh2WMAAAAAfZB9Iz/CjmdY0ll+pSXpRA==; incap_ses_1183_2483049=oynPNxe5GX49R0FwSdxqENPh2WMAAAAAqN253u9Wkeh8DJrCK9NW3w==; incap_ses_1581_2483049=5r13OxMrGE0C6FMoY9fwFffh2WMAAAAA/LiNc+UxBlh5Wtm9mDLHhA==; incap_ses_457_2483049=PVMtJxaUXE6dB7H8/JdXBk/k2WMAAAAA3pvLYqwXWdjh8biwaGpDzA==; incap_ses_1583_2483049=/NwDJ2M4al4qAHDCafL3Fdfq2WMAAAAAQlvX5iaqAgtrjlhV6sQBYQ==; incap_ses_9196_2483049=e+/7cZCcWC47UbaxWcGef2bx2WMAAAAA66LxCzFQynIvL+tk6yuh3w==; incap_ses_1382_2483049=euriAgJ+c3w1KUA/MtotEzP02WMAAAAA4QfKmihfWjK6cWgVleNd6w==; incap_ses_1379_2483049=z3uYQYjBPDl3/u1NvTEjEz/62WMAAAAAihfZtHaysm4C3HVZIFTmhQ==; ie_rcs=eF5j4cotK8lMETC0NDPUBUKW0mQPI8vEJAvjVFPdFDNDE10T05QUXQvzpDRdC6AaCyNLM6MUA1MAkXMN7g; incap_ses_9210_2483049=8f8KL0oGKA0++7m0Rn7QfzH92WMAAAAAiz9i7fBn9/CW17tUEgorPw==; incap_ses_1377_2483049=ttWyWoldJ2W0TLBbuxYcE2MA2mMAAAAAMMBU9QG0mT7EtK0HB7KsOg==; incap_ses_867_2483049=kmJ8C73Tsg6rPxPmyTQIDDIH2mMAAAAAN+/uPn3sSiTcNiiOszeiVw==; reese84=3:ZoYBeT+25nKW5lXF7GuAKw==:8NSmrPtgC68RM36dUvzxDvw8VKfoCDp1NCETBrz4M8e4wHx4zf260HsgxR6WUYTxoTfaAEEcnGTtCPrhVAYfROncCfy/IpCDzVipHxswADPNOLK+dlF7ZKQ8vcHf43JlXJGlEM3QfoU9ny3kIRxWlMVZUgWuRpKM0PTs85ASrbu1bg7+IkSrnh48S+ldjwunVH8AJ3PYAV7ojAGAE86gmiumw+A3yarspBN5JUJDr0macHn3nLIFDqxIrcv4auR0UzmG3pdzLN5YsE/POaF7huxfwLV0lX/XrNiXFIm24/m0HOBqXh0OumMjghKKoCQPvXiaKRBt/xYT6UwtcJm/Q//6Pnjmmc6HIqiMXFQiLD1jeEt2dl5wXzwp343WHXNf9QVakCq11bsYBuJwhI6hf/TPNSLUAhZ1mfgCexl+wXvGI1usQKDz9qHX+sK6IlxWv3zsZC26XDIh0aEQub5PUg==:FDuydcZ3HULJHiPeFFDazl1aDqw+CWLEzoFeQr4FfwI=; JSESSIONID=0FA1C1E6F0690CC4594CB9E723E37634.znt5; GCLB=CM_lq6DwldWYFg; incap_ses_453_2483049=wYQRJOuUuXz9nD/L72FJBr0J2mMAAAAAnMYNyFPZQwvHlHhB/KVkRw==; locationCookie=_; nlbi_2483049_2147483392=7V4ufVnN3CE/fhbvYsb6kQAAAAA5YNy2ZSr21WFpItpGFSs4; _hjOptOut=false; smyths_gtm_GOOGLEOPTIMIZE=true; smyths_gtm_GTM=false; _ga=GA1.2.1116215645.1675211422; _dc_gtm_UA-74244151-1=1; _gat_UA-74244151-1=1; _hjIncludedInSessionSample=0; _hjSession_152733=eyJpZCI6IjBmZDczN2IyLTNjZGItNDhhOC05MDFkLTIxNjJkYmMyY2ZmZiIsImNyZWF0ZWQiOjE2NzUyMzM3MzMzNjYsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _ga_XBLRRGPHR4=GS1.1.1675233726.5.1.1675233751.35.0.0',
                    'pragma': 'no-cache',
                    'referer': 'https://www.smythstoys.com/ie/en-ie/brand/pok%C3%A9mon/c/pokemon-brand',
                    'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'sec-fetch-dest': 'empty',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-site': 'same-origin',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
                    'x-requested-with': 'XMLHttpRequest'
                }

                for x in self.public_cookies:
                    print(re.sub(f"{x}=([\s\S]+?);", f"{x}={self.public_cookies[x]};", headers.get('cookie')))

                for x in self.public_cookies:
                    print(f"{x}={self.public_cookies[x]};")
                
                return headers

        except Exception as e:
            print('Error at: get_static_headers() -> with message: ', e)