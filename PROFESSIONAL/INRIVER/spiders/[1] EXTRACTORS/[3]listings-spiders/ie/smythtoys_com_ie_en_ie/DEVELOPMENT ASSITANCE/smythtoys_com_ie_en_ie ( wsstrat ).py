import logging
import re
import json
from copy import deepcopy
from bs4 import BeautifulSoup, Tag
from strategies.download.exceptions import DownloadFailureException
from strategies.website.base import WebsiteStrategy
from strategies.website.constants import Defaults, FailureMessages
from strategies.website.templates import get_result_base_template
from urllib.parse import urlencode, quote

class SmythstoysComIeEnIeWebsiteStrategy(WebsiteStrategy):
    WEBSITE = 'www.smythstoys.com/ie/en-ie'
    def __init__(self, download_strategy):
        super().__init__(download_strategy=download_strategy)
        self.logger = logging.getLogger(__name__)
        self._page = 0
        self.total_products = 0
        self.pagination_url = list() 
        self.pagination_counter = -1
        self.preserved_url = None
        self.isPaginationApi = False

    def execute(self, url, page=0, start_rank=1, postal_code=None, max_results=None, request_params=None):
        
        try:
            assert isinstance(url, str)
            assert isinstance(page, int)
            assert isinstance(start_rank, int)
            self.preserved_url = url

            if max_results:
                assert isinstance(max_results, int)
            # Init request parameters
            timeout = 10
            headers = {
                'accept': '*/*',
                'accept-encoding': 'gzip, deflate',
                'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                'cookie': 'visid_incap_2483049=Gpfpf7EvTaiG7z3CPCYm3nss0mMAAAAAQUIPAAAAAACZswYylMDD3YhOtZ3MdEHW; incap_ses_1636_2483049=zTw0an/tqmQ1UdSsbj20FgKd2WMAAAAAPwS6QDkXUaLlRCMsJa8N+A==; reese84=3:BubvMzijsnXEmAgTVIHO+Q==:V1cavTEhKoVNRtj0qp3re7uLTYlcsk5fYSmSHPDPvryD0sA2dzqTs38GoBuozkDiYBsVcLKbMRIuRDGa8LWToVNK8P3QAG0w3Yg4VBXemDNoSvgNP5yHh52feq+iF0fXJBXQvtFsrNqoU+8qVjfO9sa1bVKTTyNkJ737o3gORqPpv9DaYJqf9FDxEhgw18uAJ0L9LJzDP/7rfLFCRB+2Wrd0bdH4ajDr/oXKWeAf5m5qNxlDRN0nYR/HBR+aM0kiDvdHODlRZ8nYZ0JJGTuDnVXgpfsOkG00eH6ZeEfh0KaOdZXDNQ2pwC/FBkjIkYH/pekj868UwsYUWQaJ+7RQBCCg7sygC6i/tWgA2AA0b6KOCA5qXks+s+x8hBzPFOPQaNv9et5q9kqa7FMhYBK5v/yNuou/lkcXE6rVWm6zuBDQ5HNpUWa3hb4qDsM5gobrs5bdOvtTJOEs8VaHbXj0pQ==:bX+6zwWETbEBmPSe16E3UFneXT8265nUidcDfuNZlVM=; nlbi_2483049=SMbFQ9aBCGfwdxtAYsb6kQAAAAADoXbTtpLidqcFm79ap/IJ',
                'referer': 'https://www.smythstoys.com/ie/en-ie/',
                'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                # 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1290.1 Safari/537.13',
                'x-requested-with': 'XMLHttpRequest',
            }
            cookies = None
            if request_params and isinstance(request_params, dict):
                if 'timeout' in request_params and isinstance(request_params['timeout'], int):
                    timeout = request_params['timeout']
                if 'headers' in request_params and isinstance(request_params['headers'], dict):
                    headers = request_params['headers']
                if 'cookies' in request_params and isinstance(request_params['cookies'], dict):
                    cookies = request_params['cookies']

            last_response = None
            is_error = False
            products = list()

            while True:
                try:
                    """ url alternator """
                    if page == 0:
                        page += 1
                        next_url = url

                    else:
                        next_url = next_url

                    """ Downloader """
                    if not self.isPaginationApi:
                        print("On page: %d" % page)
                        print(next_url)
                        raw_data, last_response = self.download_strategy.download(next_url, postal_code=postal_code,
                                                                              timeout=timeout, headers=headers,
                                                                              cookies=cookies, data=None)
                    elif self.isPaginationApi:
                        raw_data = self.download_strategy.get_api_data(next_url, timeout=10, headers=None, cookies=None, page=self.pagination_counter+1)

                    if not raw_data:
                        break
                    
                    """ Product Extraction """
                    product = self._extract(raw_data, page=page, start_rank=start_rank, max_results=max_results)
                    if product:
                        products.append(product)
                    else:
                        break

                    """ Pagination loader and breaker"""
                    if not self.pagination_url: # to prevent infinite extraction of url
                        self.set_next_urls(raw_data) # load the urls
                    
                    self.pagination_counter += 1
                    
                    if (self.pagination_counter <= len(self.pagination_url) - 1):
                        next_url = self.pagination_url[self.pagination_counter]
                    elif (self.pagination_counter >= len(self.pagination_url) - 1):
                        break

                    if not next_url:
                        break

                    if products:
                        self.total_products = sum([len(page_products) for page_products in products])
                        if max_results and self.total_products >= max_results:
                            break
                        start_rank = self.total_products + 1
                        continue
                    else:
                        break
                
                except DownloadFailureException:
                    is_error = True
                    break
                
                except Exception:
                    raise
            return dict(
                is_error=is_error,
                last_page=page,
                last_response=last_response,
                products=products
            )
        
        except Exception:
            raise
    def has_data(self, raw_data):
        try:
            # page count start with zero
            # page 2 from pdp and so on are in json value
            if self._page >= 1:
                _json = json.loads(raw_data)
                raw_data = _json['htmlContent']
            soup = BeautifulSoup(raw_data, 'lxml')
            _load_btn = soup.select('.loadMoreProductsOnPage')
            if _load_btn:
                return True
            return False
        except:
            pass
        return False

    def has_next_page(self, raw_data):
        try:
            # page count start with zero
            # page 2 from pdp and so on are in json value
            if self._page >= 1:
                _json = json.loads(raw_data)
                raw_data = _json['htmlContent']
            soup = BeautifulSoup(raw_data, 'lxml')
            products = soup.select('.col-lg-4.col-md-4.col-sm-6.col-xs-6.product-padding')
            if products:
                return True
            return False
        except:
            pass
        return False

    def set_next_urls(self, raw_data):
        try:
            soup = BeautifulSoup(raw_data, 'lxml')
            limit_indicator = soup.select_one('div.margn_bot_15.hidden-xs')
            _rgx, limit = None, False
            PAGE_LIMIT = 60 # static limit indicator per page

            if limit_indicator:
                _rgx = re.search(r'[\d]{1,}', limit_indicator.get_text(strip=True))

            if (limit_indicator and _rgx):
                qoutient_limit = int(_rgx.group(0)) / PAGE_LIMIT
                limit = round(qoutient_limit)

            # url generation
            if (limit and limit > 1):
                limit = limit - 1
                for x in range(limit):
                    self.pagination_url.append(self.preserved_url)

            if self.pagination_url: 
                self.isPaginationApi = True # set next page as api mode

        except Exception as e:
            print('Error at: wsstrat -> set_next_urls() -> with message: ', e) 
    
    def get_products(self, raw_data) -> list:
        try:
            if isinstance(raw_data, dict):
                soup, products = None, None
                if (raw_data and 'htmlContent' in raw_data):
                    soup = BeautifulSoup (raw_data['htmlContent'], 'lxml')
                    products = soup.select('.col-lg-4.col-md-4.col-sm-6.col-xs-6.product-padding')

                return products if products else []

            if isinstance(raw_data, str):
                soup = BeautifulSoup(raw_data, 'lxml')
                products = soup.select('.col-lg-4.col-md-4.col-sm-6.col-xs-6.product-padding')
                
                return products if products else []
            return []

        except Exception as e:
            print('Error at: wsstrat -> get_products() -> with message: ', e)

    def _extract(self, raw_data, page, start_rank, max_results):
        template = get_result_base_template()
        results = []
        
        products = self.get_products(raw_data)

        if not products:
            print('has no products')
            return results

        for data in products:
            result = deepcopy(template)
            if max_results and max_results < start_rank :
                continue
            result["page_number"] = str(page)
            result["rank"] = str(start_rank)
            # Extract price
            result["price"]["source"], result["price"]["value"] = self.get_price(data)
            # Extract currency
            result["currency"]["source"], result["currency"]["value"] = self.get_currency(data)
            # Extract price per unit
            result["price_per_unit"]['unit']["source"], result["price_per_unit"]['unit']["value"], \
                result["price_per_unit"]['price']["source"], result["price_per_unit"]['price']["value"], \
                result["price_per_unit"]['currency']["source"], \
                result["price_per_unit"]['currency']["value"] = self.get_price_per_unit(result["currency"]["value"])
            # Extract promo
            result["promo"]["source"], result["promo"]['description']["value"], \
                result["promo"]['price']["value"], \
                result["promo"]['currency']["value"] = self.get_promo(data, result["price"]["value"], result["currency"]["value"])
            # Extract URL
            result["url"]["source"], result["url"]["value"] = self.get_url(data)
            # Extract brand
            result["brand"]["source"], result["brand"]["value"] = self.get_brand(data,)
            # Extract title
            result["title"]["source"], result["title"]["value"] = self.get_title(data)
            # Extract description
            result["description"]["source"], result["description"]["value"] = self.get_description(data)
            # Extract rating - score
            result["rating"]["score"]["source"], result["rating"]["score"]["value"], \
                result["rating"]["score"]["max_value"] = self.get_ratings(data)
            # Extract rating - reviews
            result["rating"]["reviews"]["source"], result["rating"]["reviews"]["value"] = self.get_reviews(data)
            # Extract availability
            result["availability"]["source"], result["availability"]["value"] = self.get_availability(data)
            # Extract image URLS
            result["image_urls"]["source"], result["image_urls"]["value"] = self.get_image_urls(data)
            start_rank += 1
            # Append to results list
            results.append(result)
        return results

    def get_price(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _tag = data.select_one('.price')
            if _tag:
                source = str(_tag)
                value = _tag.get_text().strip()
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.PRICE_EXTRACTION.value
        return source, value

    def get_currency(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = 'EUR'
        
        try:
            pass
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.CURRENCY_EXTRACTION.value
        return source, value

    def get_price_per_unit(self, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        unit_value = Defaults.GENERIC_NOT_FOUND.value
        price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency 
        return source, unit_value, source, price_value, source, currency_value

    def get_promo(self, data, price_value, currency):
        source = Defaults.GENERIC_NOT_FOUND.value
        description_value = Defaults.GENERIC_NOT_FOUND.value
        promo_price_value = Defaults.GENERIC_NOT_FOUND.value
        currency_value = currency
        return source, description_value, promo_price_value, currency_value

    def get_url(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _tag = data.select_one('.item-panel a')
            if _tag and _tag.has_attr('href'):
                source = str(_tag)
                value = 'https://www.smythstoys.com' + _tag.get('href')
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.URL_EXTRACTION.value
        return source, value

    def get_brand(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            pass
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.BRAND_EXTRACTION.value
        return source, value

    def get_title(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            _tag = data.select_one('.name.trackProduct')
            if _tag:
                source = str(_tag)
                value = _tag.get_text().strip()
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.TITLE_EXTRACTION.value
        return source, value

    def get_description(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            pass
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.DESCRIPTION_EXTRACTION.value
        return source, value

    def get_ratings(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        max_value = "5"  # For this site, max rating is 5 stars

        
        try:
            pass
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.RATING_EXTRACTION.value
        return source, value, max_value

    def get_reviews(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            pass
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.REVIEWS_EXTRACTION.value
        return source, value

    def get_availability(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = Defaults.GENERIC_NOT_FOUND.value
        
        try:
            # there's no indication of OOS product found yet
            value = 'true'
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.AVAILABILITY_EXTRACTION.value
        return source, value

    def get_image_urls(self, data):
        source = Defaults.GENERIC_NOT_FOUND.value
        value = []  # image_urls data type must be list
        
        try:
            _tag = data.select_one('.responsive-image')
            if _tag and _tag.has_attr('data-src'):
                source = str(_tag)
                value = [_tag.get('data-src')]
        
        except Exception as e:
            self.logger.exception(e)
            value = FailureMessages.IMAGE_URLS_EXTRACTION.value
        return source, value