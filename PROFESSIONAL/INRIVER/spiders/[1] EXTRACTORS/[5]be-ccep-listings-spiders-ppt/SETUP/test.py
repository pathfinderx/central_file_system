const tienda_listing = require('./strategies/tienda.mercadona.es/tienda_mercadona_es_listing');
const tienda_ranking = require('./strategies/tienda.mercadona.es/tienda_mercadona_es_ranking');
const sanchez_ranking = require('./strategies/sanchez.romero/sanchez_romero_ranking')
const sanchez_listing = require('./strategies/sanchez.romero/sanchez_romero_listing')
const traverse = require('./strategies/sanchez.romero/sanchez_romero_listing_searcher');
const mercadona_searcher = require('./strategies/mercadona.es/mercadona_es_listing_searcher');
const mercadona_listing = require('./strategies/mercadona.es/mercadona_es_listing');
const mercadona_ranking = require('./strategies/mercadona.es/mercadona_es_ranking');

const dia_listing = require('./strategies/amazon.dia.es/amazon_dia_es_listing');
const fresh_listing = require('./strategies/amazon.fresh.es/amazon_fresh_es_listing');
const hipercor_listing = require('./strategies/hipercor.es/hipercor_es_listing');
const hipercor_topseller_listing = require('./strategies/hipercor.es.topseller/hipercor_es_topseller_listing');
const elcorteingles_topseller_listing = require('./strategies/elcorteingles.es.topseller/elcorteingles_es_topseller_listing');

let test_dia = {
  "id": 221449,
  "company_code":"700",
  "website":"amazon.es.dia",
  "type":"listing",
  "keyword":"Soft drinks",
  "postal_code":"28942",
  "category":"NCB's",
  "subcategory":"RTD Tea",
  "source_url":"https://www.amazon.es/s?i=grocery&bbn=21445267031&rh=n%3A21445267031%2Cn%3A6198072031%2Cn%3A6347509031%2Cn%3A6347540031&s=featured-rank&dc&pd_rd_r=f838a66e-4af3-440f-83db-15c57ebb75d3&pd_rd_w=dIuqM&pd_rd_wg=dUK0q&pf_rd_p=92932b6c-cdea-4160-a108-9fc1709170d0&pf_rd_r=2D01YCY9M3X27SVKAWGN&qid=1645079376&rnid=21445267031&ref=sr_nr_n_11",
  "date":"2022-05-22T00:00:00.000Z"
}

let test_fresh = {
  "id": 216020,
  "company_code":"700",
  "website":"amazon.es.fresh",
  "type":"listing",
  "keyword":"",
  "postal_code":"46980",
  "category":"",
  "subcategory":"",
  "source_url":"https://www.amazon.es/s?bbn=21445266031&rh=n:6347574031&s=featured-rank&pd_rd_r=d8ba4556-545c-4677-a298-e64a20b75064&pd_rd_w=fNi2X&pd_rd_wg=83efh&pf_rd_p=192cf1a6-8c20-4b28-9a42-7938ab4777ed&pf_rd_r=EYC3G50GCBX768K9F2J4&ref=fs_dsk_cp_ai_sml_4_21902138031",
  "date":"2022-05-13T00:00:00.000Z"
}

let test_tienda = {
  "id": 219153,
  "company_code":"700",
  "website":"tienda.mercadona.es",
  "type":"listing",
  "keyword":"",
  "postal_code":"28045",
  "category":"agua y refrescos",
  "subcategory":"Isotónico y energético",
  "source_url":"https://tienda.mercadona.es/categories/163",
  "date":"2022-05-13T00:00:00.000Z"
}

let test_elcorte_topseller = {
  "id": 999999,
  "company_code":"700",
  "website":"elcorteingles.es.topseller",
  "type":"listing",
  "keyword":"",
  "postal_code":"28850",
  "category":"",
  "subcategory":"",
  "source_url":"https://www.elcorteingles.es/supermercado/bebidas/agua-refrescos-y-zumos/refrescos-y-gaseosa/refrescos-diversos/?sort=mostSell",
  "date":"2022-08-08T00:00:00.000Z"
}

let test_hipercor_topseller = {
  "id": 999999,
  "company_code":"700",
  "website":"hipercor.es",
  "type":"listing",
  "keyword":"",
  "postal_code":"08028",
  "category":"",
  "subcategory":"",
  "source_url":"https://www.hipercor.es/supermercado/bebidas/agua-refrescos-y-zumos/isotonicas-y-energeticas/bebidas-energeticas/",
  "date":"2022-08-08T00:00:00.000Z"
}

let test_hipercor = {
  "id": 999999,
  "company_code":"700",
  "website":"hipercor.es",
  "type":"listing",
  "keyword":"",
  "postal_code":"08028",
  "category":"",
  "subcategory":"",
  "source_url":"https://www.hipercor.es/supermercado/bebidas/agua-refrescos-y-zumos/isotonicas-y-energeticas/bebidas-energeticas/",
  "date":"2022-08-08T00:00:00.000Z"
}

let test_ranking_tienda = {
  "id": 999999,
  "company_code":"700",
  "website":"tienda.mercadona.es",
  "type":"ranking",
  "keyword":"te limon",
  "postal_code":"28045",
  "category":"",
  "subcategory":"",
  "source_url":"",
  "date":"2022-08-03T00:00:00.000Z"
}

let test_ranking_sanchez_romero = {
  "id": 999999,
  "company_code":"700",
  "website":"sanchez-romero.online",
  "type":"ranking",
  "keyword":"Refrescos sin cafeina",
  "postal_code":"28043",
  "category":"",
  "subcategory":"",
  "source_url":"",
  "date":"2022-08-03T00:00:00.000Z"
}


// let results = tienda(false, '28001', job, 'es', false, 'lum-customer-detailonline-zone-residential-country-', '8hgtjckfvklo');
async function main(){
  let results = await elcorteingles_topseller_listing(true, test_elcorte_topseller.postal_code, test_elcorte_topseller, 'es', true, 'lum-customer-detailonline-zone-kstdkfgtr1-country', 'b5vwbekgi5jr');
  //let results = await hipercor_listing(true, test_hipercor.postal_code, test_hipercor, 'es', true, 'lum-customer-c_9c4e20a3-zone-kstdkfgtr1-country', 'b5vwbekgi5jr');
  //let results = await hipercor_listing(true, test_hipercor.postal_code, test_hipercor, 'es', true, 'lum-customer-c_9c4e20a3-zone-be_unblocker-country-es-unblocker', '5ae5cfm7v22g');
  //let results = await hipercor_listing(true, test_hipercor.postal_code, test_hipercor, 'es', true, '', '');
  console.log(JSON.stringify(results));
  }
main();
