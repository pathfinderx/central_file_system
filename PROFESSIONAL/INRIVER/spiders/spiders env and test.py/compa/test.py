from pprint import pprint
from copy import deepcopy
import urllib3
import json
from dotenv import load_dotenv
from request.crawlera import CrawleraSessionRequests
from request.luminati import LuminatiSessionRequests
from strategies import StrategyFactory
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
load_dotenv()
if __name__ == '__main__':
    country = 'se'
    website = 'www.netonnet.se'
    url = 'https://www.netonnet.se/art/ljud-bild/hogtalare/bluetooth-hogtalare/utan-batteri/marshall-acton-ii-bt-white/1004815.15256/'
    # url = 'https://www.netonnet.se/art/ljud-och-bild/hogtalare/bluetooth-hogtalare/utan-batteri/marshall-stanmore-ii-bt-black/1004816.15256/'
    company_code = '100'
    
    requester = LuminatiSessionRequests(country)  # Init requester
    factory = StrategyFactory()

    country = 'uk' if country == 'gb' else country
    dl_strategy = factory.get_download_strategy(website, country)(requester)  # Init download strategy for retailer with
    ws_strategy = factory.get_website_strategy(website, country, company_code)(dl_strategy)  # Init website strategy
    
    # Get Proxy IP info
    pprint(dl_strategy.requester.proxy_info)
    
    # Get raw data
    raw_data = dl_strategy.download(url)

    # Get extracted data
    res = ws_strategy.execute(raw_data)
    deepcopy(res)
    
    # pprint(res)
    with open('../extract-transform-comparison/et_test.json', 'w', encoding='utf-8') as f:
        json.dump(res, f, ensure_ascii=False, indent=2)