05 21 20
	Annotating a Stationary Car
		* keys
			* 'mouse scroll' to scroll up
			* 'arrow keys' to look down to direct to the scene
			* 'left and right arrow keys' to turn view left or right
			* 'plus arrow key' to move forward to the frames
			* 'mouse cursor' to move camera
			* 'click thumbnail of frames' && 'c key' for specific image view of full screen
			* 'APC' aggregated point cloud (latter points to see more frames)
				* use 61 frames to identify if the objects are stationary or moving
				* disable 'GROUND' points
				* drop a box to a specific object
					* what is cuboid? shape or a box?
					* choose label
					* select different camera views
					* check coordinates
					* 'left and right arrow keys' or 'mouse drag and drop' or 'asdw' or 'shift + asdw'to change placement of the cuboids 
						- to reposition the cuboid to fit the object 
					* 'mouse drag and drop' to fit the cuboid making it fit to the object
						- make sure to hit the cuboids to the bottom of the ground
					* 'v' change the object views
			* mark it stationary

			How do I move to the left?
				a key
			How do I rotate my view (turn side to side)?
				a or d keys
				+ or - keys
			How do I go from full-screen LiDAR to full-screen camera images?
				press c key
				click on a thmbnail image of the camera view
			How do we make small adjustments to the directional heading of a cuboid to make sure it is correctly aligned with the object?
				press c key
				click on a thmbnail image of the camera view
			Before we create a cuboid for an object, where do we want the top/front of the object to face?
				press the + button
				click the add new cuboid button
			How do we make a new cuboid?
				press the + button
				click the add new cuboid button
			What is the first thing we have to do after making a new cuboid?
				adjust the dimensions (sizing)
				select a label
			How can we adjust the placement of a cuboid?
				use asdw keys
				drag & drop with cursor/mouse
				// use shift with asdw keys
			How do we make small adjustments to the directional heading of a cuboid to make sure it is correctly aligned with the object?
				drag and drop the heading in the oerhead lidar view in the cuboid detail panel
				use the rotate 90 degree or rotate 180 degrees buttons under the lidar views in the cuboid detail panel
			With a cuboid selected so the cuboid detail panel is open, which LiDAR View should we check to make sure a cuboid is sized and placed correctly?
				back view
				side view