06 05 20
	Basic APC - Dynamic
		* aggregated point clound and dynamic tracks
			- another name for moving object pathways
		* use apc to find and annotate stationary object
		* turn off apc by pressing 'P' key to find moving objects

		Q and A
			Do we use APC while annotating dynamic tracks?
				no 
			How can we use APC to help with dynamic tracks?
				finding dynamic tracks
				checking if we have fully completed dynamic tracks
			Does every task type use apc?
				no
			If I find a missing dynamic track while in APC, what is the next step I take to annotate it?
				turn off apc and use the '- & +' keys to find the object in a single frame
			Which keyboard shortcut do we use to turn on/off APC?
				P key