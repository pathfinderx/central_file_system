06 05 20
	Basic APC and Dynamic Tracks (all points or aggregated points could or apc) - Stationary
		* moving object pathways
		* we often use 'max apc' for annotating stationary object
			* but when it comes to moving object we cant use apc to annotate
				* because pathways are overlapped
				* just turn off apc to identify moving object and avoid overlapped pathways

		keys 
			* press p to activate apc

		Q and A
			What does APC stand for?
				aggregated point cloud
			Does every task type use APC?
				no
			What do we use APC for?
				finding stationary objects
				correctly sizing and placing cuboids for stationary objects
				fiding dynamic stacks/moving objects
			What do we call it when APC is turned up to the highest possible setting?
				max apc
				dense apc
			Which keyboard shortcut do we use to turn on APC?
				P key