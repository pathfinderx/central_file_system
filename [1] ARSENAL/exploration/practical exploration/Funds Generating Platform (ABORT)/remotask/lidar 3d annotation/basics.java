06 05 20
	label data
		How to take courses to learn how to label data correctly
		How to do these data labeling tasks
		How you will get paid for doing those tasks
		What types of tasks exist on Remotasks
		How can you get better at doing tasks?

		What are some examples of data labeling we do at Remotasks?
			Drawing boxes around cars
			Categorizing images
			Transcribing an image or a recording into text

		Keys
			i = see instructions

		What does a task look like?
			categorization = ?
			semantic segmentation task = color the image bsed on the different labeels
			lidar annotation task = you draw 3D boxes, or "cuboids", around a space with points that represent a specific object.


		Task types 
			Classic tasks. These are tasks involving text, image, or video.
				Categorization. Answer questions about an image, website, or text sample. These are generally easy to do, and are good tasks to st
				Image Annotation. There are multiple types:
					Line & Spline Annotation. Label objects in an image by drawing lines. When it comes to labeling images, these are generally the easiest type of tasks to do.
					Box annotation. Draw boxes around specific objects in an image. These are also good tasks to start with for labeling images.
					Polygon annotation. Draw outlines around specific objects in an image. These are of medium difficulty because more work is required to draw the accurate outlines.
					Video annotation. This is like box annotation, but you are annotating frames of a video, so you need the boxes to follow the object. This is similar, but harder than, box annotation. Each frame in a video is an image.
					Semantic segmentation. Paint the pixels in an image. These tasks are usually medium to hard difficulty, depending on the instructions for how objects should be painted, and how many different "colors" of paint there are in the image.
			LiDAR tasks. These are tasks where the data from LiDAR sensors and is represented in 3D space.
				Drawing cuboids around 3D point clouds. These tasks are difficult because you need to learn how to look at a 3-dimensional image represented by points in space ("point cloud"), identify objects based on those points ("this seems to be a car"), and then draw cuboids around those points.
				LiDAR segmentation. Paint the objects in a 3D point cloud.These tasks are like the semantic segmentation tasks above, but they are applied to a 3D point cloud, not a 2D image.

		What is LiDAR
			(light detection and ranging) is a technology used by many self-driving cars to sense their surroundings. In LiDAR annotation tasks, you will navigate around a 3D scene that was recorded by a car with a LiDAR sensor.
