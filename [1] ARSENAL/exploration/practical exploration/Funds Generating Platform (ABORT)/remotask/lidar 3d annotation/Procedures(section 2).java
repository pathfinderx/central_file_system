06 05 20
	directional headings
		* the direction where object is facing
		* it should always be in front of the object
		* it should always be in top of the screen

		* identify the object by moving the cursor
		* recheck in the camera view 
		* drag and drop the box
		* select label
		* check the projected lidar views if the directional heading is correct
		* press 't' for topdown view
		* make sure to fit the shape and direction in views
			- use 'drag and drop'
			- use 'aswd' or 'shift aswd' for huge adjustments
			- use 'left right arrow'
		
		* another way to check is see the 'camera images'
			* identify each object by adjusting the 'frames'
			* you can identify the directional heading by seeing the shaded box

		* another way to check is see the 'lidar view'
			* identify each object by adjusting the 'frames'
			* you can identify the directional heading by seeing the shaded box

		* another way to check is the 'click the cuboid'
			* check the 'projected lidar view' or 'views' if the directional heading is correct

		Q and A
			Which way on an object should the directional heading face?
				front
			Before we draw a cuboid around an object, where do we want the top/front of the object to face to make sure the directional heading is correct?
				top of my screen
			How can we tell the directional heading of a cuboid while in full-screen LiDAR view, without using any shortcuts?
				check which side of the cuboid is shaded in
				click on a cuboid to open the cuboid detal panel & check close-up lidar views
			Which shortcut can we use to make the directional heading of a cuboid more obvious while in the LiDAR view?
				press T key
			How do we make small adjustments to the directional heading of a cuboid to make sure it is correctly aligned with the object?
				elf or right arrow keys