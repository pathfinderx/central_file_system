(NON-PROFESSIONAL-LIGHT VEHICLE)
1. Traffic jam can be prevented if you:
2. When making a right turn you should:
3. When you intend to turn right or left. Signal your intention at least:
4. At an intersection with a traffic light, make a left turn only when:
5. Graft and corruption in the traffic enforcement system can be eliminated by:
6. On a four (4) lane road with single white line, you can:
7. A double solid yellow line with broken white line in between means:
8. When making a U-turn you should:
9. Signs that are triangular in shape and with a red colored border are called:
10. Signs that are round, inverted triangle or octagonal and with red colored boarders are called;
11. Signs that are round, rectangular with white and blue background are called:
12. Single with broken line on a two-lane road means:
13. What to do when you approach a crosswalk or pedestrian lane?
14. Driving an unregistered motor vehicle is a violation with a fine of:
15. Considered a parking violation?
16. Green light at an intersection means:
17. Steady red light at an intersection means:
18. Flashing yellow light means:
19. The penalty of driving a motor vehicle while under the influence of alcohol for the first offense:
20. When a driver of PUV refuses to render service, convey passengers, such violation is penalize with:



21. If the driver is using a motor vehicle in committing a crime and is convicted, his driver’s license shall be;
22. The minimum distance away from the vehicle you are following is;
23. When do you have a complete/full stop?
24. On a two-lane road, overtaking is only allowed only at the:
25. Parking is considered as a violation when a motor vehicle:
26. Parking is prohibited:
27. When the traffic light is steady green and steady left/right arrow:
28. What is the maximum penalty for driving under the influence of liquor or prohibited drugs?
29. If a driver is found to have a fake or counterfeit license, his driver’s license shall be:
30. On a wet road, you must:
31. While driving with a maximum speed and you have to stop suddenly, you should:
32. When another vehicle is following you too closely, you should:
33. The driver of the vehicle behind you should always practice “distancia amigo” or the 3-second rule to prevent:
34. When parking uphill without a curb, turn your wheels:
35. When parking downhill, you must;
36. When parking uphill, you must:
37. When you make an abrupt move especially when you are on a wet and possibly slippery road, the following action can cause you to skid and lose control:
38. Whenever you are driving, especially when overtaking:
39. When driving on a highway, do not stare at the vehicle in front of you, instead you should:
40. When driving on mountain roads during daytime, you should;
41. When driving downhill on a mountain road always:



42. The headlights should be used often as needed to:
43. At an intersection, if two (2) vehicles arrived at the sametime, which vehicle has the right of way?
44. When negotiating a curve on a highway at a relatively highspeed you should:
45. When planning to overtake a slower vehicle in front of youat night, you should:
46. You are driving on a two (2) lane road. A vehicle coming in the opposite directions decides to overtake. Judging by his speed and distance from you he will not make it and he is on the head on collision course with you, what will you do?
47. Driving in heavy rains can be extremely dangerous because visibility is limited. What should you do?
48. A flashing red signal means:
49. Your speed while driving at night should keep on:
50. One that affects your visibility?
51. When driving at night, you should:
52. Being passed is normal part of driving and should not be taken as an insult to one’s ability, you should:
53. When oncoming vehicle deliberately crosses the centerline to pass another vehicle, you should be: necessary
54. When an oncoming vehicle crosses the centerline in a straight road you should be:
55. When an oncoming vehicle is forced to cross the centerline to avoid hitting another vehicle which suddenly pulled out from the lane, just: give way
56. When an oncoming vehicle crosses the centerline in making a left turn, a driver should:
57. When approaching a flooded area and you have to go through it, what should you do?
58. A single solid yellow or white line means:
59. If two vehicles approach or enter an intersection at approximately the same time, which vehicle has the right of way?