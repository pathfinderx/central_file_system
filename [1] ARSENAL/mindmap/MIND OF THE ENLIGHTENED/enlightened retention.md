## Internal Reference
>None
---
### Harness the power of notetaking
#notetaking #notetakingadvantage
 - [From Reddit](https://www.reddit.com/r/ObsidianMD/comments/qgyjij/obsidian_and_txt_files_getting_started/)
> "_Organizing information helps you retain it effectively_"
### Problems with unsorted knowledge
- [obsidian canvas 1:58](https://www.youtube.com/watch?v=vLBd_ADeKIw)
- "A mental squeeze point is when your unsorted knowledge becomes messy it overwhelms and discourages you"