## Internal Reference
>#analyticsandtask
---
###### Scoring
1.  Professional [P] (Already defined) ^62babc
2. Ethics [ET] ^0c8ca7
	- ==1 hour = 10 points==

	
4. Research and Development [RD] ^a6324c
	- ==1 hour  = 10 points==

5. Hardening and Stimulation [HS]
	- ==1hour = 20 points==

3. Physical Exercise [PH]  ^22090e
	- ==1 hour = 10 points==

6. Special [SP] ^bf35fb
	- score varies depending on situation
	- standard basis should be ==1hour = 10 points==

###### TIme Ratio

^fe3514

[Calculate the minutes to percentage](https://sciencing.com/calculate-percent-one-hour-8185794.html)
>Divide the number of minutes by 60, which is the number of minutes in an hour. For example, convert 14.75 minutes to a percentage by dividing 14.75 by 60, which equals about 0.246, or 24.6 percent.

>Example: Research and Development (4hours and 17 minutes) 
[[analytics - scoring#^a6324c | analytics scoring]]
v1 = 1 hour x 4 = 40
v2 = 17 minutes / 60 minutes in hour = 0.283 (round to tenths or the first floating point = 3)
final score =  v1 (40) + v2 (3) = 43
