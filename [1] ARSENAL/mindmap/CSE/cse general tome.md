## Internal Reference
>#civilservice
---
### Numeric
- ### Coverage
	- types of numbers
		- [[numeric - types of numbers]]
	- arithmetic operations (PEMDAS)
		- [[arithmetic - addition operation]]
	- fractions
	- exponents
	- word problems
	- stats & probability
	- geometry
### Analytical
### Verbal
### General Information