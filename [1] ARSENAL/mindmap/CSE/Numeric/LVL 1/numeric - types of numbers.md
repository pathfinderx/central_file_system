## Internal Reference
>#civilservice
---- 
### Types of Numbers
- <mark class="hltr-grey">real number</mark> -  value of a continuous quantity that can represent a distance along a line

- <mark class="hltr-grey">natural number</mark> - the natural numbers are those used for counting and ordering <mark class="hltr-ash">[1,2,3]</mark>
- <mark class="hltr-grey">whole number</mark> - positive integer <mark class="hltr-ash">[0, 1, 2, 3]</mark>
- <mark class="hltr-grey">integer</mark> - number that is not a fraction, could be negative or positve <mark class="hltr-ash">[-2, -1, 0, 1, 2]</mark>
- <mark class="hltr-grey">rational number</mark> - a rational number is a number that can be expressed as the 'quotient' or decimal or 'fraction' of 'two integers'. <mark class="hltr-ash">E.g. 1/2, 1/3 ... </mark>
- <mark class="hltr-grey">irrational number</mark> - not rational number can be expressed as mathematical such as <mark class="hltr-ash">pi</mark> and other example would be <mark class="hltr-ash">square root</mark>