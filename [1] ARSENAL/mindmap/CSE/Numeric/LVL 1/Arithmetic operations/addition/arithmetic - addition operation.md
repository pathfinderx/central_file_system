## Internal Reference
>#civilservice
---
### Fundamentals
- ![[addition_parts.png| addition parts]]
### Techniques
- [[single digit quick add.canvas]]
- <mark class="hltr-orange"> Split and solve technique</mark>
	- [ ] one digit
		- [ ] canvas
		- [ ] 5 digits sequence
		- [ ] actual addition operation
	- [ ] two digit