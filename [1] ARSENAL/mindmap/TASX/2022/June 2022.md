## Internal Reference
>#analyticsandtask
>__Scoring Tips__^[[[analytics - scoring#Scoring]]]
---
# TasX June 2022
- ==06 09 22== (Total Score: 61)

	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 6)
		- [x] doggies
		- [x] area cleaning
		- [ ] shirts sortation
		
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)

	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 43; Tips:  [[analytics - scoring#^fe3514 | Time Ratio]] ) 
		- [x] 3 hours R&D (note taking using obsidian) (time lapse: 04:17:00)
		- [x] update scoring for today
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 10)
		- [x] gym
				>Score
	
	- [[analytics - scoring#^bf35fb| Special]]  (Score: 2)
		- [x] scan and send Id

- ==06 10 22== (Score: 65)
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 3x10 = 30)
		- [x] Obsidian
			- [x] filters and grouping
			- [x] Transfer test on copilot 70%
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 20)
		- [x] Lower
			- [x] squat 40lbs 10x4
			- [x] one leg squat 15lbs 12x4
			- [x] inverse squat 80lbs 12x4
			- [x] leg extension 40lbs 10x4
			- [x] hamstring curl 4lbs 10x4
			- [x] calf raise 55lbs 12x4

	- [[analytics - scoring#^bf35fb| Special]] (Score: 15)
		- [x] Haircut
		- [x] change table cloth
		- [ ] Gdrive transfer
			- [ ] Dl
			- [ ] transfer new
		- [ ] Pc sortation
		- [ ] shirt sortation
		- [x] papers and receipts sortation

- ==06 11 22==
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
		- [ ] Transfer math power to ratings ...
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [ ] Obsidian
			- [ ] Transfer test on copilot 100%
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 11)
		- [x] Shoulders traps, abs
	
	- [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] Gdrive transfer
			- [x] Dl
			- [ ] transfer new
		- [ ] Pc sortation
		- [ ] shirt sortation
		- [ ] clothes wash

- ==06 12 22==
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
		- [ ] Transfer math power to ratings ... 
			- [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [ ] Obsidian
			- [ ] Transfer test on copilot 100%
			- [ ] Add list comprehension sample from varuste.net
			- [ ] Add math pow on et
				-  [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
			- [ ] add dict training and other python trainings
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 11)
		- [ ] abs, trapz, biceps
	
	- [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] Gdrive transfer
			- [x] Dl
			- [ ] transfer new
		- [ ] Pc sortation
		- [ ] shirt sortation
		- [ ] clothes wash

- ==06 14 22==
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
		- [ ] Transfer math power to ratings ... 
			- [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [ ] Obsidian
			- [ ] Transfer test on copilot 100%
			- [ ] Add list comprehension sample from varuste.net
			- [ ] Add math pow on et
				-  [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
			- [ ] add dict training and other python trainings
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 11)
		- [ ] abs, trapz, biceps
	
	- [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] Gdrive transfer
			- [x] Dl
			- [ ] transfer new
		- [ ] Pc sortation
		- [ ] shirt sortation
		- [ ] clothes wash


- ==06 20 22==
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
		- [ ] Transfer math power to ratings ... 
			- [ ] [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
		- [ ] transfer latin -> make thread for encode and decoding
			- [ ] [[C:\repositories\comparison-spiders\strategies\website\br\kabum_com_br.py | path]]
			- [ ] [[C:\repositories\comparison-spiders\strategies\website\be\vandenborre_be.py | path]]
			- [ ] brand prepending [[spider | royan canin path]]

		- [ ] Trello tickets
			- [ ] transfer learnings
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [ ] Obsidian
			- [ ] Transfer test on copilot 100%
			- [ ] Add list comprehension sample from varuste.net
			- [ ] Add math pow on et
				-  [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
			- [ ] add dict training and other python trainings
			- [ ] nodelist puppeteer loop
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 11)
		- biceps
	
	- [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] Gdrive transfer
			- [ ] transfer new
		- [ ] Pc sortation
		- [ ] shirt sortation
		- [x] Scoring

- ==06 26 22==
	- [[analytics - scoring#^0c8ca7 | Ethics]] (Score: 0)
	
	- [[analytics - scoring#^62babc | Professional]] (Score: 0)
		- [ ] trello tickets
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
	
	- [[analytics - scoring#^22090e | Physical]] (Score: 11)
	
	- [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] grout
		- [ ] wallpaper
		- [ ] clothes
		- [ ] room sortation