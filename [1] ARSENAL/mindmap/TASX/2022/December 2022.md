## Internal Reference
>#analyticsandtask
>__Scoring Tips__^[[[analytics - scoring#Scoring]]]
---
==12-03-2022==
Expenses:
- 2500 electricity
- 2000 home foods
- 1000 internet
- 1000 gym
- 1000 gold
- 1500 other expenses
- : 9000
---
==12-17-2022==
- monkeytype
	- 114
	- 120, 111, 114
	- 112, 119, 121
	- 86, 92, 100
---
==12-21-2022==
- [ ] Trello
	- [ ] olssonsfiske.se
- [x] mindmap
	- [x] update git revert
		- [x] scroll first from copilot
- [ ] send daily report
- [ ] CSE Grind 
	- [ ] test canvas
	- [ ] split and solve technique
---
==12-24-2022==
- [x] transfer clothes
- [x] internet bill - pay and update
- [ ] CSE
	- [x] addition
		- [ ] single quick digit add
		- [ ] split and solve
		- [ ] chunking
		- [ ] actual exercise
---
==12-25-2022==
- [ ] CSE
	- [x] addition - stimulate
		- [x] single quick digit add
		- [x] split and solve
		- [ ] chunking
			- [ ] create chunking questionnaire
		- [x] actual exercise
	- [ ] create exercise for all operations
		- [ ] addition
		- [ ] subtraction
		- [ ] multiplication
		- [ ] division
---
==12-30-2022==
- [x] room clearning
- [ ] CSE
	- [ ] addition - stimulate
		- [ ] single quick digit add
		- [ ] split and solve
		- [ ] chunking
			- [ ] create chunking questionnaire
		- [ ] actual exercise
	- [ ] create exercise for all operations
		- [ ] addition
		- [ ] subtraction
		- [ ] multiplication
		- [ ] division
---

- ##### Research and Development
	- [ ] django fundamentals
		- [x] setup and installation
		- [x] templating, rendering
		- [x] dynamic routing
		- [x] models
		- [x] django crud
		- [x] django search and filters
		- [x] django flash message
		- [x] django authentication
			- [x] django user registration
		- [ ] message
- ##### Reminders
- [ ] update laptop repo
- ##### Copilot
	- [x] add debunked regex function
		- [x] finditer() vs findall
		- [x] metacharacters and quantifiers
	- [ ] add python trainings to copilot
	- [ ] practice lambda function
	- [ ] balance outward for entire div selection
		- [ ] (https://youtube.com/shorts/NwqhFb4B5LU?feature=share)
		- [ ] how to make mapping in vscode
	- [ ] difference between var, protected var and private var
	- [ ] learn pep8 standard
	- [ ] duplicate removal using set and conventional list container style
	- [ ] add js2py code
		- [ ] [[C:\repositories\be-comparison-spiders\strategies\website\uk\johnlewis_com.py | reference > get_delivery]]
	- [ ] weird printing style
		- [ ] [reference](https://www.youtube.com/shorts/NHquaDS6XQ0)
- ##### to purchase
	- speaker
	- notebook
	- pencil
	- eraser