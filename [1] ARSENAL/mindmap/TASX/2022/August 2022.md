## Internal Reference
>#analyticsandtask
>__Scoring Tips__^[[[analytics - scoring#Scoring]]]

* ==07 29 22==
	* [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [x] test no color in tiles
	- [ ]
* ==08 17 22==
	* [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [x] test no color in tiles
		- [ ] mango query

* ==08 18 22==
	* Expenses
		- 20 amino
		- 20 helping hands
	
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
	- [ ] Obsidian
		- [x] Transfer test on copilot 100%
		- [ ] Add list comprehension sample from varuste.net
		- [ ] Add math pow on et
			-  [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
		- [ ] add dict training and other python trainings
		- [ ] nodelist puppeteer loop
		- [ ] remove duplicate values and strip() arguments
			- [[C:\repositories\comparison-spiders\strategies\website\mx\amazon_com_mx.py | path]]
			- references:
				- [duplicate removal ref](https://www.geeksforgeeks.org/python-ways-to-remove-duplicates-from-list/)
				- [strip arguments](https://www.programiz.com/python-programming/methods/string/strip)
		- [ ] more on here
			- [ ] any() function
			- [ ] datetime
			- [ ] classes extraction
				- [ ] using parenthesis on classes with self
			- [ ] 
		- [ ] ppt node manipulation:
				- [[C:\repositories\puppeteer-comparison-spiders\strategies\be\mediamarkt-be-nl.js | path]]
				-  include:
			- [ ]  getAttribute
			- [ ]  childNode
			- [ ]  parentNode
		- [ ] Use siblings when same selectors with bottom tags and using specific indicator
			- [path](C:\repositories\ranking-spiders\strategies\website\uk\argos_co_uk.py)

* ==08 19 22==
	* [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] mango query
		- [x] variants update
			- [x] microsoft.com/fr-fr issue
		- [ ] investigate target.com webshots
		- [ ] add async approach
			- [[C:\repositories\puppeteer-comparison-spiders\strategies\nl\belsimpel-nl.js | path]]

* ==08 21 22==
	* expenses
		* 1260 mga sabon2

* ==08 22 22==
	* expenses
		* 150 bread and cheese
		
* ==08 23 22==
	* expenses
		* 1172 soaps

* ==08 24 22==
	* [x] ot form
	* [x] investigate sa/ro/bg
	* expenses
		* 260 foods

* ==08 25 22==
	* [x] investigate sa/ro/bg
	* [x] mango query

* ==08 26 22==
		- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
	- [ ] Obsidian
		- [x] Transfer test on copilot 100%
		- [ ] Add list comprehension sample from varuste.net
		- [ ] Add math pow on et
			-  [[C:\repositories\extract-transform-comparison\transformers\de\alternate_de.py | path]]
		- [ ] add dict training and other python trainings
		- [ ] nodelist puppeteer loop
		- [ ] remove duplicate values and strip() arguments
			- [[C:\repositories\comparison-spiders\strategies\website\mx\amazon_com_mx.py | path]]
			- references:
				- [duplicate removal ref](https://www.geeksforgeeks.org/python-ways-to-remove-duplicates-from-list/)
				- [strip arguments](https://www.programiz.com/python-programming/methods/string/strip)
		- [ ] more on here
			- [ ] any() function
			- [ ] datetime
			- [ ] classes extraction
				- [ ] using parenthesis on classes with self
			- [ ] 
		- [ ] ppt node manipulation:
				- [[C:\repositories\puppeteer-comparison-spiders\strategies\be\mediamarkt-be-nl.js | path]]
				-  include:
			- [ ]  getAttribute
			- [ ]  childNode
			- [ ]  parentNode
		- [ ] Use siblings when same selectors with bottom tags and using specific indicator
			- [path](C:\repositories\ranking-spiders\strategies\website\uk\argos_co_uk.py)
	---
	* [[analytics - scoring#^bf35fb| Special]] (Score: 0)
		- [ ] investigate target.com webshots
		- [ ] add async approach
			- [[C:\repositories\puppeteer-comparison-spiders\strategies\nl\belsimpel-nl.js | path]]

* ==08 28 22==
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [ ] make system for mango query builder and sql query builder
			- [x] comparisons
			- [ ] rankings
			- [ ] listings

* ==09 09 22==
	- [[analytics - scoring#^a6324c | Research And Development]] (Score: 0)
		- [x] microsoft variants settle teams
		- [x] user account in laptop
		- [x] find slacks
		- [x] list down plans
		- [x] setup ppt for laptop 80%

