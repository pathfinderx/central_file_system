## Internal Reference
>#analyticsandtask
>__Scoring Tips__^[[[analytics - scoring#Scoring]]]
---
==02 02 23==
- [x] tpm setup
- [x] payments
	- [x] internet
	- [x] LCR additional payment
	- [x] laundry
- [x] initiate backup sequence
	- [x] repositories
	- [x] Users/Kurt Kevin
	- [x] obsidian saved settings
		- [x] .obsidian
			- saved settings are already here
	- [x] sublime saved settings
		- [x] packages from app data
		- [x] settings from preferences
			- [x] sublime settings
			- [x] material settings
	- [x] sourcetree saved settings and ssh
	- [x] vscode
		- [x] extensions from users
		- logs:
			- git lens
			- monokai pro
			- python
			- auto rename tag
			- ref: https://prnt.sc/Q-trgBjf4FYs
	- [x] dbeaver
		- [x] queries
			- screenshot all db connection names: https://prnt.sc/VRD5Ctown8KV
		- [x] essential queries
		- [x] backup proect as connection and queries
	- [x] postman
		- [x] collections.js
			- [x] D:\THREADSTONE\[3] CONFIGURATION\central_command_payload_builder
		- [x] setup environment variables
			- [x] C:\Users\PathfinderX\Documents\Lightshot\Screenshot_367.png
	- [x] azure
	- [x] surfshark
	- [x] files
		- [x] downloads
		- [x] screenshots
		- [x] desktop files
	- [x] openvpn
		- [x] kurtn.ovpn from Users
	- [ ] versions
		- [ ] https://prnt.sc/Lzy_pZbEwNTb
		- [ ] https://prnt.sc/HvEYOCIBqiQh
---
==02 02 23==
- [ ] cdiscount/3p
- [ ] UML or SOLID
- [ ] fix pagination listings
	- [ ] micromania.com/fr
	- [ ] 
- [ ] create et
	- [ ] rankings all metrics
	- [ ] listings all metrics
---
- ##### Research and Development
	- [ ] django fundamentals
		- [x] setup and installation
		- [x] templating, rendering
		- [x] dynamic routing
		- [x] models
		- [x] django crud
		- [x] django search and filters
		- [x] django flash message
		- [x] django authentication
			- [x] django user registration
		- [ ] message
- ##### Reminders
	- [ ] update laptop repo
	- [ ] papers sortation
- ##### Copilot
	- [x] add debunked regex function
		- [x] finditer() vs findall
		- [x] metacharacters and quantifiers
	- [ ] add python trainings to copilot
	- [ ] practice lambda function
	- [ ] balance outward for entire div selection
		- [ ] (https://youtube.com/shorts/NwqhFb4B5LU?feature=share)
		- [ ] how to make mapping in vscode
	- [ ] difference between var, protected var and private var
	- [ ] learn pep8 standard
	- [ ] duplicate removal using set and conventional list container style
	- [ ] add js2py code
		- [ ] [[C:\repositories\be-comparison-spiders\strategies\website\uk\johnlewis_com.py | reference > get_delivery]]
	- [ ] weird printing style
		- [ ] [reference](https://www.youtube.com/shorts/NHquaDS6XQ0)