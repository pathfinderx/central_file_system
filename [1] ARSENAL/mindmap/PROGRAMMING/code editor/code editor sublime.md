## Internal Reference
>#programming
---
## shortcuts
- <mark class="hltr-orange">ctrl + k + (1)</mark>- collapse first level
- <mark class="hltr-orange">ctrl + k + (2)</mark> - collapse second level
- <mark class="hltr-orange">ctrl + k + u</mark> - uppercase selected text
- <mark class="hltr-orange">ctrl + k + l</mark> - lowercase selected text