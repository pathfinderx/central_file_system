# Internal Reference

>#programming
>

---
## Environment Setup
- ### Installation
	- Monokai Pro 
		- for general theme
		- monokai (filter spectrum) > set theme
	- Dobri Next 
		- specific only to file icons theme
		- then set only file icons theme
	- Auto rename tag
		- tag pair auto rename the closing tag
	- GitLens
		- identify git information line by line
- ### Shortcuts
	>[[general tome shortcuts | backlink to general tome shortcuts]]
	- <mark class="hltr-orange">ctrs + \\</mark> - to split screen
	- <mark class="hltr-orange">ctrl + p</mark> - Palette / Searching specific file
	- <mark class="hltr-orange">shirt + alt + c</mark> - Copy active file path
	- <mark class="hltr-orange">ctrl + g</mark> - Move to specific line
	- <mark class="hltr-orange">click line + F9</mark> - adding breakpoint
- ### shortcuts to learn
- in vscode > auto format depending on file type > <mark class="hltr-orange">alt + shift + f</mark>