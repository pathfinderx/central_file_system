# Internal Reference
>#programming
>[[git general tome]]
---
- #### git config
	- [[git configuration primordial]]
	- [[(workflow) bitbucket config remote]]
	- [[(workflow) local config without remote]]
- #### display/show commands
	- [[git show status]]
- #### terminal commands
	- [[git terminal commands]]
- #### workflows
	- ##### create
		- [[(workflow) create]]
	- ##### merge
		- [[(workflow) git merge]]
	- ##### moving or copying file/directory
		- [[(workflow) move]]
	- ##### rename
		- [[(workflow) rename]]
	- ##### remove / delete
		- [[(workflow) remove or delete]]
	- ##### revert
		- [[(workflow) git revert]]
- #### issues encountered and solutions
	- [[(procedures to watchout) issues]]