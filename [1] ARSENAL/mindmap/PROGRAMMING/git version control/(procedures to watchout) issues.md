# Internal Reference
>#programming
>[[git general tome]]
--- 
### Another git process seems to be running in this repository
- [web link](https://stackoverflow.com/questions/38004148/another-git-process-seems-to-be-running-in-this-repository)
- <mark class="hltr-orange">rm -f .git/index.lock</mark> - execute this
### when directly adding files to repo then initialize
- <mark class="hltr-red">error: '2022/Git Reporsitories/test/' does not have a commit checked out</mark>
- <mark class="hltr-red">fatal: adding files failed</mark>
- [[https://zjschulz.medium.com/error-does-not-have-a-commit-checked-out-573c0ed9c49]]
- execute this procedure:
	- <mark class="hltr-blue">keypoints: delete the git repository</mark>
		1. cd to directory
		2. <mark class="hltr-orange">ls -a</mark> - to see the hidden file .git
		3. <mark class="hltr-orange">rm -rf .git</mark> - 'rf' flag for deletion in root directory
		4. <mark class="hltr-orange">mkdir **filename** </mark>
		5. <mark class="hltr-orange">cd **filename**</mark>
		6. <mark class="hltr-orange">git init</mark>
### .gitignore not working?
- first, make sure file is not deleted or not in staged if deleted
- if deleted and staged: unstage the changes
	- <mark class="hltr-orange">git restore --staged **filename**</mark>
	- <mark class="hltr-orange">git rm -rf --cached **filepath**</mark>
- if no changes made
	- <mark class="hltr-orange">git rm -rf --cached **filepath**</mark>
- now try to make changes on the target file
### .gitignore not working again?
- <mark class="hltr-orange">git rm -rf --cached **the-stubborn-file**</mark> 
	just delete the cached files in .git if it exists before .gitignore was created
### this is not a valid source path
- utilize ssh configured in source tree > bitbucket repo
	- [[https://bitbucket.org/pathfinderx/central_file_system/admin/addon/admin/pipelines/ssh-keys]]
- uncheck private repository
	- [[https://bitbucket.org/pathfinderx/central_file_system/admin > general > repository details]]
### Failed to push the duplicate commit amends
- <mark class="hltr-red">To https://bitbucket.org/pathfinderx/central_file_system.git
			 ! [rejected]        update -> update (non-fast-forward)
			error: failed to push some refs to 'https://bitbucket.org/pathfinderx/central_file_system.git'
			hint: Updates were rejected because the tip of your current branch is behind
			hint: its remote counterpart. Integrate the remote changes (e.g.
			hint: 'git pull ...') before pushing again.
			hint: See the 'Note about fast-forwards' in 'git push --help' for details.</mark>
	---
 - <mark class="hltr-blue">recommended action (worked this way)</mark>
	 1. checkout to other_branch
	 2. merge with the primordial_branch as backup
	3. push the other_branch
	4. checkout back to primordial_branch
	5. push the primordial_branch
### Unlink of file 'db.sqlite3' failed. Should I try again? (y/n)
- disable runserver on django
### "'refs/heads/feature' exists; cannot create" - using git flow feature
1. remove the existing branch 'feature' if existing in branch or manually added 'feature' branch
2. using command 'git branch -d feature'