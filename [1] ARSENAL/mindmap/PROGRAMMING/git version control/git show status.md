# Internal Reference
>#programming
>[[git general tome]]
--- 
### Show status / Logs
- #### General
	- <mark class="hltr-orange">git status</mark> - show repo status
	- <mark class="hltr-orange">git config --global -e</mark> - show global config
- #### Staging Area
	- <mark class="hltr-orange">git ls-files</mark> - show files in staging area
- #### Commits
	- <mark class="hltr-orange">git log --oneline</mark> - oneliner
	- <mark class="hltr-orange">git log --oneline -n 10</mark> - oneline and show only 10 latest commits
	- <mark class="hltr-orange">git log --graph --decorate</mark> - complete with branch
	- <mark class="hltr-orange">git log --graph --decorate</mark> - complete with branch and display only 10 with color
	- <mark class="hltr-orange">git log --graph --decorate | tail -n 10</mark> - complete with branch and only display only 10 commits