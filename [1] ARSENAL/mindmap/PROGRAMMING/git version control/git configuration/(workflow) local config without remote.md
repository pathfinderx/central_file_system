# Internal Reference
>#programming
>[[git general tome]]
--- 
### Create local git without remote
- #### Checkers
	- <mark class="hltr-orange">git status</mark> - check branch, directory, any commits
	- <mark class="hltr-orange">git status -s</mark> 
		- -s flag means short
			- if it displays '??' it means it is untracked file
			- if it displays 
	- <mark class="hltr-orange">git ls-files</mark>
		- check files on staging area??
- #### add to staging area or index 
	- proposal for next commit
	-  for review
	- commands:
		- <mark class="hltr-orange">git add file_name</mark> - add specific
		- <mark class="hltr-orange">git add .</mark> - add all
- #### remove from staging area
	- unix command
		- <mark class="hltr-orange">rm **path**</mark>
	- git command
		- <mark class="hltr-orange">git restore --staged \<**file**></mark>
- #### remove from unstaged area or discard changes
	- <mark class="hltr-orange">git restore \<**file**></mark>
- #### commit or snapshot
	- saved in repository
	- contains information
		- ID
		- Message
		- Date / Time
		- Author
		- Complete snapshot
	- commands:
		- <mark class="hltr-orange">git commit -m "Your Message"</mark>
			- relies on staged files
		- <mark class="hltr-orange">git commit -am "Your Message"</mark>
			- skips staging and commits file