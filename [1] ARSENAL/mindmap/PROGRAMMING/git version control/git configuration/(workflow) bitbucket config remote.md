# Internal Reference
>#programming
>[[git general tome]]
--- 
### Create bitbucket remote
- #### Initial Action
	1. create repository first and copy the url after creation
	2. cd to local folder
		- <mark class="hltr-orange">git remote add origin **<bitbucket repository url>**</mark>
	2. check remote
		- <mark class="hltr-orange">git remote -v</mark>
	1. initial push
		1. <mark class="hltr-orange">git push -u -f origin master</mark>
			- the -u flag explanation:
				- [[https://stackoverflow.com/questions/18867824/what-does-the-u-flag-mean-in-git-push-u-origin-master | web reference]]
			- -f flag means force and it is required for initial push
-  #### Creating and pushing branches
	1. ##### create branch
		- note: changes in specific branch are not visible in other branch unless you checkout for the altered branch
		- terminal
			- <mark class="hltr-orange">git checkout -b <**branch-name**> master</mark>
		- bitbucket
			- on create branch button: [[https://bitbucket.org/pathfinderx/central_file_system/branches/|web reference]]
	1. ##### fetching changes
		- <mark class="hltr-orange">git fetch --all</mark>
			- note: this also updates any creation and changes in branches
			- note: recommended before executing 'git branch -r' command
	1. ##### check current branch
		- <mark class="hltr-orange">git branch -vv</mark> or <mark class="hltr-orange">git branch</mark>
	1. ##### check all branches in repository
		- <mark class="hltr-orange">git branch -r</mark>
	1. ##### pushing branch in remote for the first time
		- <mark class="hltr-orange">git push --set-upstream origin **\<branch-name>**</mark>
	1. ##### switch branch
		- <mark class="hltr-orange">git checkout <**branch-name**></mark>
	1. ##### delete branch
		- <mark class="hltr-orange">git push -d origin </branch-name></mark>
			- origin means referring to remote
		- <mark class="hltr-orange">git push -d <**branch-name**></mark>
			- without origin means deleting branch only in local
	- ==note: you cant delete branch while you are currently on that branch, you have to switch branch using git checkout command==
### Setup SSH method in bitbucket
- generate ssh key using puttygen
	- use ssh-2 rsa key	
	- generate without passphase
	- goto:
		- your profile and settings > personal settings > ssh key (under security) > add key
			- adding key
				- load private key using putty gen and copy the key inside 'OpenSSH autorized_key file' field
				- paste it to the key field