tr# Internal Reference
>#programming
>[[git general tome]]
--- 
### Theoretical
- version control system has 2 categories	
	- centralized
		- disadvantage: if server is offline, cant collaborate
	- distributed
		- advantage: every user has local copies
- compresses the content
- doesnt store duplicate content
- MINGW64 - unix powershell
- install git
- utilize bash instead of OS built in terminal
### Configuration
- #### Primordial
	- <mark class="hltr-orange">git config --<options> user.name "pathfinderX"</mark> - user name
	- <mark class="hltr-orange">git config --<options> user.email nebrilkurtkevin5@gmail.com</mark> - email
	- <mark class="hltr-orange">git config --<options> core.editor "code --wait"</mark> 
		- '--wait' flag tells the terminal to wait till new vscode instance is closed
		- default editor
		- practical usage: when you revert using 'git revert HEAD', it opens vs code (using code --wait) and wait till vscode is closed
		- [weblink](https://www.git-tower.com/learn/git/faq/undo-revert-old-commit)
	- <mark class="hltr-orange">git config --<options> -e</mark> - opens the git configuration
	- <mark class="hltr-orange">git config --<options> core.autocrlf true</mark>
		- this config is line ending handler
		- autocrlf or auto carriage return line feed or (/r /n) 
		- this configuration settings only removes the line feed for both OS in a centralized repository (/n)
		- use 'true' if windows use 'input' if macOS
	#### git config flag
	- <mark class="hltr-orange">--global </mark>
		- affects all working repositories for current operating system user
		- stored in user home directory
	- <mark class="hltr-orange">--local</mark>
		- affects only for repository
		- stored in repo's .get directory
	- <mark class="hltr-orange">--system</mark>
		- affects the entire users of on an entire machine
		- all users in operating system and repositories
		- stored in system root path
	- ref: [web link](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config)
	#### git config core.editor
	- <mark class="hltr-orange">git config --<options> core.editor "code --wait"</mark>
	- default config editor: vscode
	- other options:
		- <mark class="hltr-orange">atom --wait</mark>
		- <mark class="hltr-orange">emacs</mark>
		- <mark class="hltr-orange">nano -w</mark>
		- <mark class="hltr-orange">vim</mark>
		- <mark class="hltr-orange">subl -n -w</mark> - sublime
		- <mark class="hltr-orange">'C:/program files (x86)/sublime text 3/sublimetext.exe' -w</mark> - sublime x86
		- <mark class="hltr-orange">'C:/program files/sublime text 3/sublimetext.exe' -w</mark> - sublime x64
		- <mark class="hltr-orange">mate -w</mark> - textmate
	- [[https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config | More options here]]
- ### bitbucket config remote
	- [[(workflow) bitbucket config remote]]
- ### local config without remote
	- [[(workflow) local config without remote]]