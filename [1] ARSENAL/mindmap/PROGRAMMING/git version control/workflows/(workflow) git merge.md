# Internal Reference
>#programming
>[[git general tome]]
--- 
### Merging standard procedure
1. <mark class="hltr-orange">git branch --v</mark> - check branch
2. <mark class="hltr-orange">git checkout **branch-name**</mark> - switch branch to desired branch
3. fetch and pull for safety
	1. oneline
		- <mark class="hltr-orange">git fetch && git pull</mark>
	1. multiline
		1. <mark class="hltr-orange">git fetch origin **branch-name**</mark>
		2. <mark class="hltr-orange">git pull origin **branch-name**</mark>
1. <mark class="hltr-orange">git merge updates</mark> - merge with the desired branch (scenario: master (current branch) is outdated and should merge with origin/updates)
1. <mark class="hltr-orange">git push -u origin master</mark>- all merged commits and changes are required to be pushed
### Merging (without checking out to other branch first)
==For safety purposes by not executing **git checkout**==
- [[https://stackoverflow.com/questions/3216360/merge-update-and-pull-git-branches-without-using-checkouts | web reference]]
- git fetch <**remote**> <**source branch**>:<**target branch**>
	- <mark class="hltr-orange">git fetch . update:master</mark>
		- '.' means the local repository as the remote
	- then you can now safely checkout go to target branch so you can push
- ==now the target branch is ready to push, it is safer to checkout to that branch without making any changes to local files while checking out==