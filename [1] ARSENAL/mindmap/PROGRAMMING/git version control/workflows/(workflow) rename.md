# Internal Reference
>#programming
>[[git general tome]]
--- 
### Rename feature workflow
- [web link](https://github.com/petervanderdoes/gitflow-avh/wiki/Reference:-git-flow-feature)
- <mark class="hltr-orange">git flow feature rename **Rename a feature branch**</mark>
	- Description: Rename branch <name> to <new_name>
- <mark class="hltr-orange">git flow feature rename [-h] <**new_name**> [<**name**>]</mark>
	- Options:
		- <mark class="hltr-orange">-h</mark>,<mark class="hltr-orange">--[no]</mark> help - show this help
		- <mark class="hltr-orange">--showcommands</mark> - Show git commands while executing them
### Rename branch
- <mark class="hltr-orange">git branch -m <**old-branch-name**> <**new-branch-name**></mark>
	- -m flag means move or rename 
### Rename commit
- <mark class="hltr-orange">git commit --amend -m "Message"</mark>
	- amend the unpushed commit
	- or amend the pushed commit
- <mark class="hltr-orange">git push -u origin <**branch-name**></mark>
	- dont forget to push