# Internal Reference
>#programming
>[[git general tome]]
--- 
### create git repository
- create directory then cd to that directory, use these commands
	1. <mark class="hltr-orange">mkdir</mark>
	2. <mark class="hltr-orange">cd <**directory**></mark>
- <mark class="hltr-orange">git init</mark>
	- create master for that directory
	- creates .git file but hidden
- <mark class="hltr-orange">git init --bare</mark>
	- creates repository without a working tree
- <mark class="hltr-orange">touch <**file**></mark>
	- unix command to create file