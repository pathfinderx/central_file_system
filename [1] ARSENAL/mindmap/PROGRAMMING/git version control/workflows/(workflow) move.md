# Internal Reference
>#programming
>[[git general tome]]
--- 
### Copy frm source to destination 
- copy directory tree to destination
	- [web link](https://www.rapidtables.com/code/linux/cp.html)
	- <mark class="hltr-orange">cp -R <**dir1**> <**dir2**></mark>
		- -R recursive copy including hidden files
		- dir1 should be existing
		- dir2 can be optional: either existing or not
			- if dir2 is existing: it copies dir1 inside dir2	
				- dir2/dir1/dir1contents
			- if dir2 is not existing: it creates dir1 copy and named dir2
				- dir2
### Move file
- <mark class="hltr-orange">git mv oldfile.txt newfile.txt</mark>
	- rename file
- <mark class="hltr-orange">git mv <**file**> ..</mark>
	- move file to previous one level