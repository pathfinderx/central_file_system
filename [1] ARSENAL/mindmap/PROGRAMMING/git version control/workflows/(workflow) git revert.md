# Internal Reference
>#programming
>[[git general tome]]
--- 
### Revert workflow
- <mark class="hltr-orange">git revert HEAD</mark> - revert the latest commit
- <mark class="hltr-orange">git revert <**commit-hash**></mark> - reverts specific node
### Reverting to old commits
- only reverts the files that was changed on the targetted commit (commit-hash)
- commit-hash can be seen on the first column of the git log using 'git log --oneline' command
### Removing the unpushed commit
- <mark class="hltr-orange">git reset HEAD~1</mark> - 1 means remove 1 commit
