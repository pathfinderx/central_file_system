# Internal Reference
>#programming
>[[git general tome]]
--- 
### Remove Workflow
- #### Remove file using unix command
	- <mark class="hltr-orange">rm -rf <**filename**></mark>
		- remove .git
		- remove that directory as master
		- -rf flag means force delete (makes the deletion faster)
			- Force deletion of everything in root directory
		- <mark class="hltr-orange">rm <**path**></mark> - remove directory
- #### Remove file / Staged / Cache using git command
	- <mark class="hltr-orange">git rm -rf <**path or filename**></mark> - remove directory or file
	- <mark class="hltr-orange">git restore --staged <**file**></mark> - remove from staging area
	- <mark class="hltr-orange">git restore <**file**></mark> - remove from unstaged area or discard
	- <mark class="hltr-orange">git rm -rf --cached <filepath></mark> - remove cached files to make the .gitignore work
- #### Delete unfinished branch
	- <mark class="hltr-orange">git branch -d bugfix/execute-initial-commit</mark>
- #### Delete branch
	- <mark class="hltr-orange">git branch -d <**branch name**></mark>
		- e.g: <mark class="hltr-orange">git branch -d feature</mark>