# Internal Reference
>#programming
>[[git general tome]]
--- 
### Unix
- <mark class="hltr-orange">ctrl + L</mark> - clear
- <mark class="hltr-orange">cd ..</mark> - reverse directory to one level
- <mark class="hltr-orange">cdls() { cd "$@" && ls; }</mark> - used to cd and if successful, ls to that directory
	- <mark class="hltr-orange">cdls **path**</mark>
		- used to cd then ls to the target directory
		- [[https://unix.stackexchange.com/questions/20396/make-cd-automatically-ls#:~:text=The%20%26%26%20means%20'%20cd%20to,the%20cd%20worked%20or%20not. | web reference]]
- <mark class="hltr-orange">cmd //c tree</mark> - tree view
	- [[https://www.tecmint.com/linux-tree-command-examples/ | web reference]]
- <mark class="hltr-orange">touch **flag** **filename**</mark> 
	- '-c' = to check if there are any existing files
### Vim
<mark class="hltr-orange">:q</mark> or <mark class="hltr-orange">:q!</mark> - to quit vim
### Nano
<mark class="hltr-orange">nano **filename with extension**</mark> - to edit using nano