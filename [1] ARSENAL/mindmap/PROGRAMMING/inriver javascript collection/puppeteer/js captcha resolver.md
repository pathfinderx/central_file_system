
## Internal Reference
> #javascript 
>[[spider inriver general tome | return to home]]
---
### setup
- #### modules
	- add to package.json
	```js
	"puppeteer-extra-plugin-recaptcha": "^3.6.6" // 3.6.6 as latest version (12-07-2022)
	```
	- execute npm to install the modules declared in package.json
	```js
	npm install
	```
	- check if npm installed
	```js
	npm list
	```

- #### spider
	```js
	// put this on top
	const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha')
	```
	```js
	// declare this after puppeteer.use(StealthPlugin())
	// use api token generated from youre 2captcha account but must pay 2.99 dollars first for captcha v2
	puppeteer.use(
		RecaptchaPlugin({
		  provider: {
			id: '2captcha',
			token: '1a5fd5db596900ff240b25d0d2d3ca22'
			// token: 'c1b3d2aae777ddb883c70072bf7978e9'
		  }
		})
	)
	```
	```js
	// use browser.newPage()
	const page = await browser.newPage() 
	```
	```js
	const { solved, error } = await page.solveRecaptchas();
	// prints ERROR_ZERO_BALANCE if paid
	```
---
### References:
- [docs > 2captcha step by step process](https://www.npmjs.com/package/puppeteer-extra-plugin-recaptcha)
- [Error > Puppeteer recaptcha sending back a TypeError "is not a function"](https://www.reddit.com/r/puppeteer/comments/l2j55v/puppeteer_recaptcha_sending_back_a_typeerror_is/)
- [Error > recaptcha methods are missing on initial browser's page](https://github.com/berstend/puppeteer-extra/issues/353)
- [youtube > process.env, captcha content checker sample](https://www.youtube.com/watch?v=D52NjoZWn14)
- [youtube > other practical example](https://www.youtube.com/watch?v=awKW5H7XXRs)