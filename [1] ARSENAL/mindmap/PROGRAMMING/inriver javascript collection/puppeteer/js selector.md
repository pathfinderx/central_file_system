## Internal Reference
> #javascript
>[[spider inriver general tome | return to home]]
---

#### JS Selector 
 1. #####  for button with multiple attributes or data ^08e923
	 - [[C:\repositories\puppeteer-comparison-spiders\strategies\us\microsoft-com-en-us.js | ref: spider file (233)]]
	   
		~~~js
	   let selector3 = `.tileproductplacement button[data-pid="${variantId}"][data-inventoryid="${inventoryId}"]`
		~~~ 
1. ##### loop using 
	1. for (var x)
	2. for of
	3. foreach
4. ##### safety rendering of button click
5. ##### page scroll