## Internal Reference
> #javascript 
>[[spider inriver general tome | return to home]]
---
### 3 Types of loop
1. #### Conventional for loop
	- [[C:\repositories\puppeteer-comparison-spiders\strategies\be\mediamarkt-be-nl.js | ref: js file > description extraction]]
	```py
	let descriptionElement = document.querySelector("#productomschrijving-")
	
	if (descriptionElement) {
		let childNodes = descriptionElement.childNodes
		for (let i = 0; i <= childNodes.length - 1; i++) {
			let temp = childNodes[i].textContent.trim()
			if (!!temp && temp.toLowerCase() != "omschrijving") {
				description.push(temp)
			}     
		}
	```
1. #### Using foreach
	-  [[C:\repositories\puppeteer-comparison-spiders\strategies\be\mediamarkt-be-nl.js | ref: js file > video extraction]]
	- [[C:\repositories\puppeteer-comparison-spiders\strategies\ca\walmart-ca.js | ref: js file > image extraction > as other sample]]
	```py
	let videoElement = document.querySelectorAll('.isitetv-gallery-poster-inner-wrapper video source');
	if (videoElement.length > 0){
		videoElement.forEach((videoTag)=>{
			if (videoTag.hasAttribute('src')){
				valuesArray.push(videoTag.getAttribute('src'))
			}
		});
	}
	```
1. #### Using for of
	- [[C:\repositories\puppeteer-comparison-spiders\strategies\fr\microsoft-com-fr-fr.js | ref: js file > variant color seletion]]
	```js
	let color_in_selection = colorBtn.getAttribute('title').toLowerCase()
	if (color_in_selection.length > 0) {
		for (let x of tile_options) {
			hasColorInTiles = x.innerText.includes(color_in_selection) ? true : false 
			break  
		}
	}
	```