# Internal Reference

>#programming
---
## VSCode shortcuts
 - [[code editor vscode | vscode shortcuts]]
## Obsidian shortcuts
- [[obsidian shortcuts]]
## Sublime shortcuts
- [[code editor sublime | sublime shortcuts]]