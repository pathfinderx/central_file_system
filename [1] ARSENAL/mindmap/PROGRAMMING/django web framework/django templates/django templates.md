# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Templates
- ### Setup
	- [[django fundamentals#^cc31f3 | template directory creation]]
	- Adding app class to settings.py under INSTALLED_APPS list or object
		- <mark class="hltr-orange">core.apps.CoreConfig</mark>
	- Registration of all paths from specific app to the main project (default directory) urls.py
		- <mark class="hltr-orange">from django.urls import include</mark>
		- path('', include('core.urls'))
	- Basic http response for quick testing purposes
		- <mark class="hltr-orange">from django.http import HttpReponse  </mark>
- ### Template inheritance
	- <mark class="hltr-orange"> {% include 'navbar.html' %}</mark> - inherit all from specific template
	- <mark class="hltr-orange">{% block content %}</mark> - inherit specific block
- ### Rendering
	- <mark class="hltr-orange">from django.shortcuts import render, redirect </mark>
	- <mark class="hltr-orange">render(request, 'core/sample.html', dictionary)</mark>
		- params(request, \<template directory>, data)
	- <mark class="hltr-orange">redirect ('\<path name>')</mark>
	- #### Wrapping 
		- {{ form.as_p }} - wrap the built-in form with p tag