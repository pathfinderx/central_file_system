# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Flash Message

- <mark class="hltr-orange">from django.contrib import messages</mark> - import this
- Common methods: - [flash message external link](https://docs.djangoproject.com/en/4.1/ref/contrib/messages/)
	```py
	messages.debug(request, '%s SQL statements were executed.' % count)
	messages.info(request, 'Three credits remain in your account.')
	messages.success(request, 'Profile details updated.')
	messages.warning(request, 'Your account expires in three days.')
	messages.error(request, 'Document deleted.')
	```
- ### Usage
	- app views.py - messages are instantiated here
		```py
		from django.contrib import message
		
		def loginUser(request):
		    if request.method == 'POST':
		        try:
		            user = User.objects.get(username=username)
		        except Exception as e:
		            messages.error(request, "User does not exist")
	
	    # statements...
		```
	- app > template/app > navbar.html
		```py
		{% if messages %} # condition if messages are instantiated on backend or in views.py
			<div>
				{% for message in messages %} # loop the message
					<h3>{{ message }}</h3> # get the message
				{% endfor %}
			</div>
		{% endif %}
		```