# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Filters, guards, restrictions
- ### Decorators for logged-in session (Restriction)
	- <mark class="hltr-orange">from django.contrib.auth.decorators import login_required</mark>
	- <mark class="hltr-orange">login_required(login_url="login")</mark>
		- parameters(options="\<url or path name>")
		- redirects to specified path if condition is not met (no user session)
	```py
	from django.contrib.auth.decorators import login_required

	@login_required(login_url="login") # decorator on top of the function. Redirects to login path if no session
	def createRoom(request):
		statements...
	```
- ### restrictions (filters or guards = in laravel perspective)
	```py
	{% if room %}
		{% for x in room %}
			{% if request.user == x.host %} # restriction here: match if current user is equals to room user (fk)
				<a href="/update-room/{{x.id}}">Update room</a>
				<a href="/delete-room/{{x.id}}">Delete room</a>
				<!-- <a href="/room/{{x.id}}"><p>{{x.name}}</p></a> -->
			{% endif %}
			<hr>
		{% endfor %}
	{% else%}
	```