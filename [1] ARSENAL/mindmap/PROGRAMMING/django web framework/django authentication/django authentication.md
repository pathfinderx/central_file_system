# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Authentication
- ### Login
	- app -> template/core -> login_page.html
		```html
		<form method="POST" action="">
			{% csrf_token %}
		
			<label for="">Username:</label>
			<input type="text" name="username" placeholder="Enter username">
		
			<label for="">Password:</label>
			<input type="password" name="password" placeholder="Enter password">
		
			<input type="submit" value="Submit">
		</form>
		```
	- app -> urls.py
		```py
			path('login/', views.loginUser, name="login")
		```
	- app -> views.py
		```py
		from django.contrib.auth import authenticate, login
		
		def loginUser(request):
		    context = dict()
		    username = request.POST.get('username') # extract username input
		    password = request.POST.get('password') # extract password input
		    user = None
		    
		    if request.method == 'POST': # request method check
		        try:
		            user = User.objects.get(username=username) # check if user exist
		        except Exception as e:
		            messages.error(request, "User does not exist")
		
		        if user is not None:
		            user = authenticate(request, username=username, password=password) # authenticate
		            if user is not None:
		                login(request, user) # create session
		                return redirect('home') 
		
		    context = dict()
		
		    return render(request, 'core/login_page.html', context)
		```
- ### Logout
	- app -> template/core -> navbar.html
		```html
		</a>
		{% if request.user.is_authenticated %}
		<a href="/logout">Logout</a>
		{% else %}
		<a href="/login">Login</a>
		{% endif %}
		<hr>
		```
	- app -> urls.py
		```py
		path('logout/', views.logoutUser, name='logout'
		```
	- app -> views.py
		```py
		from django.contrib.auth import authenticate, login, logout
	
		def logoutUser(request):
			logout(request)
			redirect('home')
		```
- ### Filters, guards, retrictions
	- more on here: [[django guards, restrictions]]