## Internal Reference

>#researchanddevelopment

---
# django web framework general tome
- ##### getting started
- [[django fundamentals]]
- [[django database]]
	- [[django query filters]]
	- [[django forms]]
- [[django authentication]]
	- [[django guards, restrictions]]
- [[django flash message]]
- [[hotkeys]]
---
- #### Installations: > [[django fundamentals]]
	- <mark class="hltr-orange">pip install virtualenv</mark> = install virtual environment package if not installed globally
	- <mark class="hltr-orange">virtualenv venv</mark> = create virtual environment
	- <mark class="hltr-orange">source venv/Scripts/activate</mark> = activate virtual environment
	- <mark class="hltr-orange">pip install django</mark> = django installation

- #### Utilities for administrative tasks (Django admin)  > [[django fundamentals]]
	- <mark class="hltr-orange">django-admin startproject \<project_name> </mark>
	- <mark class="hltr-orange">django-admin startapp name</mark>
- #### Url dispather (djago.urls)  > [[django fundamentals]]
	- <mark class="hltr-orange">path()</mark> = Returns an element for inclusion in urlpatterns [[django fundamentals#Internal Reference#]]
	- <mark class="hltr-orange">re_path()</mark> = Returns an element for inclusion in urlpatterns
	- <mark class="hltr-orange">include()</mark> = A function that takes a <mark class="hltr-ash">full Python import path to another URLconf module</mark> that should be “included”
	- ##### UrlConfs Samples:
		```py
		# SPECIFIC APP
			# params: (route, views, name)
			path('', views.home, name="home")
		
			# params: (route/args, views, name)
			path('room/<str:key>/', views.room, name="room")
	
		# DEFAULT DIRECTORY
			from django.urls import path, include
			path('', include('core.urls')) # include

			# to test: # you can try to include the object that contains embedded namespace in default directory
			polls_patterns = ([
				path('', views.IndexView.as_view(), name='index'),
				path('<int:pk>/', views.DetailView.as_view(), name='detail'),
			], 'polls')
		```
	- [Urls docs](https://docs.djangoproject.com/en/4.1/topics/http/urls/)
	- ##### URLS in presentation layer
		- <mark class="hltr-orange">{{request.META.HTTP_REFERER}}</mark> = returns back to the former link or url
	- ##### More to explore:
		- [re_path()](https://docs.djangoproject.com/en/4.1/ref/urls/#re_path()) = usage of regex in urlConfs
		- [register_converter()](https://docs.djangoproject.com/en/4.1/ref/urls/#register-converter) = Helper function to return a URL pattern for serving files in debug mode
		- [include object with embedded namespace in default directory](https://docs.djangoproject.com/en/4.1/topics/http/urls/#term-instance-namespace)
- #### Database
	- ##### Models  > [[django database]]
		- <mark class="hltr-orange">register()</mark> = register the model
			```py
			# specific app -> adminpy
			from .models import Room
	
			admin.site.register(Room)
			```
		- <mark class="hltr-orange">objects</mark> = model manager
			```py
			# specific app -> views
			from .models import Room
	
			def room(request):
				varA = Room.objects.all()
				... statements
			```
			[[django database#^a48a9c| more on model management]]
	- ##### Built-in forms: > [[django forms]]
		- <mark class="hltr-orange">from django.forms import ModelForm</mark> - built-in model form
			- <mark class="hltr-orange">prefilled_form = ModelForm(instance=room_id)</mark> - prefilled form used for update operation
		- <mark class="hltr-orange">class Meta</mark> - any model modifications can be made here
		- <mark class="hltr-orange">{% csrf_token %} </mark> - secure token for post method
	- ##### Query filters: > [[django query filters]]
		- <mark class="hltr-orange">from django.db.models import Q</mark>
		- <mark class="hltr-orange">request.GET.get('query')</mark> 
- #### Flash Message: > [[django flash message]]
	- <mark class="hltr-orange">from django.contrib import message</mark> - import 
	- <mark class="hltr-orange">messages.error(request, "Message")</mark> - message creation (use exception or conditions)
	- <mark class="hltr-orange">{%if messages% }{% for message in messages %}{% message %}</mark> - condition and loop in template
- #### Authentication: > [[django authentication]]
	- ##### Login
		- <mark class="hltr-orange">from django.contrib.auth import authenticate, login</mark> - import 
		- <mark class="hltr-orange">user = authenticate(request, username=user, password=pass)</mark> - authenticate
		- <mark class="hltr-orange">login(request, user)</mark> - create session
	- ##### Logout
		- <mark class="hltr-orange">{% if request.user.is_authenticated %}</mark> - for template, session checker
		- <mark class="hltr-orange">from django.contrib.auth import logout</mark> - import
		- <mark class="hltr-orange">logout(request)</mark> - flush current session
	- ##### Restriction using decorator
		- <mark class="hltr-orange">from django.contrib.auth.decorators import login_required</mark> - import
		- <mark class="hltr-orange">@login_required(login_url='login')</mark> - decorator on top of the function
			- params(options='\<path or url name>')
	- ##### Guards or restriction
		- <mark class="hltr-orange">{% if request.user == room.host %}</mark> - just make if statement on template
- #### manage.py: > [[django fundamentals]]
	- <mark class="hltr-orange">python manage.py runserver</mark> = to run server 
	- <mark class="hltr-orange">python manage.py migrate</mark> = to migrate database models
	-  <mark class="hltr-orange">python manage.py makemigrations</mark> = instantiate the database model
	- <mark class="hltr-orange">python manage.py createsuperuser</mark> = creates an admin user