# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
#### What is Django
- Python web framework
- Server side framework
- Batteries included
- MVT design pattern (MODEL VIEW TEMPLATE)
	- [MODEL VIEW TEMPLATE](https://www.google.com/search?q=what+is+mvt+framework&rlz=1C1PASC_enPH982PH982&oq=what+is+mvt+framework&aqs=chrome..69i57j0i390l3.3615j0j7&sourceid=chrome&ie=UTF-8#imgrc=FI_OJak3RuQ0eM)
	- [[mvt.png]]
	- MODEL = Database
	- VIEW = Business logic (uri = uniform resource identifier)
	- TEMPLATES =Presentation layer 
	- reason for no controller (MVC) 
		- django takes care of the controller aspect

#### Compatibility
- [django version > python compatibility](https://docs.djangoproject.com/en/4.1/faq/install/)

#### Environment Setup
- ##### In VScode
	- use auto rename tag (auto pair tag or auto edit tag (the start and end))
- ##### Installations:
	- <mark class="hltr-orange">pip install virtualenv</mark> = install virtual environment package if not installed globally
	- <mark class="hltr-orange">virtualenv venv</mark> = create virtual environment
	- <mark class="hltr-orange">source venv/Scripts/activate</mark> = activate virtual environment
	- <mark class="hltr-orange">pip install django</mark> = django installation

#### File System on default directory
- **manage.py** = execute commands from
- **db.sqlite3** = default database 
- **wsgi.py** = web server gateway interface -> server
- **asgi.py** = asynchronous server gateway interface -> async calls
- **urls.py** = routing or uri setup
- **settings.py** = core project configuration or the command center for the entire project
	- ##### contains:
		- installed apps
		- middlware
		- templates
		- database

#### Initial launch
- <mark class="hltr-orange">django-admin</mark>
	- command-line utility for administrative tasks
	- same thing with manage.py
	- more here: [django-admin on docs](https://docs.djangoproject.com/en/4.1/ref/django-admin)
	- ##### (django-admin startproject) Starting a project and running a server:
		- <mark class="hltr-orange">django-admin startproject \<project_name></mark>
			- Creates a Django project directory structure for the given project name in the current directory or the given destination
			- then cd to \<project_name>
		- <mark class="hltr-orange">python manage.py runserver</mark>
			- turn on server
			- checkout here: http://127.0.0.1:8000/
		- ###### Other commands to try:
		<mark class="hltr-orange">	$ django-admin <command> [options]
			$ manage.py <command> [options]
			$ python -m django <command> [options]</mark>
	- ##### (django-admin startapp) Creating an app
		- <mark class="hltr-orange">django-admin startapp name [directory]</mark>
			- Creates a Django app directory structure for the given app name in the current directory or the given destination.
			- *eg* <mark class="hltr-orange">django-admin startapp myapp</mark>
				- If only the app name is given, the app directory will be created in the current working directory.
			- *eg: *<mark class="hltr-orange">django-admin startapp myapp /Users/kurt/code/myapp </mark>
		- ###### Views and urls using the default project directory (django_test)
			- Add the newly created app to the settings
				- [[adding app to settings.png]]
			- add function and basic display with route to the urls.py
				- [[Screenshot_161.png | adding function as return with route]]
				- [[Screenshot_162.png | sample result]] 
		- ###### Views and urls using the specific app (core)
			- Create function for route inside view.py
				- [[Screenshot_163.png | adding functions for route]]
			- Create urls.py and add urls with reference the function
				- [[Screenshot_164.png | adding url from reference function]]
			- Add include function in default directory's urls.py
				- [[Screenshot_165.png | adding include from specific app to default direcctory's urls.py]]
		- ###### Template (Presentation layer) ^cc31f3
			- **==Option1==**: create directory ***templates*** level with default directory
			- **==Option2==**: create templates inside specific app
				1. create directory inside specific app and name it **templates**
				2. it is required to add another directory name (and notice after creating one, file structure converted into this:)
					- [[Screenshot_168.png | directory converted]] 
			- register template to ***settings.py*** on default directory
				- [[Screenshot_166.png | add templates to settings.py]]
				- ==note: BASE_DIR== -> refers to base directory of the current project
			- modify the function in views.py from specific app
				```py
				def home (request): 
					return render(request, 'home.html') # use render function instead of HttpResponse
				```
			- ###### template inheritance or template engine
				- for reusable templates
					- [[Screenshot_167.png | using includes]]
				- practical example:
					- [[Screenshot_169.png| includes the navbar to the main template]]
					- [[Screenshot_170.png | extend the content from super template to child template]]
					- usable codes:
						```html
						{% includes 'navbar.html' %} -> includes portion of content
						
						{% block block_name %} -> creating blocks for reusable purpose
						{% endblock %}
					
						{% extends 'main.html' % } -> extends from template that has declared block content
						{% block block_name %} -> override the content
						{% enblock %}
						```
					- [templating engine](https://docs.djangoproject.com/en/4.1/topics/templates/)
				- <mark class="hltr-red">Issues encountered: 'TemplateDoesNotExist at/''</mark>
					- encountered after executing the option2 or creating template inside specific app
					- <mark class="hltr-blue">fixed: make sure theres value in **INSTALLED_APPS**</mark>
					- [more info:](https://stackoverflow.com/questions/5686044/templatedoesnotexist-at)
			- ###### rendering
				- **Using loop**
					- **views.html**
						```py
						rooms = [
							{'id': 1, 'name':'Kurt'},
						]
						
						def home(request):
							return render(request, 'core/home.html', {'rooms':rooms})
						```
					- **home.html**
						```html
						{% for x in rooms %}
							<p>{{x.name}}</p>
						{% endfor %}
						```
			- ###### dynamic url routing
				- set dynamic route:
					```py
					# home.html -> as url trigger
					{% for x in rooms %}
						<a href="/room/{{x.id}}"><p>{{x.name}} -- room</p></a>
					{% endfor %}
					    
					# urls.py  -> as receiver and assignment
					path('room/<str:key>/', views.room, name="room") # accept id as str and assign as variable key

					# views.py -> receiver/modifer/passer 
					def room(request, key):
					    value = None
					    for x in rooms:
					        if ('id' in x and 
					            'name' in x and
					            x.get('id') == int(key)):
					
					            value = x
					    context = {'value':value}  
					        
					    return render(request, 'core/room.html', context)

					# room.html -> presentation layer
					{% block content %}
					<h1>{{value.name}} - Room</h1> 
					{% endblock %}
					```