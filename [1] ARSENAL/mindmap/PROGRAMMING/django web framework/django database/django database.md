# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
#### Database
- ##### Models / Migration
	 - <mark class="hltr-orange">python manage.py migrate</mark>
		 - executes built-in migration
		 - refer to: **default_directory -> settings.py** -> **INSTALLED_APPS**
			 - identify the predefined migration
			 - more to research... 
	- ###### create model: **specific app -> models.py**
		```py
		from django.db import models
		
		class Room(models.Model):
			name = models.CharField(max_length=200)
			description = models.TextField(null=True, blank=True) # optional to add input or non required
			updated = models.DateTimeField(auto_now=True)  # auto_now takes snapshot on every update
			created = models.DateTimeField(auto_now_add=True) # auto_now_add only snapshot upon creation
			
			def __str__ (self):
				return str(self.name) # string representation of the model. It can be any attributes
		```
	 - ###### to execute the newly created model:
		 - <mark class="hltr-orange">python manage.py makemigrations</mark>
				 - responsible for creating new migrations based on the changes made on model
			 - migrations will now be instantiated on: **specific app -> migrations -> 0001_initial.py**
			 - see here: [[Screenshot_159.png | instantiated model]]
		 - <mark class="hltr-orange">python manage.py migrate</mark>
			 - responsible for applying migration
	 - ###### creating models for admin panel
		 - http://127.0.0.1:8000/admin/ = The admin panel
			 - Built-in admin panel
			 - Auto creates UI parallel to the created attributes of model
			 - Can execute UI CRUD operation base on the attributes declared from the instantiated model
		 1. Create Super user using command: <mark class="hltr-orange">python manage.py createsuperuser</mark>
		 1. Register first in admin model using command: <mark class="hltr-orange">admin.site.register(ModelName)</mark>
			 - [[Screenshot_173.png | model registration in model]]
		1. Test retrigger <mark class="hltr-orange">python manage.py makemigration</mark> then <mark class="hltr-orange">python manage.py  migrate</mark> to make changes to admin portal model instantiation
		1. Check if tables are added on admin panel: http://127.0.0.1:8000/admin/
		 - more on **models**: [[Screenshot_172.png| models image]]
	 - ###### model queryset  ^a48a9c
		 - [[Screenshot_174.png | query set sample]]
		```py
		from .models import ModelName # import model first before making any queries

		query = ModelName.objects.all() # all() retrieves all objects from table
		query = ModelName.objects.get(attribute='value') # single object that matches on attribute

		# all items from table that matches to attribute
		query = ModelName.objects.filter(attribute='value')
			ModelName.objects.filter(attribute_startswith='value')
			ModelName.objects.filter(attribute_contains='value')
			ModelName.objects.filter(attribute_icontains='value')
			ModelName.objects.filter(attribute_gt='value') # greater than
			ModelName.objects.filter(attribute_gte='value')
			ModelName.objects.filter(attribute_lt='value') # less than
			ModelName.objects.filter(attribute_lte='value') 

		query = ModelName.objects.exclude(attribute='value') # excludes any object matching a filter
		
		query = ProjectModel.objects.filter(title='value').order_by('v1', 'v2') # order with multiple params
		query = ProejectModel.objects.filter(title='value').order_by('-v1', '-v2') # starting with '-1' is reversed order

		# CRUD
		query = ModelName.objects.create(attribute='value') # create instance of a model

		query = ModelName.objects.get(attribute='value')
		query.title = 'value'
		query.save() # save changes to a particular object

		query = MOdelName.objects.last()
		query.delete() # deletes particular object

		# OTHER
		query = ModelName.objects.first()
		query.childmodel_set.all() # query models children

		# RELATIONSHIPS
		query = ModelName.objects.first()
		query.relationshipname.all()

		query = ModelName.objects.first()
		otheritem = OtherModule.objects.create(attribute='value')
		query.relationship.add(otheritem)
		
		```  
	- ###### Creating foreign key
		- using <mark class="hltr-orange">models.ForeignKey(args, args)</mark>
			- when adding foreignkey, the reference class must be above the current class that uses it
			- else you can do it this way:
				- <mark class="hltr-dark">topic = models.ForeignKey('Topic', args...)</mark>
				- just enclose the model refence with single qoute
		```py
		class Topic(models.Model):
			... statements

		class Room(models.Model):
			topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True) # when setting to null
		```
	- ###### Creating the build-in User model:
		- build-in user model more on here: [external link](https://docs.djangoproject.com/en/4.1/ref/contrib/auth/)
		- just import this: <mark class="hltr-orange">django.contrib.auth.model import User</mark>
	- ###### Issues:
		- <mark class="hltr-red">"no such table" exception</mark> or <mark class="hltr-red">"no such column" exception</mark>:
			- delete **db.sqlite3**,  **pycache directory**, **pycache from migration**(if first option fails)m  **migration model**
			- execute: <mark class="hltr-orange">**python manage.py makemigrations**</mark> first
					- execute:  <mark class="hltr-orange">**python manage.py migrate**</mark>
			- [more on here: stackoverflow solution](https://stackoverflow.com/questions/34548768/no-such-table-exception)
- ##### Forms
	- [[django forms]]
		- CRUD operations
- ##### Filters
	- [[django query filters]]
		- query filters