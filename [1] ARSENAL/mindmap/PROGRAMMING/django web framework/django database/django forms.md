# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Forms
- #### Using built-in ModelForm
	- ready made forms design
	- only requires <mark class="hltr-orange">from django.forms import ModelForm</mark> or <mark class="hltr-orange">ModelForm</mark>
	- ##### Instantiation of build in ModelForm
		```py
		# specific app -> forms.py
		from django.forms import ModelForm # import this
		from .models import Room # import the model for forms creation
		
		class RoomForm(ModelForm): # inherit ModelForm
			class Meta: # create class meta
				model = Room # can be accessed from other classes, returns predefined designs on all editable fields
				fields = '__all__' # form creation on all editable fields (you can specify fields using list or you can exclude fields)
		```
		```py
		# specific app -> views.py
		from .forms import RoomForm # import the RoomForm class from forms.py
	
		def createRoom(request):
			context = {'room_form':RoomForm} # pass the RoomForm class
			return render(request, 'core/room_form.html', context)
		```
		```html
		<!-- specific app -> templates/core/room_form.html -->
		{% extends 'main.html' %}
	
		{% block content %}
		    <div>
		        <form action="post" acton="">
		            {% csrf_token %} 
		            {{ room_form }} # render the context from backend (from forms.py -> RoomForm.Meta.model)
		            {{ room_form.as_p }} # or you can wrap it for better design
		            <input type="submit" value="Submit"/>
		        </form>
		    </div>
		{% endblock %}
		```
	- ##### Create
		```html
		<!-- specific app -> room_form.html -->
		{% extends 'main.html' %}
	
		{% block content %}
		    <div>
		        <form method="post" action=""> <!-- indicate the method="post"; action="" -> send back to current url -->
		            {% csrf_token %} 
		            {{ room_form.as_p }}
		            <input type="submit" value="Submit"/>
		        </form>
		    </div>
		{% endblock %}
		```
		```py
		# specific app -> views.py
		from django.shortcuts import render, redirect # import the redirect
		from .forms import RoomForm # from specific app -> forms.py -> RoomForm (class)

		def createRoom(request):
			context = {'room_form':RoomForm}
			if request.method == 'POST': # html form -> method="POST"
				form = RoomForm(request.POST) # instantiate RoomForm as variable
				if form.is_valid: # validate
					form.save() # save
					return redirect('home') # usage of redirect using url name
			return render(request, 'core/room.html', context)
		```
	- ##### Update
	```py 
	# specific app -> urls.py
	path('update-room/<str:pk>', views.updateRoom, name='update-room') # pass in the primary key from template
	```
	```py
	# specific app -> views.py
	def updateRoom(request, pk):
		room_id = Room.objects.get(id=pk)
		prefilled_form = ModelForm(instance=room_id) # instantiate values with reference primary key to prefill the form
		context = {'room_form': prefilled_form} # key should be same with the createRoom() because it uses the same template
	
		if request.method == 'POST': # when submitting the button
			form = ModelForm(request.POST, instance=room_id) # submitted form using POST method with target id as 2nd param
			if form.is_valid:
				form.save()
				return redirect('home')
		return render(request, 'core/room_form.html', context) # render using prefilled values
	```
	- ##### Delete
	```py
	# app -> urls.py
	path('delete-room/<str:pk>/', views.deleteRoom, name='delete-room')
	```
	```py 
	# app -> views.py

	def deleteRoom(request, pk):
		room_query = Room.objects.get(id=pk)
		if request.method in ['POST']:
			room_query.delete()
			return redirect('home')
	```
	- ##### Ordering
	```py
	# specific app -> models.py
	from django.db import Models

	class Room (models.model):
		... statements
		class Meta:
			ordering = ['-updated'] # just select from model.attributes; '-' means descending order
	```
- #### User creation form
	- <mark class="hltr-orange">from django.contrib.auth.forms import UserCreationForm</mark>
		``` py
		<form method="POST" action="">
			{% csrf_token %}
			{{ form.as_p }} # wrap the built-in form
			<input type="submit" value="Register">
		</form>
		```
	
		```py
		from django.contrib.auth.forms import UserCreationForm # built-in user creation form
		
		def registerUser(request):
			form = UserCreationForm()
			context = {'form':form}
		
			if request.method == 'POST':
				form = UserCreationForm(request.POST) # can be sent as data or context 
				if form.is_valid(): # validate
					user = form.save(commit=False) # commit option set to False allows to manipulate the data
					user.username = user.username.lower() # auto covert to lowercase
					user.save() # commit
					login(request, user) # auto login after registration
					return redirect('home')
				else:
					messages.error(request, 'Error occured during user registration')
			return render(request, 'core/registration_page.html', context)
		```