# Internal Reference
>#researchanddevelopment
>[[django general tome| return to home]]
---
## Filters
- ### Filter through search
	- Extractions from template
		- navbar.html
			```html
			<form method='GET'action="{% url 'home' %}">
				<input type="text" name="query" placeholder="Search here ... "> <!-- named as 'query' for search query in backend -->
			</form><hr>
			```
		- home.html
			```html
			<div>
				<a href="/">All</a><br>
				{% for x in topics %}
					<a href="{% url 'home' %}?query={{ x.name }}">{{ x }}</a><br> <!-- http://127.0.0.1:8000/?query=Javascript -->
				{% endfor %}
			</div>
			```
	- app -> urls
		```py
		path('', views.home, name="home"),
		```
	- app -> views.py
		- import advanced filters to make the query flexible then do the query
			```py
			from django.db.models import Q # filters
			def home(request):
				search_query = request.GET.get('query') if request.GET.get('query') is not None else ''
				queryRoom = Room.objects.filter(
					Q(topic__name__contains=search_query) | # search by topic
					Q(name__icontains=search_query) | # search by name
					Q(description__icontains=search_query) # search by description
				) 
				queryTopic =  Topic.objects.all()
				
				context = {'room':queryRoom, 'topics':queryTopic}
				return render(request, 'core/home.html', context)
			```