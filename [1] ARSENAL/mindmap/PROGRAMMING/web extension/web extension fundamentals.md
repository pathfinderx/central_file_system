## Internal Reference

>#researchanddevelopment

---
# Fundamentals
- edit in manifest.json
- #### What is manifest.json
	- make manifest.json
	- make html
	- sign the extension created to firefox
		- more on mozilla docs
	- debuggin in sign in : 
		- make sure persisted debugging is on 
	- advantage of firefox: has rect 
	- disadvantage of chrome: has no rect, scroll bottom to up > series of imgs in list > stitch
		- a lot of work! 
- #### Deployment
	- needs to be signed
	- <mark class="hltr-orange">web-ext build</mark>
		- convert to zip
	- <mark class="hltr-orange">web-ext lint</mark>
	- install est firefox