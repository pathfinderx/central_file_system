## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
### Programming terminologies
- boilerplate 
	- In Information Technology, a boilerplate is a unit of writing that can be reused over and over without change. By extension, the idea is sometimes applied to reusable programming, as in “boilerplate code.”
	- [[https://www.freecodecamp.org/news/whats-boilerplate-and-why-do-we-use-it-let-s-check-out-the-coding-style-guide-ac2b6c814ee7/ | external link]]
- "Strong opinions, losely held"
	- [external link](https://tapandesai.com/strong-opinions-loosely-held/)
	- Expound conclusion no matter how imperfect - Strong opinions
	- lose the current principles and beliefs by accepting different ideas (though completely contradiction) - losely held
	- Or it is about being open minded and ready to lose the well founded principle
		- It is important to know the other half of the truth
		- In this way we are proving ourselves wrong and in that way we are making progress
### Algorithm
- "Designing the right algorithm as solution the the problem only provides half of the work. To complete the job, it must be done efficiently" - [external link](https://books.google.com.ph/books?id=JA0FEAAAQBAJ&pg=PR9&dq=algorithms+play+an+important+role+in+our+society,+solving+numerous&hl=en&sa=X&ved=2ahUKEwie1ubFjs77AhVvsVYBHUeCAZQQ6AF6BAgCEAI#v=onepage&q=algorithms%20play%20an%20important%20role%20in%20our%20society%2C%20solving%20numerous&f=false)