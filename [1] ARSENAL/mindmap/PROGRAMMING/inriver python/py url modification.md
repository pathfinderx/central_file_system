## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
### URL Modification
1. #### access base url
	~~~py
	f'https://{self.__class__.WEBSITE}'
	~~~

1. #### parsing string
	```py
	import urllib.parse

	myString = 'hey / 23'
	myStringConverted = urllib.parse.quote_plus(myString)

	console:
	>>> myStringConverted
	>>> 'hey+%2F+23'
	```
	
1. #### parsing
	~~~py
	import urllib, json

	payload = {
		...
	}

	# parse the url
	parsed_url = urllib.parse.urlencode(payload)

	# dump the url through requester (only for post request)
	res = requester.post(..., data=json.dumps(payload))
	~~~
	1. #### more parsing examples
		[[C:\repositories\physical-stores-spiders\strategies\download\dk\elgiganten_dk.py | ref: python file]]
		```py
		from urllib.parse import urlencode, quote
		
		articleNumber = filtered_sku if filtered_sku else _sku
	            
		payload = {
			'appMode': 'b2c',
			'user': 'anonymous',
			'operationName': 'getStoresByLocationWithProduct',
			'variables': '{"articleNumber":'+ json.dumps(articleNumber) +',"latitude":56.26392,"longitude":10.501785,"thresholdKm":-1,"myStoreId":null}',
			'extensions': '{"persistedQuery":{"version":1,"sha256Hash":"5a309147ad24e1022e4253c673b45a17dbded642c48383ba71850f330a6ef568"}}'
		}
	
		queries = urlencode(payload, quote_via=quote)
		url = 'https://www.elgiganten.dk/cxorchestrator/dk/api?{}'.format(queries)
		```
1. #### modifying params if json.dumps() and urlencode() : doesnt work
 [[C:\repositories\ranking-spiders\strategies\download\es\mediamarkt_es.py | ref: python file]], 
 [[C:\repositories\ranking-spiders\strategies\download\es\mediamarkt_es.py  | ref: python file 2]]
 
	~~~py
	params = {}
	params['operationName']='GetRecoProductsById'
	params["variables"] = {
		"ids":product_ids,
		"withMarketingInfos":'false'
	}
	params["extensions"] = {
		"persistedQuery":{
			"version":1,
			"sha256Hash":"ba1251c7568dcaaa8829135e77f9a490820e5cb020a2336df176894ec4cddcae"
		},
		"pwa":{
			"salesLine":"Media",
			"country":"ES",
			"language":"es"
		}
	}

	headers = self.get_headers()
	headers['x-operation'] = 'GetRecoProductsByIds'

	url = 'https://www.mediamarkt.es/api/v1/graphql?operationName={}&variables={}&extensions={}'.format(params['operationName'],params['variables'],params['extensions'])
	url = url.replace(' ','').replace("\'", '"').replace('"false"','false')
	~~~
	
1. #### Other Options
	~~~py 
	quote()
	json.dumps()
	json.dumps(variables, ensure_ascii=False)
	~~~
	