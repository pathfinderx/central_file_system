## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
-- 

# Download Strategies
- #### Dynamic cookies
	-  ##### import

		``` py
		import cloudscraper	
		```

	-  #####  Initialize

		``` py
		def __init__ (self, requester):
			self.requester = requester
			self.scraper = cloudscraper.create_scraper()
			self.scraper.proxies = requester.proxies # obsolete?
	
		```

	-  #####  Function

		``` py
		def get_cookies (self, url):
			res = self.scraper.get(url, proxies = self.requester.session.proxies)
			cookies = res.cookies.get_dict()
			if cookies:
				cookies['pv'] = 'list'
			return cookies
		```

	-  #####  Variable Assignment

		``` py
		def download(self, url, postal_code = None, timeout=10, headers=None, cookies=None, data=None):
		# ..... statements .....
	
		cookies = self.get_cookies(url) # this
		try:
			res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
	
			# ..... statements .....
		except Exception as e:
			raise DownloadFailureException ('Download failed - Unhandled exception')
		```

	- More references
	[[ C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\us\grainger_com.py | grainger_com.py]]
	[[C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\br\submarino_com_br.py | submarino_com_br.py]]
	
	- More Statements
		``` py
		Effective on invalid deadlink issue specially when using unblocker of cloudscraper
		# has data on local using cloudscraper but invalid deadlink on main server
		# applied dynamic cookies (cloudscraper cookies) with normal requester
		# worked smoothly on main server
	
		```
---
- #### kwargs optional dynamic cookies or cloudscraper requester.get
	-  ##### Function

		``` py
		def request_data_using_scraper (self, url, timeout, headers, cookies, **kwargs):
		try:
			res = None
			if kwargs['state'] == 'cookie':
				cookies = self.get_cookies(url)
				res = self.requester.get(url, timeout=timeout, headers=headers, cookies=cookies)
			elif kwargs['state'] == 'scraper':
				res = self.scraper.get(url, timeout=timeout, headers=headers, cookies=cookies)
	
			if res.status_code in [200,201]:
				product_list = self.get_raw_data(res.text)
				return product_list
	
			if not self.next_page(res.text):
				None
	
		except Exception as e:
			print(e)
		``` 

	-  ##### Variable assignment 

		``` py
		try:
			product_list = self.request_data_using_scraper(urls[page], timeout, headers, cookies, state='cookie')
			if product_list:
				raw_data_list.append(product_list)
			else:
				product_list = self.request_data_using_scraper(urls[page], timeout, headers, cookies, state='scraper')
				if product_list:
					raw_data_list.append(product_list)
		except Exception as e:
			print(e)
		``` 
---
 - #### Unblocker Cookies
	-  ##### import
	
		``` py
		from request.unblocker import UnblockerSessionRequests
		```
	-  ##### initialize
	
		``` py
		self.unblocker = UnblockerSessionRequests('us')
		```
	-  ##### Variable assignment
	
		``` py
		cookies = self.unblocker.session.cookies.get_dict()
		res = self.requster.get(url, timeout=10, headers=headers,cookies=cookies)
		```
	- ##### Using super in init
	```py
	class AmazonDeDownloadStrategy(DownloadStrategy):
	    def __init__(self, requester):
	        super().__init__(requester)
	        self.unblocker = UnblockerSessionRequests('de')
	```
---
 - #### Redirect
	
	``` py
	for history in res.history:
		if history.status_code in [302, 301]:
			raw_data = None
			break

	```
- #### Acceptable Redirect
```py
	# statements...
```
---


