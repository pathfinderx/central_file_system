## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
#### Checkers
 - ##### data type checker
	 ~~~py
	if isinstance (data, dict)

	if type(data) == dict
	 ~~~
	 - Using instance: 
	 [[D:\THREADSTONE\2021 - 2022\[1]INRIVER\spiders\[1]comparison-spiders\ph\lazada_ph | ref: python file]]
	  ~~~py
	from bs4 import BeautifulSoup, Tag, NavigableString

	class checkInstance ():
		if isinstance (data, Tag):
				pass
	 ~~~
  - ##### raw data checker from soup
	 ~~~py
	str(soup).find('keyword')
	 ~~~
 
   - ##### raw data checker from soup
	 ~~~py
	 if 'ProductList' in str(json_list[index])
	 ~~~
	 
   - ##### check if items in list is equal to a string ()
    [[D:\THREADSTONE\2021 - 2022\[1]INRIVER\spiders\[1]comparison-spiders\ph\lazada_ph | ref: spider file]]
	[ref: site](https://www.adamsmith.haus/python/answers/how-to-check-if-a-string-contains-an-element-from-a-list-in-python)
	
	 ~~~py
   self.brand_list = ["Acana","Brit Care","Canagan","Eukanuba","Granngården","Hill's","Hills","Nutrima","Orijen","Pure Natural","Purina","Royal Canin"]
        any(x in brand_text for x in self.brand_list)
	 ~~~

---