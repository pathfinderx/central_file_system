## Internal Reference
>#programming
---
# spider inriver general tome
##### Loops
- [[py loop | loop samples]]

##### Checkers
- [[py checkers| checkers samples]]

##### BeautifulSoup Selector
- [[py beautifulsoup selector| selectors samples]]

##### Strings
- [[py strings| string samples]]

##### Regex
- [[py regex| regex  samples]]

##### Constants
- [[py constants| constants samples]]

##### URL modification
- [[py url modification| URL modification samples]]

##### JS puppeteer Selector
- [[js selector| javscript puppeteer selector samples]]

##### Download Strategies
- [[py download strategies | download strategies samples]]

##### Deep Copy
- [[py download strategies | variable preservation method sample]]

##### Logs
- [[py logs]]