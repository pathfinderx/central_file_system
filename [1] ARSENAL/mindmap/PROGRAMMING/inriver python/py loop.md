## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
# Loops
##### Access Results
- ###### for compa 
	- except for image, means it is list
```py
	for key, values in res.items():
		print(key, ' : ', res.get(key).get('value'))
```

			

- ###### for rankings
	```py
	for x in final_list:
		for y in x:
			print(y.get('title').get('value'))
	```
	
	```py
	# title with brand
	for x in final_list:
		for y in x:
			print(y.get('title').get('value'), ' -> ', y.get('brand').get('value'))
	```
	
	```py
	# with index
	for i,v in enumerate(final_list):
		for y in final_list[i]:
			print(i, y.get('title').get('value'), ' -> ', y.get('brand').get('value'))
	```
	```py
	# duplicate tester
	prod_list = list()
	for i,v in enumerate(final_list):
		for y in final_list[i]:
			print(i, y.get('title').get('value'), ' -> ', y.get('brand').get('value'))
			prod_list.append(y.get('title').get('value'))
	
	[*set(prod_list)] # if printed results above has same length, meaning theres no duplicate
	```
	
	```py
	
	# duplicate removal; order preserving
	# link: https://www.peterbe.com/plog/uniqifiers-benchmark
	noDupes = []
	[noDupes.append(x) for x in pagination_href if not noDupes.count(x)]
	```
	
	```py
	# duplicate finder
	prod_title_list = [y.get('title').get('value') for i,v in enumerate(final_list) for y in final_list[i]]
	uniqueList = []
	duplicateList = []
	for i in prod_title_list:
		if i not in uniqueList:
			uniqueList.append(i)
		elif i not in duplicateList:
			duplicateList.append(i)
	
	```

- ###### for listings
	```py
		for x in res.get('products'):
			for y in x:
				print(y.get('title').get('value'))

		for x in res.get('products'):
			for y in x:
				print(y.get('rating').get('score').get('value'))
	```

##### accessing complicated dictionary key
```py
# convert it to list so you can transform the complicated keys into an integers
json_list = list(_json.get('props').get('apolloState').get('ROOT_QUERY').values())

# example:
dict = {'a;slkdjfowiejf;alksdjf':'1', 'asdijfoapiwejf;laksdjf....':'2'}
json_list = list(dict)

# result
	# json_list = [0:'1', 1:'2']
```

##### safety index deletion or prevention of shift index issue 
[[C:\repositories\ranking-spiders\strategies\website\de\zalando_de.py | ref: python file]]

```py
products = soup.select('div.DT5BTM.w8MdNG article')
if products:
	# check and remove invalid products (as per request from validator)
	index_of_invalid_products = []
	for i, v in enumerate(products):
		url_to_test = products[i].select_one('a._LM.JT3_zV.CKDt_l.CKDt_l.LyRfpJ').get('href')
		if url_to_test:
			hasValidUrl = re.search(r'(zalando|html)', url_to_test) # temporary indicator for valid products (url that contains 'zalando' or 'html')
			if not hasValidUrl:
				index_of_invalid_products.append(i)
	if index_of_invalid_products:
		products = [values for index, values in enumerate(products) if index not in index_of_invalid_products] # deletion alternative to prevent inaccuracy of deletion due to shifting of index
	return str(products), products
```

##### Specifications
[[ C:\repositories\comparison-spiders\strategies\website\br\shopb_com_br.py (at get_specification()) | ref: python file]]
[site ref](https://stackoverflow.com/questions/40760441/exclude-unwanted-tag-on-beautifulsoup-python)

~~~py
# removal of unwanted text as child node or child element or child tag, decompose alternative (use find('html_tag').extract() method)
	tag = soup.select_one('div.abas-custom')
	if tag:
		specs_tag_parent = tag.select_one('ul')
		specs_tag_nodes = specs_tag_parent.select('li')

		if specs_tag_nodes:
			source = str(tag)
			for x in specs_tag_nodes:
				if x.find('strong'):
					specs_key = x.find('strong').get_text().replace(':','').replace('\xa0','')
					x.find('strong').extract()
					specs_value = x.get_text().replace('\xa0','')
					value.update({specs_key:specs_value})
~~~

##### List 
[[ C:\repositories\comparison-spiders\strategies\website\br\shopb_com_br.py (at get_specification()) | ref: python file]]
[site ref](https://stackoverflow.com/questions/40760441/exclude-unwanted-tag-on-beautifulsoup-python)

~~~py
# removal of unwanted text as child node or child element or child tag, decompose alternative (use find('html_tag').extract() method)
	tag = soup.select_one('div.abas-custom')
	if tag:
		specs_tag_parent = tag.select_one('ul')
		specs_tag_nodes = specs_tag_parent.select('li')

		if specs_tag_nodes:
			source = str(tag)
			for x in specs_tag_nodes:
				if x.find('strong'):
					specs_key = x.find('strong').get_text().replace(':','').replace('\xa0','')
					x.find('strong').extract()
					specs_value = x.get_text().replace('\xa0','')
					value.update({specs_key:specs_value})
~~~



