## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
# complex algorithms
- [[C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\download\br\shopb_com_br.py | shopb_com_br.py (download strategy)]]
- [[C:\repositories\FREEZED (FORMER)\listing-spiders\strategies\website\br\shopb_com_br.py | shopb_com_br.py (website strategy)]]
- [[C:\repositories\be-comparison-spiders\strategies\website\br\kabum_com_br.py | kabum_com_br.py > specs extraction]]
- ##### math half_up algorithm
	- [[C:\repositories\be-comparison-spiders\strategies\website\es\fnac_es.py | fnac_es.py > price ]]
	- [[C:\repositories\be-comparison-et\transformers\de\cyberport_de.py | et > cyberport_de.py > rating]]
	- [[C:\repositories\be-comparison-et\transformers\de\alternate_de.py| et > alternate_de.py > rating]]
	- [[C:\repositories\be-comparison-et\transformers\be\mediamarkt_be_nl.py | mediamrkt_be_nl.py > ratings ]]
		- other predefined method using Decimal package 
- ##### 1k limit pagination
	- ##### listings
		- mediamarkt.lu
		- micromania.fr
		- officedepot.com
		- officedepot.com
		- officedepot.com.mx
		- officeworks.com.au
		- online.acer.com.au
		- paris.cl
	- ##### rankings
		- mediamarkt.lu
		- mediamarkt.pt
		- microsoft.com/en-us
		- noelleming.co.nz
		- officedepot.com.mx
		- officeworks.com.au
		- online.acer.com.au
		- paris.cl
			- with hidden pagination. Resolved using while loop
			- [[C:\repositories\be-listings-spiders\strategies\website\cl\paris_cl.py]]
- ## auto fix using third party json fixer
	- ### used for bbn.th listings
	```py
	headers = {
	    'accept': 'application/json, text/javascript, */*; q=0.01',
	    'cache-control': 'no-cache',
	    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
	    'cookie': '_ga=GA1.2.1624951991.1674085566; bond_country=SG; bond_continent=AS; _pbjs_userid_consent_data=3524755945110770; _pubcid=75428833-e4f9-4d5e-a284-798c5ef6f5d6; _tfpvi=Yjk2YTkyOTAtODM5Ni00ZWNkLWJjNjctYjEwMmVlYWZiZWNlIy0xLTI%3D; waldo_country=SG; waldo_continent=AS; waldo_region=01; _lr_env_src_ats=false; pbjs-unifiedid=%7B%22TDID%22%3A%22a525d93b-d293-46f2-a5e6-02f826841e3a%22%2C%22TDID_LOOKUP%22%3A%22TRUE%22%2C%22TDID_CREATED_AT%22%3A%222022-12-18T23%3A46%3A09%22%7D; _cc_id=b3d7f7d333d4de80a9253822c5b4f923; __gads=ID=456929fe4e3ad486-223bc2c758d900d0:T=1674085569:S=ALNI_MZc7CMuWx0XSEBp9zyeLaETsKJCUg; _gid=GA1.2.1486502161.1674798055; euconsent=allow; _ublock=1; _lr_retry_request=true; panoramaId_expiry=1674884458633; __gpi=UID=00000ba7aad69597:T=1674085569:RT=1674798058:S=ALNI_MZG23wTDkGujGUntVEIXx1ttQbpVg; cto_bundle=ldAS519INWM2YUpQbFp4NTh1VUZzRHdrS0RUME40QVJTSmklMkZlOGNrJTJCbW05bVRMWGdTdkprWkxPcSUyQk9kR1RHd0c3clhBa1RvNEYlMkZkeVklMkZTY212MjlzY2pxS1pHR2VtdUtGOTgydlpQdEpjbENXemVqWkF2MG13cEFNSjk0Z0N1bUtockpCazZkSU1XaklRY3RHa21NR3glMkJqRGI2akNDYVVHREU1d0klMkJzRzdPJTJGR3JvJTNE; cto_bidid=I0UIh19DMjNTYzFEUVJ0bDNRJTJCbVVIZ3dIdVJUWlk5NGNnakZJSCUyQnJXdTdQVUpJU05iY3Rma2RPZ256VDIlMkZCa1RBM0h3SWZYM0hqVk9uJTJGNUVMc080WXJBaGtXNUpqcTVjNGJIQyUyRlkxYkhvem9VZk1BNVpXenVoVm4lMkJYS2RqbzdTRSUyQlV6; _gat=1; _gat_global=1',
	    'origin': 'https://jsonformatter.curiousconcept.com',
	    'pragma': 'no-cache',
	    'referer': 'https://jsonformatter.curiousconcept.com/',
	    'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
	    'sec-ch-ua-mobile': '?0',
	    'sec-ch-ua-platform': '"Windows"',
	    'sec-fetch-dest': 'empty',
	    'sec-fetch-mode': 'cors',
	    'sec-fetch-site': 'same-origin',
	    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
	    'x-requested-with': 'XMLHttpRequest'
	}
	
	params1 = 'data='
	params2 = urllib.parse.quote(_rgx.group(1))
	params3 = '&jsontemplate=1&jsonspec=4&jsonfix=on&autoprocess=&version=2'
	
	sample_data_payload = f"{params1}{params2}{params3}"
	
	json_val = self.requester.post('https://jsonformatter.curiousconcept.com/process', timeout=timeout, headers=headers, cookies=None, data=sample_data_payload)
	
	json_formatter_val = json.loads(json_val.text)
	
	if (json_formatter_val and 
	    'result' in json_formatter_val and
	    'data' in json_formatter_val['result']):
	
	    string_validated_val = ' '.join([x for x in json_formatter_val['result']['data']])
	
	    _json = json.loads(string_validated_val) 
	```
---