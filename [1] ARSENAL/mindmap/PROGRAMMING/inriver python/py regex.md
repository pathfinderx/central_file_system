## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
### Cheat Sheet Reference:
- [web link](https://www.shortcutfoo.com/app/dojos/python-regex/cheatsheet)
### Practical Usage:
1. ###### find script using regex ^3b9605
	~~~py 
	soup.find('script', text = re.compile ('props'))
	~~~  
1. ###### regex for sku (sequece: A123B)
	~~~py 
	if not sku:
	raw_sku = self.global_url.split('-')[-1]
	sku_test = re.match("^(?!.*(:[a-zA-Z]{2,3}|[0-9]{2,3}))[a-zA-Z0-9]{3,5}", raw_sku) # check if not valid sku (valid sku is: combination of C'letter and numbers of 5 occurences)
	if not sku_test: 
		sku_match = re.findall('(?:[0-9]{1,3}|[A-Z]{2,3}|[0-9]{1,3}){3,5}', raw_sku) # reassign new sku
		sku = sku_match[0] if sku_match and len(sku_match) >= 0 else None # ternary operation

	url = f'https://www.grainger.com/product/info?productArray={sku}&n=true'
	~~~
1. ###### for obfuscations:
	[[C:\repositories\listing-spiders\strategies\website\sk\telekom_sk.py (line 384) | ref: python file]]
	~~~py 
	_rgx = re.search(r'window\.\__INITIAL\_STATE\__.=.({.*})', scrp_tag.get_text(strip=True))
	~~~

	[[C:\repositories\ranking-spiders\strategies\download\de\bergzeit_de.py) | ref: python file]]
	~~~py 
	match1 = re.search(r'(#trboModule_13014_94284_61_container.trboModuleContainer .trbo-content .trbo-canvas .trbo-stage .trbo-item .trbo-badge)([\s\S]+)([}]])',raw).group(2)
	match2 = re.search(r'(wrapParts = me.attr\()([\s\S]+)',match1).group(2)
	match3 = re.search(r'(numberOfProducts)([\s\S]+)', match2).group(2)
	match4 = re.search(r'(trboModule_13014_94284_61_instance.setupProductSlider\(\[)([\s\S]+)',match3).group(2)
	~~~ 
1. ###### find div using a combination of find_all() tag and regex ^6b521d
	[[C:\repositories\listing-spiders\strategies\website\ru\iport_ru.py | ref: python file]]

	~~~py 
	parent_tag = soup.select_one('#catalog-listing')
	if parent_tag:
		product_tags = parent_tag.find_all('div',{'class':re.compile(r'CatalogGridstyles__CatalogGrid__Item.*')})

	~~~ 
---
### To debunk
1. <mark class="hltr-orange">re.finditer()</mark> vs <mark class="hltr-orange">re.findall()</mark>
	- <mark class="hltr-orange">re.finditer()</mark> returns iterator of matched objects
	- <mark class="hltr-orange">re.findall()</mark> returns list of matched objects
	- #### Example:
		1. using <mark class="hltr-orange">re.finditer()</mark>
			- [[C:\repositories\[1] RESEARCH\python\regex\regex.py | practical example > local link]]
		```py
		import re

		text = ''' Extract the doamin from the urls www.gcptutorials.com,
		www.wikipedia.org, www.google.com'''
		
		pattern = r'(www.([A-Za-z_0-9-]+)(.\w+))'
		
		
		find_iter_result = re.finditer(pattern, text)
		
		print(type(find_iter_result))
		print(find_iter_result)
		
		for i in find_iter_result:
		    print(i.group(2))
    
		```
		```py
		# finditer() result
		< class 'callable_iterator' >
		< callable_iterator object at 0x7f0c5cc24e48 >
		gcptutorials
		wikipedia
		google    
			 
		```
		2. using <mark class="hltr-orange">re.findall()</mark>
		```py
		import re

		text = ''' Extract the domain from the urls www.gcptutorials.com,
		www.wikipedia.org, www.google.com'''
		
		rgx = re.findall(r'(www.([A-Za-z_0-9-])+(.\w+))', text)
	
		if rgx:
			print(type(rgx))
			print(rgx)
	
			for x in rgx:
				if (isinstance(x, tuple) and len(x) > 0):
					print(x[0])
		    
		```
		```py
		# findall() result
		<class 'list'>
		[('www.gcptutorials.com', 's', '.com'), ('www.wikipedia.org', 'a', '.org'), ('www.google.com', 'e', '.com')]
		www.gcptutorials.com
		www.wikipedia.org
		www.google.com

---

### Meta characters
- Metacharacters are special characters that affect how the regular expressions around them are interpreted
- [web  link](https://pynative.com/python-regex-metacharacters/#:~:text=In%20Python%2C%20Metacharacters%20are%20special,or%20*%20%2C%20are%20special%20characters.) - and more examples here
1. <mark class="hltr-orange">.</mark> (DOT)	Matches any character except a newline.
2. <mark class="hltr-orange">^</mark> (Caret)	Matches pattern only at the start of the string.
3. <mark class="hltr-orange">$</mark> (Dollar)	Matches pattern at the end of the string
4. <mark class="hltr-orange">\*</mark> (asterisk)	Matches 0 or more repetitions of the regex.
5. <mark class="hltr-orange">\+</mark> (Plus)	Match 1 or more repetitions of the rege
6. <mark class="hltr-orange">?</mark> (Question mark)	Match 0 or 1 repetition of the regex.
7. <mark class="hltr-orange">[]</mark> (Square brackets)	Used to indicate a set of characters. Matches any single character in brackets. For example, [abc] will match either a, or, b, or c character
8. <mark class="hltr-orange">|</mark> (Pipe)	used to specify multiple patterns. For example, P1|P2, where P1 and P2 are two different regexes.
9. <mark class="hltr-orange">\\</mark> (backslash)	Use to escape special characters or signals a special sequence. For example, If you are searching for one of the special characters you can use a \ to escape them
10. <mark class="hltr-orange">[^...]</mark>	Matches any single character not in brackets.
11. <mark class="hltr-orange">(...)</mark>	Matches whatever regular expression is inside the parentheses. For example, (abc) will match to substring 'abc'

- #### Meta character combinations
	- <mark class="hltr-orange">\[^]</mark> - not matching in brackets
### Quantifiers
- [web link](https://www.pythontutorial.net/python-regex/python-regex-quantifiers/)
-  quantifiers match the preceding characters or character sets a number of times. The following table shows all the quantifiers and their meanings:
1. <mark class="hltr-orange">\*</mark>	Asterisk	Match its preceding element zero or more times.
2. <mark class="hltr-orange">\+</mark>	Plus	Match its preceding element one or more times.
3. <mark class="hltr-orange">?</mark>	Question Mark	Match its preceding element zero or one time.
4. <mark class="hltr-orange">{ n }</mark>	Curly Braces	Match its preceding element exactly n times.
5. <mark class="hltr-orange">{ n ,}</mark>	Curly Braces	Match its preceding element at least n times.
6. <mark class="hltr-orange">{ n , m }</mark>	Curly Braces	Match its preceding element from n to m times.
- #### Greedy quantifier 
	- [detailed web link](https://javascript.info/regexp-greedy-and-lazy)
	- <mark class="hltr-red"> /".+"/g</mark> - <mark class="hltr-orange">.</mark> means all characters except newline
	- "In the greedy mode (by default) a quantified character is **repeated as many times as possible.**"
	- In short to: *The regexp engine adds to the **match as many characters as it can conform to .+**, and then **shortens** that one by one by the term called **backtracking**, if the rest of the pattern in a string doesn’t match.*
	
- #### Lazy quantifier
	- "The lazy mode of quantifiers is an opposite to the greedy mode. It means: “**repeat minimal number of times**”."
	- "*Laziness is only enabled for the quantifier with ?*"
	- <mark class="hltr-orange">/".+?"/g</mark> - We can enable it by putting a question mark '?'
	- ==But remember!==
		- this remains greedy <mark class="hltr-orange">/\d+ \d+?/</mark>
	- ##### Lazy quantifier alternative
		- <mark class="hltr-orange">"[^"]+"</mark> - When the regexp engine looks for [^"]+ it stops the repetitions when it meets the closing quote, and we’re done.
			- *Please note, that this logic does not replace lazy quantifiers!* 
### Shorthands
- [web link](https://www.shortcutfoo.com/app/dojos/python-regex/cheatsheet) - good reference
- <mark class="hltr-orange">\s</mark> Match whitespace characters # same as [ \t\n\r\f\v]
- <mark class="hltr-orange">\S </mark> Match non whitespace characters #same as [^ \t\n\r\f\v]
- <mark class="hltr-orange">\w</mark> Match unicode word characters # same as [a-zA-Z0-9_]
- <mark class="hltr-orange">\W </mark> Match any character not a Unicode word character # same as [^a-zA-Z0-9_]
- <mark class="hltr-orange">\Z</mark> Match only at end of string