## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
# py beautifulsoup selector
1. ###### Select tag attributes and properties using bracket
	[[C:\repositories\ranking-spiders\strategies\website\br\mercadolivre_com_br.py | ref: python file]]
	~~~py
	tag = soup.select_one('input[name="itemId"]')
	~~~
1. ###### Select different name tags
	[[C:\repositories\comparison-spiders\strategies\website\es\mediamarkt_es.py (at get_description()) | ref: python file]]
	~~~py
	tag.find_all(['p', 'h3', 'strong'])
	~~~
1. ###### Select tag using 'or'
	~~~py
	tag_score_option = soup.select_one('a.rating-text.gtm_rp101318' or 'a[href="#reviews-section"]')
	~~~

1. ![[py regex#^3b9605| find script tag using regex]]

1. ![[py regex#^6b521d | combination of find_all() tag and regex]]
2. ###### JS selector
	1. ![[js selector#^08e923]]