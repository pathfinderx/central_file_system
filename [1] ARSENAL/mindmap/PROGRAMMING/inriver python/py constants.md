## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
# Constants
- Used only for 1 time call or 1 time variable assignment
-	There is no build-in module for python constants nor primitive call for constants. Do this instead:
	
	~~~py 
	if not self.url: # if theres no value, put value. The next time it is called it is not null again
		self.url = url

	# or use private method
	~~~
