## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
### Variable Preservation or copy
- [[C:\repositories\be-comparison-et\transformers\fr\microsoft_com_fr_fr.py | External Link]]
```py
import copy # import 

def transform_price(self, value):
	try:
		preserved_value = copy.deepcopy(value) # variable preservation
		value = re.sub('[^0-9.]', '', value.replace(",", "."))

		if (re.search(r'full price was', preserved_value.lower())):
			extracted_value, numeric_value, converted_value = None, None, None
			extracted_value = preserved_value.split('now')[-1].replace('\xa0', '')
			numeric_value = re.search(r'([\d]+[,|\.]+[\d]+)', extracted_value) if extracted_value else None
			converted_value = round(float(numeric_value.group(1).replace(',','.')))
			return converted_value
		else:
			return round(float(value), 2)
	except:
		return ETDefaults.NOT_FOUND.value

```
