## Internal Reference
>#programming
>[[spider inriver general tome | return to home]]
---
##### Removing Uncessary Characters
1. ###### Commonly used
	~~~py 
	' '.join(descriptionTag.stripped_strings)
	~~~
	~~~py 
	tag.get_text().strip()
	~~~
	
2. ###### Remove html tags
	- [[C:\repositories\extract-transform-comparison\transformers\us\fastenal_com.py | ref: python file]] 
	- [[C:\repositories\be-comparison-et\transformers\dk\magasin_dk.py | ref: et-compa]]
	 ~~~py 
	 def transform_description (self, value):
		stripper = re.compile('<.*?>')
		stripped = re.sub(stripper, '', value)
		value = html.unescape(stripped).strip()
		return value
	 ~~~

3. ###### Unicode Escapes
	~~~py 
	def decodeUTF8 (self, data):
		encoded = str(data).encode('utf_8') # encode to bytes
		decoded = encoded.decode('unicode_escape')
		stripped = decoded.replace('\\','')
		strippedSoup = BeautifulSoup(stripped, 'lxml')
		return strippedSoup
	~~~
	1. ###### Remove invisible characters revealed as bytes
		- [[C:\repositories\be-comparison-spiders\strategies\website\be\vandenborre_be.py | ref: python file]]
		```py
		if desc_list:
			# invisible characters revealed as bytes # https://prnt.sc/PXsSKWYl5dZx
			value = ' '.join(x.encode('utf8').replace(b'\xe3\x85\xa4', b'').decode().strip() for x in desc_list) 
		```
1. ###### Remove trailing spaces or succeding spaces
	[[C:\repositories\comparison-spiders\strategies\website\es\amazon_es.py | ref: python file]]

	~~~py 
	if special_description:
		special_text = ''.join([x.get_text().strip() for x in special_description])
		stripped_text = re.sub('[\s]{2}', "", description)
		description += ' ' + stripped_text if stripped_text else None

	~~~

	1. ###### Other option
	~~~py 
	 ' '.join(value.split())
	~~~
	
	2. ###### spacing issues when using get_text, especially with child elements that also have texts inside
	[[C:\repositories\ranking-spiders\strategies\website\de\alternate_de.py | ref: python file]]
	~~~py 
	 title_tag.get_text(" ", strip=True)
	~~~