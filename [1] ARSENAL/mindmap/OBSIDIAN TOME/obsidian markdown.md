
## Internal Reference
>#obsidian #notetaking
---
### Back Link to tome: [[obsidian general tome]]
# Headers 1
## Headers 2
### Headers 3
#### Headers 4 
##### Headers 5
###### Headers 6

^85f60e

#tag - just a tag

_italics_
*italics*

__bold__
**bold**

~~strikethrough using tilde~~

==highlight==

## LIST
- ###### List using dash (-)
	- 1
	- 2
	- 3

	###### List using asterisk (*)
	* 1
	* 2
	* 3

	###### List using plus sign (+)
	+ 1
	+ 2
	+ 3

	###### Numbered List (n.)
	1. One
	2. Two
	3. Three

	###### Checlist (- [ ])
	- [ ] One 
	- [ ] Two Three

## Notes and Linking
1.  ###### Link common
	-  [[obsidian general tome]]
		
2.  ###### Link with custom name
	- [[obsidian general tome | general tome]]
3.  ###### Link websites
	- www.google.com
4.  ###### Link websites with custom name
	- [google](https://www.google.com)

5.  ###### Navigation of headers from same page or other page
	- [[obsidian general tome#Hotkeys]]
		- "Hotkeys" from [[obsidian general tome]] headers name hotkey

6.  ###### Navigation of headers from same page but more specific
	- [[obsidian markdown#Notes and Linking| markdown tome > Notes and Linking Headers]]

7.  ###### Embedding Links
	- ![[obsidian general tome]]
	
8.  ###### Tables

	Column 1 | Column 2
	--- | ---
	Cell Value 1 | Cell Value 2

	-	[More Advanced tables](https://www.youtube.com/watch?v=9yKaB9zv9Zg)=

9. ###### Qoute
	
	> A random quote

10.  ###### Navigation of Footnotes 
- Footnote 1 [^1]
- Footnote 2 [^2]
- Footnote 3 ^[definition of footnote] 
	- _automatically adds footnote to the bottom_
- Footnote 4 ^[[[obsidian markdown#Notes and Linking]]]
	- _automatically adds footnote to the bottom with current file header navigation_

 [^1]: definition of footnote 1
 [^2]: definition of footnote 2
   
