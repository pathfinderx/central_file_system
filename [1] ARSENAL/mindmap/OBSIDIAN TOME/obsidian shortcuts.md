## Internal Reference
>#obsidian #notetaking
>[[obsidian general tome]]
---
#### Hotkeys and Shortcuts
1. ##### Edit mode and view Mode

	<mark class="hltr-orange">ctrl + e</mark>

2.	##### Quick swap
	<mark class="hltr-orange">	ctrl + o 
		ctrl + o + down arrow + enter
		ctrl + alt + right arrow</mark>
1. ##### Split view: Edit mode and view mode
	
	<mark class="hltr-orange">ctrl + click (view mode icon in top right)</mark>
4. ##### Note taking
	1. checklist
		- <mark class="hltr-orange">ctrl + enter</mark>
	1. code snippet
		
		- <mark class="hltr-orange"> Newline + tab {nth times} (till code snippet reveals)</mark>
		- <mark class="hltr-orange">Other Alternative: </mark> 
			
			~~~py
			or use 'tilde' {3times} + file extension
			then enclose with with 'tilde' {3times}
			eg: 
				```py
					statements
				```
			~~~
6. ##### Highlighter
	- <mark class="hltr-orange">alt + shift + a</mark> = highlight with specific color
	- <mark class="hltr-ash">alt + shift + h</mark> = default highlight
7. ##### Create canvas
	- <mark class="hltr-orange">ctrl + alt + n</mark>