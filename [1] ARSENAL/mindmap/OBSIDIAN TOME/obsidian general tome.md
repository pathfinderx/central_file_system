# Internal Reference
>#obsidian #notetaking
---

### Markdown Tome
[[obsidian markdown | markdown samples]]

### Hotkeys or Shortcuts
>#hotkeys #shortcuts 

[[obsidian shortcuts]]

### Themes
[[obsidian theme]]

### Filters and Grouping
[[obsidian filters and grouping]]

### Help or Settings
[[obsidian help or settings]]


### To add:
- [ ] filters and grouping
- [ ] Change snippet apperance using css 
	- ref: [monokai pro theme](https://forum.obsidian.md/t/monokai-pro-theme/28273)