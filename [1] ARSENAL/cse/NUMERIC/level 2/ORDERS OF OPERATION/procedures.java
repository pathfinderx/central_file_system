05 12 20
	Test
		Orders of Operation
			* basic with parenthesis [8/12] 66.7% accuracy

	Acquired Mistakes
		/ watch with signs of multiplication
		/ convert
		/ delay operation convertion
		/ careful copying signs 
		/ 6 * 4, 7 * 4, 8 * 4, 9 * 4

05 16 20
	Acquired Mistakes
		orders of operation
			* watch with multiplication (channeling for future mistakes)
			* watch with signs (fixed but channeling for future mistakes)
				/ (6-8+4)
				/ (18 + 5 * (-8))
				/ sign not copied to next simplification
				/ [2-{3+(-11)}]
				/ [3-{27+6}]
				/ [3-(1

05 25 20
	Acquired Mistakes
		* 94/4 = 24 not 22 
		* (5) signs 
		* (2) multiplication sign 
		* copied the wrong number
		- 'factor: sleepy'
		- 'factor: auto render time after eating'
		