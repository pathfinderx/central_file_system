03 26 20
	2 3 5 7
	11 13 17 19
	23 29 [3,9]
	31 37 		[1,7]
	41 43 47 			[1,3,7]
	53 59 [3,9]
	61 67 		[1,7]
	71 73 79 			[1,3,9]
	83 89 [3,9]
	97 
	101 103 107 109

	* why 1 is not included in in prime number and composite number?
		- a prime number can be factored by 2 numbers, 1 and itself, so 1 isnt a prime number
		- a composite number can be factored by more than 2 numbers, so 1 isnt a composite number

04 14 20
	120-200
		with last digit of:
			7 (in numpad: 2,3,5,6,9)
			9 (in numpad: 4,7,3,9)
			3 (in numpad: 7,6,9)

	* tips to identify every tenths digit of 120-200 prime numbers
		1.) identify if how many primes in that 10s digit
			ex: 150 or 5(in numpad) has 2 prime numbers: 151 and 157 (because its included in color structure or 1 and 7)
			ex: 190 or 9(in numpad) has 4 prime numbers: 191 193 197 199 (because it fits in all color structure 1,3 and 9 last digits)

		2.) then identify the numbers based on color structure

