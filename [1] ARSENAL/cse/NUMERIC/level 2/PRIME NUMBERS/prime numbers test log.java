
01 10 20
	Test
		Prime number 
			Mental	
				00 00 31 37
				00 00 33 89
				
				00 00 29 37
				00 00 30 08
				00 00 31 73
				00 00 30 04
				00 00 28 06 
			Written
				00 00 31 15
				00 00 26 60
				00 00 26 35

01 26 20
	Test
		Prime Numbers
		Mental
			- remarks: 'highly accurate'

04 06 20	
	Test 
		* prime numbers 1-100
			- remarks: 'highly accurate, mastered the framework'

04 12 20
	Test
		prime numbers 1
			* color structure of 1,3,7,9 numpad version 10 digits of (5/5) spatial memorization (random)
			* identify every primes of tenths digit 10 digits of (5/5) primes of tenths (random)
			* identify every primes from 100-200 (0/10) iteration
			* identify every primes from 2-200 (0/10) iteration

04 14 20
	Test
		/ prime numbers 100-200
			/ color structure of 1,3,7,9 numpad version 10 digits of (5/5) spatial memorization (random)
			/ identify every primes of tenths digit 10 digits of (5/5) primes of tenths (random)
			/ identify every primes from 100-200 (10/10) iteration
			/ identify every primes from 2-200 (5/5) iteration

05 24 20
	Test 
		/ prime numbers 1-200, 20 items (3/3)

05 25 20
	Test
		/ prime numbers 1-200, all iteration (3/3)
		/ prime numbers 1-200, random (3/3)