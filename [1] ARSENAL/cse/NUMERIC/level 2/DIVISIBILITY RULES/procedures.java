01 26 20
	Test
		Divisibility rules
		Mental, Written (2 3 4 5 6 9)
			- remarks: 'highly accurate except for divisibility of 4'

01 27 20
	Test
		Divisibility rules
			Mental, Written (2 3 4 5 6 9)
				- remarks: 'highly accurate except for divisibility of 4'

04 06 20
	* 0 is divisible to any number
	* you can identify a number without memorizing the divisibility rules by dividing the given number by the given test number but when it comes to a long digit number, divisibility rules comes in handy.

	* div rule 6
		1.) identify if the number is divisible by 2 before you do calculations for divisibility check of 3

	* almost identical rules
		4 8 16 (x2 4+ 4x8+)
		7 17 (2x 5x repeat)  
		13 19 (4x 2x repeat) 

	* all together rules
		2 lastdigit 0 2 4 6 8
		3 ++alldigits; reapeat
		4 last2digit x2+ lastdigit
		5 lastdigit 5 0
		6 dependent 2 and 3
		7 lastdigit x2- rest; repeat

		8 last 3 digit odd; +4 tenths digit / last 3 digit even; tenths digit div 8
	
		9 ++alldigit
		10 lastdigit 10 0
		11 alternative; -+Lr
		12 dependent 3 and 4  
		13 lastdigit x4+ rest; repeat
		14 dependent 2 and 7
		15 dependent 5 and 3

		16 thousands digit even; x4 hundreds digit + rest
		   thousands digit odd; +8; x4 hundreds digit + rest 

		17 lastdigit x5- rest; repeat
		18 dependent 2 and 9
		19 lastdigit x2+ rest; repeat
		20 dependent 10, last 2 digit even
		25 last2digit 25 50 75
		50 last2digit 50 00 

	* multiples of 7	multiples of 11	 	multiples of 13
		7				11					13					
		14				22					26					
		21				33					39					
		28				44					52					
		35				55					65					
		42				66					78					
		49				77					91					
		56				88					104					
		63				99										
		70				110										
		77				121										
						132										
						143										
						154										
						165										
						176										
						187										
						198										
																
04 09 20
	Test 	
		/ Divisible & Multiples
			/ master 02 - 10
				/ verbal explanation (2/2)
				/ verbal explanation random 10 items (3/3)
				/ vertical (1/1)
				/ horizontal (1/1)

			/ master 11 - 50
				/ verbal explanation (2/2)
				/ verbal explanation random 10 items (3/3)

				/ research new number (delete this right after, this is not included in test iteration)
				/ vertical (1/1)
				/ explanation difference between:
					/ 7 17; 13 19; 4 8 16 horizontal (3/3)
				/ search for actual tests
				/ make procedures to assist memory

04 14 20
	Test 
		/ divisibility rules 2-50
			/ verbal explanation (1/1)

			/ explanation difference between:
				/ 7 17; 13 19; 4 8 16 horizontal (3/3)

05 24 20
	Test
		* divisibility rules 1-50, (0/3)

05 25 20
	Test
		/ divisibility rules 1-50, all iteration (2/2)
		/ divisibility rules 1-50, random (2/2)
