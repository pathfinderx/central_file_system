05 23 20
	* basic fraction
		* unit fraction
			- a fraction with 1 as its numerator
			- denominator names the unit
			- numerator tells their number
		* improper to mixed
		* complement of a proper fraction 
 			- proper fraction we must add in order to get 1.
 			- 'this helps us understand to subract whole number with a proper fraction'

 	* common fraction
 		* a number written with 'numerator' and 'denominator' with both number as 'natural numbers'
 		* opposite of 'decimal fraction' = 'proper fraction'
 		* numerator = cardinal numbers; how many of those parts?
 		* denominator = ordinal numbers; which part
 		
=============================
links
	[1]http://www.themathpage.com/Arith/unit-fractions.htm
	[2]http://www.themathpage.com/Arith/fractions.htm
		- fraction reference