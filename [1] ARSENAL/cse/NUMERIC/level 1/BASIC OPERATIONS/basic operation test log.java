01 10 20	
	Prime number 
		Mental	
			00 00 31 37
			00 00 33 89
			
			00 00 29 37
			00 00 30 08
			00 00 31 73
			00 00 30 04
			00 00 28 06 
		Written
			00 00 31 15
			00 00 26 60
			00 00 26 35

01 11 20
	Test
		Stimulate Operations
		* addition
			* split 5 technique
				* 2 digits
					00 01 46 42 
					00 01 32 27 (97% accuracy)
					00 01 27 48 (100% accuracy)
					00 01 16 94 (100% accuracy)
					00 01 26 00 (100% accuracy, reverse)

					00 01 14 30 (100% accuracy) 
					00 01 04 59 (100% accuracy)
					00 01 11 88 (100% accuracy)
					00 01 02 82 (100% accuracy)
					00 01 00 81 (100% accuracy)

				* 3 digits
					100% accuracy
				* 4 digits
					100% accuracy
				* 5 digits
					95% accuracy
				* 7 digits
					95% accuracy 

		subtraction
		multiplication
		division

01 12 20
	Test
		Stimulate Operations
		* addition
			* split 5 technique
				* 2 digits
					00 01 27 75
					00 01 16 35
				
01 18 20
	Test
		Stimulate Operation
			* addition
				Mental
					* split 5 technique (95% accuracy)
				Written
					* 90% accuracy in (2 iterations of 10 items)
			* subtraction
				Written
					* 95% accuary in (2 iterations of 10 items)
			* multiplication
				Written
					* 90% accuracy in (2 iterations of 5 items)

01 22 20
	Acquired Mistakes
		stimlate Operations
			lag in adding
			* 41946

01 25 20
	Test
		stimulate operations
		Addition
			Metal
				* 10 digits sequence
					- difficulties
						/ 1312136713
					- remarks: 'highly accurate'
				* 2 digits
					00 01 01 76 (100% accuracy)
					00 00 55 56 (94% accuracy)
					00 00 59 00 (100% accuracy)
			Written 
				* 100% accuracy in (1 iteration in 10 items)
		Subtraction	
			Mental
				* 2 digits sequence
					- remarks: 'highly accurate'
			Written
				* 80% accuracy in (1 iteration of 5 items)
				* 100% accuracy in (2 iterations of 10 items)
		Multiplication
			Mental 
				2 digits of 20 items [numbers between 9 - 5]
					00 00 26 04
					00 00 29 29
					00 00 28 10
					00 00 25 02
					00 00 25 52
				2 digits of 20 items [numbers between 1 - 9]
					- remarks 'highly accurate'

				2 digits of 20 items with addition [numbers between 1 - 9]
					- remarks 'no ambiguity but slower compared to pure operations; still highly accurate'
			Written
				3 x 3 of 5 items
					00 06 23 27 (40% accuracy)
					00 05 21 35 (100% accuracy)
					00 04 21 21 (100% accuracy)

01 26 20
	Test
		Addition
			- remarks: 'highly accurate'
		Subtraction
			- remarks: 'highly accurate'
		Multiplication
			- remarks: 'highly accurate'
		Division
			- remarks: 'highly accurate for all rigorous test'
				- without remainders
				- with remainders
				- with decimals
				- with decimals as remainder

03 07 20
	Test
	Addition
		Mental
			* 9 mastery
				- notes: 'discovered -1 on subtrahend'
				- remarks: 'highly accurate'
			* 8 mastery
				- notes: 'discovered -2 on subrahend'
				- remakrs: 'highly accurate'

	Words Of The Enlightened: 'mathematics trains working memory therefore improves memory utilization'

03 08 20
	Test 
		Addition
			Mental
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate'

				* 20 digits continous addition
					- remarks: 'difficulty in proceeding to next 10 digit'
						- example: 98 + 7 = 105 'difficulty in handling from 90 -> 100 as proceeding next 10th digit'

				* 3 digits continous addition with 10 sets 
				* 4 digits continous addition with 10 sets
				* 5 digits continous addition with 10 sets
				* 6 digits continous addition with 10 sets
					- remarks: 'all from 3-6 digits highly accurate'

		Multiplication
			Mental
				* 2 digit multiplication with 10 sets
					remarks: 'highly accurate'

03 14 20
	Test
		Addition
			Mental
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate'

				5 digits continous addition
				6 digits continous addition
				7 digits continous addition
				8 digits continous addition
				9 digits continous addition
				10 digits continous addition
				11 digits continous addition
				12 digits continous addition
				13 digits continous addition
				14 digits continous addition
				15 digits continous addition
					- remarks: 'highly accurate' 'better when handling to proceeding next 10th digit during stimulation'

			words of the enlightened: 'mathematics trains working memory therefore improves memory utilization'

		Multiplication
			* 2 digit multiplication with 20 sets
					remarks: 'highly accurate'
					remarks: 'refrain from over rendering because it adds up confusion. e.g. prove why 6x7=42'

03 15 20	
	Test
		Addition
			Mental
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate'

03 16 20
	Test	
		Mental
			Addition
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate'

				10 digits continous addition (3/3)
				11 digits continous addition (3/3)
				12 digits continous addition (3/3)
				13 digits continous addition (2/3)
				14 digits continous addition (3/3)
				15 digits continous addition (2/3)
					- remarks: 'highly accurate' 'better when handling to proceeding next 10th digit during stimulation'
					- remarks: 'sub-consious mind, sometimes actives without awareness and provides acurate answer'
					- remarks: 'getting to 30th, 40th and 50th, accurate'
					- remarks: 88.9% accuracy

			Multiplication
				* 2 digits multiplication with 10 numbers; repeat 10 times
					- remarks: 'fast, highly accurate'

03 18 20
	Test
		Mental
			Addition
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate, fast, advance reading'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate, fast, advance reading'

				10 digits continous addition (5/5)
				11 digits continous addition (5/5)
				12 digits continous addition (5/5)
				13 digits continous addition (4/5)
				14 digits continous addition (4/5)
				15 digits continous addition (3/5)
					- remarks: 87% accuracy

03 21 20
	Test
		Mental
			/ real number system
			/ addition
					* 8 -> 6-9 mastery (-2 on subtrahend usage)
						- remarks: 'highly accurate, fast, advance reading'
					* 9 -> 6-9 mastery (-1 on subtrahend usage)
						- remarks: 'highly accurate, fast, advance reading'

					11 digits continous addition (3/3)
					12 digits continous addition (3/3)
					13 digits continous addition (3/3)
					14 digits continous addition (3/3)
					15 digits continous addition (3/3)
						- remarks: 100% accuracy 'problems with jumping to next 10th digit number, caused by caffiene pulpitation effect'

			/ multiplication

		Written
			/ addition 10 items (100%)
				/ with decimal 5 items (100%)
			/ subtraction 10 items (100%)
				/ with decimal 5 items (100%)
			/ multiplication 5 (100%)
				/ with decimal 5 items (100%)
				- remarks: 'two mistakes corrected'
			/ division 3 (100%)
				/ with decimal 3 (100%) 
				/ with remainder 3 (100%)
				- remarks: 'a lot of mistakes corrected'
			- overall remarks: 'grinded, grit tested and accomplished'

		Words of The Enlightened
			FOR CSE
				* 'work with speed and effiency'
					- 'we have multiple fields to finish, dont exhaust your self a single field'

03 22 20
	Test
		Mental
			/ addition
				* 8 -> 6-9 mastery (-2 on subtrahend usage)
					- remarks: 'highly accurate, fast, advance reading'
				* 9 -> 6-9 mastery (-1 on subtrahend usage)
					- remarks: 'highly accurate, fast, advance reading'

				11 digits continous addition (3/3)
				12 digits continous addition (3/3)
				13 digits continous addition (3/3)
				14 digits continous addition (3/3)
				15 digits continous addition (3/3)
					- remarks: 100% accuracy 'jumping to next 10th digit got better'

04 01 20
	Test
		* addition
			* random number calculation
				* 20 pairs of 3 sets
				* remarks: 'highly accurate'

			11 digits continous addition (3/3)
			12 digits continous addition (3/3)
			13 digits continous addition (3/3)
			14 digits continous addition (3/3)
			15 digits continous addition (3/3)
				- remarks: 100% accuracy 'jumping to next 10th digit got better, developed autocalculation in subconscious but doubts are restrains'
			
		* multiplication
			* random number calculation
				* 20 pairs of 3 sets
				* remarks: 'highly accurate'

		* subtraction
			* random number calculation
				* 20 pairs of 3 sets
				* remarks: 'highly accurate'

04 02 20 
	Test
		* random number calculation
		* 20 pairs of 3 sets
		* remarks: 'highly accurate'

		11 digits continous addition (3/3)
		12 digits continous addition (3/3)
		13 digits continous addition (2/3)
		14 digits continous addition (3/3)
		15 digits continous addition (3/3)
			- remarks: 93.3% accuracy

04 04 20
	Test 	
		* addition
			* random number calculation
				* 20 pairs of 3 sets
				* remarks: 'highly accurate, fast'

			15 digits continue addition (4/5)
				- remarks: 80% accuracy

		* multiplcation
			* random number calculation
				* 20 pairs of 3 sets
				* remarks: 'highly accurate'

04 06 20
	Test 
		* random number calculation
			* 20 pairs of 3 sets
			* remarks: 'highly accurate'

		11 digits continous addition (2/2)
		12 digits continous addition (2/2)
		13 digits continous addition (2/2)
		14 digits continous addition (2/2)
		15 digits continous addition (2/2)
			- remarks: 100% accuracy

04 07 20	
	Test 
		* random number calculation
			* 20 pairs of 3 sets
			* remarks: 'highly accurate'

		11 digits continous addition (2/2)
		12 digits continous addition (2/2)
		13 digits continous addition (2/2)
		14 digits continous addition (2/2)
		15 digits continous addition (2/2)
			- remarks: 100% accuracy

04 09 20
	Test 
		* random number calculation
			* 20 pairs of (03/03) 30s each
			* remarks: 'highly accurate'

		11 digits continous addition (2/2)
		12 digits continous addition (1/2)
		13 digits continous addition (2/2)
		14 digits continous addition (2/2)
		15 digits continous addition (2/2)
			- ramarks: 'accept the challenge; more pressure and challenging with timers; highly accurate'

		* multiplication 20 items of 3 sets
			= remarks: 'more pressure and challenging with timers; highly accurate'

04 10 20
	Test
		* random number calculation
		* 20 pairs of (3/3) 25s each
		* remarks: 'highly accurate'

		11 digits continous addition (2/2)
		12 digits continous addition (2/2)
		13 digits continous addition (2/2)
		14 digits continous addition (2/2)
		15 digits continous addition (2/2)
			- ramarks: 'accept the challenge; more pressure and challenging with timers; highly accurate'

04 12 20
	Test
		* random number calculation
		* 20 pairs of (3/3) 25s each
		* remarks: 'highly accurate'

		11 digits continous addition (2/2)
		12 digits continous addition (2/2)
		13 digits continous addition (2/2)
		14 digits continous addition (2/2)
		15 digits continous addition (1/2)
			- 25s each
			- ramarks: 'accept the challenge; more pressure and challenging with timers; highly accurate but pressured at 15 digits'

		* multiplication 20 items of 3 sets
			= remarks: 'more pressure and challenging with timers; highly accurate; sometimes,subconscious works fast but not accurate if not properly checked'

04 14 20
	Test
		/ random number calculation
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate'

		/ 11 digits continous addition (2/2)
		/ 12 digits continous addition (2/2)
		/ 12 digits continous addition (2/2)
		/ 14 digits continous addition (2/2)
		/ 15 digits continous addition (2/2)
			- 25s each
			- 26s at 15 digits

		/ multiplication 20 items of 3 sets

04 19 20
	Test
		/ random number calculation
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate'

		/ 11 digits continous addition (2/2)
		/ 12 digits continous addition (2/2)
		/ 12 digits continous addition (2/2)
		/ 14 digits continous addition (2/2)
		/ 15 digits continous addition (2/2)
			- 25s each
			- 26s at 15 digits
			- struggle from from 4 days of detrioration

		/ multiplication 20 items of 3 sets

04 20 20
	Test 
		/ random number calculator
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate'

		/ 11 digits continous addition (2/2)
		/ 12 digits continous addition (1/2)
		/ 12 digits continous addition (2/2)
		/ 14 digits continous addition (2/2)
		/ 15 digits continous addition (1/2)

		/ multiplication 20 items of 3 sets

05 01 20
	Test 
		* random number calculator
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate'

		/ 11 digits continous addition (2/2)
		/ 12 digits continous addition (1/2)
		/ 12 digits continous addition (1/2)
		/ 14 digits continous addition (1/2)
		/ 15 digits continous addition (1/2)

		/ multiplication 20 items of 3 sets

05 02 20
	Test
		/ random number calculator
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate and fast'
			
			/ 6-9:2, 6-9:3, 6-9:4, 6-9:5 (5x each)
			/ remarks: 'grinded'

		/ 11 digits continous addition (1/2)
		/ 11 digits continous addition (2/2)
		/ 11 digits continous addition (2/2)
		/ 11 digits continous addition (2/2)
		/ 11 digits continous addition (2/2)
		/ 11 digits continous addition (5/5)
			- 'untimed. grinding accuracy and dealing with huge numbers with 6-9 numbers'

		/ multiplication
			/ 20 items of 3 sets
			/ 6-9:3 20 items (3/3)
			/ 6-9:4 20 items (3/3)

05 14 20
	Test
		/ random number calculator
			/ 20 pairs of (3/3) 25s each
			/ remarks: 'highly accurate and fast'
			
			/ 6-9:2, 6-9:3, 6-9:4, 6-9:5 (5x each)
			/ remarks: 'grinded'

		/ 11 digits continous addition (2/2)
		/ 11 digits continous addition (2/2) 
		/ 11 digits continous addition (2/2) 62 66
		/ 11 digits continous addition (2/2) 52 78
		/ 11 digits continous addition (2/2) 
		/ 11 digits continous addition (5/5)
			- 'forced when automotically given with numbers.'
			- 'force execution skill grinded, gained confidence'
			- 'can do skill grinded'

		/ multiplication
			/ 20 items of (3/3)
			/ 6-9:3 20 items (3/3)
			/ 6-9:4 20 items (3/3)
			/ 6-9:3 or 4 20 items (4/4)
			- 'force execution skill grinded'
			- 'can do skill grinded'

05 20 20
	Test
		* random number calculator
			* 20 pairs of (3/3) 25s each
			* remarks: 'highly accurate and fast'
			
			* 6-9:2, 6-9:3, 6-9:4, 6-9:5 (5x each)
				2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 (5/5) (6-9)
				// 2 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 (0/5) (7-9)
				// 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 (0/5) (7-9)
				// 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 (0/5) (7-9)
				// 2 3 4 5 2 3 4 5 2 3 4 5 2 3 4 5 2 3 4 5 (0/5) (7-9)
				3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 (0/5) (7-9)
			* remarks: 'grinded'
			* remarks:  8+3, 9+3, 7+4, 9+4,

05 23 20
	Test
		/ random number calculator
			/ 20 pairs of (3/3) 25s each
			/ 6-9:2, 6-9:3, 6-9:4, 6-9:5 (3x each)
				2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 (3/3) (6-9)
				3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 (3/3) (7-9)
				4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 (3/3) (7-9)
				3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 (3/3) (7-9)
			- remarks: 'grinded'
			- remarks: 'corrected, accurate' 8+3, 7+4, 9+3, 9+4,


		/ 11 digits continous addition (3/10) 88 84 88 80 86 90 88 81
			- 'difficult sequence' 6 7 9 = 22 not 21
			- 'forced when automotically given with numbers.'
			- 'force execution skill grinded, gained confidence'
			- 'repeat till get 2 streak. 4or2 seconds left not 0second left'
			- 'split screen, timer, write down the answer, fast calculate, match numbers: ADOPTS TO NEW THINGS FORCED TO UNLOCK ACTIVE MEMORY'

		/ multiplication
			/ 20 items of (3/3)
			/ 6-9:3 20 items (0/3)
			/ 6-9:4 20 items (0/3)
			/ 6-9:3or4 20 items (0/4)
			- 'forced when automotically given with numbers.'
			- 'force execution skill grinded'
			- 'repeat till get 2 streak. 4or2 seconds left not 0second left'

		/ basic operations
			- remarks: 'ok'
		/ basic operations with decimal
			- remarks: 'issues with carrying the numbers to the next digit. resovolved by placing point for decimal before doing operaton'

09 24 20
		* random number calculator
			/ 20 pairs of (3/3) 25s each
			* 6-9:2, 6-9:3, 6-9:4, 6-9:5 (3x each)
				2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 (3/3) (6-9)
				3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 (3/3) (7-9)
				4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 (3/3) (7-9)
				5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 (3/3) (7-9)
				3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 (1/3) (7-9)
			- remarks: 'grinded'
			- remarks: 'corrected, accurate' 8+3, 7+4, 9+3, 9+4,

10 01 20
	/ 15 digits continous addition [1-9] (3/3) 
	/ random number calculator
			/ 20 pairs of (2/3) 25s each
			/ 6-9:2, 6-9:3, 6-9:4, 6-9:5 (3x each)
				2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 (3/3) (6-9)
				3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 (3/3) (7-9)
				4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 (3/3) (7-9)
				5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 (3/3) (7-9)
				3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 (3/3) (7-9)
			- remarks: 'grinded'
			- remarks: 'corrected, accurate' 8+3, 7+4, 9+3, 9+4,