instruction:
	select the link below
	press ctrl+alt+enter to open containing folder
	press enter to open the file
	do the test

	randomizer
		// https://www.random.org/lists/

WHAT TO TEST?
	[1]
		* Real Numbers
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\TYPES OF NUMBERS\TEST ITEM.JAVA
		* Basic Operations
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\addition.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\addition 2.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\subtraction.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\division.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\multiplication.png
		* Operations to Decimals
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\decimal addition.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\decimal subtraction.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\decimal multiplication.png
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 1\BASIC OPERATIONS\TEST ITEMS\decimal division.png
	[2]
		* Prime Numbers
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\PRIME NUMBERS\strategy 1.jpg
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\PRIME NUMBERS\Procedures.java
		* Divisible & Multiples
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\DIVISIBILITY RULES\procedures.java
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\DIVISIBILITY RULES\TEST ITEMS\1.PNG
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\DIVISIBILITY RULES\TEST ITEMS\2.PNG
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\DIVISIBILITY RULES\TEST ITEMS\3.PNG
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\DIVISIBILITY RULES\TEST ITEMS\4.PNG
		* Order of Operations
			// D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\CSE\NUMERIC\level 2\ORDERS OF OPERATION\mistakes
		* Lcd and Lcmsa
			// 