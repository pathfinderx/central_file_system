# int
int_a = int(1.2)
int_b = int("1")

# float
float_a = float(1)
float_b = float("1")

# string
str_a = str(1)
str_b = str("1.2")

print(int_a, int_b)
print(float_a, float_b)
print(str_a, str_b)
