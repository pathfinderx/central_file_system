string_a = "Hello world"
string_b = "wisdom is supreme"
string_c = '''Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua.'''
string_d = "abcdefg"

# strings are arrays
print(string_a[0], "\n")

# looping through string
for x in string_a:
    print(x)
    
# read length
print(len(string_a), "\n")

# check if certain text is present inside a text and returns a boolean
print("is" in string_b)
if "me" in string_c:
    print(True)
