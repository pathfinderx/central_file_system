# Dip Son = expected output
# str = "JohnDipPeta"
# str2 = "JaSonAy"
# print(str[4:7]) # Dip
# print(str2[2:5]) # Son


# AuKellylt = expected output
# str3 = "Ault"
# str4 = "Kelly"
# str5 = str3[:2] + str4 + str3[2:]
#print(str5)


# AJrpan = expected output
# str6 = "America"
# str7 = "Japan"
#print(str6[0] + str7[0] + str6[3] + str7[2:])

  
# more examples:
str8 = "abcdefg"
print(str8[:]) # abcdefg
print(str8[::]) # abcdefg 
print(str8[0:3]) # abc
print(str8[3::]) # defg
print(str8[0:6:2]) # ace
print(str8[-1:-7:-1]) # gfedcb
print(str8[0:-2]) # abcde

print(str8[::-2]) # geca
print(str8[::2]) # aceg