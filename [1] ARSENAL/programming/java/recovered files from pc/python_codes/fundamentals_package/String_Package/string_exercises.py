
# yaivePNT = expected output
# str8 = "PyNaTive"
# lowercase = []
# uppercase = []
#
# lowercase = [x for x in str8 if x.islower()]
# uppercase = [x for x in str8 if x.isupper()]
# concat = "".join(lowercase + uppercase)
# print(concat)


# expected output: Total counts of chars, digits,and symbols 
# str9 = "P@#yn26at^&i5ve"
# charsCount = 0
# digitsCount = 0
# symbolsCount = 0
#
# for x in str9:
#     if (x.isalpha()):
#         charsCount+=1
#     elif (x.isnumeric()):
#         digitsCount+=1
#     else:
#         symbolsCount+=1
#

# print("chars: ", charsCount, '\n', "digits: ", digitsCount, "\n", "sumbols: ", symbolsCount)


# given s1 = "Abc" and s2 = "Xyz". Expected output: AzbycX
# str10 = "Abc"
# str11 = "Xyz"
# cont1 = []
#
# for x in range(0,3):
#     cont1.append(str10[x])
#     cont1.append(str11[(x+1) * (-1)])
#
# concat1 = "".join([x for x in cont1])    
# print(concat1)


# expected result: s1 = "Yn" s2 = "PYnative". Expected result: s1 = "Ynf" s2 = "PYnative"
# str12 = "Yn"
# str13 = "Ynf"
# str14 = "PYnative"
#
# print(str12 in str14)
# print(str13 in str14)


# Find all occurrences of �USA� in a given string ignoring the case. Expected result: The USA count is: 2
# str15 = str("Welcome to USA. usa awesome, isnt't it?")
# convertedString = str15.lower()
#
# print(convertedString.count("usa"))


# str1 = "English = 78 Science = 83 Math = 68 History = 65. Expected output: sum is 294 average is 73.5"
# str16 = "English = 78 Science = 83 Math = 68 History = 65"
# cont2 = []
# temp = ""
# count = 0
#
# sum = 0
#
# for x in range(0, len(str16)):
#     if (str16[x].isnumeric()):
#         temp += str16[x]
#         count += 1
#         if (count > 1):
#             cont2.append(int(temp))
#             temp = ""
#             count = 0
#
# for x in cont2:
#     sum += x
#
# print ("sum is: ", sum)
# print ("average is: ", sum / len(cont2))





