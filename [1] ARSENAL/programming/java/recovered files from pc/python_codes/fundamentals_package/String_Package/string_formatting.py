class StringFormatting1 ():
    def __init__(self):
        pass
    
    def setAge (self):
        age = 35
        str = "My age is {}"
        print(str.format(age))
        
    def setFormats (self):
        name = "finix"
        age = 3
        dailyRevenue = 100123.7
        asset = 999333999
        theFormat = "name: {} \n age: {} \n revenue: {} \n asset: {}"
        
        print(theFormat.format(name, age, dailyRevenue, asset))

    def setInsideQoutes (self):
        str2 = "We are the \"Vikings\" from Norway"
        print(str2)
        
    def setOtherFormats (self):
        varQuantity = 23
        varItemNum = 123
        varPrice = 23.45123123
        varStatement = "I have {} piece/s of shoes with item number: {} and with {} price"
        print(varStatement.format(varQuantity, varItemNum, varPrice))
        
    def setOtherFormats2 (self, number, direction1, direction2):
        varNewFormattedString = "I have {} hands the {} and the {}"
        print(varNewFormattedString.format(number,direction1,direction2))
        
    
    def setFormatIndexing (self, age, name):
        varStringSequence = "His is name is {1}. {1} is {0} years old"
        print(varStringSequence.format(age,name))
    
stringFormatting = StringFormatting1()
stringFormatting.setOtherFormats()
stringFormatting.setOtherFormats2(2, "left", "right")
stringFormatting.setFormatIndexing(23, "Pheonix the cat")