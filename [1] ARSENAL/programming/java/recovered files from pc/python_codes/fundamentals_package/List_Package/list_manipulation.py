from pickle import FALSE
    # DECLARATION
list1 = ["apple", "banana", "orange"]
list2 = ["apple", 24, 23j, 23.4, True, False]
list3 = list(("apple", "banana")) # can be instantiated with list() constructor
list4 = [5, 4, 3, 1, 2]
list5 = [
        [1,2,4],
        [4,5,6],
        [7,8,9]
    ]

    # ACCESS

# access through indexing
# print(list1[1])
# print(list1[1::])
# print(list1[-1::-1])

# access through loop
# for x in list4:
#     for y in range(x):
#         print(y, end='') # use end in 2nd param to continue printing instead of newline
#     print()

# access through multiple loops
# loop through the multiple row list
# for x in list5:
#     for y in x:
#         print(y, " ", end='')
#     print()    

    # CHANGE VALUES

# through indexing
# list4[0::] = "apple"
# print(list4)

# through unpacking
# for x in range(0, len(list4)):
#     list4[x] = "apple"
# print(list4)

# change values more than the target index
# list1[1:2] = ["apple", "banana", "cacao", "durian"]
# print(list1)

    # APPEND ITEMS
    
# use append() to insert one item
# list1.append("unggoy")
# print(list1)

# use insert() to specify index
# list1.insert(0, "marang")
# print(list1)

# use extend to append element from another list
# list1.extend(list2)
# print(list1)

    # REMOVE ITEM

# use remove(value) to remove item by value
# list1.remove("apple")
# print(list1)
    
# use pop(index) to remove item by index or by use pop() to remove the last item
# list1.pop()
# print(list1)

# use del to remove item by index or delete all items completely
# del list1[0]
# print(list1)

# del list1
# print(list1)

    # LOOP LIST
    
# use for loop
# for x in list1:
#     print(x)
    
# use for loop through index
# for x in range(len(list1)):
#     print(x)

# use while loop through in index
# limit = 0
# while limit < len(list1):
#     print(list1[limit])
#     limit+=1
#

    # LIST COMPREHENSION
# list comprehension in list
# listValues = [x for x in list1]
# print (listValues)

# list comprehension in string
# str1 = "human"
# stringValues = [x for x in str1]
# print(stringValues)


    # SORT LIST
    
# manual not optimized

# higher = 0
# checker = False
#
# for x in range(len(list4)):
#     for y in range(len(list4) - 1):
#         if (list4[y] > list4[y+1]):
#             list4[y], list4[y+1] = list4[y+1], list4[y]
#
# print(list4)

# manual optimized
# not_swapping = True
#
# while (not_swapping):
#     not_swapping = False
#     for x in range(len(list4) - 1):
#         if (list4[x] > list4[x+1]):
#             list4[x], list4[x+1] = list4[x+1], list4[x]
#             not_swapping = True
#
# print(list4)

# ascending sort    
# list4.sort()
# print(list4)

# descending sort
# list4.sort()
# list4.reverse()
# print(list4)


    # MAKE A COPY
# list6 = list4.copy()
# print(list6)




















