a, b, c = ["cat", "mouse", "food"]

x = lambda a, b, c : a + " eats " + b + " eats " + c
print (x(a,b,c))

def func_anonymous (n):
    return lambda a : a * n

func_double = func_anonymous(2)
func_triple = func_anonymous(3)

print(func_double(11))
print(func_double(12))
print(func_triple(12))