def print_message(msg): # outer enclosing function
	def printer(): # inner function
		print (msg)
	return printer # returns the inner function

another = print_message("hello")
another() # first class function style: making the variable be callable like a function

def make_multiplier_of(n):
	def multiplier(x):
		return x * n
	return multiplier

by3 = make_multiplier_of(3) # preload the outer closure parameter 'n' (experimental)
by5 = make_multiplier_of(5) # preload the outer closure parameter 'n' (experimental)

print(by3(7)) # execute the 'x'
print(by5(7))