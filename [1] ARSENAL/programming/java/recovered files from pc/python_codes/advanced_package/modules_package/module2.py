import module1

class ModuleClass2:
    def __init__(self):
        self.module1 = module1.ModuleClass1()
    
    def execute (self):
        self.module1.greetings()
        print(self.module1.phrase)
        print(self.module1.number)
        
module2 = ModuleClass2()
module2.execute()

