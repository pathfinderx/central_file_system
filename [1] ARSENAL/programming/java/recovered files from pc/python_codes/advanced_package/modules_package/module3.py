import module1 as mod1

class ModuleClass3():
    def __init__(self):
        self.mod = mod1.ModuleClass1()

    def execute (self):
        print(self.mod.phrase)
        print(self.mod.number)
        print(self.mod.greetings())
        
module3 = ModuleClass3()
module3.execute()