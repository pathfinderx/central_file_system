''' 
    arguments
    keyword arguments
    arbitrary arguments
    arbitrary keyword arguments
    default arguments
    
    passing variables to the argument
    return
    pass
'''

def func_arguments (fname, lname):
    print(fname, lname)
func_arguments("John", "Smith")

def func_keyword_arguments (fname, lname):
    print(fname, lname)
func_keyword_arguments (fname = "John", lname = "Smith")

def func_arbitrary_arguments (*args):
    print (args[0], args[1])
func_arbitrary_arguments("John", "Smith")

def func_arbitrary_keyword_arguments (**kwargs):
    print(kwargs["fname"], kwargs["lname"])
func_arbitrary_keyword_arguments(fname = "John", lname = "Smith")

def func_default_arguments (fname = "John", lname = "Smith"):
    print(fname,lname)
func_default_arguments (fname="Pheonis", lname="The Cat")
func_default_arguments ()

dict1 = {"a":56, "b":23, "c":1}

def func_variables_type (args):
    return args
print(func_variables_type(dict1))

def func_break (args):
    pass
print(func_break(dict1))

