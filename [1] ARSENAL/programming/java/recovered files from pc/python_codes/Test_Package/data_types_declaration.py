sampleString = "Hello World"
sampleInt = 1
sampleFloat = 1.5
sampleComplex = 1j

sampleList = ["apple", "orange", 1, 2, 3]
sampleTuple = ("apple", "orange", 1, 2, 3)
sampleSet = {"apple", "John", 1, 3, 4}
sampleFrozenSet = ({"apple", "banana", "cacao"})
sampleDict = {"apple" : "caponan", "age" : 24}
sampleDict2 = {12 : "A", 3 : "B"} 

sampleRange = range(100)

sampleBoolean = True

print(sampleString)
print(sampleInt)
print(sampleFloat)
print(sampleComplex)
print(sampleList)
print(sampleSet)
print(sampleFrozenSet)
print(sampleDict)
print(sampleRange)
print(sampleBoolean)
print(sampleDict2)