# create class
class Myclass ():
    x = 5
    name = "The Creator"

# create objects, instantiation of class as object
# accessing of attributes 
myObject = Myclass()
print(myObject.x)
print(myObject.name)
print()  
    
# __init__() function arbitrary keyword argument
class MyClass2:
    def __init__(self, **kwargs):
        self.fname = kwargs["fname"]
        self.lname = kwargs["lname"]
        
myObject2 = MyClass2(fname = "John", lname = "Smith")
print(myObject2.fname, myObject2.lname)

# __init__() function with arbitrary argument
class MyClass3:
    def __init__(self, *args):
        self.fname = args[0]
        self.lname = args[1]
        self.age = args[2]
        
myObject3 = MyClass3("John", "Smith")
print(myObject3.fname, myObject3.lname)