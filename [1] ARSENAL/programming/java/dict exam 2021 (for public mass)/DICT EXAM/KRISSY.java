import java.io.*;
import java.util.*;

public class KRISSY {
    public static void main (String args []) {
        execute ();
    }

    public static void execute () {
        try {
            FileReader fileread = new FileReader("C:\\Users\\PathfinderX\\Desktop\\DICT EXAM\\source\\krissyNumber.txt");
            Scanner scan = new Scanner (fileread);

            while (scan.hasNextLine()) {
                String given = scan.nextLine(); // read and initialize
                String octal = Integer.toOctalString(Integer.parseInt(given)); // convert decimal to octal
                String classified = sort(octal); // sort the octal string

                while (!isSorted(octal,classified)) { // if not sorted then proceed else return the result as decimal converted value
                    octal = convertToOctalString(octal,classified); // difference
                    classified = sort(octal); // re-sort
                }

                System.out.println(parse(classified));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String sort (String data) {
        ArrayList <Integer> cont = new ArrayList <Integer> ();
        String result = "";

        for (int i = 0; i <= data.length() - 1; i++) {
            cont.add(data.charAt(i) - '0');
        }

        Collections.sort(cont);

        for (int i = 0; i <= cont.size() - 1; i++) {
            result += String.valueOf(cont.get(i));
        }

        return result;
    }

    public static Boolean isSorted (String octal, String classified) {
        return (octal.equalsIgnoreCase(classified)) ? true : false;
    }

    public static String convertToOctalString (String arg1, String arg2) {
        return Integer.toOctalString(parse(arg1) - parse(arg2));
    }

    public static int parse (String arg) {
        return Integer.parseInt(arg, 8);
    }
}