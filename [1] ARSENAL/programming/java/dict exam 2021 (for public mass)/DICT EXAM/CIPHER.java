import java.io.*;
import java.util.*;
import java.util.regex.*;

public class CIPHER {
	public static ArrayList <String> plaintext = new ArrayList <String> (Arrays.asList(
			"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
	));
	//		INDEX:
	//  	 0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25 

	public static ArrayList <Integer> ciphertext = new ArrayList <Integer> (Arrays.asList(
			22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47
	));
	
	public static ArrayList <String> container = new ArrayList <String> ();
	
	public static void main (String args []) {
		execute();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\PathfinderX\\Desktop\\DICT\\source\\encryptedMessage.txt");
			Scanner sc = new Scanner (fileread);
			
			while (sc.hasNextLine()) { 
				String data = sc.nextLine().replaceAll("[^0-9]", ""); // regex to replace anything that is not an integer
				if (validate(data)) {
					System.out.println(decrypt(data));
				} else {
					System.out.println("Invalid data, must be numeric and pair to proceed to decryption process");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean validate (String data) {
		boolean isNumeric = Pattern.matches("[0-9]+", data); // if every character is an integer then proceed
		boolean isEven = data.length() % 2 == 0; // check if is even (means, pair (double digit))
		
		return (isNumeric && isEven) ? true : false; // ternary operation; if both condition is true
	}
	
	public static String decrypt (String data) {
		int count = 0; String temp = "";
		int shift = 3; String decryptedMessage = "";
		
		for (int i = 0; i <= data.length() - 1; i++) {
			String value = String.valueOf(data.charAt(i));

			if (count == 0) { // this indicates the 1st digit (as indicator)
				count++; temp += value; 
			} else if (count == 1) { // this indicates that it reaches the 2nd digit (as indicator)
				count = 0; temp += value; 
				container.add(temp); temp = ""; // assign to arraylist
			}
		}
		
		for (int i = 0; i <= container.size() - 1; i++) {
			int decryptedIndex = ciphertext.indexOf(Integer.parseInt(container.get(i))); // gets the index of cipher text based on the primordial message given
			int reverseIndex = decryptedIndex - shift; // depends on how many shifts and depends if it shifts to positive or negative direction
			
			if (reverseIndex < 0) { // if it reaches index - 0 (if it uses negative shift)
				int resetIndex = plaintext.size() + reverseIndex; // resets back to highest index
				decryptedMessage += plaintext.get(resetIndex); // use the highest index
			} else {
				decryptedMessage += plaintext.get(reverseIndex); // use the regular difference shift
			}
		}
		
		container.clear();
		return decryptedMessage;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}