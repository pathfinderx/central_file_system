import java.io.*;
import java.util.*;
import java.util.regex.*;

public class KRISSY {
    public static void main (String args []) {
        execute ();
    }

    public static void execute () {
        try {
            FileReader fileread = new FileReader("C:\\Users\\PathfinderX\\Desktop\\DICT\\source\\krissyNumbers.txt");
            Scanner sc = new Scanner (fileread);

            while (sc.hasNextLine()) {
                String z = sc.nextLine();
                String c = Integer.toOctalString(Integer.parseInt(z));
                String s = sort(c);

                while (!c.equalsIgnoreCase(s)) {
                    c = Integer.toOctalString(Integer.parseInt(c, 8) - Integer.parseInt(s, 8));
                    s = sort(c);
                }

                System.out.println(Integer.parseInt(s, 8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String sort (String data) {
        ArrayList <Integer> cont = new ArrayList <Integer> ();
        String result = "";

        for (int i = 0; i <= data.length() - 1; i++) {
            cont.add(data.charAt(i) - '0');
        }

        Collections.sort(cont);

        for (int i = 0; i <= cont.size() - 1; i++) {
            result += String.valueOf(cont.get(i));
        }

        return result;
    }
}