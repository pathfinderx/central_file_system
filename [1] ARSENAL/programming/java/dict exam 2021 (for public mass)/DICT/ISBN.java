import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class ISBN {
	public static void main (String args []) {
		execute("C:\\Users\\PathfinderX\\Desktop\\DICT\\source\\isbn.txt");
	}
	
	public static void execute (String file_read) {
		try {
			FileReader file = new FileReader (file_read);
			Scanner sc = new Scanner (file);
			File file_write = new File ("C:\\Users\\PathfinderX\\Desktop\\DICT\\output\\isbn_write.txt");

			FileWriter fw = new FileWriter(file_write);
			PrintWriter print = new PrintWriter(fw);
			int validCounter = 0;
			int invalidCounter = 0;
			
			while (sc.hasNextLine()) {	
				String dataOfEveryLine = sc.nextLine();
				if (validateISBN(getisbn10Data(String.valueOf(dataOfEveryLine)))) {
					validCounter ++;
					print.println(getisbn10Data(dataOfEveryLine));
				} else {
					invalidCounter ++;
					System.out.println(getisbn10Data(dataOfEveryLine) + " is invalid");
				}
			}
			
			System.out.println("Total number valid isbn: " + validCounter);
			System.out.println("Total number of invalid isbn number: " + invalidCounter);
			System.out.println("Total number of isbn numbers tested: " + (validCounter + invalidCounter));
			print.close();
			// System.out.println(file_write.getName() + " successfully created");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getisbn10Data (String data) {
		String isbn10Data = "";
		int count = 0;
		int len = data.length();
		
		for (int i = 0; i <= len - 1; i++) {
			if (validateCharacter("[0-9X]", String.valueOf(data.charAt(i)))) {
				if (count < 10) {
					isbn10Data += String.valueOf(data.charAt(i));
					count++;
				}
			} else if (validateCharacter("[-]", String.valueOf(data.charAt(i)))) {
				// dashes will be skipped during loop and do nothing
			} else if (count < 10 && validateCharacter("[^0-9]", String.valueOf(data.charAt(i)))) {
				isbn10Data = "";
				count = 0;
			}
		}
		return isbn10Data;
	}
	
	public static boolean validateCharacter (String regexPattern, String data) {
		return (Pattern.matches(regexPattern, data) ? true : false);
	}
	
	public static boolean validateISBN (String isbn10Data) {
		int len = isbn10Data.length();
		int product = 0;
		
		for (int i = 0; i <= len - 1; i++) {
			if (isbn10Data.charAt(i) == 'X') {
				product += 10;
			} else {
				product += (isbn10Data.charAt(i) - '0') * (10 - i);
			}
		}
		
		return ((product % 11 == 0) ? true : false);
	}
}