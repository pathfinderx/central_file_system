package regex;
import java.util.regex.*;

public class RegexClass {
	public static void main (String args []) {
		System.out.println(prototype1());
		System.out.println(prototype2());
		System.out.println(prototype3());
	}
	
	public static boolean prototype1 () {
		Pattern p = Pattern.compile(".s");
		Matcher m = p.matcher("as");
		boolean checker = m.matches();
		
		return checker;
	}
	
	public static boolean prototype2 () {
		boolean checker = Pattern.compile(".s").matcher("as").matches();
		return checker;
	}
	
	public static boolean prototype3 () {
		return (Pattern.matches(".s", "as") ? true : false);
	}
}