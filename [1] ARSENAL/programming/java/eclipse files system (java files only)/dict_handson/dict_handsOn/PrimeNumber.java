package dict_handsOn;

import java.util.Scanner;

public class PrimeNumber {
	public static Scanner sc = new Scanner (System.in);
	public static boolean checker = false;
	
	public static void main (String args []) {
		choices();
	}
	
	public static void choices () {
		do {
			System.out.println("Enter a number to test if it is a prime number or not");
			int choice1 = sc.nextInt();
			primeChecker(choice1);
			
		} while (checker != true);
	}
	
	public static void primeChecker (int num) {
		if (num == 2) {
			System.out.println("Prime");
			System.out.println();
		}
		else if (num == 1) {
			System.out.println("Not Prime");
			System.out.println();
		}
		else if (num % 2 == 0) {
			System.out.println("Not Prime");
			System.out.println();
		}
		else {
			System.out.println("Prime");
			System.out.println();
		}
	}
}
