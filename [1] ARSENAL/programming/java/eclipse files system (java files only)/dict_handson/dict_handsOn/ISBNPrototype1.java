package dict_handsOn;

public class ISBNPrototype1 {
	public static void main (String args []) {
		if (execute() == true) {
			System.out.println("valid isbn number");
		} else {
			System.out.println("invalid isbn number");
		}
	}
	
	public static boolean execute () {
		String isbn = "007462542X";
		int sum = 0;
		
		if (isbn.length() == 10) {
			for (int i = 0; i < isbn.length() - 1; i++) {
				sum += (isbn.charAt(i) - '0') * (11 - (i + 1));
			}
			
			int lastValue = isbn.charAt(isbn.length() - 1);
			if (lastValue == 'X') {
				sum += 10;
			}
			else if (lastValue >= '0' && lastValue < '9') {
				sum += isbn.charAt(isbn.length() - 1) - '0' * 1;
			}
			else {
				return false;
			}
			
			return (sum % 11 == 0) ? true : false;
			
		} else {
			return false;
		}	
	}
}
