package dict_handsOn;

import java.util.ArrayList;

public class Fibonacci {
	public static void main (String args []) {
		execute();
	}
	
	public static void execute () {
		int limit = 10;
		ArrayList<Integer> intList = new ArrayList <Integer>();
		
		for (int i = 0; i <= limit; i++) {
			if (intList.size() > 1) {
				int n1 = intList.get(i - 1);
				int n2 = intList.get(i - 2);
				intList.add(n1 + n2);
			}
			else {
				intList.add(i);
			}
		}
		
		
		for (int i = 0; i <= intList.size() - 1; i++) {
			System.out.print(intList.get(i) + ", ");
		}
	}
}
