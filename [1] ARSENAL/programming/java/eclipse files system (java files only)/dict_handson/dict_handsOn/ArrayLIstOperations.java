package dict_handsOn;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLIstOperations {
	public static Scanner scan = new Scanner (System.in);
	public static ArrayList <Integer> arrayList = new ArrayList <Integer>();
	public static boolean checker = false;
	
	public static void main (String args []) {
		do {
			System.out.println("Enter the key to perform operation: ");
			System.out.println("1: read the size of the array");
			System.out.println("2: get the value by specific index in array");
			System.out.println("3: add value to the array");
			System.out.println("4: update specific value in array: set (key, value)");
			System.out.println("5: remove specific value in array");
			int key = scan.nextInt();
			switcher(key);
		} while (checker != true);
	}
	
	public static void switcher (int key) {
		switch (key) {
			case 1: 
				System.out.println("the size of the array: " + arrayList.size());
				System.out.println();
				break;
			case 2: 
				System.out.println("Enter the key to get the pair in the array");
				int index = scan.nextInt();
				
				// update this condition with isEmpty test
				if (index < arrayList.size() - 1 || index > arrayList.size() - 1 || arrayList.isEmpty()) {
					System.out.println("Out of bounds or the array has no data yet");
					System.out.println();
				} else {
					System.out.println("Key: " + index + " " + "value: " + arrayList.get(index));
					System.out.println();
				}
				break;
			case 3:
				System.out.println("add an integer to the array list");
				int input = scan.nextInt();
				arrayList.add(input);
				
				System.out.println("the integer " + input + " was added to the array list");
				System.out.println();
				break;
			case 4:
				System.out.print("This are all the value in the array list: " + arrayList);
				System.out.println();
				System.out.println("what value in the array list you want to change?");
				int inputValue = scan.nextInt();
				
				if (arrayList.indexOf(inputValue) > arrayList.size() - 1 || arrayList.indexOf(inputValue) < arrayList.size() -1) {
					System.out.println("the value you input is not in the arraylist");
					System.out.println();
				} else {
					int indexOfTheSearchedValue = arrayList.indexOf(inputValue);
					int formerValue = arrayList.get(indexOfTheSearchedValue);
					System.out.println("The value you searched: " + arrayList.get(indexOfTheSearchedValue));
					System.out.println();
					
					System.out.println("Enter a new value to perform an update operation");
					int inputNewValue = scan.nextInt();
					// perform update operation
					arrayList.set(indexOfTheSearchedValue, inputNewValue);
					
					// print the changed value
					System.out.println("This is the former value: " + formerValue + " This is the new value: " + inputNewValue);
					// print the arrayList
					System.out.println("Update operation successful!");
					System.out.println(arrayList);
				}
				break;
			case 5:
				System.out.println(arrayList);
				System.out.println("what value in the array list you want to change?");
				int searchedValue = scan.nextInt();
				int tempSearchValue;
				
				// update this condition with isEmpty test
				if (arrayList.indexOf(searchedValue) < arrayList.size() - 1 || arrayList.indexOf(searchedValue) > arrayList.size() - 1 || arrayList.isEmpty()) {
					System.out.println("Not found in the array");
				} else {
					tempSearchValue = arrayList.get(arrayList.indexOf(arrayList));
					System.out.println("This is the value you want to remove: " + arrayList.get(arrayList.indexOf(searchedValue)));
					arrayList.remove(arrayList.indexOf(searchedValue));
					
					System.out.println("Value: " + tempSearchValue + " was removed from the array list");
					System.out.println(arrayList);
					System.out.println();
				}
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + key);
		}
	}
}
