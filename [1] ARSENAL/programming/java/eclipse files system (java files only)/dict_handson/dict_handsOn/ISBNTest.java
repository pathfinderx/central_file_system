package dict_handsOn;

import java.util.ArrayList;
import java.util.Scanner;

public class ISBNTest {
	public static Scanner scan = new Scanner (System.in);
	public static ArrayList <Long> digitList = new ArrayList <Long>();
	
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		System.out.println("Enter a number to check if valid isbn number");
		long isbn = scan.nextLong();
		long tempISBN = isbn;
		long finalResult = 0;
		
		while (tempISBN > 0) {
			long lastDigit = tempISBN % 10;
			tempISBN = tempISBN / 10;
			digitList.add(lastDigit);
		}
		
		System.out.println(digitList);
		
		for (int i = 1; i <= digitList.size() ; i++) {
			finalResult += digitList.get(i - 1) * i;
			System.out.println(i + " x " + digitList.get(i - 1) + " = " + i * digitList.get(i - 1));
		}

		System.out.println("= " + finalResult);
		System.out.println();
		
		if (finalResult % 11 == 0) {
			System.out.println("The number you input is a valid isbn number");
		} else {
			System.out.println("The number you input is an invalid isbn number");
		}
	}
}
