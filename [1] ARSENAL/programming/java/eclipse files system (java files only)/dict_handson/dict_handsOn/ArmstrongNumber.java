package dict_handsOn;
import java.util.*;

public class ArmstrongNumber {
	public static Scanner scan = new Scanner (System.in);
	
	public static void main (String args []) {
		do {
			System.out.println("Enter a number: ");
			int input = scan.nextInt();
			execute(input);
		} while (1 != 2);
	}
	
	public static void execute (int input) {
		int twin = input;
		int finalResult = 0;
		
		while (twin > 0) {
			int lastDigit = twin % 10;
			twin = twin / 10;
			finalResult += armstrongNumber(lastDigit);
		}
		
		if (input ==  finalResult) {
			System.out.println("Armstrong");
		} else {
			System.out.println("Not armstrong");
		}
		System.out.println(finalResult);
	}
	
	public static int armstrongNumber (int num) {
		int result = 1;
		for (int i = 1; i <= 3; i++) {
			result *= num;
		}
		return result;
	}
}
