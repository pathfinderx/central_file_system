package int_manipulation;
import java.math.RoundingMode;
import java.math.BigDecimal;

public class DecimalManipulation {
	public static void main (String args []) {
		removeDecimals(3.1416);
		setDecimalPoints(3.1416);
	}
	
	public static void removeDecimals (double input) {
		String converted = String.valueOf(input);
		converted = converted.substring(converted.indexOf(".") + 1);
		System.out.println(converted);
	}
	
	public static void setDecimalPoints (double input) {
		BigDecimal bd = new BigDecimal (input);
		bd = bd.setScale(2, RoundingMode.HALF_DOWN);
		double converted = bd.doubleValue();
	}
}