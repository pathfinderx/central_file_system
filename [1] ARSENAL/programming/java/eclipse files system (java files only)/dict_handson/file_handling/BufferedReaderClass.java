package file_handling;
import java.io.*;
import java.util.*;

public class BufferedReaderClass {
	public static void main (String args []) {
		execute("C:\\Users\\kurt kevin nebril\\Desktop\\readThis.txt");
	}
	
	public static void execute (String link) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(link));
			while (br.ready()) {
				System.out.println((String)br.readLine());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
