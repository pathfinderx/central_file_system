package basic_programs;
import java.util.Scanner;

public class PalindromeString {
	public static Scanner scan = new Scanner (System.in);	
	
	public static void main (String args []) {
		do {
			System.out.println("Enter a string to check if string is palindrome");
			String input = scan.nextLine();
			palindrome(input);
		} while (1 != 2);
	}
	
	public static void palindrome (String input) {
		String copy = input;
		String container = "";
		
		for (int i = copy.length() - 1; i >= 0; i--) {
			container = container + copy.charAt(i);
		}
		
		if (container.toLowerCase().equals(input.toLowerCase())) {
			System.out.println("Palindrome");
		} else {
			System.out.println("Not Palindrome");
		}
	}
}
