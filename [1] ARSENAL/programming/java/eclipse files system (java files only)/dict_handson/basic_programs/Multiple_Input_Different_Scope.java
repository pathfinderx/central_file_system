package basic_programs;
import java.util.*;
import java.util.regex.*;

public class Multiple_Input_Different_Scope {
	public static Scanner sc = null;
	
	public static void main (String args []) {
		do {
			sc = new Scanner(System.in);
			System.out.println("[1] to convert string to int");
			int choice = sc.nextInt();
			
			execute_switch(choice);
		} while (1 != 2);
		
	}
	
	public static void execute_switch (int choice) {
		switch (choice) {
			case 1 :
				string_int();
				break;
			default:
				System.out.println("None of the choices just select from [1]");
		}
	}
	
	public static void string_int () {
		sc = new Scanner(System.in);
		System.out.println("Enter string to test convert to int");
		String str = sc.nextLine();
		
		int converted = Integer.parseInt(str);
		
		System.out.println(converted);
	}
}