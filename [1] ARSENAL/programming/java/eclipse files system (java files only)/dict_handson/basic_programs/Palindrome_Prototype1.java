package basic_programs;
import java.util.Scanner;

public class Palindrome_Prototype1 {
	public static Scanner sc = new Scanner (System.in);
	
	public static void main (String args []) {
		execute();
	}
	
	public static void execute () {
		do {
			System.out.println("Enter a number to check if Palindrome");
			int input = sc.nextInt();
			palindromeChecker(input);
		} while (1 != 2);
	}
	
	public static void palindromeChecker (int num) {
		int copy = num;
		String container = "";
		
		while (copy > 0) {
			int lastDigit = copy % 10;
			copy = copy / 10;
			container += lastDigit;
		}
		
		int temp = Integer.parseInt(container);
	
		if (temp == num) {
			System.out.println("Palindrome");
		}
		else {
			System.out.println("Not Palindrome");
		}
	}
}
