package basic_programs;
import java.util.ArrayList;
import java.util.Scanner;

public class Palindrome {
	public static Scanner sc = new Scanner(System.in);
	public static ArrayList <Integer> container = new ArrayList<Integer>();
	public static ArrayList <Integer> subcontainer = new ArrayList<Integer>();
	public static ArrayList <Integer> subcontainer2 = new ArrayList<Integer>();
	public static boolean checker = false;
	
	public static void main (String args []) {
		isPalindrome(); 
	}
	
	public static void isPalindrome () {
		System.out.println("Enter a number to check if palindrome: ");
		int input = sc.nextInt();
		int twin = input;
		int twin2 = input;
		int limit = 0;
		
		while (twin > 0) { // the extractor, the limit identifier
			int lastDigit = twin % 10; // extract the last digit
			twin = twin / 10; // reduce 1 digit
			container.add(lastDigit);
			limit++;
		}
		
		if (limit % 2 > 0) {
			for (int i = limit / 2; i >= 0; i--) {
				subcontainer.add(container.get(i));
			}
			
			for (int i = limit / 2; i < limit; i++) {
				subcontainer2.add(container.get(i));
			}
			
			for (int i = 0; i < subcontainer.size(); i++) {
				if (subcontainer.get(i) == subcontainer2.get(i)) {
					checker = true;
				}
				else {
					checker = false;
					break;
				}
			}
			
			if (checker == true) {
				System.out.println("Palindrome");
			}
			else if (checker == false) {
				System.out.println("Not palindrome");
			}
			System.out.println();
		}
		else {
			System.out.println("Not palindrome");
		}
	}
}