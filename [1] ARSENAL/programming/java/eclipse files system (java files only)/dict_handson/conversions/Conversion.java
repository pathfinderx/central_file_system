package conversions;
import java.util.*;
import java.util.regex.*;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

public class Conversion {
	public static Scanner sc = new Scanner (System.in);
	public static boolean checker = false;
		
	public static void main (String args []) {
		sc = new Scanner (System.in);
		int choice;
		
		do {
			System.out.println("[1] int to string");
			System.out.println("[2] string to int");
			System.out.println("[3] long to string");
			System.out.println("[4] string to long");
			System.out.println("[5] float to string");
			System.out.println("[6] string to float");
			System.out.println("[7] double to string");
			System.out.println("[8] string to double");
			System.out.println("[0] to end the program");
			
			while (!sc.hasNextInt()) {
				System.out.println("Please input an integer");
				sc.next();
			}
			
			choice = sc.nextInt();
			switch (choice) {
			case 1: {
				int_string();
				break;
			}
			case 2: {
				string_int();
				break;
			}
			case 3: {
				long_string();
				break;
			}
			case 4: {
				string_long();
				break;
			}
			case 5: {
				float_string();
				break;
			}
			case 6: {
				string_float();
				break;
			}
			case 7: {
				double_string();
				break;
			}
			case 8: {
				string_double();
				break;
			}
			case 0: {
				System.out.println("Program ended! thank you!");
				checker = true;
				break;
			}
			default:
				System.out.println("Please enter the choices provided");
				break;
			}
		} while (checker != true);	
	}
		
	public static void int_string () {
		sc = new Scanner (System.in);
		int x;
		
		do {
			System.out.println("Enter int to convert to string");
			System.out.println();
			while (!sc.hasNextInt()) {
				System.out.println("Please input integer!");
				System.out.println();
				sc.next();
			}
			x = sc.nextInt();
		} while (x <= 0);
		
		//convert
		String converted = Integer.toString(x);
		System.out.println(converted);
		System.out.println();
	}

	public static void string_int () {
		sc = new Scanner (System.in);
		
		System.out.println("Enter a number (string in data type) to convert to int");
		String x = sc.nextLine();
		boolean isValid = Pattern.compile("[0-9]+").matcher(x).matches();
		
		if (isValid == true) {
			int converted = Integer.parseInt(x);
			System.out.println("Here is the converted string to int: " + converted);
			System.out.println();
			sc.next();
		} else {
			System.out.println("input is invalid");
			System.out.println();
		}
	}

	public static void long_string () {
		sc = new Scanner (System.in);
		System.out.println("Enter long to convert to string");
		long x;
		boolean isValid = false;
		
		do {
			while (!sc.hasNextLong()) {
				System.out.println("Invalid input, please input long");
				sc.next();
			}
			x = sc.nextLong();
			isValid = true;
		} while (isValid == false);

		String converted = Long.toString(x);
		System.out.println("String to long: " + converted);
		System.out.println();
	}

	public static void string_long () {
		System.out.println("Enter string (integer type) to convert to long");
		sc = new Scanner (System.in);
		
		String input = sc.nextLine();
		boolean isValid = Pattern.compile("[0-9]+").matcher(input).matches();

		if (isValid) {
			long converted = Long.parseLong(input);
			System.out.println("converted string to long: " + converted);
			System.out.println();
		} else {
			System.out.println("Invalid input format, use integer to convert int to string");
			System.out.println();
		}
	}
	
	public static void float_string () {
		System.out.println("Enter float to convert to string");
		sc = new Scanner (System.in);
		float input;
		boolean isValid = false;
		
		do {	
			while (!sc.hasNextFloat()) {
				System.out.println("Invalid input format: enter float to convert to string");
				sc.next();
			}
			input = sc.nextFloat();
			isValid = true;
		} while (isValid == false);

		String converted = Float.toString(input);
		System.out.println("converted! float to string: " + converted);
	}

	public static void string_float () {
		System.out.println("Enter string (decimal format) to convert to float");
		sc = new Scanner (System.in);
		String input = sc.nextLine();
		boolean isValid = Pattern.compile("\\d+\\.\\d{1,}").matcher(input).matches();
		
		if (isValid == true) {
			float converted = Float.parseFloat(input);
			System.out.println("Converted! string to float: " + converted);
		} else {
			System.out.println("Invalid input format, please input decimal format to convert string to float");
		}
	}

	public static void double_string () {
		sc = new Scanner(System.in);
		System.out.println("Enter double to convert to string");
		double input;
		
		while (!sc.hasNextDouble()) {
			System.out.println("Enter number (double format) to convert to string");
			sc.next();
		}
		
		input = sc.nextDouble();
		
		System.out.println(input);
	}

	public static void string_double () {
		System.out.println("Enter string (double format) to convert to double");
		sc = new Scanner (System.in);
		String input = sc.nextLine();
		boolean isValid = Pattern.compile("\\d{1,15}\\.\\d{1,15}$").matcher(input).matches();
		
		if (isValid == true) {
			//convert
			double converted = Double.parseDouble(input);
			System.out.println("converted! string to double: " + converted);
			System.out.println();
		} else {
			System.out.println("Invalid input format cannot convert from string to double");
			System.out.println();
		}
	}
}