package conversions;
import java.util.*;
import java.util.regex.*;

public class Character_Int_Conversion {
	public static Scanner sc = new Scanner (System.in);
	
	public static void main (String args []) {
		do {
			sc = new Scanner (System.in);
			System.out.println("choose the following: ");
			System.out.println("[1] int to character");
			System.out.println("[2] character to int");
			
			while (!sc.hasNextInt()) {
				System.out.println("invalid input format, please enter the following: ");
				sc.next();
			}
			
			int input = sc.nextInt();
			switch (input) {
				case 1: {
					int_character();
					break;
				} case 2: {
					character_int();
					break;
				}
				default: {
					System.out.println("None of the choices, please select the following: ");
					break;
				}
			}
			
		} while (1 != 2);
	}
	
	public static void int_character () {
		sc = new Scanner (System.in);
		int input = 0;
		boolean checker = false;
		System.out.println("Enter int to convert to char");
		
		while (!sc.hasNextInt()) {
			System.out.println("invalid input format, please enter integer to convert to char");
			sc.next();
		}
		checker = true;
		
		get_character(input);
		get_numericalValue(input);
			
	}
	
	public static void get_character (int input) {
		char converted = (char) input;
		System.out.println("get_character: " + converted);
	}
	
	public static void get_numericalValue (int input) {
		char converted = Character.forDigit(input, 10);
		System.out.println("get_numericalValue: " + converted);
		
	}

	public static void character_int () {
		sc = new Scanner (System.in);
		System.out.println("Enter character to convert to int: ");
		char input  = sc.next().charAt(0);
		boolean isValid = Pattern.matches("[0-9]{1}", "input");
		
		if (isValid) {
			System.out.println("character you input must be and integer format to convert to int");
		} else {
			String strConvert = Character.toString(input);
			int converted = Integer.parseInt(strConvert);
			System.out.println(converted);
		}
	}
	
	public static boolean validate_input (char input) {
		return true;
	}
}