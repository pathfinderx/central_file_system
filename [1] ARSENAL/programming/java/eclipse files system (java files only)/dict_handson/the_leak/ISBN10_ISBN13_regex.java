package the_leak;
import java.util.*;
import java.util.regex.*;

public class ISBN10_ISBN13_regex {
	public static Scanner scan = new Scanner (System.in);
	
	public static void main (String args []) {
		do {
			System.out.println("Enter a number to check if it is valid isbn number");
			String input = scan.nextLine();
			execute(input);
		} while (1 != 2);
	}
	
	public static void execute (String input) {
		// replace all using regex
		String copy = input;
		String replaced = copy.replaceAll("[ |-]", "");
		
		//check if digit is 10 or 13
		if (replaced.length() == 10) {
			checkISBN10(replaced);
		} else if (replaced.length() == 13) {
			checkISBN13(replaced);
		} else {
			System.out.println("less than 10 digit or more than 13 digit: invalid isbn number");
			System.out.println();
		}
	}
	
	public static void checkISBN10 (String replaced) {
		String copy = replaced;
		int sum = 0;
		char lastDigit = copy.charAt(copy.length() - 1);
		
		// check first 9 digits
		for (int i = 0; i < copy.length() - 1; i++) {
			sum += (copy.charAt(i) - '0') * (10 - i);
			
		}
		
		// then check the last digit if X == 10, if <= 0 && >= 9
		if (lastDigit == 'X') {
			sum += 10 * 1;
		} else if (lastDigit >= '0' && lastDigit <= '9') {
			sum += convertToInt(lastDigit) * 1;
		} else {
			System.out.println("Invalid ISBN number");
		}
		
		//  modulo 11 if 0 then valid isbn10 number
		if (sum % 11 == 0) {
			System.out.println("valid isbn number");
		} else {
			System.out.println("invalid isbn number");
		}
	}
	
	public static int convertToInt (char value) {
		String converted = Character.toString(value);
		int converted2 = Integer.parseInt(converted);
		
		return converted2;
	}
	
	public static void checkISBN13 (String input) {
		String copy = input;
		int sum = 0;
		
		for (int i = 0; i <= copy.length() - 1; i++) {
			if (i % 2 == 0) {
				sum += (copy.charAt(i)) * 1;
			} else {
				sum += (copy.charAt(i)) * 3;
			}	
		}
		
		if (sum % 10 == 0) {
			System.out.println("valid isbn number");
		} else {
			System.out.println("invalid isbn nubmer");
		}
	}
}