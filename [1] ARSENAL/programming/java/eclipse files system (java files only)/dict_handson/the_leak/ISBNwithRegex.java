package the_leak;
import java.util.regex.*;
import java.util.*;

public class ISBNwithRegex {
	public static Scanner scan = new Scanner(System.in);
	
	public static void main (String args []) {
		do {
			System.out.println("Enter isbn number to validate: ");
			String isbn = scan.nextLine();
			isbn10(isbn);
		} while (1 != 2);
	}

	public static void isbn10 (String isbn) {
		String temp = isbn.replaceAll("[ -]*", "");
		long sum = 0;
		
		if (temp.length() == 10) {
			for (int i = 0; i < temp.length() - 1; i++) {
				sum += (temp.charAt(i) - '0') * (10 - i);
			}
			
			char checkDigit = (temp.charAt(temp.length() - 1));
			String checkDigitString = Character.toString(checkDigit);
			if (checkDigit == 'X') {
				sum += 10 * 1;
			} else if (Pattern.matches("[0-9]", checkDigitString) == true) {
				int convertedLastDigit = Integer.parseInt(checkDigitString);
				sum += 9 * 1;
			}
			
			System.out.println("isbn valid?: " + ((sum % 11 == 0) ? true : false));
			System.out.println();
		} else {
			System.out.println("Not valid isbn number");
			System.out.println();
		}
	}
}