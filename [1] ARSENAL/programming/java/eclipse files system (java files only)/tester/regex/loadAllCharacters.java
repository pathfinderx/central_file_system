package regex;

import java.util.*;
import java.util.regex.*;

public class loadAllCharacters {
	public static ArrayList <Character> charContainer = new ArrayList <Character> ();
	public static String alphabets = "";
	
	public static void main (String args []) {
		loadCharacters ();
		get_regex_result();
	}
	
	public static void get_regex_result () { 		
 		System.out.println(alphabets);
 		boolean result = Pattern.compile("[\\w\\W\\d\\D\\s\\S]").matcher("[abc]").find();
 		
 		System.out.println(result);
	}
	
	public static void loadCharacters () { // THIS CHARACTER LOADING COULD BE OBSOLETE, "alphabets" WONT WORK WITH compile
		char startCharacter = '!'; // STARTING CHARACTER
		char endCharacter  = '~'; // ENDING CHARACTER
		
		// THIS METHOD IS USED TO LOAD ALL CHARACTERS (alphanumberic and special characters)
		for (int i = ((int) startCharacter); i <= ((int) endCharacter); i++) {
			charContainer.add((char) i);
		}
		
		for (int i = 0; i <= charContainer.size() - 1; i++) {
			alphabets += String.valueOf(charContainer.get(i));	
		}
	}
}