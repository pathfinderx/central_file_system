package krissy;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Krissy {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\kurt kevin nebril\\Desktop\\dict attempt2\\krissy\\krissyCodes.txt");
			Scanner sc = new Scanner (fileread);
			String result = "";
			
			while (sc.hasNextLine()) {
				String z = sc.nextLine();
				String s = Integer.toOctalString(Integer.parseInt(z));
				String c = sort(s);
				System.out.println("first read: " +  z);
				System.out.println("first convert: " + s);
				System.out.println("first sort: " + c);

				while (!s.equalsIgnoreCase(c)) {
					System.out.println(c);
					z = s;
					s = Integer.toOctalString(Integer.parseInt(z));
					c = sort(s);
					System.out.println("second read: " +  z);
					System.out.println("second convert: " + s);
					System.out.println("second sort: " + c);
				}
				result += s + "\r\n";
				System.out.println("result: " +result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public static String sort (String data) {
		ArrayList <Integer> container = new ArrayList <Integer> ();
		String text = "";
		for (int i = 0; i <= data.length() - 1; i++) {
			container.add(Integer.parseInt(String.valueOf(data.charAt(i))) );
		}
		Collections.sort(container);
		
		for (Integer contents: container) {
			text += contents;
		}
		
		return text;
	}
}
