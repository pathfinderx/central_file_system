// package cipher;
import java.util.regex.*;

public class encrypt extends CipherClass {
	public static int shiftValue = 0;
	
	public encrypt (String message, int shiftTimes) {
		boolean checker = Pattern.matches("[A-Z]*", message); // validation to accept only capital letters
		
		if (checker == true) {
			shiftValue = shiftTimes;
			System.out.println("message: " + message);
			
			for (int i = 0; i <= message.length() - 1; i++) {
				container.add(plainText.indexOf(Character.toString(message.charAt(i))));
			}
			
			System.out.println("container1 index: " + container);
			shiftTheText();
			
		} else {
			System.out.print("Please enter capital Letters only without any special characters or numbers");
		}
	}
	
	public static void shiftTheText () {
		for (int i = 0; i <= container.size() - 1; i++) {
			int newIndex = container.get(i) + shiftValue;
			if (newIndex > plainText.size() - 1) {
				container.set(i, (newIndex - plainText.size()));
			} else {
				container.set(i, newIndex);
			}
		}
			
		System.out.println("shifted: " + container);
		getCipherTextValue();
	}
	
	public static void getCipherTextValue () {
		String text = "";
		
		for (int i = 0; i <= container.size() - 1; i++) {
			text += cipherText.get(container.get(i));
		}
		System.out.println("new code: copy this, paste to decrypt: " + text);
	}
	
}
