// package cipher;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class CipherDecryption {
	public static ArrayList <String> plaintext = new ArrayList <String> (Arrays.asList(
			"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
	));
	//   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25 

	public static ArrayList <Integer> ciphertext = new ArrayList <Integer> (Arrays.asList(
			22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47
	));
	//		0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  
	
	public static ArrayList <String> container = new ArrayList <String> ();
	
	public static void main (String args []) {
		execute();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\kurt kevin nebril\\Desktop\\dict attempt2\\cipher\\encryptedMessage.txt");
			Scanner sc = new Scanner (fileread);
			
			File filewrite = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\dict attempt2\\cipher\\decodedMessage.txt");
			PrintWriter print = new PrintWriter (new FileWriter (filewrite));
			while (sc.hasNextLine()) {
				String data = sc.nextLine();
				if (validate(data)) {
					print.println(decrypt(data));
//					System.out.println(decrypt(data));
				} else {
					print.println("Invalid data, must be numeric and pair to proceed to decryption process");
				}
			}
			
			print.close();
			System.out.println(filewrite.getName() + " successfully created");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean validate (String data) {
		boolean isNumeric = Pattern.matches("[0-9]+", data);
		boolean isEven = data.length() % 2 == 0;
		
		return (isNumeric && isEven) ? true : false;
	}
	
	public static String decrypt (String data) {
		int count = 0; String temp = "";
		int shift = 2; String decryptedMessage = "";
		
		for (int i = 0; i <= data.length() - 1; i++) {
			String value = String.valueOf(data.charAt(i));
			if (count == 0) {
				count++; temp += value; 
			} else if (count == 1) {
				count = 0; temp += value; container.add(temp); temp = "";
			}
		}
		
		for (int i = 0; i <= container.size() - 1; i++) {
			int decryptedIndex = ciphertext.indexOf(Integer.parseInt(container.get(i)));
			int reverseIndex = decryptedIndex - shift;
			
			if (reverseIndex < 0) {
				int resetIndex = plaintext.size() + reverseIndex;
				decryptedMessage += plaintext.get(resetIndex);
			} else {
				decryptedMessage += plaintext.get(reverseIndex);
			}
		}
		
		container.clear();
		return decryptedMessage;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}