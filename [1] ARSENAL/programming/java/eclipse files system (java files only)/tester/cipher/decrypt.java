// package cipher;
import java.util.regex.*;

public class decrypt extends CipherClass {
	public decrypt (String message, int shiftValue) {
		int counter = 0;
		String temp = "";
		
		boolean checker = Pattern.compile("[0-9]+$").matcher(message).matches();
		
		if (message.length() % 2 == 0 && checker) {
			for (int i = 0; i <= message.length() - 1; i++) { // loop the message
				if (counter < 1) {
					temp += message.charAt(i);
					counter++;
				} else if (counter == 1) { // if 2 digits, then set to the container arraylist
					temp += message.charAt(i);
					container.add(Integer.parseInt(temp));
					temp = "";
					counter = 0;
				}	
			}
			
			System.out.println("extracted: " + container);
			shift(shiftValue * (-1)); // -3 shift (negative 3 means shift to right - 3
		} else {
			System.out.println("Cipher code must be pair and even to perform test");
		}
	}

	public static void shift (int shiftValue) {
		for (int i = 0; i <= container.size() - 1; i++) {
			container.set(i, indexValidator(container.get(i), shiftValue));
		}
		
		System.out.println("shifted: " + container);
		decipher();
	}
	
	public static int indexValidator (int element, int shiftValue) {
		int elementIndex = cipherText.indexOf(element) + (shiftValue);
		if (elementIndex > cipherText.size()) {
			return elementIndex - cipherText.size();
		} else if (elementIndex < (cipherText.size()) - (cipherText.size())) {
			return cipherText.size() + (elementIndex);
		} else {
			return elementIndex;
		}
	}
	
	public static void decipher () {
		String text = "";
		
		for (int i = 0; i <= container.size() - 1; i++) {
			text += plainText.get(container.get(i));
		}
		
		System.out.println("deciphered text: " + text);
	}
}
