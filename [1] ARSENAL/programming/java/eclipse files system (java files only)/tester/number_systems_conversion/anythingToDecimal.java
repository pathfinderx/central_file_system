package number_systems_conversion;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class anythingToDecimal {
	public static Scanner sc = new Scanner (System.in);
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		boolean breaker = false;
		
		while(!breaker) {
			sc = new Scanner (System.in);
			printChoices();
			
			if (sc.hasNextInt()) {
				switch(sc.nextInt()) {
				case 1:
					binaryToDecimal();
					break;
				case 2:
					octalToDecimal();
					break;
				case 3:
					hexToDecimal();
					break;
				case 0:
					System.out.println("Bye!");
					breaker = true;
					break;
				default:
					System.out.println("Invalid input format");
				}
			} else {
				System.out.println("Invalid input");
			}
		}
	}
	
	public static void printChoices () {
		System.out.println("Enter choices: ");
		System.out.println("[1] binaryToDecimal");
		System.out.println("[2] OctalToDecimal");
		System.out.println("[3] HexadecimalToDecimal");
	}
	
	public static void binaryToDecimal () {
		boolean breaker = false;
		
		while (!breaker) {
			try {
				sc = new Scanner (System.in);
				System.out.println("Enter binary to decimal: ");
				String input = sc.nextLine();
				boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
				
				if (isValid) {
					int decimal = Integer.parseInt(input, 2);
					System.out.println("binary converted to decimal: " + decimal);
				} else {
					System.out.println("Invalid input format");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void octalToDecimal () {
		boolean breaker = false;
		
		while (!breaker) {
			try {
				sc = new Scanner (System.in);
				System.out.println("Enter octal to convert to decimal: ");
				String input = sc.nextLine();
				boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
				
				if (isValid) {
					int decimal = Integer.parseInt(input, 8);
					System.out.println("octal converted to decimal: " + decimal);
				} else {
					System.out.println("Invalid input format");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void hexToDecimal () {
		boolean breaker = false;
		while (!breaker) {
			try {
				sc = new Scanner (System.in);
				System.out.println("Enter hex to convert to decimal");
				String input = sc.nextLine();
				boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
				
				if (isValid) {
					int decimal = Integer.parseInt(input, 16);
					System.out.println("hexadecimal converted to decimal: " + decimal);
				} else {
					System.out.println("Invalid input format");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}