package number_systems_conversion;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class decimalToAnything {
	public static Scanner sc = new Scanner (System.in);
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		boolean breaker = false;
		
		while(!breaker) {
			sc = new Scanner (System.in);
			printChoices();
			
			if (sc.hasNextInt()) {
				switch(sc.nextInt()) {
				case 1:
					decimalToBinary();
					break;
				case 2:
					decimalToOctal();
					break;
				case 3:
					decimalToHex();
					break;
				case 0:
					System.out.println("Bye!");
					breaker = true;
					break;
				default:
					System.out.println("Invalid input format");
				}
			} else {
				System.out.println("Invalid input");
			}
		}
	}
	
	public static void printChoices () {
		System.out.println("Enter choices: ");
		System.out.println("[1] decimalToBinary");
		System.out.println("[2] decimalToOctal");
		System.out.println("[3] deciamlToHexadecimal");
	}
	
	public static void decimalToBinary () {
		boolean breaker = false;
		while (!breaker) {
			sc = new Scanner (System.in);
			System.out.println("Enter decimal to convert to binary");
			String input = sc.nextLine();
			boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
			
			if (isValid) {
				String binary = Integer.toBinaryString(Integer.parseInt(input));
				System.out.println("decimal to binary: " + binary);
			} else {
				System.out.println("Invalid input format");
			}
		}
	}
	
	public static void decimalToOctal () {
		boolean breaker = false;
		while (!breaker) {
			try {
				sc = new Scanner (System.in);
				System.out.println("Enter decimal to octal");
				String input = sc.nextLine();
				boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
				
				if (isValid) {
					String octal = Integer.toOctalString(Integer.parseInt(input));
					System.out.println("decimal converted to octal: " + octal);
				} else {
					System.out.println("Invalid input format");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void decimalToHex () {
		boolean breaker = false;
		while (!breaker) {
			try {
				sc = new Scanner (System.in);
				System.out.println("enter decimal to convert to hexadecimal");
				String input = sc.nextLine();
				boolean isValid = Pattern.matches("\\A\\p{ASCII}*\\z", input);
				
				if (isValid) {
					String hex = Integer.toHexString(Integer.parseInt(input));
					System.out.println("decimal to hexadecimal: " + hex);
				} else {
					System.out.println("enter decimal to convert to hexadecimal");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}