package isbn10;
import java.io.*;
import java.util.*;

public class isbn10 {
	public static void main (String args []) {
		execute ("C:\\Users\\kurt kevin nebril\\Desktop\\simpleRead.txt", "C:\\Users\\kurt kevin nebril\\Desktop\\simpleWrite.txt");
	}
	
	public static void execute (String readLocation, String writeLocation) {
		try {
			int validCount = 0;
			int invalidCount = 0;
			FileReader simpleRead = new FileReader (readLocation);
			Scanner scan = new Scanner (simpleRead);
			
			File simpleWrite = new File (writeLocation);
			FileWriter fileWrite = new FileWriter(simpleWrite);
			PrintWriter print = new PrintWriter (fileWrite);
			
			while (scan.hasNextLine()) {
				String data = scan.nextLine().replaceAll("[^0-9Xx]", "");
				if (validate(data)) {
					print.println(String.format("%s is a valid ISBN number", data));
					validCount++;
				} else {
					print.println(String.format("%s is an invalid ISBN number", data));
					invalidCount++;
				}
			}
			
			print.println(String.format("\nTotal number of ISBN numbers tested: %s", validCount + invalidCount));
			print.println(String.format("Total number of valid ISBN numbers: %s", validCount));
			print.println(String.format("Total number of invalid ISBN numbers: %s", invalidCount));
			
			print.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean validate (String data) {
		int temp = 0;

		for (int i = 0; i <= data.length() - 1; i++) {
			if (data.charAt(i) == 'X') {
				temp += 10;
			} else {
				temp += (data.charAt(i) - '0') * (10 - i);
			}
		}
		
		if (temp % 11 == 0) {
			return true;
		} else {
			return false;
		}
	}
}
