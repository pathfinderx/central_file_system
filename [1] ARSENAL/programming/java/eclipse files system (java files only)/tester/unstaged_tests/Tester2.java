package unstaged_tests;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Tester2 {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\kurt kevin nebril\\Desktop\\dict attempt2\\krissy\\krissyCodes.txt");
			Scanner sc = new Scanner (fileread);
			
			while (sc.hasNextLine()) {
				String z = sc.nextLine();
				String s = Integer.toOctalString(Integer.parseInt(z));
				String c = sort(s);
				
				while (!s.equalsIgnoreCase(c)) {
					z = s;
					s = Integer.toOctalString(Integer.parseInt(z));
					c = sort(s);
				}
				System.out.println(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String sort (String data) {
		ArrayList <Integer> cont = new ArrayList <Integer> ();
		String result = "";
		
		for (int i = 0; i <= data.length() - 1; i++) {
			int value = Integer.parseInt(String.valueOf(data.charAt(i)));
			cont.add(value);
		}
		
		Collections.sort(cont);
		
		for (int i = 0; i <= cont.size() - 1; i++) {
			result += String.valueOf(cont.get(i));
		}
		
		return result;
	}
}
