package unstaged_tests;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class TesterMain {
	public static ArrayList <String> plaintext = new ArrayList <String> (Arrays.asList(
			"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"	
	));
	
	public static ArrayList <Integer> ciphertext = new ArrayList <Integer> (Arrays.asList(
			22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47	
	));
	
	public static ArrayList <String> container = new ArrayList <String> ();
	
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\kurt kevin nebril\\Desktop\\dict attempt2\\cipher\\encryptedMessage.txt");
			Scanner sc = new Scanner (fileread);
			
			while (sc.hasNextLine()) {
				String data = sc.nextLine();
				if (validate (data)) {
					decrypt(data);
				} else {
					System.out.println("Invalid data, cannot decrypt");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean validate (String data) {
		return (Pattern.matches("[0-9]+", data) ? true : false);
	}
	
	public static void decrypt (String data) {
		int count = 0; String temp = "";
		int shift = 2; String result = "";
		
		for (int i = 0; i <= data.length() - 1; i++) {
			String value = String.valueOf(data.charAt(i));
			if (count == 0) {
				count ++; temp += value; 
			} else if (count == 1) {
				count = 0; temp += value; container.add(temp); temp = "";
			}
		}
		
		for (int i = 0; i <= container.size() - 1; i++) {
			int unshiftedIndex = ciphertext.indexOf(Integer.parseInt(container.get(i)));
			int shiftedIndex = unshiftedIndex - shift;
			
			if (shiftedIndex < 0) {
				int resetIndex = plaintext.size() + shiftedIndex;
				result += plaintext.get(resetIndex);
			} else {
				result += plaintext.get(shiftedIndex);
			}
		}
		
		container.clear();
		System.out.println(result);
	}
}