package other_manipulation;

public class ArrayManipulation {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		char [] arr = {'a', 'b', 'c'};
		for (char c : arr) {
			System.out.println(c);
		}
		
	}
}