package datatype_conversion;

import java.util.*;
import java.util.regex.*;

public class String_Anything_Conversion {
	public static Scanner sc = new Scanner (System.in);
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			printChoices();
			if (sc.hasNextInt()) {
				switch(sc.nextInt()) {
				case 1:
					toChar();
					break;
				case 2:
					toInt();
					break;
				case 3:
					toDouble();
					break;
				case 0:
					System.out.println("bye!");
					breaker = true;
					break;
				default:
					System.out.println("Invalid input format");
					break;
				}
			} else {
				System.out.println("Invalid input format");
				sc.next();
			}
		} while (!breaker);
	}
	
	public static void printChoices () {
		System.out.println("Enter choice: ");
		System.out.println("[1] toChar");
		System.out.println("[2] toInt");
		System.out.println("[3] toDouble");
		System.out.println("[0] to exit");
	}
	
	public static void toChar () {
		ArrayList <Character> cont = new ArrayList <Character> ();
		sc = new Scanner (System.in);
		boolean checker = false;
		do {
			System.out.println("\nEnter [string] to convert to character");
			String input = sc.nextLine();
			boolean isValid = Pattern.matches("[a-zA-Z0-9]+", input);
			if (isValid && !input.isEmpty()) {
				for (int i = 0; i <= input.length() - 1; i++) {
					cont.add(input.charAt(i));
				}
				System.out.println("String converted to: " + cont + " as character in arraylist" + "\n");
				checker = true;
			} else {
				System.out.println("Invalid input format");
			}
		} while (!checker);
	}
	
	public static void toInt () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			System.out.println("\nEnter string to convert to integers: ");
			String input = sc.nextLine();
			boolean isValid = Pattern.matches("[-]{0,1}[0-9]+", input);
			boolean isFloat = Pattern.matches("[-]{0,1}[0-9]{1,9}\\.[0-9]{1,9}", input);
			
			if (isValid && !input.isEmpty()) {
				System.out.println("string converted to integer: "+Integer.parseInt(input)+"\n");
				breaker = true;
			} else if (isFloat) {
				System.out.println("string converted to interger: "+Math.round(Double.parseDouble(input)));
				breaker = true;
			} else {
				System.out.println("Invalid input format");
			}
			
		} while (!breaker);
	}
	
	public static void toDouble () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			System.out.println("\nEnter string to convert to double: ");
			String input = sc.nextLine();
			boolean isValid = Pattern.matches("[-]{0,9}[0-9]+||[-]{0,1}[0-9]{1,9}\\.[0-9]{1,9}", input);
			
			if (isValid && !input.isEmpty()) {
				System.out.println("string converted to double: "+Double.parseDouble(input));
				breaker = true;
			} else {
				System.out.println("Invalid input format");
			}
			
		} while (!breaker);
	}
}
