package datatype_conversion;

import java.util.*;
import java.util.regex.*;

public class Int_Anything_Conversion {
	public static Scanner sc = new Scanner (System.in);
	
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			printChoices();
			if (sc.hasNextInt()) {
				switch(sc.nextInt()) {
				case 1:
					toStr();
					break;
				case 2:
					toDouble();
					break;
				case 0:
					System.out.println("Bye!");
					breaker = true;
					break;
				default:
					System.out.println("Invalid input format\n");
					break;
				}
			} else {
				System.out.println("Invalid input format\n");
			}
		} while (!breaker);
	}
	
	public static void printChoices () {
		System.out.println("Enter choice: ");
		System.out.println("[1] toStr");
		System.out.println("[2] toDouble");
		System.out.println("[0] to exit");
	}
	
	public static void toStr () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			System.out.println("\nEnter int to convert to str");
			if (sc.hasNextInt()) {
				System.out.println("int converted to string: "+Integer.toString(sc.nextInt()) + "\n");
				breaker = true;
			} else {
				System.out.println("Invalid input format");
			}
		} while (!breaker);
	}
	
	public static void toDouble () {
		boolean breaker = false;
		do {
			sc = new Scanner(System.in);
			System.out.println("\nEnter int to convert to double");
			
			if (sc.hasNextInt()) {
				System.out.println("int converted to double: " + Double.parseDouble(Integer.toString(sc.nextInt())) + "\n");
				breaker = true;
			} else {
				System.out.println("Invalid input format");
			}
		} while (!breaker);
	}
}
