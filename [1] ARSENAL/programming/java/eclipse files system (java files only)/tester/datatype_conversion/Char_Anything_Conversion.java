package datatype_conversion;

import java.util.*;
import java.util.regex.*;

public class Char_Anything_Conversion {
	public static Scanner sc = new Scanner (System.in);
	
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		boolean breaker = false;
		
		do {
			sc = new Scanner (System.in);
			printChoices();
			if (sc.hasNextInt()) {
				switch(sc.nextInt()) {
				case 1: 
					toStr();
					break;
				case 2:
					toInt();
					break;
				case 0:
					System.out.println("bye!");
					breaker = true;
					break;
				default:
					System.out.println("Invalid input format");
					break;
				}
			} else {
				System.out.println("Invalid input format");
			}
			
		} while (!breaker);
	}
	
	public static void printChoices () {
		System.out.println("Enter choice: ");
		System.out.println("[1] toStr");
		System.out.println("[2] toInt");
		System.out.println("[0] to exit");
	}
	
	public static void toStr () {
		boolean breaker = false;
		do {
			System.out.println("\nEnter char to convert to string");
			sc = new Scanner (System.in);
			char input = sc.next().charAt(0);
			
			if (input != ' ' || (int) input != 0) {
				System.out.println("not empty");
				System.out.println("char converted to string: " + String.valueOf(input));
			} else {
				System.out.println("empty");
			}
			
		} while (!breaker);
	}
	
	public static void toInt () {
		boolean breaker = false;
		do {
			sc = new Scanner (System.in);
			System.out.println("\nEnter char to convert to int");
			char input = sc.next().charAt(0);
			
			if (input != ' ') {
				System.out.println("char converted to int: " + Integer.parseInt(Integer.toString(input)));
			} else {
				System.out.println("Invalid input format or empty");
			}
		} while (!breaker);
	}
}
