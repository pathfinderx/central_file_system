package staged_tests;

public class PyramidClassAdvanced {
	public static void main (String args []) {
		letter();
//		leftArrow ();
//		rightArrow();	
//		triangleNumbers();
//		pascalsTriangle();
	}
	
	public static void letter () {
		int rows = 5;
		char letter = 'A';
		for (int i = 1; i <= rows; i++) {
			for (int j = 1; j <= i; j++) {
				int converted = (int) letter - 1 + i;
				System.out.print((char) converted + " ");
			}
			System.out.println();
		}
	}
	
	public static void leftArrow () {
		int rows = 5;
		 
		for (int i = rows; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print("  ");
			}
			
			for (int x = 1; x <= rows - i + 1; x++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		for (int i = 2; i <= rows; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("  ");
			}
			
			for (int x = 1; x <= rows - i + 1; x++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public static void rightArrow () {
		int rows = 5;
		
		for (int i = 1; i <= rows; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		for (int i = rows - 1; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public static void triangleNumbers () {
		int rows = 5;
		int star = 0;
		int count = 0;
		
		for (int i = 1; i <= rows; i++) {
			star = 0;
			count = 0;
			for (int spaces = 1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}
			
			while (star != 2 * i - 1) {
				if (star == 0 || star + 1 == 2 * i - 1) {
					System.out.print(i + " ");
				} else if (star <= (2 * i - 1) / 2) {
					count++;
					System.out.print(i + count + " ");
				} else {
					count--;
					System.out.print(i + count + " ");
				}
				star++;
			}
			
			System.out.println();
		}
	}

	public static void pascalsTriangle () {
		int rows = 5;
		int c = 0;
		
		for (int i = 0; i < rows; i++) {
			for (int spaces = 1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}
			
			for (int x = 0; x <= i; x++) {
				if (x == 0) {
					c = 1;
				} else {
					c = c * (i - x + 1) / x;
				} 
				System.out.printf("%4d", c);
			}
			System.out.println();
		}
	}
	public static void delay (int seconds) {
		try {
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}