package staged_tests;
import java.util.*;
import java.util.regex.*;

public class ConversionsWithValidation {
	public static Scanner sc = new Scanner (System.in);
	
	public static void main (String args []) {
		execute();
	}
	
	public static void execute () {
		do {
			sc = new Scanner (System.in);
			System.out.println("Enter choices to convert data type to the targeted data type");
			System.out.println("[1] int string");
			System.out.println("[2] string int");
			System.out.println("[3] long to string");
			System.out.println("[4] string to long");
			System.out.println("[5] float to string");
			System.out.println("[6] string to float");
			System.out.println("[7] double to string");
			System.out.println("[8] string to double");
			System.out.println("[9] char to int");
			System.out.println("[10] int to char");
			System.out.println("[11] string to char");
			System.out.println("[12] char to string");
				
			if (sc.hasNextInt()) {
				int input = sc.nextInt();
				switch (input) {
					case 1: {
						int_string();
						break;
					}
					case 2: {
						string_int();
						break;
					}
					case 3: {
						long_string();
						break;
					}
					case 4: {
						string_long();
						break;
					}
					case 5: {
						float_string();
						break;
					}
					case 6: {
						string_float();
						break;
					}
					case 7: {
						double_string();
						break;
					}
					case 8: {
						string_double();
						break;
					}
					case 9: {
						char_int();
						break;
					}
					case 10: {
						int_char();
						break;
					}
					case 11: {
						string_char();
						break;
					}
					default: {
						System.out.println("None of the choices is selected, please select from the given choices using integer enclosed in the bracket:");
						break;
					}
				}
			} else {
				System.out.println("Invalid input format");
				System.out.println("Please select from the choices provided");
				sc.next();
			}
		} while (1 != 2);
	}
	
	public static void int_string () {
		sc = new Scanner (System.in);
		System.out.println("Enter integer to convert to string");
		int input;
		
		while (!sc.hasNextInt()) {
			System.out.println("invalid input format, please input integer");
			System.out.println();
			sc.next();
		}
		
		input = sc.nextInt();
		String converted = Integer.toString(input);
		System.out.println("[integer] " + "successfully converted to " + "[string]: " + input);
		System.out.println();
	}

	public static void string_int () {
		sc = new Scanner (System.in);
		boolean isValid = false;
		
		do {
			System.out.println("Enter string [integer format] to convert to integer");
			String input = sc.nextLine();
			isValid = Pattern.compile("[0-9]{1,9}").matcher(input).matches();
			
			
			if (isValid) {
				int converted = Integer.parseInt(input);
				System.out.println("[string] " + "successfully converted to " + "[int]: " + input);
				System.out.println();
			} else {
				System.out.println("invalid format");
			}
		} while (!isValid);
	}

	public static void long_string () {
		sc = new Scanner (System.in);
		System.out.println("Enter long to convert to string");
		
		while (!sc.hasNextLong()) {
			System.out.println("Invalid input format, please input long to convert to string");
			sc.next();
		}
		
		long input = sc.nextLong();
		String converted = Long.toString(input);
		System.out.println("[string] successfully converted to long: " + converted);
		System.out.println();
	}
	
	public static void string_long () {
		sc = new Scanner(System.in);
		boolean isValid = false;
		
		do {
			System.out.println("Enter string [integer format] to convert to long");
			String input = sc.nextLine();
			isValid = Pattern.matches("[0-9]{1,18}", input);
			
			
			while(!isValid) {
				System.out.println("input format not valid");
			} 
			
			long converted = Long.parseLong(input);
			System.out.println("[string] successfully converted long: " + converted);
			System.out.println();
		} while (!isValid);
	}

	public static void float_string () {
		sc = new Scanner (System.in);
		float input;
		boolean checker = false;
		
		do {
			System.out.println("Enter float to convert to string");
			while (!sc.hasNextFloat()) {
				System.out.println("invalid input format");
				sc.next();
			}
			input = sc.nextFloat();
			checker = true;
		} while (!checker);
			
		String converted = Float.toString(input);
		System.out.println("[float] successfully converted to string: " + converted);
		System.out.println();
	}

	public static void string_float () {
		sc = new Scanner (System.in);
		String input;
		boolean isFloat;
		boolean isInteger;
		
		do {
			System.out.println("Enter string [decimal format] to convert to float");
			input = sc.nextLine();
			isFloat = Pattern.matches("\\d{1,6}\\.\\d{1,6}$", input);
			
			isInteger = Pattern.matches("[0-9]+", input);
		} while (!isInteger && !isFloat);
		
		float converted = Float.parseFloat(input);
		System.out.println("[string] successfully converted to float: " + converted);
		System.out.println();
	}

	public static void double_string () {
		sc = new Scanner (System.in);
		boolean checker = false;
		double input;
		
		do {
			System.out.println("Enter double to convert to string");
			while (!sc.hasNextDouble()) {
				System.out.println("Invalid input format, please input double [decimal format] to convert to string");
				sc.next();
			}
			input = sc.nextDouble();
			checker = true;
			String converted = Double.toString(input);
			
			System.out.println("[double] successfully converted to string: " + converted);
			System.out.println();
			
		} while (!checker);
		
	}

	public static void string_double () {
		sc = new Scanner (System.in);
		boolean isDouble = false;
		boolean isInteger = false;
		
		do {
			System.out.println("Enter string [decimal format] to convert to double");
			String input = sc.nextLine();
			isDouble = Pattern.matches("\\d{1,14}\\.\\d{1,14}$", input);
			isInteger = Pattern.matches("[0-9]{1,14}", input);
			
			if (!isDouble && !isInteger) {
				System.out.println("invalid input format, please input string [decimal format] to convert to double");
			} else {
				double converted = Double.parseDouble(input);
				System.out.println("[string] successfully converted to double: " + converted);
				System.out.println();
			}
		} while (!isDouble && !isInteger);
	}

	public static void char_int () {
		sc = new Scanner (System.in);
		System.out.println("Enter char to convert to int: ");
		char input = sc.next().charAt(0);
		String inputConvert = String.valueOf(input);
		boolean isInteger = Pattern.matches("[0-9]+", inputConvert);
		
		
		if (isInteger) {
			int integer = Character.getNumericValue(input);
			System.out.println("the character you input is integer");
			System.out.println("character integer value " + integer);
			System.out.println();
		} else {
			int ascii = input;
			System.out.println("the character you input is not integer");
			System.out.println("character ascii value: " + ascii);
			System.out.println();
		}
		
	}

	public static void int_char () {
		sc = new Scanner (System.in);
		System.out.println("Enter int to convert to character");
	}
	
	public static void string_char () {
		sc = new Scanner (System.in);
		System.out.println("input string to convert to char");
		String input = sc.nextLine();
		char converted = input.charAt(0);
		System.out.print(converted);
		System.out.println();
	}

	public static void char_string () {
		sc = new Scanner (System.in);
		System.out.println("input character to convert to string");
		char input = sc.next().charAt(0);
		String converted = Character.toString(input);
		System.out.println(converted);
	}
}