package file_handling;

import java.io.*;

public class GetFileInformation {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		File file = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\simpleWrite.txt");
		if (file.exists()) {
			System.out.println(file.getName());
			System.out.println(file.getAbsolutePath());
			System.out.println(file.canRead());
			System.out.println(file.canWrite());
			System.out.println(file.length());
		} else {
			System.out.println("File not found");
		}
	}
}