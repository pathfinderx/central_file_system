package file_handling;

import java.io.*;
import java.nio.*;

public class MoveAllFiles {
	
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
		File target = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\testFiles");
		
		try {
			if (source.exists()) {
				System.out.println(source.getAbsolutePath() + " exists \nTransfering files...");
				moveFiles(source, target);
			} else {
				System.out.println(source.getAbsolutePath() + " does not exist, cant move files from the source folder");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void moveFiles (File source, File target) {
		try {
			if (target.exists()) {
				String [] paths = source.list();
				for (String contents : paths) {
					File newSource = new File (source + "\\" + contents);
					
					if (newSource.renameTo(new File (target + "\\" + contents)) ) {
						System.out.println(newSource + " successfully transfered to: " + target.getAbsolutePath());
					} else {
						System.out.println("failed to move files");
					}
				}
				System.out.println("Done");
			} else {
				if (target.mkdir()) {
					System.out.println(target.getAbsolutePath() + " successfully created directory");
					System.out.println("nTransfering files...");
					moveFiles(source, target); // recursive
				} else {
					System.out.println("failed to make directory for target folder");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
