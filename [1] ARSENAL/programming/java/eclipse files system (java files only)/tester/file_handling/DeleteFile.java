package file_handling;

import java.io.*;

public class DeleteFile {
	public static void main (String args []) {
		deleteFile();
		deleteFolder();
	}
	
	public static void deleteFile () {
		File targetPath = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\delete_this\\newSimpleWrite.txt");
	
		try {
			if (targetPath.delete()) {
				System.out.println(targetPath.getName() + " successfully deleted");
			} else {
				System.out.println("Failed to delete file");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteFolder () {
		File targetPath = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\delete_this");
		
		try {
			if (targetPath.delete()) {
				System.out.println(targetPath.getName() + " successfully delete");
			} else {
				System.out.println("Failed to delete file");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
