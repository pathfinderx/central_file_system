package file_handling;

import java.io.*;

public class Mkdir {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		File location = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\newFolder");
		
		try {
			boolean newDirectory = location.mkdir();
			if (newDirectory) {
				System.out.println(location.getName() + " successfully created at: " + location.getAbsolutePath());
			} else {
				System.out.println("Failed to create directory");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
