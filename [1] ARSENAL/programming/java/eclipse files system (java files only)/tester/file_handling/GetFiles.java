package file_handling;

import java.io.*;

public class GetFiles {
	public static void main (String args []) {
		listOfPaths ();
		listOfFiles ();
	}
	
	public static void listOfPaths () {
		File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
		
		try {
			File [] paths = source.listFiles();
			for (File contents : paths) {
				System.out.println(contents);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void listOfFiles () {
		File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
		
		try {
			String [] paths = source.list();
			for (String contents : paths) {
				System.out.println(contents);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
