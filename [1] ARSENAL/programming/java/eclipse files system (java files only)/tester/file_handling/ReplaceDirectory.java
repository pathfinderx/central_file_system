package file_handling;

import java.io.*;

public class ReplaceDirectory {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		try {
			File file = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\testFile3");
			if (file.exists()) {
				System.out.println(file.getName() + " Directory already exists\nReplacing..");
				if (file.delete()) {
					System.out.println(file.getName() + " successfully deleted");
					execute();
				} else {
					System.out.println("Failed to delete file");
				}
			} else {
				if (file.mkdir()) {
					System.out.println(file.getName() + " successfully created directory at: " + file.getAbsolutePath());
				} else {
					System.out.println("Failed to make directory");		
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}