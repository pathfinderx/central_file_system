package file_handling;

import java.io.*;
import java.nio.file.*;
import java.nio.*;

public class MoveFile {
	public static void main (String args []) {
//		moveRename ();
		moveRename2 ();
	}
	
	public static void moveRename () { // NIO version
		String source = "C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\newSimpleWrite.txt";
		String target = "C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\readThis.txt";
		
		try {
			Path file = Files.move(Paths.get(source), Paths.get(target));
			if (file != null) {
				System.out.println("file successfully moved");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void moveRename2 () { // Io version
		try {
			File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\newSimpleWrite.txt");
			File target = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\newFolder\\newSimpleWrite.txt");
			if (source.exists()) {
				if (source.renameTo(target)) {
					System.out.println(source.getName() + " successfully moved to: " + target.getAbsolutePath());
				} else {
					System.out.println("Failed to Move file");
				}
			} else {
				System.out.println("File Does not Exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
