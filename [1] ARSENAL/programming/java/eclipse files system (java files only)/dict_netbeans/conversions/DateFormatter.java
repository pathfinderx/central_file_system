package conversions;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DateFormatter {
  public static void main (String args []) {
    format();
  }
  
  public static void format() {
    try {
      String date1 = "31/12/1988";
      Date date = new SimpleDateFormat("dd/MM/yyy").parse(date1);
      System.out.println(date);
   } catch (Exception e) {
     System.out.println(e);
   }
  }
}