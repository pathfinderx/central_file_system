package basic_programs;
import java.util.Scanner;

public class Factorial {
	public static Scanner scan = new Scanner (System.in);
	
	public static void main (String args []) {
		System.out.println("Enter a number to make factorial");
		int input = scan.nextInt();
		System.out.println(factorial(input));
	}
	
	public static int factorial (int num) {
		if (num > 0) {
			return num * factorial (num - 1);
		} else {
			return 1;
		}
	}
}