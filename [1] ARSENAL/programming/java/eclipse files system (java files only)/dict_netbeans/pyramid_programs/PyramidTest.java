package pyramid_programs;
import java.util.ArrayList;

public class PyramidTest {
  public static ArrayList <Integer> container = new ArrayList <Integer> ();
  public static ArrayList <Integer> container2 = new ArrayList <Integer> ();
  
  public static void test () {
    int rows = 6;
    int k = 0;
    int alternator = 1;
    
    for (int i = 1; i <= rows; i++) {
      k = 0;
      alternator = 0;
      container.clear();
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (k != 2 * i - 1) {
        int printLimit = 2 * i - 1;
        
        if (k == 0) {
          System.out.print(array_one_mechanism(1) + " ");
        } else if (k == printLimit - 1) {
          System.out.print(array_one_mechanism(1) + " ");
          container.clear();
        } else if (alternator % 2 == 0) {
          System.out.print("  ");
          alternator++;
        } else {
            System.out.print("* ");
//          array_one_mechanism(lastValue);
//          int lastValue = container2.get(container2.size() - 1);
          
//          if (container.size() >= 2) {
//            int pair = container.get(0) + container.get(1);
//            container2.add(pair);
//            container.clear();
//            System.out.print(pair + " ");
//          } else {
//            
//          }

          alternator++;
        }
        k++;
      }
      System.out.println();
    }
  }
  
  public static int array_one_mechanism (int value) {
    if (container.size() == 2) {
      int pair = container.get(0) + container.get(1);
      container2.add(pair);
      container.clear();
      System.out.print(container2.get(0));
      return pair;
    } else if (container.size() <= 1) { 
      container.add(value);
      array_one_mechanism(0); // to update the mechanism, notifying that it has now two data
      return value;
    }
    return value;
  }
    
}
