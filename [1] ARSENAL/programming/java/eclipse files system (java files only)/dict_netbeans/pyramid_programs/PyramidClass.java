package pyramid_programs;
import java.util.ArrayList;

public class PyramidClass {

  public static void main (String args []) {
    pascalsNumber();
  }
  
  public static void triangleAsterisk () {
    int rows = 5;
    
    for (int i = 1; i <= rows; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print("* ");
      }
      System.out.println();
    }
  }
  
  public static void triangeAlphabet () {
    int rows = 5;
    char letter = 'A';
    
    for (int i = 1; i <= rows; i++) {
      for (int j = 1; j <= i; j++) {
        int converted = letter + i - 1;
        System.out.print((char) converted + " ");
      }
      System.out.println();
    }
  }
  
  public static void invertedTriangleAsterisk () {
    int rows = 5;
    
    for (int i = rows; i >= 1; i--) {
      for (int j = 1; j <= i; j++) {
        System.out.print("* ");
      }
      System.out.println();
    }
  }

  public static void pyramidAsterisk () {
    int rows = 5;
    int star = 0;
    
    for (int i = 1; i <= rows; i++) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      
      System.out.println();
    }
  }

  public static void invertedPyramidAsterisk () {
    int rows = 5;
    int star = 0;
    
    for (int i = rows; i >= 1; i--) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      System.out.println();
    }
  }

  public static void hourGlassAsterisk () {
    int rows = 5;
    int star = 0;
    
    for (int i = rows; i >= 1; i--) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      System.out.println();
    }
    
    for (int i = 2; i <= rows; i++) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      System.out.println();
    }
  }

  public static void diamondAsterisk () {
    int rows = 5;
    int star = 0;
    
    for (int i = 1; i <= rows; i++) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++){
        System.out.print("  ");
      }
    
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      System.out.println();
    }
    
    for (int i = rows - 1; i >= 1; i--) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }      
      
      while (star != 2 * i - 1) {
        System.out.print("* ");
        star++;
      }
      System.out.println();
    }
  }

  public static void skipInsidespaces () {
    int rows = 3;
    int star = 0;
    
    for (int i = 1; i <= rows; i++) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print(" ");
      }
      
      while (star != 2 * i - 1) {
        if (star == 0 || star == (2 * i - 1) - 1) {
          System.out.print("*");
        } else {
          System.out.print(" ");
        }
        star++;
      }
     
      System.out.println();
    }
    
     for (int i = rows - 1; i >= 1; i--) {
      star = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print(" ");
      }
      
      while (star != 2 * i - 1) {
        if (star == 0 || star == (2 * i - 1) - 1) {
          System.out.print("*");
        } else {
          System.out.print(" ");
        }
        star++;
      }
      System.out.println();
    }
  }

  public static void leftArrow () {
    int rows = 5;
    int extenderRow = 6; 
    
    for (int i = rows; i >= 0; i--) {
      for (int j = 1; j <= i; j++) {
        System.out.print("  ");
      }
      
      for (int x = 1; x <= extenderRow - i; x++) {
        System.out.print("* ");
      }
      
      System.out.println();
    }
    
    for (int i = 1; i <= rows; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print("  ");
      }
      
      for (int x = 1; x <= extenderRow - i; x++) {
        System.out.print("* ");
      }
      
      System.out.println();
    }
  }

  public static void rightArrow () {
    int rows = 5;
    
    for (int i = 1; i <= rows + 1; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print("* ");
      }
      
      System.out.println();
    }
    
    for (int i = rows; i >= 1; i--) {
      for (int j = 1; j <= i; j++) {
        System.out.print("* ");
      }
      
      System.out.println();
    }
  }

  public static void pyramidNumbers () {
    int rows = 5;
    int star = 0;
    int counter = 0;
    
    for (int i = 1; i<= rows; i++) {
      star = 0;
      counter = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (star != 2 * i - 1) {
        if (star == 0) {
          System.out.print(i + " ");
        } else if (star == ((2 * i - 1) - 1) / 2) {
          counter++;
          System.out.print(i + counter + " ");
        } else if (star > ((2 * i - 1) - 1) / 2) {
          counter--;
          System.out.print(i + counter + " ");
        } else if (star == (2 * i - 1) - 1) {
           System.out.print(i + " ");
        } else {
          counter++;
          System.out.print(i + counter + " ");
        }
        star++;
      }
      System.out.println();
    }
  } 

  public static void pascalsNumber () {
     int rows = 10, coef = 1;

    for(int i = 0; i < rows; i++) {
      for(int space = 1; space < rows - i; ++space) {
        System.out.print("  ");
      }

      for(int j = 0; j <= i; j++) {
        if (j == 0 || i == 0)
          coef = 1;
        else
          coef = coef * (i - j + 1) / j;

        System.out.printf("%4d", coef);
      }

      System.out.println();
    }
  }

  public static void test () {
    int rows = 5, k = 0, counter = 0;
    
    for (int i = 1; i <= rows; i++) {
      k = 0;
      counter = 0;
      for (int spaces = 1; spaces <= rows - i; spaces++) {
        System.out.print("  ");
      }
      
      while (k != 2 * i - 1) {
        if (k <= (2 * i - 1) / 2) {
          System.out.print((i + k) + " ");
        } else {
          counter++;
          System.out.print((k - counter) + " ");
        }
        k++;
      }
      System.out.println();
    }
  }
}
