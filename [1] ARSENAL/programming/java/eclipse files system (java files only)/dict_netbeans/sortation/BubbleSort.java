package sortation;
import java.util.*;

public class BubbleSort {
   public static ArrayList<Integer> list = new ArrayList<Integer>(
           Arrays.asList(2,1,4,3,5));
   public static boolean checker = false;
  
  public static void main (String args []) {
    bubbleSort();
   }
  
  public static void bubbleSort () {
    int low = 0;
    int high = 0;
    int length = list.size() - 1;
    
    System.out.println("original: " + list);
    
    while (!checker) {
      checker = true;
      for (int i  = 0; i <= length; i++) {
        if (i < length) { // length check
          if (list.get(i) < list.get(i+1)) {
            low = list.get(i);
            high = list.get(i + 1);
            list.set(i, high);
            list.set(i + 1, low);
            low = 0;
            high = 0;
            checker = false;
          }
        }
      }
    }
    
    System.out.println("new: " + list);
  }
}
