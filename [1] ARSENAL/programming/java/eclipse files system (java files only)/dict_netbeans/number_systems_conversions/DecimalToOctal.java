package number_systems_conversions;

public class DecimalToOctal {
  public static void main (String args []) {
    decimalToOctal(348);
  }
  
  public static void decimalToOctal (int input) {
    double quotient = 0.0;
    int remainder = 0;
    String octal = "";
    
    while (input > 0) {
      quotient = input / 8;
      remainder = input % 8;
      octal += remainder;
      input = (int) quotient;
      
      System.out.println(input + " / " + 8);
      System.out.println("quotient: " + quotient);
      System.out.println("remainder: " + remainder);
      System.out.println();
    }
    
    for (int i = octal.length() - 1; i >= 0; i--) {
      System.out.print(octal.charAt(i));
    }
  }
}