package number_systems_conversions;

public class DecimalToBinary {
  public static void main (String args []) {
    convertToBinary();
  }
  
  public static void convertToBinary () {
    int input = 348;
    double quotient = 0.0;
    int remainder = 0;
    String binary = "";
    
    while (input > 0) {
      quotient = input / 2;
      remainder = input % 2;
      input = (int) quotient;
      binary += Integer.toString(remainder);
      
      System.out.println(input + " / " + 2);
      System.out.println("quotient: " + quotient);
      System.out.println("remainder: " + remainder);
      System.out.println();
    }
    
    System.out.println("Decimal converted to binary: ");
    for (int i = binary.length() - 1; i >= 0; i--) {
      System.out.print(binary.charAt(i));
    }
  }
}