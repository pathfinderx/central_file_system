package number_systems_conversions;
import java.util.Scanner;
import java.util.regex.*;

public class BinaryToDecimal {
  public static Scanner sc = new Scanner (System.in);
  public static void main (String args []) {
    do {  
      sc = new Scanner(System.in);
      System.out.println("Enter binary to convert to decimal");
      while (!sc.hasNextInt()){
        System.out.println("invalid input format");
        sc.next();
      }
      int input = sc.nextInt();
      if (isInputValid(input)) {
        convert(input);
      } else {
        System.out.println("please input 0 or 1 [binary] to convert to decimal");
      }
    } while (1 != 2);
  }
  
  public static void convert (int input) {
    int twin = input;
    int count = 0;
    int decimal = 0;
    
    while (twin != 0) {
      int lastDigit = twin % 10;
      decimal += getPower(lastDigit, count);
      System.out.println(getPower(lastDigit, count));
      twin = twin / 10;
      count++;
    }
    System.out.println(decimal);
  }
    
  public static int getPower (int lastDigit, int count) {
    int value = 0;
    if (lastDigit == 0) {
    } else {
      value = (int) Math.pow(2, count);
    }
    return value;
  }
  
  public static boolean isInputValid (int input) {
    String converted = String.valueOf(input);
    boolean isValid = Pattern.matches("[1|0]{1,}", converted);
    return isValid;
  }
}