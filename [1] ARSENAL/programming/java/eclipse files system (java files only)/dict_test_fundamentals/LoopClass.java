package dict_test_fundamentals;
import java.util.Scanner;

public class LoopClass {
	public static void whileLoop_method () {
		int counter = 0;
		while (counter < 10) {
			System.out.println("counter" + counter);
			counter++;
		}
	}
	
	public static void doWhileLoop_method () {
		int counter = 0;
		do {
			System.out.println("counter" + counter);
			counter++;
		}
		while (counter < 10);
	}
	
	public static void forLoop_method () {
		System.out.println("Enter a number:");
		Scanner sc = new Scanner (System.in);
		int publicResult = 0;
		
		int input = sc.nextInt();
		for (int i = 1; i <= 10; i++) {
			int result = input * i;
			System.out.println(input + " x " + i + " = " + result);
			
		}
	}
	
	public static void forLoopBreakContinue_method () {
		for (int i = 0; i < 10; i ++) {
			if (i == 4) {
				System.out.println("reached the increment: " + i);
				break;
			}
			System.out.println("the increment: " + i);
		}
	}
}
