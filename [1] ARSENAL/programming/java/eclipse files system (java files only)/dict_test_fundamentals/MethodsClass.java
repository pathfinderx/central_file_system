package dict_test_fundamentals;
import java.util.Scanner;
import java.util.ArrayList;

public class MethodsClass {
	public static void methodParameterInitiator () {
		Scanner sc = new Scanner (System.in);
		System.out.println("what is your full name?");
		String fullname = sc.nextLine(); 
		methodParameters(fullname);
	}
	
	public static void methodOverloadingInitiator () {
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter First name");
		String fname = sc.nextLine();
		System.out.println("Enter Middle name");
		String mname = sc.nextLine();
		System.out.println("Enter Last name");
		String lname = sc.nextLine();

		fname = fname.toUpperCase();
		System.out.println(fname);
	}
	
	public static void methodParameters (String name) {
		System.out.println("Your name is: " + name);
	}
	
	public static void methodOverloading (String fname, String mname, String lname) {
		System.out.println("Your name is: " + fname + " " + mname + " " + lname);
	}
	
	public static void methodOverloadingToUpperCase(String fname, String mname, String lname) {
		System.out.println("Upper cased name: " + fname + " " + mname + " " + lname );
	}
}
