package dict_test_fundamentals;

import java.util.ArrayList;
import java.util.Iterator;

public class RecursionClass {
	public static ArrayList<Integer> array_container;
	public static void recursionInitiator () {
		int recursion1 = recursion(10);
		System.out.println(recursion1);
//		int recursion2 = recursionHaltingOperation(5,10);
//		System.out.println(recursion2);
//		testInitiator(5,10);
	}
	
	public static int recursion (int end) {
		 if (end > 0) {
			return end + recursion(end - 1);
		 }
		 else {
			 return 0;
		 }
	}
	
	public static int recursionHaltingOperation (int start, int end) {
		if (end > start) {
			return end + recursionHaltingOperation(start, end - 1);
		}
		else {
			return end;
		}
		
	}
	
// recursion replica but not actually recursion :D
	public static void testInitiator (int start, int end) {
		array_container = new ArrayList<Integer>();
		int sum = 0;
		
		for (int i = start; i <= end; i++) {
			array_container.add(i);
		}
		
		for (int y = 0; y < array_container.size(); y++) {
			sum = sum + array_container.get(y);
		}
		
		System.out.println(sum);
		System.out.println(array_container);
	}
}
