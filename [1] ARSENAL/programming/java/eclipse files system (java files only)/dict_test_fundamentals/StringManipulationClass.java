package dict_test_fundamentals;

public class StringManipulationClass {
	public static void indexOf_method () {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		System.out.println("indexOf(): " + str.indexOf("x"));
	}
	
	public static void length_method () {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWYXZ";
		System.out.println("length(): " + str.length());
	}
	
	public static void toUpperCase_method () {
		String str = "abcefghijklmnopqrstuvwyz";
		System.out.println("toUpperCase: " + str.toUpperCase());
	}
	
	public static void toLowerCase_method () {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		System.out.println("toLowerCase(): " + str.toLowerCase());
	}
}
