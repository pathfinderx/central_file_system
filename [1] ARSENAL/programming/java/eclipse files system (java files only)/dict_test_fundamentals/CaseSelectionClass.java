package dict_test_fundamentals;
import java.util.Scanner;

public class CaseSelectionClass {
	public static void switchCase_method () {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Key to Display");
		
		int key = sc.nextInt();
		switch (key) {
			case(1): {
				System.out.println(key);
				break;
			}
			case (2): {
				System.out.println(key);
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + key);
		}
	}
}
