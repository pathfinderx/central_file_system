package dict_test_fundamentals;

public class PyramidClass {
	public static int rows = 5;
	public static void invertedTriangleNumbers () {
		int counter = 0;
		for (int i = rows; i >= 1; --i) {
			counter++;
			for (int j = 1; j <= i; ++j) {
				System.out.print(counter + " ");
			}
		System.out.println();
		}
	}
	
	public static void invertedTriangleAsterisk () {
		for (int i = rows; i >= 1; --i) {
			for (int j = 1; j <= i; ++j) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	
	public static void triangleAsterisk () {
		for (int i = 1; i <= rows; ++i) {
			for (int j = 1; j <= i; ++j) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	
	public static void invertedNumbersReverseCount () {
		for (int i = rows; i >= 1; --i) {
			for (int j = i; j >= 1; --j) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
	
	public static void alphabetTriangle () {
		char start = 'A';
		char end = 'E';
		
//		System.out.println((char)start);
		for (int i = 1; i <= 5; ++i) {
			for (int j = 1; j <= i; ++j) {
				int converted = (int)start + i - 1;
				System.out.print((char)converted);
			}
			System.out.println();
		}
	}
	
	public static void alphabetReverseTriangle () {
		char start = 'A';
		int count = 0;
		
		for (int i = 5; i >= 1; --i) {
			count++;
			for (int j = 1; j <= i; ++j) {
				int converted = start + count - 1;
				System.out.print((char)converted + " ");
			}
			System.out.println();
		}
	}
	
	public static void pyramidPrototype1 () {
		int rows = 9; 
		final long startTime = System.currentTimeMillis();
		
		for (int i = 1; i <= rows;  ++i) {
			for (int spaceLeft = 1; spaceLeft <= rows-i; ++spaceLeft) {
				delayExecution(500);
				System.out.print("* ");
			}
			if (i%2==0) {
				for (int x = 1; x <= i; x++) {
					delayExecution(500);
					System.out.print(x + " ");
				}
			}
			else {
				for (int x = 1; x <= i; x++) {
					delayExecution(500);
					System.out.print(x + " ");
				}
			}
			if (i%2==0) {
				for (int x = 2; x <= i; x++) {
					delayExecution(500);
					System.out.print(x + " ");
				}
			}
			else {
				for (int x = 2; x <= i; x++) {
					delayExecution(500);
					System.out.print(x + " ");
				}
			}
			for (int spaceRight = 1; spaceRight <= rows-i; ++spaceRight) {
				delayExecution(500);
				System.out.print("* ");
			}
			
			System.out.println();
		}
		final long endTime = System.currentTimeMillis();
		System.out.println("Total execution time: " + (endTime - startTime));
	}
	
	public static void pyramidPrototype2 () {
		int rows = 5, k = 0;

	    for (int i = 1; i <= rows; ++i, k = 0) {
	      for (int space = 1; space <= rows - i; ++space) {
	    	delayExecution(500);
	        System.out.print("1 ");
	      }

	      while (k != 2 * i - 1) {
	    	  delayExecution(500);
	        System.out.print("* ");
	        ++k;
	      }
	      System.out.println();
	    }
	}
	
	public static void pyramidPrototype3 () {
		int rows = 5;
		int k = 0;
		
		for (int i = 1; i <= rows; ++i, k= 0) {
			for (int space = 1; space <= (rows-i); ++space) {
				delayExecution(300);
				System.out.print("* ");
			}
			while (k != 2 * i - 1) {
				System.out.print(k+ " ");
				++k;
			}
			System.out.println();
		}
	}

	public static void pyramidRightLeftHemisphere_numbers () {
		int rows = 5;
		int num = 0; // temp number that resets every new row
		
		for (int i = 1; i <= rows; i++) {
			num = 0; // reset to new 0 as it gets to new row
			for (int space = 1; space <= rows - i; space++) { // the space
				System.out.print("  ");
			}
			
			while (num != 2 * i - 1) {
				int num_i = i + num; 
				int theStackLimit = 2 * i - 1;
				int rightPrintLimiter = theStackLimit / 2;
				int rightHemisphereIndicator = theStackLimit + 1;
				
				if (num_i <= theStackLimit) { // check iteration if inside stacklimit
					System.err.print(num_i + " "); // prints the left hemisphere
				} else if (num_i == rightHemisphereIndicator) { // check if outside the right hemisphere
					for (int x = 1; x <= rightPrintLimiter; x++) { // prints right hemisphere
						System.out.print(theStackLimit - x + " "); // reverse the iteration
					}
				}
				num++;
			}
			
			System.out.println();
		}
	}

	public static void leftArrow () {
		int rows = 5;
		int num = 0;
		
		for (int i = 1; i <= rows; i++) {
			num = 0;
			for (int space = 1; space <= rows - i; space++) {
				System.out.print("  ");
				num ++;
			}
			
			for (int y = 1; y <= rows - num; y++) {
				System.out.print("* ");
			}
			
			System.out.println();
		}
		
		for (int i = rows ; i >= 1; i--) {
			num = 0;
			if (i <= 4) {
				for (int space = 1; space <= rows - i; space++) {
					System.out.print("  ");
					num++;
				}
				
				for (int y = 1; y <= rows - num; y++) {
					System.out.print("* ");
				}
				System.out.println();
			}
		}
	}
	
	public static void rightArrow () {
		int rows = 5;
		
		for (int i = 1; i <= rows; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		
		for (int i = rows; i >= 1; i--) {
			if (i <= 4) {
				for (int j = 1; j <= i; j++) {
					System.out.print("* ");
				}
				System.out.println();
			}
		}
	}
	
	public static void skipInsideSpaces () {
		int rows = 3; 
		int num = 0;
		int num2 = 0;
		
		for (int i = 1; i <= rows; i++) {
			num = 0;
			for (int space = 1; space <= rows - i; space++) {
				System.out.print("  ");
			}
			
			while (num != 2 * i - 1) {
				int stackLimit = 2 * i - 1;
				
				if (num < 1) {
					System.out.print("* ");
				} else if ((num + 1) == stackLimit) {
					System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
				num++;
			}
			
			System.out.println();
		}
		
		for (int i = rows; i >= 1; i--) {
			num2 = 0;
			for (int space = 1; space <= rows - i; space++) {
				System.out.print("  ");
			}
			
			if (i < rows) {
				while (num2 != 2 * i - 1) {
					int stackLimit = 2 * i - 1;
					
					if (num2 < 1) {
						System.out.print("* ");
					} else if ((num2 + 1) == stackLimit) {
						System.out.print("* ");
					} else {
						System.out.print("  ");
					}
					num2++;
				}
				System.out.println();
			}
		}
	}
	
	public static void delayExecution (int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
