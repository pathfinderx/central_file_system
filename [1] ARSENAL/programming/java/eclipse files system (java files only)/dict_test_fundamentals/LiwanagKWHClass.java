package dict_test_fundamentals;

import java.util.Scanner;

public class LiwanagKWHClass {
	public static void kiloWattHourCalculation () {
		Scanner sc = new Scanner(System.in);
		boolean checker = false;
		final double generationCharge_40KWH = 5;
		final double generationCharge_50KWH = 20;
		final double distributionCharge_10KWH = 1;
		final double distributionCharge_40KWH = 2;
		final double distributionCharge_50KWH = 3;
		double bill = 0.0;
		
		while (checker != true) {
			System.out.println("Enter KWH value: ");
			int inputValue = sc.nextInt();
			
			if (inputValue >= 10 && inputValue <= 40) {
				bill = inputValue * distributionCharge_10KWH;
				if (inputValue == 40) {
					bill = inputValue * distributionCharge_10KWH + generationCharge_40KWH;
					System.out.println("Total bill: " + bill);
					System.out.println();
					inputValue = 0;
					bill = 0;
				} else {
					System.out.println("Total bill: " + bill);
					System.out.println();
					inputValue = 0;
					bill = 0;
				}
			}
			else if (inputValue > 40 && inputValue <= 50) {
				if (inputValue == 50) {
					bill = inputValue * distributionCharge_40KWH + generationCharge_50KWH;
					System.out.println("Total Bill: " + bill);
					System.out.println();
				} else {
					bill = inputValue * distributionCharge_40KWH + generationCharge_40KWH;
					System.out.println("Total bill: " + bill);
					System.out.println();
					inputValue = 0;
					bill = 0;
				}
			}
			else if (inputValue > 50) {
				bill = inputValue * distributionCharge_50KWH + generationCharge_50KWH;
				System.out.println("Total Bill: " + bill);
				System.out.println();
				inputValue = 0;
				bill = 0;
				
			}
			else if (inputValue < 10) {
				System.err.println("Goodbye");
				checker = true;
			}
			else {
				System.out.println("invalid");
			}
		}	
	}
}
