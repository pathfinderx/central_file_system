package dict_test_fundamentals;
import java.util.Scanner;
import java.util.*;

public class ArrayManipulationClass {
	public static ArrayList<String> cars = new ArrayList<String>();
	
	public static void arrayTest_method () {
		String[] cars = {"toyota", "honda", "nissan", "porche"};
		for (int i = 0; i < cars.length; i++) {
			System.out.println(cars[i]);
		}
	}
	
	public static void crudArray_method () {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Press [1] to push value to array");
		System.out.println("Press [2] to pop value to array");
		System.out.println("Press [3] to check the contents of array");
		int key = sc.nextInt();
		
		switch (key) {
			case (1): {
				pushArray_method();
				break;
			}
			case (2): {
				break;
			}
			case (3): {
				readArray_method();
				break;
			}
			default: {
				throw new IllegalArgumentException("Unexpected Value: " + key);
			}
		}
	}
	
	public static void pushArray_method () {
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter type of car: ");
		String input = sc.nextLine();
		
		cars.add(input);
		System.out.println("added item to the car list");
		crudArray_method();
	}
	
	public static void readArray_method () {
		if (cars.size() != 0) {
			System.out.println("Car List:");
			for (int i = 0; i < cars.size(); i++) {
				System.out.println(cars.get(i));
			}
			crudArray_method();
		} 
		else {
			System.err.println("No cars in the list yet");
			crudArray_method();
		}
		
	}
}
