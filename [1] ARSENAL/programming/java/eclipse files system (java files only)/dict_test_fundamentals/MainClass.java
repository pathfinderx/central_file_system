package dict_test_fundamentals;
import java.util.Scanner;

public class MainClass {
	public static void main (String args []) {
		LoopClass loop_class = new LoopClass();
		StringManipulationClass stringManipulation_class = new StringManipulationClass();
		CaseSelectionClass caseSelection_class = new CaseSelectionClass();
		ArrayManipulationClass arrayManipulation_class = new ArrayManipulationClass();
		MethodsClass method_class = new MethodsClass();
		RecursionClass recursion_class = new RecursionClass();
		FactorialClass factorial_class = new FactorialClass();
		PyramidClass pyramid_class = new PyramidClass();
		TestClass test_class = new TestClass();
		LiwanagKWHClass liwanagKHW_class = new LiwanagKWHClass();
		TestClass test_class2 = new TestClass();
		
//		stringManipulation_class.indexOf_method();
//		stringManipulation_class.length_method();
//		stringManipulation_class.toUpperCase_method();
//		stringManipulation_class.toLowerCase_method();
		
//		loop_class.whileLoop_method();
//		loop_class.doWhileLoop_method();
//		loop_class.forLoop_method();
//		loop_class.forLoopBreakContinue_method();
		
//		caseSelection_class.switchCase_method();
		
//		arrayManipulation_class.arrayTest_method();
//		arrayManipulation_class.crudArray_method();
		
//		method_class.methodParameterInitiator ();
//		method_class.methodOverloadingInitiator();
		
//		recursion_class.recursionInitiator();
//		factorial_class.factorialTest();
//		factorial_class.factorialLoop();
		
//		pyramid_class.invertedTriangleNumbers();
//		pyramid_class.invertedTriangleAsterisk();
//		pyramid_class.triangleAsterisk();
//		pyramid_class.invertedNumbersReverseCount();
//		pyramid_class.alphabetTriangle();
//		pyramid_class.alphabetReverseTriangle();
//		pyramid_class.pyramidPrototype1();
//		pyramid_class.pyramidPrototype2();
//		pyramid_class.pyramidPrototype3();
	
//		liwanagKHW_class.kiloWattHourCalculation();
		
//		test_class.test();
//		test_class.pyramidTest();0
	}
}
