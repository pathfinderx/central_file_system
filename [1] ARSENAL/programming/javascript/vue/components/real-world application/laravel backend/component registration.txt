* register in: 'resources/assets/js/app.js'
	- folder unstructured:
		- Vue.component('your-component', require('./components/YourComponent.vue'));

	- folder structured:
		- Vue.component('your-component', require('./components/your_folder/YourComponent.vue'));

* call in veiw layer:
	<html>
		<your-component></your-component>
	</html>

* configure your components in: 'resources/assets/js/components/app.js'
	* <template></template>:
		- the elements to display in view layer
		- can access data locally and globally
	* <script>
		export default {}
	</script>
		- data are locally registered here
		- can access global data in vue instance found in 'resources/assets/js/app.js'

ISSUES:
	* 'Uncaught (in promise) Error: Request failed with status code 404':
		- check web/route