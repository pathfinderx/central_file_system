# looping in *args
class Sophisticated_Args:
    def __init__(self):
        pass
    
    def function1 (self, *args):
        result = 0
        for x in args:
            print (x)
        print()
            
    def function2 (self, **kwargs):
        print(kwargs["fname"], kwargs["lname"], "\n")
        
    def function3 (self, a, b, *args, **kwargs):
        print (f"a is {a}")
        print (f"b is {b}")
        print (f"c is {args}")
        print(f"{kwargs['phrase']}", "\n")
        
    def unpack (self, *args):
        print (args)
            
object1 = Sophisticated_Args()
object1.function1(1,3,3,4,45,5)
object1.function2(fname = "Phoenix", lname = "The Cat")
object1.function3(1, 2, 3, 4, 5, phrase="Pheonix The Cat")
object1.unpack(*[1,2,3,4,5]) # unpack the tuple add the argument with '*'
