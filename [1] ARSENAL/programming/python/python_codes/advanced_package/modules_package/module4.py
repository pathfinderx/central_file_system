from module1 import name
from module1 import ModuleClass1

class ModuleClass4:
    def __init__(self):
        pass
        
    def getAttributes(self):
        print(name)
        
    def getClassAttributes(self):
        print(ModuleClass1().phrase)
        
module4 = ModuleClass4()
module4.getAttributes()
module4.getClassAttributes()