def square(x):
    return x**2

def cube(x):
    return x**3

def process (func, *args):
    result = []
    for x in args[0]:
        result.append(func(x))
    return result

def getFunc ():
    return cube

myList = [1,2,3,4,5]
getSquare = process(square, myList) 
getCube = process(cube, myList)
getFunc = getFunc()

print(getSquare)
print(getCube)
print(getFunc(x for x in myList))