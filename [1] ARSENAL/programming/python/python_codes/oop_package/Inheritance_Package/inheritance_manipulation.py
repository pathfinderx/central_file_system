# creating parent class
class ParentClass:
    def __init__(self):
        self.fname = "John"
        self.lname = "Smith"
        
    def printName (self):
        return self.fname, self.lname

# creating child class and accessing its attributes and properties 
class ChildClass(ParentClass):
    def __init__(self):
        super().__init__()

parentClass = ParentClass()
childClass = ChildClass()

print(childClass.fname)
print(childClass.lname)
print(childClass.printName(), "\n")

# overriding 
class ChildClass2 (ParentClass):
    def __init__(self, fname, lname):
        self.fname = fname
        self.lname = lname
        
    def printName(self):
        print(self.fname, self.lname, "\n")
        
childClass2 = ChildClass2("John", "Smith")
childClass2.printName()

# inherit and improvise or overloading the __init__()
class BaseClass:
    def __init__(self):
        pass

class DerivedClass (BaseClass):
    def __init__(self, age):
        BaseClass.__init__(self)
        self.age = age
        
derivedClass = DerivedClass(23)
print(derivedClass.age)













