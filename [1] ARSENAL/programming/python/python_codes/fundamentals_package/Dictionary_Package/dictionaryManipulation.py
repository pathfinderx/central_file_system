import operator
dict1 = {"a" : 4, "c" : 3,  "b" : 2, "d" : 26, "e" : 25}

# sortation ascending
# dict1_ascending = sorted(dict1.items(), key=lambda x:x[0])
# converted_asc_dict = dict(dict1_ascending)
# print(converted_asc_dict)

# sortation descending
# dict1_descending = sorted(dict1.items(), key = lambda x:x[1])
# converted_dsc_dict = dict(dict1_descending)
# print(converted_dsc_dict)

# concat dict
# dic1 = {1:10, 2:20}
# dic2 = {3:30, 4:40}
# dic3 = {5:50,6:60}
# dic4 = {}
#
# for x,y in (dic1.items(), dic2.items(), dic3.items()):
#     dic4.update({x:y})
# print(dic4)

# add two values then put to as new dict
# dict5 = {0: 10, 1: 20}
# sum = 0
#
# for x,y in dict5.items():
#     sum += y
#
# dict5.update({len(dict5): sum})
# print(dict5)

# Sample Dictionary ( n = 5) : Expected Output : {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
# dict6 = {}
# for x in range(1, 5+1):
#     dict6.update({x:x*x})
#
# print(dict6)
