class TestClass ():
    def __init__(self):
        pass
        
    def accessGlobalVariable(self):
        global x
        x = 1
        
class TestClass2 (TestClass):
    def __init__(self):
        TestClass.__init__()
        pass
    
    def accessGlobalVariable(self):
        print (x)
        
testClass = TestClass()
print(testClass.accessGlobalVariable())