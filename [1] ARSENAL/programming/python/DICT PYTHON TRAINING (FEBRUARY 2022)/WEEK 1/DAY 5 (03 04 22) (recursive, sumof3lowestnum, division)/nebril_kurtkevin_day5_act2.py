def sumOf3LowestNum (raw):
    sortedList = []
    while raw:
        minimum = raw[0]
        for item in raw:
            if item < minimum:
                minimum = item
        sortedList.append(minimum)
        raw.remove(minimum)

    if sortedList:
        sumDig = 0
        for x in range (0, 3):
            sumDig += sortedList[x]

        return sumDig
        
result = sumOf3LowestNum([10, 20, 30, 40, 50, 60, 7])
print ("The sum of 3 min numbers: ", result)