import sys

in1, in2 = int(input ("Enter first operand: ")), int(input ("Enter second operand: "))
def division(x, y):
    if x < y:
        return 0
    return 1 + division(x - y, y)

def divide(x, y):
    if y == 0:
        print('Cannot divide to zero')
        sys.exit()

    sign = 1
    if x * y < 0:
        sign = -1

    return sign * division(abs(x), abs(y))
    
result = divide(in1, in2)
print('Quotient: ', result)