noun1 = input("Enter noun 1: ")
noun2 = input("Enter noun 2: ")
noun3 = input("Enter noun 3: ")
adj1 = input("Enter adjective 1: ")
adj2 = input("Enter adjective 2: ")
adj3 = input("Enter adjective 3: ")

text1 = '''
ORIGINAL:
Happy Birthday to You
Happy Birthday to You
Happy Birthday Dear Kurt

May good luck go with you,
'''

text2 = f'''
MODIFIED TEXT:
Happy Birthday to {noun1.upper()}
Happy Birthday to {noun2.upper()}
Happy Birthday Dear {noun3.upper()}

May {adj1.upper()} {adj2.upper()} {adj3.upper()} luck go with you,
'''
print(text1 + '\n' + text2)