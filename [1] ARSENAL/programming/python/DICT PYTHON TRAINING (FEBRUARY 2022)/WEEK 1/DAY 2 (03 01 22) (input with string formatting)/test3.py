# round (number, digits)
# x = round(5.654654)
# print(x)


# pow
# returns the value of x to the power of y (xy)
# third argument is the modulo
# print(pow(4,3))
# print(pow(4 ** 3))
# x = pow (4,3)
# y = pow (4,3,5)

# math.ceil() - round a number upward to its nearest integer
# import math
# print(math.ceil(1.4)) # 2
# print(math.ceil(-5.3)) # -5

# math.factorial() - returns the factorial of a number\
# import math
# print(math.factorial(9))

# MATHEMATICAL CONSTANT
# print(math.pi)

# SAMPLE NUMBER FORMATTING
a = 3.123
print('using && : %.2f ' %a)
print('format() ; {:.2f}'.format(a))
print('using format(): ', format(a, '.2f'))