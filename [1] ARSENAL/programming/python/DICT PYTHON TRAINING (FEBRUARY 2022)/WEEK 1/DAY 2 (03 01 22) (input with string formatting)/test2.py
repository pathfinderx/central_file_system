# MULTIPLY THE PRINT
# print ("hello " * 2)

# SLICING
# my_list = ["a", 'b', 'c']
# print(my_list[1])
# print(my_list[:])

# ACCESSING STRING INDEX
# str = "PROGRAM"
# print(str[0:4])

# STRING IS IMMUTABLE
# a = 'hello'
# a[0] = 'w'

# string can be deleted
# a = "hello"
# del a

# ITERATING THROUGH STRING
# count = 0 
# for letter in "Hello World":
#     if (letter == 'l'):
#         count += 1
#         print (letter)
    # 'in' in for loop checks the membership

# MEMBERSHIP TEST
# a = 'hello'
# print('b' in a) # this returns boolean

# STRING PLACEHOLDERS 1 (direct)
# text1 = "My name is {}"
# text2 = text1.format('Kurt')

# STRING PLACEHOLDER 2 (index assginment)
# text1 = "My name is {0}, im {1} years old"
# text2 = text1.format('Kurt', 1)
# print(text2)

# STRING PLACE HOLDER 3 (variable)
# text1 = "My name is {name}, im {age} years old"
# text2 = text1.format(name="Kurt", age=22)
# print(text2)

# STRING PLACEHOLDRS (%)
# item = 'milk'
# cost = 35.58
# print("This product is: %s, costs: %.2f" % (item, cost))

# STRING PLACEHOLDERS (F STRING AND {})
# item = 'milk'
# cost = 35.58
# print(f"the product {item}, costs: {cost * 3}")

# STRING upper() FUNCTION
# text = "hello kerk"
# print(text.upper())

# STRING lower() FUNCTION
# text = "HELLO KERK"
# print(text.lower())

# STRING len() FUNCTION
# text = "kerk"

# STRING replace() FUNCTION 
# text = "banana"
# print(text.replace(text, 'apple'))

# STRINT title() function


# STRING split() FUNCTION
text = "HELLO, KERK RANA. KURT NEBRIL"
print(text.split(','))