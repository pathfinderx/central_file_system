emp_name = input ("Enter employee name: ")
hours_rendered = int (input("Enter hours rendered: "))
sss = int (input("Enter sss contribution: "))
phil_health = int (input("Enter Phil Health contribution: "))
housing_loan = int (input("Enter Housing Loan contribution: "))

RATE_PER_HOUR = 500
TAX = 500.0
gross_salary = float((RATE_PER_HOUR * hours_rendered))

total_deductions = sss + phil_health + housing_loan + TAX
net_salary = float(gross_salary - total_deductions)

print("====== PAYSLIP ======")
print("====== EMPLOYEE INFORMATION ======")
print(f'Employee Name: {emp_name}')
print(f'Rendered Hours: {hours_rendered}')
print(f'Rate per Hour: {RATE_PER_HOUR}')
print('Gross Salary: %.1f ' %gross_salary)
print("====== DEDUCTIONS ======")
print(f'SSS: {sss}')
print(f'PhilHealth: {phil_health}')
print(f'Other Loan: {housing_loan}')
print(f'Tax: {TAX}')
print(f'Total Deductions: {total_deductions}')
print('\nNet Salary:PHP %.1f ' %net_salary)

