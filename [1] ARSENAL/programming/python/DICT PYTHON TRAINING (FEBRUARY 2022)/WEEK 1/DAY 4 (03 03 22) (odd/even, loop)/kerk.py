halt = False
word_bank = list()
while not halt:
    word_input = input ("Enter a word: ")
    repeat = input("Type (Y/y) if yes. Type (N/n) if No: ")

    if repeat == "Y" or repeat == "Y".lower():
        word_bank.append(word_input)
        halt = False
    elif repeat == "N" or repeat == "N".lower():
        word_bank.append(word_input)
        print("Total words: ", len(word_bank))
        print("Word bank: ", word_bank)
        halt = True