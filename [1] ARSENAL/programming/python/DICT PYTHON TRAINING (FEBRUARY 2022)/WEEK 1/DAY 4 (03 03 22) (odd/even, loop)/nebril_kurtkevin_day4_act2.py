while True:
    n1, n2 = int (input ("Enter number 1: ")), int (input ("Enter number 2: "))
    print(f"The sum of n1 and n2 is: {n1 + n2}")
    
    n3 = input("Try again? [Y/y] if Yes and [N/n] if No: ")
    if n3 in ["Y","y"]:
        continue
    elif n3 in ["N","n"]:
        print("Thank you!")
        break