wordlist = []
while True:
    word = input ("Enter a word: ")
    breaker = input("Try again? [Y/y] if Yes and [N/n] if No: ")
    if breaker in ["Y","y"]:
        wordlist.append(word)
        continue
    elif breaker in ["N","n"]:
        wordlist.append(word)
        print("\nTotal number of words: ", len(wordlist), "\nWords in the list: ")
        for x in wordlist:
            print(x, end=', ')
        break
    else:
        print("\nWrong choice, please select from: [Y/y] if Yes and [N/n] if No. \nDiscarding your word and repeating the loop...\n")