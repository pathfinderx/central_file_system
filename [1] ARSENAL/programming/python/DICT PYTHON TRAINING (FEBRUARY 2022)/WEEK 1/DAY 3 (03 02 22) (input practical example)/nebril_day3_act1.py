office, years_service = input ("Enter office designation: "), int (input("Enter years in service: "))

IT, ACCT, HR = 'IT', 'ACCT', 'HR'

if office in [IT, IT.lower()]:
    print("Your bonus is: 5000") if years_service < 10 else print("Your bonus is: 10000")
elif office in [ACCT, ACCT.lower()]:
    print("Your bonus is: 6000") if years_service < 10 else print("Your bonus is: 12000")
elif office in [HR, HR.lower()]:
    print("Your bonus is: 7500") if years_service < 10 else print("Your bonus is: 15000")