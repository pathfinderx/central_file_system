name = "Kurt Kevin C. Nebril"
math_grade = 90.4
science_grade = 90.4
english_grade = 90
status = "Passed"

print(f'Name: {name}')
print(f'Math grade: {math_grade}')
print(f'Science grade: {science_grade}')
print(f'English: {english_grade}')
print(f'Status: {status}')