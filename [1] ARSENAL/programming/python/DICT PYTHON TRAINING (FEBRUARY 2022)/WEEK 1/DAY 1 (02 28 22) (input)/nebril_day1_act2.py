name = input("Enter your name: ")
math_grade = float (input("Enter your math grade: "))
science_grade = float (input("Enter your science grade: "))
english_grade = int (input("Enter your english grade: "))
average = (math_grade + science_grade + english_grade) / 3

print(f'\nName: {math_grade}')
print(f'Math grade: {math_grade}')
print(f'Science grade: {science_grade}')
print(f'English grade: {english_grade}')
print(f'Average: ' + '{0:.2f}'.format(average))