
print ("CALCULATOR APP")

choice = input ("SELECT FROM THE FOLLOWING: [1] Add [2] Subtract [3] Multiply [4] Divide")
n1 = float(input("Enter input 1: "))
n2 = float(input("Enter input 2: "))

def execute (choice, n1, n2):
    try:
        if choice in ['1']:
            return (n1 + n2)
        elif choice in ['2']:
            return (n1 - n2)
        elif choice in ['3']:
            return (n1 * n2)
        elif choice in ['4']:
            return (n1 / n2)
    except ValueError:
        print("Invalid Innput!")
    except ArithmeticError:
        print("Arithmetic Error! ")
    except ZeroDivisionError:
        print ("Error when executing division! ")
    except FloatingPointError:
        print ("Error with floating point")
    except Exception as e:
        print("Unknown Error: ", e)

print(f"The result between {n1} and {n2} is : {execute (choice, n1, n2)}")