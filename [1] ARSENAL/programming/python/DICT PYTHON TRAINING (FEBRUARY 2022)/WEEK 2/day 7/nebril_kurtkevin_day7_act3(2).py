class Person:
    def displayCredentials (self, name="Default Name", email="defaultemail@default.com", cnum="0-000-000-0000"):
        print(f'Person name: {name}')
        print(f'Person email: {email}')
        print(f'Person contact number: {cnum} \n')

person_class = Person()
person_class.displayCredentials("John Hammond", "johnhammond@hotmail.com", "1-654-125-4050")
person_class.displayCredentials("Kevin Mitnick", "kevinmitnick@nasa.gov.com", "1-125-785-9632")
person_class.displayCredentials("Edward Snowden", "edwardsnowden@nsa.gov.com", "1-789-456-1236")