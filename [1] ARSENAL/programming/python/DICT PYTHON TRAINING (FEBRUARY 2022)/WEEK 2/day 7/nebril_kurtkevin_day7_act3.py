class Car:
    def __init__ (self, color, model, manufacturer):
        self.color = color
        self.model = model
        self.manufacturer = manufacturer

    def displayValue (self):
        print(f'Car color: {self.color}')
        print(f'Car model: {self.model}')
        print(f'Car manufacturer: {self.manufacturer} \n')

car_class = Car("Blue", "2022", "Toyota")
car_class2 = Car("Red", "2021", "Nissan")
car_class3 = Car("Black", "2019", "Isuzu")

car_class.displayValue()
car_class2.displayValue()
car_class3.displayValue()