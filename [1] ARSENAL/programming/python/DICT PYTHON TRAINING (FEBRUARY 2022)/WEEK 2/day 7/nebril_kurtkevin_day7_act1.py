import random

namesContainer = []
while True:
    choice = input("Do you want to generate a new name: [Y/n]: ")

    firstname = ['Waldo','Clara','Cedric','Archer','Helen','Roger','Otis','Genevieve','Dane','Tammy']
    middlename = ['Elliott','Silva','Parker','Skinner','Stevenson','Morales','Fields','Willis','Bradley','Stephenson']
    lastname = ['Holden','Bowen','Baxter','Wallaker','Wolfe','Lucas','Kimmons','Page','Obrien','Obrien',]    
    randomInt1, randomInt2, randomInt3 = random.sample(range(0, 9), 3)

    if choice in ['Y', 'y']:
        generatedName = f'{firstname[randomInt1]} {middlename[randomInt2]} {lastname[randomInt3]}'
        namesContainer.append(generatedName)
        print(f"Congratulatations! Your new name is {generatedName}")
    elif choice in ['N', 'n']:
        print("Thank you!\n")
        print(f"Here generated names: ")
        for names in namesContainer:
            print(names)
        break