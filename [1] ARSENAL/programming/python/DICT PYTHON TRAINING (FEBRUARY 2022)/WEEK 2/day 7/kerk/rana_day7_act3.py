class Student:
    name = ''
    email = ''
    cnum = ''

    def showInformation(self):
        print('Name: {}'.format(self.name))
        print('Email: {}'.format(self.email))
        print('Contact Number: {}'.format(self.cnum))

newClass = Student()
newClass.name = 'Kerk'
newClass.email = 'kerk@gmail.com'
newClass.cnum = '0945 456 4555'
newClass.showInformation()