operation = input('Enter operation (1) to add (2) to subtract (3) to multiply (4) to divide: ')
in1 = float(input('Enter input 1: '))
in2 = float(input('Enter input 2: '))

try:
    if operation == '1':
        print(f'Result: {in1} + {in2} = {in1 + in2}')
    if operation == '2':
        print(f'Result: {in1} - {in2} = {in1 - in2}')
    if operation == '3':
        print(f'Result: {in1} * {in2} = {in1 * in2}')
    if operation == '4':
        print(f'Result: {in1} / {in2} = {in1 / in2}')
except ArithmeticError as e:
    print('Error: ', e)
except ValueError as e:
    print('Error: ', e)
except Exception as e:
    print('Error: ', e)