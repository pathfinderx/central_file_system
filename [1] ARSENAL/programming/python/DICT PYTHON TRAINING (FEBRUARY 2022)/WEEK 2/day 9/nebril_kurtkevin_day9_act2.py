import os, re, json

class FileHandling:
    def __init__(self):
        self.__loopBreaker = False
        self.container = dict()
        self.users = list()
        self.usersIndex = list()
        
    def get_loopBreaker (self):
        return self.__loopBreaker

    def fetchData (self):
        try:
            file = open('json.txt', 'r')
            data = file.read()
            self.users = json.loads(data)
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('json.txt')
        except Exception as e:
            print(f'{e} \n')

    def createFile(self, filename):
        try:
            newFile = open(f"{filename}", "x")
            newFile.close()
            print(f"{filename} created at: {os.path.abspath(filename)}")
        except Exception as e:
            print(f'{e} \n')

    def addRecord (self, name, email, address):
        try:
            check = open('json.txt', 'r').read()
            if re.search(r'[a-zA-Z0-9]+', check):
                self.fetchData()
                self.saveToRecords(name=name, email=email, address=address, state="NEW")
            else:
               self.saveToRecords(name=name, email=email, address=address, state="NEW")
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('json.txt')
        except Exception as e:
            print(f'Error: {e}')
            
    def saveToRecords (self, **kwargs):
        try:
            if kwargs['state'] == 'NEW':
                self.container.update({
                    "name":kwargs['name'],
                    "email":kwargs['email'],
                    "address":kwargs['address']
                })
                self.users.append(self.container)

            # Append to json.txt
            jsonFile = open(f"json.txt", "a")
            self.clearRecord(state="NONE")
            _json = json.dumps(self.users)
            jsonFile.write(_json)
            jsonFile.close()

            # Append to records.txt
            textFile = open(f"records.txt", "a")
            for x in range(0,len(self.users)):
                textFile.write(f'----- Credential {x+1} -----\n')
                textFile.write(f'Name: {self.users[x].get("name")}\n')
                textFile.write(f'Email: {self.users[x].get("email")}\n')
                textFile.write(f'Address: {self.users[x].get("address")}\n')
                textFile.write(" \n")
            textFile.close()
            print("Information saved at file: records.txt\n")
        except Exception as e:
            print(f'{e} \n')
            
    def viewRecord (self):
        try:
            records = open('records.txt', 'r').read()
            if re.search(r'[a-zA-Z0-9]+', records):
                print(records)
            else:
                print('No data in records\n')
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('records.txt')
        except Exception as e:
            print(f'Error: {e}')

    def editRecord (self):
        try:
            self.fetchData()
            if self.users:
                print('Emails to edit in records: ')
                for x in range(len(self.users)):
                    self.usersIndex.append(x)
                    print(f'[{x}] - {self.users[x].get("email")}')
                targetIndex = int(input('Select index to edit email: '))
                
                if targetIndex in self.usersIndex:
                    userEmail = self.users[targetIndex].get("email")
                    newEmail = input(f'Enter new email to edit this credential ({self.users[targetIndex].get("email")}): ')
                    if newEmail:
                        self.users[targetIndex].update({
                            'email':newEmail
                        })
                    print(f"Successfully edited ({userEmail}) to ({newEmail})")
                    self.saveToRecords(state="None")
                else:
                    print("Index not found in record\n")
            else:
                print('Record is empty. Not allowed to edit null data\n')
        except Exception as e:
            print(f'{e} \n')
        
    def clearRecord (self, **kwargs):
        try:
            file = open('records.txt', 'r+')
            file.truncate(0)
            file.close()

            file = open('json.txt', 'r+')
            file.truncate(0)
            file.close()

            print("No records found. \n") if kwargs['state'] == 'USERSELECTION' else None
                
        except Exception as e:
            print(f'{e} \n')
            
    def userSelection(self, choice):
        try:
            if choice in ['A', 'a']:
                name = input("Enter name: ")
                email = input("Enter email: ")
                address = input("Enter address: ")
                if name and email and address:
                    self.addRecord(name, email, address)
                else:
                    print('Please fill up all the input')

            elif choice in ['B', 'b']:
                self.viewRecord ()

            elif choice in ['C', 'c']:
                self.editRecord()

            elif choice in ['D', 'd']:
                self.clearRecord (state="USERSELECTION")

            elif choice in ['E', 'e']:
                self.__loopBreaker = True
                print('Thank you')
            else:
                print("Invalid Choice. Try again \n")
        except Exception as e:
            print(f'{e} \n')

class_file_handling = FileHandling()
loopBreaker = class_file_handling.get_loopBreaker()
while not loopBreaker:
    choice = input("[A] Add Record [B] View Record [C] Edit Contact [D] Clear Record [E] Exit: ")
    class_file_handling.userSelection(choice)
    loopBreaker = class_file_handling.get_loopBreaker()