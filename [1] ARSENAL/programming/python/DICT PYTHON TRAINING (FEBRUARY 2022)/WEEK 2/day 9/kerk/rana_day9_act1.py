import os

halt = False
while not halt:
    try:
        fileName = input("Input file name: ")
        if os.path.exists(fileName):
            option = input("file exists! [a] continue [b] exit: ")
            if option == 'a':
                continue
            elif option == 'b':
                halt = True
        else:
            newFile = open('{}'.format(fileName), 'x')
            newFile.close()
            print("{} file created! ".format(fileName))

            option = input("file exists! [a] continue [b] exit: ")
            if option == 'a':
                continue
            elif option == 'b':
                halt = True
    except Exception as e:
        print(e)