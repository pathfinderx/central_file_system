import os
import re

halt = False
class File:
    def add (self,name, email, address):
        try:
            newFile = open("myFile.txt", "a")
            newFile.write('{}\n{}\n{}\n'.format(name,email,address))
            newFile.write('\n')
            newFile.close()
            print("Saved! ")
        except Exception as e:
            print(e)
            file = open("myFile.txt", 'x')
            file.close()
            print("file created")

    def view (self,):
        try:
            file = open("myFile.txt", "r")
            print(file.read())
        except Exception as e:
            print(e)
            file = open("myFile.txt", 'x')
            file.close()
            print("file created")

    def edit (self, old, new):
        try:
            file = open('myFile.txt', 'r')
            content = file.read()
            replace = re.sub('{}'.format(old), new, content)

            if replace:
                self.clear()
                newFile = open('myFile.txt', 'a')
                newFile.write(replace)
                newFile.close()

                print('{} edited to {}'.format(old,new))
            else:
                print("No file match occurs")
        except Exception as e:
            print(e)

    def clear(self,):
        try:
            open('myFile.txt', 'w').close()
        except Exception as e:
            print(e)
        
fileClass = File()
while not halt:
    print("Select from these following: ")
    print('[A] Add')
    print('[B] View')
    print('[C] Edit')
    print('[D] Clear')
    print('[E] Exit')
    option = input(':')

    if option == 'A' or option == 'a':
        name = input("Enter name: ")
        email = input("Enter email: ")
        address = input("Enter address: ")
        fileClass.add(name, email, address)

    elif option == 'B' or option == 'b':
        fileClass.view ()

    elif option == 'C' or option == 'c':
        old = input("Enter old email: ")
        new = input("Enter new email: ")
        fileClass.edit(old, new)

    elif option == 'D' or option == 'd':
        fileClass.clear()

    elif option == 'E' or option == 'e':
        print('Thank you')
        halt = True