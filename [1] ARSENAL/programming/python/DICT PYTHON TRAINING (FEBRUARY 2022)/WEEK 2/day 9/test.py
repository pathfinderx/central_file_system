# import os
# fileName = input("Please enter the name of the file you'd like to use: ")
# path = 'D:/THREADSTONE/[1] ARSENAL/programming/python/DICT PYTHON TRAINING (FEBRUARY 2022)/WEEK 2/day 9/'

# while not os.path.isfile(path+fileName):
#     fileName = input("Whoops! No such file! Please enter the name of the file you'd like to use.")
import os.path

while True:
    try:
        fileName = input("Enter file name to begin the execution: ")
        path = 'D:/THREADSTONE/[1] ARSENAL/programming/python/DICT PYTHON TRAINING (FEBRUARY 2022)/WEEK 2/day 9/'

        if os.path.isfile(f"{path}{fileName}"):
            choice = input("File Exists. [1] Create file? [2] Exit: ")
            if choice in ['1']:
                continue
            elif choice in ['2']:
                print("Thank you! ")
                break
        else:
            newFile = open(f"{path}{fileName}", "a")
            newFile.write("New File Created! ")
            newFile.close()
    except Exception as e:
        print('Error: ', e)