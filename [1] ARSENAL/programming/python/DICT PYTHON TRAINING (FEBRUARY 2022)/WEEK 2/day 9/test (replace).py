import os, re

def addRecord (name, email, address):
    try:
        newFile = open(f"records.txt", "a")
        newFile.write(f'{name}\n{email}\n{address}')
        newFile.write(f'\n')
        newFile.close()
        print("Information saved! ")
    except Exception as e:
        print(f'Error: {e}')

def displayRecord ():
    try:
        file = open("records.txt", "r")
        print(file.read())
    except Exception as e:
        print(f'Error: {e}')

def editRecord (target, new):
    # replaced = re.sub('[ES]', 'a', s)
    with open ('records.txt', 'r' ) as f:
        content = f.read()
        replace = re.sub(f'{target}', new, content)
        f.write(replace)
def clearRecord ():
    try:
        file = open('records.txt', 'r+')
        file.truncate(0)
        file.close()
        print("No records found.")
    except Exception as e:
        print(f'Error: {e}')
        
choice = input("[A] Add Record [B] View Record [C] Edit Email [D] Clear All Records [E] Exit: ")
    
if choice in ['A', 'a']:
    name = input("Enter name: ")
    email = input("Enter email: ")
    address = input("Enter address: ")
    addRecord(name, email, address)

elif choice in ['B', 'b']:
    displayRecord ()

elif choice in ['C', 'c']:
    target = input("Enter target email: ")
    new = input("Enter new email: ")
    editRecord(target, new)

elif choice in ['D', 'd']:
    clearRecord()

elif choice in ['E', 'e']:
    print('Thank you')