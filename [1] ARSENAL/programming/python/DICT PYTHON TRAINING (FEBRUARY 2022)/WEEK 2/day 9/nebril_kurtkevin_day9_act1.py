import os.path

while True:
    try:
        fileName = input("Enter file name to begin the execution: ")
      
        if os.path.isfile(f"{fileName}"):
            choice = input("File Exists. [1] Restart [2] Exit: ")
            if choice in ['1']:
                continue
            elif choice in ['2']:
                break
        else:
            newFile = open(f"{fileName}", "x")
            newFile.close()
            print("File does not exist. New file created! ")
    except Exception as e:
        print('Error: ', e)