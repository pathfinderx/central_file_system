import GrossSalary

class SalaryDeductionsClass:
    def __init__ (self):
        self.GrossSalary = GrossSalary.GrossSalaryClass()

    def get_salary_deductions (self):
        loan = float(input("Loan: "))
        insurance = float(input("Insurance: "))
        tax =  self.GrossSalary.get_gross_salary() * 0.12

        return (tax + loan + insurance)

    def get_gross_salary (self):
        return self.GrossSalary.get_gross_salary()