import SalaryDeductions
import GrossSalary

def execute():
    SalaryDeductionsClass = SalaryDeductions.SalaryDeductionsClass()
    salary_deduction = SalaryDeductionsClass.get_salary_deductions()
    net_salary = (SalaryDeductionsClass.get_gross_salary() - salary_deduction)
    print(f"Total Deductions:", salary_deduction)
    print(f"Net Salary: Php {net_salary}")