def salaryDeductions (grossSalary, loan, insurance):
    totalDeductions = tax(grossSalary) + loan + insurance
    return totalDeductions

def tax (grossSalary):
    return (12 / 100) * grossSalary