from GrossSalary import grossSalary
from SalaryDeductions import salaryDeductions
from SalaryDeductions import tax
from NetSalary import netSalary

name = input("Name: ")
hour = int (input("Hour: "))
loan = float(input ("Loan: "))
insurance = float(input ("Insurance: "))

grossSalary = grossSalary(hour)
salaryDeduction = salaryDeductions(grossSalary, loan, insurance)
tax = tax(grossSalary)
netSalary = netSalary(grossSalary, salaryDeduction)

print('\nName: {}'.format(name))
print('Hour: {}'.format(hour))
print('Gross Salary: Php {}'.format(grossSalary))
print('Tax: Php {}'.format(tax))
print('Loan: Php {}'.format(loan))
print('Insurance: Php {}'.format(insurance))
print('Total Deductions: {}'.format(salaryDeduction))
print('Net salary: Php {}'.format(netSalary))