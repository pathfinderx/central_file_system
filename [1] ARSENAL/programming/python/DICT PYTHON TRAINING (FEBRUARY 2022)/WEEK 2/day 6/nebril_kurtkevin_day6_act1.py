print("Record Keeping App")
storage = {}
while True:
    choice = input ("\nEnter choice: [1] Add data, [2] Delete data, [3] Exit: ")
    if choice in ["1"]:
        try:
            key = input ("Enter key: ")
            value = input ("Enter value: ")
            if key and value:
                storage.update({key:value})
            else:
                print('Key and value should both have values')
            print(f'Values in dictionary: {storage}')
        except Exception as e:
            print(e)

    elif choice in ["2"]:
        try:
            key = input ("\nEnter key to delete: ")
            storage.pop(key, None) if storage.pop(key, None) else print("Unable to find the key: Failed to delete key and its value")
            print(f'Values in dictionary: {storage}')
        except Exception as e:
            print(e)

    elif choice in ["3"]:
        print("Thank you!")
        break