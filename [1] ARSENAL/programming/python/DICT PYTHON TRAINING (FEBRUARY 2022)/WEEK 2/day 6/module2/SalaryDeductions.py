def get_tax (gross_salary):
    return (gross_salary * 0.12)

def get_salary_deductions (gross_salary, loan = 0, insurance = 0):
    tax = gross_salary * 0.12
    return (tax + loan + insurance) 