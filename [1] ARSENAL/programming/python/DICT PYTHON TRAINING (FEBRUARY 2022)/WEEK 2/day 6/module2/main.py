from GrossSalary import get_gross_salary
from SalaryDeductions import get_salary_deductions, get_tax
from NetSalary import get_net_salary

try:
    print('SALARY COMPUTATION APP \n')
    name = input("Enter Name: ")
    hour = int(input("Enter Hour: "))
    loan = float(input("Enter Loan: "))
    insurance = float(input("Enter Insurance: "))
    RATE = 500
except Exception as e:
    print("An Error occured while entering values.\nWith error: ", e)

if name and hour:
    gross_salary = get_gross_salary(RATE, hour)
    tax = get_tax(gross_salary)
    salary_deductions = get_salary_deductions(gross_salary, loan, insurance)
    net_salary = get_net_salary(gross_salary, salary_deductions)

    print (f'''
        OUTPUT:

        Name: {name}
        Hour: {hour}

        Gross Salary: Php {gross_salary}

        Tax: Php {tax}
        Loan: Php {loan}
        Insurance: Php {insurance}

        Total Deductions: Php {salary_deductions}

        Net Salary: Php {net_salary}
    ''')
else:
    print("Please fill-in all the input")