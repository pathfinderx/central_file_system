def get_net_salary(gross_salary, salary_deductions):
    return (gross_salary - salary_deductions) if gross_salary and salary_deductions else None
        