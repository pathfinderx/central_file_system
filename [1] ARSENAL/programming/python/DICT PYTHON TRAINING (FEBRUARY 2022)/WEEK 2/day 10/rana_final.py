import os
import re
import json

class Parent:
    def __init__(self):
        self.halt = False

class Child(Parent):
    def __init__(self):
        super().__init__()
        self.__newData = {}
        self._listData = []

    def setHalt(self):
        self.halt = True
        return self.halt

    def getHalt(self):
        return self.halt

    def getAndExtract(self):
        try:
            file = open('rana.txt', 'r')
            data = file.read()
            try:
                self._listData = json.loads(data)
            except Exception as e:
                print('text file has no data: ', e)
        except FileNotFoundError as e:
            try:
                file = open("rana.txt", 'x')
                file.close()
                print("file created")
            except Exception as e:
                print(e)
        except Exception as e:
            print(e)

    def add (self, name, date, time, adult, child):
        try:
            self.getAndExtract()

            self.__newData['name'] = name
            self.__newData['date'] = date
            self.__newData['time'] = time
            self.__newData['adult'] = adult
            self.__newData['child'] = child
            self.__newData['total'] = ((500 * adult) + (300 * child))
            self._listData.append(self.__newData)

            open('rana.txt', 'w').close()
            file = open(f"rana.txt", "a")
            data = json.dumps(self._listData)
            file.write(data)
            file.close()

            print('added!')

        except Exception as e:
            print(e)
            file = open("rana.txt", 'x')
            file.close()
            print("file created")

    def view (self, **options):
        try:
            self.getAndExtract()
            if self._listData:
                headers = []
                if options['options'] == 'view':
                    headers = ["Name","Date","Time","Adult","Child","Total"]
                elif options['options'] == 'delete':
                    headers = ["Index","Name","Date","Time","Adult","Child","Total"]
                elif options['options'] == 'report':
                    adults = 0
                    kids = 0
                    total = 0
                    for x in self._listData:
                        adults += x.get('adult')
                        kids += x.get('child')
                        total += x.get('total')
                    return [adults,kids,total]
                row_format ="{:>15}" * (len(headers) + 1)
                body = []

                print(row_format.format("", *headers))
                for x in range(len(self._listData)):
                    if options['options'] == 'delete':
                        body.append(x)
                    for k,v in self._listData[x].items():
                        body.append(v)
                    print(row_format.format("", *body))
                    body = []
            else:
                print("No records found")

        except Exception as e:
            print(e)
            file = open("rana.txt", 'x')
            file.close()
            print("file created")

    def delete (self,):
        try:
            self.view(options='delete')
            indexToDelete = int(input('Select Index to Delete: '))
            if indexToDelete >= 0:
                del self._listData[indexToDelete]

                open('rana.txt', 'w').close()
                file = open(f"rana.txt", "a")
                data = json.dumps(self._listData)
                file.write(data)
                file.close()

                print('Deleted! \n')
            else:
                print('Invalid index')

        except Exception as e:
            print(e)

    def report (self):
        try:
            self.view(options='view')
            data = self.view(options='report')
            if data:
                headers = ["Total Adults", "Total Kids", "GrandTotal"]
                row_format ="{:>15}" * (2)
                print(row_format.format("", f'Total Adults: {data[0]}'))
                print(row_format.format("", f'Total Kids: {data[1]}'))
                print(row_format.format("", f'Total GrandTotal: {data[2]}'))
        except Exception as e:
            print(e)
        
childClass = Child()
while not childClass.getHalt():
    print("Select from these following: ")
    print('[A] View all reservation')
    print('[B] Make Reservation')
    print('[C] Delete Reservation')
    print('[D] Generate Report')
    print('[E] Exit')
    option = input(':')

    if option == 'A' or option == 'a':
      childClass.view (options='view')

    elif option == 'B' or option == 'b':
        name = input("Enter name: ")
        date = input("Enter date: ")
        time = input("Enter time: ")
        adult = int(input("Enter adult: "))
        child = int(input("Enter child: "))
        childClass.add(name, date, time, adult, child)

    elif option == 'C' or option == 'c':
        childClass.delete()

    elif option == 'D' or option == 'd':
        childClass.report()

    elif option == 'E' or option == 'e':
        print('Thanks')
        childClass.setHalt()