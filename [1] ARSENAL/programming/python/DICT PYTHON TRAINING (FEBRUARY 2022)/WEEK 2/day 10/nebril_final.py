import os, re, json

class RestaurantReservation:
    def __init__ (self):
        self.__loopBreaker = False
        self.container = dict()
        self.reservations = list()
        self.reservationsIndex = list()
        self.__CHILDHEAD = 300
        self.__ADULTHEAD = 500

    def get_loopBreaker(self):
        return self.__loopBreaker

    def fetchData(self):
        try:
            file = open('json.txt', 'r')
            data = file.read()
            self.reservations = json.loads(data)
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('json.txt')
        except Exception as e:
            print(f'{e} \n')

    def createFile(self, filename):
        try:
            newFile = open(f"{filename}", "x")
            newFile.close()
            print(f"{filename} created at: {os.path.abspath(filename)}")
        except Exception as e:
            print(f'{e} \n')

    def clearRecord (self, **kwargs):
        try:
            open('json.txt', 'w').close()
            open('nebril.txt', 'w').close()

            print("No records found. \n") if kwargs['state'] == 'USERSELECTION' else None
                
        except Exception as e:
            print(f'{e} \n')

    def viewAllReservations(self):
        try:
            records = open('nebril.txt', 'r').read()
            if re.search(r'[a-zA-Z0-9]+', records):
                print(records)
            else:
                print('No data in records\n')
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('nebril.txt')
        except Exception as e:
            print(f'Error: {e}')

    def makeReservations(self, name, date, time, numChild, numAdult):
        try:
            check = open('json.txt', 'r').read()
            if re.search(r'[a-zA-Z0-9]+', check):
                self.fetchData()
                self.saveToRecords(name=name, date=date, time=time, numChild=numChild, numAdult=numAdult, state="NEW")
            else:
               self.saveToRecords(name=name, date=date, time=time, numChild=numChild, numAdult=numAdult, state="NEW")
        except FileNotFoundError as e:
            print(f'Error: {e}')
            print(f'auto creating file..')
            self.createFile('json.txt')
        except Exception as e:
            print(f'Error: {e}')

    def saveToRecords (self, **kwargs):
        try:
            if kwargs['state'] == 'NEW':
                reservationTotal = (self.__CHILDHEAD * kwargs['numChild']) + (self.__ADULTHEAD * kwargs['numAdult'])
                self.container.update({
                    "name":kwargs['name'],
                    "date":kwargs['date'],
                    "time":kwargs['time'],
                    "numChild":kwargs['numChild'],
                    "numAdult":kwargs['numAdult'],
                    "grandTotal":reservationTotal
                })
                self.reservations.append(self.container)

            # Append to json.txt
            jsonFile = open(f"json.txt", "a")
            self.clearRecord(state="NONE")
            _json = json.dumps(self.reservations)
            jsonFile.write(_json)
            jsonFile.close()

            # Append to nebril.txt
            textFile = open(f"nebril.txt", "a")
            textFile.write(f'Date\t\tTime\t\tName\t\tAdults\t\tChildren\n')
            for x in range(0,len(self.reservations)):
                textFile.write(f"{self.reservations[x].get('date')}\t{self.reservations[x].get('time')}\t\t{self.reservations[x].get('name')}\t\t{self.reservations[x].get('numAdult')}\t\t{self.reservations[x].get('numChild')}")
                textFile.write(" \n")
            textFile.close()
            print("Information saved at file: nebril.txt\n")
        except Exception as e:
            print(f'{e} \n')

    def deleteReservations(self):
        try:
            self.fetchData()
            if self.reservations:
                print('Reservations to delete: ')
                print(f'Date\t\tTime\t\tName\t\tAdults\t\tChildren\n')
                for x in range(len(self.reservations)):
                    self.reservationsIndex.append(x)
                    print(f"[{x}] - {self.reservations[x].get('date')}\t{self.reservations[x].get('time')}\t\t{self.reservations[x].get('name')}\t\t{self.reservations[x].get('numAdult')}\t\t{self.reservations[x].get('numChild')}")
                targetIndex = int(input('Select index to delete reservation: '))

                if targetIndex in self.reservationsIndex:
                    del self.reservations[targetIndex]
                    print(f"Reservation Successfully Deleted")
                    self.saveToRecords(state="None")
                else:
                    print("Index not found in record\n")
            else:
                print('Record is empty. Not allowed to edit null data\n')
        except Exception as e:
            print(f'{e} \n')

    def generateReport(self):
        try:
            self.fetchData()

            try:
                totalAdults, totalKids, grandTotal = 0, 0, 0
                for x in self.reservations:
                    totalAdults += x.get('numAdult')
                    totalKids += x.get('numChild')
                    grandTotal += x.get('grandTotal')
            except ArithmeticError as e:
                print(f'{e} \n')

            print('REPORT')
            print(f'Date\t\tTime\t\tName\t\tAdults\t\tChildren\tSubtotal')
            for x in range(0,len(self.reservations)):
                print(f"{self.reservations[x].get('date')}\t{self.reservations[x].get('time')}\t\t{self.reservations[x].get('name')}\t\t{self.reservations[x].get('numAdult')}\t\t{self.reservations[x].get('numChild')}\t\t{self.reservations[x].get('grandTotal')}")
            print(f'\nTotal number of Adults: {totalAdults}')
            print(f'Total number of Kids: {totalKids}')
            print(f'GrandTotal: PHP {grandTotal}')
            print('\n--------------------------- nothing follows ---------------------------')
        except Exception as e:
            print(f'{e} \n')

    def userSelection(self, selection):
        try:
            if selection in ['A', 'a']:
              self.viewAllReservations()

            elif selection in ['B', 'b']:
                name = input("Enter name: ")
                date = input("Enter date: ")
                time = input("Enter time: ")
                numChild = int(input("Enter number of child: "))
                numAdult = int(input("Enter number of adults: "))

                if name and date and time and numChild and numAdult:
                    self.makeReservations(name, date, time, numChild, numAdult)
                else:
                    print('Please fill up all the input')

            elif selection in ['C', 'c']:
                self.deleteReservations()

            elif selection in ['D', 'd']:
                self.generateReport()

            elif selection in ['E', 'e']:
                self.__loopBreaker = True
                print('Thank you!')

            else:
                print("Invalid selection. Try again \n")
        except Exception as e:
            print(f'{e} \n')

class superSystem(RestaurantReservation):
    def __init__(self):
        super().__init__()

    def userSelection(self, selection):
        RestaurantReservation.userSelection(self, selection) 

superSystem = superSystem()
loopBreaker = superSystem.get_loopBreaker()
while not loopBreaker:
    selection = input("[A] View all Reservations [B] Make Reservation [C] Delete Reservation [D] Generate Report [E] Exit: ")
    superSystem.userSelection(selection)
    loopBreaker = superSystem.get_loopBreaker()