
# encapsulation
class Employee:
    def __init__ (self, name, salary):
        self.name = name
        self.__salary = salary

    def showSal (self):
        print(f"Name: {self.name} Salary: {self.__salary}")

employee_class = Employee("John", "500000")
employee_class.showSal()