class Employee:
    def __init__(self, name):
        self.name = name
        self.__email = None
        self.__cnum = None

    def set_email (self, email):
        self.__email = email
        
    def get_email (self):
        return self.__email

    def set_cnum (self, cnum):
        self.__cnum = cnum

    def get_cnum (self):
        return self.__cnum

class_employee = Employee('Edward Snowden')
class_employee.set_email('edwardsnowden@nsa.gov')
class_employee.set_cnum('+6345885233')
print(f'Name: {class_employee.name}')
print(f'Email: {class_employee.get_email()}')
print(f'Contact Number: {class_employee.get_cnum()}\n')

class_employee = Employee('John Hammond')
class_employee.set_email('johnhammond@nasa.gov')
class_employee.set_cnum('+69875455')
print(f'Name: {class_employee.name}')
print(f'Email: {class_employee.get_email()}')
print(f'Contact Number: {class_employee.get_cnum()}')