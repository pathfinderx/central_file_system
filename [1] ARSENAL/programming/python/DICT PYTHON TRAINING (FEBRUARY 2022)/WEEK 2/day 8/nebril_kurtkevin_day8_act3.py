class House:
    def __init__(self):
        self.floorSize, self.noOfFloors, self.noOfDoors = 1, 1, 1

    def switchOn(self):
        print(f'The light has switched! ')

    def lightOpen(self):
        print(f'The light was opened! ')

    def ovenOpen(self):
        print(f'The oven was opened! ')

class TownHouse (House):
    def __init__(self):
        super().__init__()

    def switchOn(self): # override
        House.lightOpen(self) 
        House.ovenOpen(self)

class_townhouse = TownHouse()
class_townhouse.floorSize, class_townhouse.noOfFloors, class_townhouse.noOfDoors = 2, 2, 2
print(f'The floorsize town house: {class_townhouse.floorSize}')
print(f'The number of floors town house: {class_townhouse.noOfFloors}')
print(f'The number of doors town house: {class_townhouse.noOfDoors}')
class_townhouse.switchOn()