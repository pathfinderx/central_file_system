class Student ():
    def __init__ (self, name, math_grade, science_grade, english_grade):
        self.__name = name
        self.__math_grade = math_grade
        self.__science_grade = science_grade
        self.__english_grade = english_grade
        
    def computeAverage (self):
        return ((self.__math_grade + self.__science_grade + self.__english_grade) / 3)
        
    def displayResult (self):
        print(f'\nName: {self.__name}')
        print(f'Math: {self.__math_grade}')
        print(f'Science: {self.__science_grade}')
        print(f'English: {self.__english_grade}')
        print(f'Average: {self.computeAverage()} (Passed)') if self.computeAverage() >= 75 else print(f'Average: {self.computeAverage()} Failed')

name = input('Enter Name: ')
math = float(input('Enter Math: '))
science = float(input('Enter science: '))
english = float(input('Enter english: '))

class_student = Student(name, math, science, english)
class_student.displayResult()