* intallation and setup
	* install through and find the latest stable version # https://www.python.org/downloads/
	* setup environment variables
		* check first if it has python folder # C:\Users\kurt kevin nebril\AppData\Local\Programs
		* then go to 
			# rclinck 'This pc'
			# select 'properties'
			# select 'advanced system settings'
			# select 'environment variables'

* some resources
	# https://www.javatpoint.com/java-programs
	# https://beginnersbook.com/2017/09/java-examples/
	# https://www.programiz.com/python-programming/examples

* good resources / references
	# https://towardsdatascience.com/top-13-resources-to-learn-python-programming-46f3b0b74b91
		# https://www.pythonforbeginners.com/
		# https://www.learnpython.org/en/Hello%2C_World%21
		# https://github.com/swaroopch/byte-of-python/releases/tag/va62ee8caa3475e8733c40918bbe16719b4904693
		# file:///C:/Users/kurt%20kevin%20nebril/Downloads/byte-of-python.pdf
		# https://www.pythonforbeginners.com/python-tutorial#4
		# https://www.w3schools.com/python/python_variables.asp
	
* eclipse setup
	* dark theme
		# https://www.youtube.com/watch?v=XdXj0za2diY
	* pydev
		# https://www.youtube.com/watch?v=NDFbXIiqT4o

* project structure
	# https://docs.python-guide.org/writing/structure/

* Data Types
	* difference between list, tuple, set, dict
		#https://www.geeksforgeeks.org/differences-and-applications-of-list-tuple-set-and-dictionary-in-python/

	* string
		* string concatenation
			# https://www.geeksforgeeks.org/python-program-to-convert-a-list-to-string/
		* slicing
			# http://pythonchallenges.weebly.com/string-slicing.html
		* string exercises
			# https://pynative.com/python-string-exercise/

	* list
		# https://www.programiz.com/python-programming/list-comprehension

* printing 
	# https://www.afternerd.com/blog/how-to-print-without-a-newline-in-python/