'''
Fundaments outline
	Data types
		Declaration
		difference between list, tuple, set, dict, frozenset
		differences of methods
		Checkers
		Casting
		Numbers
		String
		List
		Dictionary
		Built-in Methods
			sorted()
	Iterable and Iterators
	Scope
	Modules
	User Input
OOP
	Classes
	Inheritance
	
Advanced
	Lambda
	Regex
	Function

	PIPI
	Virtual Environment

Sublime build system
'''
@FUNDAMENTALS
	@Data types
		# Text Type:	str
		# Numeric Types:	int, float, complex
		# Sequence Types:	list, tuple, range
		# Mapping Type:	dict
		# Set Types:	set, frozenset
		# Boolean Type:	bool
		# Binary Types:	bytes, bytearray, memoryview

		@Declaration
			# Python has no command for declaring a variable.
			# A variable is created the moment you first assign a value to it.
				# example:
					# x = 123      # integer
					# x = 123L     # long integer
					# x = 3.14      # double float
					# x = "hello"       # string
					# x = [0,1,2]       # list
					# x = (0,1,2)       # tuple
				# x = open(‘hello.py’, ‘r’)  # file

		@difference between list, tuple, set, dict, frozenset

					heterogenous	|	mutable			| 	multiple rows	|	ordered		|	duplicate	|	symbol
					--------------------------------------------------------------------------------------------
			list:		yes			|	yes				| 	yes				|	yes			|	yes			|	[]
			tuple:	 	no			|	no 				|	yes				|	yes			|	yes			|	()
			set:		yes			|	yes				|	no 				|	no 			|	no 			|	{}
			dict:		yes			|	yes				|	value-key		|	yes			|	no 			| 	{}	
			frozenset:	

		@differences of methods
				List: 							Dictionary
									# ACCESS
				# id[range]						# id[key]
				# .index(value)					# .get(index)
												# .keys()

									# ADD
				# id[key] = value 				# id[key] = v
				# .append(value)				# .update({key:value})
				# .insert(index, value)			
				# .extend(id)

									# UPDATE
				# id[index] = value				# id[key] = value
				# id[range] = [value, value]	# update({key:value})
				# .insert(index, value)			

									# REMOVE
				# .remove(value)				# .pop(key)
				# .pop()						# .popitem()
				# .pop(index)					# del id[key]
				# del id[index]					# .clear()
				# del id
				# .clear()

									# LOOP
				# for x in id 					# for x in id
				# for x in range(len(id))		# for x in id.values()
				# while i < len(id)				# for x in id.keys()
				# [expression loop]				# for x, y in id.items()

									# COPY
				# id2 = id1.copy()				# id2 = id1.copy()
				# id2 = list(id1)				# id2 = dict(id1)

									# JOIN
				# id3 = id1 + id2
				# for x in id1:
					# id2.append(x)
				# id2.extend(id1)

		@Checkers
			List:
				# https://appdividend.com/2020/01/21/python-list-contains-how-to-check-if-item-exists-in-list/
				# check if empty 
					(len(list) > 0)
			Dict, json or tag:
				# 'attributes' in var
				# var.get('attributes')
				# tag.hast_attr('attributes')
		@Casting
			* If you want to specify the data type of a variable, this can be done with casting.
				# int:
					# x = int(1)   # x will be 1
					# y = int(2.8) # y will be 2
					# z = int("3") # z will be 3

				# float
					# x = float(1)     # x will be 1.0
					# y = float(2.8)   # y will be 2.8
					# z = float("3")   # z will be 3.0
					# w = float("4.2") # w will be 4.2

				# string
					# x = str("s1") # x will be 's1'
					# y = str(2)    # y will be '2'
					# z = str(3.0)  # z will be '3.0'

		@Numbers
		
			# Declaration
				# x = 1    # int
				# y = 2.8  # float
				# z = 1j   # complex

				# x = 35e3 # float indicate 'e' as power of 10
				# y = 12E4  # float indicate 'e' as power of 10
				# z = -87.7e100  # float indicate 'e' as power of 10

				# x = 3+5j # complex 'j' as an imaginary number
				# y = 5j # complex 'j' as an imaginary number
				# z = -5j # complex 'j' as an imaginary number

				# to check datatype:
					# type()

		@String 
			@Variable Assignments (unpacking)
				# assign multiple variables with multiple values in one line (unpacking)
					# x, y, z = "Orange", "Banana", "Cherry"

					# >>> print(x,y,z)
					# orange banana apple

				# assign one value in multiple variables
					# x = y = z = "Orange"

					# >>> print(x,y,z)
					# orange orange orange

				# unpack a collection and reassign to variables (unpacking)
					# fruits = ["apple", "banana", "cherry"]
					# x, y, z = fruits

					# >>> print(x,y,z)
					# apple banana orange

			@slicing or indexing
				# if reading backwards (negative indexing), always include the 3d param
					#  print(str[-1:-7:-1]) # 1st param: starting at last; 2nd param: end -1; 3rd param skip times

			@String formatting
				# https://www.w3schools.com/python/python_string_formatting.asp
				# 2 ways to format 
					# use 'f' key word 
						'''
							def arg_printer(a, b, *args):
							print(f'a is {a}')
							print(f'b is {b}')
							print(f'args are {args}')
						'''
					# use '.format(varString)'
						'''
							def setOtherFormats2 (self, number, direction1, direction2):
								varNewFormattedString = "I have {} hands the {} and the {}"
								print(varNewFormattedString.format(number,direction1,direction2))
						'''
					# use indexing inside curly brace	
						'''
							def setFormatIndexing (self, age, name):
								varStringSequence = "His is name is {1}. {1} is {0} years old"
								print(varStringSequence.format(age,name))
						'''	

		@List
			# can be created using list() contructor but with double quote
			access
				# can be access through loop or indexing
				# get index of the list value
					# use .index()
					# list.index(list[x])

			append items
				# use the normal indexing id[index]
				# use append() to insert one item at the end of the list
				# use insert() insert item to specific index
					# insert(index, value) method works though declared 
				# use extend() to append another element to the list

			change item
				# use indexing, range, loop
				# use insert(index, newValue) # but cannot replace the value at specified index, just moved

			remove item
				# use remove() to remove item by value
				# use pop() to remove item by index
				# use del to remove item by index or delete all items completely

			loop list
				# use for or while loop

			list comprehension
				# can be used to create new list based on the values of an existing list
				# sample # variable = [item for item in list]

				# variable = [expression loop]
				# variable = [expression loop conditions...]
				# variable = [conditions expression loop]

				# https://www.programiz.com/python-programming/list-comprehension

			sort list
				# you can do manual
				# or use sort() ascending order
				# use reverse() descending order

			copy
				# You cannot copy a list simply by typing list2 = list1, because: list2 will only be a reference to list1, and changes made in list1 will automatically also be made in list2. 
					# instead, use a copy() 
				# or use list() method

			join
				# join multiple lists together, almost the same as append
				# use list1 + list2 to append two lists together
				# use append() to insert one item to the end of the list

		@Dictionary
			# more examples here
				# D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\Programming\java\PythonFundamentals\Dictionary_Package\dictionaryManipulation.py

		@BUILT-IN METHODS
			@sorted()
				# returns a new list while keeping the original iterable intact
				# sorted(iterable, key, reverse)
					# iterable - list, tuple and dictionary
					# if dictionary, convert the iterable by .items() so you can get both key and value
					
					# key - modifications if uses .items() function ; .items() references to key:value result of the dictionary
					# use lambda function e.g.: 
						# lambda x:x[1] - returns values since x[0] is key then x[1] is values
					
					# reverse - boolean type, default false
						# reverse = True - if you want to make descending order


	@Iterable and Iterators
		# iterables are data types like: sequential, mapping and set type
		# iterate through iterators using iter(iterables) method
			# then print using next(var)
			# or just use the conventional loop to iterate over iterables

		# counterpart of iterator
			# https://www.programiz.com/python-programming/generator
			# use 'generators' or 'yield': almost the same as iterators
				# 'generators' and 'iterators' 

			# purpose of generator:
				# unlike return that creates the entire sequence of memeory before returning result
				# generator can only be produced one at a time
				#  

	@Scope
		# uses global keyword to make the local variable accessible as global scope
			'''
				def testFunction ():
					global x
					x = 1
			'''

	@Modules
		# the same to inheritance but different approach
		# same as a code library.
		# file containing a set of functions that can be included in your application

		# use 'import filename' at the top of the code, more examples on your testcodes
		# use 'import filename as newfilename' at the top of the code, more examples on your testcodes
			# same function as 'import filename' but using 'as' renames the filename  
		# use 'from filename import properties_attributes' to get only specific module
			# useful when accessing classes from other files so you dont have to instantiate class as object
			# just call the class as import name then call its attributes

		# use this more simplified: 'from file_name import object_name as object_name_shorthand'
			# use this if you want a shorthand of the object you imported
			# use without 'has' preserves the real attribute name but if you want shorthand then use it

	@User Input
		# 3.6 version
			# variable = input ("Statement")
		
		# 2.7 version
			# variable = raw_input("Statement")

	@Regex
		# import re
		# methods:
			# findall - returns list of matches
			# search - returs a match object 
			# split - returns a list of splitted string at each match
			# sub - replace one or many matches with string

		# group of special meanings
			# metacharacters - symbol : special characters
			# special sequence - symbol : '\'
			# sets - symbol : '[]'

	@PIPI
		# https://docs.python.org/3/installing/index.html
		# makes pip install packages in your home directory instead, which doesn't require any special privileges.
		# comands
			
			# upgrade
				# pip install --upgrade --user some_package
				# pip install --upgrade some_package
				# python -m pip install --upgrade some_package
			# It is possible that pip does not get installed by default. One potential fix is:
				# python -m ensurepip --default-pip

			# install 
				# pip install package_name
				# specific version
					# python -m pip install some_package==version 
				# minimum version
					# python -m pip install "some_package>=version"
			# install specific version of package according to python version
				# py -3.2 -m pip install some_package
			# install with modified list of versions
				# create .txt file that contains lists of requirements
					# requirements.txt
						# lxml >= 4.0
						# beautifulsoup4 >= 2.2.1
						# requests >= 1
				# then run this
					# pip install -r requirements.txt

			# uninstall
				# pip uninstall repository_name
			# uninstall all dependencies entirely
				# pip freeze | xargs pip uninstall -y

			# check 
				# check dependencies installed
					# pip list
				# dependencies and versions
					# pip freeze
				# where python currently used with corresponding directory
					# where python
				# python version
					# go to directory first
						# C:\Users\kurt kevin nebril\AppData\Local\Programs\Python\Python39>
							# pip --version	

@OOP AND FUNCTIONS
	@Classes
		# contructor of objects or blueprint of objects
		# The __init__() function
			# __init__(self, arg1, arg2s) 
			# built in as class constructor
		# self parameter
			# a must parameter to every class methods
			# references to the instance variables of a class
			# self can be changed to any names you like
		# You can delete properties on objects by using the del keyword:
			# del objectName.classProperties
		# You can delete the object
			# del objectName
		# class definitions cannot be empty, if you like to proceed without error
			# use pass

	@Inheritance
		# parent class and child class : same as : super class and sub class
			# Parent class being inherited from, also called BASE class.
			# Child class inherits from another class, also called DERIVED CLASS.
			# super function() derived class inherit all the methods and properties from its base class

			# create child class by not inheriting the properties of base class
				'''
					class DerivedClass (BaseClass):
						pass
				'''

			# create child class and inherit from parent to child class
				# just add the parent class on the parameter
				# make sure to include the super().__init__(self) or ParentClassName.__init__(self) to inherit properties and attributes
				'''
					class ChildClass (ParentClass)
						def __init__(self):
							super().__init__(self)
				'''

			# create child class by overriding the base class properties and attributes 
			# just dont include the super().__init__() function
			'''	
				class DerivedClass (BaseClass):
					def __init__(self):
						...
			'''

			# create child class by overloading the base class properties and attributes 
			'''
				class BaseClass:
					def __init__(self):
						pass

				class DerivedClass (BaseClass):
					def __init__(self, age):
						BaseClass.__init__(self)
						self.age = age
						
				derivedClass = DerivedClass(23)
				print(derivedClass.age)
			'''

@ADVANCED
	@Lambda Function
		# lambda x:x[1]
			''' equivalent to:
					def returnFunction (Element):
						return Element[1]
			'''

	@Function 
		# parameter? arguements?
			# parameters are variables DECLARED inside the parenthesis
			# arguments are values that are SENT to the function when called

		# positional arguments and keyword arguments
			# positional arguments are declared by name only
			# keyword arguments are declared by a name and a value

		# arbitrary arguments *args:
			# positional argument
			# purpose of this is to reduce numbers of declared arguments in parameter
			# just add '*' symbol to specify that arguments are arbitrary

			# args is converted to tuple, you can loop through it

			''' 
				def function1 (*args)
					print("ball is for", args[2])
				function("a", "b", "c')
			''' 
			''' best read with loop
				def addition(*args):
					result = 0
					for i in args:
						result += i
					return result
			'''
			''' string formatting
				def arg_printer(a, b, *args):
					print(f'a is {a}')
					print(f'b is {b}')
					print(f'args are {args}')
				arg_printer(3, 4, 5, 8, 3)
				
				a is 3
				b is 4
				args are (5, 8, 3)
			'''

		# keyword arguments
			''' key = value syntax.
				order of the arguments does not matter.

				example:
					def my_function(child3, child2, child1):
						print (child 1)
					my_function(child1 = "Emil", child2 = "Tobias", child3 = "Linus")
			'''

		# arbitrary keyword arguments **kwargs
			# purpose of this is to reduce numbers of declared arguments in parameter
			# arbitrary keys(keyword) of the argument 
			# '**' add two asterisks before the key name as arbitrary argument
			# function will receive a dictionary of arguments, and can access the items accordingly:
			'''
				def my_function(**kid):
					print("His last name is " + kid["lname"])
				my_function(fname = "Tobias", lname = "Refsnes")
			'''


		# dafault parameter value
			# best utilized as default alternative value for empty arguments
			'''
				def my_country (mycountry = "Philippines")
					print (mycountry)

			'''

		# OTHERS
			# passing data type to the argument - you can pass any data type
			# return - you can return a data
			# pass - pass if 

		# https://towardsdatascience.com/10-examples-to-master-args-and-kwargs-in-python-6f1e8cc30749

	@Lambda
		# small anonymous function
		# can take many arguments but only have one expression
			# lambda arguments : expression
			# example
			# x = lambda a, b : a + b
			# print(x(a,b)

	@First Class Function
		# first-class function treats function as values or variables

	@Higher Order Function
		# if it contains or accepts function as parameter or returns a function as an output
		# returns a function as an output or 'closure'

	@Closure
		# three characteristics:
			# nested function
			# has access to free variable in outer scope
			# returned from enclosing function
		# purpose
			# prevent the use of global values and can also be data hiding
		# counterpart: 'class' : when attributes and methods gets larger

	@Decorators

@FRAMEWORKS AND DEPENDENCIES
	@Virtual Environment
		# dont forget to setup virtual env
			# right click + this pc > 
			# advanced system settings >
			# environment variables >
			# add new on system variables 
				# PYTHON_HOME as variable name
				# C:\Users\kurt kevin nebril\AppData\Local\Programs\Python\Python39 
			
				# PYTHON_SCRIPT as variable name
				# C:\Users\kurt kevin nebril\AppData\Local\Programs\Python\Python39\Scripts
				
				# add new on system variables then add 
					# %PYTHON_HOME%
					# %PYTHON_HOME%
			
			# or directly edit on path then create new
				# C:\Users\kurt kevin nebril\AppData\Local\Programs\Python\Python39
				# C:\Users\kurt kevin nebril\AppData\Local\Programs\Python\Python39\Scripts
		# pipenv vs virtualenv
			# pipenv is a packaging tool for Python application and manages package dependencies and its sub-dependencies. 
			# virtualenv is a tool to create isolated Python environments. If you install virtualenv under python 3.8, virtualenv will by default create virtual environments that are also of version 3.8.

			# https://www.youtube.com/watch?v=HCVaqeQepno (1:19)
				# pipenv - new community standard (pip/virtualenv) extended funcionality in a single app

		# install virtual env
			# pip install virtualenv
		# create virtual environment
			# virtualenv venv_name
		# activation
			# in git bash
				# source venv_folder/Scripts/activate
			# in cmd
				# cd venv_folder/Scripts
				# activate
		# deactivation
			# deactivate

	@beautifulsoup
		# web scraping or web data extraction
			# extract only the relevant data
			# exported to the format that is useful to the user

			# example
				# from amazon get product price and price name then 
					# convert to spreadsheet

				# given one or more urls 
				# advanced scrapers render the entire website that includes javascipt and css
				# the scaper will be choosing the content of the page

		# activate you virtualenv and install these
			# webscarping tool
				# pip install beautifulsoup4
			# parsers (either 1 of these)
				# pip install lxml
				# pip install html5lib
			# request library
				# pip install requests

		# import object beautifulsoup4 and request
			# from bs4 import BeautifulSoup
			# import request

		# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
		# BeautifulSoup object parameters
			# BeautifulSoup(html_file, 'TheParserDependency')

		# Methods:
			# .prettify()
			# .find_all() - returns list, tag
			# .select_one() - returns Tag
			# .select() - returns list
			# .find() - 

			# .find_parents
			# .nextsiblings
			# .next_element
			# .parent
			# .find(name, attr, recursive, string, **kwargs)
				# name - tag name
				# attr - selector (id, or class)
				# recursive - childs
				# string - specific string
				# kwargs - other values

			# .get_text() - get the value of the attribute
			# .string - gets the value of the attribute
			# .name - gets the attribute name

			# .children
			# .contents
			# .descendants

		# checking the type of elements:
			# https://stackoverflow.com/questions/41758715/how-to-check-whether-or-not-a-iterating-variable-navigablestring-or-tag-type
			# https://www.programcreek.com/python/example/61531/bs4.NavigableString
			'''
				from bs4 import Tag, NavigableString

				if (type(Tag) == bs4.element.Tag): # if no import
				if (type(Tag) == bs4.element.NavigableString): # if no import
				if (type(Tag) == Tag):
				if (type(Tag) == NavigableString):
			'''
			'''
				from bs4 import Tag, NavigableString

				# better to use type() for checking but use this as secondary
				if isinstance(tag, Tag):
				if isinstance(tag.string, NavigableString):
				if isinstance(tag, bs4.element.Tag) # if no import
				if isinstance(tag, bs4, element.NavigableString) # if no import
			'''

		# get the html file
			# with open("file_path/index.html") as html_file:
				# soup = Beaituf...

		# more to learn
			# if scraping with large websites, better use public api because it deals up data in more efficient way 

		# warning: you might slow down servers after making tons of request, 
			# some sites monitor and blocks you out if gets a lot of requests

@Learn this to improve
	Learn more about data structure
		# https://www.tutorialspoint.com/questions/category/data-structure

	Learn more about decorators; first order function on python training
		# 

	Learn more about proper naming convention
		# https://visualgit.readthedocs.io/en/latest/pages/naming_convention.html
		# generator
			# https://www.fantasynamegenerators.com/medieval-names.php
			# http://sph.mn/dynamic/svn?words=module

		# Directory naming convention
			# project - PythonFundamentalProject (Pascal Case started : name then Project)
				# package  - test_package (snake case started : name then package)
					# file - test_mainpulation.py (snake case : name.extention)

						# class - TestClass (Pascal Case: name first and the Class)
							# objects - testClass (camel Case : object name)
							# function or methods = setName (camel Case : start with set, get, crud, oprations or verb then method name)
								# instance variables = varFname (camel Case : start var then variable name)
								# global variables = varOptions ()

	Java vs Python
		# https://www.edureka.co/blog/java-vs-python/

@Questions
	@build system Environment
	# view location of packages folder
		# preferences > browse packages
		# C:\Users\kurt kevin nebril\AppData\Roaming\Sublime Text 3\Packages\Users\

	# create python build system
		# Tools > build system > new build system
		# copy this from sublime:
			# https://www.sublimetext.com/docs/build_systems.html
			'''
				{
					"cmd": ["python", "$file"],
					"selector": "source.python",
					"file_regex": "^\\s*File \"(...*?)\", line ([0-9]*)"
				}
			'''
			
		# get python directory using cmd then type command : 'where python'

		# then replace this:	
			'''
				{
					"cmd": ["C:/Python/Python38/python.exe", "$file"],
					"selector": "source.python",
					"file_regex": "^\\s*File \"(...*?)\", line ([0-9]*)"
				}
			'''

		# add '-u' to allow delay execution 
		# then replace this:	
			'''
				{
					"cmd": ["C:/Python/Python38/python.exe", "-u" "$file"],
					"selector": "source.python",
					"file_regex": "^\\s*File \"(...*?)\", line ([0-9]*)"
				}
			'''

	how to clear the console
		type this command to console: 
			# import os
			# clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')
			# clearConsole()

	directory sortation
		# https://thispointer.com/python-get-list-of-files-in-directory-sorted-by-date-and-time/

	Difference between method overloading and method overriding
		# method overloading : same method name in the same class
			# but different in method signature (or parameter numbers, type, etc)
		# method overriding : same method name in the same class 
			# and have the same method signature
		# purpose: to increase readability of the program.
			# while having existing naming convention but different behaviours

		# image
			# D:\Controlling System\MemoBase(Core)\[1]NEW ARSENAL\Programming\python\method overriding.PNG

		# method overriding error:
			# same method name and signature in the same class will produce an error
				# try to 

	What is properties and attributes?
		# properties are methods
		# attributes are variable

	printing without newline
		# use (end='') argument to explicitly mention the string that should be appended at the end of the line. 
		# print("* ", end='')
		# https://www.afternerd.com/blog/how-to-print-without-a-newline-in-python/

	Sorted
		# bubble sort
			# https://stackabuse.com/bubble-sort-in-python/
		# sort() vs sorted()
			# https://towardsdatascience.com/sorting-a-dictionary-in-python-4280451e1637
			# sort() can only be used in list while sorted can work with any iterable such as: list, tuple, dict
			# sort() changes the original list while sorted() returns new list while keeping the original iterable intact
	
	Shortcuts
		# unpacking
		# list comprehension
		# lambda

	Unpacking a return value
		'''	
			def unpack (myData):
				return myData

			myData = (1,2,3,4,5)
			print(*unpack(myData)) # just add '*' on the called function
		'''

	Ways of inheriting properties from other files
		# use the conventional inheritance approach (easier to implement)
		# use the import (more flexible and have varieties of ways)

	What is the difference between compiler dependent and interpreter dependent programming language?

	differenctiate python: map(), filter(), reduce()

# good reference:
	# https://www.programiz.com/python-programming/closure
	# "write a journal and log; someday when you become great it will become a historical reference"