resources:
	// https://stackoverflow.com/questions/62479441/java-how-to-check-if-a-13-digit-isbn-number-is-valid
		* isbn 13
	// https://www.youtube.com/watch?v=Esoi0M8544U
		* good resource	
	// https://www.instructables.com/How-to-verify-a-ISBN/
		* isbn instructions
	// http://www.hahnlibrary.net/libraries/isbncalc.html
		* the calculator sample
	// https://www.geeksforgeeks.org/program-check-isbn/
		* sample program
	// https://www.youtube.com/watch?v=Esoi0M8544U
		* good resource

	// https://www.topshelfcomix.com/catalog/isbn-list
		* list of valid isbn 13

	// list of valid isbn 13 and isbn 10 
		* 978-1-60309-025-4 
		* 007462542X  