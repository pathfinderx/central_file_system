// * oop fundamental [3 4] 
// * string manipulation advanced [5 6]
// * array manipulation advanced [7 8]

// * the leak [9 10 11 12]
// 	* isbn
// 	* cipher decipher the words
// 	* number to words converter

// * file handling
// 	* declarations [13 14]
// 	* mini app [15 16]

// * mini programs [18 19 20 17]
	// * array manipulation
	// * string manipulation
	// * integer manipulation
	// * convertions

	// * isbn
	// * cipher decipher the words
	// * number to words converter

	// * crud app file handling > database (jdbc)

// * extreme test [21 22 23 24]
// 	* fundamentals
// 	* mini programs
// 	* the leak
// 	* file handling

	* oop for pillars [21]
	* string manipulation [21]
	* int manipulation [21]
	* array manipulation [21] 

	/ regex and validation [20] [needs mastery]
	* sortation [20] [research phase]
	/ conversions [20] [needs mastery]

	* the leak
		/ isbn [23]
		* cipher / decipher [21] [research]
		/ number systems [21] [research phase]
		* number to words [21] [start]

	* file handling and database [22] [start]
	* extreme test to all [23 24] (2 days) 
		6 hours straight a day, 5 mins break every 30 mins

	* extreme test
		* array manipulation
		* string manipulation
		* integer manipulation
		* convertions

		* isbn
		* cipher decipher the words
		* number to words converter

		* crud app file handling > database (jdbc)

	* specific test:
		* int manipulation
			* decimal manipulation
			* date manipulation how to format
			* how to convert an integer into a negative number
		* string manipulation
			* charAt()
			* getNumericalValue()
			* type cast
			* '0'
		* regex
		* the leak
			* isbn 10
			* isbn 13

		* file handling and database