Resources
	news
		// https://www.facebook.com/civilservicegovph/posts/are-you-an-it-specialist-learn-about-your-option-to-apply-for-the-electronic-dat/2634533366817145/

		// https://www.facebook.com/120965482898573/videos/3734362063323700
		// http://csc.gov.ph/new-updates/1567-csc-adds-two-programming-languages-under-edpse-grant.html
		// https://www.facebook.com/DICT.mc1/photos/pcb.598066137029422/598065950362774/
		// https://www.facebook.com/DICTCaraga/posts/2082823251849039

	resources
		// https://www.w3schools.com/java/default.asp
		// https://ceng.eskisehir.edu.tr/emrekacmaz/bil158/Algorithms3.pdf
		// file:///D:/Controlling%20System/MemoBase(Core)/[1]NEW%20ARSENAL/Programming/[1]dict/Algorithms3.pdf
	
		// else if pseudocode and flowchart 
			// http://larp.marcolavoie.ca/en/DocHTM/9900.html
		// how to use flashcards effectively
			// https://www.youtube.com/watch?v=mzCEJVtED0U

		// returning a method
			// https://docs.oracle.com/javase/tutorial/java/javaOO/returnvalue.htm
		// java tpoint
			// https://www.javatpoint.com/factorial-program-in-java

-------------------------------
	quizzer
		// https://www.journaldev.com/370/java-programming-interview-questions
		// https://quizizz.com/admin/private

	stimulators
		// www.keybr.com
		// https://pomofocus.io/
		// https://www.javatpoint.com/java-programs

-------------------------------
Fundamentals
	array manipulation
		// https://alvinalexander.com/source-code/java/how-create-populate-static-list-arraylist-linkedlist-syntax-in-java/
			- how to populate array list

	string manipulation
		// https://www.javatpoint.com/how-to-read-character-in-java
			* read character in java
		// https://stackoverflow.com/questions/12438336/why-we-use-0-beside-charati
			* forcing the char to convert to int
		// https://www.geeksforgeeks.org/different-ways-for-integer-to-string-conversions-in-java/
			* different ways: integer to string conversions 
		// https://www.tutorialspoint.com/Java-String-substring-Method-example#:~:text=The%20substring(int%20beginIndex%2C%20int,the%20substring%20is%20endIndex%2DbeginIndex.
			* substring method
		// https://www.freecodecamp.org/news/c-ternary-operator/
			* ternay operation

	regex
		// https://www.geeksforgeeks.org/how-to-validate-a-username-using-regular-expressions-in-java/

	ternary operations 
		// https://www.freecodecamp.org/news/c-ternary-operator/
			* ternary operation sample to reduce if else blocks
The Leak
	isbn
		// https://www.instructables.com/How-to-verify-a-ISBN/
			* isbn instructions
		// http://www.hahnlibrary.net/libraries/isbncalc.html
			* the calculator sample
		// https://www.geeksforgeeks.org/program-check-isbn/
			* sample program
		// https://stackoverflow.com/questions/62479441/java-how-to-check-if-a-13-digit-isbn-number-is-valid
			* isbn 13
		// https://www.youtube.com/watch?v=Esoi0M8544U
			* good resource	


