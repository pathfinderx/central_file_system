Exam Requirements
	according to: 'Lourdesita Deresco' 09498017700 (smart) & 09179626315 (globe)
		Coverage
			written programming
			theory
			flowchart


		Bring 
			2 hours exam
			ballpen
			mask and faceshield

		Where and When
			DICT Agusan del Norte Provincial Office, J. Rosales Ave., Butuan City (near City Comelec Office)
			at 8AM (30mins before the said schedule) on 
			February 19, 2021 (Friday)

outline course based on 'filipinoEinstein'
	* program stimulation
	* number system
	* data structrure
	* object oriented programming
	* networking concept
	* file access methods
	* database concept

Extreme Test Procedures and Outline actual
	- 'endurance test 12 to 12 hours exam'

	* fundamentals theoretical
	* program stimulation: fundamentals (data structure, methods, classes(oop)) && applied fundamentals
	* word problem
		* make pseudocode with explanation
		* make flowchart with explanation
		* program stimulation
			* written
			* hands on
				fundamentals = https://www.w3schools.com/java/exercise.asp?filename=exercise_syntax1
	* pyramids
	* debugging
	* network concept
	* database concept

Extreme Test V2 (practical)
	* make psuedocode with explanation
	* stimulate program with timer
	* explanation

The Leak
	* isbn validator
	* cipher / decipher the words
	* number systems converter
	* number to words converter