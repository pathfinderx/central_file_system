package tester;

public class TestClass {
	public static void main (String args []) {
		test();
	}

	public static void test () {
		int rows = 5;
		int star = 0;
		int counter = 0;

		for (int i = 1; i <= rows; i++) {
			star = 0;
			for (int spaces =  1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}

			while (star != 2 * i - 1) {
				int theStackLimit = 2 * i - 1;
				int printLimiter = theStackLimit / 2;

				if (star <= theStackLimit) {
					counter++;
					System.err.print(counter + " ");
				} 
				// else if (star > theStackLimit) {
				// 	counter--;
				// 	System.out.print(counter + " ");
				// }
				star++;
			}

			System.out.println();
		}
	}
}