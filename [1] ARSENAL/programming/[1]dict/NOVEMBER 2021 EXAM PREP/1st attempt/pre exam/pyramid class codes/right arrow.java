package tester;

public class TestClass {
	public static void main (String args []) {
		test();
	}

	public static void test () {
		int rows = 5;

		for (int i = rows; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print("  ");
			}

			for (int x = 1; x <= rows - i + 1; x++) {
				System.err.print("* ");
			}

			System.out.println();
		}

		for (int i = 1; i <= rows; i++) {
			if (i > 1) {
				for (int j = 1; j <= i; j++) {
					System.out.print("  ");
				}
				for (int x = rows; x >= i; x--) {
					System.err.print("* "); 
				}
				System.out.println();
			}
		}
	}
}