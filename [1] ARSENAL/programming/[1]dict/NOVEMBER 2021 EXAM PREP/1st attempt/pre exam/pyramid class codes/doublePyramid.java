package tester;

public class TestClass {
	public static void main (String args []) {
		delayExecution(500)
		
	}

	public static void test () {
		int rows = 5;
		int star = 0;

		for (int i = rows; i >= 1; i--) {
			star = 0;
			for (int spaces = 1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}

			while (star != 2 * i - 1) {
				System.out.print("* ");
				star++;
			}

			System.out.println();
		}

		for (int i = 1; i <= rows; i++) {
			star = 0;
			for (int spaces = 1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}

			while (star != 2 * i - 1) {
				System.out.print("* ");
				star++;
			}

			System.out.println();
		}
	}

	public static void delayExecution (int millisecond) {
		try {
			test();
			Thread.sleep(millisecond);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}