package tester;

public class TestClass {
	public static void main (String args []) {
		skipInsideSpacesPyramid();
	}
	
	public static void skipInsideSpacesPyramid () { // 
		int rows = 6;
		int star = 0;
		
		for (int i = 1; i <= rows; i++) {
			star = 0;
			for (int spaces = 1; spaces <= rows - i; spaces++) {
				System.out.print("  ");
			}
			
			while (star != 2 * i - 1) {
				if (star % 2 != 0) { // check if even
					System.out.print("  ");
				} else { // if odd
					if (star == 0 || star + 1 == 2 * i - 1) { // check the last
						System.out.print("1 ");
					} else { // if not last (warning!! this program is static; for research purpose only (experimental)
						if (star == ((2 * i - 1) / 2) && i == 5) { // identify the i = 4 and if star = 5 
							System.out.print((i - 2) * 2 + " "); // print 6
						} else if (star == ((2 * i - 1) / 2) - 1 && i == 6 || star == ((2 * i - 1) / 2) + 1 && i == 6) { // identify if star = 5 and star = 7
							System.err.print("10 ");
						} else {
							System.out.print(i - 1 + " ");
						}
					}
				}
				star++;
			}
			
			System.out.println();
		}
	}
}