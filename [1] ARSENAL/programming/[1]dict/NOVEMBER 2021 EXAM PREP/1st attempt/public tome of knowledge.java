// BASIC THEORETICAL
// 	* Java is a popular programming language, created in 1995.
// 	* It is owned by Oracle, and more than 3 billion devices run Java.
// 	* It is used for:
// 		Mobile applications (specially Android apps)
// 		Desktop applications
// 		Web applications
// 		Web servers and application servers
// 		Games
// 		Database connection
// 	* Every line of code that runs in Java must be inside a class
// 	* The main() method is required and you will see it in every Java program:
// 	* Any code inside the main() method will be executed.

---------------------------
FUNDAMENTALS
	Variables
		String - stores text, such as "Hello". String values are surrounded by double quotes
		int - stores integers (whole numbers), without decimals, such as 123 or -123
		float - stores floating point numbers, with decimals, such as 19.99 or -19.99
		char - stores single characters, such as 'a' or 'B'. Char values are surrounded by single quotes
		boolean - stores values with two states: true or false

	Display Variables
		* The println() method is often used to display variables.
		* To combine both text and a variable, use the + character:
		* You can also use the + character to add a variable to another variable:

	The general rules for constructing names for variables (unique identifiers) are:
		Names can contain letters, digits, underscores, and dollar signs
		Names must begin with a letter
		Names should start with a lowercase letter and it cannot contain whitespace
		Names can also begin with $ and _ (but we will not use it in this tutorial)
		Names are case sensitive ("myVar" and "myvar" are different variables)
		Reserved words (like Java keywords, such as int or boolean) cannot be used as names

	Primitive Data Types
		A primitive data type specifies the size and type of variable values, and it has no additional methods.

		Primitive number types are divided into two groups:
			Integer types stores whole numbers, positive or negative (such as 123 or -456), without decimals. Valid types are byte, short, int and long. Which type you should use, depends on the numeric value.
			Floating point types represents numbers with a fractional part, containing one or more decimals. There are two types: float and double.
		Byte
			The byte data type can store whole numbers from -128 to 127. This can be used instead of int or other integer types to save memory when you are certain that the value will be within -128 and 127:
		Short
			The short data type can store whole numbers from -32768 to 32767:
		Int
			The int data type can store whole numbers from -2147483648 to 2147483647. In general, and in our tutorial, the int data type is the preferred data type when we create variables with a numeric value.
		Long
			The long data type can store whole numbers from -9223372036854775808 to 9223372036854775807. This is used when int is not large enough to store the value. Note that you should end the value with an "L":

		Floating Point Types
			You should use a floating point type whenever you need a number with a decimal, such as 9.99 or 3.14515.

			Float
				The float data type can store fractional numbers from 3.4e−038 to 3.4e+038. Note that you should end the value with an "f":
			Double
				The double data type can store fractional numbers from 1.7e−308 to 1.7e+308. Note that you should end the value with a "d":
				A floating point number can also be a scientific number with an "e" to indicate the power of 10:
			Use float or double?
				The precision of a floating point value indicates how many digits the value can have after the decimal point. The precision of float is only six or seven decimal digits, while double variables have a precision of about 15 digits. Therefore it is safer to use double for most calculations.
			Booleans
				A boolean data type is declared with the boolean keyword and can only take the values true or false:
				Boolean values are mostly used for conditional testing, which you will learn more about in a later chapter.
			Characters
				The char data type is used to store a single character. The character must be surrounded by single quotes, like 'A' or 'c':
			Strings
				The String data type is used to store a sequence of characters (text). String values must be surrounded by double quotes:s

	Non Primitive Data types
		String, Arrays, Classes, Interface

	Syntax
		* comment
			// single line
			/* 
				multi line
			*/

--------------------------
// FLOWCHART AND PSEUDOCODE
// 	* what is algorithm?
// 		ans: finite set of instruction to solve a problem
// 	* what is pseudocode?
// 		ans: finite set of instruction in a text form
// 		ans: without specific language
// 	* what is flowchart?
// 		ans: finite set of instruction in a diagram from
	
// 	* important to remember in an algorithm?
// 		ans: correct order
// 	* algorithm can be used?
// 		ans: solve problems
// 	* algorithms can be designed using 
// 		ans: flowchart and pseudocode

// 	* symbol to connect flowchart
// 		ans: circle
// 	* symbol for process?
// 		ans: rectangle
// 	* symbol for start and end
// 		ans: oval
// 	* symbol for input and output
// 		ans: parallelogram
// 	* symbol for decision in a flowchart?
// 		ans: diamond
// 	* three types of control structure
// 		ans: sequence, selection, repetition
// 	* flowchart can only have how many exit arrows?
// 		ans: 2 exit arrows
// 	* data sent from the computer is called 
// 		ans: output

// 	* integer contains what?
// 		ans: negative and positive number
// 	* string is close what qoute?
// 		ans: double qoute
// 	* character is enclosed with what qoute
// 		ans: single qoute

// 	* Used to ask a question that can be answered in a binary format (Yes/No,
// 	True/False) 
// 		ans: Decision
// 	* Indicates any type of internal operation inside the Processor or Memory or to initialize
// 		ans: Process
// 	* Used for any Input / Output (I/O) operation. Indicates that the computer is to obtain data
// 	or output results 
// 		ans: input/output 
// 	* Allows the flowchart to be drawn without intersecting lines or without a reverse flow.
// 		ans: Connector
// 	* Used to invoke a subroutine or an interrupt program.
// 		ans: Predefined Process
// 	* Indicates the starting or ending of the program, process, or interrupt program.
// 		ans: Terminal
// 	* Shows direction of flow. 
// 		ans: Flow Lines

// 	* purpose of control structure?
// 		ans: to have better algorithm by limiting the control structure
// 	* what control structure in a case where the steps in an algorithm are constructed in such a way that, no condition step is required?
// 		ans: sequence
// 	* what control sturcture in a case where in the algorithm, one has to make a choice of two alternatives by making decision depending on a given condition?
// 		ans: selection
// 	* other term for selection structure?
// 		ans: case selection structure
// 	*  occasions when we need to extend the conditions that are to be tested. Often there are conditions to be linked. 
// 		ans: Compound Logical Operators
// 	* Repeating the if … else statements a number of times can be somewhat confusing. An
// 	alternative method provided in a number of languages is to use a selector determined by
// 	the alternative conditions that are needed. In our pseudo-code, this will called a
// 		ans: case statement
// 	* A third control structure causes the certain steps to be repeated. What is this called?
// 		ans: repetition
// 	* The Repetition structure can be implemented using
// 		ans: 
// 			Repeat Until Loop or Do While Loop
// 			The While Loop
// 			The For Loop
// 	* Any program instruction that repeats some statement or sequence of statements a number of times is called an
// 		ans: iteration or a loop

// 	* logical operators in pseudocode
// 		what logical operator is this? <> 
// 			ans: is not equal to

// 	* keywords for pseudocode
// 		Use variables: _ of type integer = initialize
// 		Display = print
// 		Accept _ = input
// 		End Program = end
// 		If - Then - Else - Elif- EndIf

--------------------------
OBJECT ORIENTED PROGRAMMING
	trivial program - program that does not impact the business industry
	4 pillars of oop :
		Encapsulation
			- reduce parameters?
		Abstraction
			- some properties are hidden
			- irrelevant details are ignored
			- a model of complex system that includes only essential details
				- fundamental way of managing complexity
			- includes essential details relative to the perspective of the viewer
		Inheritance
			- inherit properties
		Polymorphism
			- reduce switch cases

	Classes
		- blueprints or considered as framework
		- classes defines the structure of the objects
		- has two types of variables:
			* class variables
				protected static final int MINYEAR = 1234; // this is class variable because it is direct to class and it is static
			* instance variable
				public Date () {
					int date = 0; // this is the instance variable because it is an attribute of an object
				}
		- 

	Object
		- properties
		- basic run-time entities in oop design
		- variabes (data) and actions (methods) defines the behavior of the objects

	-------
	quick facts
		* 'static' - a single copy of static variable is maintained through the whole object of a class
			- static variables can be used to maintain information to the entire class
			- static variables should be the standard procedures to for every constants
			- static variables can only be intialized once at the start of the execution
		* use capital letters when naming contants
	-------
	execute this:
		* object instantiation
		* 

	-------
	resources:
		// https://books.google.com.ph/books?hl=en&lr=&id=UPhrjggcTwwC&oi=fnd&pg=PA242&dq=oop+java+documentation&ots=mObXXiGzBs&sig=ZJtlE5OrSkThqABmEnbJmLYiarA&redir_esc=y#v=onepage&q&f=false

--------------------------
Java Program Stimulation
	User Input - procedures
		import java.util.Scanner;
		Scanner name = new Scanner (System.in);
		String value = nextInt();

	Strings Methods
		length() - length of the string
		toUpperCase() - uppercase
		toLowerCase() - lowercase
		indexOf("string") - find the index of (returns the first index of the occurence)

	Array vs ArrayList
		difference
			array - fixed size and cannot be changed
			arrayList - dynamic size; requires import java.util.ArrayList;
		decleration
			array - String [] array_name = {};
			arrayList - ArrayList<String> array_name = new ArrayList<String>();
				- public decleration
					- public static ArrayList<Integer> arrayName;
					- then instantiate inside method
						- arrayName = new ArrayList<Integer> 
		read the size
			array - array_name.length
			arrayList - array_name.size()
		display items
			array - array_name[0]
			arrayList - array_name.get(0) - specific
				- array_name - call the array_name if you want to display all values in arrayList
		append
			array - n/a
			araryList - array_name.add()
		change items
			arrayList - array_name.set(index, Value)
		remove item
			arrayList - array_name.remove(index)

		shortcut - sgasr = size(index), get(index), add(value), set(index, value), remove(index)

	Methods
		A method is a block of code which only runs when it is called.
		You can pass data, known as parameters, into a method.
		Methods are used to perform certain actions, and they are also known as functions.
		Why use methods? To reuse code: define the code once, and use it many times.
		what is recursion?
			- calling by the function itself to chunk down complex data
          
	Classes
		public static void main (String args []) {} - java class requires main method to execute
		public static void myMethod () {} - does not return something (if return is used > compile time error )
		public static int myMethod () {} - requires return
		import java.util.* - all util imports
		static - means that the method belongs to the Main class and not an object of the Main class. 
		void - means that this method does not have a return value.

	Others
		how to declare main method
		how to declare method
			how to declare oop method and initialize to main

		how to import, initialize and use scanner?
		how to use switch case?
		remember the scope
		how to set public variable
			* public static String variableName;

--------------------------
SHORTCUT KEYS

	* remove multiple lines tab in java ide
		* shift + tab

--------------------------
TESTING
	// test execution speed of program
		final long startTime = System.currentTimeMillis();
		final long endTime = System.currentTimeMillis();
		System.out.println("Total execution time: " + (endTime - startTime));

	// delay the execution speed of the program
	public static void delayExecution (int secondsToSleep) {
		try {
		    Thread.sleep(secondsToSleep);
		} catch (InterruptedException ie) {
		    Thread.currentThread().interrupt();
		}
	}

--------------------------
THE LEAK

book isbn validator , cipher/decipher the words, number systems converter, number to words converter
string manipulation

--------------------------
	Words of the Enlightened
		* dont let your knowledge drift away