@Data Type Conversion
	structure (loop, handlers, switch case) and loop control
			00 12 00
			00 03 00
			00 03 23
			00 03 29
			00 03 15

			00 05 53
			00 03 40
			00 02 51
			00 02 30
			00 02 30

	String
		toChar
			00 06 47
			00 04 14
			00 03 36
			00 03 13
		toInt
			00 03 27
			00 02 22
			00 02 11
			
		toDouble
			00 05 48

	11 09 21
		String > char, int, double
			00 15 00
		Int > string, double
			00 11 15
		Double > string, int
			00 09 18
		Char > string > int (as decimal)
			00 11 19

		isbn 10
			simple
				00 10 46
			strong
				00 16 28

		file handling
			File Information
				00 03 48
				00 02 04
				00 01 42
			Delete
				00 04 25
				00 03 51
			Mkdir (Replace)
				00 04 26
			GetFiles Files
				00 03 27
			GetFiles Directory
				00 02 25
			MoveFiles
				00 14 46

		regex fundamentals
			meta characters
				* 58%
				* 86%
				* 86%
				* 100%
				* 100%
			quantifiers
				* 100%
				* 100%
			chracter classes
				* 100%
				* 100%

	11 12 21
		/ isbn10
		/ String > char, int, double
		/ Int > string, double

	11 13 21
		/ Double > string, int
		/ Char > string > int (as decimal)
		* file handling
			* basic
				/ deletefile
				/ delete directory
				/ getfile information after create
				/ getfiles
					/ filename
					/ location
				/ mkdir
				/ mistakes to remember: 
					/ print.close()

			/ advanced
				/ move files

	11 15 21
		/ netbeans
			/ isbn10
			/ file handling

		* regex
		* number systems conversion
		* krissy
		* cipher