@Checkers
	a.getClass().getName() - // check for type
	file.getName() - // can be used to get name
	regex
	value.contains("\t") - // check if contains tab

	Datatype Checkers if empty
		data_type.isEmpty()

	Character input is empty
		char_type != ' '

@Reminders
	String.valueOf(x.charAt(i)) - // convert character to string
	charAt(i) - '0' - // forcing the character to return the string value

@File Handling
	checkers:
		// exists() - check file if exists
		// source.list().length()

	useful methods for validation
		// while (sc.hasNextLIne())
		// sc.nextLine()
		// replaceAll()
		// substring
		// indexOf

	@File Information Access Methods
		File file = new File (location)
			file.exists()
			file.getName()
			file.getAbsolutePath()
			file.canRead()
			file.canWrite()
			file.lenth()

	@Reader Types
		FileReader
			int read() - // reads single character
			int read (char [] cbuf) - // read characters into array
		BufferedReader
			int read() - // used to read single character
			String readLine() - // read string

	@Writer Types
		FileWriter
			void write (String text) - // used to write the string into FileWriter
			void write (char c) - // used to write the char into FileWriter

		PrintWriter
			void println(boolean x) - // print boolean vaue
			void println(char[] x) - // print array of characters
			void println(x) - // print integer
			PrintWriter append(char c) // append specified character to the writer
			void print(Object obj) - // print object

		BufferedWriter
			newLine () - // used to add a new line by writing a line separator
			write () - 	// used to write a single character

	@Reading content of the directory
		File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
			// read the contents with path
				File [] paths = source.listFiles();
				for (File contents : paths) {
					System.out.println(contents);
				}
			// read the contents file name only
				String [] paths = source.list();
				for (String contents : paths) {
					System.out.println(contents);
				}

	@Deleting file or directory
		// delete file
			File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\readThis.txt");
			if (source.delete()) {
				System.out.println(source.getName() + " successfully deleted");
			}
		// delete directory
			File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn") // exclude file
			if (source.delete()) {
				System.outl.println(source.getAbsolutePath() + " successfully deleted");
			}

	@Mkdir
		File source = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
		if (source.mkdir()) {
			Systme.out.println(source.getAbsolutePath() + " successfully created directory");
		}

	@Moving File
		// TWO METHODS
			* source.replaceTo(target)
			source.replaceTo(new File (target))

@Int Manipulation
	.floor() - // gives the largest integer that is less than or equal to the argument. 
		-100.675 = -101.0 
		-90 = -90.0
		84.6 = 84.0
		0.45 = 0.0
	.ceil() - // gives the smallest integer that is greater than or equal to the argument. 
		-100.675 = 100.0 
		-90 = -90.0 
		84.6 = 85.0
		0.45 = 1.0
	.round() - // returns the closed int or long as per argument 
		84.6 = 85 
		0.45 = 0
	.abs() - // returns the absolute value of the argument or returns a positive number 
		-45 = 45

	// randomize
	Random random = new Random();
		int integers = random.nextInt(50);
		int doubles = random.nextDouble();
		int longs = random.nextLong()
		int booleans = random.nextBoolean();

	// how to cut into 2 digits number
			* use BigDecimal

	// extract last value of int (only works in integer not in double)
		// % 10 // get the last digit
		// / 10 // divide 10 removes the last digit 

	// extract last value of double
		// use string manipulation

@Array Manipulation
	Collections
		Collections.sort(arrayList_name) // ascending order
		Collections.reverse(arrayList_name) // descending order
		Collections.shuffle(arrayList_name) // randomize content

	ArrayList
		static delaration
			public static ArrayList <Integer>;
			then: arrayName = new ArrayList<Integer>

		declaration as object
			ArrayList <Integer> arrayName = new ArrayList <Integer> ();

		populated declaration
			ArrayList <Integer> arrayName = new ArrayList <Integer> (Arrays.asList("a", "b", "c"))
			ArrayList <Integer> arrayName = new ArrayList <Integer> () {{ 
				add("a"); 
				add("b");
				add(0); }};

		declaring as return type for function
			public static ArrayList <Integer> container () {
				return container
			}

		methods
			arrayList[0] - access
			.size() - check size
			.indexOf(value) - returns the index of the value; argument should be the value 
			.get(index) - access

			.add(value) - add
			.set(index, value) - change items
			.remove(index) - remove item
			
			.isEmpty() - returns true if has no value
			.clear() - wipe out all values

			SGASRII - shortcut (size, get, add, set, remove, isEmpty, indexOf)
	Array
		declaration
			* char [] ch = {val1, val2}
			/*
				char [] ch;
				myString.toCharArray();
			*/

		access
			char[0]
			/*
				foreach ()
			*/

@Data Type Conversion
		* string - character // [a-zA-Z0-9]+
		* string - int // [0-9]{1,9}
		* string - double - int
			// [-]{0,1}[0-9]+
			// [-]{0,1}[0-9]{1,9}\\.[0-9]{1,9}
			// Math.round(Double.parseDouble(input))
		* string - double // [-]{0,1}[0-9]{1,9}\\.[0-9]{0,9}||[0-9]+

		* int - string // Integer.toString(input)
		* int - double // Double.parseDouble(Integer.toString(input))

		* double - string // Double.toString(sc.nextDouble())
		* double - int // Math.round(sc.nextDouble())

		* char - string // String.valueOf(charInput)
		* char - int // (int) input || Integer.parseInt(Integer.toString(input))

	Double TestCase
		123
		-123
		123.0
		0.4
		123.42
		--123.42

@Number System Conversion
	* built-in
		character to decimal - (int) characterVal
		character to octal - Integer.toOctalString(chracterVal)
		character to hexadecimal - Integer.toHexString(characterVal)
		character to binary - Integer.toBinaryString(characterVal)

		number system to character - (char) numberSystems
	* legacy

@Regex
	java.util.regex

	matcher class methods
		boolean matches() // test whether the expression matches the pattern
		boolean find() // find the expression that matches the pattern
		boolean find(int start) // find the expression that matches the pattern from the given start number
		String group() // returns the matches subsequence
		int start() //returns the starting index of them matches subsequence
		int end() // returns the total number of the matches subsquence

@Netbeans configuration
	Change appearance
		Tools > Options > Appearance > Look and feel

	Change auto suggestion
		Tools > Options > Editor > code completion

	Change Tabs 
		Tools > Option > Editor > Formatting

@Newly learned
	Random random = new Random
		* random.nextInt(50)
		* min + (max - min) * random.nextDouble()

	* BigDecimal bd = new BigDecimal()
	* bd.
	* bd.doubleValuefor 

@String Builder
	instantiate
		StringBuilder sb = new StringBuilder(string);

	methods
		.append(String s)
		.insert(int offset, String s)
			insert(int, char)
			insert(int, booean)
			insert(int, int)
			insert(int, float)
			insert(int, double)
		.replace(int startindex, int endindex, String s)
		.delete(int startindex, int endindex)
		.reverse()
		.capacity()
		.ensureCapacity(int minimumCapacity)
		.charAt(int index)
		.length()
		.substring(int beginindex)
			.substring(int beginindex, int endindexs)

		air dr cec ls

	samples
