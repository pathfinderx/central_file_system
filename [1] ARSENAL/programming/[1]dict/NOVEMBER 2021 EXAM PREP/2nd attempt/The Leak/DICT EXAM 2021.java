guidelines
	* 3 major coverages
		* ISBN
			* given numbers are from notepad and you have to read it using file handling (note: use regex to filter the numbers (neglect dashes, neglect tabs, unnecessary contents)
				* example values inside the textfile:
					1 2 3 4 5 6 7 8 9 x
						0-4314-8110-5
					0-1991-2443-4
			* then process if the value is valid
			* then extract all the values then write to textfile named: specificnameNOtepad, write "valid" or "not valid" right side of the isbn number, write reporting 
				* example values inside the notepad to be printed:
					0-4314-8110-5 is valid
					0-1991-2443-4 is not valid

					total numbers of isbn numbers: 10
					total number of valid: 2
					total number of invalid: 8

		* KRISSY NUMBER
			* extract numbers from the textfile then process it like this:
				z = decimal
				s = z to octal
				c = s sort ascending
				can only do this a couple of times until s is already sorted
			* write the result to new textfile

		* DECYPHER THE WORD
			* get the value in the textfile (note: use substring? or use something to extract line by line)
			* decipher the ciphertext extracted from the source text file
			* process the result then print to the new text file line by line

		* fields to master:
			* regex
				* neglect unnecessary contents or characters
			* file handling
				* read (line by line then process) use split()
				* write (line by line) use split()