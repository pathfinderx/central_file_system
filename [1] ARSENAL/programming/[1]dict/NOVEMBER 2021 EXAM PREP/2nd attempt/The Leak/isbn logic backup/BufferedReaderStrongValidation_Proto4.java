package isbn10;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class isbn10_reader_writer_validation {
	public static void main (String args []) {
		execute("C:\\Users\\kurt kevin nebril\\Desktop\\readThis.txt");
	}
	
	public static void execute (String link) {
		try { // read
			FileReader file = new FileReader (link);
			Scanner sc = new Scanner (file);
			while (sc.hasNextLine()) {
				delay();
				System.out.println(getSubstring(sc.nextLine())); // after getting the substring, parameterize this as validate isbn
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getSubstring (String data) {
		String isbn10Data = "";
		int count = 0;
		int len = data.length();
		
		for (int i = 0; i <= len - 1; i++) {
			if (validateCharacter("[0-9X]", String.valueOf(data.charAt(i)))) {
				if (count < 10) {
					isbn10Data += String.valueOf(data.charAt(i));
					count++;
				}
			} else if (validateCharacter("[-]", String.valueOf(data.charAt(i)))) {
				// dashes will be skipped during loop and do nothing
			} else if (count < 10 && validateCharacter("[^0-9]", String.valueOf(data.charAt(i)))) {
				isbn10Data = "";
				count = 0;
			}
		}
		
		return isbn10Data;
	}
	
	public static boolean validateCharacter (String regexPattern, String data) {
		return (Pattern.matches(regexPattern, data) ? true : false);
	}
	
	public static void delay () {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}