package file_handling;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Pattern;

public class BufferedReaderStrongValidationClass {
	public static void main (String args []) {
		execute("C:\\Users\\kurt kevin nebril\\Desktop\\readThis.txt");
	}
	
	public static void execute (String link) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(link));
			while (br.ready() || br.read() != -1) {
				delay();
				System.out.println(strip(br.readLine()));
				
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String strip (String value) {
		int count = 0;
		
		for (int i = 0; i <= value.length() - 1; i++) { // linear search
			if (count != 10) {
				if (validate("[0-9]", String.valueOf(value.charAt(i))) || validate("[X]", String.valueOf(value.charAt(i))) ) {
					count++;
				} else if (validate("[-]", String.valueOf(value.charAt(i)))) {
					// do nothing
				} else if (String.valueOf(value.charAt(i)).contains("\t")) {
					// do nothing
				} else if (validate("[^0-9]", String.valueOf(value.charAt(i)))) {
					break;
				} else {
					count = 0;
					break;
				}
			} else {
				break;
			}
		}
		
		if (count == 10) { // if successful in linear search
			count = 0;
			System.out.print(count + ")");
			return value;
			
		} else { // if not successful in linear count, do inverse linear search
			for (int i = value.length() - 1; i >= 0; i--) { // linear search
				if (count != 10) {
					if (validate("[0-9]", String.valueOf(value.charAt(i))) || validate("[X]", String.valueOf(value.charAt(i))) ) {
						count++;
					} else if (validate("[-]", String.valueOf(value.charAt(i)))) {
						// do nothing
					} else if (String.valueOf(value.charAt(i)).contains("\t")) {
						// do nothing
					} else if (validate("[^0-9]", String.valueOf(value.charAt(i)))) {
						break;
					} else {
						count = 0;
						break;
					}
				} else {
					break;
				}
			}
			System.out.print(count + ")");
			return value;
		}
	}
	
	public static Boolean validate (String regex, String value) {
		boolean checker = Pattern.matches(regex, value);
		
		return checker;
	}
	
	public static void delay () {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
