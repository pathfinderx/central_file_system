package isbn10;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class isbn10_reader_writer_validation {
	public static void main (String args []) {
		execute("C:\\Users\\kurt kevin nebril\\Desktop\\readThis.txt");
	}
	
	public static void execute (String link) {
		try {
			FileReader file = new FileReader (link);
			Scanner sc = new Scanner (file);
			File strongWrite = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\strongWrite.txt");
			FileWriter fw = new FileWriter(strongWrite);
			PrintWriter print = new PrintWriter(fw);
			int validCounter = 0;
			int invalidCounter = 0;
			
			while (sc.hasNextLine()) {
				delay();
				String dataOfEveryLine = sc.nextLine();
				if (validateISBN(getisbn10Data(String.valueOf(dataOfEveryLine)))) {
					validCounter ++;
					print.println(getisbn10Data(dataOfEveryLine) + "is valid");
				} else {
					invalidCounter ++;
					print.println(getisbn10Data(dataOfEveryLine) + "is invalid");
				}
			}
			
			print.println("\nTotal number valid isbn: " + validCounter);
			print.println("Total number of invalid isbn number: " + invalidCounter);
			print.println("Total number of isbn numbers tested: " + validCounter + invalidCounter);
			print.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getisbn10Data (String data) {
		String isbn10Data = "";
		int count = 0;
		int len = data.length();
		
		for (int i = 0; i <= len - 1; i++) {
			if (validateCharacter("[0-9X]", String.valueOf(data.charAt(i)))) {
				if (count < 10) {
					isbn10Data += String.valueOf(data.charAt(i));
					count++;
				}
			} else if (validateCharacter("[-]", String.valueOf(data.charAt(i)))) {
				// dashes will be skipped during loop and do nothing
			} else if (count < 10 && validateCharacter("[^0-9]", String.valueOf(data.charAt(i)))) {
				isbn10Data = "";
				count = 0;
			}
		}
		
		return isbn10Data;
	}
	
	public static boolean validateCharacter (String regexPattern, String data) {
		return (Pattern.matches(regexPattern, data) ? true : false);
	}
	
	public static boolean validateISBN (String isbn10Data) {
		int len = isbn10Data.length();
		int product = 0;
		
		for (int i = 0; i <= len - 1; i++) {
			if (isbn10Data.charAt(i) == 'X') {
				product += 10;
			} else {
				product += (isbn10Data.charAt(i) - '0') * (10 - i);
			}
		}
		
		return ((product % 11 == 0) ? true : false);
	}
	
	public static void delay () {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}