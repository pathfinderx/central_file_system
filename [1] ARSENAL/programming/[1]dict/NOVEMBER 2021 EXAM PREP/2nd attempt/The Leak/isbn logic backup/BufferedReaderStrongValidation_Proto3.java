package file_handling;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Pattern;

public class BufferedReaderStrongValidationClass {
	public static void main (String args []) {
		execute("C:\\Users\\kurt kevin nebril\\Desktop\\readThis.txt");
	}
	
	public static void execute (String link) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(link));
			while (br.ready() || br.read() != -1) {
				delay();
				System.out.println(strip(br.readLine()));
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String strip (String value) {
		boolean countStarted = false;
		int count = 0;
		int subsStart = 0;
		
		for (int i = 0; i <= value.length() - 1; i++) { // linear search
			if (validate("[0-9]", String.valueOf(value.charAt(i))) || validate("[X]", String.valueOf(value.charAt(i))) ) {
				if (count < 10) {
					if (!countStarted) {
						countStarted = true;
						subsStart = i;
					}
					count++;
				}
			} else if (validate("[-]", String.valueOf(value.charAt(i))) ) {
				// skip below 
			} else if (count < 10 && validate("[^0-9]", String.valueOf(value.charAt(i)))) {
				count = 0;
				countStarted = false;
				subsStart = 0;
			}
		}
		
		System.out.print(String.format("%s|%s|%s%s) ", subsStart, count, value.charAt(subsStart), value.charAt(subsStart+1)));
		
		return value;
	}
	
	public static Boolean validate (String regex, String value) {
		boolean checker = Pattern.matches(regex, value);
		
		return checker;
	}
	
	public static void delay () {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
