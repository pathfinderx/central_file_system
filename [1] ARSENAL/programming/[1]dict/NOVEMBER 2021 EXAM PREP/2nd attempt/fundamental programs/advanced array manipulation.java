resources:
	// https://alvinalexander.com/source-code/java/how-create-populate-static-list-arraylist-linkedlist-syntax-in-java/
		- how to populate array list
			ArrayList<Integer> theArray = new ArrayList<Integer>() {{
				add(0);
				add(1);
			}};

Array vs ArrayList
	* difference
		array - fixed size and cannot be changed
		arrayList - dynamic size; requires import java.util.ArrayList;
	* decleration
		array - String [] array_name = {};
		arrayList - ArrayList<String> array_name = new ArrayList<String>();
			- public decleration
				- public static ArrayList<Integer> arrayName;
				- then instantiate inside method
					- arrayName = new ArrayList<Integer> 
	* read the size
		array - array_name.length
		arrayList - array_name.size()
	* display items
		array - array_name[0]
		arrayList - array_name.get(0) - specific
			- array_name - call the array_name if you want to display all values in arrayList
	* append
		array - n/a
		araryList - array_name.add()
	* change items
		arrayList - array_name.set(index, Value)
	* remove item
		arrayList - array_name.remove(index)
	* search for value:
		indexOf(identifier) - search for value of the array and returns the index of that value
	* to check if its empty
		arrayName.isEmpty() - returns true if array has no value

	shortcut - sgasrii = size(index), get(index), add(value), set(index, value), remove(index)

ACCESSING EVERY DIGIT OF INTEGER
	* always make a copy of a number: sample_int = temp
	* get the last digit: sample_int = sample_int % 10
	* remove the last digit: sample_int = sample_int / 10

DATA TYPE CONVERSIONS
	Int: Integer.parseInt(identifier);

COMPARING TWO STRING
	* dont forget to always lowercase the string: toLowerCase()
	* then use this built-in function: equals()

SOME ESSENTIAL CODES TO REMEMBER
	* indentifier.getClass().getSimpleName()
	* multiply the number by itself: 
		* make a loop first to nth times
		* globalVariable *= identifier
	* how to turn on java suggestion?
		* window > preferences > java > editor > advanced > "select the check box"

TRY THIS
	// https://www.baeldung.com/java-multi-dimensional-arraylist