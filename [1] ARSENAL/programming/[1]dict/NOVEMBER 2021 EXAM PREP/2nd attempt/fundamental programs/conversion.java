String
	* String to int
		* public static int parseInt(String s)  
			* The parseInt() is the static method of Integer class.
			* ex: int i=Integer.parseInt("200");  
		
		* Integer.valueOf()
			* String to Integer object
int
	* int to String
		* public static String valueOf (int i)
			* valueOf() is the static method of String class.
			* ex: 
				int i=10;  
				String s=String.valueOf(i); //Now it will return "10"  

		* public static String toString(int i)  
			* is the static method of Integer class
			* ex: 
				int i=10;  
				String s= Integer.toString(i);//Now it will return "10" 

		* public static String format(String format, Object... args)
			* ex 
				int i=200;  
				String s=String.format("%d",i);

* shortcuts
	* String to any number data types use : use parse :ex: Integer.parseInt(input)
	* any number data types to String: use String.toString(input)

	* char to String
		* String.valueOf(characterInput)
	* String to Char
		* theString.charAt(0)

* essential methods
	* String.valueOf()
	* Character.getNumericValue()
	// * Character.forDigit()

* 2 ways to convert anything to string
	* the primitive data type toString method
		ex: Integer.toString(input)
	* the String class method String.valueOf(input)
		ex: String.valueOf(input)

* decimal manipulation
	// * https://mkyong.com/java/java-display-double-in-2-decimal-points/
		* ways of manipulating decimal places


-------------------------
binary - base 2
	0 - off
	1 - on
decimal - base 10
	[0-9] - 10 digits
octal - base 8
	[0-7] - 8 digits
hexadecimal - base 16
	[0-9] - 10 digits
	A(10) B(11) C(12) D(13) E(14) F(15)

binary - decimal
	* technique using successive division

RESOURCE
	// https://www.youtube.com/watch?v=FFDMzbrEXaE
-------------------------