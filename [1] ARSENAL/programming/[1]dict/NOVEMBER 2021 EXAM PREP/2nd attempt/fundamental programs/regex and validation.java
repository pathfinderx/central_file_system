* java regex (regular expression) - api to define pattern for searching and manipulating strings
	- used for making constraings on email and password
	- used for validation

* java.util.regex package provides
	* MatchResult Interface
	* Matcher class
		- performs match operation on a character sequence
	* Pattern class
		- used to define patterns
	* PatternSyntaxException class
---
	* regex character classes
		1	[abc]	a, b, or c (simple class)
		2	[^abc]	Any character except a, b, or c (negation)
		3	[a-zA-Z]	a through z or A through Z, inclusive (range)
		4	[a-d[m-p]]	a through d, or m through p: [a-dm-p] (union)
		5	[a-z&&[def]]	d, e, or f (intersection)
		6	[a-z&&[^bc]]	a through z, except for b and c: [ad-z] (subtraction)
		7	[a-z&&[^m-p]]	a through z, and not m through p: [a-lq-z](subtraction)

	* regex quantifiers
		- specify the number of occurrences of a character.
		Regex	Description
		X?	X occurs once or not at all
		X+	X occurs once or more times
		X*	X occurs zero or more times
		X{n}	X occurs n times only
		X{n,}	X occurs n or more times
		X{y,z}	X occurs at least y times but less than z times

	* regex metacharacters
		- metacharacters work as shortcodes
		Regex	Description
			.	Any character (may or may not match terminator)
			\d	Any digits, short of [0-9]
			\D	Any non-digit, short for [^0-9]
			\s	Any whitespace character, short for [\t\n\x0B\f\r]
			\S	Any non-whitespace character, short for [^\s]
			\w	Any word character, short for [a-zA-Z_0-9]
			\W	Any non-word character, short for [^\w]
			\b	A word boundary
			\B	A non word boundary

	* scopes in character classes
		\x Literal escape    	
	   	[...] Grouping	
    	a-z Range	
    	[a-e][i-u] Union	
    	[a-z&&[aeiou]] Intersection	

    * essential methods
    	* find()
    	* matches()
    	* group()

	WHAT I LEARNED DURING EXECUTION
		* 3 different types of declaration (initialization)
		* character classes, quantifiers and metacharacters
		* quantifier "*" is effective
		* difference between method find() and matches()
			* find() finds for occurences in targeted value
			* matches() matches the whole targeted value with the regex rule

	other info (resources)
		// * https://stackoverflow.com/questions/32773866/meaning-behind-underscore-character-in-regular-expressions
			// * \w = Any word character, short for [a-zA-Z_0-9]
			// * what does underscore in character classes means? [a-zA-Z_0-9]
		// * https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
			// * regex rules
		// https://stackoverflow.com/questions/14134558/list-of-all-special-characters-that-need-to-be-escaped-in-a-regex
			// * escape the special characters
		// https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
			// * good reference
		// http://tutorials.jenkov.com/java-regex/index.html
			// also good reference

		// https://java2blog.com/input-validation-java/
			// * validation
		// https://stackoverflow.com/questions/3059333/validating-input-using-java-util-scanner
			// * validation int with while loop (stackoverflow)
		// https://www.javascan.com/968/java-regular-expression-to-check-number-with-dot-separator-and-two-decimal
			// * unexplored good reference
		// https://stackoverflow.com/questions/8327705/what-are-and-in-regular-expressions
			// * unexplored good reference
		// https://www.rexegg.com/regex-quickstart.html
			// * unexplored good reference

		tester
			// https://regexr.com/
				// * javascript
			// https://www.regextester.com/97778
				// * java

		videos
			// https://www.youtube.com/watch?v=rhzKDrUiJVk