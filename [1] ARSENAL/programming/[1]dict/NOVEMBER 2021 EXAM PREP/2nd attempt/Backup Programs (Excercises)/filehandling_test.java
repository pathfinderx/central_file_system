package tester;

import java.io.*;
import java.util.*;

public class TestClass {
    public static void main (String args []) {
        execute ();
    }
    
    public static void execute () {
        File sourceDirectory = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn");
        File sourceFile = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\strongRead2.txt");
        
        String [] contents = sourceDirectory.list();
        for (String value : contents) {
            System.out.println(value);
        }
        
        System.out.println("\n");
        File targetFile = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\strongWrite2.txt");
        if (sourceFile.renameTo(targetFile)) {
            System.out.println("Succesfully renamed: " + sourceFile.getName() + " to: " + targetFile.getName());
        } else {
            System.out.println("Failed to rename: " + sourceFile.getName());
        }
        
        System.out.println("\n");
        for (String value : contents) {
            System.out.println(value);
        }
        
        System.out.println("\n");
        try {
            if (targetFile.exists()) {
                Scanner sc = new Scanner (new FileReader(targetFile));
                while(sc.hasNextLine()) {
                    System.out.println(sc.nextLine());
                }
            } else {
                System.out.println(targetFile.getName() + " not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}