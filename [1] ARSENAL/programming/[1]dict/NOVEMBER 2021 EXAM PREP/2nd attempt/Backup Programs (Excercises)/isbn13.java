package unstaged_tests;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Tester {
	public static void main (String args []) {
		execute ();
	}
	
	public static void execute () {
		try {
			FileReader fileread = new FileReader ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\isbn13.txt");
			Scanner sc = new Scanner (fileread);
			
			int countValid = 0;
			int countInvalid = 0;
			File filewrite = new File ("C:\\Users\\kurt kevin nebril\\Desktop\\isbn\\isbn13Write.txt");
			PrintWriter print = new PrintWriter (new FileWriter (filewrite));
			
			while (sc.hasNextLine()) {
				String data = isbn13_strip(sc.nextLine());
				if (validateISBN13(data)) {
					countValid++;
					print.println(data + " is valid isbn13 number");
				} else {
					countValid--;
					print.println(data + " is invalid isbn13 number");
				}
			}
			
			print.println("\nTotal number of isbn13 numbers: " + (countValid + countInvalid));
			print.println("Total number of valid isbn numbers: " + countValid);
			print.println("Total number of invalid isbn numbers: " + countInvalid);
			print.close();
			
			System.out.println(filewrite.getName() + " sucessfully craeted and written");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String isbn13_strip(String data) {
		int count = 0;
		String isbn13 = "";
		
		for (int i = 0; i <= data.length() - 1; i++) {
			String value = String.valueOf(data.charAt(i));
			if (validateChar("[0-9]", value)) {
				if (count < 13) {
					count++;
					isbn13 += value;
				}
			} else if (validateChar("[-]", value)) {
				// ignore dashes
			} else if (count < 13 && validateChar("[^0-9]", value)) {
				count = 0;
				isbn13 = "";
			}
		}
		
		return isbn13; 
	}
	
	public static boolean validateChar (String regex, String data) {
		return (Pattern.matches(regex, data) ? true : false);
	}
	
	public static boolean validateISBN13 (String data) {
		int sum = 0;
		int count = 0;
		
		for (int i = 0; i <= data.length() - 1; i++) {
			if (count == 0) {
				count ++;
				sum += (data.charAt(i) - '0') * 1;
			} else if (count == 1) {
				count = 0;
				sum += (data.charAt(i) - '0') * 3;
			}
		}
		
		return (sum % 10 == 0) ? true : false;
	}
}