package unstaged_tests;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.math.*;

public class Tester {
	public static ArrayList <String> letters = new ArrayList <String> ();
	public static ArrayList <Integer> numbers = new ArrayList <Integer> ();
	public static ArrayList <Double> doubles = new ArrayList <Double> ();
	public static ArrayList <Boolean> booleans = new ArrayList <Boolean> ();
	
	public static void main (String args []) {
//		sortLetters ();
//		sortNumbers ();
//		sortDoubles ();
		sortBooleans ();
	}
	
	public static void sortLetters () {
		char start = 'A';
		char end = 'z';
		System.out.println(String.format("%s %s",(int) start, (int) end));
		
		for (int i = (int) start ; i <= (int) end - 1; i++) {
			System.out.print((char) i);
			if (Pattern.matches("[a-zA-Z]", String.valueOf((char) i))) {
				letters.add(String.valueOf((char) i));
			}
		}
		
		System.out.println();
		System.out.println(letters);
		Collections.reverse(letters);
		System.out.println(letters);
		
	}
	
	public static void sortNumbers () {
		Random random = new Random();
		for (int i = 0; i <= 10; i++) {
			numbers.add(random.nextInt(10));
		}
		
		System.out.println(numbers);
		Collections.sort(numbers);
		System.out.println(numbers);
	}
	
	public static void sortDoubles () {
		Random random = new Random();
		for (int i = 0; i <= 10; i++) {
			int min = 50; int max = 100;
			double randomize = min + (max - min) * random.nextDouble();
			BigDecimal bd = new BigDecimal(randomize).setScale(2, RoundingMode.HALF_DOWN);
			doubles.add(bd.doubleValue());
		}
		System.out.println(doubles);
		Collections.sort(doubles);
		System.out.println(doubles);
	}
	
	public static void sortBooleans () {
		Random random = new Random();
		for (int i = 0; i <= 10; i++) {
			boolean randomizedBoolean = random.nextBoolean();
			booleans.add(randomizedBoolean);
		}
		System.out.println(booleans);
		Collections.sort(booleans);
		System.out.println(booleans);
	}
}