package tester;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class TestClass {
  public static Scanner sc = new Scanner (System.in);

  public static void main (String args []) {
      execute ();
  }
    
  public static void execute () {
    boolean breaker = false;

    while (!breaker) {
      sc = new Scanner (System.in);
      printChoices();
      switch(sc.nextInt()){
        case 1:
          decimalToBinary();
          break;
        case 2:
          decimalToOctal();
          break;
        case 3:
          decimalToHexadecimal();
          break;
        default:
            System.out.println("Invalid input format");
            break;
      }
    }
  }
    
    public static void printChoices () {
        System.out.println("Enter choice: ");
        System.out.println("[1] decimalToBinary");
        System.out.println("[2] decimalToOctal");
        System.out.println("[3] decimalToHexadecimal");
    }
    
  public static void decimalToBinary () {
    boolean breaker = false;

    while (!breaker) {
      sc = new Scanner (System.in);
      System.out.println("Enter decimal to convert to binary: ");

      if (sc.hasNextInt()) {
        ArrayList <Integer> binary = new ArrayList();
        int input = sc.nextInt();
        
        double dividend = Double.parseDouble(String.valueOf(input));
        int divisor = 2;
        int remainder = 0;
        
        while (input != 0) {
          remainder = input % divisor;
          input = input / divisor;
          binary.add(remainder);
          System.out.println(String.format("%s | %s", input, remainder));
        }
        
        System.out.println("The binary: ");
        for (int i = binary.size() - 1; i >= 0; i--) {
          System.out.print(binary.get(i));
        }
        System.out.println("\n");
        
      } else {
        System.out.println("Invalid input format");
      }
    }
  }

  public static void decimalToOctal () {
    boolean breaker = false;
    
    while (!breaker) {
      System.out.println("Enter decimal to convert to octal");
      if (sc.hasNextInt()) {
        ArrayList <Integer> octal = new ArrayList <Integer> ();
        int input = sc.nextInt();
        int divisor = 8;
        
        while (input != 0) {
          int remainder = input % divisor;
          input = input / divisor;
          octal.add(remainder);
          System.out.println(String.format("%s | %s", input, remainder));
        }
        
        System.out.println("Octal: ");
        for (int i = octal.size() - 1; i >= 0; i--) {
          System.out.print(octal.get(i));
        }
        System.out.println("\n");
      } else {
        System.out.println("Invalid input format");
      }
    }
  }

  public static void decimalToHexadecimal () {
    boolean breaker = false;
    
    while (!breaker) {
      System.out.println("Enter decimal to convert to hexadecimal");
      if (sc.hasNextInt()) {
        ArrayList <String> alphabets = new ArrayList <String> (Arrays.asList("A", "B", "C", "D", "E", "F"));
        String hexadecimal = "";
        int input = sc.nextInt();
        int divisor = 16;
        
        while (input != 0) {
          int remainder = input % divisor;
          input = input / divisor;
          
          if (remainder >= 10 && remainder <= 15) {
            int temp = (remainder % 10);
            hexadecimal += alphabets.get(temp);
            System.out.println(String.format("%s | %s", input, alphabets.get(temp)));
          } else {
            hexadecimal += remainder;
            System.out.println(String.format("%s | %s", input, remainder));
          }
          
          System.out.println("Hexadecimal");
          for (int i = hexadecimal.length() - 1; i >= 0; i--) {
            System.out.print(hexadecimal.charAt(i));
          }
          System.out.println();
        }
      } else {
        System.out.println("Invalid Input format");
      }
    }
  }
}