Theoretical
	# version control system has 2 categories	
		# centralized
			# disadvantage: if server is offline, cant collaborate
		# distributed
			# advantage: every user has local copies

		# compresses the content
		# doesnt store duplicate content

		# MINGW64 - unix powershell

	# install git
	# utilize bash instead of OS built in terminal

@Configuration
	'git config --<options> user.name "pathfinderX"'
		# user name

	'git config --<options> user.email nebrilkurtkevin5@gmail.com'
		# email

	'git config --<options> core.editor "code --wait"'
		# '--wait' flag tells the terminal to wait till new vscode instance is closed
		# default editor

		# practical usage: when you revert using 'git revert HEAD', it opens vs code (using code --wait) and wait till vscode is closed
		# https://www.git-tower.com/learn/git/faq/undo-revert-old-commit

	'git config --<options> -e'
		# opens the git configuration

	'git config --<options> core.autocrlf true'
		# this config is line ending handler
		# autocrlf or auto carriage return line feed or (/r /n) 
		# this configuration settings only removes the line feed for both OS in a centralized repository (/n)
		# use 'true' if windows use 'input' if macOS

	'git config --help || git config -h'
		# commands

	# config options
		# local
			# affects only to local repository
		# global
			# affects the current user of the OS
		# system
			# affects the entire users of the OS

@Display
	general
		'git status'
			# show repo status

		'git config --global -e'

	staging area
		'git ls-files'
			# show files in staging area

	commits
		'git log --oneline'
			# oneliner

		'git log --oneline -n 10'
			# oneline and show only 10 latest commits

		'git log --graph --decorate'
			# complete with branch

		'git log --graph --decorate'
			# complete with branch and display only 10 with color

		'git log --graph --decorate | tail -n 10'
			# complete with branch and only display only 10 commits

@Create
	create git repository
		create directory then cd to that directory, use these commands
			'mkdir'
			'cd <directory>'

		'git init'
			# create master for that directory
			# creates .git file but hidden

		'git init --bare'
			# creates repository without a working tree

		'touch <file>'
		# unix command to create file

@Copy frm source to destination 

	copy directory tree to destinaiton
		'cp -R <dir1> <dir2>'
			# -R recursive copy including hidden files
			# dir1 should be existing
			# dir2 can be optional: either existing or not
				# if dir2 is existing: it copies dir1 inside dir2	
					# dir2/dir1/dir1contents
				# if dir2 is not existing: it creates dir1 copy and named dir2
					# dir2

	# ref: https://www.rapidtables.com/code/linux/cp.html

@Move
	'git mv oldfile.txt newfile.txt'
		# rename file

	'git mv <file> ..'
		# move file to previous one level

@Rename
	# rename flow
		# ref: https://github.com/petervanderdoes/gitflow-avh/wiki/Reference:-git-flow-feature
		# git flow feature rename - Rename a feature branch
		# Description
		# Rename branch <name> to <new_name>

		# Synopsis
			git flow feature rename [-h] <new_name> [<name>]

		# Options
		# -h,--[no]help
		# show this help

		# --showcommands
		# Show git commands while executing them

		# 

	# -m flag means move or rename 
		'git branch -m <old-branch-name> <new-branch-name>'

	# rename latest commit message
		'git commit --amend -m "Message"'
			# amend the unpushed commit
			# or amend the pushed commit

		'git push -u origin <branch-name>'
			# dont forget to push

@Remove
	unix command
		'rm -rf <filename>'
			remove .git
			remove that directory as master
			-rf flag means force delete (makes the deletion faster)
				 Force deletion of everything in root directory

		'rm <path>'
			# remove directory

	git command
		'git rm -rf <path or filename>'
			# remove directory or file

		'git restore --staged <file>'
			# remove from staging area

		'git restore <file>'
			# remove from unstaged area or discard

		'git rm -rf --cached <filepath>'
			# remove cached files to make the .gitignore work

	delete unfinished branch
		'git branch -D bugfix/execute-initial-commit'

	delete branch
		'git branch -d <branch name>'
		e.g: 'git branch -d feature'


@Revert
	'git revert HEAD'
		# revert the latest commit

	'git revert <commit-hash>'
		# reverts 


	@Removing the unpushed commit
		'git reset HEAD~1'
			# 1 means remove 1 commit

-----
@WORKFLOW (create git repo)
	1: 'git init'

	2: 'git status'

	3: 'git add .'

	4: 'git config --global -e'

		4: 'git config'
			# .. do the following config if required

	5: 'git commit -m "Initial commit"' 


@WorkFlow (without remote?)
	checkers
		'git status'
			# check branch, directory, any commits

		'git status -s'
			# -s flag means short
				# if it displays '??' it means it is untracked file
				# if it displays 

		'git ls-files'
			# check files on staging area??
	
	add to staging area or index 
		# proposal for next commit
		# for review

		# commands:
			'git add file_name'
				# add specific

			'git add .'
				# add all

	remove from staging area
		unix command
			'rm <path>'

		git command
			'git restore --staged <file>'

	remove from unstaged area or discard changes
		'git restore <file>'

	commit or snapshot
		# saved in repository
		# contains information
			# ID
			# Message
			# Date / Time
			# Author
			# Complete snapshot

		# commands:
			'git commit -m "Your Message"'
				# relies on staged files

			'git commit -am "Your Message"'
				# skips staging and commits file
			
@workflow (create bitbucket remote)
	# create repository first and copy the url after creation
	# cd to local folder
		'git remote add origin <bitbucket repository url>'

	# check remote
		'git remote -v'

	# push for the first time
		'git push -u -f origin master'
			# the -u flag explanation: https://stackoverflow.com/questions/18867824/what-does-the-u-flag-mean-in-git-push-u-origin-master
			# -f flag means force and it is required for first time push

	# creating and pushing branches
		# create branch
			# note: changes in specific branch are not visible in other branch unless you checkout for the altered branch

			# terminal
				'git checkout -b <branch-name> master'
			# bitbucket 
				# on create branch button: https://bitbucket.org/pathfinderx/central_file_system/branches/

		# fetching changes
			'git fetch --all'

			# note: this also updates any creation and changes in branches
			# note: recommended before executing 'git branch -r' command

		# check current branch
			'git branch -vv'

		# check all branches in repository
			'git branch -r'

		# pushing branch in remote for the first time
			'git push --set-upstream origin <branch-name>'

	# switch branch
		'git checkout <branch-name>'

	# deleting branch
		'git push -d origin </branch-name>'
			# origin means referring to remote

		'git push -d <branch-name>'
			# without origin means deleting branch only in local

		# note: you cant delete branch while you are currently on that branch, you have to switch branch using git checkout command

@workflow (setup ssh method in bitbucket)
	# generate ssh key using puttygen
		# use ssh-2 rsa key	
		# generate without passphase

		# goto:
			# your profile and settings > personal settings > ssh key (under security) > add key
				# adding key
					# load private key using putty gen and copy the key inside 'OpenSSH autorized_key file' field
					# paste it to the key field

@workflow (merge branch)
	# check branch
		'git branch --v'
	# switch branch to desired branch
		'git checkout <branch-name>'

	# fetch and pull for safety
		'git fetch && git pull'
		# or
		'git fetch origin <branch-name>'
		'git pull origin <branch-name>'

	# merge with the desired branch (scenario: master (current branch) is outdated and should merge with origin/updates)
		'git merge updates'

	# all merged commits and changes are required to be pushed
		'git push -u origin master'	

	@merging (without checking out to other branch first)
		[https://stackoverflow.com/questions/3216360/merge-update-and-pull-git-branches-without-using-checkouts]

		# git fetch <remote> <source branch>:<target branch>
			'git fetch . update:master'
			# '.' means the local repository as the remote

		# now the target branch is ready to push, it is safer to checkout to that branch without making any changes to local files while checking out
		

-----

@Unix commands
	'ctrl + L'
		# clear

	'cd ..'
		# reverse directory to one level

	'cd /'
		# cd to root directory

	'cdls() { cd "$@" && ls; }' # used to cd and if successful, ls to that directory
		'cdls <path>'
			# used to cd then ls to the target directory
			# ref: https://unix.stackexchange.com/questions/20396/make-cd-automatically-ls#:~:text=The%20%26%26%20means%20'%20cd%20to,the%20cd%20worked%20or%20not.

	'cmd //c tree'
		# tree view
		# ref: https://www.tecmint.com/linux-tree-command-examples/

	'touch <flag> <filename>'
		# -c = to check if there are any existing files

@VIM commands
	':q' or ':q!' 
		# to quit vim

@Nano commands
	'nano <filename with extension>'

-----

@Discovered @Reminders
	'git config <flag>'
		# flags:
			# --global 
				# affects all working repositories for current operating system user
				# stored in user home directory

			# --local
				# affects only for repository
				# stored in repo's .get directory

			# --system
				# affects the entire users of on an entire machine
				# all users in operating system and repositories
				# stored in system root path

			# ref: https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config

	'git config --<options> core.editor "code --wait"'
		# default config editor: vscode
		# other options:
			"atom --wait"
			"emacs" 
			"nano -w" 
			"vim" 
			"subl -n -w" 	
				# sublime
			"'C:/program files (x86)/sublime text 3/sublimetext.exe' -w"
				# sublime x86
			"'C:/program files/sublime text 3/sublimetext.exe' -w"
				# sublime x64
			"mate -w"
				# textmate

		# some options here: https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config

	Reverting the old commit
		# only reverts the files that was changed on the targetted commit (commit-hash)
		# commit-hash can be seen on the first column of the git log using 'git log --oneline' command

@Issues (Procedures to watchout)
	Another git process seems to be running in this repository
		# https://stackoverflow.com/questions/38004148/another-git-process-seems-to-be-running-in-this-repository)

		# execute: 
			'rm -f .git/index.lock'

	when directly adding files to repo then initialize
		# error: '2022/Git Reporsitories/test/' does not have a commit checked out
		# fatal: adding files failed

		# do this procedures:
			# delete the git repository
				# cd to directory
				'ls -a'
					# to see the hidden file .git
				'rm -rf .git'
					# '-rf' for deletion in root directory
				'mkdir <filename>'

				'cd <filename>'

				'git init'

		# ref: https://zjschulz.medium.com/error-does-not-have-a-commit-checked-out-573c0ed9c49

	Why adding directory wont change the git status?
		# directories are ignored unless you put a file on it

	.gitignore not working?
		# first, make sure file is not deleted or not in staged if deleted

		# if deleted and staged: unstage the changes
			'git restore --staged <filename>'
			
			# then
				'git rm -rf --cached <filepath>'

		# no changes ever made
			'git rm -rf --cached <filepath>'

		# now try to make changes the target file

	.gitignore not working again?
		# just delete the cached files in .git if it exists before .gitignore was created
			'git rm -rf --cached <the-stubborn-file>'


	"this is not a valid source path"
		# utilize ssh configured in source tree > bitbucket repo
			# https://bitbucket.org/pathfinderx/central_file_system/admin/addon/admin/pipelines/ssh-keys

		# uncheck private repository
			# https://bitbucket.org/pathfinderx/central_file_system/admin > general > repository details

	"Failed to push the duplicate commit amends"
		# With message:
			# To https://bitbucket.org/pathfinderx/central_file_system.git
			#  ! [rejected]        update -> update (non-fast-forward)
			# error: failed to push some refs to 'https://bitbucket.org/pathfinderx/central_file_system.git'
			# hint: Updates were rejected because the tip of your current branch is behind
			# hint: its remote counterpart. Integrate the remote changes (e.g.
			# hint: 'git pull ...') before pushing again.
			# hint: See the 'Note about fast-forwards' in 'git push --help' for details.

		# recommended action (worked this way)
			# checkout to other_branch
			# merge with the primordial_branch as backup
			# push the other_branch

			# checkout back to primordial_branch
			# push the primordial_branch

	"Unlink of file 'db.sqlite3' failed. Should I try again? (y/n)"
		# disable runserver on django

	"'refs/heads/feature' exists; cannot create" - using git flow feature
		# remove the existing branch 'feature' if existing in branch or manually added 'feature' branch
		# using command 'git branch -d feature'