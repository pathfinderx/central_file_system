	[set 1]
	* aristocrat
	* motor pool
	* interval
	* ambidexterity
	* interjection
	* constraints
	* integrity
	* catalog
	* meta data
	* torrents

	[set 2]
	* concurrency
	* proliferation
	* naive
	* sophisticated 
	* savior-faire
	* corresponds
	* gradually
	* severity
	* audacious

	[set 3]
	* disassociate
	* subjective
	* objective
	* pandemic 
	* epidemic

-----------------------------
11 05 19
	* aristocrat
	* motor pool
	* interval
	* ambidexterity
	* interjection
	* constraints
	* integrity
	* catalog
	* meta data
	* torrents

	Result
		1st iteration (one way)
			attempt 1: 01 01 20
			attempt 2: 36 49
			attempt 3: 31 87
			attempt 4: 33 43
			attempt 5: 29 91
		2nd iteration (one way and return)
			attempt 1: 01 09 34
			attempt 2: 59 06
			attempt 3: 01 07 48
			attempt 4: 01 00 20
			attempt 5: 56 50
	remarks (words of the enlightened): 'fresh start gives rapid retention'

----------------------------

11 06 19
	Result
		3rd iteration (one way)
			attempt 1: 24 41
			attempt 2: 22 47
			attempt 2: 22 46
		4th iteration (one way and return)
			attempt 1: 47 29
			attempt 2: 46 46
	remarks (words of the enlightened): 'retention effects better the day after stimulation'

	* concurrency
	* proliferation
	* naive
	* sophisticated 
	* savior-faire

	* corresponds
	* gradually
	* severity
	* audacious

	Result
		1st iteration (one way)
			attempt 1: 39 86
			attempt 2: 47 93
			attempt 3: 47 07
			attempt 4: 31 55
			attempt 5: 33 83
			attempt 6: 32 61
			attempt 7: 28 08
			attempt 8: 26 76
			attempt 9: 24 53
		2nd iteration (two way)
			attempt 1: 56 17
			attempt 2: 52 66 

	remarks (words of the enlightened): 
		attempt 2: 'make definition simple, to avoid ambiguities at active stimulation'
		attempt 3: 'understand the definition first and be consistent. Being creative at active stimulation results to inconsistent definition'
		attempt 4: 'advance reading to the next word helps'

----------------------------

03 08 20
	recall
		* aristocrat
		* motor pool
		* interval
		* ambidexterity
		* interjection
		* constraints
		* integrity
		* catalog
		* meta data
		* torrents

		* concurrency
		* proliferation
		* naive
		* sophisticated 
		* savior-faire

		* corresponds
		* gradually
		* severity
		* audacious

03 10 20
	recall
		[set 1]
		[set 2]
		[set 3]

03 16 20
	recall
		[set 1]
		[set 2]
		[set 3]