New words

(1-27-17)
[7:45pm]
 * aristocrat
	- group of people that is higher than social class
	- rich and has more power
 * motor pool
	- vehicles in a specific area where it is available for use
 * interval
	- point of time between events
 * ambidexterity
	- specific skills can be executed by hands equally
 * interjection
	- expressive words
 * constraints
	- something that hinders the present motion
 * integrity
	- unity
	- moral sense
 * catalog
	- list of things that are organized
 * meta data
	- information about data

(1-28-17)
	[12:17pm]
	* torrents
		- haviy rain; stream of water coming down
	* concurrency
		- the fact of events or circumstances existing at the same time.
	* proliferation
		- rapid reproduction
	* naive
		- lack of knowledge, wisdom and experience
	* sophisticated 
		- having particular knowledge of something
	* savior-faire
		- the ability to act appropriately in social situations.

	AUGUST
	* corresponds
	    - compatible with the characteristics
	* gradually
	    - slow progress
	* severity
	    - condition of being severe.
	* audacious
		- invulnerable to intimidation


03 10 20	
	* disassocate
		- detachment from reality when it comes to psychology
	* subjective
		- personal perspective
	* objective
		- purely based on evidences or facts
	* pandemic 
		- prevalent to whole world
		- prevalent = widespread
	* epidemic
		- prevalent to a community

2019 - 2020
	Please Add this with CSE 2019 words
