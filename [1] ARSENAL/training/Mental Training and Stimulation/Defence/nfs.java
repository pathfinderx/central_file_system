11 08 19
	Clubhouse and Lennox
		03 20 94
		03 17 70
		03 17 91
		03 20 34

		03 19 91
		03 16 19
		03 12 94
	Notes:
		- 'give remarks on every executions of the corners': 'very good / good / average / bad'

11 13 19
	Diamond Park
		02 56 99
		03 01 45
		02 50 84
		02 53 04
		02 49 59
	notes: 'corner execution remarks applied'

11 15 19
	Diamond Park
		02 56 90
		02 51 14
		02 53 16
		02 50 72
		02 49 52

11 28 19
	HWY 201
		03 02 68
		03 04 06
		02 57 71

11 30 19
	HWY 201
		03 00 24
		02 59 65

12 01 19
	03 03 09
	02 58 33
	02 56 11

12 04 19
	03 11 79
	03 10 80
	03 11 75

12 30 19
	Hastings (2 laps)
		07 05 63
		07 -- --
		07 10 --
		07 01 41 

01 08 20
	bay bridge and sea side
	03 05 73
	03 03 51
	02 58 90

01 09 20
	bay bridge and sea side
		03 07 --
		03 01 --
		03 03 --
		03 01 57
		03 03 --

01 10 19
	forest green
		03 14 54
	bay bridge and sea side
		03 57 34

01 11 19
	forest green
		03 10 42
		03 12 88
		03 09 95

01 13 20
	bay bridge and seaside
		02 58 50
		02 56 10

01 17 20
	Clubhouse and Lennox
		03 20 41
		03 20 23
		03 18 56
		03 06 79 (used shortcut)

01 18 20
	Clubhouse and Lennox
		03 22 78
		03 16 89
		03 12 27

	forest green
		03 12 92
		03 13 91
		03 11 12
		03 09 43

01 22 20
	industrial and bristol
		01 56 31
		01 54 43
		01 54 82
		01 55 17
		01 55 10

01 25 20
	industrial and bristol
		01 55 41
		01 54 52
		01 53 03

	diamond park
		02 51 02
		02 52 53
		02 50 58
		02 50 22
		02 50 09

01 26 20
	diamond park
		02 50 77

	hwy 201
		03 01 38
		02 57 21

01 27 20
	seaside interchange
		03 07 84
		03 03 45
		02 59 15

01 28 20
	seaside interchanges
		03 01 77
		02 57 64

	ironhorse and coast
		02 40 21
		02 41 52
		02 41 15
		02 39 46

01 29 20
	camdem and dunwich
		02 49 63
		02 48 97
		02 47 25

02 05 20
	camdem and dunwich
		02 49 38
		02 45 45
		02 45 89

02 09 20
	seaside and Lennox
		02 21 37
		02 20 12
		02 18 74

02 11 20
	seaside and Lennox
		02 19 27
		02 18 86
		02 18 39
		02 18 26
		02 17 16

	hwy 99 projects
		02 54 39

02 16 20
	hwy 99 projects
		02 56 93
		02 52 72
		02 50 73

	bond and forest green
		01 51 62

02 17 20
	bond and forest green
		01 51 21
		01 49 45

02 27 20
	seagate and camdem
		02 46 78
		02 40 22
		02 44 94
		02 43 89
		02 41 91

	valley and state
		02 34 46

02 28 20
	valley and state
		02 32 40
		02 31 06
		02 29 57
		02 29 24

03 04 20
	state and warrant
		03 43 54
		03 38 87
		03 36 70
		03 32 11

	beach and chancellor
		02 00 42

03 06 20
	beach and chancellor
		02 02 75
		02 00 03

03 07 20
	camdem and fisher
		02 05 24
		02 02 48
		01 59 98
		01 57 15

03 08 20
	camdem and fisher
		02 03 07
		02 00 28
		01 57 80
		01 57 83

	lyons and state
		03 41 58
		03 37 78

	Bond and country club
		01 58 41

03 10 20
	Bond and coutry club
		01 58 29
		01 55 98
		01 55 62

	Hwy 201 and lyons
		01 57 07
		01 41 90

03 14 20
	Hwy 201 and lyons
		01 45 39
		01 40 80
		01 40 60

	West Park and Lyons
		01 18 41

03 15 20
	West Park and Lyons
		01 16 01
		01 14 09

	Stadium Chase
		02 43 07
		02 40 96
		02 37 73

	Diamond Valley
		02 33 55
		02 30 44

03 16 20
	Stadium and Chase
		02 42 71
		02 39 14
		02 40 17
		02 37 89

	Union Row and Ocean
		02 44 24
		02 37 12

	Interchange and Bond
		01 41 24

03 17 20
	Interchange and Bond
		01 44 95		
		01 43 40
		01 43 14
		01 42 34
		01 41 94

	Camdem and Ironwood
		02 50 89

	camdem and route 55
		03 25 05
		03 25 25
		03 23 43

03 18 20
	camdem and route 55
		02 23 77
		02 23 83

	north bay and harbor
		02 17 97
		02 17 74
		02 15 25

03 20 20
	north bay and harbor
		02 14 71
		02 10 45

	stadium and highway
		02 38 11
		02 36 54

	boundary and marina
		01 24 65
		01 25 47
		01 23 71

	bristol and bayshore
		01 35 08

03 22 20
	bristol and bayshore
		01 33 15
		01 28 94

	beacon and station
		01 34 72
		01 31 97

	chase and bristol
		01 36 15

03 23 20
	chase and bristol
		01 30 40


03 27 20
	rockridge and union
		01 48 40
		01 45 55
		01 44 42

	campus and chancellor
		01 49 69
		01 38 04
		01 38 97

04 01 20
	clubhouse and lennox
		03 18 25
		03 17 91
		03 15 13

04 07 20
	forest green
		03 13 89

04 08 20
	industrial and bristol
		01 55 41
		01 53 25

	diamond park
		02 49 33

04 10 20
	hwy 201
		03 00 57
		03 00 64
		02 57 73

04 12 20
	seaside interchanges
		02 57 10

	ironhorse and coast
		02 42 --
		02 39 05

04 14 20
	ironhorse and coast
		02 40 34

	camdem and dunwich
		02 48 23


04 18 20
	seaside and lennox
		02 19 18
		02 17 68

04 19 20
	camdem and dunwich
		04 47 70

	forest bond and forest green
		01 48 77

	seagate and camdem
		02 44 95
		02 40 22

04 20 20
	valley and state
		02 30 13

	state and warrent
		03 43 --
		03 33 48
		03 30 04

	beach and chancellor
		02 01 06
		01 59 50

04 29 20 
	clubhouse and lennox
		03 20 20 	
		03 16 20
		03 15 83
		03 16 04
		03 12 83
		03 12 35
		03 11 87

05 01 20
	clubhouse and lennox
		03 14 --
		03 11 31
		03 10 33

05 02 20
	bay bridge and seaside
		03 02 08
		03 00 00
		02 54 71
		02 54 57

05 03 20
	iron horse and coast
		02 38 07

	camdem and fisher
		01 58 66
		01 58 10
		01 56 22

	nfs world loop
		06 03 53
		05 54 99

05 04 20
	industrial and bristol
		01 53 52
		01 52 42

05 05 20 
	forest green
		03 13 19
		03 10 98
		03 07 95

	hwy 201
		03 01 --
		02 58 28

05 07 20
	hwy 201
		02 57 69


05 12s 20
	seaside interchange
		03 03 89
		02 57 65
		02 57 49
		02 56 76

05 13 20
	ironhorse and coast
		02 39 31
		02 36 95

05 14 20
	camdem and dunwich
		02 48 06
		02 46 57

05 16 20
	seaside and lennox
		02 20 --
		02 17 21

05 17 20
	hwy 99 projects
		02 55 35
		02 54 13
		02 50 40

05 21 20
	bond and forest green
		01 49 65
		01 50 75
		01 48 35
		01 47 77

05 23 20
	seagate and camdem
		02 47 --
		02 42 76
		02 40 49
		02 38 69 

05 25 20
	valley and state
		02 27 99

	fisher road
		03 55 55 

	petersburg and crossing
		06 31 26

05 26 20	
	fisher road
		03 53 55


05 30 20
	diamond park
		02 48 07

05 31 20
	fisher road
		04 02 --
		03 57 44
		03 55 00

	forest green
		03 10 47

	clubhouse and lennox
		03 16 47

06 01 20
	clubhouse and lennox
		03 11 55
		03 10 44

06 02 20
	industrial and bristol
		01 52 88
		01 51 80

	seaside interchange
		02 55 49
		02 55 04
		02 52 11

06 05 20	
	seaside interchange
		02 53 60

06 06 20
	Hastings
		06 53 35

06 12 20
	Hastings
		06 59 53
		06 55 16

06 13 20
	clubhouse and lennox
		03 14 50
		03 11 56

	forest green
		03 09 99

06 14 20
	forest green
		03 09 33

06 17 20
	Hastings
		06 56 46

06 18 20
	Gray point
		02 43 87
		02 43 08
		02 42 60

06 20 20 
	gray point
		02 41 59
		02 43 40
		02 42 76
		02 42 20
		02 41 06
		02 41 84

	Hastings
		06 58 70
		06 54 58

06 21 20	
	Hastings
		06 54 47

06 22 20
	Hastings
		06 55 70

06 23 20
	Hastings
		06 51 36

	Clubhouse and Lennox
		03 13 14
		03 11 02

06 29 20
	Hastings
		06 51 60

06 30 20
	Hastings
		06 52 62
		06 51 88

07 03 20
	Forest Green
		03 06 58

07 05 20
	Clubhouse and Lennox
		03 11 48

07 29 30
	riverside (knockout)
		06 36 85
		06 33 83
		06 30 23

08 27 20
	01 52 03

09 14 20
	dunwich village (knockout)
		06 55 74
		06 55 56
		05 50 13

09 21 20
	industrial and bristol
		01 53 54
		01 51 43
		01 51 05
		01 51 94

09 24 20
	industrial and bristol
		01 51 33
		01 51 50
		01 50 93
		01 51 10
		
10 01 20
	industrial and bristol
		01 49 32

	diamond park
		02 49 29

10 02 20
	bay bridge and seaside
		02 59 10
		03 02 28 
		02 59 13
		02 59 17
		02 58 84
		02 56 18
		02 57 32
		02 54 99

	forest Green
		03 09 58

10 10 20
	state and warrent
		03 40 53
		03 29 96
		03 30 23

	beach and chancellor
		01 59 85
		01 56 72
		
10 11 20	
	not recorded, just stimulation

10 17 20
	diamond park
		02 51 58
		02 50 25
		02 50 03 
		02 47 62

10 25 20
	
		02 51 85
		02 54 95
		02 55 90
		02 51 95
		02 51 49

10 26 20
	seaside interchange
		02 54 65

12 25 20
	clubhouse and lennox
		03 16 69
		03 17 99
		03 15 01

01 13 21
	forest Green
		03 14 53
		03 09 86

01 18 21
	forest green
		03 11 22
		03 09 10
		03 09 27

01 18 21
	forest green
		03 08 44
		03 09 28
		03 07 83
		03 08 57
		03 08 05

10 25 21
	seaside interchange 
		02 54 39

10 26 21
	seaside interchange	
		02 54 31
		02 53 55	

10 27 21
	seaside interchange
		02 52 12


10 28 21
	forest green
		03 09 --

		03 10 45
		03 10 10
		03 08 60

10 30 21
	forest green
		03 06 30

	bay bridge and seaside
		02 57 23
		02 53 35

10 31 21
	Clubhouse and Lennox

	seaside interchange
		02 53 57
		
11 02 21
	seaside and camdem
		02 29 72