05-08-17
Wor, pra, dev
work-o

Home Q1
	* paint(Set A)
	* in-out[35mins]

School Q4
	* project RetentionTester(System)[1hr]
	* learn framework[1hr]

Personal Q5
	* Set for training[30mins]
	* Operation Intimate plan[30mins]
	* Task Retention[1hr]
	
-----------------------------------------------
05-09-17

/ Pra
/ Paint door 8
/ Bicycle Paint

-----------------------------------------------
05-10-17

/ Pra
/ Work-o
/ Paint door 4,3
/ Flowers
/ Clear outside

-----------------------------------------------
05-11-17

/ Wor, Pra, Dev
/ Work-o

/ Wash Clothes

/ Expenses
/ Task Retention
0 find retention tester
-----------------------------------------------
05-12-17

/ Wor, Pra, Dev
* Work-O

/ Task Retention
* Create Tester system
* Plan for learning new framework
* Plan for a monitored training(statistical)
------------------------------------------------
05-13-17

Personal
/ Wor, Pra, Dev
/ Work-O
/ train left hand
/ Plan for a monitored training(statistical)

CS
/ Sort the unsorted files
/ Project Retention

School
* Plan Tester system
/ Plan for learning new framework

Home
/ paint door 5

-------------------------------------------------
05-15-17
PERSONAL
	/ Wor, Pra, Dev [.30]
	* Physical Training [1]

	/ Prepare written statistical output for Attitude training [.15]
	* Cognitive Training 
		/ left hand training [.15]
		/ language training [.30]
		* mathematical training(basic) [1]
		* read science stuff(master one field) [.30]
SCHOOL
	* learn new framework [1]
	* create retention tester [2]
	* learn DBMS [2]

=================================================
Daily Routines(For training)
	Personal
		- Wor, Pra, Dev [1]
		- Physical Training [1]
		- Cognitive Training 
			- left hand training [.15]
			- language training [.30]
			- mathematical training(basic) [1]
			- read science stuff(master one field) [.30]
		- Attitude(Should be statistical)
			- Prompt Execution Training
			- Endurance Training
			- Humble-down Training
	School
		- learn new framework [1]
		- create retention tester [2]
		- learn DBMS [2]
