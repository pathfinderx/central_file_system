11 01 21
	* prayer, worship
	/ floor, outside
	/ warm up
		/ isbn10 simple and strong
			/ 16 16 72 - strongisbn
			/ 09 51 85 - simpleisbn
		/ pyramid class all
			/ 41 48 38

	* krissy
		* identify 

	* fundamentals
		* regex
		* conversion and validation
		* number system conversion

	/ home
		/ garbage, allround clearing
		/ shirt selection
		** sort shoes container

11 02 21
	[1354]
	[1400 1410] understanding: arraylist ()
	[1552 1608] warm up
	[1625 1635] understanding: load all characters and conversions 
	[1635 1645] understanding: load all characters and conversions 
	[1645 1655] understanding: load all characters and conversions 
	[1655 1706] understanding: load all characters and conversions 

	[1706 1716] understanding: load all characters and conversions
	[1800] execute: regex declaration

	* fundamentals
		* regex
			* making character unicode load to arraylist for regex testing
			* 
		* conversion and validation
		
		* int manipulation
		* arraylist maniputlation
		* string manipulation

11 03 21
	[1111]
	[1322]
	[1456 1730] shopee
	[1900 2311] finishing, next page

	* scrape
		* shopee.ph
			/ get_price /
				items[item_basic][price]
			/ get_currency /
				items[item_basic][currency]
				or default
			/ get_promo /
				items[item_basic][price_min_before_discount]
				items[item_basic][discount]
			/ get_url
				https://shopee.ph/Two-Tone-Desk-Mat-Laptop-Mat-Pad-Gaming-Large-Mouse-Pad-Long-Mousepad-Leatherette-Waterproof-FIVERS-i.278437261.5353588707?
				shopid = items[item_basic][shopid]
				itemid = items[item_basic][itemid]
				re.sub('[^A-Za-z0-9]+', '-', items['item_basic']['name'])
                url = f'https://shopee.ph/{name}-i.{str(shopid)}.{str(itemid)}?'
                source, value = str(tag), url
			** get_brand
				tag[item_basic][brand]
			/ get_title
				tag[item_basic][name]
			** get_description
				tag[item_basic][brand]
			/ get_rating score
				tag[item_basic][item_rating][rating_star]
			/ get_rating reviews
				tag[item_basic][item_rating][rating_star]
				tag[item_basic][item_rating][rating_count][0]
			/ get_availability
				tag[item_basic][stock]
			/ get_image_url
				tag[item_basic][image]
				"https://cf.shopee.ph/file/6821b2d9090e126d61e7e88784aa39df"
			** next page
				should_next_page
			* tests
		* maxitoys.be

	* krissy
		* know fundamental process of conversion 

	* number system conversion

	* others
		* scraping improvement
			* link all the strategies like:
				* regex to remove consecutive spaces: can be found in extract-tranform comparison
				* get the url category list
				* get the url itemid, shopid
				* how to check multiple keys in dict 
					* all (k in product_tag.get('item_basic') for k in ('itemid', 'shopid', 'name')):

11 04 21
	[0620 0636] warm up typing (16)
	[0636 0717] pyramid class (41)
	// [0717 0800] bfast, animal feeding (43)
	[0800 0817] floor (17)
	[0819 0836] isbn10 simple and isbn10 strong (17)
	[0839 0917] shopee next page and test
	[1028 1131] chat with kt team
	[1132 1140] develop: maxitoys
	[1330 1347] meeting
	[1347 1855] develop: maxitoys 

	/ shopee
		/ nextpage
		/ test

	/ maxitoys.be
		/ get_price /
		** get_currency /
		/ get_promo /
		/ get_url /
		/ get_brand /
		/ get_title /
		** get_description
		** get_rating score
		** get_rating reviews
		** get_availability
		/ get_image_url
		** next page
			should_next_page
		/ test
		/ backup
		/ report

	Intel
		* rerun - 
		* reetl - wa ka dayun ang data padung sql but couch
		* re webshot

11 05 21
	[0645 0730] cleaning, quick sort, bfast
	[0730 1547] listenguided intruction, execute guided instructions, execute acual bug fixes
	[1629 ]

	/ tranfer instructions
	/ backup files
	/ fix prod 1 in putty
	/ listen guided instructions
	/ execute guided instructions
	/ execute actual bug fixes
		/ Validation-PRODUCT : 622
		/ Validation-PRODUCT : 624
		/ Validation-Rankings : 378
		/ Validation-Rankings : 379

 	/ pyramid class
 	/ isbn10
 	* regex theoretical
	* int manipulation
	* data type conversions with validation
	* number system conversion automated
	* array manipulation

	* krissy
 		

11 06 21
	[0645 1130] cleaning (4hr45m)
	[1320 1345] doctrination (25)
	[1713] warmup  

 11 07 21
 	[0715 0729] warm up
 	[1631] 

	/ church
	/ doctrination

	* data type conversion initiation
11 08 21
	[0615 0702] cleaning, quick sort
	[0702 0714] warm up
	[0714 0736] eat, feeding
	[0736 0917] switch case mastery
	[1141 1408] data type conversion: string to everything 
	[1408 1543] bath, master: data type convertions: string to everything, data type conversion: fix double parse to int
	[1732 2033] kim, dinner, finishing of conversion 

	/ cleaning
	/ warm up
		* 95+ wpm (7) (25 words)
		* 95+ wpm (2) (50 words)

	/ data type conversion
		/ master switch validation and loop control 3x
			/ structure (loop, handlers, switch case) and loop control
			/ string
				/ toChar
				/ toInt
				/ toDouble
			/ int
				/ toString
				/ toDouble
			/ double
				/ toString
				/ toInt
			/ char
				/ string
				/ int (decimal)

11 09 21
	9hrs29mins
	[0907 0921] warm up typing (14)
	[0921 0932] regex review (11)
	[0932 1230] conversion warm up (2hr58min)
	[0100 1415] file handling (2hr15min)
	[1615 1815] file handling (2hr)
	[1824 1929] regex quiizz (1hr5min)
	[1929 2115] file handling complete (1hr46min)

	/ typing
		/ 5 (25 words) (100wpm)
		/ 2 (50 words) (100 wpm)

	/ do all conversion as warm up
	/ do isbn10
		/ simple
		/ strong

	* file handling 
		/ delete
			/ file
			/ folder
		/ get file information
		/ mkdir
		/ getDirectories
		/ move to versions

		/ combination
			/ mkdir
			/ get file information && getdirectories
				/ if good, get path
			/ test two methods
				/ move to the created created folder(mkdir result)
				/ move and the cratead file and delete the old file
				* read the file

	/ regex
		/ regex theorerical
		/ regex tests

	/ backup all
		/ tasks
		/ dict[1]

11 10 21
	[0700 0837] quick sort, pasayan retrieval, poop clearing, cleaning, cat feeding
	[0837 0925] brush teeth, make script
	[0925 0957] comms, tech val

	/ make script
	* backup all
		/ script
		* check what to backup
	* git
	/ rerun procedures
	/ hotkey country code

	/ tech val fix
		/ tech-product
			/ 1059
		 	/ 1060
		 	/ 1061
		/ daily - compa
			/ dns-shop.ru
			/ 783
			/ 778
			/ 796

11 11 21
	[0645 0715] cleaning
	[0715 0944] emotional adjustment
	[0944 2200] bug fix  

	/ investigate how to use bulk webshot

11 12 21
	[0700 1000] assignment
	[1000 1230] bug fixes
	[0115 1800] bug fixes
	[1915 2235] dict

	/ assignment
	/ fix rankings and push
		/ 1
		/ 2 
		/ 3
	/ dict exercise
		/ isbn10
		/ String > char, int, double
		/ Int > string, double
	/ backup

11 13 21
	[0700 0813] cleaning, bfast, animal feeding program
	[0813 ]



	/ setup
	* fix 
		/ xxl.se
			/ rank 300
			/ rank 307
			/ rank 320

	* setup hotkey after fix
		/ company code
		/ company name
		/ etc
		* prod number
	* fix runner tool for rankings 

	// * how to make productivity report

11 14 21
	/ sunday service
	/ cleaning
	/ file handling 

11 15 21
	[] cleaning, warm up
	[] dict warm up
	[1530 1619] number systems conversion 
	[1619 1629] number systems conversion: legacy decimal to int (10)
	[1629 1639] number systems conversion: legacy decimal to int (10)
	[1639 1649] number systems conversion: legacy decimal to int (10)

	[1649 1659] number systems conversion: legacy decimal to octal (10)
	[1649 1659] number systems conversion: legacy decimal to octal (10)

	[1659 1709] number systems conversion: legacy decimal to hexadecimal (10)
	[1709 1719] number systems conversion: legacy decimal to hexadecimal (10)
	[1719 1729] number systems conversion: legacy decimal to hexadecimal (10)
	[1729 1739] number systems conversion: legacy decimal to hexadecimal (10)

	[1739 1836] number systems conversion: automated conversion and ascii validation ()
	[1836 1840] number systems conversion: automated conversion and ascii validation ()
	[1853 1900] number systems conversion: automated conversion and ascii validation ()

	[1919 1929] number systems conversion: automated conversion and ascii validation ()
	[2050 2147] number systems conversion: automated conversion and ascii validation ()

	/ cleaning
	/ warm up
		/ 3x 100+ wpm 25 words
		/ 2x 100+ wpm 50 words

	/ dict
		[1030]
			/ warmup isbn10
			/ warmup file handling
				/ read file
				/ rename
				/ transfer

		/ transition to netbeans

		/ number system conversion (promotes krissy, string manipulation and int manipulation)
			/ legacy
				/ decimal to binary
				/ decimal to octal
				/ decimal to hexadecimal
			/ shortcut

	/ authorization letter

11 16 21
	[1343 1353] number systems conversion
	[1432 1456] decimal to anything
	[1512 1542] anything to decimal
	[1605 1640] isbn13
	[1715 1727] collections, sortation
	[2014] 

	warm up
		/ 3 100+ wpm (25 words)
		/ 2 100+ wpm (50 words)
		/ 1 99 wpm (100 words)

		/ master two way number system conversion
		/ isbn13
		* krissy
			/ collections (sort, reverse, shuffle)
				/ letters
				/ numbers
				/ double
					/ random
					/ setScale (2) // or 2 decimal places
			/ sortation
			* string builder
			/ know fundamental process of conversion
			/ identify biate on phone screenshot

11 17 21
	[0630 0700] prod setup
	[0700 0849] cleaning, bath, bfast, quicksort
	[0849 0900] warm up
	[0900 1217] coms with team, load
	[1217 ]

	/ warm up
		/ 2 101+ wpm (50 words)
		/ 3 101+ wpm (25 words) 
		/ 1 101 wpm (100 words)

	/ setup detail
		/ dbeaver
		/ sourcetree
		/ load prod

11 18 21
	[0600 0713] cleaning, quick sort
	[0715 0829] create algorithm 
	[0945 1005] create algorithm
	[1005 1900] create algorithm

11 19 21
	[0600 0700] cleaning, accept greets

11 20 21
	[0730 0905] cleaning, quick sort, bfast, cats feeding, kim to bank
	[1021 1051] fix bug
	[1051 1120] procrastinate, fix bug
	[1120 1150] fix bug compa, test
	[1150 1220] test
	[1437 1507] investigate listings
	[1507 1537] investigate listings
	[1537 1607] investigate listings
	[1607 1637] investigate listings
	[1637] fix and test listings


	
		/ 3 102+ wpm (25 words)

11 21 21
	[0832 0931s] warm up

	/ warmup
		/ 3 102+ wpm (25 words)
		/ 2 102+ wpm (50 words)
		/ 1 102+ wpm (100 words)

11 22 21
	/ clothoes, quick sort, cleaning [0600-0700]
	
	/ check adjust and push [0930-1000] 
		/ get info sir jim

	/ warm up [1515]
		/ 1 103+ wpm (100 words)
		/ 2 103+wpm (50 words)
		/ 3 103+wpm (25 words)

	* cipher

11 23 21 - 
	[0630 0830] string builder, cipher
	[0847 1021] cipher experiment
	[1021 1143] cipher mastery
	[1143 1213] cipher mastery
	[1400 1444] warm up
	[1444 1534] writing essential things to remember
	[1534 1900] kim, eat, dict invesigation and decision
	[1900 2255] cipher, extras, data type conversion, load sort

	* cleaning, clearing
	/ cipher [0630 0700]
		/ string manipulation using stringbuiler
		/ do cipher exercise
	* extreme test (0/2 iterations)
		/ extras
			/ regex exercises 
			/ collections
				/ radomize and sort
					/ letters
					/ numbers
					/ doubles
						/ use bigdecimal
					/ boolean
		/ data type conversion
			/ string to anything
			/ int to anything
			/ double to anything
	

	* prep [1900-2000]
		/ clothes
			* toothbrush
			/ shirt, short pants, brief, handkerchief
			/ vaccine cards, ids, detail papers, other papers
		* load to phone
			/ java public tome of knowledge
			/ samples
				/ isbn10
				/ isbn13
				/ cipher
			/ other samples
			* new music
		* detail requirements
			* report to sir ryan or sir jave for tomorrow plan

	/ sort pc, quick
	/ backup
		/ dict
		/ inriver

	/ teach kim calamari
		/ setup browser
		/ clock

11 24 21 - 11 25 21
	/ message to supervisor
		/ message to everyone
		/ create letter

	* prep
		/ clothes
		/ money
		/ load laptop, keyboard, charger, electronics
		* what to wear tom
		* what to eat tom
		/ paper for dict

	/ nailcut
	/ message biate
		* remessage

	/ number systems conversion
		/ decimal to anything
			/ 00 13 45
		/ anything to decimal
			/ 00 09 07
	* file handling
	/ isbn10, 
	/ cipher
		00 21 13
		00 10 55 (logic only)
		00 09 00 (logic only)

	/ krissy
		00 07 31 (guided)
		00 16 23 (complete)
		00 06 45 (complete)
		04 38 06 (logic only)

	/ reseearch [1600 1900]
		/ krissy

11 26 21
	/ travel back
	/ duty

11 27 21
	[0830 0845] cleaning

	/ warm up
		1 105 wpm (100 words)
		2 105+ wpm (50 words)
		3 105+ wpm (25 words)

	/ bug fix

11 28 21
	[0535 0800] prep, worship god
	[0800 0841] quick sort, cleaning, breakfast
	[0841 0849] warm up 

	/ warmup
		/ 1 103+ wpm (100 words)
		/ 2 103+ wpm (50 words)
		/ 3 103+ wpm (25 words)

	* daily issues
		* improve sql query only get the needed columns

	* node instructions
		* print? instructions
			* all setups
			* issues > solution in bug fixes
				* all decisoin making

		* create first things first
			* open all sites
				* list of sites and applications
			* dbeaver procedures
			* bug fix procedures in all metrics
				* technical analysis in all problems (issues solution: like an ai) : refer to 'fix scenarios'
			* rerun procedures
			* git procedures

	* gantt chart

// create java file for scripting
// puppeteer

// now is the best time to learn and push while not busy
// * others
// 		* scraping improvement
// 			* link all the strategies like:
// 				* regex to remove consecutive spaces: can be found in extract-tranform comparison
// 				* get the url category list
// 				* get the url itemid, shopid
// 				* how to check multiple keys in dict 
// 					* all (k in product_tag.get('item_basic') for k in ('itemid', 'shopid', 'name')):

11 30 21
	* warm up


12 03 21
	/ change user name
	/ change user cmd
		/ copy the youtube url instruction in configuration
	/ audio driver
		/ test music
		/ test mic

	* install required 
		* sublime
		* putty
		* sourcetree
		* dbeaver
	* transfer files
		* inriver