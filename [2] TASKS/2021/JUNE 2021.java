-------------------
// * oop fundamental [3 4] 
// * string manipulation advanced [5 6]
// * array manipulation advanced [7 8]

// * the leak [9 10 11 12]
// * file handling
// 	* declarations [13 14]
// 	* mini app [15 16]

// * mini programs [18 19 20 17]

// * extreme test [21 22 23 24]
// 	* file handling
// 	* the leak
// 	* mini programs

new schedule
	* string manipulation [20]
	* int manipulation [20]
	* array manipulation [20]

	/ regex and validation [16]
	* sortation [16]
	* conversions [16] 50%

	* the leak
		/ isbn [16]
		* cipher / decipher [16]
		* number systems [17]
		* number to words [17]

	* file handling [18 19]
	
	* extreme test to all [21 24] (3 days) 
		6 hours straight a day, 5 mins break every 30 mins
-------------------

06 01 21
	* warmup 
	/ write transpo 
	* transfer basic java declarations on quizzer
	
	* sublime sortation 
	* files sortation

	* Dict research and execution: to implement
		* String manipulation (advanced)
		* Array manipulation (research array manipulation)
			* array manipulation basic
			* arraylist manipulation
		/ recursion
			/ Use recursion to add all of the numbers up to 10.
			/ halting condition
		* switch case declaration

	* oop

06 02 21
	/ warm up
		/ user input 
		/ recursion 
		/ switch case declaration

06 03 21	
	* sortation
		* new arsenal
		* gdrive for dict repository

	* switch case declaration 
	* practice liwanag class 
	* practice advanced string manipulation
	* string manipulation advanced

	* research oop

06 05 21
	/ warmup
	/ sortation
		/ new arsenal
		* gdrive for dict repository

	/ switch case declaration 
	* practice basic string manipulation
	* practice basic number manipulation

	[1341 1319] fibonacci (fundamental programs) (25)

06 06 21
	/ tae
	* laba
	* continue with string manipulation and numbers manipulation
	* add all your learnings to extreme test

	* assignment: 
		* how to get specific digit of decimal number in java
			e.g.: 3.1416 - get 4, how?
		* how to identify if the number is decimal

06 07 21
	* string manipulation
	
	* 14309
	* 2500
	* 1000
	* 1350
	127 212 627 89 

06 09 21
	/ pratice this:
		/ factorial
		/ armstrong number

06 10 21
	/ input all things we learned to the tester as:
		/ fundamental code declaration
		/ hands on
	* add this
		* different types of sortation

06 10 21
	// * transpo
	* the leak
		* isbn validator
			/ test with check number X. use String as input not int
			* test with this: // https://www.youtube.com/watch?v=5qcrDnJg-98
			* test with isbn 13
			* test with regex
			* test with isbn 10 to isbn 13 converter
			* test with isbn 13 to isbn 10 converter
		* regex

	* practice with java code fundamental declarations
		* do conversion tests

	* add to arsenal

06 11 21
	* the leak
		* isbn validator 
			* test with this: // https://www.youtube.com/watch?v=5qcrDnJg-98
			/ isbn 10
			* test with isbn 13
			/ test with regex
			* test with isbn 10 to isbn 13 converter
			* test with isbn 13 to isbn 10 converter
		/ regex
			/ practice the patter and matcher declaration
			/ practical use
			/ apply with String replaceAll()

		/ add to arsenal

06 12 21
	* litter box
	* clothes

	* regex (hardening)
		/ practice the patter and matcher declaration
		/ practical use
		/ apply with String replaceAll()

	* isbn
		* isbn 10 with validation
		* test isbn 13
		* test isbn convertions
			* 10 to 13
			* 13 to 10

	* practice with java code fundamental declarations
		* do conversion tests
		* string manipulation
			* some built-in functions

06 13 21
	/ litter box
	/ clothes

	* regex (hardening)
		/ practice the patter and matcher declaration
		/ practical use
		/ apply with String replaceAll()

		/ practice 3 types of declaration
		* practice simple problems
		* do advanced or practical

	* isbn
		/ isbn 10 with regex validation
		/ test isbn 13 with regex validation
		/ isbn 13 and isbn 10 with regex validation
		* test isbn convertions
			* 10 to 13
			* 13 to 10

	* prayer meeting

06 14 21
	/ warm up
	/ break

06 15 21
	* conversions
		/ research
		/ declarations
		/ practice basic
		* practice advanced

	* add to quizzer
		* conversions
			* forcing character to show the integer value
		* multiple input from different scopes using Scanner
			// https://stackoverflow.com/questions/17622556/scanner-method-opened-and-closed-twice
		* what you learned from regex

	* back read all the questions

	* sort
		// * https://stackoverflow.com/questions/4450045/difference-between-matches-and-find-in-java-regex
		// https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
		// https://stackoverflow.com/questions/14134558/list-of-all-special-characters-that-need-to-be-escaped-in-a-regex
		// https://www.topshelfcomix.com/catalog/isbn-list

	* to practice
		* concat decimal points in decimal
		* float and double manipulation
			* display 3 digits
			* round off to what digit and display

06 16 21
	* conversions
		/ research
		/ declarations
		/ hardening
		* practice advanced

	[] conversions (hardening) (25)
	[] conversions (hardening) (25)
	[] conversions (hardening) (25)
	[] conversions (hardening) (25)
	[] conversions (hardening) (25)

	[] conversions float string (25)
	[] conversions double string  (25)
	[] conversion  char to int (25)
	[] conversions char to string , int to char (25)
	[] convedrsions research for decimal manipulation (25)

06 (17-18-19) 21
	* test
		* conversions
			* characters to anything
			* decimal number manipulations
			* date manipulation

			* decimal
			* hexadecimal
			* octal 

	* conversions
		* research
		* declarations
		* practice basic
		* practice advanced

	* sorations
		* research
		* practice basic
		* practice advanced or practical

	* cipher / decipher
	* number systems
	* number to words

	[] conversions char to int
	[] conversions understanding the char behaviour

06 19 21
	* test
		* conversions
			* characters to anything
			* decimal number manipulations
			* date manipulation

			* decimal
			* hexadecimal
			* octal 

	* conversions
		* research
		* declarations
		* practice basic
		* practice advanced

	* sorations
		* research
		* practice basic
		* practice advanced or practical

	* cipher / decipher
	* number systems
	* number to words

	[] int to character (25)
	// [] audit
	[] 

	exp
		06 18 21
			received
				220 last change from ampayon
				200 kim
				10000 nny
				= 10420

			exp personal
				420 fare and meals
				= 420

			exp nny
				126 pretea  (payable bboy)
				500 pet welfare balance
				= 626

		06 19 21
			exp nny
				20 fare color shot 
				890 gasul
				20 fare home
				= 930

			exp personal
				20 fare mercado (payable bboy)
				380 charger (payable bboy)
				20 fare home
				= 420

			-14 loss (payable bboy)

06 (20 21) 21	
	* test
		* date manipulation

		* number systems
			* decimal
			* hexadecimal
			* octal 

	* number to words

	* sortation
		* research
		* practice basic
		* practice advanced or practical

	* oop for pillars [21]
	* string manipulation [21]
	* int manipulation [21]
	* array manipulation [21] 

	* 06 20 21	
		[] conversions boolean to string (25)
		[] conversions number systems conversions: research (25)
		[] conversions number systems conversions: research (25)

		[] decimal manipulation (25)
		[] conversions, decimal manipulations (25)
		[] decimal manipulation (25)
		[] decimal manipulation [hardening] (25)
		[] decimal manipulation [encode instruction] (25)

		[] binary to decimal (25)
		[] binary to decimal (25)

	* 06 21 21
		* [] netbeans setup (25)
		* [] netbeans setup (25)
		* [] transfer files (25)
		* [] test netbeans with decimal manipulation (25)

		* [] string to date format (25)
		* [] date format: research, execute (25)
		* [] date format: research, execute (25)

06 22 21
	* test
		/ date manipulation

		* number systems
			* decimal
			* hexadecimal
			* octal 

	* number to words

	* sortation
		* research
		* practice basic
		* practice advanced or practical

	* oop for pillars 
	* string manipulation
	* int manipulation
	* array manipulation 

	[] finish date manipulation (hardening) (25)
	[] research: decimal to binary (25)
	[] research: decimal to binary (25)
	[] research: decimal to binary (25)

	[] research: binary to decimal(25)
	[1811] resetup (25)
	[1830] binary to decimal, rewrite essential methods, decimal manipulation, transpo (25)
	[2243] binary to decimal (25)
	[2316] decimal to binary (25)

06 23 21
	* test
		/ date manipulation

		* number systems
			/ decimal to binary and vice versa
			/ hexadecimal
			/ octal 

	* number to words

	* sortation
		* research
		* practice basic
		* practice advanced or practical

	* oop for pillars 
	* string manipulation
	* int manipulation
	* array manipulation 

	[] setup, start  (25)
	[] decimal to binary (25)
	[] decimal to binary (25) 
	[2337] decimal to binary (hardening) (25)

	[] binary to decimal (hardening) (25)
	[] binary to decimal (hardening) (25)
	[0157] binary to octal, binary to hexadecimal (25)
	[] binary to hexa ()

06 24 21
	* cipher decipher [0300] 36 minms
	* file handling [0430] 2hrs
	* warmp up all necessaries [0600] 1hrs30mins

	[1110] finish hex (25)
	[1135] finish hex (25)
	[] hex (mastery) (25)
	[] hex (mastery) (25)

	[] decimal everything (25)
	[] number systems conversion (hardening) (25)
	[] number systems conversion (25)
	[] number systems conversions (25)

	[] sortation (25) 
	[] sortation (25)
	[] sortation (25)
	[] sortation (25)

	[] file handling (25)
	[] cipher decipher (25)

	[]

06 27 21
	/ warmup

06 28 21
	* sort files in pc
	* backup files
	* sort receipts
	* make report

	* prepare plan for napolcom
	0950 129 6791