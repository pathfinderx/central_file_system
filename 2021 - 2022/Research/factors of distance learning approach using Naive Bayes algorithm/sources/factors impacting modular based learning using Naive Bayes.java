Research: FACTORS IMPACTING MODULAR BASED LEARNING NAIVE BAYES ALGORITHM
	FACTORS 1.) UK
		* internal factors	
			* age 
			* gender 
			* previous educational level
			* engagement in online learning activities [frequency using engagment in online settings]
		* external factors 
			* region [restrictions of access to learning resources]

	* identifying the relationship between student_characteristics(), demographic_characteristics(groups of humans) and distance_learning_outcomes() central issue to learning_anaylytics() and learning_achievements()

	* unsupervised_learning(clustering algorithm) helps us identify the demographic_characteristics(groups of humans) based on FACTORS to identify the learning_achievements()

	FACTORS 2.) INDIA
		* external factors
			* internet connectivity
			* understanding of basic technology utilization

	FACTORS 3.) JAKARTA INDONESIA 
		* predict the main factors that affect learning outcomes of distance learning students
		* contains 4 assess indicators of distance learning outcomes
			* forum discussion (best indicator to learning outcome)
			* personal assignment
			* quiz
		* the above assessment is then discussed to Prediction_accuracy using Naive_Bayes_Algorithm with Rapid_miner_application

	FACTORS 4.) CZECK
		* limited technical resources

	FACTORS 5.) MALANG INDONESIA
		* covid-19

CHAPTER I REFERENCES:
	1.) UNSUPERVISED LEARNING FOR UNDERSTANDING STUDENT ACHIEVEMENT IN A DISTANCE LEARNING SETTING
		*  discuss how analytics and data mining helps in identifying learning_achievements()
			* supporting details..
			* the hurdles of neglecting demographics and its effects
				* supporting details
		* the purpose of this study is to address challenges by using clustering algorithm to identify the factors to learning achievement
			* what unsupervised_learning(clustering algorithm) was used 
		* the objectives of the study:
			* demographics based on thier characteristics, frequency of learning online activities
			* the achievements specific to groups
		* why unsupervised_learning(clustering algorithm) selected for the research is crucial for this research
			* supporting details
		* the following sections, we describe the method to investigate the influence of different student characteristics on learning achievement in more detail. we then present the finds of a study on a distance learning course data set using this method.

	2.) PROBLEMS FACED BY STUDENTS AND TEACHERS DURING ONLINE EDUCATION DUE TO COVID-19 AND HOW TO RESOLVE THEM
		* the importance of technology to education system or generally the acquisition of knowledge
		* how the pandemic gives constraints

		* how it changes the settings and finilize the decision to settle with online learning platform at home
		
		* identify the demograhics using analytical_tools (unsupervised_learning()?)
		* endeavors are affected with less internet connectivity as engaging to online learning settings
		* supporting details to scope of areas with connectivity and without connectivity

		* budget constraints for connecting major cities and provinces with internet
		* also, with the inadequate hardwares that makes it perenial

		* government and private sectors should set up institions for enrollees to learn basic utilization of technology
		* self problem solving to settle unexpected techological problems	
			* privacy also becomes concern in digital world

	3.) INDONESIA PREDICTION LEARNINGS ACHIEVEMENTS INDICATORS IN DISTANCE LEARNING STUDENTS
		* online learning or distance learning as alternative for face-to-face learning systems
		* online learning is utilized in tertiary education to improve quality of learning
		* online learning as an alternative for limited university capacity
		* hurdles:
			* increasing need for resources due to high demand in which is still inadequate
			* no learning activities are done face-to-face (it must be done with regulation)
			* quality outcome of distance learning
				* through differences in characteristics and learning patters resulting to the affect of learning outcomes
		* it is necessary to predict the the achievement of student learning
			* through naive bayes (because it was achieved before using this algorithm)
			* this has contributed to the accuracy of predictive data (this would provide and indicator for recommendation)

	4.) UTILIZATION OF DISTANCE EDUCATION DURING COVID19 CRISIS
		* due to covid19 crisis, school learning approach in different levels have changed unexpectedly

	5.) 

	.) PERSONAL DATA GATHERING 
		* with the current settings (modular system)
			* problems occur:
				* not doing their job descriptions as a teacher
					* wasted their time for printing
						* and printing takes time 
						* printers get obliterated too
			* solution presented by gordon
				* make books
					* reusable for 3-5 years
					* only printing workbooks and and work sheets
					* the hurdles: 
						* takes time to create books and lack of budget to accomodate millions of students

Chapter I 
	/ due to covid19 crisis, school learning approach in different levels have changed unexpectedly
	/ online learning or distance learning as alternative for face-to-face learning systems

	* discuss how analytics and data mining helps in identifying learning_achievements()
	* supporting details..
	* the hurdles of neglecting demographics and its effects
		* supporting details


----------------------
CHAPTER I CONTENT

Factors Impacting Modular Based Learning Outcomes Using the Naïve Bayes Algorithm

The Covid-19 pandemic that has lasted several months made a significant impact to our way of living. Various branches of government and private sectors are affected because of restriction orders imposed by the government agencies (Flanders Investment & Trade, 2021; IATF Resolutions | IATF, n.d.; The Philippines and COVID-19: Impact on the Private Sector, 2020). Consequently, these procedures are intended to prevent and suppress the spread of COVID19. As a result, the education branch is not an exemption and recently, it has made a prevalent alteration to learning approaches considering the DEPED selected the blended type of learning which includes: modular learning system, radio and television broadcast and online teaching particularly to secondary level education (DepEd Prepares Self-Learning Modules for Education’s New Normal | Department of Education, 2020).

The current state of affairs demand education institutions to be responsive to the current conditions that we undertake, especially for the secondary education where students are not amply contrived to learn independently (Kusumaningrum et al., 2020). Hence, it will induce a great impact to both process of learning and learning outcome (Kusumaningrum et al., 2020). These matters of implications provide both advantage and negative factors. The factor includes the unpreparedness of both educator and student through the effect of inadequacy of such matter: learning materials, electronic devices, internet connection and understanding of basic technology utilization (A. Latip, 2020). While the advantage postulates to flexibility of learning through the utilization of information technology, broadcasting medium and modular learning system (Kusumaningrum et al., 2020). 

On the other hand, distance learning examines the student’s rigorous processing of information with the implementation of learning materials that it possessed. Learning achievements are directly proportional to the accuracy and foresight which necessitates student’s independence and high level of motivation in executing learning processes (Kusumaningrum et al., 2020). Therefore, regardless of the possession of all the required learning materials and mediums, student’s motivation still needs to be taken consideration which inflicts a challenge to both students and educators. 

Another factor includes the demand of maintaining the quality of learning outcomes to students, the fact that there are various differences in demographic characteristics and learning patterns that directly impacts the learning outcomes (Binus University, 2019). For this reason, it is necessary to predict the achievement of student learning outcomes so that the organizing institution can prepare students better (Binus University, 2019). 

Many studies have investigated the predictive factors of student success in distance learning through various demographic characteristics (Liu & d’Aquin, 2017). Nevertheless, student’s characteristics are often neglected in the analysis of factors leading to successful or poor learning outcomes (Liu & d’Aquin, 2017). Moreover, the analysis of diverse factors that may influence students’ learning achievement requires complex learning analytic technique to analyse the mixed data (Liu & d’Aquin, 2017). Some previous studies have predicted the results of student learning outcomes using Naïve Bayes and resulted to generate contributions to the accuracy of predictive data (Binus University, 2019). 

The purpose of this research is to address these challenges by applying unsupervised learning using Naïve Bayes algorithm for investigating the factors impacting students learning outcomes limited only through modular learning system. In the following sections, we describe the method to investigate the influence of different student characteristics on learning achievement with composite details.

----------------------
CHAPTER I REFERENCES
	[1] 
	// The Philippines and COVID-19: Impact on the private sector. (2020, June). UN Women | Asia and the Pacific. https://asiapacific.unwomen.org/en/digital-library/publications/2020/08/the-philippines-and-covid-19-impact-on-the-private-sector

	// Flanders Investment & Trade. (2021, July 16). CORONAVIRUS - The situation in Philippines | Flanders Trade. https://www.flandersinvestmentandtrade.com/export/nieuws/coronavirus-situation-philippines

	// IATF Resolutions | IATF. (n.d.). INTER-AGENCY TASK FORCE. Retrieved September 6, 2021, from https://iatf.doh.gov.ph/?page_id=77

	[3]
	// DepEd prepares Self-Learning Modules for education’s new normal | Department of Education. (2020, July 2). Deped. https://www.deped.gov.ph/2020/07/02/deped-prepares-self-learning-modules-for-educations-new-normal/

	[4] [5] [8]
	// Kusumaningrum, D. E., Budiarti, E. M., Triwiyanto, T., & Utari, R. (2020, October 17). The Effect of Distance Learning in an Online Learning Framework on Student Learning Independence during the Covid-19 Pandemic. IEEE Conference Publication | IEEE Xplore. https://ieeexplore.ieee.org/document/9276564

	[7]
	// A. Latip, "Peran Literasi Teknologi Informasi Dan Komunikasi Pada Pembelajaran Jarak Jauh di Masa Pandemi Covid-19", EduTeach: Jurnal Edukasi dan Teknologi Pembelajaran, vol. 1, no. 2, Jun. 2020.

	[8]
	// A. D. Dumford and A. L. Miller, "Online learning in higher education: exploring advantages and disadvantages for engagement", J Comput High Educ, vol. 30, no. 3, pp. 452-465, Dec. 2018.

	[9] [10]
	// Kusumaningrum, D. E., Budiarti, E. M., Triwiyanto, T., & Utari, R. (2020, October 17). The Effect of Distance Learning in an Online Learning Framework on Student Learning Independence during the Covid-19 Pandemic. IEEE Conference Publication | IEEE Xplore. https://ieeexplore.ieee.org/document/9276564

	[11] [12] [16]
	// Binus University. (2019, December 1). Prediction Learning Achievement Indicators in Distance Learning Students. IEEE Conference Publication | IEEE Xplore. https://ieeexplore.ieee.org/abstract/document/9225897

	[13] [14] [15]
	// Liu, S., & d’Aquin, M. (2017, April 1). Unsupervised learning for understanding student achievement in a distance learning setting. IEEE Conference Publication | IEEE Xplore. https://ieeexplore.ieee.org/document/7943026/

----------------------
	resources:
		// smart and dynamic citation tool
		// https://www.scribbr.com/apa-citation-generator/?select=6ea66180-0f2f-11ec-9b19-b72dd51f6fa1